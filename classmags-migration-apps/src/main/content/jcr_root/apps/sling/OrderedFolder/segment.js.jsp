<%--

 Copyright 1997-2009 Day Management AG
 Barfuesserplatz 6, 4001 Basel, Switzerland
 All Rights Reserved.

 This software is the confidential and proprietary information of
 Day Management AG, ("Confidential Information"). You shall not
 disclose such Confidential Information and shall use it only in
 accordance with the terms of the license agreement you entered into
 with Day.

 ==============================================================================

 Segment renderer for sling:OrderedFolder nodes

 Creates the segmentation registration for the current folder.

--%><%@page
    session="false"
    contentType="application/x-javascript"
    pageEncoding="utf-8"
    import="
        java.io.PrintWriter,
        java.io.StringWriter,
        java.util.Iterator,
        java.util.Set,
        java.util.HashSet,
        java.util.Arrays,
        org.apache.commons.lang3.StringEscapeUtils,
        org.apache.commons.codec.digest.DigestUtils,
        org.apache.sling.api.SlingHttpServletResponse,
        org.apache.sling.api.wrappers.SlingHttpServletResponseWrapper,
        org.apache.sling.api.resource.Resource,
        com.day.cq.wcm.api.NameConstants
    "
%><%
%><%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling" %><%
%><sling:defineObjects /><%

    /* check if this is a parent request (as this script will recurrently call itself) */
    boolean isParentRequest = slingRequest.getAttribute("isParentSegmentJSRequest") == null;

    /* all further requests will be considered as child requests */
    slingRequest.setAttribute("isParentSegmentJSRequest", false);

    /* include children nodes */
    StringBuilder result = new StringBuilder();
    CustomResponseWrapper responseWrapper = new CustomResponseWrapper(slingResponse);
    Iterator<Resource> iter = resourceResolver.listChildren(resource);
    final Set<String> excludedNames = new HashSet<String>(Arrays.asList(NameConstants.NN_CONTENT, "rep:policy"));

    while (iter.hasNext()) {
        Resource child = iter.next();

        /* skip excludedNames */
        if (!excludedNames.contains(child.getName())) {
            RequestDispatcher rd = slingRequest.getRequestDispatcher(child.getPath() + ".segment.js");
            String childResult;

            /* perform request to a child page */
            rd.include(request, responseWrapper);
            childResult = responseWrapper.getString();

            /* append child result to buffer */
            if ((childResult != null) && (childResult.length() > 0)) {
                result.append(childResult);
            }

            /* clear wrapper */
            responseWrapper.clearWriter();
        }
    }

    /* send the buffered result */
    if (isParentRequest) {
        /* this is parent request - contains concatenated segments of all children from parent request */
        String ETag = "\"" + DigestUtils.md5Hex(result.toString()) + "\"";

        response.addHeader("ETag", ETag);

        /* content did not change - return 304 */
        if (ETag.equals(request.getHeader("If-None-Match"))) {
            response.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
            return;
        } else {
            /* returning all segments here */
            %><%= result %><%
        }
    } else {
        /* this is recurrent request called from this script - just return the result */
        %><%= result %><%
    }

%><%!

    class CustomResponseWrapper extends SlingHttpServletResponseWrapper {
        private StringWriter string = new StringWriter();
        private PrintWriter writer = new PrintWriter(string);

        public CustomResponseWrapper(SlingHttpServletResponse slingHttpServletResponse) {
            super(slingHttpServletResponse);
        }

        public PrintWriter getWriter() {
            return writer;
        }

        public String getString() {
            return string.toString();
        }

        public void clearWriter() {
            writer.close();
            string = new StringWriter();
            writer = new PrintWriter(string);
        }
    }

%>