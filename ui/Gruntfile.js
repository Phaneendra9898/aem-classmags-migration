'use strict';

module.exports = function (grunt) {
  // Display the elapsed execution time of grunt tasks
  require('time-grunt')(grunt);

  // Automatically load required Grunt tasks. 
  // Load time of Grunt does not slow down even if there are many plugins.
  require('jit-grunt')(grunt, {
    useminPrepare: 'grunt-usemin',
    ngtemplates: 'grunt-angular-templates'
  });

  // Configurable paths for the application
  var appConfig = {
    app: require('./bower.json').appPath || 'app',
    dist: 'dist'
  };

  // Configuration for all the tasks
  grunt.initConfig({
    // Project settings
    scienceWorld: appConfig,

    // Watches files for changes and runs tasks based on the changed files
    watch: {
      bower: {
        files: ['bower.json']
      },
      js: {
        files: ['<%= scienceWorld.app %>/scripts/{,*/}*.js'],
        tasks: ['newer:jshint:all', 'newer:jscs:all'],
        options: {
          livereload: '<%= connect.options.livereload %>'
        }
      }, 
      sass: {
        files: ['<%= scienceWorld.app %>/styles/{,*/}*.{scss,sass}'],
        tasks: ['sass:server', 'autoprefixer']
      },     
      styles: {
        files: ['<%= scienceWorld.app %>/styles/{,*/}*.css'],
        tasks: ['newer:copy:styles', 'postcss']
      },
      gruntfile: {
        files: ['Gruntfile.js']
      },
      livereload: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          '<%= scienceWorld.app %>/{,*/}*.html',
          '.tmp/styles/{,*/}*.css',
          '<%= scienceWorld.app %>/styles/{,*/}*.{scss,sass}',
          '<%= scienceWorld.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
        ]
      }
    },

    // Compiles Sass to CSS and generates necessary files if requested
    sass: {
      options: {
        includePaths: [
          'bower_components'
        ]
      },
      dist: {
        files: [{
          expand: true,
          cwd: '<%= scienceWorld.app %>/styles',
          src: ['*.scss'],
          dest: '.tmp/styles',
          ext: '.css'
        }]
      },
      server: {
        files: [{
          expand: true,
          cwd: '<%= scienceWorld.app %>/styles',
          src: ['*.scss'],
          dest: '<%= scienceWorld.app %>/styles',
          ext: '.css'
        }]
      }
    },

    // grunt server settings
    connect: {
      options: {
        port: 9000,        
        hostname: 'localhost',
        livereload: 35729
      },
      livereload: {
        options: {
          open: true,
          middleware: function (connect) {
            return [
              connect.static('.tmp'),
              connect().use(
                '/bower_components',
                connect.static('./bower_components')
              ),
              connect().use(
                '/app/styles',
                connect.static('./app/styles')
              ),
              connect.static(appConfig.app)
            ];
          }
        }
      },
      dist: {
        options: {
          open: true,
          base: '<%= scienceWorld.dist %>'
        }
      }
    },

    // Make sure there are no obvious mistakes
    jshint: {
      options: {
        jshintrc: '.jshintrc',
        reporter: require('jshint-stylish')
      },
      all: {
        src: [
          'Gruntfile.js',
          '<%= scienceWorld.app %>/scripts/{,*/}*.js'
        ]
      }
    },

    // code styles are up to par
    jscs: {
      options: {
        config: '.jscsrc',
        verbose: true
      },
      all: {
        src: [
          'Gruntfile.js',
          '<%= scienceWorld.app %>/scripts/{,*/}*.js'
        ]
      }
    },

    // Empties folders to start fresh
    clean: {
      dist: {
        files: [{
          dot: true,
          src: [
            '.tmp',
            '<%= scienceWorld.dist %>/{,*/}*',
            '!<%= scienceWorld.dist %>/.git{,*/}*'
          ]
        }]
      },
      server: '.tmp'
    },

    // Add vendor prefixed styles
    autoprefixer: {
      options: {
        browsers: ['last 1 version']
      },
      server: {
        options: {
          map: true
        },
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '**/*.css',
          dest: '.tmp/styles/'
        }]
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '**/*.css',
          dest: '.tmp/styles/'
        }]
      }
    },

    // Add vendor prefixed styles
    postcss: {
      options: {
        processors: [
          require('autoprefixer-core')({browsers: ['last 1 version']})
        ]
      },
      server: {
        options: {
          map: true
        },
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      },
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/styles/',
          src: '{,*/}*.css',
          dest: '.tmp/styles/'
        }]
      }
    },
    // Renames files for browser caching purposes
    filerev: {
      dist: {
        src: [
          '<%= scienceWorld.dist %>/scripts/{,*/}*.js',
          '<%= scienceWorld.dist %>/styles/{,*/}*.css',
          '<%= scienceWorld.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
          '<%= scienceWorld.dist %>/styles/fonts/*'
        ]
      }
    },

    cssmin: {
      minify: {
        src:  '<%= scienceWorld.dist %>/styles/{,*/}*.css',
        dest: '<%= scienceWorld.dist %>/minified.min.css'
      }
    },

    uglify:{
      uglify: {
        src:  '<%= scienceWorld.dist %>/scripts/{,*/}*.js',
        dest: '<%= scienceWorld.dist %>/mind.min.js'
      }
    },

    // Reads HTML for usemin blocks to enable smart builds that automatically
    // concat, minify and revision files. Creates configurations in memory so
    // additional tasks can operate on them
    useminPrepare: {
      html: '<%= scienceWorld.app %>/index.html',
      options: {
        dest: '<%= scienceWorld.dist %>',
        flow: {
          html: {
            steps: {
              js: ['concat', 'uglifyjs'],
              css: ['cssmin']
            },
            post: {}
          }
        }
      }
    },

    // Performs rewrites based on filerev and the useminPrepare configuration
    usemin: {
      html: ['<%= scienceWorld.dist %>/{,*/}*.html'],
      css: ['<%= scienceWorld.dist %>/styles/{,*/}*.css'],
      js: ['<%= scienceWorld.dist %>/scripts/{,*/}*.js'],
      options: {
        assetsDirs: [
          '<%= scienceWorld.dist %>',
          '<%= scienceWorld.dist %>/images',
          '<%= scienceWorld.dist %>/styles'
        ],
        patterns: {
          js: [[/(images\/[^''""]*\.(png|jpg|jpeg|gif|webp|svg))/g, 'Replacing references to images']]
        }
      }
    },

    imagemin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= scienceWorld.app %>/images',
          src: '{,*/}*.{png,jpg,jpeg,gif}',
          dest: '<%= scienceWorld.dist %>/images'
        }]
      }
    },

    svgmin: {
      dist: {
        files: [{
          expand: true,
          cwd: '<%= scienceWorld.app %>/images',
          src: '{,*/}*.svg',
          dest: '<%= scienceWorld.dist %>/images'
        }]
      }
    },

    htmlmin: {
      dist: {
        options: {
          collapseWhitespace: true,
          conservativeCollapse: true,
          collapseBooleanAttributes: true,
          removeCommentsFromCDATA: true
        },
        files: [{
          expand: true,
          cwd: '<%= scienceWorld.dist %>',
          src: ['*.html'],
          dest: '<%= scienceWorld.dist %>'
        }]
      }
    },

    ngtemplates: {
      dist: {
        options: {
          module: 'scienceWorldApp',
          htmlmin: '<%= htmlmin.dist.options %>',
          usemin: 'scripts/scripts.js'
        },
        cwd: '<%= scienceWorld.app %>',
        src: 'views/{,*/}*.html',
        dest: '.tmp/templateCache.js'
      }
    },

    // ng-annotate tries to make the code safe for minification automatically
    // by using the Angular long form for dependency injection.
    ngAnnotate: {
      dist: {
        files: [{
          expand: true,
          cwd: '.tmp/concat/scripts',
          src: '*.js',
          dest: '.tmp/concat/scripts'
        }]
      }
    },

    // Copies remaining files to places other tasks can use
    copy: {
      dist: {
        files: [{
          expand: true,
          dot: true,
          cwd: '<%= scienceWorld.app %>',
          dest: '<%= scienceWorld.dist %>',
          src: [
            '*.{ico,png,txt}',
            '*.html',
            'images/{,*/}*.{webp}',
            'styles/fonts/{,*/}*.*'
          ]
        }, {
          expand: true,
          cwd: '.tmp/images',
          dest: '<%= scienceWorld.dist %>/images',
          src: ['generated/*']
        }, {
          expand: true,
          cwd: 'bower_components/bootstrap/dist',
          src: 'fonts/*',
          dest: '<%= scienceWorld.dist %>'
        }]
      },
      styles: {
        expand: true,
        cwd: '<%= scienceWorld.app %>/styles',
        dest: '.tmp/styles/',
        src: '{,*/}*.css'
      }
    },

    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: [
        'sass:server'
      ],
      dist: [
        'sass:dist',
        'copy:styles',
        'imagemin',
        'svgmin'
      ]
    }
  });
  
  grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
        if (target === 'dist') {
      return grunt.task.run(['build', 'connect:dist:keepalive']);
  }

  grunt.task.run([
    'clean:server',
    'newer:jshint',
    'newer:jscs',
    'concurrent:server',
    'postcss:server',
    'connect:livereload',
    'watch'
  ]);
  });

  grunt.registerTask('build', [
    'clean:dist',
    'useminPrepare',
    'concurrent:dist',
    'postcss',
    'ngtemplates',
    'concat',
    'ngAnnotate',
    'copy:dist',
    'cssmin',
    'uglify',
    'filerev',
    'usemin',
    'htmlmin'
  ]);

  grunt.registerTask('default', [
    'newer:jshint',
    'newer:jscs',
    'build'
  ]);
};
