var app = angular.module('scienceWorld', []);
app.controller('slideshowCtrl', ["$scope", "$http", function($scope, $http) {
    $scope.imgArray = [
        { "slideshowImagePath": "../../images/slide-show.png", "slideshowTitle": "Bat Anatomy:1", "slideshowCaption": "Large ears and fleshy noseleaves help the bat echolocate.", "slideshowHeader": "Teen Mummy: Chemistry reveals the adventures of a girl who lived nearly 3,500 years ago" },
        { "slideshowImagePath": "../../images/slide-show.png", "slideshowTitle": "Clothing:", "slideshowCaption": "Egtved Girl wore a cropped shirt and a miniskirt—a surprisingly modern outfit, given that she wore it 3,500 years ago.", "slideshowHeader":"Teen Mummy: Chemistry reveals the adventures of a girl who lived nearly 3,500 years ago"},
        { "slideshowImagePath": "../../images/slide-show.png", "slideshowTitle": "Bat Anatomy:1", "slideshowCaption": "Large ears and fleshy noseleaves help the bat echolocate.", "slideshowHeader":"Teen Mummy: Chemistry reveals the adventures of a girl who lived nearly 3,500 years ago" }
    ];
    $scope.coverImagePath = "../../images/articleimage.jpg";
    $scope.backgroundColor = "#ff004b" ;
    $scope.disabledNumber = "true" ;
}]);
