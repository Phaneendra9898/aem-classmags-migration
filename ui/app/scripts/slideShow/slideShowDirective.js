app.directive('swSlideshow', function() {
    return {
        restrict: 'E',
        scope: {
            data: '=',
            coverimage: '=',
            backgroundcolor: '=',
            disablenumber: '=',
            uniqueid: '='
        },
        controller: ['$scope', function ($scope) {
            $scope.slides = angular.copy($scope.data);
            $scope.bgColor = {'background-color': $scope.backgroundcolor};
            
            $('body').keyup(function(event) {
                if (event.keyCode == 37) {
                    $('.left-icon').trigger('click');
                } else if (event.keyCode == 39) {
                    $('.right-icon').trigger('click');
                }
            });
        }],
         templateUrl: 'slideShowPartial.html'
    }
});
