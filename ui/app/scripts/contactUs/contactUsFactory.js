app.factory("contactUsFactory", ["$http", "$q" , function($http, $q){
	var deffered = $q.defer();
	var contactObj = {};

	contactObj.contactRadiochange = function(radiodata){
		//var url = "url?topic="+radiodate;
		var data = {
                params: {
                    'topic':radiodata
                }
            },
            path = location.href +'?'+ $.param(data.params);
		$http.post(path, data.params)
		.then(function(response){
			console.log(response);
			alert(response);
		})
	};
	contactObj.contactFormPost = function(contactData){
		$http.post(location.href, contactData)
		.then(function(response){
			alert(response);
		})
	};

	return contactObj;
}]);