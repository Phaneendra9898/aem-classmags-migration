app.directive('juniorContentTile', function() {
    return {
        restrict: 'E',
        scope: {
            imageSrc: "@",
            imageHyperlink: '@',
            contentTitle: '@',
            displayDate: '@',
            subject: '@',
            description: '@',
            contentType: '@',
            position: '@',
            videoId: '@',
            videoLength: '@',
            styleType: '@',
            showDownloadIcon: '@',
            showShareIcon: '@',
            showViewArticleLink: '@',
            contentDate: '@',
            bookmarkTitle: '@',
            bookmarkDate: '@',
            bookmarkType: '@',
            userRole: '@',
            viewArticleLinkUrl: '@',
            shareLinkUrl: '@',
            displayStyle: '@',
            articleLinkText: '@'
        },
        link: function(scope, elem, attrs) {
            scope.showDownload = scope.showDownloadIcon === 'true';
            scope.showShare = scope.showShareIcon === 'true';
            scope.showView = scope.showViewArticleLink === 'true';
            scope.role = scope.userRole || 'everyone';
            if (scope.contentType === 'lessonPlan' || scope.contentType === 'Lesson Plans') {
                scope.articleLinkText = 'Download Lesson Plan';
            } else if (scope.contentType === 'skillsSheet' || scope.contentType === 'Skills Sheets') {
                scope.articleLinkText = 'Download Skill Sheet';
            } else if (scope.contentType === 'slideshow') {
                scope.articleLinkText = 'Launch Slideshow';
            } else if (scope.contentType === 'game') {
                scope.articleLinkText = 'Download Game';
            } else if (scope.contentType === 'activity') {
                scope.articleLinkText = 'Download Activity';
            } else if (scope.contentType === 'video') {
                scope.articleLinkText = 'Watch Video';
            } else if (scope.contentType === 'article') {
                scope.articleLinkText = 'View Article';
            } else {
                scope.articleLinkText = 'View Article';
            }
        },
        templateUrl: '../../../scripts/common/juniorContentTile/contentTilePartial.html'
    }
});
