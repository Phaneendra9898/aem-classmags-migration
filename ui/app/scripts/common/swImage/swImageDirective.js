app.directive('swImage', function() {
    return {
        restrict: 'E',
        scope: {
            src:'@',
            alt: '@',
            height: '@',
            width: '@',
            imageClass: '@'
        },
        templateUrl: '../../../scripts/common/swImage/swImagePartial.html'
    }
});
