app.directive('swContentTile', function() {
    return {
        restrict: 'E',
        scope: {
            imageSrc:"@",
            imageHyperlink: '@',
            contentTitle: '@',
            displayDate:'@',
            subject:'@',
            description:'@',
            contentType:'@',
            position:'@',
            videoId:'@',
            videoLength:'@',
            styleType:'@',
            showDownloadIcon: '@',
            showShareIcon:'@',
            showViewArticleLink: '@', 
            contentDate:'@',  
            bookmarkTitle:'@',          
            bookmarkDate:'@',            
            bookmarkType:'@',
            userRole:'@',
            viewArticleLinkUrl: '@',
            shareLinkUrl: '@',
            displayStyle: '@'
        },        
        link: function(scope, elem, attrs) {
            scope.showDownload = scope.showDownloadIcon == 'true';
            scope.showShare = scope.showShareIcon == 'true';
            scope.showView = scope.showViewArticleLink == 'true'; 
            scope.role = scope.userRole || 'everyone'; 
        },
        templateUrl: '../../../scripts/common/ssContentTile/contentTilePartial.html'
    }
});
