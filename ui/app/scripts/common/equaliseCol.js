'use strict';
//$(document).ready(function() {
    $(window).on("load resize", function() {
       resourcesColEqual();

        var equalrowarr = $('.equal-row-wrapper');
        if (equalrowarr.length > 0) {
            equalrowarr.each(function() {
                var colmaxHeight  =  0;
                $(this).find('.equal-col-wrapper').height('auto');
                var colArr = $(this).find('.equal-col-wrapper');
                colArr.each(function()  {
                    colmaxHeight  =  Math.max(colmaxHeight,  $(this).outerHeight());

                });

                $(this).find('.equal-col-wrapper').height(colmaxHeight);
            });
        }


    });


//});

function resourcesColEqual(){
     var  maxHeight  =  0,
            colmaxHeight = 0;
        var rowarr = $('.resources-container .row-wrapper');
        var curWidth = $(window).width();
    $('.resources-container .tile-desc').dotdotdot();
        rowarr.each(function() {
            maxHeight  =  0;
            $(this).find('.col-wrapper').height('auto');
            var colArr = $(this).find('.col-wrapper');
            colArr.each(function()  {
                maxHeight  =  Math.max(maxHeight,  $(this).outerHeight());

            });
            if (curWidth <= 481)
                $(this).find('.col-wrapper').height(maxHeight + 30);
            else
                $(this).find('.col-wrapper').height(maxHeight);
        });

}

