$(window).on('load', function() {

		if($('.iarticle-aggregator').length>0)
        seeAllOnLoad();

    }

);

function seeAllOnLoad(){
    var articleArr = $('.iarticle-aggregator .resources-container .row-wrapper'),
            articleArrCount = articleArr.length;

        articleArr.each(function() {
            var colArr = $(this).find('.col-md-3'),
                colArrCount = colArr.length;

            if (colArrCount > 4) {

                for (var i = 4; i <= colArrCount; i++) {
                    $(colArr[i]).hide();
                }

                $(this).find('.see-all [data-state="expand"]').addClass('showarticles');
            } else {
            	$(this).find('.see-all [data-state="expand"]').removeClass('showarticles');
                $(this).find('.see-all [data-state="expand"]').hide();
            }

        });
}


function updateTiles($this, param, event) {
    event.preventDefault();
    var $curResourceConatiner = $this.closest('.resources-container'),
        $curColArr = $($curResourceConatiner).find(' .row-wrapper .col-md-3'),
        curColArrCount = $($curResourceConatiner).find(' .row-wrapper .col-md-3').length;
    
    if (param == 'expand') {
    	$($curResourceConatiner).find('.see-all [data-state="expand"]').removeClass('showarticles');
        $($curResourceConatiner).find('.see-all [data-state="collapse"]').addClass('showarticles');
        $($curColArr).show();
        
    } else if (param == 'collapse') {
        $($curResourceConatiner).find('.see-all [data-state="collapse"]').removeClass('showarticles');
        $($curResourceConatiner).find('.see-all [data-state="expand"]').addClass('showarticles');
        
        for (var i = 4; i <= curColArrCount; i++) {
            $($curColArr[i]).hide();
        }
        
    }

    return false;
}

$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
	if($('.iarticle-aggregator').length>0){
    resourcesColEqual();
    seeAllOnLoad();
	}
});
