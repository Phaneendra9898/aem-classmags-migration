'use strict';

app.factory('myBookmarksFactory', ["$http", "$q", function($http, $q) {
    var getBookmarks = function() {
        var deferred = $q.defer();
        $http({
            url: "myBookmarks.json",
            method: 'GET'
        }).then(function(response) {
            deferred.resolve(response);
        }, function(response) {
            deferred.reject(response);
        });
        return deferred.promise;
    };

    return {
        getBookmarks: getBookmarks
    };
}]);
