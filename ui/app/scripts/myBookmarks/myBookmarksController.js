'use strict';

app.controller('myBookmarksController', ['$scope', 'myBookmarksFactory', '$timeout', function($scope, myBookmarksFactory) {
    initialize();

    /**
     * Function to initialize the scope properties and functions.
     */
    function initialize() {
        $scope.currentTab = 'articles';
        $scope.isSpinnerShow = true;
        $scope.magazinesData = [];
        $scope.magazineList = [];
        $scope.subjectsList = [];
        $scope.subjectsData = [];
        $scope.contentTypeList = [];
        $scope.contentTypesData = [];
        $scope.itemsPerPage = 12;
        $scope.pagedAlerts = [];
        $scope.currentPage = 1;
        $scope.goToPrevPage = goToPrevPage;
        $scope.goToNextPage = goToNextPage;
        $scope.setPage = setPage;
        $scope.openTab = openTab;
        $scope.sortBookmarks = sortBookmarks;
        $scope.filterBookmarks = filterBookmarks;
        $scope.resetFilters = resetFilters;
        sortBookmarks('publishedDate', true);
        myBookmarksFactory.getBookmarks().then(handleSuccess, handleError);
    }

    /**
     * Function to handle the Get bookmarks success response
     * @param  {Object} response [Response from backend]
     */
    function handleSuccess(response) {
        var data = response.data,
            response = data.response;

        if (data && response) {
            var pageData = response.myBookmarksPageData || [],
                assetData = response.myBookmarksAssetData || [];
            $scope.bookmarksData = pageData.concat(assetData);
            $scope.bookmarksCount = $scope.bookmarksData.length;
            if ($scope.bookmarksCount > 0) {
                $scope.magazineList = response.magazinesNameMap;
                Object.keys($scope.magazineList).forEach(function(eachMagazine) {
                    var filteredObject = $scope.bookmarksData.filter(function(eachArticleObject) {
                        eachArticleObject['publishedDate'] = new Date(eachArticleObject.sortingDate);
                        if (eachArticleObject.tags) {
                            eachArticleObject.tags.forEach(function(eachTag) {
                                if ($scope.subjectsList.indexOf(eachTag) === -1) {
                                    $scope.subjectsList.push(eachTag);
                                }
                            });
                        }
                        if ($scope.contentTypeList.indexOf(eachArticleObject.contentType) === -1) {
                            $scope.contentTypeList.push(eachArticleObject.contentType);
                        }
                        return eachArticleObject.magazineType === eachMagazine;
                    });

                    $scope.magazinesData.push({
                        'name': $scope.magazineList[eachMagazine],
                        'id': eachMagazine,
                        'count': filteredObject.length
                    });
                });

                $scope.contentTypeList.forEach(function(eachType) {
                    var filteredData = $scope.bookmarksData.filter(function(eachObject) {
                        return eachObject.contentType === eachType;
                    });

                    $scope.contentTypesData.push({
                        'contentType': eachType,
                        'count': filteredData.length
                    });
                });

                $scope.subjectsList.forEach(function(eachSubject) {
                    var subjectId = eachSubject.replace(/\s/g, '').toLowerCase(),
                        filteredObject = $scope.bookmarksData.filter(function(eachArticleObject) {
                            return eachArticleObject.tags && eachArticleObject.tags.indexOf(eachSubject) !== -1;
                        });
                    $scope.subjectsData.push({
                        'name': eachSubject,
                        'id': subjectId,
                        'count': filteredObject.length
                    });
                });
                $scope.articleList = angular.copy($scope.bookmarksData);
                groupToPages();
            }
        }
        $scope.isSpinnerShow = false;
    }

    /**
     * Function to handle the Get bookmarks failure response
     * @param  {Object} response [Response from backend]
     */
    function handleError(response) {
        //handle error
        $scope.isSpinnerShow = false;
    }

    /**
     * Function to toggle between the tabs
     * @param  {Sring} tabName [Tab name to get selected]
     */
    function openTab(tabName) {
        $scope.currentTab = tabName;
    }

    /**
     * Functio to sort bookmarks based on sort condition passed
     * @param  {String} sortCondition [description]
     */
    function sortBookmarks(sortCondition, sortDirection) {
        $scope.sortCondition = sortCondition;
        $scope.sortDirection = sortDirection;
    }

    /**
     * Function to filter the bookmarks based on the filter type selected
     */
    function filterBookmarks() {
        var filteredArray = [],
            isMagazineFilterSelected = false,
            isSubjectFilterSelected = false,
            isTypeFilterSelected = false,
            magazineFilterResults = [],
            subjectFilterResults = [],
            typeFilterResults = [],
            resultsToFilter = [],
            filterResults = [];

        $scope.magazinesData.forEach(function(eachMagazine) {
            if (eachMagazine.isSelected) {
                filteredArray = $scope.bookmarksData.filter(function(eachBookmark) {
                    return eachMagazine.id === eachBookmark.magazineType;
                });
                magazineFilterResults = magazineFilterResults.concat(filteredArray);
                isMagazineFilterSelected = true;
            }
        });

        resultsToFilter = isMagazineFilterSelected ? magazineFilterResults : $scope.bookmarksData;

        $scope.subjectsData.forEach(function(eachSubject) {
            if (eachSubject.isSelected) {
                filteredArray = resultsToFilter.filter(function(eachBookmark) {
                    return eachBookmark.tags && eachBookmark.tags.indexOf(eachSubject.name) !== -1;
                });
                subjectFilterResults = subjectFilterResults.concat(filteredArray);
                isSubjectFilterSelected = true;
            }
        });

        if (isMagazineFilterSelected && isSubjectFilterSelected) {
            filterResults = subjectFilterResults;
        } else if (isMagazineFilterSelected) {
            filterResults = magazineFilterResults;
        } else if (isSubjectFilterSelected) {
            filterResults = subjectFilterResults;
        } else {
            filterResults = $scope.bookmarksData;
        }

        $scope.contentTypesData.forEach(function(eachType) {
            if (eachType.isSelected) {
                filteredArray = filterResults.filter(function(eachBookmark) {
                    return eachType.contentType === eachBookmark.contentType;
                });
                typeFilterResults = typeFilterResults.concat(filteredArray);
                isTypeFilterSelected = true;
            }
        });

        if (isTypeFilterSelected) {
            filterResults = typeFilterResults;
        }

        $scope.articleList = angular.copy(filterResults);
        $scope.currentPage = 1;
        groupToPages();
    }

    /**
     * Function to reset all filters and refresh the view
     */
    function resetFilters() {
        $scope.subjectsData.forEach(function(eachSubject) {
            eachSubject.isSelected = false;
        });

        $scope.magazinesData.forEach(function(eachMagazine) {
            eachMagazine.isSelected = false;
        });

        $scope.contentTypesData.forEach(function(eachContentType) {
            eachContentType.isSelected = false;
        });

        $scope.articleList = angular.copy($scope.bookmarksData);
        $scope.currentPage = 1;
        groupToPages();
    }

    /**
     * Function to group bookmarks results into different pages
     */
    function groupToPages() {
        $scope.pagedAlerts = [];
        $scope.articleList.forEach(function(eachItem, i) {
            if (i % $scope.itemsPerPage === 0) {
                $scope.pagedAlerts[Math.floor(i / $scope.itemsPerPage)] = [$scope.articleList[i]];
            } else {
                $scope.pagedAlerts[Math.floor(i / $scope.itemsPerPage)].push($scope.articleList[i]);
            }
        });
        getPagesRange();
    }

    /**
     * Function to get the page numbers based on the results
     * @return {[type]} [description]
     */
    function getPagesRange() {
        $scope.pages = [];
        for (var i = 1; i <= $scope.pagedAlerts.length; i++) {
            $scope.pages.push(i);
        }
    }

    /**
     * Function to shift to previous page
     */
    function goToPrevPage() {
        if ($scope.currentPage > 0) {
            $scope.currentPage--;
        }
    }

    /**
     * Function to move to next page
     */
    function goToNextPage() {
        if ($scope.currentPage < $scope.pagedAlerts.length) {
            $scope.currentPage++;
        }
    }

    /**
     * Function to set the current page number with the page number passed
     * @param {String} pageNum [Selected page number]
     */
    function setPage(pageNum) {
        $scope.currentPage = pageNum * 1;
    }
}]);
