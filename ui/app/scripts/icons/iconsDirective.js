app.directive('swIcons', function() {
    return {
        restrict: 'E',
        scope: {
            iconName: '@'
        },
        template: '<div class="col-md-2 icon-classname-content"><div class="icon-classname-content-pad"><div class="icon-height-container"><div class="sw-icon {{iconName}}"></div></div><div class="icon-names"><p>sw-icons-buttons</p><p>{{iconName}}</p></div></div></div>'
    };
});
