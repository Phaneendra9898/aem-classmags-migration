app.directive('swVocabularyPopover', ["$timeout", function($timeout) {
    return {
        restrict: 'E',
        scope: {
            word:"=",
            onopen: '&',
            onclose: '&'
        },
        controller:["$scope","$sce", function($scope, $sce) {            
            angular.element('body').on('click','a.js-sw-popover-close', function(){
                $scope.$apply(function(){
                    $scope.word.show=false;
                });
            });
            $scope.showPopup = function() {
                $timeout(function(){
                    $scope.onopen($scope.word);
                })
            }
            $scope.htmlPopover = $sce.trustAsHtml('<div><div class="close-mark">' 
                +'<a class="js-sw-popover-close" href="javascript:void(0);"><span class="close-icon glyphicon glyphicon-remove"></span></a></div>' +
                '<img class="img-responsive vocabulary-image" src="' + $scope.word.imagepath + '">' +
                '<ul class="list-unstyled pull-left popover-elements">' +
                '<li><h4 class="popover-name">' + $scope.word.name + '</h4></li>' +
                '<li><span class="popover-pronoutext">' + $scope.word.pronunciationtext + '</span></li>' +
                '</ul>' +
                '<a onclick="this.firstChild.play()" class="pull-right">' +
                '<audio src="' + $scope.word.audiopath + '"></audio><span class="glyphicon glyphicon-volume-up audio-icon"></span></a>' +
                '<div class="popover-definition"><p>' + $scope.word.definition + '</p></div></div>');
        }],
        template: '<a class="vocabulary-word" href="javascript:void(0);" popover-trigger="none" popover-placement="auto" popover-is-open="word.show"' 
                    +'ng-click="showPopup()" uib-popover-html="htmlPopover" >{{word.name}}</a>'
    };
}]);
