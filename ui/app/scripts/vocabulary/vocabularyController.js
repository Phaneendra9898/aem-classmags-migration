
   app.controller("vocabularyController", ["$scope", "$sce", "$compile", function($scope, $sce, $compile){
        
        $scope.vocabularyData=[];        
        var $vocabularyDataElement = angular.element("#vocabularySection");
        var $artcileDataSectionElement = angular.element(".js-article-data-section");

         $vocabularyDataElement.find(".js-vocabulary-word").each(function(){
            var $element = angular.element(this);            

            $scope.vocabularyData.push({
                "name":$element.attr('data-vocabulary-word') || "" ,
                "pronunciationtext": $element .attr('data-vocabulary-pronunciationtext')  || "",
                "definition":$element.attr('data-vocabulary-definition')  || "",
                "audiopath":$element.attr('data-vocabulary-audiopath')  || "",
                "imagepath":$element.attr('data-vocabulary-imagepath')  || "",
                "show":false
            });
        });

        angular.forEach($scope.vocabularyData, function(value, key) {
            var articleContent="";
            var articleContentModified="";
            value = $scope.vocabularyData[key];
            var wordRegEx = new RegExp('\\b' + value.name + '\\b', 'i');
             $artcileDataSectionElement.each(function(){
                var matchFound = false;
                $(this).find(".js-article-item").each(function(){
                    var $articleElement = angular.element(this)[0];                
                    articleContent = $articleElement.innerHTML;
                    if(wordRegEx.test(articleContent)){
                        var match = articleContent.match(wordRegEx);
                        var newElement = '<sw-vocabulary-popover'
                                +'  word="vocabularyData['+key+']'
                                +'" onopen="showPopover(\''+value.name+'\')'
                                +'" onclose="closePopover(\''+value.name+'\')'                                            
                                +'" ></sw-vocabulary-popover>';                     
                                articleContentModified = articleContent.replace(wordRegEx, newElement);
                                angular.element(this).children().remove();
                                angular.element(this).append($compile(articleContentModified)($scope));                        
                        matchFound=true;
                        return false;                           
                    }                
                });
                if(matchFound){
                    return false;
                }
            });
        });

        $scope.showPopover = function(word){
            angular.forEach($scope.vocabularyData, function(value, key){
                if($scope.vocabularyData[key].name === word){
                    $scope.vocabularyData[key].show=true;
                }
                else{
                    $scope.vocabularyData[key].show=false;
                }
            })
        }

        $scope.closePopover = function(word){

        }
    }]);

  