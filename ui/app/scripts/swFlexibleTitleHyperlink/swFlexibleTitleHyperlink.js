app.directive('swFlexibleTitleHyperlink', function() {
    return {
        restrict: 'E',
        scope: {
            title:'@',
            hyperLinkText: '@',
            hyperLinkUrl: '@',
            recordsCount: '@',
            color: '@',
            titlePosition: '@',
            isRuleExists: '=',
            titleFontSize: '@',
            imageUrl: '@'
        },
        templateUrl: '../../../scripts/swFlexibleTitleHyperlink/swFlexibleTitleHyperlinkPartial.html'
    };
});
