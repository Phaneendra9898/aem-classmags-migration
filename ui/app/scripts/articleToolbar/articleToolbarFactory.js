app.factory('articleToolbarFactory', ["$http", "$q", function($http, $q) {
    var toggleBookmark = function(operation, resourcePath, appName) {
        var deferred = $q.defer(),
            data = {
                params: {
                    ':operation': 'social:bookmark:' + operation,
                    'bookmarkPagePath': location.pathname,
                    'appName': appName
                }
            },
            path = resourcePath + '.social.json?' + $.param(data.params);
        $http.post(path, data.params)
            .then(function(response) {
                deferred.resolve(response);
            }, function(response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    var toggleBookmarkByContentType = function(operation, resourcePath, appName, contentType, pagePath, isShowViewArticle, viewArticleLink) {
        var deferred = $q.defer(),
            data;
        if (contentType.toLowerCase() === "article") {
            data = {
                params: {
                    ':operation': 'social:bookmark:' + operation,
                    'bookmarkPagePath': pagePath,
                    'appName': appName
                }
            };
        } else {
            data = {
                params: {
                    ':operation': 'social:bookmark:' + operation,
                    'bookmarkedTeachingResourcePath': pagePath,
                    'appName': appName
                }
            };
        }

        if (isShowViewArticle) {
            data.params['viewArticleLinkPath'] = viewArticleLink;
        }

        var path = resourcePath + '.social.json?' + $.param(data.params);
        $http.post(path, data.params)
            .then(function(response) {
                deferred.resolve(response);
            }, function(response) {
                deferred.reject(response);
            });
        return deferred.promise;
    };

    return {
        toggleBookmark: toggleBookmark,
        toggleBookmarkByContentType: toggleBookmarkByContentType
    }
}]);
