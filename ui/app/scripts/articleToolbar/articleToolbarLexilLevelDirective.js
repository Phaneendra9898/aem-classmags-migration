app.directive('swArticleLexilLevel', function() {
    return {
        restrict: 'A',       
        link: function(scope, elem, attrs) {            
            var lexilLevelChange = function(){
                var $artcileDataSectionElement = angular.element(".js-article-data-section");
                $artcileDataSectionElement.each(function(){  
                $(this).find(".js-article-item").each(function(){
                    var $articleItem = angular.element(this);
                    if($articleItem.attr('id') == $(elem).val()-1 ){
                        $articleItem.show();
                    }
                    else{
                        $articleItem.hide();
                    }          
                });
            });
            }
            $(elem).change(function(){
                lexilLevelChange();                
            });
            lexilLevelChange();
        }
    }       
});