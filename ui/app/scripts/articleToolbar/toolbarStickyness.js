$(document).scroll(function() {
    if ($('.article-toolbar-layout').length > 0) {
        var scrollTop = $(window).scrollTop(),
            toolbarOffset = $('.article-toolbar-layout').offset().top,
            distance = (toolbarOffset - scrollTop),
            docScrollTop = $(document).scrollTop(),
        toolbarWrapperHeight = $('.article-toolbar-layout').parents('.acs-commons-resp-colctrl-row').offset().top + $('.article-toolbar-layout').parents('.acs-commons-resp-colctrl-row').height() - 400;
        if (docScrollTop > toolbarWrapperHeight) {
            $('.article-toolbar-layout .article-toolbar').removeClass('position-fixed');
        } else {
            if (distance <= 20) {
                $('.article-toolbar-layout .article-toolbar').addClass('position-fixed');
            } else if ($('.article-toolbar-layout .article-toolbar').hasClass('position-fixed')) {
                $('.article-toolbar-layout .article-toolbar').removeClass('position-fixed');

            }

        }
    }


});