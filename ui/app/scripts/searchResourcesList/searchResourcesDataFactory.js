app.factory('searchResourcesDataFactory', ["$http", "$q", function($http, $q) {

  var getSearchResults = function(params){
	  var deferred = $q.defer();
	  $http({
		  url:"sample.json",
		  method:'GET'
	  }).then(function(response){
		  var results = response;	
          console.log(response);
		  deferred.resolve(response);
	  }, function(response){
		  deffered.reject(response);
	  });	  
	  return deferred.promise;
  }
  
  var getGridData = function(query){
      var searchURL = "/bin/classmags/migration/search";
      var deferred = $q.defer();
      searchURL = searchURL+query;
      $http({
            url: "sample.json",
            method: 'GET'
      }).then(function(response){               
            deferred.resolve(response);  
      }, function(response){
            deferred.reject(response);
      });
      return deferred.promise;   
  }
  
  return {
	  getGridData: getGridData,
      getSearchResults: getSearchResults
  }
 
}]);