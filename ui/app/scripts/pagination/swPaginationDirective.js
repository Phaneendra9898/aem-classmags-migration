app.directive('swPagination', function() {
    return {
        restrict: 'E',
        scope: {
            totalIssuePageCount : '=',
            totalIssueRecords: '=',
            totalSearchArticlesResources: '=',
            totalSearchIssueResources: '=',
            bookmarkArticleList : '=',
            bookmarkCurrentPage : '=',
            records : '=',
            pages : '=',
            myBookmarkPage: '@',
            myBookmarkTotalRecords: '=',
            onPrevious : '&',
            onNext : '&',
            onChange : '&'
        },
        templateUrl: '../../../scripts/pagination/swPaginationPartial.html'
    }
});
