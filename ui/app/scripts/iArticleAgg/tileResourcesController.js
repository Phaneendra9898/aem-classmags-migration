'use strict';
app.controller("tileResourcesController", ["$scope","$timeout", function($scope,$timeout) {
    $('.resources-container .tile-desc').dotdotdot();
    $scope.SeeAllVisible = false;
    $scope.showAll = false;
    var tileArr = angular.element('.row-wrapper .col-md-3');
    var tileCount = angular.element('.row-wrapper .col-md-3').length;
   
    $scope.hideExtraTiles = function() {
        for (var i = 4; i < tileCount; i++) {
            angular.element(tileArr[i]).hide();
        }
    }
    $timeout(function(){
        
       if (angular.element('.row-wrapper .col-md-3').length > 4) {
        $scope.hideExtraTiles();
        $scope.SeeAllVisible = true;

    } else {
        $scope.SeeAllVisible = false;
    } 
        
    });
    
    
    $scope.showExtra = function() {
        $scope.showAll = !$scope.showAll;
        
        angular.element('.row-wrapper .col-md-3').show();    
         $('.resources-container .tile-desc').dotdotdot();
        if (angular.equals($scope.showAll, false)) {
            $scope.hideExtraTiles();
        }    
    }  
}]);