app.directive('commonHeader', function() {
    return {
        restrict: 'E',
        scope: {
            headerText:'@'
        },
        templateUrl: '../../../scripts/commonHeader/commonHeaderPartial.html'
    }
});
