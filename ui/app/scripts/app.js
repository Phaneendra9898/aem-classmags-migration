var app;
(function() {
    app = angular.module("scienceWorld",['ngAnimate', 'ngSanitize','ui.bootstrap', 'dibari.angular-ellipsis']);
    fetchData().then(bootstrapApplication);
    
    function fetchData() {
        var initInjector = angular.injector(["ng"]);
        var $http = initInjector.get("$http");
        return $http.get("/bin/classmags/getuserrole?"+Date.now(), {cache: false}).then(function(response) {
            if(response && response.data && response.data.role){
                app.constant("swUserRole", response.data.role);                 
            }
            else{
                app.constant("swUserRole", false);
            }            
        }, function(errorResponse) {
            app.constant("swUserRole", false);
        });
    }
    
    app.controller("scienceWorldController", ["$scope", "$timeout", "$compile", 
    function($scope, $timeout, $compile) {
    $timeout(function() {
        $(function() {
            $('[data-ajax-component]').each(function() {
                var $this = $(this),
                url = $this.data('url'),
                queryParams = $this.data('ajax-query-parameters');
                url += "?t=" + (new Date()).getTime();

                if (queryParams) {
                url += "&" + queryParams;
                }

                $.get(url, function(data) {
                if (!data.match(/\sdata-ajax-component/)) {
                $this.replaceWith($compile(data)($scope));
                }
                });
            });
        });
        });
    }]);

    function bootstrapApplication() {
    angular.element(document).ready(function() {
        angular.bootstrap(document, ["scienceWorld"]);
    });
    }
}());




