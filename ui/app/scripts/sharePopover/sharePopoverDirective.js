app.directive('swShowPopover', ["$compile", "$sce", function($compile, $sce) {
    return {
        restrict: 'E',
        scope: {
            linkUrl: '@'
        },
        link: function(scope, element, attrs) {
            element.find('a').popover();              
            element.find('a').click(function(){                
                $('[data-toggle="popover"]').popover('hide');                    
                element.find('[data-toggle="popover"]').popover('toggle');
            });              
            
            element.on('click', '.js-share-popover-close', function(){ 
                element.find('[data-toggle="popover"]').popover('hide');            
            });
            
            scope.popoverContent = $sce.trustAsHtml("<span class='close-icon glyphicon glyphicon-remove js-share-popover-close'></span><input class='popover-text form-control' type='text' value='" + scope.linkUrl + "'/>");
        },
        template: '<a  href="" data-toggle="popover" class="share-popover-layout"  data-content="{{popoverContent}}" data-html="true" data-placement="auto top" data-trigger="manual"><span class="sw-icon upload-icon"></span></a>'
    };
}]);
