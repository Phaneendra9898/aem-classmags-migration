app.directive('swContentTiles', function() {
    return {
        restrict: 'E',
        scope: {
            imageSrc:"@",
            imageHyperlink: '@',
            title: '@',
            displayDate:'@',
            subject:'@',
            description:'@',
            contentType:'@',
            position:'@',
            videoId:'@',
            videoLength:'@',
            styleType:'@',
            shareableExtensionFlag: '@',
            shareableRoleFlag:'@' 
        },
        controller: ['$scope', '$timeout', function($scope, $timeout) {
            $scope.isCustom = false;
            $scope.showShareIcon = false;
            $scope.showDownloadIcon = false;
            
            if($scope.contentType){
                if($scope.contentType.toLowerCase() == 'asset' &&
                  $scope.shareableExtensionFlag == 'true' &&
                  $scope.shareableRoleFlag == 'true'){
                        $scope.showShareIcon = true;
                }
                else if($scope.contentType.toLowerCase() == 'article' 
                    || $scope.contentType.toLowerCase() =='issue'){
                    if($scope.shareableRoleFlag == 'true'){
                        $scope.showShareIcon = true;
                    }
                }
                else{
                    $scope.showShareIcon = false;
                }
                
                if($scope.contentType.toLowerCase() == 'asset' && $scope.shareableExtensionFlag == 'true' ){
                    $scope.showDownloadIcon = true;
                }
                else{
                    $scope.showDownloadIcon = false;
                }

                if($scope.contentType.toLowerCase() == 'custom'){
                    $scope.isCustom = true;
                }
            }
            $timeout(function(){
                $('.content-tiles-misc-container .content-description').dotdotdot(); 
                $('.content-tiles-misc-container .content-description-v').dotdotdot(); 
                $(window).resize(function(){
                    $('.content-tiles-misc-container .content-description').dotdotdot(); 
                    $('.content-tiles-misc-container .content-description-v').dotdotdot();   
                });
            });             
        }],
        templateUrl: 'contentTilesPartial.html'
    }
});
