$(document).ready(function() {

       document.getElementById("copyButton").addEventListener("click", function() {
           copyToClipboard(document.getElementById("result"));
       });

       function copyToClipboard(elem) {
           // create hidden text element, if it doesn't already exist
           var targetId = "_hiddenCopyText_";
           var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
           var origSelectionStart, origSelectionEnd;
           if (isInput) {
               // can just use the original source element for the selection and copy
               target = elem;
               origSelectionStart = elem.selectionStart;
               origSelectionEnd = elem.selectionEnd;
           } else {
               // must use a temporary form element for the selection and copy
               target = document.getElementById(targetId);
               if (!target) {
                   var target = document.createElement("textarea");
                   target.style.position = "absolute";
                   target.style.left = "-9999px";
                   target.style.top = "0";
                   target.id = targetId;
                   document.body.appendChild(target);
               }
               target.textContent = elem.textContent;
           }
           // select the content
           var currentFocus = document.activeElement;
           target.focus();
           target.setSelectionRange(0, target.value.length);

           // copy the selection
           var succeed;
           try {
               succeed = document.execCommand("copy");
           } catch (e) {
               succeed = false;
           }
           // restore original focus
           if (currentFocus && typeof currentFocus.focus === "function") {
               currentFocus.focus();
           }

           if (isInput) {
               // restore prior selection
               elem.setSelectionRange(origSelectionStart, origSelectionEnd);
           } else {
               // clear temporary content
               target.textContent = "";
           }
           return succeed;
       }

       $("#dimensionButton").click(function(){
         $("#value2").val(($("#value1").val()*1280)/1440);
         $("#value4").val(($("#value3").val()*1280)/1440);

       });

       $("#apply").click(function() {
           $('#err').text('');
           $("#sampleElement").removeAttr('style');
           var copy = $("#cssText").val().substring($('textarea').val().indexOf("{"));
           copy = copy.replace(/;/g, ",");
           copy = copy.replace(/,/g, "',");
           copy = copy.replace(/:/g, "':'");
           copy = copy.replace(/width/g, "'width");
           copy = copy.replace('height', "'height");
           copy = copy.replace(/font/g, "'font");
           copy = copy.replace(/line-/g, "'line-");
           copy = copy.replace(/letter/g, "'letter");
           copy = copy.replace(/color/g, "'color");
           copy = copy.replace(/text/g, "'text");
           copy = copy.replace(/opacity/g, "'opacity");
           copy = copy.replace(/\n/g, '');
           copy = copy.replace(/,}/g, '}');
           copy = copy.replace(/ /g, '');
           copy = copy.replace(/\'/g, '"')


           try {
               var cssOBJ = JSON.parse(copy);
           } catch (err) {
               $("#err").text(err.message);
               return;
           }



           var fontsArr = [];
           var fontFamily = '';

           var getFontFamily = function() {
               if (cssOBJ['font-family'] == "Lato") {
                   if (cssOBJ['font-weight'] == "thin" || cssOBJ['font-weight'] == 100) {
                       fontFamily = "$font01";
                       cssStyle['font-family'] = "Lato-HairLine";
                       cssStyle['font-weight'] = 100;
                   } else if (cssOBJ['font-weight'] == "extraLight" || cssOBJ['font-weight'] == 200) {
                       fontFamily = "$font02";
                       cssStyle['font-family'] = "Lato";
                       cssStyle['font-weight'] = 200;
                   } else if (cssOBJ['font-weight'] == "light" || cssOBJ['font-weight'] == 300) {
                       fontFamily = "$font03";
                       cssStyle['font-family'] = "Lato-Light";
                       cssStyle['font-weight'] = 300;
                   } else if (cssOBJ['font-weight'] == "normal" || cssOBJ['font-weight'] == 400) {
                       fontFamily = "$font04"
                       cssStyle['font-family'] = "Lato-Regular";
                       cssStyle['font-weight'] = 400;
                   } else if (cssOBJ['font-weight'] == "medium" || cssOBJ['font-weight'] == 500) {
                       fontFamily = "$font05";
                       cssStyle['font-family'] = "Lato";
                       cssStyle['font-weight'] = 500;
                   } else if (cssOBJ['font-weight'] == "semiBold" || cssOBJ['font-weight'] == 600) {
                       fontFamily = "$font06";
                       cssStyle['font-family'] = "Lato";
                       cssStyle['font-weight'] = 600;
                   } else if (cssOBJ['font-weight'] == "bold" || cssOBJ['font-weight'] == 700) {
                       fontFamily = "$font07";
                       cssStyle['font-family'] = "Lato-Bold";
                       cssStyle['font-weight'] = 700;
                   } else if (cssOBJ['font-weight'] == "extraBold" || cssOBJ['font-weight'] == 800) {
                       fontFamily = "$font08";
                       cssStyle['font-family'] = "Lato";
                       cssStyle['font-weight'] = 800;
                   } else if (cssOBJ['font-weight'] == "black" || cssOBJ['font-weight'] == 900) {
                       fontFamily = "$font09";
                       cssStyle['font-family'] = "Lato-Black";
                       cssStyle['font-weight'] = 900;
                   } else if(cssOBJ['font-weight'] == undefined){
                      fontFamily = "$font04"
                       cssStyle['font-family'] = "Lato-Regular";
                       cssStyle['font-weight'] = 400;
                   }else {
                       fontFamily = null;
                   }

               } else if (cssOBJ['font-family'] == "LibreFranklin") {
                   if (cssOBJ['font-weight'] == "thin" || cssOBJ['font-weight'] == 100) {
                       fontFamily = "$font1";
                       cssStyle['font-family'] = "LibreFranklin-Thin";
                       cssStyle['font-weight'] = 100;
                   } else if (cssOBJ['font-weight'] == "extraLight" || cssOBJ['font-weight'] == 200) {
                       fontFamily = "$font2";
                       cssStyle['font-family'] = "LibreFranklin-ExtraLight";
                       cssStyle['font-weight'] = 200;
                   } else if (cssOBJ['font-weight'] == "light" || cssOBJ['font-weight'] == 300) {
                       fontFamily = "$font3";
                       cssStyle['font-family'] = "LibreFranklin-Light";
                       cssStyle['font-weight'] = 300;
                   } else if (cssOBJ['font-weight'] == "normal" || cssOBJ['font-weight'] == 400) {
                       fontFamily = "$font4";
                       cssStyle['font-family'] = "LibreFranklin-Regular";
                       cssStyle['font-weight'] = 400;
                   } else if (cssOBJ['font-weight'] == "medium" || cssOBJ['font-weight'] == 500) {
                       fontFamily = "$font5";
                       cssStyle['font-family'] = "LibreFranklin-Medium";
                       cssStyle['font-weight'] = 500;
                   } else if (cssOBJ['font-weight'] == "semiBold" || cssOBJ['font-weight'] == 600) {
                       fontFamily = "$font6";
                       cssStyle['font-family'] = "LibreFranklin-SemiBold";
                       cssStyle['font-weight'] = 600;
                   } else if (cssOBJ['font-weight'] == "bold" || cssOBJ['font-weight'] == 700) {
                       fontFamily = "$font7";
                       cssStyle['font-family'] = "LibreFranklin-Bold";
                       cssStyle['font-weight'] = 700;
                   } else if (cssOBJ['font-weight'] == "extraBold" || cssOBJ['font-weight'] == 800) {
                       fontFamily = "$font8";
                       cssStyle['font-family'] = "LibreFranklin-ExtraBold";
                       cssStyle['font-weight'] = 800;
                   } else if (cssOBJ['font-weight'] == "black" || cssOBJ['font-weight'] == 900) {
                       fontFamily = "$font9";
                       cssStyle['font-family'] = "LibreFranklin-Black";
                       cssStyle['font-weight'] = 900;
                   } else if(cssOBJ['font-weight'] == undefined){
                       fontFamily = "$font4";
                       cssStyle['font-family'] = "LibreFranklin-Regular";
                       cssStyle['font-weight'] = 400;
                   } else {
                       fontFamily = null;
                   }

               }
               else if (cssOBJ['font-family'] == "NeoSansStd") {
                   if (cssOBJ['font-weight'] == "medium" || cssOBJ['font-weight'] == 500) {
                       fontFamily = "$fontN5";
                       cssStyle['font-family'] = "NeoSansStd-Medium";
                       cssStyle['font-weight'] = 500;
                   } else if (cssOBJ['font-weight'] == "bold" || cssOBJ['font-weight'] == 700) {
                       fontFamily = "$fontN7";
                       cssStyle['font-family'] = "NeoSansStd-Bold";
                       cssStyle['font-weight'] = 700;
                   } else if (cssOBJ['font-weight'] == "black" || cssOBJ['font-weight'] == 900) {
                       fontFamily = "$fontN9";
                       cssStyle['font-family'] = "NeoSansStd-Black";
                       cssStyle['font-weight'] = 900;
                   } else {
                       fontFamily = null;
                   }

               }
               else if (cssOBJ['font-family'] == "Signika") {
                   if (cssOBJ['font-weight'] == "normal" || cssOBJ['font-weight'] == 400) {
                       fontFamily = "$fontS4";
                       cssStyle['font-family'] = "Signika-Regular";
                       cssStyle['font-weight'] = 400;
                   } else if (cssOBJ['font-weight'] == "light" || cssOBJ['font-weight'] == 300) {
                       fontFamily = "$fontS3";
                       cssStyle['font-family'] = "Signika-Regular";
                       cssStyle['font-weight'] = 300;
                   } else if (cssOBJ['font-weight'] == "bold" || cssOBJ['font-weight'] == 700) {
                       fontFamily = "$fontS7";
                       cssStyle['font-family'] = "Signika-Bold";
                       cssStyle['font-weight'] = 700;
                   }else if (cssOBJ['font-weight'] == "semiBold" || cssOBJ['font-weight'] == 600) {
                       fontFamily = "$fontS6";
                       cssStyle['font-family'] = "Signika-Semibold";
                       cssStyle['font-weight'] = 600;
                   } else if(cssOBJ['font-weight'] == undefined){
                        fontFamily = "$fontS4";
                       cssStyle['font-family'] = "Signika-Regular";
                       cssStyle['font-weight'] = 400;
                   }else {
                       fontFamily = null;
                   }

               }
               else if (cssOBJ['font-family'] == "CooperStd") {
                   if (cssOBJ['font-weight'] == "black" || cssOBJ['font-weight'] == 900) {
                       fontFamily = "$fontCR9";
                       cssStyle['font-family'] = "CooperBlackStd";
                       cssStyle['font-weight'] = 900;
                   } else {
                       fontFamily = null;
                   }

               }
               else if (cssOBJ['font-family'] == "ProximaNova") {
                   if (cssOBJ['font-weight'] == "normal" || cssOBJ['font-weight'] == 400) {
                       fontFamily = "$fontP4";
                       cssStyle['font-family'] = "ProximaNova-Regular";
                       cssStyle['font-weight'] = 400;
                   } else if (cssOBJ['font-weight'] == "medium" || cssOBJ['font-weight'] == 500) {
                       fontFamily = "$fontP5";
                       cssStyle['font-family'] = "ProximaNova-Medium";
                       cssStyle['font-weight'] = 500;
                   }else if (cssOBJ['font-weight'] == "bold" || cssOBJ['font-weight'] == 700) {
                       fontFamily = "$fontP7";
                       cssStyle['font-family'] = "ProximaNova-Bold";
                       cssStyle['font-weight'] = 700;
                   }else if (cssOBJ['font-weight'] == "black" || cssOBJ['font-weight'] == 900) {
                       fontFamily = "$fontP9";
                       cssStyle['font-family'] = "ProximaNova-Black";
                       cssStyle['font-weight'] = 900;
                   }else if(cssOBJ['font-weight'] == undefined){
                        fontFamily = "$fontP4";
                       cssStyle['font-family'] = "ProximaNova-Regular";
                       cssStyle['font-weight'] = 400;
                   }else{
					   fontFamily = null;
				   }

               }
               else if (cssOBJ['font-family'] == "ProximaNovaACond") {
                   if (cssOBJ['font-weight'] == "black" || cssOBJ['font-weight'] == 900) {
                       fontFamily = "$fontPA9";
                       cssStyle['font-family'] = "ProximaNovaACond-Black";
                       cssStyle['font-weight'] = 900;
                   } else {
                       fontFamily = null;
                   }

               }else if (cssOBJ['font-family'] == "ProximaNova-Extrabld") {
                   if (cssOBJ['font-weight'] == "normal" || cssOBJ['font-weight'] == 400) {
                       fontFamily = "$fontPE4";
                       cssStyle['font-family'] = "ProximaNova-Extrabld";
                       cssStyle['font-weight'] = 400;
                   } else if(cssOBJ['font-weight'] == undefined){
                       fontFamily = "$fontPE4";
                       cssStyle['font-family'] = "ProximaNova-Extrabld";
                       cssStyle['font-weight'] = 400;
                   }else {
                       fontFamily = null;
                   }

               }else if (cssOBJ['font-family'] == "SlateStd") {
                   if (cssOBJ['font-weight'] == "extraLight" || cssOBJ['font-weight'] == 200) {
                       fontFamily = "$fontSL2";
                       cssStyle['font-family'] = "SlateStd-ExtraLight";
                       cssStyle['font-weight'] = 200;
                   } else if (cssOBJ['font-weight'] == "light" || cssOBJ['font-weight'] == 300) {
                       fontFamily = "$fontSL3";
                       cssStyle['font-family'] = "SlateStd-Light";
                       cssStyle['font-weight'] = 300;
                   } else if (cssOBJ['font-weight'] == "normal" || cssOBJ['font-weight'] == 400) {
                       fontFamily = "$fontSL4";
                       cssStyle['font-family'] = "SlateStd";
                       cssStyle['font-weight'] = 400;
                   } else if (cssOBJ['font-weight'] == "medium" || cssOBJ['font-weight'] == 500) {
                       fontFamily = "$fontSL5";
                       cssStyle['font-family'] = "SlateStd-Medium";
                       cssStyle['font-weight'] = 500;
                   } else if (cssOBJ['font-weight'] == "bold" || cssOBJ['font-weight'] == 700) {
                       fontFamily = "$fontSL7";
                       cssStyle['font-family'] = "SlateStd-Bold";
                       cssStyle['font-weight'] = 700;
                   } else if (cssOBJ['font-weight'] == "black" || cssOBJ['font-weight'] == 900) {
                       fontFamily = "$fontSL9";
                       cssStyle['font-family'] = "SlateStd-Black";
                       cssStyle['font-weight'] = 900;
                   } else if(cssOBJ['font-weight'] == undefined){
                       fontFamily = "$fontSL4";
                       cssStyle['font-family'] = "SlateStd";
                       cssStyle['font-weight'] = 400;
                   }else {
                       fontFamily = null;
                   }

               }else if (cssOBJ['font-family'] == "SerifaStd") {
                   if (cssOBJ['font-weight'] == "thin" || cssOBJ['font-weight'] == 100) {
                       fontFamily = "$fontSE1";
                       cssStyle['font-family'] = "SerifaStd-Thin";
                       cssStyle['font-weight'] = 100;
                   } else if (cssOBJ['font-weight'] == "extraLight" || cssOBJ['font-weight'] == 200) {
                       fontFamily = "$fontSE2";
                       cssStyle['font-family'] = "SerifaStd-ExtraLight";
                       cssStyle['font-weight'] = 200;
                   } else if (cssOBJ['font-weight'] == "light" || cssOBJ['font-weight'] == 300) {
                       fontFamily = "$fontSE3";
                       cssStyle['font-family'] = "SerifaStd-Light";
                       cssStyle['font-weight'] = 300;
                   } else if (cssOBJ['font-weight'] == "normal" || cssOBJ['font-weight'] == 400) {
                       fontFamily = "$fontSE4";
                       cssStyle['font-family'] = "SerifaStd-Regular";
                       cssStyle['font-weight'] = 400;
                   } else if (cssOBJ['font-weight'] == "bold" || cssOBJ['font-weight'] == 700) {
                       fontFamily = "$fontSE7";
                       cssStyle['font-family'] = "SerifaStd-Bold";
                       cssStyle['font-weight'] = 700;
                   } else if (cssOBJ['font-weight'] == "black" || cssOBJ['font-weight'] == 900) {
                       fontFamily = "$fontSE9";
                       cssStyle['font-family'] = "SerifaStd-Black";
                       cssStyle['font-weight'] = 900;
                   } else if(cssOBJ['font-weight'] == undefined){
                        fontFamily = "$fontSE4";
                       cssStyle['font-family'] = "SerifaStd-Regular";
                       cssStyle['font-weight'] = 400;
                   }else {
                       fontFamily = null;
                   }

               }else if (cssOBJ['font-family'] == "UtopiaStd") {
                   if (cssOBJ['font-weight'] == "normal" || cssOBJ['font-weight'] == 400) {
                       fontFamily = "$fontU4";
                       cssStyle['font-family'] = "UtopiaStd-Regular";
                       cssStyle['font-weight'] = 400;
                   } else if (cssOBJ['font-weight'] == "semiBold" || cssOBJ['font-weight'] == 600) {
                       fontFamily = "$fontU6";
                       cssStyle['font-family'] = "UtopiaStd-SemiBold";
                       cssStyle['font-weight'] = 600;
                   } else if (cssOBJ['font-weight'] == "bold" || cssOBJ['font-weight'] == 700) {
                       fontFamily = "$fontU7";
                       cssStyle['font-family'] = "UtopiaStd-Bold";
                       cssStyle['font-weight'] = 700;
                   } else if(cssOBJ['font-weight'] == undefined){
                       fontFamily = "$fontU4";
                       cssStyle['font-family'] = "UtopiaStd-Regular";
                       cssStyle['font-weight'] = 400;
                   } else {
                       fontFamily = null;
                   }

               } else if (cssOBJ['font-family'] == "BasicCommercialLTCom") {
                   if (cssOBJ['font-weight'] == "extraLight" || cssOBJ['font-weight'] == 200) {
                       fontFamily = "$fontB2";
                       cssStyle['font-family'] = "BasicCommercialLTCom-ExtraLight";
                       cssStyle['font-weight'] = 200;
                   } else if (cssOBJ['font-weight'] == "normal" || cssOBJ['font-weight'] == 400) {
                       fontFamily = "$fontB4";
                       cssStyle['font-family'] = "BasicCommercialLTCom-Roman";
                       cssStyle['font-weight'] = 400;
                   } else if (cssOBJ['font-weight'] == "bold" || cssOBJ['font-weight'] == 700) {
                       fontFamily = "$fontB7";
                       cssStyle['font-family'] = "BasicCommercialLTCom-Bold";
                       cssStyle['font-weight'] = 700;
                   } else if (cssOBJ['font-weight'] == "extraBold" || cssOBJ['font-weight'] == 800) {
                       fontFamily = "$fontB8";
                       cssStyle['font-family'] = "BasicCommercialLTCom-Blk";
                       cssStyle['font-weight'] = 800;
                   } else if(cssOBJ['font-weight'] == undefined){
                        fontFamily = "$fontB4";
                       cssStyle['font-family'] = "BasicCommercialLTCom-Roman";
                       cssStyle['font-weight'] = 400;
                   }else {
                       fontFamily = null;
                   }

               } else if (cssOBJ['font-family'] == "Canaro") {
                   if (cssOBJ['font-weight'] == "thin" || cssOBJ['font-weight'] == 100) {
                       fontFamily = "$fontC1";
                       cssStyle['font-family'] = "Canaro-Thin";
                       cssStyle['font-weight'] = 100;
                   } else if (cssOBJ['font-weight'] == "extraLight" || cssOBJ['font-weight'] == 200) {
                       fontFamily = "$fontC2";
                       cssStyle['font-family'] = "Canaro-ExtraLight";
                       cssStyle['font-weight'] = 200;
                   } else if (cssOBJ['font-weight'] == "light" || cssOBJ['font-weight'] == 300) {
                       fontFamily = "$fontC3";
                       cssStyle['font-family'] = "Canaro-Light";
                       cssStyle['font-weight'] = 300;
                   } else if (cssOBJ['font-weight'] == "normal" || cssOBJ['font-weight'] == 400) {
                       fontFamily = "$fontC4";
                       cssStyle['font-family'] = "Canaro-Book";
                       cssStyle['font-weight'] = 400;
                   } else if (cssOBJ['font-weight'] == "medium" || cssOBJ['font-weight'] == 500) {
                       fontFamily = "$fontC5";
                       cssStyle['font-family'] = "Canaro-Medium";
                       cssStyle['font-weight'] = 500;
                   } else if (cssOBJ['font-weight'] == "semiBold" || cssOBJ['font-weight'] == 600) {
                       fontFamily = "$fontC6";
                       cssStyle['font-family'] = "Canaro-SemiBold";
                       cssStyle['font-weight'] = 600;
                   } else if (cssOBJ['font-weight'] == "bold" || cssOBJ['font-weight'] == 700) {
                       fontFamily = "$fontC7";
                       cssStyle['font-family'] = "Canaro-Bold";
                       cssStyle['font-weight'] = 700;
                   } else if (cssOBJ['font-weight'] == "extraBold" || cssOBJ['font-weight'] == 800) {
                       fontFamily = "$fontC8";
                       cssStyle['font-family'] = "Canaro-ExtraBold";
                       cssStyle['font-weight'] = 800;
                   } else if (cssOBJ['font-weight'] == "black" || cssOBJ['font-weight'] == 900) {
                       fontFamily = "$fontC9";
                       cssStyle['font-family'] = "Canaro-Black";
                       cssStyle['font-weight'] = 900;
                   } else if(cssOBJ['font-weight'] == undefined){
                       fontFamily = "$fontC4";
                       cssStyle['font-family'] = "Canaro-Book";
                       cssStyle['font-weight'] = 400;
                   } else {
                       fontFamily = null;
                   }

               } else if (cssOBJ['font-family'] == "VAGRounded") {
                   if (cssOBJ['font-weight'] == "thin" || cssOBJ['font-weight'] == 100) {
                       fontFamily = "$fontV1";
                       cssStyle['font-family'] = "VAGRounded-Thin";
                       cssStyle['font-weight'] = 100;
                   } else if (cssOBJ['font-weight'] == "light" || cssOBJ['font-weight'] == 200) {
                       fontFamily = "$fontV2";
                       cssStyle['font-family'] = "VAGRounded-Light";
                       cssStyle['font-weight'] = 200;
                   } else if (cssOBJ['font-weight'] == "bold" || cssOBJ['font-weight'] == 700) {
                       fontFamily = "$fontV7";
                       cssStyle['font-family'] = "VAGRounded-Bold";
                       cssStyle['font-weight'] = 700;
                   } else if (cssOBJ['font-weight'] == "black" || cssOBJ['font-weight'] == 900) {
                       fontFamily = "$fontV9";
                       cssStyle['font-family'] = "VAGRounded-Black";
                       cssStyle['font-weight'] = 900;
                   } else {
                       fontFamily = null;
                   }

               }else if (cssOBJ['font-family'] == "GeometricSlab703BT-Xtra") {
                   if (cssOBJ['font-weight'] == "light" || cssOBJ['font-weight'] == 200) {
                       fontFamily = "$fontG2";
                       cssStyle['font-family'] = "GeometricSlab703BT-XtraLight";
                       cssStyle['font-weight'] = 200;
                   } else if (cssOBJ['font-weight'] == "medium" || cssOBJ['font-weight'] == 500) {
                       fontFamily = "$fontG5";
                       cssStyle['font-family'] = "GeometricSlab703BT-XtraMedium";
                       cssStyle['font-weight'] = 500;
                   } else if (cssOBJ['font-weight'] == "bold" || cssOBJ['font-weight'] == 700) {
                       fontFamily = "$fontG7";
                       cssStyle['font-family'] = "GeometricSlab703BT-XtraBold";
                       cssStyle['font-weight'] = 700;
                   } else if (cssOBJ['font-weight'] == "black" || cssOBJ['font-weight'] == 900) {
                       fontFamily = "$fontG9";
                       cssStyle['font-family'] = "GeometricSlab703BT-XtraBlack";
                       cssStyle['font-weight'] = 900;
                   } else {
                       fontFamily = null;
                   }

               }else if (cssOBJ['font-family'] == "Geometric706BT-B") {
                   if (cssOBJ['font-weight'] == "medium" || cssOBJ['font-weight'] == 500) {
                       fontFamily = "$fontGB5";
                       cssStyle['font-family'] = "Geometric706BT-MediumB";
                       cssStyle['font-weight'] = 500;
                   }else if (cssOBJ['font-weight'] == "black" || cssOBJ['font-weight'] == 900) {
                       fontFamily = "$fontGB9";
                       cssStyle['font-family'] = "Geometric706BT-BlackB";
                       cssStyle['font-weight'] = 900;
                   } else {
                       fontFamily = null;
                   }

               }else if (cssOBJ['font-family'] == "StoneInformalStd") {
                   if (cssOBJ['font-weight'] == "medium" || cssOBJ['font-weight'] == 500) {
                       fontFamily = "$fontSI5";
                       cssStyle['font-family'] = "StoneInformalStd-Medium";
                       cssStyle['font-weight'] = 500;
                   }else if (cssOBJ['font-weight'] == "semibold" || cssOBJ['font-weight'] == 600) {
                       fontFamily = "$fontSI6";
                       cssStyle['font-family'] = "StoneInformalStd-SemiBold";
                       cssStyle['font-weight'] = 600;
                   }else if (cssOBJ['font-weight'] == "bold" || cssOBJ['font-weight'] == 700) {
                       fontFamily = "$fontSI7";
                       cssStyle['font-family'] = "StoneInformalStd-Bold";
                       cssStyle['font-weight'] = 700;
                   } else {
                       fontFamily = null;
                   }

               }else if (cssOBJ['font-family'] == "KarnakPro") {
                   if (cssOBJ['font-weight'] == "bold" || cssOBJ['font-weight'] == 700) {
                       fontFamily = "$fontK7";
                       cssStyle['font-family'] = "KarnakPro-Bold";
                       cssStyle['font-weight'] = 700;
                   }else if (cssOBJ['font-weight'] == "black" || cssOBJ['font-weight'] == 900) {
                       fontFamily = "$fontK9";
                       cssStyle['font-family'] = "KarnakPro-Black";
                       cssStyle['font-weight'] = 900;
                   }else {
                       fontFamily = null;
                   }

               }
               else {
                   fontFamily = null;
               }
               return fontFamily;
           }

           var colors = [{
               name: '$liteBlue',
               color: '#0099ff'
           }, {
               name: '$navyBlue',
               color: '#0000FF'
           }, {
               name: '$linkBlue',
               color: '#3333FF'
           }, {
               name: '$liteGray',
               color: '#999999'
           }, {
               name: '$lightgray',
               color: 'lightgray'
           }, {
               name: '$dimLiteGray',
               color: '#efefef'
           }, {
               name: '$gray',
               color: '#333333'
           }, {
               name: '$skyBlue',
               color: '#28aedd'
           }, {
               name: '$orange',
               color: '#fb5e00'
           }, {
               name: '$blue',
               color: '#015ddb'
           }, {
               name: '$violet',
               color: '#ab1763'
           }, {
               name: '$white',
               color: '#ffffff'
           }, {
               name: '$liteBlue3',
               color: '#0000FF'
           }, {
               name: '$liteAsh1',
               color: '#ddd'
           }, {
               name: '$liteAsh2',
               color: '#d8d8d8'
           }, {
               name: '$liteAsh3',
               color: '#e3e3e3'
           }, {
               name: '$liteAsh4',
               color: '#f4f4f4'
           }, {
               name: '$litePink',
               color: '#dbbde5'
           }, {
               name: '$liteGreen',
               color: '#b5efce'
           }, {
               name: '$bluishGreen',
               color: '#ace6e8'
           }, {
               name: '$black',
               color: '#000000'
           }, {
               name: '$liteBlue2',
               color: '#2c7aec'
           }, {
               name: '$textLinkBlue',
               color: ' #00C3DE'
           }, {
               name: '$liteAsh5',
               color: '#0c3d6c'
           }, {
               name: '$darkGray',
               color: '#666666'
           }, {
               name: '$bodyGrey',
               color: ' #f9fafb '
           }, {
               name: '$breadGray',
               color: '#555555'
           }, {
               name: '$liteAsh7',
               color: '#504c51'
           }, {
               name: '$liteAsh8',
               color: '#47585f'
           }, {
               name: '$liteAsh6',
               color: '#444444'
           }, {
               name: '$liteGray2',
               color: '#5a5a5a'
           }, {
               name: '$deepRed',
               color: '#fe424d'
           }, {
               name: '$ashGrey',
               color: '#95a0a9'
           }, {
               name: '$liteBlue4',
               color: '#eff4fb'
           }, {
               name: '$darkRed',
               color: '#ea2128'
           }, {
               name: '$bluishGreen2',
               color: '#68d2db'
           }, {
               name: '$lightBlack',
               color: '#191919'
           }, {
               name: '$linkblue',
               color: '#06447f'
           }, {
               name: '$black1',
               color: 'rgba(0, 0, 0, 0.9)'
           }, {
               name: '$black2',
               color: 'rgba(0, 0, 0, 0.6)'
           }, {
               name: '$lightGray1',
               color: '#d5dee4'
           }, {
               name: '$skillSheetTileNewBg',
               color: '#3498db'
           }, {
               name: '$issueTileBgBlue',
               color: '#eff5fc'
           },{
               name: '$issueGridButtonFontColor',
               color: '#174877'
           },{
               name: '$lightGrey2',
               color: '#606162'
           },{
               name: '$tabBgGrey',
               color: '#e2e4e7'
           },{
               name: '$tabBgBlue',
               color: '#00aeef'
           },{
               name: '$tabBgGreyText',
               color: '#818387'
           },{
               name: '$globinpPlaceholdText',
               color: '#00171f'
           },{
               name: '$globalNavExporeGray',
               color: '#7c7c7c'
           },{
               name: '$contentTileSubjectBlue',
               color: '#0054a6'
           },{
               name: '$titleHyperlinkColor',
               color: '#00a6fb'
           },{
               name: '$emailSubjectFieldGrey',
               color: '#777c81'
           },{
               name: '$requiredFieldstextGrey',
               color: '#898989'
           },{
               name: '$errorFieldTextColor',
               color: '#d60000'
           },{
               name: '$emailFormInputFontColor',
               color: '#858585'
           },{
               name: '$textHyperlinkColor',
               color: '#0083ca'
           },{
               name: '$commentFormDescText',
               color: '#282828'
           },{
               name: '$commentLabelTextGrey',
               color: '#595a5b'
           },{
               name: '$commentInputBorder',
               color: '#d7d7d7'
           },{
               name: '$commentFormNoteText',
               color: '#828282'
           },{
               name: '$commentHeadingBorderColor',
               color: '#909090'
           },{
               name: '$marketingSubscriptionGrey',
               color: '#110c0e'
           },{
               name: '$marketingSubscriptionLightGrey',
               color: '#1e1d1e'
           },{
               name: '$marketingSubscriptionBorderGrey',
               color: '#c8c8c8'
           },
         {
           name: '$subscriptionTextGrey',
           color: '#2b2a2b'
         }];

           var getColorName = function(code) {
               return colors.find(function(color) {
                   return color.color.toLowerCase() == code.toLowerCase();
               });
           }
           var cssStyle = {};

           var cssFontString = "@include font(";
           if (cssOBJ['font-family']) {
               if (getFontFamily()) {
                   cssFontString = cssFontString + getFontFamily() + ", ";
               } else {
                   $("#err").text('Font family do not exits.');
                   $("#result").val('');
                   return;
               }
           } else {
               $("#err").text('Font family do not exits. ');
               $("#result").val('');
               return;
           }

           if (cssOBJ['font-size']) {
               cssFontString = cssFontString + cssOBJ['font-size'] + ", ";
               cssStyle['font-size'] = cssOBJ['font-size'];
           } else {
               cssFontString = cssFontString + "null" + ", ";
           }

           if (cssOBJ['line-height']) {
               cssFontString = cssFontString + cssOBJ['line-height'] + ", ";
               cssStyle['line-height'] = cssOBJ['line-height'] * cssOBJ['font-size'].replace("px", "") + "px";
           } else {
               cssFontString = cssFontString + "null" + ", ";
           }

           if (cssOBJ['color']) {

               if (getColorName(cssOBJ['color'])) {
                   cssFontString = cssFontString + getColorName(cssOBJ['color']).name;
                   cssStyle['color'] = cssOBJ['color'];
               } else {
                   $("#err").text('Color do not exists. Please add in constants, fontsDemo and retry.');
                   $("#result").val('');
                   return;
               }
           } else {
               cssFontString = cssFontString + "null";
           }

           if (cssOBJ['letter-spacing']) {
               cssFontString = cssFontString + ", " + cssOBJ['letter-spacing'] + ")";
               cssStyle['letter-spacing'] = cssOBJ['letter-spacing'];
           } else {
               cssFontString = cssFontString + ")";
           }

           cssFontString = cssFontString + ";";

           $("#sampleElement").css(cssStyle);
           $("#result").val(cssFontString);
       });

   });
