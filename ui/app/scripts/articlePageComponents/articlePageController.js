app.controller("articlePageComponentsController", ["$scope", "$timeout", "$rootScope", function($scope, $timeout, $rootScope) {
    /*
        var screenWidth;
        $rootScope.$on("OBJECTIVE_AND_SKILLS", function(event, data) {
            var $elementArticleTextComponent = $("#article-text-page-container").clone();
            var $elementSubjectsCovered = $('.subjects-covered');
            var $elementObjectivesSkills = $('.objectives-and-skills');
            var $elementArticleText = $(".article-text");

            var alignArticleComponents = function() {
                screenWidth = viewport().width;
                if (screenWidth <= 991) {
                    $("#article-text-page-container").empty();
                    $("#article-text-page-container").append($elementArticleText);
                    $("#article-text-page-container").append($elementObjectivesSkills);
                    $("#article-text-page-container").append($elementSubjectsCovered);
                } else {
                    $("#article-text-page-container").empty();
                    $("#article-text-page-container").append($elementArticleTextComponent);
                }

            }
            alignArticleComponents();
            $(window).on("resize", function() {
                alignArticleComponents();
            });

            function viewport() {
                var e = window,
                    a = 'inner';
                if (!('innerWidth' in window)) {
                    a = 'client';
                    e = document.documentElement || document.body;
                }
                return {
                    width: e[a + 'Width'],
                    height: e[a + 'Height']
                };
            }

        });
    */
    $scope.scrollToTop = function() {
    $("html, body").animate({
        scrollTop: 0
    }, 1000);
};
}]);
