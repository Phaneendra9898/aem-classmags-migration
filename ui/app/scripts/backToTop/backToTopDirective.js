app.directive('swBackToTop', function() {
    return {
        restrict: 'E',

        link: function(scope) {
            scope.scrollToTop = function() {
                $("html, body").animate({
                    scrollTop: 0
                }, 1000);
            };
        },

        templateUrl: '../../../scripts/backToTop/backToTopPartial.html'
    }
});
