app.directive('swImage', function() {
    return {
        restrict: 'E',
        scope: {
            src:'@',
            alt: '@',  
            height: '@',
            width: '@'          
        },               
        templateUrl: './imageResponsivePartial.html'
    }
});
