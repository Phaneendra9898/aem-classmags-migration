app.directive('swTextHyperlink', function() {
    return {
        restrict: 'E',
        scope: {
            title:'@',
            hyperLinkText: '@',
            hyperLinkURL: '@',
            bgcolor:"@",
            recordsCount: '@'
        },
        templateUrl: '../../../scripts/textHyperlink/swTextHyperlinkPartial.html'
    }
});
