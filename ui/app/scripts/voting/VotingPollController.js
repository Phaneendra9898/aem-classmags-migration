app.controller("votingPollController", ["$scope", "$sce", "$compile", "votingFactory", function($scope, $sce, $compile, votingFactory) {
  $scope.pollshow = true;
  $scope.studentFormError = false;
  $scope.teacherDataFormError = false;
  $scope.showThanksMessage = false;
  $scope.showVoteEnded = false;
  $scope.isStudent = false;
  $scope.isTeacher = true;
 $scope.teacherFormInvalid =false;
     $scope.showResults =false;

/*
if(swUserRole && (swUserRole.toLowerCase() == "teacher" )){
      $scope.isTeacher = false;
}else if(swUserRole && (swUserRole.toLowerCase() == "student" )){

  $scope.isStudent = true;
}
*/
  $scope.showHidePollResults = function() {
    $scope.pollshow = !$scope.pollshow;
  }
  var votingSuccessCallback = function(response,param) {

      var votingresponse=response.data.response;
      if(param== 'onload'){
          if(!(votingresponse.votingEnabled)){
              $scope.showVoteEnded = true;
             // $scope.showThanksMessage = false;
          }
      }else{
          $scope.showThanksMessage = true;

      }

     prepareGraph(votingresponse);
  }


   var prepareGraph = function(votingresponse){

       var typeOfGraph=angular.element('.voting-poll-view').data('reporttype');
       console.log('typeOfGraph'+typeOfGraph);

       var optionArray=votingresponse.answerOptionsMap;

       var votingItem=[];
       for(var i in optionArray){


		votingItem.push([i,parseInt(optionArray[i])]);
}



			drawGraph(typeOfGraph,votingItem);

   }
  var drawGraph = function(typeOfGraph,votingItem){

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.charts.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.

      function drawChart() {

  console.log(votingItem);

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'VotingOption');
        data.addColumn('number', 'Votes');
        data.addRows(votingItem);

        // Set chart options
        var options = {'width':610,
                       'height':233};

          var chart;
        // Instantiate and draw our chart, passing in some options.
          if(typeOfGraph == "barGraph")
				chart = new google.visualization.ColumnChart(document.getElementById('poll-results-presentation'));
          else

         chart = new google.visualization.PieChart(document.getElementById('poll-results-presentation'));

        chart.draw(data, options);
      }
  }

 var votingData={};
 var votingQueryParams={};
  var votingDataProcessing = function(currentpagepath,votingDateExpiry,votingquestionId,loadparam) {

            votingData = {

              "questionUUID": votingquestionId,
              "votePagePath": currentpagepath,
              "disableVotingDate": votingDateExpiry

            };

if(loadparam == 'onsubmit'){
  votingQueryParams = {
     params: {
       ':operation': 'social:voting:submitVote'

     }
   };
}
else if(loadparam == 'onload'){
  votingQueryParams = {
     params: {
       ':operation': 'social:voting:getVoteData'
     }
   };
}
            queryURLParam = currentpagepath+'.social.json?'+$.param(votingQueryParams.params)+'&voteData='+JSON.stringify(votingData);
            votingFactory.getVotingResults(queryURLParam).then(function(response) {
              votingSuccessCallback(response,"onload");
              console.log('succes voint rsults' + response);
            }, function(response) {
              //handle error
            });

  }

  var queryURLParam = '';
  var currentpagepath=angular.element('.voting-poll-view').data('currentpath'),
  votingDateExpiry = angular.element('.voting-poll-view').data('disablevotingdate'),
    votingquestionId = angular.element('.voting-poll-view').data('questionid');
   votingDataProcessing(currentpagepath,votingDateExpiry,votingquestionId,"onload");

    $scope.removeErrors = function(formName){
    $scope.studentFormError = false;
         $scope.teacherFormInvalid =false;
         $scope.teacherDataFormError =false;
}

  $scope.validatePollData = function(formName) {
    if (formName == "studentDataForm") {

       var answerStudentOptionArray = angular.element('.student-view-answer').find('.label-text');
        console.log(answerStudentOptionArray.length);

        $scope.studentFormError = true;
        answerStudentOptionArray.each(function() {

          if (($(this).find('input').is(':checked'))) {
            $scope.studentFormError = false;
          }

        });




      if (!($scope.studentFormError)) {

        var answerStudentOptionArray = angular.element('.student-view-answer').find('.label-text');

        votingData.answerOptions = {};
        answerStudentOptionArray.each(function() {
          var answerOption = $(this).find('.option-text').text().trim();


          if ($(this).find('input').is(':checked')) {
            votingData.answerOptions[answerOption] = 1;
          } else {
            votingData.answerOptions[answerOption] = 0;
          }

        });



        votingQueryParams = {
           params: {
             ':operation': 'social:voting:submitVote'
           }
         };
          queryURLParam = currentpagepath+'.social.json?'+$.param(votingQueryParams.params)+'&voteData='+JSON.stringify(votingData);

        votingFactory.getVotingResults(queryURLParam).then(function(response) {
          votingSuccessCallback(response,"afterStudentValidation");
             $scope.showThanksMessage=true;
        }, function(response) {
          //handle error
        });
      }


    }else if (formName == "teacherDataForm") {
      var inputArr = angular.element('form[name="teacherDataForm"]').find('input[type="text"]');
      var totalSumTeacherVotes = 0;

       // console.log('$scope.teacherDataForm.$submitted && !(teacherDataForm.$valid)'+$scope.teacherDataForm.$submitted && !(teacherDataForm.$valid));

	 $scope.teacherFormInvalid=true;
      angular.forEach(inputArr, function(value, key) {
          var currentElement=angular.element(value).val();
          if(currentElement>=0 && currentElement!=''){
              $scope.teacherFormInvalid=false;

          }
          if(currentElement!='')
        totalSumTeacherVotes = totalSumTeacherVotes + parseInt(currentElement);


      });
        console.log('totalSumTeacherVotes'+totalSumTeacherVotes);
      if (totalSumTeacherVotes > 40 && !($scope.teacherFormInvalid)) {
        $scope.teacherDataFormError = true;

      }


      if (!($scope.teacherDataFormError) && !($scope.teacherFormInvalid)) {
        var answerTeacherOptionArray = angular.element('.teacher-view-answer').find('.label-text');

        votingData.answerOptions = {};
        answerTeacherOptionArray.each(function() {
          var answerOption = $(this).find('.option-text').text().trim();

            var teacherinputVal= $(this).find('input').val().trim();

            if(teacherinputVal== undefined || teacherinputVal=='')
				votingData.answerOptions[answerOption] = 0;
            else
            votingData.answerOptions[answerOption] = teacherinputVal;


        });
        votingQueryParams = {
           params: {
             ':operation': 'social:voting:submitVote'
           }
         };


  queryURLParam = currentpagepath+'.social.json?'+$.param(votingQueryParams.params)+'&voteData='+JSON.stringify(votingData);
        console.log("yeacherquery:::" + queryURLParam);


        votingFactory.getVotingResults(queryURLParam).then(function(response) {

          votingSuccessCallback(response,"afterTeacherValidation");
        }, function(response) {
          //handle error
        });


    }
  }
}
}]);
