'use strict';

app.factory('votingFactory', ["$http", "$q", function($http, $q) {

  var getVotingResults = function(queryURLParam){

    var deferred = $q.defer();
    var queryURL='';
    queryURL = queryURLParam;
    console.log('queryURL:::'+queryURL);
    $http({
          url: queryURL,
          method: 'POST'
    }).then(function(response){
          deferred.resolve(response);
    }, function(response){
          deferred.reject(response);
    });
    return deferred.promise;
}

return {
	  getVotingResults: getVotingResults
  }
}]);
