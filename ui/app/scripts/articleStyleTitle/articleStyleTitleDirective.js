app.directive('ssHeader', function() {
    return {
        restrict: 'E',
        scope: {
            title:"@",
            bgcolor:"@",
            hyperLinkText: '@',
            hyperLinkURL: '@'
        },
        templateUrl: '../../../scripts/articleStyleTitle/articleStyleTitle.html'
    }
});
