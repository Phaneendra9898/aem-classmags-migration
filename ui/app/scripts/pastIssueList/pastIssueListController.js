app.controller("pastIssueListController", ["$scope", "$http", "issueGridDataFactory", "$timeout", function($scope, $http, issueGridDataFactory, $timeout) {
    var perPage = 12;
    $scope.issueItems = [];
    $scope.pages = [];
    $scope.totalPageCount;
    var gridResults = [];
    $scope.records={};



    var populateGridResults = function(response) {
        gridResults = response.data.issueList.issue;
        $scope.totalRecords = gridResults.length;
        $scope.totalPageCount = Math.ceil($scope.totalRecords / perPage);
        for (var i = 1; i <= $scope.totalPageCount; i++) {
            $scope.pages.push(i);
        }
        $scope.records.pageNumber = $scope.pages[0];
        $scope.issueItems = gridResults;
        if ($scope.totalRecords > perPage) {
            $scope.paginate(1);
        }
    };

    var init = function() {
        $timeout(function(){
            //get the current page path and pass it as query param
            var documentResult = angular.element(".pastIssueGrid-main-container"); 
            var path = documentResult.attr('data-page-path');
            issueGridDataFactory.getGridData({
                sortBy: '',
                filterBy: '',
                pageNumber: $scope.records.pageNumber,
                perPage: perPage

            }, path).then(function(response) {
                populateGridResults(response);
            });
        });
    };

    $scope.paginate = function(pageNumber) {
        var showFrom = perPage * (pageNumber - 1);
        var showTo = showFrom + perPage;
        var results = angular.copy(gridResults);
        $scope.issueItems = results.slice(showFrom, showTo);
    };

    $scope.onpageViewAll = function() {
        $scope.issueItems = gridResults;
    }

    $scope.onpagePrevious = function() {
        if ($scope.records.pageNumber > 1) {
            $scope.records.pageNumber = $scope.records.pageNumber - 1;
            $scope.paginate($scope.records.pageNumber);
        }
    }
    $scope.onpageNext = function() {
        if ($scope.records.pageNumber < $scope.totalPageCount) {
            $scope.records.pageNumber = $scope.records.pageNumber + 1;
            $scope.paginate($scope.records.pageNumber);
        }
    }
    $scope.onPageSelection = function(pageNumber) {
		$scope.pageNumber = pageNumber; 
        $scope.paginate($scope.records.pageNumber);
    }

    init();

}]);