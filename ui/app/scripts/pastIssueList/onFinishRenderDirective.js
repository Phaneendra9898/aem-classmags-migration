app.directive('onFinishRender', ["$timeout", "$window", function ($timeout, $window) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {                 
                $window.onresize = function(){
                    $timeout(function () {
                        equaliseDotDotDot();
                    });
                }
                         
                if (scope.$last === true) {                    
                    $timeout(function () {
                        equaliseDotDotDot();
                    });
                }

                var equaliseDotDotDot = function(){
                    angular.element('.tile-desc').dotdotdot();  
                    var maxHeight = 0;                    
                    var curWidth=$(window).width();
                    if(curWidth <=991){
                        var rowarr = $('.pci-resources-container .pci-row-wrapper.hidden-md'); 
                    }
                    else{
                        var rowarr = $('.pci-resources-container .pci-row-wrapper.hidden-sm'); 
                    }
                         
                    angular.forEach(rowarr, function(currentObject) {
                        maxHeight  =  0;
                        angular.element(currentObject).find('.pci-col-wrapper').css('height', 'auto');
                        var colArr = angular.element(currentObject).find('.pci-col-wrapper');
                        angular.forEach(colArr, function(currentObject)  {
                            maxHeight  =  Math.max(maxHeight, angular.element(currentObject).outerHeight());
                        });

                        angular.element(currentObject).find('.pci-col-wrapper').css('height', maxHeight);

                        var tileWrapperPastDateClosObj = angular.element(currentObject).closest('.tile-wrapper-past-date');
                        
                        var rightSectionHeight = tileWrapperPastDateClosObj.outerHeight(true);

                        var leftSectionHeight = tileWrapperPastDateClosObj.siblings().find('.past-issue-wrapper').outerHeight(true);

                        var maxHeightLeftRight = Math.max(leftSectionHeight, rightSectionHeight);

                        var rowWrapMarTop = angular.element(currentObject).outerHeight(true) - angular.element(currentObject).outerHeight();
                        if(rightSectionHeight > leftSectionHeight) { 
                            tileWrapperPastDateClosObj.siblings().find('.past-issue-wrapper').css('height', maxHeightLeftRight);
                        }
                        if(rightSectionHeight < leftSectionHeight) {
                            angular.element(currentObject).find('.pci-col-wrapper').css('height', maxHeightLeftRight - rowWrapMarTop - angular.element(currentObject).siblings('.past-issue-date-container').outerHeight(true));
                        }

                    });  

                };
        }
    }
}]);