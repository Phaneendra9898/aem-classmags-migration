app.controller("issueGridController", ["$scope", "$http", "issueGridDataFactory", function($scope, $http, issueGridDataFactory) {
    var perPage = 12;
    $scope.issueItems = [];
    $scope.pages = [];
    $scope.totalPageCount;
    var gridResults = [];


    var populateGridResults = function(response) {
        gridResults = response.data.issueList.issue;
        $scope.totalRecords = gridResults.length;
        $scope.totalPageCount = Math.ceil($scope.totalRecords / perPage);
        for (var i = 1; i <= $scope.totalPageCount; i++) {
            $scope.pages.push(i);
        }
        $scope.pageNumber = $scope.pages[0];
        if ($scope.totalRecords > perPage) {
            $scope.paginate(1);
        } else {
            $scope.issueItems = gridResults;
        }
    }

    var init = function() {
        issueGridDataFactory.getGridData({
            sortBy: '',
            filterBy: '',
            pageNumber: $scope.pageNumber,
            perPage: perPage

        }).then(function(response) {
            populateGridResults(response);
        });
    };

    $scope.paginate = function(pageNumber) {
        var showFrom = perPage * (pageNumber - 1);
        var showTo = showFrom + perPage;
        var results = angular.copy(gridResults);
        $scope.issueItems = results.slice(showFrom, showTo);
    };

    $scope.onpageViewAll = function() {
        $scope.issueItems = gridResults;
    }

    $scope.onpagePrevious = function() {
        if ($scope.pageNumber > 1) {
            $scope.pageNumber = $scope.pageNumber - 1;
            $scope.paginate($scope.pageNumber);
        }
    }
    $scope.onpageNext = function() {
        if ($scope.pageNumber < $scope.totalPageCount) {
            $scope.pageNumber = $scope.pageNumber + 1;
            $scope.paginate($scope.pageNumber);
        }
    }
    $scope.onPageSelection = function() {
        $scope.paginate($scope.pageNumber);
    }

    init();

}]);