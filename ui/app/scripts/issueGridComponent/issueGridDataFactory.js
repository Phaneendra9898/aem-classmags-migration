app.factory('issueGridDataFactory', ["$http", "$q", function($http, $q) {
  var getGridData = function(params){
	  var deferred = $q.defer();
	  $http({
		  url:"sample.json",
		  method:'GET'
	  }).then(function(response){
		  var results = response;		  
		  deferred.resolve(response);
	  }, function(response){
		  deffered.reject(response);
	  });
	  return deferred.promise;
  }
  return {
	  getGridData: getGridData
  }
 
}]);