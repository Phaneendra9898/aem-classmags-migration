$(window).on('load',function() {
	var iframeBody = $("#ContentFrame").contents().find("body");
	var mainTab = iframeBody.find(".main-tabs li");
	var tabContent = iframeBody.find(".main-tab-content .tab-pane");

	$(mainTab).click(function(event) {
		var ele = $(this).data('id');
		document.cookie = "tabcookie=" + ele;

	});
	var getCookie = function(name) {
		var value = "; " + document.cookie;
		var parts = value.split("; " + name + "=");
		if (parts.length == 2)
			return parts.pop().split(";").shift();
	}
	var tab_cookie = getCookie("tabcookie");
	
	if (tab_cookie) {
		var tabsArr = $(mainTab);
		tabsArr.each(function() {
			var tabId = $(this).data('id');
			if (tab_cookie == tabId) {
				mainTab.removeClass('active');
				tabContent.removeClass('active in');
				iframeBody.find(".main-tabs li[data-id=" + tabId + "]")
						.addClass("active");
				iframeBody.find(".main-tab-content .tab-pane[id="+ tabId + "]").addClass("active in");
			}
		});
	}
});
