app.directive('swTextHyperlink', function() {
    return {
        restrict: 'E',
        scope: {
            title:'@',
            hyperLinkText: '@',
            hyperLinkUrl: '@',
            recordsCount: '@',
            bgcolor: '@'
        },
        templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/sciencespin2/clientlibs/sciencespin2-core/scripts/textHyperlink/swTextHyperlinkPartial.html'
    }
});
