(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name scholasticKids.directive:gameContainer
     * @description
     * # gameContainer
     * Directive of the scholasticKids
     */
    function gameFlashContainer() {
        return {
            restrict: 'E',
            controller: gameFlashContainerController,
            scope: {
                containerTitle: '@?',
                containerImage: '@?',
                containerId: '@?',
                containerPath: '@?',
                containerGameTitle: '@?',
                containerGameDesc: '@?',
                containerGameFlashData: '@?',
                swGameAuthorize:'@'
            },
            templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/clientlibs/classmags-migration-core/templates/game-flash-container.html'
        };
    }

    gameFlashContainerController.$inject = ['$scope', '$timeout', 'authorizationService', 'swUserRole'];
    function gameFlashContainerController($scope, $timeout, authorizationService, swUserRole) {


        /**
         * function that inits the directive and
         * @return {void}
         * asigns the values
         */
        $scope.init = function() {
            $scope.containerTitle = $scope.containerTitle || 'Play Now';
            $scope.containerModal = true;
            if (navigator.userAgent.match(/iPad/i) !== null) {
                $scope.unSupportedError = true;
            }
        };
        //execte init task
        $scope.init();

        /**
         * function that sets up the modal config and events
         * @return {void}
         */
        $scope.openFlashModal = function() {
            if(!authorizationService.verifyAccess($scope.swGameAuthorize, swUserRole)){       
                return;
            }            
            var $modalContent = $('#' + $scope.containerId).html();
            $('#game-container-modal-body').hide();
            $('#html5-game').show();
            $('#game-container-modal-title').text();
            $('#game-container-modal-body').html($modalContent);
            $('#game-container-modal').modal();
            $('#game-container-modal .close').show();
            $('#game-container-modal').on('shown.bs.modal', _onShowFlashModal).on('hidden.bs.modal', _onHideFlashModal);        
        };

        /**
         * bootstrap modal onShow callback function
         * @param  {object} e bootstrap modal event
         * @return {void}
         */
        function _onShowFlashModal(e) {
            $('.main-container').css('table-layout', 'auto');
            var $this = $(e.currentTarget).css('display', 'block'),
                $window = $(window),
                $dialog = $this.find('.game-container-modal'),
                offset = ($window.height() - $window.scrollTop() - $dialog.height()) / 2,
                marginBottom = parseInt($dialog.css('margin-bottom'), 10),
                marginTop = offset < marginBottom ? marginBottom : offset;
            $dialog.append('<div class="spinnercontainer" id="html5-game"><img src="/etc/designs/scholastic/classroom-magazines-migration/images/icons/spinner.gif"></div>');
            $('#html5-game').show();
            $dialog.css('height', '100%');
            $dialog.find('.modal-content').css('height', '100%');
            $('#game-container-modal-body').height('100%');

            $timeout(function() {
                $('#game-container-modal-body').css('height', '100%');
                $('#html5-game').hide();
                $('#game-container-modal-body').show();
            }, 1500);
        }

        /**
         * bootstrap modall onHide callback function
         * @param  {object} e bootstrap modal event
         * @return {void}
         */
        function _onHideFlashModal() {
            $('.main-container').css('table-layout', 'fixed');
            $('#game-container-modal-body').html('<div></div>');
        }
    }

    angular.module('scienceWorld').directive('gameFlashContainer', gameFlashContainer);
}());
