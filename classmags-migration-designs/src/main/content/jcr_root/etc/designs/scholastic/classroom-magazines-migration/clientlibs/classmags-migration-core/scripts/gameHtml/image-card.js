(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name scholasticKids.directive:imageCard
   * @description
   * # imageCard
   * Directive of the scholasticKids
   */
  function imageCard() {
    return {
      restrict: 'E',
      controller: imageCardController,
      scope: {
        icon: '@',
        cardLink: '=',
        cardTitle: '=',
        cardImgPath: '=?',
        cardImgTitle: '=?',
        cardImgAlt: '=?',
        cardImgSilo: '=?',
        cardImgEmpty: '@?',
        cardHideExitBumper: '=?',
        cardType: '=?'
      },
      templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/clientlibs/classmags-migration-core/templates/image-card.html',
      replace: true
    };
  }

  imageCardController.$inject = ['$scope'];

  function imageCardController($scope) {
    if ($scope.cardType === 'externalUrl' || $scope.cardType === 'pdf') {
      $scope.target = '_blank';
    } else {
      $scope.target = '_self';
    }
  }

  angular.module('scienceWorld')
    .directive('imageCard',imageCard);
}());
