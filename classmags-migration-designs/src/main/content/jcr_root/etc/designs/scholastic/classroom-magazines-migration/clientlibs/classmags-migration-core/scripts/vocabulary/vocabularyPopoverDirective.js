app.directive('swVocabularyPopover', ["$timeout", function($timeout) {
    return {
        restrict: 'E',
        scope: {
            word:"=",
            onopen: '&',
            onclose: '&'
        },
        controller:["$scope","$sce", function($scope, $sce) {
        	 var vocabContent='';
            angular.element('body').on('click','a.js-sw-popover-close', function(){
                $scope.$apply(function(){
                    $scope.word.show=false;
                });
            });
            $scope.showPopup = function() {
                    $scope.onopen($scope.word);
            }
			var audioHtml="";
			if($scope.word.audiopath){
			audioHtml='<a onclick="this.firstChild.play()" ><audio src="' + $scope.word.audiopath + '"></audio><span class="sw-icon sound-icon"></span></a>';
			}
            
            if($scope.word.imagepath && $scope.word.photocredit){
            	vocabContent= '<div class="vocabulary-wrapper"><div class="image-wrapper"><img class="img-responsive vocabulary-image" src="'+ $scope.word.imagepath + '"><div class="photo-credit">'+$scope.word.photocredit+'</div></div><div class="text-wrapper"><div class="close-mark"><a class="js-sw-popover-close" href="javascript:void(0);"><span class="sw-icon grey-close-btn "></span></a></div><div class="popover-name-wrapper"><span class="popover-name">' + $scope.word.name + '</span>'+audioHtml+'</div><div><div class="popover-pronoutext">' + $scope.word.pronunciationtext + '</div></div><div class="popover-definition"><p>' + $scope.word.definition + '</p></div></div></div>';
            }else if($scope.word.imagepath && !$scope.word.photocredit){
            	vocabContent= '<div class="vocabulary-wrapper"><div class="image-wrapper"><img class="img-responsive vocabulary-image" src="'+ $scope.word.imagepath + '"></div><div class="text-wrapper"><div class="close-mark"><a class="js-sw-popover-close" href="javascript:void(0);"><span class="sw-icon grey-close-btn "></span></a></div><div class="popover-name-wrapper"><span class="popover-name">' + $scope.word.name + '</span>'+audioHtml+'</div><div><div class="popover-pronoutext">' + $scope.word.pronunciationtext + '</div></div><div class="popover-definition"><p>' + $scope.word.definition + '</p></div></div></div>';
            }
            else{
            	 vocabContent='<div class="vocabulary-wrapper"><div class="text-wrapper"><div class="close-mark"><a class="js-sw-popover-close" href="javascript:void(0);"><span class="sw-icon grey-close-btn "></span></a></div><div class="popover-name-wrapper"><span class="popover-name">' + $scope.word.name + '</span>'+audioHtml+'</div><div><div class="popover-pronoutext">' + $scope.word.pronunciationtext + '</div></div><div class="popover-definition"><p>' + $scope.word.definition + '</p></div></div></div>';
            }
               
            $scope.htmlPopover = $sce.trustAsHtml(vocabContent);
            
        }],
        template: '<a class="vocabulary-word" href="javascript:void(0);" popover-trigger="none" popover-placement="bottom" popover-is-open="word.show"' 
                    +'ng-click="showPopup()" uib-popover-html="htmlPopover" >{{word.name}}</a>'
    };
}]);
