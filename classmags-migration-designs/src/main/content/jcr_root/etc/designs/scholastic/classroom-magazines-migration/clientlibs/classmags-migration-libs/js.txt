#base=jquery/dist
jquery-3.1.1.min.js

#base=angular
angular.js

#base=angular-animate
angular-animate.min.js

#base=angular-sanitize
angular-sanitize.min.js

#base=angular-bootstrap
ui-bootstrap-tpls.js

#base=bootstrap/dist/js
bootstrap.min.js

#base=handlebars
handlebars.min.js

#base=angular-ellipsis/src
angular-ellipsis.js

#base=angular-touch
angular-touch.min.js