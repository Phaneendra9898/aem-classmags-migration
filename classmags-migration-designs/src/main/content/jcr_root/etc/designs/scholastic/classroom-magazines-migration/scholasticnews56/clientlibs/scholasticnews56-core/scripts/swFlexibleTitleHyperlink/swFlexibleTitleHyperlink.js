app.directive('swFlexibleTitleHyperlink', function() {
    return {
        restrict: 'E',
        scope: {
            title:'@',
            hyperLinkText: '@',
            hyperLinkUrl: '@',
            recordsCount: '@',
            color: '@',
            titlePosition: '@',
            isRuleExists: '=',
            titleFontSize: '@',
            imageUrl: '@',
            customBackground:'@'
        },
        controller:["$scope","$sce", function($scope, $sce){
		  $scope.title = $sce.trustAsHtml($scope.title);
        }],
        templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/scholasticnews56/clientlibs/scholasticnews56-core/scripts/swFlexibleTitleHyperlink/swFlexibleTitleHyperlinkPartial.html'
    };
});
