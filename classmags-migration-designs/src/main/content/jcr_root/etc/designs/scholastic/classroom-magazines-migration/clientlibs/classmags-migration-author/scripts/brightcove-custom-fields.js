(function () {
    $(document).on("dialog-ready", function() {
        var EMBEDDED="embedded";
        var $element = $('.brightcove-custom-form-fields');
        var selectedTab="";

        var hideOrShowBrightCoveFields = function(show){
            if(show){
                $element.find(".coral-Form-fieldset").show();
            }
            else{
                $element.find(".coral-Form-fieldset").hide();
            }
        }

        var ShowBrightCoveFields = function(){
            selectedTab = $($element.find(':checked')).val();
            if(selectedTab === EMBEDDED){
               hideOrShowBrightCoveFields(false);
            }
            else{
				hideOrShowBrightCoveFields(true);
            }
        }

        ShowBrightCoveFields();

        $element.click(function(){
			ShowBrightCoveFields();
        });

    });
})();