'use strict';

app.factory("contactUsFactory", ["$http", "$q" , function($http, $q){
	
	var contactObj = {};

	contactObj.contactRadiochange = function(pagePath, radiodata){
		var deferred = $q.defer();
		$http({            
            url: pagePath,
            method: 'GET',
            params: {"topic": radiodata}
			}).success(function(response){				
				deferred.resolve(response);
			}).error(function(response){
				deferred.reject(response);
			});
			return deferred.promise;
	};

	contactObj.contactFormPost = function(pagePath, contactData){
		var deferred = $q.defer();
		var data = JSON.stringify(contactData) ;
		$http({            
            url: pagePath,
            method: 'POST',
            params: {"data": data}
			}).success(function(response, status){
            if(status == 200){
				deferred.resolve(true);
            }
            else{
                deffered.resolve(false);
            }

			}).error(function(response){
				deferred.reject(response);
			});
			return deferred.promise;	
	};

	return contactObj;
}]);