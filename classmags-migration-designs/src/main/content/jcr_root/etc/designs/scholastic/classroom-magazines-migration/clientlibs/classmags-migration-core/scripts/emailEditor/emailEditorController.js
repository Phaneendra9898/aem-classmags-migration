'use strict';

app.controller("emailEditorController", ["$scope", "emailEditorFactory", function($scope, emailEditorFactory) {
	$scope.emailData = {};
	$scope.submitEmailForm = function(){
        
        if($scope.emailEditorForm.$valid){
            if($scope.emailData && !$scope.emailData.subject){
                $scope.emailData.subject = '';
            }
            emailEditorFactory.emailEditorPost($scope.emailData, $scope.currentPagePath)
                .then(emailHandleSuccess, emailHandleError);
        }
	}

	function emailHandleSuccess(response) {
        location.href= $scope.pageDirectPath;
    }

    function emailHandleError(response) {
        angular.element('.correct-error-text-container').show();        
    }
}]);