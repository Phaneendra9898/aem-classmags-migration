app.controller("articleAggregatorController", ["$scope", "$timeout", "$element", "swUserRole", "bookmarkPaths", "articleToolbarFactory", function($scope, $timeout, $element, swUserRole, bookmarkPaths, articleToolbarFactory) {
    $timeout(function() {
        if (angular.element('.iarticle-aggregator').length > 0)
            seeAllOnLoad();
    });

    function seeAllOnLoad() {
        var articleArr = angular.element('.iarticle-aggregator .resources-container .row-wrapper'),
            articleArrCount = articleArr.length;
        angular.forEach(articleArr, function(val, key) {
            var colArr = angular.element(val).find('.col-md-3'),
                colArrCount = colArr.length;
            if (colArrCount > 4) {
                for (var i = 4; i <= colArrCount; i++) {
                    angular.element(colArr[i]).addClass('remove-article').hide();
                }
                $scope.isShow = true;
                angular.element(val).find('.see-all [data-state="expand"]').addClass('showarticles');
            } else {
                angular.element(val).find('.see-all [data-state="expand"]').removeClass('showarticles').hide();
                angular.element(val).find('.see-all [data-state="collapse"]').hide();
            }
        });
    }


    $scope.updateTiles = function(param, $event) {
        if (param === 'expand') {
            $scope.isShow = false;
            $element.find('.col-md-3').show();
        } else {
            $scope.isShow = true;
            $element.find('.col-md-3.remove-article').hide();
        }
    }

    angular.element('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
        if (angular.element('.iarticle-aggregator').length > 0) {
            seeAllOnLoad();
            angular.element(window).trigger('resize');
        }
    });

    var userRole,
        bookmarkResourcePath = $('#bookmarkSocialPostPath').val(),
        appName = $("#core-main-container").attr("data-app-name").toLowerCase();
    $timeout(function() {
		$scope.isBookmarkAdded = returnBookmarkStatus();
    });


    if (swUserRole) {
        userRole = swUserRole.toLowerCase();
    }

    $scope.addBookmark = function() {
        if (userRole === "teacher" || userRole === "student") {
            $scope.isSpinnerShow = true;
            articleToolbarFactory.toggleBookmarkByContentType('addbookmark',
                bookmarkResourcePath,
                appName,
                'article',
                $scope.pagePath).then(handleSuccess, handleError);
        }
    };

    $scope.deleteBookmark = function() {
        $scope.isSpinnerShow = true;
        articleToolbarFactory.toggleBookmarkByContentType('deletebookmark',
            bookmarkResourcePath,
            appName,
            'article',
            $scope.pagePath).then(handleSuccess, handleError);
    };

    function handleSuccess(response) {
        var data = response.data;
        if (data && data.response) {
            $scope.isBookmarkAdded = data.response.bookmarkEnabled;
        }
        $scope.isSpinnerShow = false;
    }

    function handleError(response) {
        //handle error
        $scope.isSpinnerShow = false;
    }

    function returnBookmarkStatus() {
        return bookmarkPaths && bookmarkPaths.pagePaths && bookmarkPaths.pagePaths.indexOf($scope.pagePath) !== -1;
    }

}]);
