app.directive('swTextHyperlink', function() {
    return {
        restrict: 'E',
        scope: {
            title: '@',
            hyperLinkText: '@',
            hyperLinkUrl: '@',
            bgcolor: "@",
            recordsCount: '@',
            isBorderEnable: '='
        },
        templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/clientlibs/classmags-migration-core/scripts/textHyperlink/swTextHyperlinkPartial.html'
    }
});

app.decorator('swTextHyperlinkDirective', ["$delegate", function($delegate) {
    var appName = $("#core-main-container").attr("data-app-name");
    if (appName && (appName.toLowerCase() === "scholasticnews3" || appName.toLowerCase() === "scholasticnews4" || appName.toLowerCase() === "scholasticnews56" || appName.toLowerCase() === "sn3" || appName.toLowerCase() === "sn4" ||
            appName.toLowerCase() === "sn56" || appName.toLowerCase() === "geographyspin" ||
            appName.toLowerCase() === "sciencespin36" || appName.toLowerCase() === "dynamath")) {
        return ([$delegate[1]]);
    }
    return ([$delegate[0]]);
}]);
