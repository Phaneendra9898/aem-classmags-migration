var app;
(function() {
    app = angular.module("scienceWorld", ['ngAnimate', 'ngSanitize', 'ngTouch', 'ui.bootstrap', 'dibari.angular-ellipsis']);
    fetchData();

    function fetchData() {
        var initInjector = angular.injector(["ng"]);
        var $http = initInjector.get("$http");
        return $http.get("/bin/classmags/getuserrole?" + Date.now(), {
            cache: false
        }).then(function(response) {
            if (response && response.data && response.data.role) {
                app.constant("swUserRole", response.data.role);

                if(response.data.entitlements){
                    app.constant("swUserEntitlements", response.data.entitlements);
                }
                else{
                    app.constant("swUserEntitlements", []);
                }

                if (response.data.role != "anonymous" && $("#sw-sdm-home-page-logged-out").val() != "") {
                    if (location.pathname === $("#sw-sdm-home-page-logged-out").val() ||
                        location.pathname === "/" || 
                        ($("#sw-sdm-home-page-logged-out").val() &&
                        $("#sw-sdm-home-page-logged-out").val().indexOf(location.pathname)!=-1)
                        || (location.pathname && location.pathname.indexOf("home-page-logged-out")!=-1)
                        ) {
                        if ($("#sw-sdm-home-page-logged-in").val() && $("#sw-sdm-home-page-logged-in").val().indexOf(".html") != -1) {
                            location.href = $("#sw-sdm-home-page-logged-in").val();
                        } else {
                            location.href = $("#sw-sdm-home-page-logged-in").val() + ".html";
                        }
                    }
                }
                
                dumbleData.user=response.data.user;

            } else {
                app.constant("swUserRole", false);
            }
            var bookmarkComponentPath = $('#bookmarkSocialPostPath').val();
            if (bookmarkComponentPath) {
                var data = {
                        params: {
                            ':operation': 'social:bookmark:getAllSite',
                            'bookmarkPagePath': decodeURIComponent(location.pathname),
                            'appName': $("#core-main-container").attr("data-app-name").toLowerCase()
                        }
                    },
                    path = bookmarkComponentPath + '.social.json?' + $.param(data.params);
                $http.post(path, data.params)
                    .then(function(response) {
                        app.constant("bookmarkPaths", response.data.response);
                        bootstrapApplication();
                    }, function(response) {
                        console.log(response);
                        app.constant("bookmarkPaths", []);
                        bootstrapApplication();
                    });
            } else {
                bootstrapApplication();
                app.constant("bookmarkPaths", []);
            }
        }, function(errorResponse) {
            app.constant("swUserRole", false);
        });
    }

    app.controller("scienceWorldController", ["$rootScope", "$scope", "$timeout", "$compile", "swUserRole", function($rootScope, $scope, $timeout, $compile, swUserRole) {
        $timeout(function() {
            $(function() {
                $('[data-ajax-component]').each(function() {
                    var ajaxComponent = "";

                    var $this = $(this),
                        url = $this.data('url'),
                        queryParams = $this.data('ajax-query-parameters');
                    url += "?t=" + (new Date()).getTime();

                    if (queryParams) {
                        url += "&" + queryParams;
                    }

                    if (url.indexOf("objectives_and_skill") !== -1) {
                        ajaxComponent = "OBJECTIVE_AND_SKILLS";
                    } else if (url.indexOf("article_resources") !== -1) {
                        ajaxComponent = "ARTICLE_RESOURCES";
                    } else if (url.indexOf("content_hub") !== -1) {
                        ajaxComponent = "CONTENT_HUB";
                    }

                    $.get(url, function(data) {
                        if (!data.match(/\sdata-ajax-component/)) {
                            $this.replaceWith($compile(data)($scope));
                            if (ajaxComponent) {
                                $rootScope.$emit(ajaxComponent, 'AJAX_COMPONENT_LOADED');
                            }
                        }
                    });

                });
            });
        });
        
        //finding the type of user logged in
        if (swUserRole && (swUserRole.toLowerCase() == "teacher")) {
            $scope.isTeacher = true;
        } else if (swUserRole && (swUserRole.toLowerCase() == "student")) {
            $scope.isStudent = true;
        }
        
    }]);

    function bootstrapApplication() {
        angular.element(document).ready(function() {
            angular.bootstrap(document, ["scienceWorld"]);
        });
    }
}());
