app.directive('swArticleLexilLevel', function($rootScope,$timeout) {
  return {
    restrict: 'A',
    link: function(scope, elem, attrs) {
      var lexilLevelChange = function() {
        var $artcileDataSectionElement = angular.element(".js-article-data-section");
        $artcileDataSectionElement.each(function() {
          $(this).find(".js-article-item").each(function() {
            var $articleItem = angular.element(this);
            if ($articleItem.attr('id') == $(elem).val() - 1) {
              $articleItem.show();
            } else {
              $articleItem.hide();
            }
          });
        });

        var $artcileDebateSectionElement = angular.element('.js-article-sidebar-wrapper');
          
          if($artcileDebateSectionElement.length>0){
            $artcileDebateSectionElement.each(function() {
                var currentOptionSelected = $(elem).find("option:selected").data('toolbar');
                if ($(this).data('sidebar') == currentOptionSelected) {
                    $(this).show();

                } else {
                    $(this).hide();
                }
            }); 
          }
        

      }
      
      $(elem).change(function() {
        lexilLevelChange();
          $timeout(function(){
           $rootScope.$emit('LEXILE_LEVEL', 'TOOLBAR_LEXILE_LEVEL_CHANGED');
          });
      });
      lexilLevelChange();
    }
  }
});
