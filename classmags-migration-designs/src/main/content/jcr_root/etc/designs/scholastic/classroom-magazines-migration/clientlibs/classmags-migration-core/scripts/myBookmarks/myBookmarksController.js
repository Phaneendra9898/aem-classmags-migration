'use strict';

app.controller('myBookmarksController', [
    '$scope',
    'myBookmarksFactory',
    '$timeout',
    function($scope, myBookmarksFactory, $timeout) {
        initialize();

        /**
     * Function to initialize the scope properties and functions.
     */
        function initialize() {
            $scope.currentTab = 'articlesTab';
            $scope.isSpinnerShow = true;
            $scope.magazineList = [];
            initializeFilterVariables();
            $scope.issueTabData = [];
            $scope.articleTabData = [];
            $scope.itemsPerPage = 12;
            $scope.pagedAlerts = [];
            $scope.currentPage = 1;
            $scope.page = {
                'currentPage': 1
            };
            $scope.goToPrevPage = goToPrevPage;
            $scope.goToNextPage = goToNextPage;
            $scope.setPage = setPage;
            $scope.openTab = openTab;
            $scope.sortBookmarks = sortBookmarks;
            $scope.filterBookmarks = filterBookmarks;
            $scope.resetFilters = resetFilters;
            $scope.getBookmarkPaths = getBookmarkPaths;
            sortBookmarks('bookmarkDateSaved', true);
            $timeout(function() {
                var appName = $("#core-main-container").attr("data-app-name").toLowerCase();
                myBookmarksFactory.getBookmarks(appName).then(handleSuccess, handleError);
                
            });
        }

        /**
     * Function to handle the Get bookmarks success response
     * @param  {Object} response [Response from backend]
     */
        function handleSuccess(response) {
            var data = response.data,
                response = data.response,
                articleData = [],
                issuesData = [];

            if (data && response) {
                var pageData = response.myBookmarksPageData || [],
                    assetData = response.myBookmarksAssetData || [];
                $scope.bookmarksCount = pageData.length + assetData.length;
                $scope.bookmarkPaths = [];

                angular.forEach(pageData, function(eachObj) {
                    if (eachObj.contentType && (eachObj.contentType.toLowerCase() === 'issues' || eachObj.contentType.toLowerCase() === 'issue')) {
                        issuesData.push(eachObj);
                    } else {
                        articleData.push(eachObj);
                    }
                    if($scope.bookmarkPaths.indexOf(eachObj.bookmarkPath) === -1) {
                      $scope.bookmarkPaths.push(eachObj.bookmarkPath);
                    }
                });

                angular.forEach(assetData, function(eachObj) {
                    if($scope.bookmarkPaths.indexOf(eachObj.bookmarkPath) === -1) {
                      $scope.bookmarkPaths.push(eachObj.bookmarkPath);
                    }
                });
                $scope.articleTabData = articleData.concat(assetData);
                $scope.issueTabData = issuesData;
                $scope.magazineList = response.magazinesNameMap;
                openTab('articlesTab', 'articleTabData');
            }
            $scope.isSpinnerShow = false;
        }

        function getBookmarkPaths(pathsData) {
            if (pathsData) {
                if (pathsData.operation === 'add') {
                    $scope.bookmarkPaths.push(pathsData.path);
                } else if (pathsData.operation === 'delete') {
                    $scope.bookmarkPaths.splice($scope.bookmarkPaths.indexOf(pathsData.path), 1);
                }
            }
            return $scope.bookmarkPaths;
        }

        /**
     * Function to handle the Get bookmarks failure response
     * @param  {Object} response [Response from backend]
     */
        function handleError(response) {
            //handle error
            $scope.isSpinnerShow = false;
        }

        /**
       * Function to initialize filter related variables
       */
        function initializeFilterVariables() {
            $scope.subjectsList = [];
            $scope.contentTypeList = [];
            $scope.magazinesData = [];
            $scope.contentTypesData = [];
            $scope.subjectsData = [];
        }

        /**
     * Function to toggle between the tabs
     * @param  {Sring} tabName [Tab name to get selected]
     */
        function openTab(tabName, dataProperty) {
            $scope.currentTab = tabName;
            $scope.bookmarksData = $scope[dataProperty];
            initializeFilterVariables();
            if ($scope.bookmarksData.length > 0) {
                Object.keys($scope.magazineList).forEach(function(eachMagazine) {
                    var filteredObject = $scope.bookmarksData.filter(function(eachArticleObject) {
                        eachArticleObject['publishedDate'] = new Date(eachArticleObject.sortingDate).getTime();
                        
                        eachArticleObject.bookmarkDateSaved = eachArticleObject.bookmarkDateSaved;
                        
                        if ($scope.contentTypeList.indexOf(eachArticleObject.contentType) === -1) {
                            $scope.contentTypeList.push(eachArticleObject.contentType);
                        }
                        return eachArticleObject.magazineType === eachMagazine;
                    });
                    if(filteredObject.length>0){
                        $scope.magazinesData.push({'name': $scope.magazineList[eachMagazine], 'id': eachMagazine, 'count': filteredObject.length});
                    }
                });

                $scope.contentTypeList.forEach(function(eachType) {
                    var filteredData = $scope.bookmarksData.filter(function(eachObject) {
                        return eachObject.contentType === eachType;
                    });

                    $scope.contentTypesData.push({'contentType': eachType, 'count': filteredData.length});
                });
            }
            $scope.isMagazineSelected = false;

            if($scope.magazinesData.length === 1) {
                $scope.isMagazineSelected = false;
                $scope.magazinesData[0].isSelected = true;
                filterBookmarks();
            }

            if($scope.contentTypesData.length === 1){
                $scope.contentTypesData[0].isSelected = true;
            }

            $scope.articleList = angular.copy($scope.bookmarksData);
            groupToPages();
        }

        /**
     * Functio to sort bookmarks based on sort condition passed
     * @param  {String} sortCondition [description]
     */
        function sortBookmarks(sortCondition, sortDirection) {
            $scope.sortCondition = sortCondition;
            $scope.sortDirection = sortDirection;
        }

        /**
     * Function to filter the bookmarks based on the filter type selected
     */
        function filterBookmarks(type) {
            var filteredArray = [],
                isMagazineFilterSelected = false,
                isSubjectFilterSelected = false,
                isTypeFilterSelected = false,
                magazineFilterResults = [],
                subjectFilterResults = [],
                typeFilterResults = [],
                resultsToFilter = [],
                filterResults = [];
            if (!type) {
                $scope.subjectsList = [];
                $scope.subjectsData = [];
            }

            $scope.magazinesData.forEach(function(eachMagazine) {
                if (eachMagazine.isSelected) {
                    filteredArray = $scope.bookmarksData.filter(function(eachBookmark) {
                        return eachMagazine.id === eachBookmark.magazineType;
                    });
                    magazineFilterResults = magazineFilterResults.concat(filteredArray);
                    isMagazineFilterSelected = true;
                    if (!type) {
                        angular.forEach(filteredArray, function(eachBookmark) {
                            if (eachBookmark.tags) {
                                eachBookmark.tags.forEach(function(eachTag) {
                                    if ($scope.subjectsList.indexOf(eachTag) === -1) {
                                        $scope.subjectsList.push(eachTag);
                                    }
                                });
                            }
                        });
                        $scope.subjectsList.forEach(function(eachSubject) {
                            var subjectId = eachSubject.replace(/\s/g, '').toLowerCase(),
                                filteredObject = filteredArray.filter(function(eachObject) {
                                    return eachObject.tags && eachObject.tags.indexOf(eachSubject) !== -1;
                                });
                            if (filteredObject.length > 0) {
                                var resultArray = $scope.subjectsData.filter(function(obj){
                                    return obj.name === eachSubject;
                                });
                                if(resultArray.length > 0) {
                                    resultArray[0].count = resultArray[0].count + filteredObject.length;
                                } else {
                                    $scope.subjectsData.push({'name': eachSubject, 'id': subjectId, 'count': filteredObject.length});
                                }

                            }
                        });
                    }
                }
            });

            if (isMagazineFilterSelected) {
                $scope.isMagazineSelected = true;
                resultsToFilter = isMagazineFilterSelected
                    ? magazineFilterResults
                    : $scope.bookmarksData;

                $scope.subjectsData.forEach(function(eachSubject) {
                    if (eachSubject.isSelected) {
                        filteredArray = resultsToFilter.filter(function(eachBookmark) {
                            return eachBookmark.tags && eachBookmark.tags.indexOf(eachSubject.name) !== -1;
                        });
                        subjectFilterResults = subjectFilterResults.concat(filteredArray);
                        isSubjectFilterSelected = true;
                    }
                });
                if(subjectFilterResults.length > 0) {
                    var deepFilterResults = [];
                        angular.forEach(subjectFilterResults, function(baseItem) {
                        if(deepFilterResults.length === 0) {
                            deepFilterResults.push(baseItem);
                        } else {                            
                            if(deepFilterResults.indexOf(baseItem) === -1) {
                                deepFilterResults.push(baseItem);
                            }
                        }   
                        
                            
                        });
                    subjectFilterResults = deepFilterResults;
                }
            } else {
                $scope.subjectsData.forEach(function(eachSubject) {
                    eachSubject.isSelected = false;
                });
                $scope.isMagazineSelected = false;
            }

            if (isMagazineFilterSelected && isSubjectFilterSelected) {
                filterResults = subjectFilterResults;
            } else if (isMagazineFilterSelected) {
                filterResults = magazineFilterResults;
            } else if (isSubjectFilterSelected) {
                filterResults = subjectFilterResults;
            } else {
                filterResults = $scope.bookmarksData;
            }

            $scope.contentTypesData.forEach(function(eachType) {
                if (eachType.isSelected) {
                    filteredArray = filterResults.filter(function(eachBookmark) {
                        return eachType.contentType === eachBookmark.contentType;
                    });
                    typeFilterResults = typeFilterResults.concat(filteredArray);
                    isTypeFilterSelected = true;
                }
            });

            if (isTypeFilterSelected) {
                filterResults = typeFilterResults;
            }

            $scope.articleList = angular.copy(filterResults);
            $scope.currentPage = 1;
            $scope.page = {
                'currentPage': 1
            };
            groupToPages();
        }

        /**
     * Function to reset all filters and refresh the view
     */
        function resetFilters() {
            $scope.subjectsData.forEach(function(eachSubject) {
                eachSubject.isSelected = false;
            });

            $scope.magazinesData.forEach(function(eachMagazine) {
                eachMagazine.isSelected = false;
            });

            $scope.contentTypesData.forEach(function(eachContentType) {
                eachContentType.isSelected = false;
            });

            $scope.isMagazineSelected = false;
            $scope.articleList = angular.copy($scope.bookmarksData);
            $scope.currentPage = 1;
            $scope.page = {
                'currentPage': 1
            };
            groupToPages();
        }

        /**
     * Function to group bookmarks results into different pages
     */
        function groupToPages() {
            $scope.pagedAlerts = [];
            $scope.articleList.forEach(function(eachItem, i) {
                if (i % $scope.itemsPerPage === 0) {
                    $scope.pagedAlerts[Math.floor(i / $scope.itemsPerPage)] = [$scope.articleList[i]];
                } else {
                    $scope.pagedAlerts[Math.floor(i / $scope.itemsPerPage)].push($scope.articleList[i]);
                }
            });
            getPagesRange();
        }

        /**
     * Function to get the page numbers based on the results
     * @return {[type]} [description]
     */
        function getPagesRange() {
            $scope.pages = [];
            for (var i = 1; i <= $scope.pagedAlerts.length; i++) {
                $scope.pages.push(i);
            }
        }

        /**
     * Function to shift to previous page
     */
        function goToPrevPage() {
            if ($scope.currentPage > 1) {
                $scope.currentPage--;
            }
            $scope.page = {
                'currentPage': $scope.currentPage
            };
        }

        /**
     * Function to move to next page
     */
        function goToNextPage() {
            if ($scope.currentPage < $scope.pagedAlerts.length) {
                $scope.currentPage++;
            }
            $scope.page = {
                'currentPage': $scope.currentPage
            };
        }

        /**
     * Function to set the current page number with the page number passed
     * @param {String} pageNum [Selected page number]
     */
        function setPage(pageNum) {
            $scope.currentPage = pageNum * 1;
            $scope.page = {
                'currentPage': $scope.currentPage
            };
        }
    }
]);
