app.directive('swFlexibleTitleHyperlink', function() {
    return {
        restrict: 'E',
        scope: {
            title:'@',
            hyperLinkText: '@',
            hyperLinkUrl: '@',
            recordsCount: '@',
            color: '@',
            titlePosition: '@',
            isRuleExists: '=',
            titleFontSize: '@',
            imageUrl: '@',
            customBackground:'@',
            backgroundColor:'@'
        },
        controller:["$scope","$sce", function($scope, $sce){
		  $scope.title = $sce.trustAsHtml($scope.title);
        }],
        templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/clientlibs/classmags-migration-core/scripts/swFlexibleTitleHyperlink/swFlexibleTitleHyperlinkPartial.html'
    };
});

app.decorator('swFlexibleTitleHyperlinkDirective', ["$delegate", function($delegate) {
    var appName = $("#core-main-container").attr("data-app-name");
    if (appName && (appName.toLowerCase() === "scholasticnews3" || appName.toLowerCase() === "scholasticnews4" || appName.toLowerCase() === "scholasticnews56" || appName.toLowerCase() === "sn3" || appName.toLowerCase() === "sn4" ||
            appName.toLowerCase() === "sn56" || appName.toLowerCase() === "geographyspin" ||
            appName.toLowerCase() === "sciencespin36" || appName.toLowerCase() === "math" || appName.toLowerCase() === "dynamath" ||
            appName.toLowerCase() === "superscience")) {
        return ([$delegate[1]]);
    }
    return ([$delegate[0]]);
}]);