
   app.controller("vocabularyController", ["$scope", "$sce", "$compile","$rootScope", function($scope, $sce, $compile,$rootScope){

        var vocabUpdate = function() {
        $scope.vocabularyData=[];        
        var $vocabularyDataElement = angular.element("#vocabularySection");
        var $artcileDataSectionElement = angular.element(".js-article-data-section");

         $vocabularyDataElement.find(".js-vocabulary-word").each(function(){
            var $element = angular.element(this);            

            $scope.vocabularyData.push({
                "name":$element.attr('data-vocabulary-word') || "" ,
                "pronunciationtext": $element .attr('data-vocabulary-pronunciationtext')  || "",
                "definition":$element.attr('data-vocabulary-definition')  || "",
                "audiopath":$element.attr('data-vocabulary-audiopath')  || "",
                "imagepath":$element.attr('data-vocabulary-imagepath')  || "",
                "photocredit":$element.attr('data-vocabulary-photocredit')  || "",
                "show":false
            });
        });

        angular.forEach($scope.vocabularyData, function(value, key) {
            var articleContent="";
            var articleContentModified="";
            value = $scope.vocabularyData[key];
            var wordRegEx = new RegExp('\\b' + value.name + '\\b', 'i');
             $artcileDataSectionElement.each(function(){
                var matchFound = false;
                $(this).find(".js-article-item").each(function(index){
                    if(!($(this).css('display') == 'none')){
                    var $articleElement = angular.element(this)[0];                
                    articleContent = $articleElement.innerHTML;
                        if(wordRegEx.test(articleContent)){
                            var match = articleContent.match(wordRegEx);
                           var newElement = '<span class="vocabulary-main-container-wrapper"><sw-vocabulary-popover'
                                    +'  word="vocabularyData['+key+']'
                                    +'" ></sw-vocabulary-popover></span>';                             
                                    articleContentModified = articleContent.replace(wordRegEx, newElement);
                                    angular.element(this).children().remove();
                                    angular.element(this).append($compile(articleContentModified)($scope));                        
                            matchFound=true;
                            return false;                           
                        }  
                    }
                });
                if(matchFound){
                    return false;
                }
            });
        });
       }

       vocabUpdate();

       $rootScope.$on("LEXILE_LEVEL", function(event, data) {
           vocabUpdate();
       });

        $scope.showPopover = function(word){
            angular.forEach($scope.vocabularyData, function(value, key){
                if($scope.vocabularyData[key].name === word){
                    $scope.vocabularyData[key].show=true;
                }
                else{
                    $scope.vocabularyData[key].show=false;
                }
            })
        }

        $scope.closePopover = function(word){

        }
    }]);

