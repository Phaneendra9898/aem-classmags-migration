var DumbleData = DumbleData || {},
  dumbleData = dumbleData || {},
  dumbleOmni = dumbleOmni || {};

(function(DumbleData, dumbleData){
  var omniture = {
    domain: {
      channel: 'Migration',
      experience: 'Classroom Magazines',
      experienceType: 'Content',
      name: 'CM:Migration',
      server: 'migration'
    },
    page: {
      name: '',
      type: ''
    },
    self: {
      implementation: 'CM',
      version: '1.0.1'
    },
    user: null
  };

  try{
    omniture.page.name = document.querySelector('meta[name="cq:page"]').attributes.content.value;
  }catch(e){}
  try{
    omniture.page.type = document.querySelector('meta[name="cq:template"]').attributes.content.value;
  }catch(e1){}

  DumbleData.setUserSubStatus = function(status){
    if(omniture.user){
      omniture.user.subStatus = status;
    }
  };
  DumbleData.setUserTrialStatus = function(status){
    if(omniture.user){
      omniture.user.freeTrial = status;
    }
  };

  dumbleOmni = {
    pageName: omniture.page.name,
    channel: omniture.domain.server,
    dumbleSiteName: omniture.domain.name,
    dumbleSiteChannel: omniture.domain.channel,
    dumbleExperienceType: omniture.domain.experienceType,
    dumblePageType: omniture.page.type,
    server: omniture.domain.server
  };
}(DumbleData || {}, dumbleData || {}));

