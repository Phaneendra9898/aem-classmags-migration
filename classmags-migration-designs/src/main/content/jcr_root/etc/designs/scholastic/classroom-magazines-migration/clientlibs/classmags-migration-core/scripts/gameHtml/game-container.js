(function() {
    'use strict';

    /**
     * @ngdoc function
     * @name scholasticKids.directive:gameContainer
     * @description
     * # gameContainer
     * Directive of the scholasticKids
     */
    function gameContainer() {
        return {
            restrict: 'E',
            controller: gameContainerController,
            scope: {
                containerTitle: '@?',
                containerImage: '@?',
                containerGame: '@?',
                containerId: '@?',
                containerModal: '@?',
                containerPath: '@?',
                containerGameTitle: '@?',
                containerGameDesc: '@?',
                data: '=cardData',
                swGameAuthorize: '@'
            },
            templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/clientlibs/classmags-migration-core/templates/game-container.html'
        };
    }

    gameContainerController.$inject = ['$scope', '$timeout', 'authorizationService', 'swUserRole'];
    function gameContainerController($scope, $timeout, authorizationService, swUserRole) {     
        /**
         * if Ipad
         */
        function _isIpad() {
            var isIpad = navigator.userAgent.match(/iPad/i) != null;
            var isFlash = false;
            var iframeSrc = $('game-container').attr('container-path');
            if(iframeSrc){
            var iframeExt = iframeSrc.split('.').pop();            
            var errorTemplate = '<div class="mobile-warning ipad-version"><i class="icon-play"></i><h3>Oooops!</h3><p>This game doesn\'t work properly on mobile, please check back on your tablet or computer.</p></div>';
            
            if (iframeExt == 'swf' || iframeExt == 'flv' || iframeExt == 'fla') {
                isFlash = true;
            }

            if (isIpad && isFlash) {
                //remove iframe and show oops message
                $('.gamesIframeContainer, .mobile-warning').remove();
                $('.game-wrapper').append(errorTemplate);
            }
        }
        }

        /**
         * bootstrap modal onShow callback function
         * @param  {object} e bootstrap modal event
         * @return {void}
         */
        function _onShowModal(e) {
            if(!authorizationService.verifyAccess($scope.swGameAuthorize, swUserRole)){       
                return;
            }

            $('.main-container').css('table-layout', 'auto');
            var $this = $(e.currentTarget).css('display', 'block'),
                $window = $(window),
                $dialog = $this
                .find('.game-container-modal'),
                offset = ($window.height() -
                    $window.scrollTop() - $dialog.height()) / 2,
                marginBottom = parseInt(
                    $dialog.css('margin-bottom'), 10),
                marginTop = offset < marginBottom ? marginBottom :
                offset;
            var $iframe = $('#iframe-' + $scope.containerId);
            $dialog.append('<div class="spinnercontainer" id="html5-game"><img src="/etc/designs/scholastic/classroom-magazines-migration/images/icons/spinner.gif"></div>');
            $('#html5-game').show();
            var checkCanvasReady = setInterval(function() {
                var $canvas = $iframe.contents().find('canvas');

                if ($canvas.length) {

                    $dialog.css('height', '100%');

                    $dialog.find('.modal-content').css('height', '100%');


                    $('#game-container-modal-body').height('100%');


                    clearInterval(checkCanvasReady);
                    $('body iframe').contents().find('#closeButton').bind('click', function(e) {
                        $("#game-container-modal").modal("hide");
                    });

                    $timeout(function() {
                        $('#game-container-modal-body').css('height', '100%');
                        $('#html5-game').hide();
                        $('#game-container-modal-body').show();
                    }, 1500);


                    /* previous existing code for reference
                     *   var heigthModal = parseInt($canvas.attr('height')) + 100;
                    // $dialog.width($canvas.attr('width'));
                    //$dialog.height($canvas.attr('height'));
                    //$dialog.find('.modal-content').height($canvas.attr('height'));
                    //$dialog.find('.modal-content').css('width','100%');
                    //  $('#game-container-modal-body').css('height','100%');
                    // $('#game-container-modal-body').css('width','100%');
                     */

                }
            }, 500);


            var checkExist = setInterval(function() {
                var game = $('.game-container-modal .game-container-iframe iframe').contents().find('#theGame');
                if (game.children().length) {
                    $('.game-container-modal').width(game.width()).height(game.height());
                    clearInterval(checkExist);
                }
            }, 500);
        }

        /**
         * bootstrap modall onHide callback function
         * @param  {object} e bootstrap modal event
         * @return {void}
         */
        function _onHideModal(e) {
            var content = '<div></div>';
            $('.main-container').css('table-layout', 'fixed');
            $('#game-container-modal-body').html(content);
            var $iframe = $('#iframe-' + $scope.containerId);
            $iframe.attr('src', '');
        }

        /**
         * function that sets up the modal config and events
         * @return {void}
         */
        function _setUpModal() {
            $('#game-container-modal-body').hide();
            $('#html5-game').show();
            var content = $('#' + $scope.containerId).html();
            $('#game-container-modal-title').text();
            $('#game-container-modal-body').html(content);

            $('#game-container-modal').modal();
            $('#game-container-modal .close').hide();

            $('#game-container-modal').on('shown.bs.modal', _onShowModal);
            $('#game-container-modal').on('hidden.bs.modal', _onHideModal);
        }

        /**
         * function that opens video modal and load
         * the brightcove video
         * @return {void}
         */
        $scope.openModal = function() {
            if(!authorizationService.verifyAccess($scope.swGameAuthorize, swUserRole)){       
                return;
            }

            var videoId = $scope.containerPath,
                videoPlayerId = 'game-container-player-modal';
            var $iframe = $('#iframe-' + $scope.containerId);
            $iframe.attr('src', $scope.containerPath);
            _setUpModal();
        };

        /**
         * function that inits the directive and
         * asigns the values
         * @return {void}
         */
        $scope.init = function() {
            if ($scope.data) {
                $scope.containerTitle = $scope.data.containerTitle ? $scope.data.containerTitle : '';
                $scope.containerImage = $scope.data.containerImage ? $scope.data.containerImage : '';
                $scope.containerModal = $scope.data.containerModal ? $scope.data.containerModal : '';
                $scope.containerPath = $scope.data.containerPath ? $scope.data.containerPath : '';
            }

            _isIpad()
        };
        //execte init task
        $scope.init();
    }

    angular.module('scienceWorld').directive('gameContainer', gameContainer);
}());
