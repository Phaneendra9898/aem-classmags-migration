(function(document, $, ns) {
    "use strict";

    var fetchSelectedPageId = function(){
        var apiURL = "";
        var iframe = document.getElementById('ContentFrame');
        var innerDoc = iframe.contentDocument || iframe.contentWindow.document;  
        apiURL = apiURL + $(innerDoc).find("#eReader-page-path").attr("data-ereader-core-path")+"/_jcr_content/ereader-configuration.infinity.json";

        $.ajax({
            type: 'GET',
            url: apiURL,
            contentType: 'application/json; charset=utf-8',
            success: function(data){
                if(data){
                    $('#js-pageIdsList select[name="./pageId"]').val(data. pageId).change();
                }
            }
        });
    }

    var fetchPageIds = function(){
        var eReaderPath = $("input[name='./linkToEReaderNew']").val();
        if(eReaderPath){
        $.ajax({
            type: 'GET',
            url: "/bin/classmags/ereader?eReaderPath=" + eReaderPath,
            contentType: 'application/json; charset=utf-8',
            success: function(data) {
                var li_items = '',option_items = '';
                $.each(data.pages, function(i, data) {
                    option_items = option_items + '<option value=' + data + '>' + data + '</option>';
                    li_items = li_items + '<li class="coral-SelectList-item coral-SelectList-item--option is-highlighted" data-value="' + data + '" aria-selected="false" role="option" tabindex="' + i + '">' + data + '</li>';

                    if (i === 0) {
                        $('#pageId button span').html(data);
                    }

                });
            
                $('#js-pageIdsList select[name="./pageId"]').html(option_items);
                $('#js-pageIdsList ul.coral-SelectList').html(li_items);
                fetchSelectedPageId();
            },
            error: function() {
                $('.coral-Modal-backdrop').hide();
            }
        });
    }

    }

    $(document).on("dialog-ready", function() {
        if($("#js-eReaderSettingsTab-Control").length){
            fetchPageIds();
        }
    });

    $(document).on("click", "#js-fetchEReaderInfo", function() {
         if($("#js-eReaderSettingsTab-Control").length){
            fetchPageIds();
         }
    });
})(document, Granite.$, Granite.author);