app.controller("articlePageComponentsController", ["$scope", "$timeout", "$rootScope", "$compile", function($scope, $timeout, $rootScope, $compile) {
    /*
        var screenWidth;
        $rootScope.$on("OBJECTIVE_AND_SKILLS", function(event, data){
            var $elementArticleTextComponent = $("#article-text-page-container").clone();
            var $elementSubjectsCovered = $('.subjects-covered');
            var $elementObjectivesSkills = $('.objectives-and-skills');
            var $elementArticleText = $(".article-text");

            var alignArticleComponents = function(){
                screenWidth = viewport().width;
                if(screenWidth <= 991) {
                    $("#article-text-page-container").empty();
                    $("#article-text-page-container").append($compile($elementArticleText)($scope));
                    $("#article-text-page-container").append($elementObjectivesSkills);
                    $("#article-text-page-container").append($elementSubjectsCovered);
                }
                else{
                    $("#article-text-page-container").empty();
                    $("#article-text-page-container").append($compile($elementArticleTextComponent)($scope));
                }

            }
            if(screenWidth <= 991){
                alignArticleComponents();
            }

            $(window).on("resize", function(){
                alignArticleComponents();
            });

            function viewport() {
                var e = window, a = 'inner';
                if (!('innerWidth' in window )) {
                    a = 'client';
                    e = document.documentElement || document.body;
                }
                return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
            }

        });
    */
    $scope.scrollToTop = function() {
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
    };
}]);
