app.controller("breakingNewsController", ["$scope", "$sce", "$compile", "$element", function($scope, $sce, $compile, $element) {
    $scope.bannerVisible = true;

    var publishmode = $element.find('.breaking-news-banner-container').data('ispublish'),
        breakingNewsLastModifiedDate = $element.find('.breaking-news-banner-container').data('lastpublisheddate');
    breakingNewsLastModifiedDate = new Date(breakingNewsLastModifiedDate);

    if (publishmode) {
        var getUserModifiedDateCookie = localStorage.getItem("user-breaking-news-banner-hide");
        if (getUserModifiedDateCookie) {
            if (breakingNewsLastModifiedDate > new Date(getUserModifiedDateCookie)) {
                //show the banner
                $scope.bannerVisible = true;
            } else {
                //hide the banner
                $scope.bannerVisible = false;
            }
        } else {
            //show the banner
            $scope.bannerVisible = true;
        }
    } else {
        $scope.bannerVisible = true;
    }

    $scope.updateBannerVisibility = function() {
        $scope.bannerVisible = false;
        localStorage.setItem("user-breaking-news-banner-hide", new Date());
    }
}]);