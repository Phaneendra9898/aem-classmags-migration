app.directive('swTextHyperlink', function() {
    return {
        restrict: 'E',
        scope: {
            title:'@',
            hyperLinkText: '@',
            hyperLinkUrl: '@',
            recordsCount: '@',
            bgcolor: '@'
        },
        templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/scholasticnews56/clientlibs/scholasticnews56-core/scripts/textHyperlink/swTextHyperlinkPartial.html'
    }
});
