app.directive('tdiShowDescription', function() {
  return {
    restrict: 'A',
    link: function(scope, elem, attrs) {
      var contentTile = elem.find('sw-content-tile');
      if(contentTile.length > 0) {
      	elem.find(".app-tdi-content.visible-xs").removeClass("visible-xs");
      	elem.find(".app-tdi-banner-image").remove();
      }
    }
  }
});
