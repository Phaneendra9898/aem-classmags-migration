(function(document, $) {

    "use strict";

    var contentPath = '';

    $(document).on("foundation-contentloaded", function(e) {

        //Default content path
        contentPath = $(".foundation-collection").data("foundationCollectionId");
        if (contentPath != undefined && contentPath.charAt(contentPath.length - 1) != '/') {
				contentPath = contentPath + "/";
        }

        if (document.querySelector('.dam-upload-game')) {
        	_initialize();
        }
    });

    $(document).on("foundation-collection-navigate", function(e) { //FixMe: For Clumn View, content loaded event should be triggered on column navigation as well

        // In columns view we have create folder functionality in selection mode as well.
            contentPath = $(".foundation-selections-item").data("foundation-collection-item-id");
            if (contentPath != undefined && contentPath.charAt(contentPath.length - 1) != '/') {
				contentPath = contentPath + "/";
            }

        if ($(".cq-damadmin-admin-childpages.foundation-collection").data("foundationLayout").layoutId === "column") {
            _initialize();
        }

    });

    function _initialize() {
        var fileUploadSelectSelector = ".dam-upload-game"; //FixMe: It should iterate & initialize all coral-fileupload, coral-chunkfileupload elements available on page
        var fileUpload = document.querySelector(fileUploadSelectSelector); 
        if (!fileUpload) {
            return;
        }

        if (fileUpload.attributes.getNamedItem('is') && fileUpload.attributes.getNamedItem('is').value != ".dam-upload-game") {
            //Workarounf for Firexfox, Safari CQ-73789
            fileUpload.attributes.removeNamedItem('is');
            fileUpload.attributes.item('is').value = ".dam-upload-game";
        }
        Coral.commons.ready(fileUpload, function() {
            fileUpload.initialize();
        });

        var assetFU = new DamFileUpload().set('fileUpload', fileUpload);
        assetFU.initialize();
    }

    var uploadDialogEl;

    var DamFileUpload = new Class({
        fileUpload: null,
        uploadDialog: null,
        contentPath: '',
        contentType: '',
        _ILLEGAL_FILENAME_CHARS: ['*', '/', ':', '[', '\\', ']', '|', '#', '%'],

        set: function(prop, value) {
            this[prop] = value;
            return this;
        },

        initialize: function() {

            var dragCounter = 0;

            var self = this;

            //Create an upload dialog.
            self._createUploadGameDialogTemplate();

            var $foundationPath = $('.foundation-content-path');

            // Content path
            self.contentPath = $(".cq-damadmin-admin-childpages.foundation-collection").data("foundationCollectionId");

            // Content type: Folder or asset
            self.contentType = $foundationPath.data("foundationContentType");

            // Set the action attribute/
            self.fileUpload.setAttribute('action', self._getActionPath(self.contentPath));

            //Asynchronous upload by default
            self.fileUpload.setAttribute('async', true);

            //Container's Layout
            self.fileUpload.layoutId = $(".cq-damadmin-admin-childpages.foundation-collection").data("foundationLayout").layoutId;

            self.duplicateAssets = [];

            self.restrictedFiles = [];

            self.forbiddenFiles = [];

            self.chunkRetryHanlder;

            self.manualPause = false;

            if (!self.fileUpload) {
                return;
            }
            // Add the file-added event listener
            Coral.commons.ready(self.fileUpload, function() {
                self.fileUpload
                    .off('coral-fileupload:fileadded')
                    .on('coral-fileupload:fileadded', function(event) {
                        // Add file name parameter out of the file
                        event.detail.item.name = event.detail.item.file.name;
                        self._addFileListToDialog(event);
                    })
                    .on('coral-fileupload:filesizeexceeded', function(event) {
                        // Add to rejected list if filesizeexceeded
                        self.fileUpload.rejectedFiles = self.fileUpload.rejectedFiles || [];
                        self.fileUpload.rejectedFiles.push(event.detail.item);
                    })
                    .on('coral-fileupload:filemimetyperejected', function(event) {
                        // Add to rejected list if filemimetyperejected
                        self.fileUpload.rejectedFiles = self.fileUpload.rejectedFiles || [];
                        self.fileUpload.rejectedFiles.push(event.detail.item);
                    })
                    .on('change', function(event) {
                        if (self.fileUpload.uploadQueue && self.fileUpload.uploadQueue.length) {
                            self.uploadDialog.show();
                        }
                        if (self.fileUpload.rejectedFiles && self.fileUpload.rejectedFiles.length) {
                            self._showRejectedFiles();
                        }
                    })
                    .on('coral-fileupload:fileremoved', function(event) {
                        event.detail.item.listDialogEL.remove();

                    })
                    .off('coral-fileupload:loadend')
                    .on('coral-fileupload:loadend', function(event) {
                        self._fileUploaded(event);
                    })
                    .off('coral-fileupload:load')
                    .on('coral-fileupload:load', function(event) {
                        self._onFileLoaded(event);
                    })
                    .off('coral-fileupload:error')
                    .on('coral-fileupload:error', function(event) {
                        self._fileUploadedStatus(event);
                    })
                    .off('coral-fileupload:abort')
                    .on('coral-fileupload:abort', function(event) {
                        self._fileUploadCanceled(event);
                    })
                    .off('coral-chunkfileupload:progress')
                    .on('coral-chunkfileupload:progress', function(event) {
                        self._onChunkLoaded(event);
                    })
                    .off('coral-chunkfileupload:loadend')
                    .on('coral-chunkfileupload:loadend', function(event) {
                        self._refresh(event);
                    })
                    .off('coral-chunkfileupload:cancel')
                    .on('coral-chunkfileupload:cancel', function(event) {
                        self._fileChunkedUploadCanceled(event);
                    })
                    .off('coral-chunkfileupload:pause')
                    .on('coral-chunkfileupload:pause', function(event) {
                        self._pauseChunkUpload(event);
                    })
                    .off('coral-chunkfileupload:resume')
                    .on('coral-chunkfileupload:resume', function(event) {
                        self._resumeChunkUpload(event);
                    })
                    .off('coral-chunkfileupload:error')
                    .on('coral-chunkfileupload:error', function(event) {
                        self._fileChunkedUploadError(event);
                    })
                    .off('coral-chunkfileupload:querysuccess')
                    .on('coral-chunkfileupload:querysuccess', function(event) {
                        self._fileChunkedUploadQuerySuccess(event);
                    })
                    .off('coral-chunkfileupload:queryerror')
                    .on('coral-chunkfileupload:queryerror', function(event) {
                        self._fileChunkedUploadQueryError(event);
                    });

                    /*$("coral-shell-content")[0].addEventListener('drop', function(event) {
                        dragCounter = 0;
                        event.preventDefault();
                        self._dropZoneDrop(event);
                        self.fileUpload._onInputChange(event); // workaround until Coral.FileUpload adds an API to add Files programmatically
                    }, false);

                    $("coral-shell-content")[0].addEventListener('dragover', function(event) {
                        event.preventDefault(); // Preventing dragover is also required to prevent the browser of loading the file
                    }, false);

                    $("coral-shell-content")[0].addEventListener('dragleave', function(event) {
                        dragCounter--;
                        if (dragCounter === 0) {
                            self._dropZoneDragLeave(event);
                        }
                    }, false);

                    $("coral-shell-content")[0].addEventListener('dragenter', function(event) {
                        event.preventDefault(); // Preventing dragover is also required to prevent the browser of loading the file
                        dragCounter++;
                        self._dropZoneDragEnter(event);
                    }, false);*/

            });
        },

        _onFileLoaded: function(event) {
            var self = this;
            var el = event.detail.item.element;
            var progressBar = $('coral-progress', el)[0];
            if (progressBar) {
                progressBar.value = 100; //FixMe: There is lag between progress bar status & refresh action, hence not removing the progress bar
                $('coral-icon', el).each(function(cnt, item) {
                    item.remove();
                });
            }
            event.detail.item.listDialogEL.remove();
        },
        _fileUploaded: function(event){
        	var self = this;
        	self._fileUploadedStatus(event);
    	},

        _onError: function(event) {
            var self = this;
            var errorDialog = new Coral.Dialog()
                .set({
                    variant: 'error',
                    header: {
                        innerHTML: Granite.I18n.get('Error')
                    },
                    content: {
                        innerHTML: function() {
                            var contentHtml = Granite.I18n.get("Failed to upload the following file:");
                            contentHtml = contentHtml + "<br><br>";
                            contentHtml = contentHtml + event.detail.item.name;
                            return contentHtml;
                        }()
                    },
                    footer: {
                        innerHTML: '<button is="coral-button" variant="primary" coral-close>Ok</button>'
                    }
                }).show();
        },
		_detectDuplicateAssets: function(event) {
			var self = this;
			if (self.duplicateAssets.length > 0) {
				var firstDuplicate = self.duplicateAssets[0];
				var uploadedAsset = "<b>" + _g.XSS.getXSSValue(firstDuplicate[0]) + "</b>";
				var duplicatedBy = firstDuplicate[1].split(":");
				var duplicatedByMess = "<br><br>";

				for ( var i = 0; i < duplicatedBy.length; i++) {
					duplicatedByMess += _g.XSS.getXSSValue(duplicatedBy[i]) + "<br>"
				}

				var assetExistsMessage = Granite.I18n
						.get(
								"The asset uploaded {0} already exists in these location(s):",
								uploadedAsset);
				assetExistsMessage += duplicatedByMess;
				var description = Granite.I18n
						.get("Click 'Keep It' to keep or 'Delete' to delete the the uploaded asset.");

				var applyToAllMessage = Granite.I18n.get("Apply to all");
				var userSelection = "";
				var content = assetExistsMessage + "<br><br>" + description;
				var applyToAll = "<label><input type=\"checkbox\"><span></span></label>"
						+ applyToAllMessage;
				content = "<p>" + content + "</p>";
				if (self.duplicateAssets.length > 1) {
					content += applyToAll;
				}
				var dialog = new Coral.Dialog().set({
    				id: 'buyDialog',
    				header: {
      					innerHTML: 'Duplicate Asset Detected !'
    				},
    				content: {
    					innerHTML: content
   			 		}
  				});
            	$.each(dialog.getElementsByTagName('coral-dialog-footer'), function(cnt, item) {
                    //Keep Button
                    var keepButton = new Coral.Button().set({
                        variant: 'default',
                        innerText: Granite.I18n.get('Keep')
                    });
                    keepButton.label.textContent = Granite.I18n.get('Keep');
                    item.appendChild(keepButton);
                    keepButton.on('click', function() {
        				if (self.duplicateAssets.length === 1) {
							self.duplicateAssets = [];
							self._refresh(event);
						} else {
			// remove duplicates[0]
							self.duplicateAssets.splice(0, 1);
							self._detectDuplicateAssets(event);
            				//self._refresh(event);
						}
                        dialog.hide();
            			self._refresh(event);
					});

                    //Delete Button
                    var deleteButton = new Coral.Button().set({
                        variant: 'primary'
                    });
                    deleteButton.label.textContent = Granite.I18n.get('Delete');
                    item.appendChild(deleteButton);
                    deleteButton.on('click', function() {
                        if (self.duplicateAssets.length === 1) {
                            var a = [];
                            for ( var i = 0; i < self.duplicateAssets.length; i++) {
								a[i] = self.duplicateAssets[i][0];
							}
						self._deleteAssetsAfterUpload(a);
						self.duplicateAssets = [];
						//self._refresh(event);
						} else {
							var a = [];
							a[0] = self.duplicateAssets[0][0];
							self._deleteAssetsAfterUpload(a);
			// remove duplicates[0]
							self.duplicateAssets.splice(0, 1);
							self._detectDuplicateAssets(event);
						}
                        dialog.hide();
            			self._refresh(event);
					});
  				document.body.appendChild(dialog);
            	dialog.show();


				});
        	}
		},
        _onChunkUploadError: function(event) {
            var self = this;
            var element = event.detail.item.element;
            $('coral-icon.coral-Icon--pauseCircle', element)[0].hide();
            $('coral-icon.coral-Icon--playCircle', element)[0].show();
            $('.coral-Progress-status', element)[0].style.background = 'red';
            var errorDialog = new Coral.Dialog()
                .set({
                    variant: 'error',
                    header: {
                        innerHTML: Granite.I18n.get('Error')
                    },
                    content: {
                        innerHTML: function() {
                            var contentHtml = Granite.I18n.get("Failed to upload the following file:");
                            contentHtml = contentHtml + "<br><br>";
                            contentHtml = contentHtml + event.detail.item.name;
                            contentHtml = contentHtml + "<br><br>";
                            contentHtml = contentHtml + Granite.I18n.get("You may retry.");
                            return contentHtml;
                        }()
                    },
                    footer: {
                        innerHTML: '<button is="coral-button" variant="primary" coral-close>Ok</button>'
                    }
                }).show();
        },

        _onChunkLoaded: function(event) {
            var self = this;
            var el = event.detail.item.element;
            if (el) {
                var progress = ((event.detail.offset + event.detail.chunk.size) / (event.detail.item.file.size)) * 100;
                if (progress === 100) {
                    self._onFileLoaded(event);
                } else {
                    $('coral-progress', el)[0].value = progress;
                }
            }
        },

        /**
         * Creates a template for upload dialog which is not attached to the dom
         * */
        _createUploadGameDialogTemplate: function() {
            var self = this;
            if (!uploadDialogEl) {
                uploadDialogEl = new Coral.Dialog().set({
                    header: {
                        innerHTML: Granite.I18n.get('Upload HTML5 Game (zip file)')
                    }
                });     


                $(uploadDialogEl).attr('id', 'renameSelected');
                $.each(uploadDialogEl.getElementsByTagName('coral-dialog-footer'), function(cnt, item) {
                    //Cancel Button
                        var cancelButton = new Coral.Button().set({
                            variant: 'default',
                            innerText: Granite.I18n.get('Cancel')
                        });
                        cancelButton.label.textContent = Granite.I18n.get('Cancel');
                        item.appendChild(cancelButton);
                        cancelButton.on('click', function() {
                            self.uploadDialog.hide();
                            self.fileUpload.clear();
                            $('coral-dialog-content div', self.uploadDialog).each(function(cnt, itm) {
                                itm.remove();
                            });
                        });
    
                        //Upload Button
                        var uploadButton = new Coral.Button().set({
                            variant: 'primary'
                        });
                        uploadButton.label.textContent = Granite.I18n.get('Upload Game');
                        item.appendChild(uploadButton);
                        uploadButton.on('click', function() {
                            self._submit();
                        });

                });
            }
            self.uploadDialog = uploadDialogEl;
        },

        _addFileListToDialog: function(event) {
            var self = this;
            var item = event.detail.item;
            var fileName = item.file.name;
            var div = document.createElement('form');
			div.action = contentPath;
			div.method = 'POST';
            div.name = 'uploadGameForm';
			div.enctype = 'multipart/form-data';

            div.setAttribute('style', 'margin-bottom:0.5rem;');
            div.appendChild(new Coral.Textfield()
                .set({
                    name: 'uploadGame',
                    value: fileName,
                })
                .on('change', function(event) {
                    if (!self.utils.validZipFileName(this.value)) {
                        this.className = this.className + ' is-invalid';
                        Array.prototype.slice.call(this.parentElement.getElementsByTagName('coral-tooltip')).forEach(function(item) {
                            item.remove();
                        });
                        this.parentElement.appendChild(new Coral.Tooltip()
                            .set({
                                variant: 'error',
                                content: {
                                    innerHTML: Granite.I18n.get("Only zip files are allowed.<br/>Please select another file")
                                }
                            })
                            .show()
                        );
                        event.preventDefault();
                        return false;
                    } else {
                        this.classList.remove("is-invalid");
                        Array.prototype.slice.call(this.parentElement.getElementsByTagName('coral-tooltip')).forEach(function(item) {
                            item.remove();
                        });
                        self.utils.addOrReplaceInCustomPatameter(item,
                            'fileName',
                            this.value)
                    }
                })
                .on('keypress', function(event) {
                    if (!self.utils.validCharInput(event.charCode)) {
                        event.preventDefault();
                    }
                })
            );
            div.appendChild(new Coral.Button()
                .set({
                    variant: 'minimal',
                    icon: 'close',
                    iconsize: 'XS'
                })
                .on('click', function(event) {
                    self.fileUpload._clearFile(fileName);
                })
            );
            item.listDialogEL = div;
            $.each(self.uploadDialog.getElementsByTagName('coral-dialog-content'), function(cnt, item) {
                item.appendChild(div);
            });
        },

        _getActionPath: function(path) {
            path = path.trim();
            if (path.charAt(path.length - 1) === "/") {
                path = path.substring(0, path.length - 1);
            }
            return Granite.HTTP.externalize(path) + '.createasset.html';
        },

        _submit: function() {
            var self = this;
            var contentPath = $(".cq-damadmin-admin-childpages.foundation-collection").data("foundationCollectionId");
            if (contentPath != self.contentPath) {
                self.contentPath = contentPath;
            }

            if (self.fileUpload.uploadQueue && self.fileUpload.uploadQueue.length) {
                // add browser warning for the refresh
                window.onbeforeunload = Granite.I18n.get('Upload is in Progress. Refreshing page will lose the files that have not completed the upload.');

                var allFileNamesValid = true;
                $('coral-dialog-content form input', self.uploadDialog).each(function(cnt, ech) {
                    if (!self.utils.validZipFileName(ech.value)) {
                        allFileNamesValid = false;
                        this.className = this.className + ' is-invalid';
                        Array.prototype.slice.call(this.parentElement.getElementsByTagName('coral-tooltip')).forEach(function(item) {
                            item.remove();
                        });
                        this.parentElement.appendChild(new Coral.Tooltip()
                            .set({
                                variant: 'error',
                                content: {
                                    innerHTML: Granite.I18n.get("Only zip files are allowed.<br/>Please select another file")
                                }
                            })
                            .show()
                        );

                    }
                });

                if (allFileNamesValid) {
                    // get if any duplicate files are being uploaded
                    var duplicates = self.utils.getDuplicates(self.fileUpload.uploadQueue, self.contentPath);
                    if (duplicates && duplicates.length) {
                        self._showDuplicates(duplicates);
                    } else {
                        self._continueUpload();
                    }
                    self.uploadDialog.hide();
                } else {

                }
            }
        },

        _showDuplicates: function(duplicates) {
            var self = this;
            var xssName = _g.XSS.getXSSValue(duplicates[0]["name"]);
            var firstDuplicate = "<b>" + xssName + "</b>";
            var duplicateDialog = new Coral.Dialog().set({
                header: {
                    innerHTML: Granite.I18n.get('Name Conflict')
                }
            }).show();
            var assetExistsMessage;
            var description;
            if (self.contentType === 'folder') {
                assetExistsMessage = Granite.I18n.get(
                    "An asset named {0} already exists in this location.",
                    firstDuplicate);
                description = Granite.I18n
                    .get("Click 'Create Version' to create the version of the asset or 'Replace' to replace the asset or 'Keep Both' to keep both assets.");
            } else if (self.contentType === 'asset') {
                assetExistsMessage = Granite.I18n.get(
                    "A rendition named {0} already exists in this location",
                    firstDuplicate);
                description = Granite.I18n
                    .get("Click 'Replace' to replace the rendition or 'Keep Both' to keep both renditions or 'X' to cancel the upload.");
            }
            $.each(duplicateDialog.getElementsByTagName('coral-dialog-content'), function(cnt, item) {
                item.innerHTML = '<p>' + assetExistsMessage + '<br><br>' + description + '</p>';
                if (duplicates.length > 1) {
                    var checkbox = new Coral.Checkbox();
                    $('coral-checkbox-label', checkbox).each(function() {
                        this.innerHTML = Granite.I18n.get("Apply to all");
                    });
                    item.appendChild(checkbox);
                }
            });

            // Add buttons to footer
            $.each(duplicateDialog.getElementsByTagName('coral-dialog-footer'), function(cnt, item) {

                if (self.contentType === 'folder') {
                    var createVersionButton = new Coral.Button().set({
                        variant: 'primary'
                    });
                    createVersionButton.label.textContent = Granite.I18n.get("Create Version");
                    item.appendChild(createVersionButton);
                    createVersionButton.on('click', function() {
                        self._duplicateOperations.createVersion(duplicateDialog, self, duplicates);
                        duplicateDialog.hide();
                    });
                }

                var replaceButton = new Coral.Button().set({
                    variant: 'primary'
                });
                replaceButton.label.textContent = Granite.I18n.get("Replace");
                item.appendChild(replaceButton);
                replaceButton.on('click', function() {
                    self._duplicateOperations.replace(duplicateDialog, self, duplicates);
                    duplicateDialog.hide();
                });

                var keepBothButton = new Coral.Button().set({
                    variant: 'primary'
                });
                keepBothButton.label.textContent = Granite.I18n.get("Keep Both");
                item.appendChild(keepBothButton);
                keepBothButton.on('click', function() {
                    self._duplicateOperations.keepBoth(duplicateDialog, self, duplicates);
                    duplicateDialog.hide();
                });

                var cancelButton = new Coral.Button().set({
                    variant: 'primary'
                });
                cancelButton.label.textContent = Granite.I18n.get("Cancel");
                item.appendChild(cancelButton);
                cancelButton.on('click', function() {
                    duplicateDialog.hide();
                });
            });
        },

        _showRejectedFiles: function() {
            var self = this;
            var relectedDialog = new Coral.Dialog()
                .set({
                    variant: 'error',
                    header: {
                        innerHTML: Granite.I18n.get('Rejected Files')
                    },
                    content: {
                        innerHTML: function() {
                            var contentHtml = Granite.I18n.get("Following file(s) are " +
                                "either violating the size constraint " +
                                "or violating the mimetyepe constraint");
                            contentHtml = contentHtml + "<br><br>";
                            for (var i = 0; i < self.fileUpload.rejectedFiles.length; i++) {
                                contentHtml = contentHtml + self.fileUpload.rejectedFiles[i]["name"] + "<br>";
                            }
                            return contentHtml;
                        }()
                    },
                    footer: {
                        innerHTML: '<button is="coral-button" variant="primary" coral-close>Ok</button>'
                    }
                }).show();
        },

        _fileUploadCanceled: function(event) {
            var self = this;
            var item = event.detail.item;

            if (self.fileUpload._canChunkedUpload(item) === true) {
                self._cleanUpChunkedUpload(item);
                self.fileUpload._cancelChunckedUpload(item);
                var cancelEvent = {};
                cancelEvent.item = item;
                cancelEvent.fileUpload = self.fileUpload;
                self._fileUploadItemCleanup(cancelEvent);
            } else if (item.xhr.readyState === 0) {
                var cancelEvent = {};
                cancelEvent.item = item;
                cancelEvent.fileUpload = self.fileUpload;
                self._fileUploadItemCleanup(cancelEvent);
            } else {
                self._cancelUpload(item);
            }

        },

        _fileUploadItemCleanup: function(event) {
            var self = this;
            // hide the item
            // Using document.getElementById as direct jquery selector behaves erroneously for xss prone string
            $(document.getElementById("file_upload_" + event.item.name)).hide();

            self._refresh(event);
        },

        _changeIconPlayToPause: function(item) {
            var $el = $(document.getElementById("progress_" + item.name));
            $(".coral-Progress-bar", $el).addClass("small");
            $('.coral-Icon--playCircle', $el).hide();
            $('.coral-Icon--pauseCircle', $el).removeAttr("hidden");
            $('.coral-Icon--pauseCircle', $el).show();
            $('.coral-Progress-status', $el).css('background', '#2480cc');
        },

        _changeIconPauseToPlay: function(item) {
            var $el = $(document.getElementById("progress_" + item.name));
            $('.coral-Icon--pauseCircle', $el).hide();
            $('.coral-Icon--playCircle', $el).removeAttr("hidden");
            $('.coral-Icon--playCircle', $el).show();
            $('.coral-Progress-status', $el).css('background', '#FF0000');
        },

         /**
         Cancel upload of a file item
         @param {Object} item
         Object representing a file item
         */
        _cancelUpload: function(item) {
            item.xhr.abort();
        },

        _deleteAssetsAfterUpload: function(files) {
            // deletes the files from the server
            for (var i = 0; i < files.length; i++) {
                var filePath = Granite.HTTP.externalize(files[i]);
                Granite.$.ajax({
                    async: false,
                    url: filePath,
                    type: "POST",
                    data: {
                        ":operation": "delete"
                    }
                });
            }
        },

        _fileUploadedStatus: function(event) {
            var self = this;
			if ((event.detail && event.detail.item.status && event.detail.item.status == 200)) {
                self._refresh(event);
                return;
            } else if ((event.detail && event.detail.item.status && event.detail.item.status == 409)) {
                if (event.detail.item.status && event.detail.item.status == 409) {
                    var response = event.detail.item.responseText;
                    var title = $("#Path", response).text();
                    var changeLog = $("#ChangeLog", response).text().trim();
                } else {
                    //this condition is to support IE9
                    var title = event.message.headers['Path'];
                    var changeLog = event.message.headers['ChangeLog'];
                }
                var indexOfDuplicates = changeLog.indexOf("duplicates") + "duplicates(\"".length;
                var duplicates = changeLog.substring(indexOfDuplicates, changeLog.indexOf("\"", indexOfDuplicates));
                var arr = [title, duplicates];
                self.duplicateAssets.push(arr);
                self._detectDuplicateAssets(event);
            } else if ((event.detail && event.detail.item.status && event.detail.item.status == 415) ||
                (event.detail && event.detail.item.status && event.detail.item.status == 415)) {
                if (event.detail && event.detail.item.status && event.detail.item.status == 415) {
                    var response = event.item.xhr.responseText;
                    var msg = $("#Message", response).text();
                } else {
                    //this condition is to support IE9
                    var msg = $("#Message", response).text();
                }
                var file = msg.substr(0, msg.lastIndexOf(':'));
                var mimetype = msg.substr(msg.lastIndexOf(':') + 1);
                var obj = {
                    fileName: file,
                    mimeType: mimetype
                };
                self.restrictedFiles.push(obj);
            } else if ((event.detail && event.detail.item.status && event.detail.item.status == 403) ||
                (event.detail && event.detail.item.status && event.detail.item.status == 403)) {
                var forbiddenFile = {
                    fileName: event.item.fileName
                };
                self.forbiddenFiles.push(forbiddenFile);
            } else {
                self._onError(event);
            }
            // refresh the content when all the files are uploaded


        },

        /**
         * will be called up on fileuploadsuccess, fileUploadedStatus and fileuploadcanceled.
         * when all the files have beend processed, then refreshes the content and
         * returns true otherwise returns false;
         */
        _refresh: function(event) {
            var self = this;
            window.damUploadedFilesCount = window.damUploadedFilesCount || 0;
            window.damUploadedFilesCount++;
            if (window.damUploadedFilesCount === window.damTotalFilesForUpload) {
                window.damUploadedFilesCount = 0;
                window.damTotalFilesForUpload = 0;
                // clean onbeforeunload
                window.onbeforeunload = null;

                //refresh foundation-content
                var contentApi = $(".foundation-content").adaptTo("foundation-content");
                if (contentApi) {
                    contentApi.refresh();
                } else {
                    location.reload();
                }

                self._cleanup(event);
                return true;
            } else {
                return false;
            }

        },

        _continueUpload: function(event) {
            var self = this;
            //Hide the empty message banner before adding the cards
            $(".cq-damadmin-assets-empty-content").hide();
            
            self.fileUpload.uploadQueue.forEach(function(item, cnt) {
                item.parameters = item.parameters ? item.parameters : [];

                var assetName = _g.XSS.getXSSValue(item.name);

                item.xhr = new XMLHttpRequest();

                var file = item._originalFile;
                var f = new FormData();

                var damPath = self.utils._getValidActionPath(self.contentPath);

	            f.append("assetName", assetName);
	        	f.append("damPath", damPath);
	        	f.append("uploadGame", file);

                f.append("_charset_", "utf-8");
                
                item.xhr.onreadystatechange = function() {
                	var json = this.responseText;
                	if(json !== '' && json.length > 0){
        	            var response = JSON.parse(json);
	                	if (this.readyState == 4 && this.status == 200){
	                		self._refresh(event);
	                	} else if(this.readyState == 4 && this.status == 500) {
	            	        self._fileUploadError(response);
	                    }
                	}
                }
                
                item.xhr.open("POST", "/bin/classmags/migration/upload/game", true); 
                item.xhr.send(f);

            });

            window.damTotalFilesForUpload = window.damTotalFilesForUpload || 0;
            window.damTotalFilesForUpload = window.damTotalFilesForUpload + self.fileUpload.uploadQueue.length;
            self.fileUpload.uploadQueue = [];

        },
        _fileUploadError: function(response){
        	var ui = $(window).adaptTo("foundation-ui");
            ui.alert(Granite.I18n.get("Error"), response.errors, "error");
        },
        
        //Creating item at ListMode
        _createRow: function(item) {
            var self = this;
            var assetName = _g.XSS.getXSSValue(item.name);
            item.parameters.forEach(function(each) {
                if (each.name == 'fileName') {
                    assetName = _g.XSS.getXSSValue(each.value);
                }
            });

            var row = new Coral.Table.Row();
            row.id = "file_upload_" + assetName;
            var thumbnailCell = new Coral.Table.Cell();
            var thumbnailImg = document.createElement("img");
            thumbnailImg.setAttribute('src', '');
            thumbnailImg.classList.add("foundation-collection-item-thumbnail")
            var thumbnailCellLabel = $('coral-td-label', thumbnailCell)[0];
            thumbnailCellLabel.appendChild(thumbnailImg);

            var titleCell = new Coral.Table.Cell();
            titleCell.classList.add("foundation-collection-item-title");
            titleCell.value = assetName;
            var titleCellLabel = $('coral-td-label', titleCell)[0];
            titleCellLabel.innerText = assetName.substring(0, 20); //FixMe:Truncating title's width to prevent overflow.

            row.appendChild(thumbnailCell);
            row.appendChild(titleCell);

            var progress = new Coral.Progress();
            progress.classList.add("dam-asset-upload-list-item-progress");
            progress.id = "progress_" + assetName;
            var progressBar = $('.coral-Progress-bar', progress)[0];
            progressBar.classList.add("dam-asset-upload-list-item-progress-bar");

            titleCellLabel.appendChild(progress);
            var cancelIcon = new Coral.Icon().set({
                size: 'XS',
                icon: 'closeCircle'
            }).on('click', function() {
                self.fileUpload.cancel(item);
            });
            cancelIcon.classList.add("dam-asset-upload-list-item-cancel-icon");
            var pauseIcon = new Coral.Icon().set({
                size: 'XS',
                icon: 'pauseCircle'
            }).on('click', function() {
                self.fileUpload.pause(item);
            });
            pauseIcon.classList.add("dam-asset-upload-list-item-pause-icon");
            var playIcon = new Coral.Icon().set({
                size: 'XS',
                icon: 'playCircle'
            }).on('click', function() {
                self.fileUpload.resume(item);
            }).hide();
            playIcon.classList.add("dam-asset-upload-list-item-play-icon");
            progress.appendChild(cancelIcon);

            if (self.fileUpload._canChunkedUpload(item)) {
                progress.appendChild(pauseIcon);
                progress.appendChild(playIcon);
            }
            item.element = row;
            return row;

        },

        //Creating item at ColumnMode
        _createColumn: function(item) {
            var self = this;
            var assetName = _g.XSS.getXSSValue(item.name);
            item.parameters.forEach(function(each) {
                if (each.name == 'fileName') {
                    assetName = _g.XSS.getXSSValue(each.value);
                }
            });

            var column = new Coral.ColumnView.Item();
            column.id = "file_upload_" + assetName;
            var columnContent = $('coral-columnview-item-content', column)[0];
            columnContent.innerText = assetName.substring(0, 20); //FixMe:Truncating title's width to prevent overflow.
            columnContent.setAttribute("title", assetName);
            var progress = new Coral.Progress();
            progress.classList.add("dam-asset-upload-column-item-progress");
            progress.id = "progress_" + assetName;
            var progressBar = $('.coral-Progress-bar', progress)[0];
            progressBar.classList.add("dam-asset-upload-column-item-progress-bar");
            columnContent.appendChild(progress);
            var cancelIcon = new Coral.Icon().set({
                size: 'XS',
                icon: 'closeCircle'
            }).on('click', function() {
                self.fileUpload.cancel(item);
            });
            cancelIcon.classList.add("dam-asset-upload-column-item-cancel-icon");
            var pauseIcon = new Coral.Icon().set({
                size: 'XS',
                icon: 'pauseCircle'
            }).on('click', function() {
                self.fileUpload.pause(item);
            });
            pauseIcon.classList.add("dam-asset-upload-column-item-pause-icon");
            var playIcon = new Coral.Icon().set({
                size: 'XS',
                icon: 'playCircle'
            }).on('click', function() {
                self.fileUpload.resume(item);
            }).hide();
            playIcon.classList.add("dam-asset-upload-column-item-play-icon");
            progress.appendChild(cancelIcon);

            if (self.fileUpload._canChunkedUpload(item)) {
                progress.appendChild(pauseIcon);
                progress.appendChild(playIcon);
            }
            item.element = column;
            return column;

        },

        //Creating item at CardMode
        _createMasonry: function(item) {
            var self = this;
            var assetName = _g.XSS.getXSSValue(item.name);
            item.parameters.forEach(function(each) {
                if (each.name == 'fileName') {
                    assetName = _g.XSS.getXSSValue(each.value);
                }
            });

            var card = new Coral.Card();
            card.id = "file_upload_" + assetName;
            var progress = new Coral.Progress();
            progress.classList.add("dam-asset-upload-card-item-progress");
            progress.id = "progress_" + assetName;
            var progressBar = $('.coral-Progress-bar', progress)[0];
            progressBar.classList.add("dam-asset-upload-card-item-progress-bar");

            var cardContent = $('coral-card-content', card)[0];
            cardContent.appendChild(progress);
            var cancelIcon = new Coral.Icon().set({
                size: 'XS',
                icon: 'closeCircle'
            }).on('click', function() {
                self.fileUpload.cancel(item);
            });
            cancelIcon.classList.add("dam-asset-upload-card-item-cancel-icon");
            var pauseIcon = new Coral.Icon().set({
                size: 'XS',
                icon: 'pauseCircle'
            }).on('click', function() {
                self.fileUpload.pause(item);
            });
            pauseIcon.classList.add("dam-asset-upload-card-item-pause-icon");
            var playIcon = new Coral.Icon().set({
                size: 'XS',
                icon: 'playCircle'
            }).on('click', function() {
                self.fileUpload.resume(item);
            }).hide();
            playIcon.classList.add("dam-asset-upload-card-item-play-icon");
            progress.appendChild(cancelIcon);
            if (self.fileUpload._canChunkedUpload(item)) {
                progress.appendChild(pauseIcon);
                progress.appendChild(playIcon);
            }
            var title = document.createElement('h4');
            title.innerHTML = assetName;
            cardContent.appendChild(title);
            var masonryItem = new Coral.Masonry.Item();
            masonryItem.appendChild(card);
            item.element = masonryItem;
            return masonryItem;

        },

        _cleanup: function(event) {
            var self = this;
            window.onbeforeunload = null;
            window.damFileUploadSucces = 0;
            self.fileUpload.list = [];
            self.fileUpload.uploadQueue.length = 0;
            self.fileUpload.rejectedFiles = [];
        },

        _fileChunkedUploadCanceled: function(event) {
            var self = this;
            var item = event.detail.item;
            self._changeIconPauseToPlay(item);
        },

        _cleanUpChunkedUpload: function(item) {
            var self = this;
            var filePath = self.contentPath + "/" + UNorm.normalize("NFC", item.name);
            var jsonPath = filePath + "/_jcr_content/renditions/original.1.json?ch_ck = " + Date.now();
            jsonPath = Granite.HTTP.externalize(jsonPath);
            var result = Granite.$.ajax({
                type: "GET",
                async: false,
                url: jsonPath
            });
            if (result.status === 200) {
                self._deleteChunks([item]);
            } else {
                self._deleteAssetsAfterUpload([filePath]);
            }
        },

        _deleteChunks: function(files) {
            // deletes the files from the server

            var foundationContentPath = self.contentPath;
            var foundationContentType = self.contentType;
            if (foundationContentType === "asset") {
                foundationContentPath += "/_jcr_content/renditions";
            }
            foundationContentPath = Granite.HTTP.externalize(foundationContentPath);
            for (var i = 0; i < files.length; i++) {
                var filePath = foundationContentPath + "/" + UNorm.normalize("NFC", files[i]["fileName"]);
                if (self.fileUpload._canChunkedUpload(files[i]) === true) {
                    self.fileUpload._deleteChunkedUpload(filePath);
                }
            }
        },


        //Pause
        _pauseChunkUpload: function(event) {
            var self = this;
            var item = event.detail.item;
            self.fileUpload._cancelChunckedUpload(item);
        },

        _resumeChunkUpload: function(event) {
            var self = this;
            var item = event.detail.item;
            self._changeIconPlayToPause(item);
            var url = self._getChunkInfoUrl(item, self.fileUpload);
            self.fileUpload._queryChunkedUpload(url, item);
        },

        _getChunkInfoUrl: function(item, fileupload) {
            var index = fileupload.attributes['action'].value.indexOf(".createasset.html");
            return fileupload.attributes['action'].value.substring(0, index) + "/" + item["name"] + ".3.json";
        },

        _fileChunkedUploadQuerySuccess: function(event) {
            var self = this;
            try {
                var json = JSON.parse(event.detail.originalEvent.target.responseText);
                var bytesUploaded = json["jcr:content"]["sling:length"];
                if (bytesUploaded) {
                    self.fileUpload._resumeChunkedUpload(event.detail.item, bytesUploaded);
                } else {
                    event.message = "No chunk upload found."
                    self._fileUploadedStatus(event);
                }
            } catch (err) {
                event.message = "Error in query chunked upload."
                self._fileUploadedStatus(event);
            }
        },

        _fileChunkedUploadQueryError: function(event) {
            var self = this;
            self._fileChunkedUploadError(event);
        },

        _fileChunkedUploadError: function(event) {
            var self = this;

            if ((event.detail && event.detail.originalEvent.target.status && event.detail.originalEvent.target.status == 200)){
                	return;
            } else if ((event.detail && event.detail.originalEvent.target.status && event.detail.originalEvent.target.status == 415)) {
                if (event.detail.originalEvent.target.status && event.detail.originalEvent.target.status == 415) {
                    var response = event.detail.item.xhr.responseText;
                    var msg = $("#Message", response).text();
                } else {
                    //this condition is to support IE9
                    var msg = $("#Message", response).text();
                }
                var file = msg.substr(0, msg.lastIndexOf(':'));
                var mimetype = msg.substr(msg.lastIndexOf(':') + 1);
                var obj = {
                    fileName: file,
                    mimeType: mimetype
                };
                self.restrictedFiles.push(obj);

                var item = getFileObject(file);
                var contextPath = CUIFileUpload.options.uploadUrl.substr(0, CUIFileUpload.options.uploadUrl.indexOf("/content/dam"));
                if (self.fileUpload._canChunkedUpload(item) === true) {
                    self.fileUpload._cancelChunckedUpload(item);
                    var cancelEvent = {};
                    cancelEvent.item = item;
                    cancelEvent.fileUpload = CUIFileUpload;
                    self._fileUploadCanceled(cancelEvent);
                } else if (item.xhr.readyState === 0) {
                    var cancelEvent = {};
                    cancelEvent.item = item;
                    cancelEvent.fileUpload = CUIFileUpload;
                    self._fileUploadCanceled(cancelEvent);
                } else {
                    self._cancelUpload(item);
                }
            } else if ((event.detail && event.detail.originalEvent.target.status && event.detail.originalEvent.target.status == 403)) {
                var forbiddenFile = {
                    fileName: event.detail.item.file.name
                };
                self.forbiddenFiles.push(forbiddenFile);

                var forbiddenFileObject = event.detail.item;;

                if (self.fileUpload._canChunkedUpload(forbiddenFileObject) === true) {
                    self.fileUpload._cancelChunckedUpload(forbiddenFileObject);
                    var cancelEvent = {};
                    cancelEvent.item = forbiddenFileObject;
                    cancelEvent.fileUpload = CUIFileUpload;
                    self._fileUploadCanceled(cancelEvent);
                } else if (forbiddenFileObject.xhr.readyState === 0) {
                    var cancelEvent = {};
                    cancelEvent.item = forbiddenFileObject;
                    cancelEvent.fileUpload = CUIFileUpload;
                    self._fileUploadCanceled(cancelEvent);
                } else {
                    self._cancelUpload(forbiddenFileObject);
                }
            } else if ((event.detail && event.detail.originalEvent.target.status && event.detail.originalEvent.target.status == 409)) {
                if (event.detail.originalEvent.target.status && event.detail.originalEvent.target.status == 409) {
                    var response = event.detail.item.xhr.responseText;
                    var title = $("#Path", response).text();
                    var changeLog = $("#ChangeLog", response).text().trim();
                } else {
                    //this condition is to support IE9
                    var title = event.message.headers['Path'];
                    var changeLog = event.message.headers['ChangeLog'];
                }
                var indexOfDuplicates = changeLog.indexOf("duplicates") + "duplicates(\"".length;
                var duplicates = changeLog.substring(indexOfDuplicates, changeLog.indexOf("\"", indexOfDuplicates));
                var arr = [title, duplicates];
                self.duplicateAssets.push(arr);
                self._detectDuplicateAssets(event);
          	} else {
                self._changeIconPauseToPlay(event.detail.item);
                if (self.manualPause === false) {
                    // retry after 5 seconds
                    self.chunkRetryHanlder = setTimeout(function() {
                        self._resumeChunkUpload(event);
                    }, 5000);
                }
            }
        },


        //Drop Zone implementation

        /*_dropZoneDragEnter: function(event) {
            var message = Granite.I18n.get("Drag and drop to upload");
            var dragAndDropMessage = $('<div class=\"drag-drop-message\" style="text-align: center;"><h1 > <span>{</span>' + message + '<span>}</span></h1></div>');
            $('.foundation-collection').overlayMask('show', dragAndDropMessage);
        },

        _dropZoneDragLeave: function(event) {
            $('.foundation-collection').overlayMask('hide');
        },

        _dropZoneDrop: function(event) {
            $('.foundation-collection').overlayMask('hide');
        },*/

        _duplicateOperations: {
            keepBoth: function(duplicateDialog, damfileupload, duplicates) {
                var applyAllCheckbox = duplicateDialog.getElementsByTagName('coral-checkbox')[0];
                if ((applyAllCheckbox && applyAllCheckbox.checked) || duplicates.length === 1) {
                    this._autoResolveDuplicateFileNames(damfileupload, duplicates);
                    damfileupload._continueUpload(damfileupload.fileUpload);
                } else {
                    this._autoResolveDuplicateFileNames(damfileupload, [duplicates[0]]);
                    duplicates.splice(0, 1);
                    damfileupload._showDuplicates(duplicates);
                }

            },
            replace: function(duplicateDialog, damfileupload, duplicates) {
                var applyAllCheckbox = duplicateDialog.getElementsByTagName('coral-checkbox')[0];
                if ((applyAllCheckbox && applyAllCheckbox.checked) || duplicates.length === 1) {
                    this._addReplaceAssetParam(damfileupload.fileUpload, duplicates);
                    damfileupload._continueUpload(damfileupload.fileUpload);
                } else {
                    this._addReplaceAssetParam(damfileupload.fileUpload, [duplicates[0]]);
                    duplicates.splice(0, 1);
                    damfileupload._showDuplicates(duplicates);
                }
            },
            createVersion: function(duplicateDialog, damfileupload, duplicates) {
                var applyAllCheckbox = duplicateDialog.getElementsByTagName('coral-checkbox')[0];
                if ((applyAllCheckbox && applyAllCheckbox.checked) || duplicates.length === 1) {
                    damfileupload._continueUpload(damfileupload.fileUpload);
                } else {
                    duplicates.splice(0, 1);
                    damfileupload._showDuplicates(duplicates);
                }
            },
            _autoResolveDuplicateFileNames: function(damfileupload, duplicates) {
                var duplicatesIndex = 0;
                for (var i = 0; i < damfileupload.fileUpload.uploadQueue.length && duplicatesIndex < duplicates.length; i++) {
                    if (duplicates[duplicatesIndex].name === damfileupload.fileUpload.uploadQueue[i].name) {
                        damfileupload.fileUpload.uploadQueue[i].parameters = damfileupload.fileUpload.uploadQueue[i].parameters ? damfileupload.fileUpload.uploadQueue[i].parameters : [];
                        damfileupload.utils.addOrReplaceInCustomPatameter(damfileupload.fileUpload.uploadQueue[i],
                            'fileName',
                            this._resolveFileName(duplicates[duplicatesIndex].name, window.damDirectoryJson));
                        duplicatesIndex++;
                    }
                }
            },
            _resolveFileName: function(fileName, directoryJson) {
                var fn = fileName;
                var fileExtn = "";
                if (fileName.indexOf(".") !== -1) {
                    fn = fileName.substr(0, fileName.lastIndexOf("."));
                    fileExtn = fileName.substr(fileName.lastIndexOf(".") + 1);
                }
                var counter = 1;
                var tempFn;
                do {
                    tempFn = fn + counter;
                    counter++;
                } while (directoryJson[UNorm.normalize("NFC", tempFn) + "." + fileExtn]);
                return tempFn + "." + fileExtn;
            },
            _addReplaceAssetParam: function(fileUpload, duplicates) {
                for (var j = 0; j < duplicates.length; j++) {
                    var duplicateFileName = duplicates[j]["name"];
                    for (var i = 0; i < fileUpload.uploadQueue.length; i++) {
                        var fileName = fileUpload.uploadQueue[i].name;
                        if (duplicateFileName === fileName) {
                            fileUpload.uploadQueue[i].parameters = fileUpload.uploadQueue[i].parameters ? fileUpload.uploadQueue[i].parameters : [];
                            fileUpload.uploadQueue[i].parameters.push({
                                name: ':replaceAsset',
                                value: 'true'
                            });
                            break;
                        }
                    }
                }
            }
        },

        utils: {
        	validZipFileName: function(fileName) {
                var utls = this;
                if (typeof fileName === 'string') {
                    if (fileName != "" && !/\.zip$/.test(fileName)) {
                        return false;
                    } else {
                        return true;
                    }
                } else {
                    var allFileNamesValid = true;
                    fileName.forEach(function(eachName) {
                        if (eachName != "" && !/\.zip$/.test(eachName)) {
                            allFileNamesValid = false;
                        }
                    });
                    return allFileNamesValid;
                }
            },
            _getValidActionPath: function(path) {
                path = path.trim();
                if (path.charAt(path.length - 1) === "/") {
                    path = path.substring(0, path.length - 1);
                }
                return Granite.HTTP.externalize(path);
            },
            validCharInput: function(input) {
                var code = event.charCode;
                var restrictedCharCodes = [42, 47, 58, 91, 92, 93, 124, 35];
                if ($.inArray(code, restrictedCharCodes) > -1) {
                    return false;
                }
                return true;
            },
            contains: function(str, chars) {
                for (var i = 0; i < chars.length; i++) {
                    if (str.indexOf(chars[i]) > -1) {
                        return true;
                    }
                }
                return false;
            },
            getDuplicates: function(uploadFiles, contentPath) {
                var duplicates = [];
                var duplicateCount = 0;
                var jsonPath = "";
                var jsonResult;
                var foundationContent = $(".foundation-content-path");
                var foundationContentType = foundationContent
                    .data("foundationContentType");
                var resourcePath = encodeURIComponent(contentPath).replace(/%2F/g,
                    "/");
                if (foundationContentType === "folder") {
                    jsonPath = resourcePath + ".1.json?ch_ck = " + Date.now();
                } else if (foundationContentType === "asset") {
                    jsonPath = resourcePath + "/_jcr_content/renditions.1.json?ch_ck = " + Date.now();
                }
                jsonPath = Granite.HTTP.externalize(jsonPath);
                var result = Granite.$.ajax({
                    type: "GET",
                    async: false,
                    url: jsonPath
                });
                if (result.status === 200) {
                    jsonResult = JSON.parse(result.responseText);
                    for (var i = 0; i < uploadFiles.length; i++) {
                        if (jsonResult[UNorm.normalize("NFKC",
                                uploadFiles[i].name ? uploadFiles[i].name : uploadFiles[i].file.name)]) {
                            duplicates[duplicateCount] = uploadFiles[i];
                            duplicateCount++;
                        }
                    }
                }
                window.damDirectoryJson = jsonResult;
                return duplicates;
            },
            addOrReplaceInCustomPatameter: function(item, name, value) {
                item.parameters = item.parameters ? item.parameters : [];
                var isPresent = false;
                item.parameters.forEach(function(itm) {
                    isPresent = true;
                    if (itm.name === name) {
                        itm.value = value;
                    }
                });
                if (!isPresent) {
                    item.parameters.push({
                        name: name,
                        value: value
                    })
                }

                // Specific handling for file name. As file object also contains name
                if (name === 'fileName') {
                    item.name = value;
                }
            }
        }

    });


})(document, Granite.$);