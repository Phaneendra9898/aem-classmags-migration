'use strict';

app.controller("searchResourceListSearchPageController", ["$scope", "$http", "searchResourcesDataFactory", "$timeout", function($scope, $http, searchResourcesDataFactory, $timeout) {
    var NO_OF_RECORDS_PER_PAGE = 12;

    dumbleData.search={};
    dumbleData.search.filters=[];
    var filters={};

    var updateQueryStringParameter = function(uri, key, value) {
        var re = new RegExp("([?&])" + key + "=.*?(&|#|$)", "i");
        if (uri.match(re)) {
            return uri.replace(re, '$1' + key + "=" + value + '$2');
        } else {
            var hash = '';
            if (uri.indexOf('#') !== -1) {
                hash = uri.replace(/.*#/, '#');
                uri = uri.replace(/#.*/, '');
            }
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
            return uri + separator + key + "=" + value + hash;
        }
    };

    var getParameterByName = function(name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    };

    var removeType = function(atypes, id) {
        var atypesArr = [];
        var atypeString = "";
        atypesArr = atypes.split(",");
        for (var i = 0; i < atypesArr.length; i++) {
            if (atypesArr[i] !== "" && atypesArr[i] != id) {
                atypeString = atypeString + "," + atypesArr[i];
            }
        }
        return atypeString.replace(",", "");
    };

    var filterData = function(id, type, isChecked) {
        if ($scope.currentTab === "article") {
            if (type === "type") {
                var atypes = getParameterByName('atypes');
                if (atypes=="" || atypes) {
                    if (atypes.indexOf(id) === -1) {
                        if(atypes==""){
                            atypes = atypes+id;
                        }
                        else{
                            atypes = atypes + "," + id;
                        }
                        location.href = updateQueryStringParameter(location.href, 'atypes', atypes);
                    } else {
                        if (!isChecked) {
                            atypes = removeType(atypes, id);
                            location.href = updateQueryStringParameter(location.href, 'atypes', atypes);
                        }
                    }
                } else {
                    location.href = location.href + "&atypes=" + id;
                }

            } else if (type === "subject") {
                var asubjects = getParameterByName('asubjects');
                if (asubjects=="" || asubjects) {
                    if (asubjects.indexOf(id) === -1) {
                        if(asubjects==""){
                           asubjects = asubjects+id;
                        }
                        else{
                            asubjects = asubjects + "," + id;
                        }

                        location.href = updateQueryStringParameter(location.href, 'asubjects', asubjects);
                    } else {
                        if (!isChecked) {
                            asubjects = removeType(asubjects, id);
                            location.href = updateQueryStringParameter(location.href, 'asubjects', asubjects);
                        }
                    }

                } else {
                    location.href = location.href + "&asubjects=" + id;
                }
            }
        } else {
            if (type === "subject") {
                var isubjects = getParameterByName('isubjects');
                if (isubjects) {
                    if (isubjects.indexOf(id) === -1) {
                        isubjects = isubjects + "," + id;
                        location.href = updateQueryStringParameter(location.href, 'isubjects', isubjects);
                    } else {
                        if (!isChecked) {
                            isubjects = removeType(isubjects, id);
                            location.href = updateQueryStringParameter(location.href, 'isubjects', isubjects);
                        }
                    }
                } else {
                    location.href = location.href + "&isubjects=" + id;
                }
            }
        }
    };

    var populateFilters = function(data) {
        if (data && data.facets && data.facets.myHashMap) {
            if (data.facets.myHashMap.subject_tags_ss) {
                var subjectTags = data.facets.myHashMap.subject_tags_ss.myArrayList;
                var matchFound = false;

                angular.element(".search-resources-main-container").find(".js-subject").each(function() {
                    var $element = $(this);
                    matchFound = false;
                    angular.forEach(subjectTags, function(value, key) {
                        if ($element.find("span.checkbox-text").text().trim().toLowerCase() == value.myHashMap.value.toLowerCase()) {
                            if($element.find('input').prop('checked')){
                                $element.find("span.checkbox-text").addClass('checkbox-filter-selected');
                            }
                            $element.find("span.checkbox-text").text($element.find("span").text() + " (" + value.myHashMap.count + ")");
                            matchFound = true;
                            return;
                        }
                    });
                    if (!matchFound) {
                        $element.find('input').attr("checked", true);
                        $element.find('input').attr("disabled", true);
                        $element.find("span.checkbox-text").text($element.find("span").text() + " (0)").addClass('checkbox-text-disabled');
                        $element.find("span.sw-icon").addClass('checkbox-checked-disabled');
                    }
                });
            }

            if (data.facets.myHashMap.type_s) {
                var typeTags = data.facets.myHashMap.type_s.myArrayList;
                var matchFound = false;
                angular.element(".search-resources-main-container").find(".js-type").each(function() {
                    var $element = $(this);
                    matchFound = false;
                    angular.forEach(typeTags, function(value, key) {
                        if ($element.find("span.checkbox-text").text().trim().toLowerCase() == value.myHashMap.value.toLowerCase()) {
                            if($element.find('input').prop('checked')){
                                $element.find("span.checkbox-text").addClass('checkbox-filter-selected');
                            }
                            $element.find("span.checkbox-text").text($element.find("span").text() + " (" + value.myHashMap.count + ")");
                            matchFound = true;
                            return;
                        }
                    });
                    if (!matchFound) {
                        $element.find('input').attr("checked", true);
                        $element.find('input').attr("disabled", true);
                        $element.find("span.checkbox-text").text($element.find("span").text() + " (0)").addClass('checkbox-text-disabled');
                        $element.find("span.sw-icon").addClass('checkbox-checked-disabled');
                    }
                });
            }
        }
    };


    var searchSuccessCallback = function(response) {
        if (response && response.data && response.data.length > 0) {
            angular.forEach(response.data, function(record) {
                if (record && record.type === "Issue") {
                    $scope.issuesList = record.solrDocumentList;
                    $scope.totalIssues = record.total;
                    $timeout(function(){
                        if ($scope.currentTab === "issue") {
                            populateFilters(record);                           
                        }
                        });
                        $scope.resultsCount = $scope.totalIssues;
                    
                   

                } else if (record && record.type === "Article") {
                    $scope.articleList = record.solrDocumentList;
                    angular.forEach($scope.articleList, function(value, key) {
                        if (value.type_s && value.type_s.toLowerCase() === 'article' ||
                            value.type_s && value.type_s.toLowerCase() === 'issue') {
                            value.redirectionURL = value.id;
                            value.downloadURL = value.id;
                        } else {
                            value.redirectionURL = value.container_url_s;
                            value.downloadURL = value.id;
                        }
                    });
                    if ($scope.currentTab === "article") {
                        $timeout(function(){
                            populateFilters(record);
                        });
                        $scope.resultsCount = $scope.articleList.length;
                       
                    }
                    $scope.totalArticlesResources = record.total;
                }
            });

            if (getParameterByName('text')) {
                $timeout(function() {
                    angular.element(".search-resources-main-container").find(".js-subject").click(function() {
                        var $inputElement = $(this).find("input");
                        if($inputElement.prop("checked")){
                            $inputElement.prop('checked', false);

                        }
                        else{
                             $inputElement.prop('checked', true);
                        }

                        if (!$inputElement.prop("disabled")) {
                            filterData($inputElement.attr("id"), "subject", $inputElement.prop("checked"));
                        }
                    });

                    angular.element(".search-resources-main-container").find(".js-type").click(function() {

                        var $inputElement = $(this).find("input");
                        if($inputElement.prop("checked")){
                            $inputElement.prop('checked', false);

                        }
                        else{
                             $inputElement.prop('checked', true);
                        }

                        if (!$inputElement.prop("disabled")) {
                            filterData($inputElement.attr("id"), "type", $inputElement.prop("checked"));
                        }
                    });
                });
            }
        }
        else{
            $scope.resultsCount = 0;
        }

        /* paging logic */
        if ($scope.currentTab == 'article' && $scope.articleList) {
            $scope.totalRecords = $scope.totalArticlesResources;
            $scope.totalPageCount = Math.ceil($scope.totalRecords / perPage);
            $scope.searchItems = gridResults;
            for (var i = 1; i <= $scope.totalPageCount; i++) {
                $scope.pages.push(i);
            }

            if (getParameterByName('astart')) {
                $scope.records.pageNumber = getParameterByName('astart') / 12 + 1;
            } else {
                $scope.records.pageNumber = $scope.pages[0];
            }
        }

        /* paging logic */
        if ($scope.currentTab == 'issue' && $scope.issuesList) {
            $scope.totalRecords = $scope.totalIssues;
            $scope.totalPageCount = Math.ceil($scope.totalRecords / perPage);
            $scope.searchItems = gridResults;
            for (var i = 1; i <= $scope.totalPageCount; i++) {
                $scope.pages.push(i);
            }

            if (getParameterByName('istart')) {
                $scope.records.pageNumber = getParameterByName('istart') / 12 + 1;
            } else {
                $scope.records.pageNumber = $scope.pages[0];
            }
        }

        searchDataLayerUpdate();
    };

    /* function to build searchQuery */
    var search = function() {
        var queryURL = location.search;
        $scope.isSpinnerShow = true;
        searchResourcesDataFactory.getSearchResults(queryURL).then(function(response) {
            searchSuccessCallback(response);
            $scope.isSpinnerShow = false;
            $timeout(function(){
                setURLDefaults();
            });
           
        }, function(response) {
            //handle error
        });

        var setURLDefaults = function(){
           
            var tabName = getParameterByName('tab');

            var searchResultsPagePath = $("#search-results-page-path").val();
            var queryModified = false;

            if (tabName == 'article') {
                $scope.currentTab = 'article';
                $("#articlesResourcesTab").addClass('active');
                $('#articleResourcesIssuesTab').removeClass('active');
                var sortBy = getParameterByName('asort');
                $scope.sortCondition = sortBy;
            } else if (tabName == 'issue') {
                $('#articleResourcesIssuesTab').addClass('active');
                $("#articlesResourcesTab").removeClass('active');
                $scope.currentTab = 'issue';
                var sortBy = getParameterByName('isort');
                $scope.sortCondition = sortBy;
            } else {
                $scope.currentTab = 'article';
                $("#articlesResourcesTab").addClass('active');
                $('#articleResourcesIssuesTab').removeClass('active');
                var sortBy = getParameterByName('asort');
                $scope.sortCondition = sortBy;
            }
            
          if((getParameterByName("text"))!= '' && (getParameterByName("text"))!= null){
              $scope.searchText = decodeURIComponent(getParameterByName("text"));
          }

            var atypes = getParameterByName('atypes');
            if (atypes) {
                angular.forEach(atypes.split(","), function(value, key) {
                    angular.element(".search-resources-main-container").find(".js-type").each(function() {
                        if ($(this).find("input").attr("id") == value) {
                            $(this).find("input").prop('checked', true);
                        }
                    });
                });
            }

            var asubjects = getParameterByName('asubjects');
            if (asubjects) {
                angular.forEach(asubjects.split(","), function(value, key) {
                    angular.element(".search-resources-main-container").find(".js-subject").each(function() {
                        if ($(this).find("input").attr("id") == value) {
                            $(this).find("input").prop('checked', true);
                        }
                    });
                });
            }

            var isubjects = getParameterByName('isubjects');
            if (isubjects) {
                angular.forEach(isubjects.split(","), function(value, key) {
                    angular.element(".search-resources-main-container").find(".js-subject").each(function() {
                        if ($(this).find("input").attr("id") == value) {
                            $(this).find("input").prop('checked', true);
                        }
                    });

                });
            }

        }
    };

    $scope.currentTab = 'article';
    $scope.openTab = function(tabName) {
        $('body').hide();
        $scope.currentTab = tabName;
        if (getParameterByName('tab')) {
            location.href = updateQueryStringParameter(location.href, 'tab', $scope.currentTab);
        } else {
            location.href = location.href + "&tab=" + tabName;
        }
    };
    $scope.records = {};
    $scope.isContentLoaded = false;

    $scope.bindTitle = function(titleText) {
        if($scope.resultsCount === 0 && titleText) {
            var searchText = $scope.searchText;
            return titleText.replace("*****", searchText || '');
        }
    }

    var perPage = 12;
    $scope.issueItems = [];
    $scope.pages = [];
    $scope.totalPageCount;
    var gridResults = [];

    var init = function() {
        search();
    };

    var searchDataLayerUpdate =function(){
        if(angular.element('.search-resources-main-container').length>0){

            dumbleData.search.method='search result';

            if($scope.totalArticlesResources + $scope.totalIssues >0){

                dumbleData.search.results='true';
            }else{

                dumbleData.search.results='false';
            }

            dumbleData.search.sort=$scope.sortCondition;
            dumbleData.search.pagination=$scope.records.pageNumber +" Out of "+$scope.totalPageCount;
            dumbleData.search.keyword=$scope.searchText;
            dumbleData.page.template="search";

            var adumbleDataSubjects = getParameterByName('asubjects');

            if (adumbleDataSubjects) {
            var adumbleDataSubjectsArray=adumbleDataSubjects.split(",");
                angular.forEach(adumbleDataSubjectsArray, function(value, key) {
                    filters.category='subject';
                    filters.value=value;
                    dumbleData.search.filters.push(angular.copy(filters));
                });
            }


            var idumbleDataSubjects = getParameterByName('isubjects');

            if (idumbleDataSubjects) {
                var idumbleDataSubjectsArray=idumbleDataSubjects.split(",");
                angular.forEach(idumbleDataSubjectsArray, function(value, key) {
                    filters.category='subject';
                    filters.value=value;
                    dumbleData.search.filters.push(angular.copy(filters));
                });
            }


            var adumbleDataTypes = getParameterByName('atypes');

            if (adumbleDataTypes) {
                var adumbleDataTypesArray=adumbleDataTypes.split(",");
                angular.forEach(adumbleDataTypesArray, function(value, key) {
                    filters.category='type';
                    filters.value=value;
                    dumbleData.search.filters.push(angular.copy(filters));
                });
            }

           }
        }

    $scope.paginate = function(pageNumber) {
        if ($scope.currentTab === 'article') {
            if (getParameterByName('astart')) {
                location.href = updateQueryStringParameter(location.href, 'astart', (pageNumber - 1) * NO_OF_RECORDS_PER_PAGE);
            } else {
                location.href = location.href + "&astart=" + (pageNumber - 1) * NO_OF_RECORDS_PER_PAGE;
            }
        } else {
            if (getParameterByName('istart')) {
                location.href = updateQueryStringParameter(location.href, 'istart', (pageNumber - 1) * NO_OF_RECORDS_PER_PAGE);
            } else {
                location.href = location.href + "&istart=" + (pageNumber - 1) * NO_OF_RECORDS_PER_PAGE;
            }
        }
    };

    $scope.onpagePrevious = function() {
        if ($scope.records.pageNumber > 1) {
            $scope.records.pageNumber = $scope.records.pageNumber - 1;
            $scope.paginate($scope.records.pageNumber);
        }
    };

    $scope.onpageNext = function() {
        if ($scope.records.pageNumber < $scope.totalPageCount) {
            $scope.records.pageNumber = $scope.records.pageNumber + 1;
            $scope.paginate($scope.records.pageNumber);
        }
    };

    $scope.onPageSelection = function() {
        $scope.paginate($scope.records.pageNumber);
    };

    $scope.sortBy = function(param) {
        if (getParameterByName('text')) {
            $scope.sortCondition = param;
            if ($scope.currentTab === "article") {
                if (getParameterByName('asort')) {
                    location.href = updateQueryStringParameter(location.href, 'asort', param);
                } else {
                    location.href = location.href + "&asort=" + param;
                }
            } else {
                if (getParameterByName('isort')) {
                    location.href = updateQueryStringParameter(location.href, 'isort', param);
                } else {
                    location.href = location.href + "&isort=" + param;
                }
            }
        }
    };

    $scope.clearFilters = function() {
        var urlArr = location.href.split("?");
        var queryArr = [];
        var queryString = '';

        if (urlArr.length > 1) {
            queryArr = urlArr[1].split("&");

            angular.forEach(queryArr, function(value, key) {
                if ($scope.currentTab === 'article') {
                    if (value.indexOf("atypes") === -1 && value.indexOf("asubjects") === -1) {
                        queryString = queryString + "&" + value;
                    }
                } else {
                    if (value.indexOf("isubjects") === -1) {
                        queryString = queryString + "&" + value;
                    }
                }
            });
            queryString = queryString.replace("&", "?");
            location.href = urlArr[0] + queryString;
        }
    };

    init();
}]);
