app.directive('swShowPopover', ["$compile", "$sce", function($compile, $sce) {
    return {
        restrict: 'E',
        scope: {
            linkUrl: '@'
        },
        link: function(scope, element, attrs) {
            element.find('a').popover();
            element.find('a').click(function() {
                $('[data-toggle="popover"]').popover('hide');
                element.find('[data-toggle="popover"]').popover('toggle');
            });

            element.on('click', '.js-share-popover-close', function() {
                element.find('[data-toggle="popover"]').popover('hide');
            });


            element.on('click', '.share-popover-copy', function(event) {
                var target = event.currentTarget;
                copyToClipboard(angular.element(target.parentElement).find('.popover-text')[0]);
            });

            function copyToClipboard(elem) {
                // create hidden text element, if it doesn't already exist
                var targetId = "_hiddenCopyText_";
                var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
                var origSelectionStart, origSelectionEnd;
                if (isInput) {
                    // can just use the original source element for the selection and copy
                    target = elem;
                    origSelectionStart = elem.selectionStart;
                    origSelectionEnd = elem.selectionEnd;
                } else {
                    // must use a temporary form element for the selection and copy
                    target = document.getElementById(targetId);
                    if (!target) {
                        var target = document.createElement("textarea");
                        target.style.position = "absolute";
                        target.style.left = "-9999px";
                        target.style.top = "0";
                        target.id = targetId;
                        document.body.appendChild(target);
                    }
                    target.textContent = elem.textContent;
                }
                // select the content
                var currentFocus = document.activeElement;
                target.focus();
                target.setSelectionRange(0, target.value.length);

                // copy the selection
                var succeed;
                try {
                    succeed = document.execCommand("copy");
                } catch (e) {
                    succeed = false;
                }
                // restore original focus
                if (currentFocus && typeof currentFocus.focus === "function") {
                    currentFocus.focus();
                }

                if (isInput) {
                    // restore prior selection
                    elem.setSelectionRange(origSelectionStart, origSelectionEnd);
                } else {
                    // clear temporary content
                    target.textContent = "";
                }
                return succeed;
            }

            scope.popoverContent = $sce.trustAsHtml("<span class='sw-icon grey-close-btn js-share-popover-close' id='shareLinkPopover'></span><div class='popover-data'>Here’s the link to use in your LMS, " +
                "Google Classroom, or elsewhere</div><input class='popover-text form-control' type='text'  value='" + scope.linkUrl + "' readonly/><span class='sw-icon copy-link-btn share-popover-copy'></span>");
        },
        template: '<a  href="" data-toggle="popover" class="share-popover-layout"  data-content="{{popoverContent}}" data-html="true" data-placement="bottom" data-trigger="manual"><span class="sw-icon upload-icon"></span></a>'
    };
}]);




