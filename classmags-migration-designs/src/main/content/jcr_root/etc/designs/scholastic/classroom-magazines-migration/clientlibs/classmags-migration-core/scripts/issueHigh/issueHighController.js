app.controller("issueHighController", [
    "$scope",
    "$timeout",
    "swUserRole",
    "articleToolbarFactory",
    "bookmarkPaths",
    function($scope, $timeout, swUserRole, articleToolbarFactory, bookmarkPaths) {
        var bookmarkResourcePath,
            userRole,
            appName;
        $scope.pagePaths = [];

        $timeout(function() {
            bookmarkResourcePath = $('#bookmarkSocialPostPath').val();
            appName = $("#core-main-container").attr("data-app-name").toLowerCase();
            returnBookmarkStatus();
        });

        if (swUserRole) {
            userRole = swUserRole.toLowerCase();
        }

        /**
       * Function to handle add bookmark operation
       * @param  {Number} tileIndex [Index of the issue high tile]
       */
        $scope.addBookmark = function(tileIndex) {
            if (userRole === "teacher" || userRole === "student") {
                var type = 'issueHighlight',
                    pagePath = $scope.pagePaths[tileIndex];
                $scope.isSpinnerShow = true;
                $scope.tileIndex = tileIndex;
                if (tileIndex === undefined) {
                    type = 'issue';
                    pagePath = $scope.pagePath;
                }
                articleToolbarFactory.toggleBookmarkByContentType('addbookmark', bookmarkResourcePath, appName, type, pagePath, false).then(handleSuccess, handleError);
            }
        };

        /**
       * Function to handle delete bookmark functionality
       * @param  {Number} tileIndex [Index of the issue high tile]
       */
        $scope.deleteBookmark = function(tileIndex) {
            var type = 'issueHighlight',
                pagePath = $scope.pagePaths[tileIndex];
            $scope.isSpinnerShow = true;
            $scope.tileIndex = tileIndex;
            if (tileIndex === undefined) {
                type = 'issue';
                pagePath = $scope.pagePath;
            }
            articleToolbarFactory.toggleBookmarkByContentType('deletebookmark', bookmarkResourcePath, appName, type, pagePath, false).then(handleSuccess, handleError);
        };

        /**
       * Function to handle server success response
       * @param  {Object} response [server response]
       */
        function handleSuccess(response) {
            var data = response.data;
            if (data && data.response) {
                if ($scope.tileIndex !== undefined) {
                    $scope['isBookmarkAdded' + $scope.tileIndex] = data.response.bookmarkEnabled;
                } else {
                    $scope.isIssueBookmarked = data.response.bookmarkEnabled;
                }
            }
            $scope.isSpinnerShow = false;
        }

        /**
       * Function to handle server failure response
       * @param  {Object} response [server response]
       */
        function handleError(response) {
            //handle error
            $scope.isSpinnerShow = false;
        }

        /**
         * Function to handle the bookmark status
         */
        function returnBookmarkStatus() {
            if (bookmarkPaths) {
                var pagePaths = bookmarkPaths.pagePaths,
                    assetPaths = bookmarkPaths.assetPaths;
                $scope.isIssueBookmarked = pagePaths && pagePaths.indexOf($scope.pagePath) !== -1;

                angular.forEach($scope.pagePaths, function(eachPath, index) {
                    if (assetPaths && assetPaths.indexOf(eachPath) !== -1) {
                        $scope['isBookmarkAdded' + index] = true;
                    }
                });
            }
        }
    }
]);
