app.service('authorizationService', function(){
     var userRoles = {
        EVERYONE: "EVERYONE",
        TEACHER:"TEACHER",
        TEACHER_OR_STUDENT: "TEACHERORSTUDENT",
        STUDENT: "STUDENT",
        ANONYMOUS: "ANONYMOUS"          
    }  
    this.swLoginNavLink="";
    this.verifyAccess = function(role, currentUserRole){         
       if(role && currentUserRole){
           role = role.trim().toUpperCase();
           currentUserRole = currentUserRole.trim().toUpperCase();
           if(role === userRoles.EVERYONE){
               return true;
           }
           if(role === userRoles.ANONYMOUS && currentUserRole == userRoles.ANONYMOUS){
               return true;
           }
           if(role === userRoles.TEACHER || role === userRoles.TEACHER_OR_STUDENT){
               if(currentUserRole === userRoles.TEACHER){
                   return true;
               }
           }
           if(role === userRoles.STUDENT || role === userRoles.TEACHER_OR_STUDENT){
               if(currentUserRole === userRoles.STUDENT){
                   return true;
               }
           }           
       }
       else if(currentUserRole){
           return true;
       }
       else{
           return false;
       }
    } 
});