(function (document, $) {

	"use strict";

    var contentPath = null;

	$(document).on("foundation-contentloaded", function(e){

        //Default content path
        contentPath = $(".foundation-collection").data("foundationCollectionId");
        if (contentPath != undefined && contentPath.charAt(contentPath.length - 1) != '/') {
				contentPath = contentPath + "/";
        }
        if (document.querySelector('.dam-upload-brightcovefile')) {
            var damCreateFolder = new DamCreateFolder().set('uploadBrightCoveVideo', document.querySelector('.dam-upload-brightcovefile'));
            damCreateFolder.initialize();
        }
	});

    $(document).on("foundation-selections-change", function(e) {
        if($(".foundation-selections-item").length == 1) {
            // In columns view we have create folder functionality in selection mode as well.
            contentPath = $(".foundation-selections-item").data("foundation-collection-item-id");
            if (contentPath != undefined && contentPath.charAt(contentPath.length - 1) != '/') {
				contentPath = contentPath + "/";
            }
            if (document.querySelector('.cq-damadmin-admin-actions-createfolder-at-activator')) {
                var damCreateFolder = new DamCreateFolder().set('uploadBrightCoveVideo', document.querySelector('.cq-damadmin-admin-actions-createfolder-at-activator'));
                damCreateFolder.initialize();
            }
        }
    });

	var createDialogEl;

	var DamCreateFolder = new Class({

		uploadBrightCoveVideo: null,
		dialog: null,
		_ILLEGAL_FILENAME_CHARS: "%/\\:*?\"[]|\n\t\r. #{}^;+",

		set: function(prop, value) {
			this[prop] = value;
			return this;
		},

		initialize: function () {
			var self = this;

			var $foundationPath = $('.foundation-content-path');

			if (!self.dialog) {
				self.dialog = self.createDialog();
			}

			Coral.commons.ready(self.uploadBrightCoveVideo, function() {
				self.uploadBrightCoveVideo
				.on("click", function(event){
					self.dialog.show();
				});
			})

		},

		createDialog: function () {
			var self = this;
			var dialogExist = true;
			if (!createDialogEl) {
			    dialogExist = false;
			    createDialogEl = new Coral.Dialog().set({
				id: 'uploadBrightCoveVideo',
				header: {
					innerHTML: Granite.I18n.get("Create a new AEM Video from Brightcove")
				}
			    });
            }

            var dialog = createDialogEl;
            var content = dialog.querySelector('coral-dialog-content');
            if (!dialogExist) {
            // The modal is basically a form
			var contentForm = content.appendChild(document.createElement('form'));
			contentForm.action = contentPath;
			contentForm.method = 'POST';
			contentForm.encType = 'application/x-www-form-urlencoded';

			// Name
			contentForm.appendChild(function(){
				var fieldDiv = document.createElement('div');
				var titleDiv = document.createElement('div');
				titleDiv.innerHTML = Granite.I18n.get("Brightcove Video ID");
				titleDiv.className += ' text';
				var input = new Coral.Textfield().set({
					name: ':brightcoveVideoID'
				}).on('keypress', function(event){
					var charCode = event.which || event.keyCode;
			        if (self._isRestricted(charCode)) {
			            event.preventDefault();
			        }
				}).on('input', function(event){
				     var name = $(this).get(0);
				     self._validateAndAddTooltip (name, name.value);
				});
				dialog.nameInput = input;
				fieldDiv.appendChild(titleDiv);
				fieldDiv.appendChild(input);
				return fieldDiv;
			}());

			var footer = dialog.querySelector('coral-dialog-footer');
			var cancel = new Coral.Button();
			cancel.label.textContent = Granite.I18n.get('Cancel');
			footer.appendChild(cancel).on('click', function (){
				self._cleanDialog();
				dialog.hide();
			});

			var submitButton = new Coral.Button().set({
				variant: 'primary',
				disabled: true
			}).on('click', function (){
				self._submit();
			});
			submitButton.label.textContent = Granite.I18n.get('Create Video');

			footer.appendChild(submitButton);

			// Few settings to be used on various actions
			dialog.submit = submitButton;
			dialog.isPrivate = false;

            } else {
                 var contentForm = content.childNodes.item(0);
                 contentForm.action = contentPath;
            }

			return dialog;
		},

		_submit: function () {
			var self = this;

			var nameInput = $('input[name=":brightcoveVideoID"]', self.dialog)[0];
			var name = nameInput.value;

			var form = self.dialog.querySelector('form');
			$.ajax({
				type: form.method,
				url: '/bin/classmags/migration/create/brightcove/video',
				contentType: form.encType,
				data: {
                    brightcoveVideoID: name,
                    damPath: form.getAttribute("action")
                  },
				cache: false
			}).done(function (data, textStatus, jqXHR) {
				self._cleanDialog();
				self.dialog.hide();
				self._refresh();
			}).fail(function (jqXHR, textStatus, errorThrown) {

                console.log('after fail call');
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);

                var ui = $(window).adaptTo("foundation-ui");
                ui.alert(Granite.I18n.get("Error"), jqXHR.responseText, "error");

                var errDialog = new Coral.Dialog().set({
                	header: {
                		innerHTML: Granite.I18n.get("Error")
                	},
                	content: {
                		innerHTML: Granite.I18n.get(jqXHR.responseText)
                	}
                });

				var errorDlgOkBtn = new Coral.Button();
				errorDlgOkBtn.variant = 'primary';
				errorDlgOkBtn.label.textContent = Granite.I18n.get('Ok');

                var footer = errDialog.querySelector('coral-dialog-footer');
				console.log('footer');
                console.log(footer);
                footer.appendChild(errorDlgOkBtn).on('click', function (event) {
                	self._cleanDialog();
                	errDialog.hide();
                });
                console.log('errDialog');
                console.log(errDialog);
            });
		},

		_checkExistance: function (name) {
			var self = this;
	        var result = $.ajax({
	            type: 'GET',
	            async: false,
	            url: Granite.HTTP.externalize(contentPath + ".1.json"),
	            cache: false
	        });
	        var folderList = JSON.parse(result.responseText);
	        for (var i in folderList) {
	            if (name === i) {
	                return true;
	            }
	        }
	        return false;
	    },

          _validateAndAddTooltip: function(inputElement, enteredText) {
              var self = this;
              var toDisable = false;
              if (enteredText === "") {
                  toDisable = toDisable || true;
              }
              // Remove the stale tooltips if any
              Array.prototype.slice.call(inputElement.parentElement.getElementsByTagName('coral-tooltip')).forEach(function(item) {
                  item.remove();
              });

              // Do validation and add tooltip if required
              if (self._hasRestrictedChar(enteredText)) {
                  inputElement.parentElement.appendChild(new Coral.Tooltip().set({
                      variant: 'error',
                      content: {
                          innerHTML: Granite.I18n.get("The name must not contain {0}, so replaced by {1}", [self._ILLEGAL_FILENAME_CHARS.toString().replace(/[,]/g, " "), "-"])
                      }
                  })).show();
                  self.dialog.nameInput.value = self._replaceRestrictedCodes(enteredText.toLowerCase()).replace(/ /g, '-');
              } else {
                  self.dialog.nameInput.value = enteredText.toLowerCase().replace(/ /g, '-');
              }


              if (toDisable) {
                  self.dialog.submit.disabled = true;
              } else {
                  self.dialog.submit.disabled = false;
              }
          },
		
		_cleanDialog: function () {
			var self = this;
			$.each(self.dialog.getElementsByTagName('input'), function (cnt, input){
				if (input.type === 'text') {
					input.value = "";
				} else if (input.type === 'checkbox') {
					input.checked = false;
				}
			});
			self.dialog.submit.disabled = true;
		},
		
		_isRestricted: function (code) {
			var self = this;
			var charVal = String.fromCharCode(code);
	        if (self._ILLEGAL_FILENAME_CHARS.indexOf(charVal) > -1 ) {
	            return true;
	        } else {
	            return false;
	        }
		},
		
		_hasRestrictedChar: function (textValue) {
			var self = this;
			for (var i = 0, ln = textValue.length; i < ln; i++) {
	            if (self._isRestricted(textValue.charCodeAt(i))) {
	                return true;
	            }
	        }
			return false;
		},

        _replaceRestrictedCodes: function(name) {
            var self = this;
            var jcrValidName = "";
            for (var i = 0, ln = name.length; i < ln; i++) {
                if (self._isRestricted(name.charCodeAt(i))) {
                    jcrValidName += '-';
                } else {
                    jcrValidName += name[i];
                }
            }
            return jcrValidName;
        },
		
		_refresh: function() {
			var contentApi = $(".foundation-content").adaptTo("foundation-content");
			if(contentApi){
	            contentApi.refresh();
	        }else{
	            location.reload(true);
	        }
		}
		
	});
	
})(document, Granite.$);