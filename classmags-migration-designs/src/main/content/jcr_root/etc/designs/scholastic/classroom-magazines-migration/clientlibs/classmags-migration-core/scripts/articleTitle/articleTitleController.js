app.controller("articleTitleController", ["$scope", function($scope) {
	var articleElements = document.querySelectorAll('.article-title-component');
	angular.forEach(articleElements, function(eachElementObj){
		var titleColor = eachElementObj.getAttribute('data-highlight-color');
		var textColor = eachElementObj.getAttribute('data-font-color');
		var $articleElement = $(eachElementObj);
		if(titleColor){
			$articleElement.find('div,p,ol li,ul li').css({'background-color': titleColor, 'display': 'inline'});
			if(!$articleElement.hasClass('.sub-heading')) {
				$articleElement.css('line-height', 1.50);
			}
		}
		$articleElement.find('div,p,ol li,ul li').css('color', textColor);
	});
}]);