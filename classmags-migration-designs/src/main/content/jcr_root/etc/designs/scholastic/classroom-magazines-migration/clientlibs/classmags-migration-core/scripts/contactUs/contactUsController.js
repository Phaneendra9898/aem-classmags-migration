app.controller("contactUsController", ["$scope", "contactUsFactory", "$timeout", 
	function($scope, contactUsFactory, $timeout ){
	$scope.contactUsData = {};
	var recepient ='';
    $scope.isSuccess = false;
    $scope.isError = false;
    $scope.helpOptions = [];
    $scope.isContactSpinnerShow = false;
    $scope.contactFormHide = false;

	$timeout(function(){
		recepient = $(".contact-form-questions").attr("data-recipient");
	})



	$scope.submitContactForm = function(){
		// if($scope.issueData && $scope.issueData.subTopicTitle)	{
		// 	$scope.contactUsData.issue = $scope.issueData.subTopicTitle;
		// }

		$scope.contactFormHide = true;
		$scope.isContactSpinnerShow = true;
		if($scope.issueData)	{			
			$scope.contactUsData.issue = $scope.issueData;
		}
		else{
			$scope.contactUsData.issue ='';
		}
		
		$scope.contactUsData.recepient = recepient;
		if($scope.contactForm.$valid){
            contactUsFactory.contactFormPost($scope.currentPagePath, $scope.contactUsData)
                .then(contactHandleSuccess, contactHandleError);
        }
	}

	function contactHandleSuccess(response) {
        if(response){        	
			$scope.isSuccess = true;
			$scope.isContactSpinnerShow = false;
			location.href= $scope.pageDirectPath; 
        }
    }

    function contactHandleError(response) {
        $scope.isError = true;
       console.log('error');
    }

	$scope.onSelectOptionsChange = function(value, index){
		$scope.contactUsData.topic = value;
		$scope.issueOptionsDisabled = false;

        for(var i=0; i<$scope.helpOptions.length; i++){
            $scope.helpOptions[i]= false;
        }
        $scope.helpOptions[index]=true;

		contactUsFactory.contactRadiochange($scope.currentRadioPagePath, value)
		.then(function(response){
			$scope.dropdownOptions = [];
			$scope.dropdownOptions = response;
		}, function(response){

		});

	}

}]);


	

