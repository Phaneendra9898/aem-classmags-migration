app.directive('swFlexibleTitleHyperlink', function() {
    return {
        restrict: 'E',
        scope: {
            title:'@',
            hyperLinkText: '@',
            hyperLinkUrl: '@',
            recordsCount: '@',
            color: '@',
            titlePosition: '@',
            isRuleExists: '=',
            titleFontSize: '@',
            imageUrl: '@',
            customBackground:'@',
            backgroundColor:'@'
        },
        controller:["$scope","$sce", function($scope, $sce){
		  $scope.title = $sce.trustAsHtml($scope.title);
        }],
        templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/superscience/clientlibs/superscience-core/scripts/swFlexibleTitleHyperlink/swFlexibleTitleHyperlinkPartial.html'
    };
});
