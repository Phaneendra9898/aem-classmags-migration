app.directive('swFlexibleTitleHyperlink', function() {
    return {
        restrict: 'E',
        scope: {
            title:'@',
            hyperLinkText: '@',
            hyperLinkUrl: '@',
            recordsCount: '@',
            color: '@',
            titlePosition: '@',
            isRuleExists: '=',
            titleFontSize: '@',
            imageUrl: '@',
            customBackground:'@',
            backgroundColor: '@',
            connectorColor: '@'
        },
        controller:["$scope","$sce", function($scope, $sce){
		  $scope.title = $sce.trustAsHtml($scope.title);
        }],
        templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/dynamath/clientlibs/dynamath-core/scripts/swFlexibleTitleHyperlink/swFlexibleTitleHyperlinkPartial.html'
    };
});
