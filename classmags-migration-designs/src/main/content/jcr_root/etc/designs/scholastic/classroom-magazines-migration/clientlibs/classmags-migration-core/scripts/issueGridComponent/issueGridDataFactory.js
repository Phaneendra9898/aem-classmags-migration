app.factory('issueGridDataFactory', ["$http", "$q",
function($http, $q) {
  var getGridData = function(params, path){
	  var deferred = $q.defer();
	  $http({
		  url:"/bin/classmags/migration/pastissues?path="+path,
		  method:'GET'
	  }).then(function(response){
		  deferred.resolve(response);
	  }, function(response){
		  deffered.reject(response);
	  });
	  return deferred.promise;
  }
  return {
	  getGridData: getGridData
  }
 
}]);