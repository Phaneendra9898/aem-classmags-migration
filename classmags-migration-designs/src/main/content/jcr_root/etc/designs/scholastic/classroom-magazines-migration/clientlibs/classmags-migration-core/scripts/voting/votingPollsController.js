app.controller("votingPollController", ["$scope", "$sce", "$compile", "$element", "votingFactory", "swUserRole", function($scope, $sce, $compile, $element, votingFactory, swUserRole) {

    $scope.pollshow = false;
    $scope.studentFormError = false;
    $scope.teacherDataFormError = false;
    $scope.showThanksMessage = false;
    $scope.showVoteEnded = false;
    $scope.teacherFormInvalid = false;
    $scope.showResults = false;
    $scope.formServerError = false;

    //common variables and object needed as an input to the request query string
    var currentpagepath = $element.find('.voting-poll-view').data('currentpath'),
        votingDateExpiry = $element.find('.voting-poll-view').data('disablevotingdate'),
        votingquestionId = $element.find('.voting-poll-view').data('questionid');

    var votingData = {

        "questionUUID": votingquestionId,
        "votePagePath": currentpagepath,
        "disableVotingDate": votingDateExpiry

    };
    var votingQueryParams = {};
    var queryURLParam = '';


    //toggling of the results on click on See Rsults button
    $scope.showHidePollResults = function() {
        $scope.pollshow = !$scope.pollshow;
        if ($scope.pollshow) {
            votingDataProcessing("onload");
        }
    }

    //Showing the voting thankyou message after the successful voting
    var votingresponse;
    var votingSuccessCallback = function(response, param) {
        if (response && response.data) {
            votingresponse = response.data.response;

            if ((param === "onsubmit") || (param == 'onload' && !(votingresponse.answerOptionsMap))) {
                $element.find('.spinnercontainer').removeClass('in');
            }

            if (param == 'onload') {
                if (!(votingresponse.votingEnabled)) {
                    $scope.showVoteEnded = true;
                    // $scope.showThanksMessage = false;
                }
            } else {
                $scope.showThanksMessage = true;

            }

            if (param == 'onsubmit') {
                $scope.showResults = true;
            }

            if ((param === "onload") && votingresponse.answerOptionsMap) {
                prepareGraph(votingresponse);
                $scope.showResults = true;

            } else {
                $scope.pollshow = false;
            }
        }
    }

    //Showing the voting exceeded message
    var votingFailureCallback = function(response) {
        if (response && response.data) {

            if (!(response.data.answerOptionsMap)) {
                $element.find('.spinnercontainer').removeClass('in');
            }

            if (response.data["status.code"] == 400 && response.data["status.message"] == "Vote Submission Limit Exceeded") {
                $scope.formServerError = true;
                prepareGraph(votingresponse);
                $scope.showResults = true;
                $scope.pollshow = false;

            } else {
                //non supported error
            }
        }
    }

    //prepare the data input needed for google charts
    var prepareGraph = function(votingresponse) {
        var typeOfGraph = $element.find('.voting-poll-view').data('reporttype');
        var optionArray = votingresponse.answerOptionsMap;
        var votingItem = [];
        var inputColorArray = ['#793e96', '#0099d5', '#ee822c', 'grey', '#006EFF'];

        votingItem.push(['VotingOption', 'Votes', {
            role: 'style'
        }]);

        var k = 0;
        for (var i in optionArray) {
            votingItem.push([i, parseInt(optionArray[i]), inputColorArray[k]]);
            k++;
        }
        drawGraph(typeOfGraph, votingItem, inputColorArray);


    }

    //creating the dynamic table to be displayed beside piechart
    var createTable = function(rowdata, itemcolorarray) {
        var tablecontents = "";
        var totalVotes = 0;
        tablecontents = "<table>";

        tablecontents += "<tr>";
        tablecontents += "<td></td>";
        tablecontents += "<td>" + " # Votes " + "</td>";
        tablecontents += "<td>" + "% of Total" + "</td>";
        tablecontents += "</tr>";

        for (var j = 1; j < rowdata.length; j++) {

            totalVotes = totalVotes + rowdata[j][1];
        }

        var voteInPercentage = 0;
        for (var i = 1; i < rowdata.length; i++) {
            voteInPercentage = (rowdata[i][1] / totalVotes) * 100;
            voteInPercentage = voteInPercentage.toFixed(1);

            tablecontents += "<tr class='content'>";
            tablecontents += "<td><span style='background-color:" + rowdata[i][2] + "'></span>" + rowdata[i][0] + "</td>";
            tablecontents += "<td>" + rowdata[i][1] + "</td>";
            tablecontents += "<td>" + voteInPercentage + " % </td>";
            tablecontents += "</tr>";

        }

        tablecontents += "<tr>";
        tablecontents += "<td>" + "Total Votes" + "</td>";
        tablecontents += "<td>" + totalVotes + "</td>";
        tablecontents += "<td> " + "" + "</td>";
        tablecontents += "</tr>";

        tablecontents += "</table>";
        var $charttableContainer = $element.find('.voting-results-table');
        $charttableContainer.html(tablecontents);

    }

    // drawing charts using google charts API
    var drawGraph = function(typeOfGraph, votingItem, inputColorArray) {
        // Load the Visualization API and the corechart package.
        google.charts.load('current', {
            'packages': ['corechart']
        });

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawChart);

        // Callback that creates and populates a data table,
        // instantiates the pie chart, passes in the data and
        // draws it.

        function drawChart() {

            $element.find('.spinnercontainer').removeClass('in');
            // Create the data table.
            var data = google.visualization.arrayToDataTable(votingItem);


            // Set chart options
            var options = {};

            if (typeOfGraph.toLowerCase() === "piechart") {
                options = {
                    'width': 280,
                    'height': 233,
                    colors: inputColorArray
                };
            } else if (typeOfGraph.toLowerCase() === "bargraph") {
                options = {
                    'width': 610,
                    'height': 233
                };
            }

            var chart;
            // Instantiate and draw our chart, passing in some options.

            var chartContainer = $element.find('.voting-results-chart');

            if (typeOfGraph.toLowerCase() === "bargraph") {
                chart = new google.visualization.ColumnChart(chartContainer[0]);
            } else if (typeOfGraph.toLowerCase() === "piechart") {
                chart = new google.visualization.PieChart(chartContainer[0]);
            }

            if (typeOfGraph.toLowerCase() === "piechart") {
                createTable(votingItem, inputColorArray);
            }

            chart.draw(data, options);

            // hiding the graphics having the colored labels display
            var SVGGraphicElements = $element.find('.voting-results-chart svg g');
            angular.element(SVGGraphicElements[0]).hide();


        }
    }


    //sending the request with differen endpoints based on load or submit
    var votingDataProcessing = function(loadparam) {

        if (loadparam == 'onsubmit') {

            votingQueryParams = {
                params: {
                    ':operation': 'social:voting:submitVote',
                    'voteData': JSON.stringify(votingData)

                }
            };
        } else if (loadparam == 'onload') {
            votingQueryParams = {
                params: {
                    ':operation': 'social:voting:getVoteData',
                    'voteData': JSON.stringify(votingData)
                }
            };

        }
        queryURLParam = currentpagepath + '.social.json?' + $.param(votingQueryParams.params);
        $element.find('.spinnercontainer').addClass('in');

        if (loadparam == 'onsubmit') {
            votingFactory.getVotingResults(queryURLParam).then(function(response) {
                votingSuccessCallback(response, "onsubmit");

            }, function(response) {
                //handle error
                votingFailureCallback(response);
            });

        } else if (loadparam == 'onload') {
            votingFactory.getVotingResults(queryURLParam).then(function(response) {
                votingSuccessCallback(response, "onload");

            }, function(response) {
                //handle error
            });

        }

    }

    votingDataProcessing("onload");

    // errors to be not shown while the user is retying the data

    $scope.removeErrors = function(formName) {
        $scope.studentFormError = false;
        $scope.teacherFormInvalid = false;
        $scope.teacherDataFormError = false;
    }


    //validating -Error Case: throwing error respectively for student or teacher 
    // No Error Case : calling the request processing funtion

    $scope.validatePollData = function(formName) {
        if (formName == "studentDataForm") {

            var answerStudentOptionArray = $element.find('.student-view-answer .label-text');

            $scope.studentFormError = true;
            answerStudentOptionArray.each(function() {

                if (($(this).find('input').is(':checked'))) {
                    $scope.studentFormError = false;
                }

            });

            if (!($scope.studentFormError)) {

                var answerStudentOptionArray = $element.find('.student-view-answer .label-text');

                votingData.answerOptions = {};
                answerStudentOptionArray.each(function() {
                    var answerOption = $(this).find('.option-text').text().trim();


                    if ($(this).find('input').is(':checked')) {
                        votingData.answerOptions[answerOption] = 1;
                    } else {
                        votingData.answerOptions[answerOption] = 0;
                    }

                });

                votingDataProcessing("onsubmit");

            }

        } else if (formName == "teacherDataForm") {
            var inputArr = $element.find('form[name="teacherDataForm"] input[type="text"]');
            var totalSumTeacherVotes = 0;

            $scope.teacherFormInvalid = true;
            angular.forEach(inputArr, function(value, key) {
                var currentElement = $element.find(value).val();
                if (currentElement >= 0 && currentElement != '') {
                    $scope.teacherFormInvalid = false;

                }
                if (currentElement != '') {
                    totalSumTeacherVotes = totalSumTeacherVotes + parseInt(currentElement);
                }


            });

            if (totalSumTeacherVotes > 40 && !($scope.teacherFormInvalid)) {
                $scope.teacherDataFormError = true;

            }

            if (!($scope.teacherDataFormError) && !($scope.teacherFormInvalid)) {
                var answerTeacherOptionArray = $element.find('.teacher-view-answer .label-text');

                votingData.answerOptions = {};
                answerTeacherOptionArray.each(function() {
                    var answerOption = $(this).find('.option-text').text().trim();

                    var teacherinputVal = $(this).find('input').val().trim();

                    if (teacherinputVal == undefined || teacherinputVal == '') {
                        votingData.answerOptions[answerOption] = 0;
                    } else {
                        votingData.answerOptions[answerOption] = teacherinputVal;
                    }

                });

                votingDataProcessing("onsubmit");
            }
        }
    }
}]);