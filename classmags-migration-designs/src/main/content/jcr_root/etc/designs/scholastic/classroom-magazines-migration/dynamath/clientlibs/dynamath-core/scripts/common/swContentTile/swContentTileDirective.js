app.directive('swContentTile', [
    '$timeout',
    'bookmarkPaths',
    'swUserRole',
    function($timeout, bookmarkPaths, swUserRole) {
        return {
            restrict: 'E',
            scope: {
                imageSrc: "@",
                imageHyperlink: '@',
                contentTitle: '@',
                displayDate: '@',
                subject: '@',
                description: '@',
                contentType: '@',
                position: '@',
                videoId: '@',
                videoLength: '@',
                styleType: '@',
                showDownloadIcon: '@',
                showShareIcon: '@',
                showViewArticleLink: '@',
                contentDate: '@',
                bookmarkTitle: '@',
                bookmarkDate: '@',
                bookmarkType: '@',
                userRole: '@',
                viewArticleLinkUrl: '@',
                shareLinkUrl: '@',
                displayStyle: '@',
                articleLinkText: '@',
                gameId: '@',
                gamePath: '@',
                gameFlashContainer: '@',
                gameType: '@',
                getBookmarkPaths: '&?',
                bookmarkPath: '@',
                slideshowId: '@',
                slideshowdata: '@',
                slideshowCoverImage: '@',
                slideshowDisableNumber: '@',
                slideshowNumberColor: '@'
            },
            link: function(scope, elem, attrs) {                
                if(scope.contentType === "Polls" 
                    || scope.contentType === "polls" || scope.contentType === "blog" 
                    || scope.contentType === "Blog" || scope.contentType === "quiz" 
                    || scope.contentType === "Quiz"){
                    scope.contentType = "article";
                } 

                scope.showDownload = scope.showDownloadIcon === 'true';
                scope.showShare = scope.showShareIcon === 'true';
                scope.showView = scope.showViewArticleLink === 'true';
                scope.role = scope.userRole || 'everyone';
                if (scope.contentType === 'lessonPlan' || scope.contentType === 'Lesson Plans') {
                    scope.articleLinkText = 'Download Lesson Plan';
                } else if (scope.contentType === 'skillsSheet' || scope.contentType === 'Skills Sheets') {
                    scope.articleLinkText = 'Download Skills Sheets';
                } else if (scope.contentType === 'slideshow') {
                    scope.articleLinkText = 'Launch Slideshow';
                } else if (scope.contentType === 'game') {
                    scope.articleLinkText = 'Download Game';
                } else if (scope.contentType === 'activity') {
                    scope.articleLinkText = 'Download Activity';
                } else if (scope.contentType === 'video') {
                    scope.articleLinkText = 'Watch Video';
                } else if (scope.contentType === 'article') {
                    scope.articleLinkText = 'View Article';
                } else if (scope.contentType === 'Blogs' || scope.contentType === 'blog') {
                    scope.articleLinkText = 'View Blog';
                } else {
                    scope.articleLinkText = 'View Article';
                }

                scope.openGamePopup = function($event) {
                    $timeout(function() {
                        var $clickedElement = angular.element($event.currentTarget);
                        var $triggerElement = $clickedElement.closest('.content-tile-body').find('.content-card-link');
                        angular.element($triggerElement).trigger('click');

                    });
                }
            },
            templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/dynamath/clientlibs/dynamath-core/scripts/common/swContentTile/contentTilesPartial.html',
            controller: ["$scope", "articleToolbarFactory", "bookmarkPaths",
                function contentTileController($scope, articleToolbarFactory, bookmarkPaths) {
                var bookmarkResourcePath = $('#bookmarkSocialPostPath').val(),
                    appName,
                    userRole;

                if ($scope.displayStyle === 'bookmark') {
                    $scope.bookmarkPaths = $scope.getBookmarkPaths();
                    appName = $scope.bookmarkTitle.replace(/\s/g, '').toLowerCase();
                    if (appName === 'juniorscholastic') {
                        appName = 'junior';
                    }
                } else {
                    $scope.bookmarkPaths = (bookmarkPaths.pagePaths || []).concat(bookmarkPaths.assetPaths || []);
                    appName = $("#core-main-container").attr("data-app-name").toLowerCase();
                }
                $scope.isBookmarkAdded = returnBookmarkStatus();

                if (swUserRole) {
                    userRole = swUserRole.toLowerCase();
                }

                $scope.addBookmark = function() {
                    if (userRole === "teacher" || userRole === "student") {
                        $scope.isSpinnerShow = true;
                        articleToolbarFactory.toggleBookmarkByContentType('addbookmark', bookmarkResourcePath, appName, $scope.contentType, $scope.bookmarkPath, $scope.showView, $scope.viewArticleLinkUrl).then(handleSuccess, handleError);
                    }
                };

                $scope.deleteBookmark = function() {
                    $scope.isSpinnerShow = true;
                    articleToolbarFactory.toggleBookmarkByContentType('deletebookmark', bookmarkResourcePath, appName, $scope.contentType, $scope.bookmarkPath).then(handleSuccess, handleError);
                };

                function handleSuccess(response) {
                    var data = response.data;
                    if (data && data.response) {
                        $scope.isBookmarkAdded = data.response.bookmarkEnabled;
                        if ($scope.getBookmarkPaths) {
                            if ($scope.isBookmarkAdded) {
                                $scope.getBookmarkPaths({path: $scope.bookmarkPath, operation: 'add'});
                            } else {
                                $scope.getBookmarkPaths({path: $scope.bookmarkPath, operation: 'delete'});
                            }
                        }
                    }
                    $scope.isSpinnerShow = false;
                }

                function handleError(response) {
                    //handle error
                    $scope.isSpinnerShow = false;
                }

                function returnBookmarkStatus() {
                    return $scope.bookmarkPaths.indexOf($scope.bookmarkPath) !== -1;
                }
            }]
        };
    }
]);
