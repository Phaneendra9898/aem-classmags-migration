app.factory('contentHubDataFactory', ["$http", "$q",
function($http, $q) {

  var getGridData = function(params){
	  var deferred = $q.defer();
	  $http({
		  url:"sample.json",
		  method:'GET'
	  }).then(function(response){
		  var results = response;	
		  deferred.resolve(response);
	  }, function(response){
		  deffered.reject(response);
	  });	  
	  return deferred.promise;
  }
  
  var getSearchResults = function(query){
      var searchURL = "/bin/classmags/migration/contenthub";
      var deferred = $q.defer();
      searchURL = searchURL+query;
      $http({
            url: searchURL,
            method: 'GET'
      }).then(function(response){
            deferred.resolve(response);
      }, function(response){
            deferred.reject(response);
      });
      return deferred.promise;   
  }
  
  return {
	  getGridData: getGridData,
      getSearchResults: getSearchResults
  }
 
}]);