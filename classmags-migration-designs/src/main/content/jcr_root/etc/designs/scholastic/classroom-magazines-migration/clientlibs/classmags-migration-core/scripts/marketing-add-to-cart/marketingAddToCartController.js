'use strict';

app.controller('marketingAddToCartController', [
    '$scope',
    '$timeout',
    function($scope, $timeout) {
        initialize();

        /**
     * Function to initialize the scope properties and functions.
     */
        function initialize() {
            $scope.closePopover = closePopover;
        }

        function closePopover(event) {
            angular.element(event.currentTarget).closest('.popover').prev('.subscription-magazine-learn-link').trigger('click');
        }

    }
]);
