app.directive('swTabComponent', function() {
    return {
        restrict: 'E',
        scope: {
            tabs: "="
        },
        link: function(scope, elem, attrs) {
            if (scope.tabs.length > 0) {
                setTimeout(function() {
                    $(".main-tabs li:first").addClass("active");
                    $(".main-tab-content div:first").addClass("active in");
                }, 100);
            }

            angular.forEach(scope.tabs, function(value, key) {
                scope.tabs[key].id = scope.tabs[key].name.split(' ').join('').toLowerCase() + key;
            });

        },
        templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/clientlibs/classmags-migration-core/scripts/tab-component/tab-component-partial.html'
    }
});
