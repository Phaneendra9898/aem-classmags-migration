var SDMConfig = (function() {
var sdm_cookie_name = "sdm_nav_ctx";
var sdm_auth_cookie ="sw_auth_cookie";

function notifyLogout() {
    var theCookie = new Cookie();
    var authpcode = $("#core-main-container").attr("data-app-name").toLowerCase();
    theCookie.DeleteCookie(sdm_cookie_name, "/", ".scholastic.com");
    theCookie.DeleteCookie(sdm_auth_cookie,"/", ".scholastic.com");
    window.location.href = $("#sdm-logout").attr("js-data-core-sdm-logout");
    return false;
}

function notifyStillActive() {
}

return {
    cookieName: sdm_cookie_name,
    notifyLogOut: notifyLogout,
    notifyStillActive: notifyStillActive,
    numSecondsBetweenStillActiveNotifications: 600
};
}());
