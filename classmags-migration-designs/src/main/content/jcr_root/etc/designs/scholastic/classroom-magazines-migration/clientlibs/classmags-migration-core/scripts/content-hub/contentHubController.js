app.controller("contentHubController", ["$scope", "$http", "contentHubDataFactory", "$timeout","$rootScope", function($scope, $http, contentHubDataFactory, $timeout,$rootScope) {

    $rootScope.$on("CONTENT_HUB", function(event, data){
    var searchURL = '';
    var filterBy = '';
    var NO_OF_RECORDS_PER_PAGE = 12;
    var contenthubQueryParams = {};
    var perPage = 12;
	$scope.sortCondition = 'published';
    $scope.pages = [];
    $scope.records={};


	var defaultUncheck = function(){
        if(contenthubQueryParams.subjects!= undefined && contenthubQueryParams.subjects!= null){
          var subjectArray=contenthubQueryParams.subjects.split(',');

          for(var i=0; i<subjectArray.length; i++){

                angular.element(".content-hub-main-container .js-subject").each(function() {
                        var $subjElement = $(this);
                        var $subTagElement=$(this).find('span').text();

                        if(subjectArray[i].trim().toLowerCase() == $subTagElement.trim().toLowerCase()){
                            $subjElement.find('input').attr("disabled", true);
                            $subjElement.hide();
                        }
                });
            }
        }

        if(contenthubQueryParams.contentTypes!= undefined && contenthubQueryParams.contentTypes!= null){
          var contentTypeArray=contenthubQueryParams.contentTypes.split(',');


          for(var i=0; i<contentTypeArray.length; i++){

                angular.element(".content-hub-main-container .js-type").each(function() {
                        var $subjElement = $(this);
                        var $subTagElement=$(this).find('span').text();

                        if(contentTypeArray[i].trim().toLowerCase() == $subTagElement.trim().toLowerCase()){
                            $subjElement.find('input').attr("disabled", true);
                            $subjElement.hide();
                        }
                });
            }
        }
	}

    var populateFilters = function(data) {
        if (data && data.facets && data.facets.myHashMap) {
            if (data.facets.myHashMap.subject_tags_ss) {
                var subjectTags = data.facets.myHashMap.subject_tags_ss.myArrayList;
                var matchFound = false;

                angular.element(".content-hub-main-container").find(".js-subject").each(function() {
                    var $element = $(this);
                    matchFound = false;
                    var $elementSubjectTag = $element.find("label .checkbox-text");
                    var subjectTag = $elementSubjectTag.text();
                    if (subjectTag.indexOf("(") > -1) {
                        $elementSubjectTag.text(subjectTag.split("(")[0].trim());
                    }
                    angular.forEach(subjectTags, function(value, key) {
                        if ($element.find("label .checkbox-text").text().trim().toLowerCase() == value.myHashMap.value.toLowerCase()) {
                            $element.find("label .checkbox-text").text($element.find("label .checkbox-text").text() + " (" + value.myHashMap.count + ")");
                            matchFound = true;
                            $element.find('input').attr("disabled", false);
                            $element.find('input').attr("checked", false);
                            $element.find('label, .sw-icon').removeClass("checkbox-checked-disabled");
                            return;
                        }
                    });
                    if (!matchFound) {
                        $element.find('input').attr("checked", true);
                        $element.find('input').attr("disabled", true);
                        $element.find('label, .sw-icon').addClass("checkbox-checked-disabled");
                        $element.find("label .checkbox-text").text($element.find("label .checkbox-text").text() + " (0)");
                    }

                });
            }

            if (data.facets.myHashMap.type_s) {
                var typeTags = data.facets.myHashMap.type_s.myArrayList;
                var matchFound = false;
                angular.element(".content-hub-main-container").find(".js-type").each(function() {
                    var $element = $(this);
                    matchFound = false;
                    var $elementTypeTag = $element.find("label .checkbox-text");
                    var typeTag = $elementTypeTag.text();
                    if (typeTag.indexOf("(") > -1) {

                        $elementTypeTag.text(typeTag.split("(")[0].trim());

                    }
                    angular.forEach(typeTags, function(value, key) {
                         if ($element.find("label .checkbox-text").text().trim().toLowerCase() == value.myHashMap.value.toLowerCase()) {
                            $element.find("label .checkbox-text").text($element.find("label .checkbox-text").text() + " (" + value.myHashMap.count + ")");
                            matchFound = true;
                            $element.find('input').attr("disabled", false);
                            $element.find('input').attr("checked", false);
                            $element.find('label, .sw-icon').removeClass("checkbox-checked-disabled");
                            return;
                        }
                    });
                    if (!matchFound) {
                        $element.find('input').attr("checked", true);
                        $element.find('input').attr("disabled", true);
                        $element.find('label, .sw-icon').addClass("checkbox-checked-disabled");
                        $element.find("label .checkbox-text").text($element.find("label .checkbox-text").text() + " (0)");
                    }
                });
            }
        }
    }

    var removeItem = function(text, val){
        var updatedString ="";

        var textArray = text.split(",");
        for(var i=0; i<textArray.length; i++){
            if(textArray[i] != val){
                updatedString = updatedString + textArray[i]+",";
            }
        }

        if(updatedString.charAt((updatedString.length)-1) == ','){
        updatedString = updatedString.substring(0, updatedString.length-1);

        }
        return updatedString;

    }

    var filterData = function(id, type, isChecked){
        if(type === "subject"){
            if(isChecked){
                if(contenthubQueryParams.subjectFilters.length>0){
                    if(contenthubQueryParams.subjectFilters.indexOf(id) == -1){
                 		contenthubQueryParams.subjectFilters = contenthubQueryParams.subjectFilters +","+id;
                    }
                }
                else{
					contenthubQueryParams.subjectFilters = id;
                }

            }
            else{
				contenthubQueryParams.subjectFilters = removeItem(contenthubQueryParams.subjectFilters, id);
        	}
        }
        else{
        	   if(isChecked){
                   if(contenthubQueryParams.contentTypes.length>0){
                       if(contenthubQueryParams.contentTypes.indexOf(id) == -1){
                    		contenthubQueryParams.contentTypes = contenthubQueryParams.contentTypes +","+id;
                       }
                   }
                   else{
   					contenthubQueryParams.contentTypes = id;
                   }

               }
               else{
   				contenthubQueryParams.contentTypes = removeItem(contenthubQueryParams.contentTypes, id);
           	}

        }

 	formQuery(contenthubQueryParams);

    }


    var contentHubSuccessCallback = function(response) {
        if (response && response.data && Object.keys(response.data).length > 0) {
            var responseData = response.data

            if (response.data.type == "contenthub") {

                $scope.contentHubList = responseData.solrDocumentList;
                $scope.totalContentHubRecords = responseData.total;
                populateFilters(responseData);
          }

        }


         /* paging logic */
        if($scope.contentHubList){

            $scope.totalRecords = $scope.totalContentHubRecords;
            $scope.totalPageCount = Math.ceil($scope.totalRecords / perPage);
            $scope.pages = [];
            for (var i = 1; i <= $scope.totalPageCount; i++) {
                $scope.pages.push(i);
            }


            if( $scope.totalPageCount > 1){
                $scope.records.pageNumber = contenthubQueryParams.start/12+1;
            }
            else{
            $scope.records.pageNumber = $scope.pages[0];
            }


        }

    }



    $timeout(function(){

        angular.element(".content-hub-main-container .js-subject").click(function(){

            contenthubQueryParams.start=0;
            var $inputElement = $(this).find("input");
            if(!$inputElement.prop("disabled") && !$inputElement.prop("checked")){
                $inputElement.prop("checked",true);
                filterData($inputElement.attr("id"), "subject", $inputElement.prop("checked"));
            }else{
                $inputElement.prop("checked",false);
                filterData($inputElement.attr("id"), "subject", $inputElement.prop("checked"));
            }
        });

        angular.element(".content-hub-main-container .js-type").click(function(){

             contenthubQueryParams.start=0;
            var $inputElement = $(this).find("input");
            if(!$inputElement.prop("disabled") && !$inputElement.prop("checked")){
                $inputElement.prop("checked",true);
                filterData($inputElement.attr("id"), "type", $inputElement.prop("checked"));
            }else{
                $inputElement.prop("checked",false);
                filterData($inputElement.attr("id"), "type", $inputElement.prop("checked"));
            }
        });
    });



    $scope.paginate = function(pageNumber) {

		contenthubQueryParams.start=(pageNumber-1)*NO_OF_RECORDS_PER_PAGE;
        formQuery(contenthubQueryParams);

    };

    $scope.onpagePrevious = function() {
        if ($scope.records.pageNumber > 1) {
            $scope.records.pageNumber = $scope.records.pageNumber - 1;
            $scope.paginate($scope.records.pageNumber);
        }
    }
    $scope.onpageNext = function() {
        if ($scope.records.pageNumber < $scope.totalPageCount) {
            $scope.records.pageNumber = $scope.records.pageNumber + 1;
            $scope.paginate($scope.records.pageNumber);
        }
    }
    $scope.onPageSelection = function() {
        $scope.paginate($scope.records.pageNumber);
    }




    $scope.sortBy = function(param) {

        $scope.sortCondition = param;
        contenthubQueryParams.sort = $scope.sortCondition;
        formQuery(contenthubQueryParams);
    }



    var init = function() {

        var contentHubPagePath = $("#content-hub-page-path").val();
        var contentHubContentTypes = $("#content-hub-types").val();
        var contentHubSubjects = $("#content-hub-subjects").val();
        var contentHubSubjectOperator = $("#content-hub-operator").val();
        var contentHubSubjectsUpdated='';

        contenthubQueryParams = {
            contentTypes: contentHubContentTypes,
            subjects: contentHubSubjects,
            subjectFilters:contentHubSubjectsUpdated,
            path: contentHubPagePath,
            subjectOperator: contentHubSubjectOperator,
            sort: 'published',
            start: 0



        };

        formQuery(contenthubQueryParams);
        $scope.sortCondition = 'published';
		defaultUncheck();

    };

    var formQuery = function(contenthubQueryParams) {

        var queryString = '';

        if(value && value.indexOf("&")>-1){
            value = value.replace(/&/g,"%26");
        }

        $.each(contenthubQueryParams, function(key, value) {

            if (key == 'contentTypes') {
                queryString = queryString + '?' + key + '=' + $.trim(value);
            } else {
                queryString = queryString + '&' + key + '=' + $.trim(value);
            }
        });

        $scope.isSpinnerShow = true;
        contentHubDataFactory.getSearchResults(queryString).then(function(response) {
            contentHubSuccessCallback(response);
            $scope.isSpinnerShow = false;
        }, function(response) {
            //handle error
        });


    }

    $scope.clearFilters = function() {
    	  angular.element(".content-hub-main-container").find(".js-subject").each(function() {

              var $inputElement = $(this).find("input");
              if(!$inputElement.prop("disabled") && $inputElement.prop("checked")){
                  $inputElement.prop("checked",false);
              }
          });

          angular.element(".content-hub-main-container").find(".js-type").each(function() {

              var $inputElement = $(this).find("input");
              if(!$inputElement.prop("disabled") && $inputElement.prop("checked")){
                  $inputElement.prop("checked",false);
              }
          });
          init();
    }

    init();
    });

}]);
