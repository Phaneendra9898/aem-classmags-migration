// BEGIN cookiemanager.JS

      var kPreferenceEmail = "EM";
      var kPreferencePlugins = "PL";
      var kPreferenceCustomLogo = "CL";
      var kPreferenceAudio = "AU";
      var kPreferenceFrameless = "FL";
      var kPreferenceAdvertising = "AD";
      var kPreferenceToggle = "TO";
      var kPreferenceCurrentHome = "GH";
      var kPreferenceAppToggle = "ATO";
      var kPreferenceAppCurrentHome = "AGH";
      var kPreferencePrintFullArticle = "PFA";

      var kPREFSCookie = "prefs";
      var nameValueSeparator = ">";
      var CookieSeparator = "|";
      var MAX_COOKIE_LENGTH  = "3950";
      
      var kPreferenceValueKids = "K";
      var kPreferenceValueClassic = "C";
      var kPreferenceValuePassport = "P";
      var kPreferenceValueYes = "Y";
      var kPreferenceValueNo = "N";
      var kGOLDomain = ".digital.scholastic.com";
      var myCookieName; 

function cookiemanager(cookieName) {
  var myCookieName = kPREFSCookie;

  if (arguments.length == 1) {
    myCookieName = cookieName;
    
  }
  
      /* STATIC VARIABLES */

      /* Store all of the current cookies into array so we can access them. */
      var cookie        = "";
      var index         = 0;
      var tmpcookie     = "";
      var kDefaultPath ="/";

      this.clear = function() {
            DeleteCookie(myCookieName, kDefaultPath, kGOLDomain); 
      }

      this.stripTrailingPipe = function(cookieString) {
            if (cookieString.charAt(cookieString.length - 1) == CookieSeparator) {
                  cookieString = cookieString.substr(0, cookieString.length - 1);
            }     
            return cookieString;
      }     
      
      /* This function is deprecated, here for backwards compatibility */
      this.getSinglePrefValue = function(keyName) {
        return this.getSingleValue(keyName);
      }


this.getSingleValue = function(keyName) {
      /* Specify a single key and I shall return you null or a value. */
      nullValue = null;

      var existingCookieString = theCookie.GetCookie(myCookieName);
      
      if (existingCookieString == null || keyName == null) {
            return nullValue; 
      }     
      
      existingCookieString = this.stripTrailingPipe(existingCookieString);
      /* Get the entire array and split them by individual setting, then key/val */
      settingsArray = existingCookieString.split(CookieSeparator);
      var settingSize = settingsArray.length;
      for (index = 0; index < settingSize; index++) {
            thisSetting = settingsArray[index].split(nameValueSeparator);
            if (thisSetting[0] == keyName) {
                  return thisSetting[1];  
            }
      }
      return nullValue;
}     

//BS hack
this.setPassword = function(yourValue) {
      theCookie.SetCookieNoEscape(myCookieName, yourValue, null, kDefaultPath, kGOLDomain);      
}


this.set = function(yourKey, yourValue) {
      
      concatString = yourKey + nameValueSeparator + yourValue + CookieSeparator;
      //alert(concatString);
      var existingCookieString = theCookie.GetCookie(myCookieName);
      //alert(existingCookieString);
      var cookieToSet = "";
      var settingSize;
      var index = 0;
      
      //alert(existingCookieString);
      
      
      if (existingCookieString != null)
      {
            /* Get the string passed in, look for the key */
            prefToSet = concatString.substr(0, concatString.indexOf(nameValueSeparator));
            
            existingCookieString = this.stripTrailingPipe(existingCookieString);
            /* Get the entire settings array and split them by individual setting, then key/val */
            settingsArray = existingCookieString.split(CookieSeparator);
            
            settingSize = settingsArray.length;
            var keyFound = -1;
            for (index = 0; index< settingSize && keyFound == -1; index++) {
                  /* split to look for this setting's key/value */
                  thisSetting = settingsArray[index].split(nameValueSeparator);
                  
                  if(thisSetting[0] != null) {
                        /* if this preference is the one we're looking to set, discard the old value */
                        if (thisSetting[0] == prefToSet) {
                              keyFound = index;
                        }                 
                  }
                  
            }
            
            if (keyFound == -1) {
                  cookieToSet = existingCookieString + CookieSeparator + concatString;      
            }
            else {
                  for (index = 0; index < settingSize; index ++) {
                        if (index == keyFound) {
                              cookieToSet = cookieToSet + concatString; 
                        }     
                        else {
                              cookieToSet = cookieToSet + settingsArray[index] + CookieSeparator;
                        }     
                  }
            }
            theCookie.SetCookieNoEscape(myCookieName, cookieToSet, null, kDefaultPath, kGOLDomain);                  
            
      }
      else
      {
            theCookie.SetCookieNoEscape(myCookieName, concatString, null, kDefaultPath, kGOLDomain);      
      }
            
      
}

}


// END cookiemanager.js

