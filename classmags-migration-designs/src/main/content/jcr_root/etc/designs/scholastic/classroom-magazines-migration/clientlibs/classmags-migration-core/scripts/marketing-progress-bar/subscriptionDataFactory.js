'use strict';

app.factory('subscriptionDataFactory', [
    "$http",
    "$q",
    function($http, $q) {
        var submitSubscriptionData = function(scope, subscriptionPath) {
            var deferred = $q.defer();
            var data = {
                    params: {
                        ':operation': 'social:order:createOrder',
                        'subscriptionData': JSON.stringify(scope.subscriptionData)
                    }
                },
            path = subscriptionPath;
            var query = "";
			for (key in data.params) {
    			query += encodeURIComponent(key)+"="+encodeURIComponent(data.params[key])+"&";
			} 

            $http.post(path, query,{ headers: {'Content-Type': 'application/x-www-form-urlencoded'},}).then(function(response) {
                deferred.resolve(response);
            }, function(response) {
                deferred.reject(response);
            });

            return deferred.promise;
        };

        return {submitSubscriptionData: submitSubscriptionData};
    }
]);
