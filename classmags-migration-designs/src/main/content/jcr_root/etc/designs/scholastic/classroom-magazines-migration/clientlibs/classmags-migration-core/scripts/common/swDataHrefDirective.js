app.directive('swDataHref', function() {
   return {
       restrict: 'A',
       link: function (scope, element, attrs) {
               element.css({"cursor": "pointer"});
               if(!attrs.swAuthorize){
                   element.click(function(){
                       if(attrs.swDataHref){
                            location.href = attrs.swDataHref;
                        }
                   });
               }
        }
   }
});

