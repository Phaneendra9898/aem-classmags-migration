app.directive('swImage', function() {
    return {
        restrict: 'E',
        scope: {
            src:'@',
            alt: '@',
            height: '@',
            width: '@',
            imageClass: '@',
            modalToggle : '@',
            modalTarget : '@',
            popUpImageConfigured: '@',
            imagePopupClass: '@'
        },
        templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/clientlibs/classmags-migration-core/scripts/common/swImage/swImagePartial.html'
    }
});
