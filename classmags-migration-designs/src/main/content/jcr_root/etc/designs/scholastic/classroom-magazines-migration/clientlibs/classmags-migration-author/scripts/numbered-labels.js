(function () {
    var DATA_EAEM_NESTED = "data-eaem-nested";
    var DATA_MAX_ITEMS = "data-maxItems";
    var DATA_MIN_ITEMS = "data-minItems";
    var CFFW = ".coral-Form-fieldwrapper";

    function setSelect($field, value){
        var select = $field.closest(".coral-Select").data("select");

        if(select){
            select.setValue(value);
        }
    }

    function setCheckBox($field, value){
        $field.prop( "checked", $field.attr("value") == value);
    }

    //reads multifield data from server, creates the nested composite multifields and fills them
    function addDataInFields() {
        function getMultiFieldNames($multifields){
            var mNames = {}, mName;

            $multifields.each(function (i, multifield) {
                mName = $(multifield).children("[name$='@Delete']").attr("name");

                mName = mName.substring(0, mName.indexOf("@"));

                mName = mName.substring(2);

                mNames[mName] = $(multifield);
            });

            return mNames;
        }

        function buildMultiField(data, $multifield, mName){
            if(_.isEmpty(mName) || _.isEmpty(data)){
                return;
            }

            _.each(data, function(value, key){
                if(key == "jcr:primaryType"){
                    return;
                }

                $multifield.find(".js-coral-Multifield-add").click();

                _.each(value, function(fValue, fKey){
                    if(fKey == "jcr:primaryType"){
                        return;
                    }

                    var $field = $multifield.find("[name='./" + fKey + "']").last(),
                        type = $field.prop("type");

                    if(_.isEmpty($field)){
                        return;
                    }

                    //handle single selection dropdown
                    if( type == "select-one"){
                        setSelect($field, fValue);
                    }else if( type == "checkbox"){
                        setCheckBox($field, fValue);
                    }else{
                        $field.val(fValue);
                    }
                });
            });
        }

        $(document).on("dialog-ready", function() {
            var componentName = $("h2.coral-Heading").text();
            var tabName = $('.cq-dialog-content').find('.coral-TabPanel-tab.is-active').text();
    		if(componentName.indexOf('Article Configuration')> -1 && tabName.indexOf('Article Settings') > -1){ 
                var $element= $(".custom-multifield-label");
                var READ_LEVEL_LABEL="Reading Level";
                var reOrderMultiFieldLabels = function(){
                	$element.find(".coral-Form-fieldlabel").each(function(index, element){
                        $(element)[0].innerHTML = READ_LEVEL_LABEL+" "+(index+1);
                    });
                }
                $element.on("click", ".js-coral-Multifield-add", function (e) {
                    reOrderMultiFieldLabels();
                });
                
                $element.on("click", ".js-coral-Multifield-remove", function (e) {
                    reOrderMultiFieldLabels();
                });
                
                $element.on("dragend", function (e) { 
                    reOrderMultiFieldLabels();
                });
            }

            var $multifields = $("[" + DATA_EAEM_NESTED + "]");
			$(".coral-Multifield-add").click(function(){
                var currentNumItems = ($(this).prev()).children().length;
                var maxItems = $(this).parent().attr(DATA_MAX_ITEMS);
                if(typeof currentNumItems != "undefined" && typeof maxItems != "undefined"){
                    if(currentNumItems == maxItems-1){
                        $(this).hide();
                    }
                }
            });            
            if(_.isEmpty($multifields)){
                return;
            }

            var mNames = getMultiFieldNames($multifields),
                $form = $(".cq-dialog"),
                actionUrl = $form.attr("action") + ".infinity.json";

            $.ajax(actionUrl).done(postProcess);

            function postProcess(data){
                _.each(mNames, function($multifield, mName){
                    buildMultiField(data[mName], $multifield, mName);
                });
            }
            removeFieldHandler();
        });
    }

    function removeFieldHandler(){
		$(".coral-Multifield-list").delegate(".coral-Multifield-remove", "click", function(){
            var maxItems = ((($(this)).parent()).parent()).parent().attr(DATA_MAX_ITEMS);
            var currentNumItems = ($(this).parent().parent().children()).length;
            if(typeof currentNumItems != "undefined" && typeof maxItems != "undefined"){
                if(currentNumItems <= maxItems){            
                    (($(this).parent()).parent()).next().show();
                }
            }
        });
    }

    //collect data from widgets in multifield and POST them to CRX
    function collectDataFromFields(){
        function fillValue($form, fieldSetName, $field, counter){
            var name = $field.attr("name");

            if (!name) {
                return;
            }

            //strip ./
            if (name.indexOf("./") == 0) {
                name = name.substring(2);
            }

            var value = $field.val();

            if( $field.prop("type") == "checkbox" ){
                value = $field.prop("checked") ? $field.val() : "";
            }

            $('<input />').attr('type', 'hidden')
                .attr('name', fieldSetName + "/" + counter + "/" + name)
                .attr('value', value )
                .appendTo($form);

            //remove the field, so that individual values are not POSTed
            $field.remove();
        }

        $(document).on("click", ".cq-dialog-submit", function () {
            var $multifields = $("[" + DATA_EAEM_NESTED + "]");

            if(_.isEmpty($multifields)){
                return;
            }

            var $form = $(this).closest("form.foundation-form"),$fieldSets, $fields;
            var shouldBreakExecution = false;
            $multifields.each(function(i, multifield){
                $fieldSets = $(multifield).find(".coral-Form-fieldset");
                $fieldSets.each(function (counter, fieldSet) {
                    $fields = $(fieldSet).children().children(CFFW);
                    $fields.each(function (j, field) {
                    	//This is for actual validation when there is an is-invalid class on
                    	//one of the field inside multifield.
                    	//This works by adding is-invalid on the main item on dialog
						if($(this).find(".coral-Textfield").hasClass("is-invalid")){
                            $(this).closest('.coral-Form-fieldset').addClass("is-invalid");
							shouldBreakExecution = true;
                            return false;
						}
						//This is for onload validation of sub items. 
						//Checks mandatory sub items and stops dialog execution
						if($(this).find(".coral-Textfield").attr("aria-required")=='true'){
							if($(this).find(".coral-Textfield").val().length==0){
								 $(this).closest('.coral-Form-fieldset').addClass("aria-required");
								 shouldBreakExecution = true;
		                         return false;
							}
						}
                    });
                });
            });
            if(!shouldBreakExecution){
                   $multifields.each(function(i, multifield){
               	   $fieldSets = $(multifield).find(".coral-Form-fieldset");
				   $fieldSets.each(function (counter, fieldSet) {
                        $fields = $(fieldSet).children().children(CFFW);
                        $fields.each(function (j, field) {
                                fillValue($form, $(fieldSet).data("name"), $(field).find("[name]"), (counter + 1));
                        });
                    });
                });
            }            
            var currentNumItems = ($(".coral-Multifield-add").prev()).children().length;
			var minItems = $(".coral-Multifield-add").parent().attr(DATA_MIN_ITEMS);
			if(typeof currentNumItems != "undefined" && typeof minItems != "undefined"){
            	if(currentNumItems < minItems){
					$(window).adaptTo("foundation-ui").alert("Error", "Add atleast one tile");
					return false;
                }
				else{
					return true;
				}
            } 
        });
    }

    $(document).ready(function () {
        addDataInFields();
        collectDataFromFields();
    });
})();