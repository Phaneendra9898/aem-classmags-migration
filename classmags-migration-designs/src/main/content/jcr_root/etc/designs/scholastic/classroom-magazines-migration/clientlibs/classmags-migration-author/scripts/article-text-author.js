(function () {
    var ARTICLE_TEXT = "Article - Text";

    //reads multifield data from server, creates the nested composite multifields and fills them
    function addDataInFields() {
        function buildArticleTextDialog(articleConfigInfinityUrl, $form, articleTextUrl){
			var articleConfig= $.getJSON( articleConfigInfinityUrl, function( data ) {

              var items = [];
              $.each( data, function( key, val ) {
                  if(key === 'lexileSettings'){
					items = val;
                  }
              });
              var $tabNav = $form.find('.coral-TabPanel-navigation');
              var $tavContent = $form.find('.coral-TabPanel-content');
              var size = Object.keys(items).length;
                 if(size > 0){
                $.each(items, function (index, value) {
                    if(value.readingLevel != undefined){
                        var coralId = 'coral-'+index;
                        var title = './'+index+'/title';
                            if(index == 1){
                                $tabNav.append( "<a class='coral-TabPanel-tab' href='#' data-toggle='tab' role='tab' tabindex='0' aria-selected='true' aria-controls="+coralId+" aria-disabled='false'>"+value.readingLevel+"</a>" );
                                $tavContent.append( "<section class='full-width custom-dialog-width coral-TabPanel-pane coral-FixedColumn'"
                                                                  		+ "id="+coralId+" role='tabpanel' aria-hidden='false'>"
                                    									+"<div class='coral-FixedColumn-column coral-RichText-FixedColumn-column'>"
                                                                        +"<div class='coral-Form-fieldwrapper'><label class='coral-Form-fieldlabel'></label><div class='richtext-container'>"
                                                                        +   "<input type='hidden' class='coral-Form-field coral-Textfield rteTitle' name="+title+" value='' data-usefixedinlinetoolbar='true'"
                                                                        +    "<input type='hidden' class='coral-RichText-isRichTextFlag' name='./textIsRich' value='true'>"
                                                                        +    "<div class='full-width custom-dialog-width rte-value coral-RichText-editable coral-Form-field coral-Textfield coral-Textfield--multiline coral-RichText' data-config-path='/mnt/override/apps/scholastic/classroom-magazines-migration/components/content/article-text/cq:dialog/content/items/tabs/items/article-settings/items/column/items/title.infinity.json' data-use-fixed-inline-toolbar='true'></div>"
                                                                        + "</div></div></div>" 
                                                                     + "</section>");
                            } else {
                                $tabNav.append( "<a class='coral-TabPanel-tab' href='#' data-toggle='tab' role='tab' tabindex='-1' aria-selected='false' aria-controls="+coralId+" aria-disabled='false'>"+value.readingLevel+"</a>" );
                               	$tavContent.append( "<section class='full-width custom-dialog-width coral-TabPanel-pane coral-FixedColumn'"
                                                                  		+ "id="+coralId+" role='tabpanel' aria-hidden='false'>"
                                    									+"<div class='coral-FixedColumn-column coral-RichText-FixedColumn-column'>"
                                                                        +"<div class='coral-Form-fieldwrapper'><label class='coral-Form-fieldlabel'></label><div class='richtext-container'>"
                                                                        +   "<input type='hidden' class='coral-Form-field coral-Textfield rteTitle' name="+title+" value='' data-usefixedinlinetoolbar='true'"
                                                                        +    "<input type='hidden' class='coral-RichText-isRichTextFlag' name='./textIsRich' value='true'>"
                                                                        +    "<div class='full-width custom-dialog-width rte-value coral-RichText-editable coral-Form-field coral-Textfield coral-Textfield--multiline coral-RichText' data-config-path='/mnt/override/apps/scholastic/classroom-magazines-migration/components/content/article-text/cq:dialog/content/items/tabs/items/article-settings/items/column/items/title.infinity.json' data-use-fixed-inline-toolbar='true'></div>"
                                                                        + "</div></div></div>" 
                                                                     + "</section>");
                            }
                    }
                });
                 } else {
					$tabNav.append( "<a class='coral-TabPanel-tab' href='#' data-toggle='tab' role='tab' tabindex='0' aria-selected='true' aria-controls='coral-1' aria-disabled='false'>Default</a>" );
                    $tavContent.append( "<section class='full-width custom-dialog-width coral-TabPanel-pane coral-FixedColumn'"
                                        + "id='coral-1' role='tabpanel' aria-hidden='false'>"
                                        +"<div class='coral-FixedColumn-column coral-RichText-FixedColumn-column'>"
                                        +"<div class='coral-Form-fieldwrapper'><label class='coral-Form-fieldlabel'></label><div class='richtext-container'>"
                                        +   "<input type='hidden' class='coral-Form-field coral-Textfield rteTitle' name='./1/title' value='' data-usefixedinlinetoolbar='true'"
                                        +    "<input type='hidden' class='coral-RichText-isRichTextFlag' name='./textIsRich' value='true'>"
                                        +    "<div class='full-width custom-dialog-width rte-value coral-RichText-editable coral-Form-field coral-Textfield coral-Textfield--multiline coral-RichText' data-config-path='/mnt/override/apps/scholastic/classroom-magazines-migration/components/content/article-text/cq:dialog/content/items/tabs/items/article-settings/items/column/items/title.infinity.json' data-use-fixed-inline-toolbar='true'></div>"
                                        + "</div></div></div>" 
                                        + "</section>"); 
                 }

                 articleText();
            });

        function articleText(){
            $.getJSON( articleTextUrl, function( data ) {
                  var counter = 1;
                  $.each( data, function( key, val ) {
                      if(isInt(key) ){
                         var $field = $form.find("[name='./" + counter + "/title']").last(); 
                         var $div = $field.next('div');
                         $div.html(val.title);
                         counter++;
                      }
                  });
				});
        	}
        }
        
        function isInt(value) {
          var x;
          if (isNaN(value)) {
            return false;
          }
          x = parseFloat(value);
          return (x | 0) === x;
        }
        
        $(document).on("dialog-ready", function() {
            var componentName = $("h2.coral-Heading").text();

            if(componentName.indexOf(ARTICLE_TEXT)> -1){

            var $form = $(".cq-dialog"),
            actionUrl = $form.attr("action") 
                var iframeBody = $("#ContentFrame").contents().find("body");
                var articleConfigCompPath = iframeBody.find(".article-config-comp").attr('article-config-path');

                var articleConfigInfinityUrl = articleConfigCompPath+".infinity.json";

                $.ajax(actionUrl).done(postProcess);
    
                function postProcess(data){
                    buildArticleTextDialog(articleConfigInfinityUrl, $form, actionUrl+'.infinity.json');
                }
                
                //adding this class for article text caption RTE
        		var $coralRichText = $( ".cq-dialog .richtext-container .article-text-image-rte" ).siblings('.coral-RichText');
                $coralRichText.addClass('rte-custom-width');
            }

        });
    }

    //collect data from widgets in multifield and POST them to CRX
    function collectDataFromRTEs(){
        function fillValue($form, $field, counter){

            var value = $field.html();

            $('<input />').attr('type', 'hidden')
                .attr('name', "./"+counter+"/title")
                .attr('value', value )
                .appendTo($form);

            //remove the field, so that individual values are not POSTed
            $field.remove();
        }

        $(document).on("click", ".cq-dialog-submit", function () {

            var $form = $(this).closest("form.foundation-form");

            var nameRteArr = $('.rte-value');
                $(nameRteArr).each(function(index){
                	fillValue($form, $(this), (index + 1)); 
            	});

        });
    }

    $(document).ready(function () {
        addDataInFields(); 
        collectDataFromRTEs();
    });
})();