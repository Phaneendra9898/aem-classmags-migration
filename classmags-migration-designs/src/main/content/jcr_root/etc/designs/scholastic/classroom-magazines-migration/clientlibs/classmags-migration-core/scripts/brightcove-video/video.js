var SCHVideo;
(function(){
    var videoPlayer,
        $videoPlayerContainer = $('#video_player_container');

    SCHVideo = {
        pause: function(){
            videoPlayer.pause();
        },
        loadVideo: function(videoId){
            videoPlayer.catalog.getVideo(videoId, function(error, video) {
                videoPlayer.catalog.load(video);
            });
        },
        play: function(videoId){
            if(!videoPlayer){
                videojs("modal_video_player").ready(function() {
                  videoPlayer = this;
                  videoPlayer.on('loadedmetadata', function(){
                    this.play();
                  });
                  SCHVideo.loadVideo(videoId);
                });
            }else{
                SCHVideo.loadVideo(videoId);
            }
        }
    };
})();

function seeMoreDialog() {
	$("#seeMorePopup").modal();
	$('.modal-backdrop.in').css('z-index', '9999');
}

//$('.video_play').click(function(e){ // dynamic
$('body').on('click','.video_play',function(e){

$('body').addClass('modal_open');
    $(".s_video_player").css("left", "0");

    videoId = $(this).attr('data-video');
    SCHVideo.play(videoId);
})
    

//});

var s_close_video = function() {
    $(".s_video_player").css("left", "150%");
    $('body').removeClass('modal_open');

    SCHVideo.pause();
};

//--------------Redirect link logic-------------///
$('a.redirect_link').each(function(e){
    redirectLink = jQuery(this).attr('data-link');
    jQuery(this).attr("href",redirectLink);

});
//--------------En redirect link logic-------------///
