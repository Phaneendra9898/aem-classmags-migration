app.directive('swMarketingContentTile', [
    '$timeout',
    function($timeout) {
        return {
            restrict: 'E',
            scope: {
                imageSrc: "@",
                imageHyperlink: '@',
                contentTitle: '@',
                contentType: '@',
                position: '@',
                videoId: '@',
                videoLength: '@',
                gameId: '@',
                gamePath: '@',
                gameFlashContainer: '@',
                gameType: '@',
                gameTitle: '@',
                isNewWindow: '@'
            },
            link: function(scope, elem, attrs) {
                scope.openGamePopup = function($event) {
                    $timeout(function() {
                        var $clickedElement = angular.element($event.currentTarget);
                        var $triggerElement = $clickedElement.closest('.content-tile-body').find('.content-card-link');
                        angular.element($triggerElement).trigger('click');
                    });
                }
            },
            templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/clientlibs/classmags-migration-core/scripts/marketing-contentTile/marketing-contentTilePartials.html',
            controller: contentTileController
        };

        function contentTileController($scope) {

        }
    }
]);
