app.controller("articleToolbarController", ["$scope", "$timeout", "$rootScope", "swUserRole", "articleToolbarFactory", function($scope, $timeout, $rootScope, swUserRole, articleToolbarFactory) {
    var toolbarUpdate = function() {
            $scope.isTeachingPresent = true;
            $scope.isVideoComponentPresent = true;
            var $teachingResourcesComponent = angular.element("#TeachingResourcesComponent");
            var $videoComponent = angular.element("#VideoComponent");
            if ($teachingResourcesComponent.length > 0) {
                $scope.isTeachingPresent = true;
            } else {
                $scope.isTeachingPresent = false;
            }

            if ($videoComponent.length > 0) {
                $scope.isVideoComponentPresent = true;
            } else {
                $scope.isVideoComponentPresent = false;
            }
        },
        bookmarkResourcePath,
        userRole,
        appName;

    $timeout(function() {
        bookmarkResourcePath = $('#bookmarkSocialPostPath').val();
        appName = $("#core-main-container").attr("data-app-name").toLowerCase();
        articleToolbarFactory.toggleBookmark('getAllSite', bookmarkResourcePath, appName, $scope.pagePath).then(handleSuccess, handleError);
    });

    $rootScope.$on("ARTICLE_RESOURCES", function(event, data) {
        toolbarUpdate();
    });

    $scope.openMagazine = function($event) {
        $event.stopPropagation();
    };

    $scope.isBookmarkAdded = false;

    if (swUserRole) {
        userRole = swUserRole.toLowerCase();
    }

    $scope.addBookmark = function() {
        if (userRole === "teacher" || userRole === "student") {
            $scope.isSpinnerShow = true;
            articleToolbarFactory.toggleBookmark('addbookmark', bookmarkResourcePath, appName, $scope.pagePath).then(handleSuccess, handleError);
        }
    };

    $scope.deleteBookmark = function() {
        $scope.isSpinnerShow = true;
        articleToolbarFactory.toggleBookmark('deletebookmark', bookmarkResourcePath, appName, $scope.pagePath).then(handleSuccess, handleError);
    };

    function handleSuccess(response) {
        var data = response.data;
        if (data && data.response) {
            $scope.isBookmarkAdded = data.response.bookmarkEnabled;
        }
        $scope.isSpinnerShow = false;
    }

    function handleError(response) {
        //handle error
        $scope.isSpinnerShow = false;
    }

    toolbarUpdate();
}]);