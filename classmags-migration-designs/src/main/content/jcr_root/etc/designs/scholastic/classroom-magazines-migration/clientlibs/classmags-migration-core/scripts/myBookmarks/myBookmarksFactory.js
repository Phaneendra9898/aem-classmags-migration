'use strict';

app.factory('myBookmarksFactory', ["$http", "$q", function($http, $q) {
    var getBookmarks = function(appName) {
        var deferred = $q.defer();
        var data = {
                params: {
                    ':operation': 'social:bookmark:getMyBookmarks',
                    'bookmarkPagePath': location.pathname,
                    'appName': appName
                }
            },
            path = $('#bookmarkSocialPostPath').val() + '.social.json?' + $.param(data.params);
        $http.post(path, data.params)
            .then(function(response) {
                deferred.resolve(response);
            }, function(response) {
                deferred.reject(response);
            });

        return deferred.promise;
    };

    return {
        getBookmarks: getBookmarks
    };
}]);
