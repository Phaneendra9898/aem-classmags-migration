(function(document, $) {
    "use strict";
    var createNamespace;
    $(document).on("foundation-contentloaded", function(e) {
        if (!createNamespace) {
            createNamespace = new CreateNamespace();
            createNamespace.initialize();
        }
    });

    var CreateNamespace = new Class({
        dialog: null,
        contentPath: '',
        _ILLEGAL_FILENAME_CHARS: "%/\\:*?\"[]|\n\t\r. #{}^;+",
        initialize: function() {
            var self = this;
            $('body').on('keypress', '#js-app-create-page-custom-title', function(e) {
                self._validateAndAddTooltip($("#js-app-create-page-custom-title"), $("#js-app-create-page-custom-title").val());
            });
            $('body').on('input', '#js-app-create-page-custom-title', function(e) {
                self._validateAndAddTooltip($("#js-app-create-page-custom-title"), $("#js-app-create-page-custom-title").val());
            });
        },
        _replaceRestrictedCodes: function(val) {
            return val.replace(/[\/:\[\]*|'#]/g, "-");
        },
        _validateAndAddTooltip: function(inputElement, enteredText) {
            var self = this;

            Array.prototype.slice.call(inputElement.get(0).parentElement.getElementsByTagName('coral-tooltip')).forEach(function(item) {
                  item.remove();
            });

            if (self._hasRestrictedChar(enteredText)) {
                inputElement.get(0).parentElement.appendChild(new Coral.Tooltip().set({
                    variant: 'error',
                    id: 'js-app-tooltip-error',
                    content: {
                        innerHTML: Granite.I18n.get("The name must not contain {0}, so replaced by {1}", [self._ILLEGAL_FILENAME_CHARS
                            .toString().replace(/[,]/g, " "), "-"
                        ])
                    }
                })).show();
                $("#js-app-create-page-custom-name").val(self._replaceRestrictedCodes(enteredText.toLowerCase()).replace(/ /g, '-'));
            } else {
                $("#js-app-create-page-custom-name").val(enteredText.toLowerCase().replace(/ /g, '-'));
            }

        },

        _isRestricted: function(code) {
            var self = this;
            var charVal = String.fromCharCode(code);
            if (self._ILLEGAL_FILENAME_CHARS.indexOf(charVal) > -1) {
                return true;
            } else {
                return false;
            }
        },
        _hasRestrictedChar: function(textValue) {
            var self = this;
            for (var i = 0, ln = textValue.length; i < ln; i++) {
                if (self._isRestricted(textValue.charCodeAt(i)) || textValue.charCodeAt(i) > 127) {
                    return true;
                }
            }
            return false;
        }
    });

})(document, Granite.$);