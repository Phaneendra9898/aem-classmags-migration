app.directive('swSlideshow', function() {
    return {
        restrict: 'E',
        scope: {
            data: '@',
            coverimage: '@',
            backgroundcolor: '@',
            disablenumber: '@',
            uniqueid: '@',
            contentType: '@'
        },
        controller: ['$scope', '$sce', function ($scope, $sce) {
            $scope.slides = JSON.parse($scope.data);
            $scope.bgColor = {'background-color': $scope.backgroundcolor};
            
            $('body').keyup(function(event) {
                if (event.keyCode == 37) {
                    $('.left-icon').trigger('click');
                } else if (event.keyCode == 39) {
                    $('.right-icon').trigger('click');
                }
            });

            $scope.trustAsHtml = function(htmlDynamicContent){
                return $sce.trustAsHtml(htmlDynamicContent);
            }            

            $scope.launchAudio = function(slideNum){
                angular.element('.active .audio-wrapper').find('audio')[slideNum].play();
            }

            $('.grey-close-btn').on('click', function(){
                angular.element('.audio-wrapper').find('audio')[slideNum].stop();
            });
        }],
        templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/clientlibs/classmags-migration-core/scripts/slideshow/slideshowPartial.html'
    }
});