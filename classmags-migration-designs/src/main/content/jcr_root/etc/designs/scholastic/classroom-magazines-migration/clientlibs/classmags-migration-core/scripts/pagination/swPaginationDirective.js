app.directive('swPagination', function() {
    return {
        restrict: 'E',
        scope: {
            totalIssuePageCount : '=',
            totalIssueRecords: '=',
            totalSearchArticlesResources: '=',
            totalSearchIssueResources: '=',
            bookmarkCurrentPage : '=',
            records : '=',
            pages : '=',
            myBookmarkPage: '@',
            myBookmarkTotalRecords: '=',
            onPrevious : '&',
            onNext : '&',
            onChange : '&'
        },
        templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/clientlibs/classmags-migration-core/scripts/pagination/swPaginationPartial.html'
    }
});
