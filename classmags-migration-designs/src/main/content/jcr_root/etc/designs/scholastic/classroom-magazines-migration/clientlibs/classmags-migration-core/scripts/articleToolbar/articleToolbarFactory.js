app.factory('articleToolbarFactory', [
    "$http",
    "$q",
    function($http, $q) {
        var toggleBookmark = function(operation, resourcePath, appName, bookmarkPath) {
            var deferred = $q.defer(),
                data = {
                    params: {
                        ':operation': 'social:bookmark:' + operation,
                        'bookmarkPagePath': decodeURIComponent(bookmarkPath),
                        'appName': appName
                    }
                };

            if (operation === 'addbookmark') {
                data.params['viewArticleLinkPath'] = decodeURIComponent(bookmarkPath);
            }

            var path = resourcePath + '.social.json?' + $.param(data.params);
            $http.post(path, data.params).then(function(response) {
                deferred.resolve(response);
            }, function(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        };

        var toggleBookmarkByContentType = function(operation, resourcePath, appName, contentType, pagePath, isShowViewArticle, viewArticleLink) {
            var deferred = $q.defer(),
                data;
            contentType = contentType.toLowerCase()
            if (contentType === "article" || contentType === "articles" || contentType === "issue" || contentType === "issues" || contentType === "poll" || contentType === "blog" || contentType === "blogs"|| contentType === "custompage") {
                data = {
                    params: {
                        ':operation': 'social:bookmark:' + operation,
                        'bookmarkPagePath': decodeURIComponent(pagePath),
                        'appName': appName
                    }
                };
            } else {
                data = {
                    params: {
                        ':operation': 'social:bookmark:' + operation,
                        'bookmarkedTeachingResourcePath': decodeURIComponent(pagePath),
                        'appName': appName
                    }
                };
            }

            if (isShowViewArticle) {
                data.params['viewArticleLinkPath'] = viewArticleLink;
            }

            var path = resourcePath + '.social.json?' + $.param(data.params);
            $http.post(path, data.params).then(function(response) {
                deferred.resolve(response);
            }, function(response) {
                deferred.reject(response);
            });
            return deferred.promise;
        };

        return {toggleBookmark: toggleBookmark, toggleBookmarkByContentType: toggleBookmarkByContentType}
    }
]);
