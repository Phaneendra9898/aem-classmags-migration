app.controller("globalNavigationController", [
    "$scope",
    "$timeout",
    "authorizationService",
    "swUserRole",
    "swUserEntitlements","$sce",
    function($scope, $timeout, authorizationService, swUserRole, swUserEntitlements, $sce) {
        $('[data-submenu]').submenupicker();
        $scope.isLoggedIn = false;

        $timeout(function() {
            $("#subscribe-now-link").attr("href", $(".subscribe-text").attr("href"));
            $("#subscribe-now-link2").attr("href", $(".subscribe-text").attr("href"));
            $('.js-breadcrumb-home-nav-link').attr('href', $("#sw-sdm-home-page-logged-out").val());

            var setProfileMenu = function() {
                //$scope.profileLink = $("#sdm-nav").find(".sdm-usericon-image").attr("src");
                $scope.profileName = $("#sdm-nav").find(".sdm-username").text();
                $scope.$apply(function() {
                    $scope.isLoggedIn = true;
                    $('.js-breadcrumb-home-nav-link').attr('href', $("#sw-sdm-home-page-logged-in").val());
                });

                $scope.entitlements = angular.copy(swUserEntitlements);  
                $scope.entitlementsLength = swUserEntitlements.length;
                if($scope.entitlementsLength>4){
                    $scope.entitlements.length = 4;
                }
                for(var i=0; i<$scope.entitlements.length; i++){
                    $scope.entitlements[i].thumbnail = $sce.trustAsResourceUrl($scope.entitlements[i].thumbnail);
                }
            }

            $('body').on('DOMNodeInserted', '#sdm-nav', function(e) {
                setProfileMenu();
            });

            var getSDMJSfile = function() {
                var jsSDMResource = "";
                if ($("#core-main-container").attr("data-core") === "true") {
                    jsSDMResource = $("<script type='text/javascript' src='https://digital.scholastic.com/resources/nav-widget/sdm-nav-bar.js'>");
                } else {
                    jsSDMResource = $("<script type='text/javascript' src='https://dp-portal-dev1.scholastic-labs.io/resources/nav-widget/sdm-nav-bar.js'>");
                }
                $("head").append(jsSDMResource);
            }
            getSDMJSfile();
        });

        $(document).ready(function() {
            var screenWidth = $(document).width();
            var screenHeight = $(document).height();
            var mainView = "";
            var intermediateView = "";
            var isSubViewClicked = false;
            var $menuPopOverElement = $('.pop-over-menu-div');
            var $menuContentElement = $(".js-nav-menu");
            var loggedInUrl = $("#sw-sdm-home-page-logged-in").val();
            var loggedOutUrl = $("#sw-sdm-home-page-logged-out").val();
            $('.menu-list').find('.js-dropdown-dynamic').unwrap().unwrap().unwrap();
            $menuPopOverElement.css({
                "height": screenHeight,
                left: -$menuPopOverElement.width()
            });
            $(".navbar-collapse.collapse").hide();
            var goBackClicked = false;

            $('body').on('click', '.menu-list a', function(e) {
                var href = $(this).attr('href');
                if (href) {
                    window.location = href;
                }
            });

            $('body').on('click', '.js-nav-menu a', function(e) {
                var href = $(this).attr('href');
                if (href) {
                    window.location = href;
                }
            });

            var openPopover = function() {
                if ($('.pop-over-menu-div').hasClass('active-block')) {
                    closePopoverDiv();
                } else {
                    $menuContentElement.append($('.menu-list'));
                    $('.pop-over-menu-div').animate({
                        left: 0
                    }, 100, function() {
                        $('.pop-over-menu-div').addClass("active-block");
                    });

                    $('.app-navbar-header, .scholastic-main-wrapper, .marketing-globalFooter-main-container').animate({
                        left: $('.pop-over-menu-div').width()
                    }, 100);

                    $('.menu-back').hide();
                    mainView = $(".js-nav-menu").html();
                    $menuContentElement.append($menuContentElement.find('.primary-button'));
                }
            };
            $('body').on('click', '.nav-menu-icon', function(e) {
                openPopover();
                $(".secondary-items").show();
            });
            $('body').on('click', '.nav-menu-link', function(e) {
                openPopover();
                $(".secondary-items").show();
            });

            $menuPopOverElement.on('click', '.js-dropdown', function(e) {
                mainView = $(".js-nav-menu").html();
                var subView = $(this).find(".js-dropdown-menu");
                if (subView.length > 0) {
                    $menuContentElement.empty();
                    $menuContentElement.append(subView);

                    /*
                $menuPopOverElement.css({"height":screenHeight, "right":-screenWidth});
                $('.pop-over-menu-div').animate({
                      right: 0
                },500).addClass("active-block");
                */

                    $menuContentElement.children('ul').removeClass("dropdown-menu");
                    $('.menu-back').show();
                }
            });

            $menuPopOverElement.on('click', ".js-dropdown-submenu", function(e) {
                intermediateView = $(".js-nav-menu").html();
                var subView = $(this).find(".js-dropdown-submenu-menu");
                if (subView.length > 0) {
                    $menuContentElement.empty();
                    $menuContentElement.append(subView);
                    $menuContentElement.children('ul').removeClass("dropdown-menu");
                    isSubViewClicked = true;
                    $('.menu-back').show();

                    /*
                $menuPopOverElement.css({"height":screenHeight, "right":-screenWidth});
                $('.pop-over-menu-div').animate({
                      right: 0
                },500).addClass("active-block");
                */
                }

            });

            $menuPopOverElement.on('click', ".menu-back", function(e) {
                $menuContentElement.empty();
                if (!isSubViewClicked) {
                    $menuContentElement.append(mainView);
                    $('.menu-back').hide();
                } else {
                    $menuContentElement.append(intermediateView);
                    isSubViewClicked = false;
                    $('.menu-back').show();
                }

                /*
            $menuPopOverElement.css({"height":screenHeight, "right":-screenWidth});
            $('.pop-over-menu-div').animate({
                  right: 0
            },500).addClass("active-block");
            */

            });

            $menuPopOverElement.on('click', ".menu-close", function(e) {
                closePopoverDiv();
            });

            function closePopoverDiv() {
                $('.pop-over-menu-div').animate({
                    left: -$('.pop-over-menu-div').width()
                }, function() {
                    $('.pop-over-menu-div').removeClass("active-block");
                });
                $('.app-navbar-header, .scholastic-main-wrapper, .marketing-globalFooter-main-container').animate({
                    left: 0
                }, 100);
                $menuContentElement.empty();
                $menuContentElement.append(mainView);
                $('.menu-back').hide();
            }

            $scope.search = function(path) {
                if ($scope.searchText && $scope.searchText.trim()) {
                    var queryURL = "?text=" + $scope.searchText + "&tab=article&asort=published&isort=published" + "&arecords=" + $scope.recordsPerPage + "&path=" + $scope.searchPagePath.replace(/.html/i, '');
                    location.href = path + queryURL;
                }
            }

            $scope.seeAllSubjscriptions = function(){
                angular.element("#sdm-portal-link").trigger("click");
            }

            $scope.showWait = false;
            $scope.login = function(sdmURL, role) {
                $timeout(function() {
                        var appCode = $("#core-main-container").attr("data-app-name").toLowerCase();
                        if (!goBackClicked) {
                            $('body').hide();
                            if (!authorizationService.swLoginNavLink) {
                                if (!location.search) {
                                    location.href = sdmURL + "?app=" + appCode + "&state=" + location.pathname + "&role=" + role;
                                } else {
                                    location.href = sdmURL + location.search + "&app=" + appCode + "&state=" + location.pathname + "&role=" + role;
                                }
                            } else {
                                if (!location.search) {
                                    location.href = sdmURL + "?app=" + appCode + "&state=" + authorizationService.swLoginNavLink + "&role=" + role;
                                } else {
                                    location.href = sdmURL + location.search + "&app=" + appCode + "&state=" + authorizationService.swLoginNavLink + "&role=" + role;
                                }
                            }

                        }
                }, 500);
            }

            $scope.hideWait = function() {
                $scope.showWait = false;
                $scope.isOptionSelected = false;
            }

            $scope.goBack = function() {
                goBackClicked = true;
            }

            $scope.userRedirection = function() {
                if (swUserRole && (swUserRole.toLowerCase() == "teacher" || swUserRole.toLocaleLowerCase() == "student")) {
                    location.href = loggedInUrl;
                } else {
                    location.href = loggedOutUrl;
                }

            }

        });
    }
]);
