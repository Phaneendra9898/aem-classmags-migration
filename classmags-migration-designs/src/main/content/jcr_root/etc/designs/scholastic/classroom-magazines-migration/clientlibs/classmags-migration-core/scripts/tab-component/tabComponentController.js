app.controller("tabComponentController", ["$scope", function($scope) {
    angular.element(document).ready(function(){
        $(".main-tabs li").hide();
        var MAX_WIDTH_TABS = '20%';        
        var newWidth ='';        
        var numberOfTabs = angular.element(".main-tabs").find("li").length;
        var mintabWidth = 100 / numberOfTabs;
        if(mintabWidth > 20){
            newWidth = mintabWidth+'%';
        }
        else{
            newWidth = MAX_WIDTH_TABS;
        }

        angular.element(".main-tabs li").css("width", newWidth);
        angular.element(".main-tabs li").show();
    });
}]);
