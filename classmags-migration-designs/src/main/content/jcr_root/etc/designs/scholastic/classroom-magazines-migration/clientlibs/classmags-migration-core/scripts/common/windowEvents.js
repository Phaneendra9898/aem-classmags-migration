(function() {
    var contentTilesDynamicHeight = function() {
        if (document.body.clientWidth < 1440) {
            if ($(".image-container-resource")) {
                $(".image-container-resource").each(function() {
                    var dynamicWidthDifference = 1440 - document.body.clientWidth;
                        var computedHeight = 162.91 - dynamicWidthDifference*.14+8;
                        if(computedHeight<162.91){
                            $(this).height(computedHeight);   
                        }
                        else{
                             $(this).height(162.91);   
                        }
                        $(".content-tile-container .content-tile-body .content-module .content-module-container .content-wrapper .content-one-up .img-container img").height(180-dynamicWidthDifference*.14-9);    
                        $(".junior-content-tile-container .content-tile-body .content-module .content-module-container .content-wrapper .content-one-up .img-container img").height(180-dynamicWidthDifference*.14-9);    
                        $(".dynamath-content-tile-container .content-tile-body .content-module .content-module-container .content-wrapper .content-one-up .img-container img").height(180-dynamicWidthDifference*.14-9);                         

                });
            }
            if($(".image-container")){
                $(".image-container").each(function(){
                    var dynamicWidthDifference = 1440 - document.body.clientWidth;
                    $(".content-tile-container .content-tile-body .content-module .content-module-container .content-wrapper .content-one-up .img-container img").height(180-dynamicWidthDifference*.14-9);
                    $(".junior-content-tile-container .content-tile-body .content-module .content-module-container .content-wrapper .content-one-up .img-container img").height(180-dynamicWidthDifference*.14-9);  
                    $(".dynamath-content-tile-container .content-tile-body .content-module .content-module-container .content-wrapper .content-one-up .img-container img").height(180-dynamicWidthDifference*.14-9);             
                });
            }
        } 
        else {
            $(".image-container-resource").each(function() {
                $(this).height(162.91);   
                $(".content-tile-container .content-tile-body .content-module .content-module-container .content-wrapper .content-one-up .img-container img").height(180); 
                $(".junior-content-tile-container .content-tile-body .content-module .content-module-container .content-wrapper .content-one-up .img-container img").height(180);
                $(".dynamath-content-tile-container .content-tile-body .content-module .content-module-container .content-wrapper .content-one-up .img-container img").height(180);                   
            });
        }
    }

    /*window resize*/
    $(window).resize(function() {
        contentTilesDynamicHeight();
    });

    /*window load*/
    window.onload = function() {
        contentTilesDynamicHeight();
    };

    /*global funciton to handle game image load*/
    window.resizeGameContentTileImageHeight = function(img){
        /*
         var dynamicWidthDifference = 1440 - document.body.clientWidth;
         $(img).height(180-dynamicWidthDifference*.14-9);
        */
        
        var dynamicWidthDifference = 1440 - document.body.clientWidth;
        $(".content-tile-container .content-tile-body .content-module .content-module-container .content-wrapper .content-one-up .img-container img").height(180-dynamicWidthDifference*.14-9);    
        $(".junior-content-tile-container .content-tile-body .content-module .content-module-container .content-wrapper .content-one-up .img-container img").height(180-dynamicWidthDifference*.14-9);    
        $(".dynamath-content-tile-container .content-tile-body .content-module .content-module-container .content-wrapper .content-one-up .img-container img").height(180-dynamicWidthDifference*.14-9);  

    }

})();
