'use strict';

app.controller('progressBarController', [
    '$scope',
    '$location',
    'subscriptionDataFactory',
    function($scope, $location, subscriptionDataFactory) {

        var successURL = "",
            failureURL = "";
        initialize();

        /**
     * Function to initialize the scope properties and functions.
     */
        function initialize() {
            $scope.cartData = [];
            $scope.isBilling = true;
            $scope.toggleSubscriptionType = false;
            $scope.subscriptionType = {
                'newSubscription': true
            };
            $scope.magazinePrices = [];
            $scope.shippingPrices = [];
            $scope.activeTabIndex = 0;
            $scope.billing = {
                'state': ''
            };
            $scope.changeTab = changeTab;
            $scope.saveMagazineQuantity = saveMagazineQuantity;
            $scope.updateMagazineQuantity = updateMagazineQuantity;
            $scope.continueSubscription = continueSubscription;
            $scope.toggleShippingDetails = toggleShippingDetails;
            $scope.returnFormattedPrice = returnFormattedPrice;
            $scope.setSubscription = setSubscription;
            $scope.getTotalPrice = getTotalPrice;
            $scope.validate = validate;
            $scope.updateQuantity = updateQuantity;
            $scope.formatPhoneNumber = formatPhoneNumber;
            var mainSection = angular.element('.subscription-progress-bar-main');
            $scope.activeColor = mainSection.attr('data-active-color');
            $scope.deactiveColor = mainSection.attr('data-deactive-color');
            successURL = mainSection.attr('data-success-url');
            failureURL = mainSection.attr('data-failure-url');
            $scope.tabElements = angular.element('.subscription-progress-bar-content')
            changeTab(0);
        }

        function changeTab(tabIndex) {
            $scope.tabElements.css('border-color', $scope.deactiveColor);
            $scope.tabElements.find('a').css('background-color', $scope.deactiveColor);
            $scope.tabElements.eq(tabIndex).css('border-color', $scope.activeColor);
            $scope.tabElements.eq(tabIndex).find('a').css('background-color', $scope.activeColor);
            $scope.activeTabIndex = tabIndex;
            $scope.toggleSubscriptionType = false;
            if (tabIndex === 1) {
                $scope.cartData = $scope.cartData.filter(function(eachItem) {
                    return eachItem.quantity > 0;
                });
                $scope.cartData.forEach(function(eachItem) {
                    eachItem.magazinePrice = $scope.magazinePrices[eachItem.magazineName];
                    eachItem.shippingPercentage = $scope.shippingPrices[eachItem.magazineName];
                    eachItem.subTotal = eachItem.quantity * eachItem.magazinePrice;
                    eachItem.shippingPrice = eachItem.subTotal * eachItem.shippingPercentage / 100;
                });
            }
        }

        function saveMagazineQuantity(event, index, isCart) {
            var currentElement = angular.element(event.currentTarget),
                elementValue = currentElement.val();
            if (elementValue.length > 3) {
                currentElement.val(elementValue.slice(0, 3));
            }
            if (isCart) {
                index = index + 1;
            }

            $scope.cartData[index] = currentElement.data();
            $scope.cartData[index].quantity = currentElement.val();
        }

        function updateMagazineQuantity(index, quantity) {
            if (quantity >= 0) {
                var cartObject = $scope.cartData[index];
                cartObject.quantity = quantity;
                cartObject.subTotal = quantity * cartObject.magazinePrice;
                cartObject.shippingPrice = cartObject.subTotal * cartObject.shippingPercentage / 100;
            }
        }

        function continueSubscription() {
            if ($scope.isBilling) {
                $scope.shipping = angular.copy($scope.billing);
            }

            if ($scope.activeTabIndex == 2) {
                $scope.subscriptionData = {
                    'billing': $scope.billing,
                    'shipping': $scope.shipping,
                    'billingOption': $scope.billingOption,
                    'cartData': angular.copy($scope.cartData),
                    'subscriptionType': $scope.subscriptionType,
                    'subTotalPrice': $scope.subTotalPrice,
                    'totalPrice': $scope.totalPrice,
                    'totalShippingPrice': $scope.totalShippingPrice
                };

                if ($scope.orderNumber) {
                    $scope.subscriptionData = {
                        'orderNumber': $scope.orderNumber
                    };
                }
                if ($scope.customerNumber) {
                    $scope.subscriptionData = {
                        'customerNumber': $scope.customerNumber
                    };
                }
                subscriptionDataFactory.submitSubscriptionData($scope, $('#subscriptionPath').val()).then(handleSuccess, handleError);
                return;
            }

            if ($scope.activeTabIndex === 1) {
                if ($scope.toggleSubscriptionType) {
                    $scope.toggleSubscriptionType = false;
                    $scope.changeTab(2);
                }
                $scope.toggleSubscriptionType = true;
            } else {
                $scope.changeTab($scope.activeTabIndex + 1);
            }
        }

        function handleSuccess(response) {
            console.log(successURL);
            console.log(response);
            location.href = successURL + '.html';
        }

        function handleError(response) {
            console.log(failureURL);
            console.log(response);
            //$('.subscription-error-modal').modal('show');
            location.href = failureURL + '.html';
        }

        function toggleShippingDetails() {
            if (!$scope.isBilling) {
                $scope.shipping = {};
            }
        }

        function validate(field, errorMessage) {
            var _popover,
                $currentField = $('#' + field.$name);
            if (!field.$valid) {
                _popover = $currentField.popover({
                    trigger: 'manual',
                    content: "<span class='glyphicon glyphicon-exclamation-sign error-icon'></span><span class='error-message'>" + $('#' + field.$name).attr('data-error') + "</span>",
                    html: true
                });
                return $currentField.popover("show");
            }
            return $currentField.popover("hide");
        }

        function setSubscription(selectedType) {
            $scope.subscriptionTypeText = selectedType;
        }

        function getTotalPrice() {
            $scope.subTotalPrice = 0;
            $scope.totalShippingPrice = 0;
            $scope.totalPrice = 0;
            $scope.cartData.forEach(function(eachItem) {
                $scope.subTotalPrice = $scope.subTotalPrice + eachItem.subTotal;
                $scope.totalShippingPrice = $scope.totalShippingPrice + eachItem.shippingPrice;
            });
            $scope.totalPrice = $scope.subTotalPrice + $scope.totalShippingPrice;
            return $scope.subTotalPrice;
        }

        function returnFormattedPrice(price) {
            price = parseFloat(price) || 0;
            return '$' + price.toFixed(2);
        }

        function updateQuantity(event) {
            var currentElement = angular.element(event.currentTarget),
                elementValue = currentElement.val();
            if (elementValue.length > 3) {
                currentElement.val(elementValue.slice(0, 3));
            }
        }

        function formatPhoneNumber(event) {
            var currentElement = event.currentTarget,
                phoneNumber = currentElement.value;
            phoneNumber = phoneNumber.replace(/[^\d]/g, '');
            currentElement.value = phoneNumber.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3");
        }
    }
]);
