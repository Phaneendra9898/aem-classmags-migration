'use strict';

app.factory('emailEditorFactory', ["$http", "$q", function($http, $q) {
	
	var emailEditorObj = {};

	emailEditorObj.emailEditorPost = function(emailData, pagePath){
        var deferred = $q.defer();
        var data = JSON.stringify(emailData) ;

        $http({
  			url: pagePath+".emailEditor.html",
  			method: 'POST',
            params: {"data": data}
			}).then(function(response) {
                deferred.resolve(response);
            }, function(response) {
                deferred.reject(response);
            });
        return deferred.promise;
	};

	return emailEditorObj;
}]);