app.directive('swAuthorize', ['authorizationService', 'swUserRole', 
function(authorizationService, swUserRole) {
   return {
       restrict: 'A',
       scope: {
          swAuthorize: '@'
       },
       link: function (scope, element, attrs) {  
           $("#sw-authorize-login-prompt-modal").find("input[type=radio]")
               .click(function(){
                $(".js-sw-login-modal-wait").show();    
                $(".js-sw-login-modal-body").hide();
           });          
           
           var showWait = function(){                 
                $(".js-sw-login-modal-wait").hide();    
                $(".js-sw-login-modal-body").show();
           }
           
           var pageAccessRestricted = function(){
                isPageAuthorized = false; 
                $('#sw-authorize-login-prompt-modal').modal('show'); 
                showWait();
                $('#sw-authorize-login-prompt-close, #modal-go-back').click(function(){
                    //window.history.back(); 
                    location.href = $("#sw-sdm-home-page-logged-in").val();
                });
           }
              
           var contentAccessRestricted = function(event){
                event.preventDefault();  
                event.stopPropagation();                 
                $('#sw-authorize-login-prompt-modal').modal('show'); 
                showWait();
                $('#sw-authorize-login-prompt-close, #modal-go-back').click(function(){
                $('#sw-authorize-login-prompt-modal').modal('hide');
                });                
           }
           var eventValidated = false;
           
           var attachContentEvent = function(){              
                element.find('a').on('click', function(event){       
                    var $selectedElement = $(this);
                    if(!authorizationService.verifyAccess(scope.swAuthorize, swUserRole)){
                        if($selectedElement.attr("href")){
                            authorizationService.swLoginNavLink = $selectedElement.attr("href");
                        }
                        else{
                            authorizationService.swLoginNavLink ="";
                        }
                        event.stopPropagation();
                        contentAccessRestricted(event);
                    }
                }); 
               
                element.on('click', function(event){
                    var $selectedElement = $(this);
                    if(!authorizationService.verifyAccess(scope.swAuthorize, swUserRole)){
                        if($selectedElement.attr("sw-data-href")){
                            authorizationService.swLoginNavLink = $selectedElement.attr("sw-data-href");
                        }
                        else{
                            authorizationService.swLoginNavLink ="";
                        }
                        contentAccessRestricted(event);
                    }                    
                });
           }            
       
           if(attrs['swPage']==="true"){            
                if(!authorizationService.verifyAccess(scope.swAuthorize, swUserRole)){
                    if(swUserRole && (swUserRole.toLowerCase() == "teacher" || swUserRole.toLocaleLowerCase() =="student")){
                         $('body').hide();
                         location.href = $("#sw-sdm-home-page-logged-in").val();
                    }
                    else{
                        pageAccessRestricted();
                    }
                 }
           }
           else{
               attachContentEvent();
           }
        }
   }
}]);

