'use strict';
app.controller("articleHeroImageController", ["$scope", "$timeout", function($scope, $timeout) {
    $timeout(function() {
        $("#HeroImageComponent .modal-wide").on("show.bs.modal", function() {
            var height = $(window).height();
            var width = $(window).width();

            $(this).find(".modal-dialog .article-hero-modal-image").css({
                'height': height - 75,
                'width': width
            });

            $(this).find(".modal-dialog img").css({
                'max-height': height - 75,
                'max-width': width - 75
            });
        });
    });
}]);
