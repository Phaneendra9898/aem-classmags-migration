app.directive('swTextHyperlink', function() {
    return {
        restrict: 'E',
        scope: {
            title:'@',
            hyperLinkText: '@',
            hyperLinkUrl: '@',
            recordsCount: '@',
            bgcolor: '@'
        },
        templateUrl: '/etc/designs/scholastic/classroom-magazines-migration/geographyspin/clientlibs/geographyspin-core/scripts/textHyperlink/swTextHyperlinkPartial.html'
    }
});
