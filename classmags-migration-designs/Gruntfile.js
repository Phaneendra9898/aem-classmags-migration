'use strict';

module.exports = function (grunt) {  
  var appConfig = {
    app: 'src/main/content/jcr_root/etc/designs/scholastic/classroom-magazines-migration', 
    nodeModules:'node_modules'   
  };

  // Configuration for all the tasks
  grunt.initConfig({
    // Project settings
    core: appConfig,

    sync: {
    main: {
        files: [
          {expand: true, src: ['<%= core.nodeModules%>/@scholastic/ereader/dist/**'], 
          dest: '<%= core.app %>/clientlibs/reader'}
        ]
      }
    },

    // Compiles Sass to CSS and generates necessary files if requested
    sass: {
      options: {
       
      },
      server: {
        files: [{
          expand: true,
          cwd: '<%= core.app %>/clientlibs/classmags-migration-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/clientlibs/classmags-migration-core/styles',
          ext: '.css'
        },
        {
          expand: true,
          cwd: '<%= core.app %>/superscience/clientlibs/superscience-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/superscience/clientlibs/superscience-core/styles',
          ext: '.css'
        },
        {
          expand: true,
          cwd: '<%= core.app %>/junior/clientlibs/junior-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/junior/clientlibs/junior-core/styles',
          ext: '.css'
        },
        {
          expand: true,
          cwd: '<%= core.app %>/scholasticnews3/clientlibs/scholasticnews3-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/scholasticnews3/clientlibs/scholasticnews3-core/styles',
          ext: '.css'
        },
		{
          expand: true,
          cwd: '<%= core.app %>/scholasticnews/clientlibs/scholasticnews-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/scholasticnews/clientlibs/scholasticnews-core/styles',
          ext: '.css'
        },
        {
          expand: true,
          cwd: '<%= core.app %>/scholasticnews4/clientlibs/scholasticnews4-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/scholasticnews4/clientlibs/scholasticnews4-core/styles',
          ext: '.css'
        },
        {
          expand: true,
          cwd: '<%= core.app %>/scholasticnews56/clientlibs/scholasticnews56-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/scholasticnews56/clientlibs/scholasticnews56-core/styles',
          ext: '.css'
        },
        {
          expand: true,
          cwd: '<%= core.app %>/geographyspin/clientlibs/geographyspin-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/geographyspin/clientlibs/geographyspin-core/styles',
          ext: '.css'
        },
        {
          expand: true,
          cwd: '<%= core.app %>/sciencespin2/clientlibs/sciencespin2-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/sciencespin2/clientlibs/sciencespin2-core/styles',
          ext: '.css'
        },
        {
          expand: true,
          cwd: '<%= core.app %>/sciencespin36/clientlibs/sciencespin36-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/sciencespin36/clientlibs/sciencespin36-core/styles',
          ext: '.css'
        },
        {
          expand: true,
          cwd: '<%= core.app %>/math/clientlibs/math-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/math/clientlibs/math-core/styles',
          ext: '.css'
        },
        {
          expand: true,
          cwd: '<%= core.app %>/dynamath/clientlibs/dynamath-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/dynamath/clientlibs/dynamath-core/styles',
          ext: '.css'
        },
        {
          expand: true,
          cwd: '<%= core.app %>/action/clientlibs/action-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/action/clientlibs/action-core/styles',
          ext: '.css'
        },
        {
          expand: true,
          cwd: '<%= core.app %>/upfront/clientlibs/upfront-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/upfront/clientlibs/upfront-core/styles',
          ext: '.css'
        }
        ]
      },
     superscience: {
        files: [{
          expand: true,
          cwd: '<%= core.app %>/superscience/clientlibs/superscience-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/superscience/clientlibs/superscience-core/styles',
          ext: '.css'
        }]
      },
      junior: {
        files: [{
          expand: true,
          cwd: '<%= core.app %>/junior/clientlibs/junior-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/junior/clientlibs/junior-core/styles',
          ext: '.css'
        }]
      },
     scholasticnews3: {
        files: [{
          expand: true,
          cwd: '<%= core.app %>/scholasticnews3/clientlibs/scholasticnews3-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/scholasticnews3/clientlibs/scholasticnews3-core/styles',
          ext: '.css'
        }]
      },
      scholasticnews4: {
        files: [{
          expand: true,
          cwd: '<%= core.app %>/scholasticnews4/clientlibs/scholasticnews4-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/scholasticnews4/clientlibs/scholasticnews4-core/styles',
          ext: '.css'
        }]
      },
      scholasticnews56: {
        files: [{
          expand: true,
          cwd: '<%= core.app %>/scholasticnews56/clientlibs/scholasticnews56-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/scholasticnews56/clientlibs/scholasticnews56-core/styles',
          ext: '.css'
        }]
      },
      geographyspin: {
        files: [{
          expand: true,
          cwd: '<%= core.app %>/geographyspin/clientlibs/geographyspin-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/geographyspin/clientlibs/geographyspin-core/styles',
          ext: '.css'
        }]
      },
      sciencespin2: {
        files: [{
          expand: true,
          cwd: '<%= core.app %>/sciencespin2/clientlibs/sciencespin2-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/sciencespin2/clientlibs/sciencespin2-core/styles',
          ext: '.css'
        }]
      },
      sciencespin36: {
        files: [{
          expand: true,
          cwd: '<%= core.app %>/sciencespin36/clientlibs/sciencespin36-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/sciencespin36/clientlibs/sciencespin36-core/styles',
          ext: '.css'
        }]
      },
      math: {
        files: [{
          expand: true,
          cwd: '<%= core.app %>/math/clientlibs/math-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/math/clientlibs/math-core/styles',
          ext: '.css'
        }]
      },
      dynamath: {
        files: [{
          expand: true,
          cwd: '<%= core.app %>/dynamath/clientlibs/dynamath-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/dynamath/clientlibs/dynamath-core/styles',
          ext: '.css'
        }]
      },
      action: {
        files: [{
          expand: true,
          cwd: '<%= core.app %>/action/clientlibs/action-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/action/clientlibs/action-core/styles',
          ext: '.css'
        }]
      },
      upfront: {
        files: [{
          expand: true,
          cwd: '<%= core.app %>/upfront/clientlibs/upfront-core/styles',
          src: ['*.scss'],
          dest: '<%= core.app %>/upfront/clientlibs/upfront-core/styles',
          ext: '.css'
        }]
      }
    },

    watch: {     
      sass: {
        files: ['<%= core.app %>/clientlibs/classmags-migration-core/styles/{,*/}*.{scss,sass}',
                '<%= core.app %>/clientlibs/classmags-migration-core/styles/core/{,*/}*.{scss,sass}',
                '<%= core.app %>/superscience/clientlibs/superscience-core/styles/{,*/}*.{scss,sass}',
                '<%= core.app %>/superscience/clientlibs/superscience-core/styles/core/{,*/}*.{scss,sass}',
                '<%= core.app %>/junior/clientlibs/junior-core/styles/{,*/}*.{scss,sass}',
                '<%= core.app %>/junior/clientlibs/junior-core/styles/core/{,*/}*.{scss,sass}',
                '<%= core.app %>/scholasticnews3/clientlibs/scholasticnews3-core/styles/{,*/}*.{scss,sass}',
                '<%= core.app %>/scholasticnews3/clientlibs/scholasticnews3-core/styles/core/{,*/}*.{scss,sass}',
                '<%= core.app %>/scholasticnews4/clientlibs/scholasticnews4-core/styles/{,*/}*.{scss,sass}',
                '<%= core.app %>/scholasticnews4/clientlibs/scholasticnews4-core/styles/core/{,*/}*.{scss,sass}',
                '<%= core.app %>/scholasticnews56/clientlibs/scholasticnews56-core/styles/{,*/}*.{scss,sass}',
                '<%= core.app %>/scholasticnews56/clientlibs/scholasticnews56-core/styles/core/{,*/}*.{scss,sass}',
                '<%= core.app %>/geographyspin/clientlibs/geographyspin-core/styles/{,*/}*.{scss,sass}',
                '<%= core.app %>/geographyspin/clientlibs/geographyspin-core/styles/core/{,*/}*.{scss,sass}',
                '<%= core.app %>/sciencespin2/clientlibs/sciencespin2-core/styles/{,*/}*.{scss,sass}',
                '<%= core.app %>/sciencespin2/clientlibs/sciencespin2-core/styles/core/{,*/}*.{scss,sass}',
                '<%= core.app %>/sciencespin36/clientlibs/sciencespin36-core/styles/{,*/}*.{scss,sass}',
                '<%= core.app %>/sciencespin36/clientlibs/sciencespin36-core/styles/core/{,*/}*.{scss,sass}',
                '<%= core.app %>/math/clientlibs/math-core/styles/{,*/}*.{scss,sass}',
                '<%= core.app %>/math/clientlibs/math-core/styles/core/{,*/}*.{scss,sass}',
                '<%= core.app %>/dynamath/clientlibs/dynamath-core/styles/{,*/}*.{scss,sass}',
                '<%= core.app %>/dynamath/clientlibs/dynamath-core/styles/core/{,*/}*.{scss,sass}',
                '<%= core.app %>/action/clientlibs/action-core/styles/{,*/}*.{scss,sass}',
                '<%= core.app %>/action/clientlibs/action-core/styles/core/{,*/}*.{scss,sass}',
                '<%= core.app %>/upfront/clientlibs/upfront-core/styles/{,*/}*.{scss,sass}',
                '<%= core.app %>/upfront/clientlibs/upfront-core/styles/core/{,*/}*.{scss,sass}'
                ],
        tasks: ['sass:server']
      }
    },
      
    // Run some tasks in parallel to speed up the build process
    concurrent: {
      server: [
        'sass:server'
      ],
      superscience:[
        'sass:superscience'
      ],
      junior:[
        'sass:junior'
      ],
      scholasticnews3:[
        'sass:scholasticnews3'
      ],
      scholasticnews4:[
        'sass:scholasticnews4'
      ],
      scholasticnews56:[
        'sass:scholasticnews56'
      ],
      geographyspin:[
        'sass:geographyspin'
      ],
      sciencespin2:[
        'sass:sciencespin2'
      ],
      sciencespin36:[
        'sass:sciencespin36'
      ],
      math:[
        'sass:math'
      ],
      dynamath:[
        'sass:dynamath'
      ],
      action:[
        'sass:action'
      ],
      upfront:[
        'sass:upfront'
      ]
    }
  }); 
  
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-sync');

  grunt.registerTask('build', 'Compile then start a connect web server', function (target) {     
      grunt.task.run([
        'concurrent:server'
      ]); 
  });
    
  grunt.registerTask('superscience', 'Compile then start a connect web server', function (target) {
      grunt.task.run([
        'concurrent:superscience'     
      ]); 
  });
    
  grunt.registerTask('junior', 'Compile then start a connect web server', function (target) {
      grunt.task.run([
        'concurrent:junior'
     
      ]); 
  });
    
  grunt.registerTask('scholasticnews3', 'Compile then start a connect web server', function (target) {
      grunt.task.run([
        'concurrent:scholasticnews3'     
      ]); 
  });

  grunt.registerTask('scholasticnews4', 'Compile then start a connect web server', function (target) {
      grunt.task.run([
        'concurrent:scholasticnews4'     
      ]); 
  });

  grunt.registerTask('scholasticnews56', 'Compile then start a connect web server', function (target) {
      grunt.task.run([
        'concurrent:scholasticnews56'     
      ]); 
  });

  grunt.registerTask('geographyspin', 'Compile then start a connect web server', function (target) {
      grunt.task.run([
        'concurrent:geographyspin'     
      ]); 
  });

  grunt.registerTask('sciencespin2', 'Compile then start a connect web server', function (target) {
      grunt.task.run([
        'concurrent:sciencespin2'     
      ]); 
  });

  grunt.registerTask('sciencespin36', 'Compile then start a connect web server', function (target) {
      grunt.task.run([
        'concurrent:sciencespin36'     
      ]); 
  });

  grunt.registerTask('math', 'Compile then start a connect web server', function (target) {
      grunt.task.run([
        'concurrent:math'     
      ]); 
  });

  grunt.registerTask('dynamath', 'Compile then start a connect web server', function (target) {
      grunt.task.run([
        'concurrent:dynamath'     
      ]); 
  });

  grunt.registerTask('action', 'Compile then start a connect web server', function (target) {
      grunt.task.run([
        'concurrent:action'     
      ]); 
  });

  grunt.registerTask('upfront', 'Compile then start a connect web server', function (target) {
      grunt.task.run([
        'concurrent:upfront'     
      ]); 
  });
    
  grunt.registerTask('develop', 'Compile then start a connect web server', function (target) {     
      grunt.task.run([
        'concurrent:server',
        'watch'
        
      ]); 
  });

};
