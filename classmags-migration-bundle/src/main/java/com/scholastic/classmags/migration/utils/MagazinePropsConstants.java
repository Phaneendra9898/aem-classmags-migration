package com.scholastic.classmags.migration.utils;

/**
 * The Class MagazinePropsConstants.
 */
public class MagazinePropsConstants {

    /** The Constant SCIENCE_WORLD. */
    public static final String SCIENCE_WORLD = "scienceworld";

    /** The Constant SUPERSCIENCE. */
    public static final String SUPERSCIENCE = "superscience";

    /** The Constant ACTION. */
    public static final String ACTION = "action";

    /** The Constant ART. */
    public static final String ART = "art";

    /** The Constant CHOICES. */
    public static final String CHOICES = "choices";

    /** The Constant DYNA_MATH. */
    public static final String DYNA_MATH = "dynamath";

    /** The constant GEOGRAPHY_SPIN. */
    public static final String GEOGRAPHY_SPIN = "geographyspin";

    /** The constant JUNIOR_SCHOLASTIC. */
    public static final String JUNIOR_SCHOLASTIC = "junior";

    /** The constant LETS_FIND_OUT. */
    public static final String LETS_FIND_OUT = "letsfindout";

    /** The constant MATH. */
    public static final String MATH = "math";

    /** The constant MY_BIG_WORLD. */
    public static final String MY_BIG_WORLD = "mybigworld";

    /** The constant SCHOLASTIC_NEWS_ONE. */
    public static final String SCHOLASTIC_NEWS_ONE = "sn1";

    /** The constant SCHOLASTIC_NEWS_TWO. */
    public static final String SCHOLASTIC_NEWS_TWO = "sn2";

    /** The constant SCHOLASTIC_NEWS_THREE. */
    public static final String SCHOLASTIC_NEWS_THREE = "sn3";

    /** The constant SCHOLASTIC_NEWS_FOUR. */
    public static final String SCHOLASTIC_NEWS_FOUR = "sn4";

    /** The constant SCHOLASTIC_NEWS_FIVE_SIX. */
    public static final String SCHOLASTIC_NEWS_FIVE_SIX = "sn56";

    /** The constant SCIENCE_SPIN_K_ONE. */
    public static final String SCIENCE_SPIN_K_ONE = "sciencespink1";

    /** The constant SCIENCE_SPIN_K_TWO. */
    public static final String SCIENCE_SPIN_TWO = "sciencespin2";

    /** The constant SCIENCE_SPIN_THREE_TO_SIX. */
    public static final String SCIENCE_SPIN_THREE_TO_SIX = "sciencespin36";

    /** The constant SCOPE. */
    public static final String SCOPE = "scope";

    /** The constant STORYWORKS. */
    public static final String STORYWORKS = "storyworks";

    /** The constant STORYWORKS_JUNIOR. */
    public static final String STORYWORKS_JUNIOR = "storyworksjr";

    /** The constant UPFRONT. */
    public static final String UPFRONT = "upfront";

    /** The Constant SCIENCE_WORLD_NAME. */
    public static final String SCIENCE_WORLD_NAME = "Science World";

    /** The Constant SUPERSCIENCE_NAME. */
    public static final String SUPERSCIENCE_NAME = "SuperScience";

    /** The Constant ACTION_NAME. */
    public static final String ACTION_NAME = "Action";

    /** The Constant ART_NAME. */
    public static final String ART_NAME = "Scholastic Art";

    /** The Constant CHOICES_NAME. */
    public static final String CHOICES_NAME = "Choices";

    /** The Constant DYNA_MATH_NAME. */
    public static final String DYNA_MATH_NAME = "DynaMath";

    /** The Constant GEOGRAPHY_SPIN_NAME. */
    public static final String GEOGRAPHY_SPIN_NAME = "Geography Spin";

    /** The Constant JUNIOR_SCHOLASTIC_NAME. */
    public static final String JUNIOR_SCHOLASTIC_NAME = "Junior Scholastic";

    /** The Constant LETS_FIND_OUT_NAME. */
    public static final String LETS_FIND_OUT_NAME = "Let's Find Out";

    /** The Constant MATH_NAME. */
    public static final String MATH_NAME = "Scholastic Math";

    /** The Constant MY_BIG_WORLD_NAME. */
    public static final String MY_BIG_WORLD_NAME = "My Big World";

    /** The Constant SCHOLASTIC_NEWS_ONE_NAME. */
    public static final String SCHOLASTIC_NEWS_ONE_NAME = "Scholastic News 1";

    /** The Constant SCHOLASTIC_NEWS_TWO_NAME. */
    public static final String SCHOLASTIC_NEWS_TWO_NAME = "Scholastic News 2";

    /** The Constant SCHOLASTIC_NEWS_THREE_NAME. */
    public static final String SCHOLASTIC_NEWS_THREE_NAME = "Scholastic News 3";

    /** The Constant SCHOLASTIC_NEWS_FOUR_NAME. */
    public static final String SCHOLASTIC_NEWS_FOUR_NAME = "Scholastic News 4";

    /** The Constant SCHOLASTIC_NEWS_FIVE_SIX_NAME. */
    public static final String SCHOLASTIC_NEWS_FIVE_SIX_NAME = "Scholastic News 5/6";

    /** The Constant SCIENCE_SPIN_K_ONE_NAME. */
    public static final String SCIENCE_SPIN_K_ONE_NAME = "Science Spin K-1";

    /** The Constant SCIENCE_SPIN_TWO_NAME. */
    public static final String SCIENCE_SPIN_TWO_NAME = "Science Spin 2";

    /** The Constant SCIENCE_SPIN_THREE_TO_SIX_NAME. */
    public static final String SCIENCE_SPIN_THREE_TO_SIX_NAME = "Science Spin 3-6";

    /** The Constant SCOPE_NAME. */
    public static final String SCOPE_NAME = "Scope";

    /** The Constant STORYWORKS_NAME. */
    public static final String STORYWORKS_NAME = "Storyworks";

    /** The Constant STORYWORKS_JUNIOR_NAME. */
    public static final String STORYWORKS_JUNIOR_NAME = "Storyworks Jr.";

    /** The Constant UPFRONT_NAME. */
    public static final String UPFRONT_NAME = "The New York Times Upfront";

    /** The Constant MARKETING_SCIENCE_WORLD. */
    public static final String MARKETING_SCIENCE_WORLD = "mkt-sw-world";

    /** The Constant MARKETING_SCIENCE_WORLD. */
    public static final String SCIENCE_WORLD_MKT = "scienceworld_mkt";

    /** The Constant MARKETING_JUNIOR_SCHOLASTIC. */
    public static final String JUNIOR_SCHOLASTIC_MKT = "junior_mkt";
    
    /** The constant SCHOLASTIC_NEWS_THREE. */
    public static final String SCHOLASTIC_NEWS_THREE_MKT = "sn3_mkt";

    /** The constant SCHOLASTIC_NEWS_FOUR. */
    public static final String SCHOLASTIC_NEWS_FOUR_MKT = "sn4_mkt";

    /** The constant SCHOLASTIC_NEWS_FIVE_SIX. */
    public static final String SCHOLASTIC_NEWS_FIVE_SIX_MKT = "sn56_mkt";
    
    /** The Constant DYNA_MATH_NAME. */
    public static final String DYNA_MATH_NAME_MKT = "dynamath_mkt";
    
    /** The constant SCHOLASTIC_NEWS */
    public static final String SCHOLASTIC_NEWS = "sn";
    
    /** The constant SCHOLASTIC_NEWS */
    public static final String SCHOLASTIC_NEWS_MKT = "sn_mkt";
		
    /** The constant MATH. */
    public static final String MATH_MKT = "math_mkt";
	
	/** The Constant ACTION. */
    public static final String ACTION_MKT = "action_mkt";
	
	 /** The Constant SUPERSCIENCE. */
    public static final String SUPERSCIENCE_MKT = "superscience_mkt";

    /**
     * Instantiates a new magazine props constants.
     */
    private MagazinePropsConstants() {

    }
}
