package com.scholastic.classmags.migration.controllers;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.models.GlobalNavSubscribe;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * Use Class for GlobalNavSubscribe.
 */
public class GlobalNavSubscribeUse extends WCMUsePojo {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( GlobalNavSubscribeUse.class );

    /** The global nav subscribe. */
    private GlobalNavSubscribe globalNavSubscribe;

    /** The user logged out flag. */
    private boolean userLoggedOutFlag;

    /**
     * Checks if is user logged out flag.
     *
     * @return the userLoggedOutFlag
     */
    public boolean isUserLoggedOutFlag() {
        return userLoggedOutFlag;
    }

    /**
     * Gets the global nav subscribe.
     *
     * @return the globalNavSubscribe
     */
    public GlobalNavSubscribe getGlobalNavSubscribe() {
        return globalNavSubscribe;
    }

    /**
     * Sets the global nav subscribe.
     *
     * @param globalNavSubscribe
     *            the globalNavSubscribe to set
     */
    public void setGlobalNavSubscribe( GlobalNavSubscribe globalNavSubscribe ) {
        this.globalNavSubscribe = globalNavSubscribe;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() throws Exception {
        LOG.info( "Inside the Activate Method for GlobalNavSubscribeUse" );
        SlingHttpServletRequest request = getRequest();
        LOG.info( "Request Details: " + request );
        if ( request != null ) {
            globalNavSubscribe = request.adaptTo( GlobalNavSubscribe.class );
            LOG.info( "GlobalNavSubscribe Details: " + globalNavSubscribe );
            userLoggedOutFlag = StringUtils.equals( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS, RoleUtil.getUserRole( request ) );
        }
    }

}
