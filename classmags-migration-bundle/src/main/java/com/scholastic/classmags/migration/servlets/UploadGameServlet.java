package com.scholastic.classmags.migration.servlets;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.services.UploadGameService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * Servlet used to get a zip file and a path in the DAM, with a HTML5 or Flash
 * game compressed, in order to unzip the files/folders within the zip and
 * create folder structure of the game in the DAM.
 */
@SlingServlet( paths = { "/bin/classmags/migration/upload/game" } )
public class UploadGameServlet extends SlingAllMethodsServlet {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1496871066150117433L;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( UploadGameServlet.class );

    /** The upload game service. */
    @Reference
    private UploadGameService uploadGameService;

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache
     * .sling.api.SlingHttpServletRequest,
     * org.apache.sling.api.SlingHttpServletResponse)
     */
    @Override
    protected void doGet( SlingHttpServletRequest request, SlingHttpServletResponse response ) {
        doPost( request, response );
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache
     * .sling.api.SlingHttpServletRequest,
     * org.apache.sling.api.SlingHttpServletResponse)
     */
    @Override
    protected void doPost( SlingHttpServletRequest request, SlingHttpServletResponse response ) {
        String responseMessage = null;
        // Create json object to send response
        JSONObject jsonResponseObject = new JSONObject();

        try {

            RequestParameter gameFile = request.getRequestParameter( "uploadGame" );

            String damPath = request.getParameter( "damPath" );
            LOG.info( "gameFile is-------- " + gameFile + "-----and damPath is------- " + damPath );

            if ( gameFile != null && !gameFile.isFormField() && StringUtils.isNotBlank( damPath ) ) {
                LOG.info( "A file is coming with the request and " + "gameFile.getFileName() is "
                        + gameFile.getFileName() );
                InputStream gameFileStream = gameFile.getInputStream();
                responseMessage = uploadGameService.uploadGame( gameFileStream, damPath, gameFile.getFileName() );
                if ( responseMessage == null ) {
                    response.setStatus( HttpServletResponse.SC_INTERNAL_SERVER_ERROR );
                    jsonResponseObject.put( ClassMagsMigrationConstants.SUCCESS, false );
                    responseMessage = "Error in uploading games.";
                    jsonResponseObject.put( ClassMagsMigrationConstants.ERRORS, responseMessage );
                } else {
                    jsonResponseObject.put( ClassMagsMigrationConstants.SUCCESS, true );
                    jsonResponseObject.put( "responseMessage", responseMessage );
                }

            } else {
                responseMessage = "The zip file or the dam path are not present.<br />"
                        + " Please check the zip file and path and try to upload the game again";
                response.setStatus( HttpServletResponse.SC_BAD_REQUEST );
                jsonResponseObject.put( ClassMagsMigrationConstants.SUCCESS, false );
                jsonResponseObject.put( ClassMagsMigrationConstants.ERRORS, responseMessage );
            }

            response.setContentType( "text/html" );
            response.getWriter().write( jsonResponseObject.toString() );
        } catch ( IOException e ) {
            LOG.error( "Error creating game in UploadGameServlet: " + e );
        } catch ( JSONException e ) {
            LOG.error( "Error creating json response object in UploadGameServlet: " + e );
        }
    }
}
