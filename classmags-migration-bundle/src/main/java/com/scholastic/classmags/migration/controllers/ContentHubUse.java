package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.scholastic.classmags.migration.models.SearchFilter;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class ContentHubUse.
 */
public class ContentHubUse extends WCMUsePojo {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( ContentHubUse.class );

    private List< SearchFilter > subjectFilters = new ArrayList< SearchFilter >();

    private List< SearchFilter > contentFilters = new ArrayList< SearchFilter >();

    private String currentPagePath;

    private String subjects;

    private String contentTypes;

    private String subjectOperator;
    
    boolean isAuthor;
    
    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() throws Exception {

        currentPagePath = getCurrentPage().getPath();
        
        MagazineProps magazineProps = getSlingScriptHelper().getService( MagazineProps.class );
        String name = magazineProps.getMagazineName( currentPagePath );
        
        ResourceResolver serviceResolver = getServiceResourceResolver(
                getSlingScriptHelper().getService( ResourceResolverFactory.class ) );
        
        isAuthor  = isAuthorMode( getSlingScriptHelper().getService( SlingSettingsService.class ) );
        
        subjectFilters = CommonUtils.getSearchFilters( serviceResolver,
                    ClassMagsMigrationConstants.CLASS_MAGS_TAG_ROOT_PATH + name);

        Resource component = getResource().getChild( ClassMagsMigrationConstants.NODE_NAME_CONTENT_TYPES );
        
        contentFilters = CommonUtils.getContentTypeFilters( component, getRequest(), isAuthor );
        
        String[] subjectTags =  getProperties().get( "subjectTags", String[].class );
        
        subjects = getTagTitles( subjectTags, serviceResolver );
        
        String[] contentTags = getProperties().get( "contentTags", String[].class );
        
        if(ArrayUtils.isNotEmpty(contentTags)){
        	contentFilters = Collections.emptyList();
        }
        
        contentTypes = getTagTitles( contentTags, serviceResolver );
        
        subjectOperator = ( String ) getProperties().get( "subjectOperator", String.class );
        
        
        if(serviceResolver !=null && serviceResolver.isLive()){
            serviceResolver.close();
        }
    }

    public String getTagTitles(String[] tags, ResourceResolver resourceResolver){
        String val = StringUtils.EMPTY;
        TagManager tagManager = resourceResolver.adaptTo( TagManager.class );
        if(ArrayUtils.isNotEmpty( tags )){
            for(String t: tags){
                Tag tag = tagManager.resolve( t );
                if(null!=tag){
                    val = val + tag.getTitle() + ",";
                }
            }
        }
        val = val.endsWith( "," )? val.substring( 0, val.lastIndexOf( "," ) ):val;
        tagManager = null;
        return val;
    }
    
    public List< SearchFilter > getSubjectFilters() {
        return subjectFilters;
    }

    public List< SearchFilter > getContentFilters() {
        return contentFilters;
    }

    /**
     * This method will get service resolver for the accessing the tags
     * 
     * @param ResourceResolverFacatory
     * @return service ResourceResolver
     */
    private ResourceResolver getServiceResourceResolver( ResourceResolverFactory resourceResolverFactory ) {
        ResourceResolver serviceResolver = null;
        final Map< String, Object > authInfo = Collections.singletonMap( ResourceResolverFactory.SUBSERVICE,
                ( Object ) ClassMagsMigrationConstants.READ_SERVICE );
        try {
            serviceResolver = resourceResolverFactory.getServiceResourceResolver( authInfo );
        } catch ( LoginException e ) {
            LOG.error( "Error getting service resource resolver : {}", e );
        }
        return serviceResolver;
    }
    
    public static boolean isAuthorMode(SlingSettingsService slingSettingsService){
        boolean isAuthor = true;
        Set<String> runModes = slingSettingsService.getRunModes();
        if(runModes.contains( "author" )){
            isAuthor=true;
        }else{
            isAuthor=false;
        }
        return isAuthor;
    }
    
    public String getCurrentPagePath() {
        return currentPagePath;
    }

    public String getSubjects() {
        return subjects;
    }

    public String getContentTypes() {
        return contentTypes;
    }

    public String getSubjectOperator() {
        return subjectOperator;
    }

}
