package com.scholastic.classmags.migration.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * The Class VideoArchive.
 */
public class VideoArchive {

    /** The id. */
    private long id;

    /** The name. */
    private String name;

    /** The short description. */
    private String shortDescription;

    /** The long description. */
    private String longDescription;

    /** The creation date. */
    private String creationDate;

    /** The published date. */
    private String publishedDate;

    /** The start date. */
    private String startDate;

    /** The tags. */
    private List< String > tags;

    /** The video still URL. */
    private String videoStillURL;

    /** The thumbnail URL. */
    private String thumbnailURL;

    /** The flv URL. */
    @SerializedName( "FLVURL" )
    private String flvURL;

    /** The renditions. */
    private List< BrightcoveVideoRenditions > renditions;

    /** The captioning. */
    private BrightcoveVideoCaption captioning;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the new id
     */
    public void setId( long id ) {
        this.id = id;
    }

    /**
     * Gets the name.
     * 
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     * 
     * @param name
     *            the new name
     */
    public void setName( String name ) {
        this.name = name;
    }

    /**
     * Gets the short description.
     * 
     * @return the short description
     */
    public String getShortDescription() {
        return shortDescription;
    }

    /**
     * Sets the short description.
     * 
     * @param shortDescription
     *            the new short description
     */
    public void setShortDescription( String shortDescription ) {
        this.shortDescription = shortDescription;
    }

    /**
     * Gets the long description.
     * 
     * @return the long description
     */
    public String getLongDescription() {
        return longDescription;
    }

    /**
     * Sets the long description.
     * 
     * @param longDescription
     *            the new long description
     */
    public void setLongDescription( String longDescription ) {
        this.longDescription = longDescription;
    }

    /**
     * Gets the creation date.
     * 
     * @return the creation date
     */
    public String getCreationDate() {
        return creationDate;
    }

    /**
     * Sets the creation date.
     * 
     * @param creationDate
     *            the new creation date
     */
    public void setCreationDate( String creationDate ) {
        this.creationDate = creationDate;
    }

    /**
     * Gets the published date.
     * 
     * @return the published date
     */
    public String getPublishedDate() {
        return publishedDate;
    }

    /**
     * Sets the published date.
     * 
     * @param publishedDate
     *            the new published date
     */
    public void setPublishedDate( String publishedDate ) {
        this.publishedDate = publishedDate;
    }

    /**
     * Gets the start date.
     * 
     * @return the start date
     */
    public String getStartDate() {
        return startDate;
    }

    /**
     * Sets the start date.
     * 
     * @param startDate
     *            the new start date
     */
    public void setStartDate( String startDate ) {
        this.startDate = startDate;
    }

    /**
     * Gets the tags.
     * 
     * @return the tags
     */
    public List< String > getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * 
     * @param tags
     *            the new tags
     */
    public void setTags( List< String > tags ) {
        this.tags = tags;
    }

    /**
     * Gets the video still URL.
     * 
     * @return the video still URL
     */
    public String getVideoStillURL() {
        return videoStillURL;
    }

    /**
     * Sets the video still URL.
     * 
     * @param videoStillURL
     *            the new video still URL
     */
    public void setVideoStillURL( String videoStillURL ) {
        this.videoStillURL = videoStillURL;
    }

    /**
     * Gets the thumbnail URL.
     * 
     * @return the thumbnail URL
     */
    public String getThumbnailURL() {
        return thumbnailURL;
    }

    /**
     * Sets the thumbnail URL.
     * 
     * @param thumbnailURL
     *            the new thumbnail URL
     */
    public void setThumbnailURL( String thumbnailURL ) {
        this.thumbnailURL = thumbnailURL;
    }

    /**
     * Gets the flv URL.
     * 
     * @return the flv URL
     */
    public String getFlvURL() {
        return flvURL;
    }

    /**
     * Sets the flv URL.
     * 
     * @param flvURL
     *            the new flv URL
     */
    public void setFlvURL( String flvURL ) {
        this.flvURL = flvURL;
    }

    /**
     * Gets the renditions.
     * 
     * @return the renditions
     */
    public List< BrightcoveVideoRenditions > getRenditions() {
        return renditions;
    }

    /**
     * Sets the renditions.
     * 
     * @param renditions
     *            the new renditions
     */
    public void setRenditions( List< BrightcoveVideoRenditions > renditions ) {
        this.renditions = renditions;
    }

    /**
     * Gets the captioning.
     * 
     * @return the captioning
     */
    public BrightcoveVideoCaption getCaptioning() {
        return captioning;
    }

    /**
     * Sets the captioning.
     * 
     * @param captioning
     *            the new captioning
     */
    public void setCaptioning( BrightcoveVideoCaption captioning ) {
        this.captioning = captioning;
    }

}