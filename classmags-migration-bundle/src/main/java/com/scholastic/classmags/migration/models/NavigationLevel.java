package com.scholastic.classmags.migration.models;

/**
 * The Class NavigationLevel.
 */
public class NavigationLevel {

    /** The link title. */
    private String linkTitle;

    /** The link path. */
    private String linkPath;

    /** The navigation type. */
    private String navigationType;

    /**
     * Gets the link title.
     *
     * @return the link title
     */
    public String getLinkTitle() {
        return linkTitle;
    }
    
    /**
     * Sets the link title.
     * 
     * @param linkTitle The link title.
     */
    public final void setLinkTitle( final String linkTitle ) {
        this.linkTitle = linkTitle;
    }
    
    /**
     * Gets the link path.
     *
     * @return the link path
     */
    public String getLinkPath() {
        return linkPath;
    }
    
    /**
     * Sets the link path.
     * 
     * @param linkPath The link path.
     */
    public final void setLinkPath( final String linkPath ) {
        this.linkPath = linkPath;
    }
    
    /**
     * Gets the navigation type.
     *
     * @return the navigation type
     */
    public String getNavigationType() {
        return navigationType;
    }
    
    /**
     * Sets the navigation type.
     * 
     * @param linkPath The navigation type.
     */
    public final void setNavigationType( final String navigationType ) {
        this.navigationType = navigationType;
    }
}
