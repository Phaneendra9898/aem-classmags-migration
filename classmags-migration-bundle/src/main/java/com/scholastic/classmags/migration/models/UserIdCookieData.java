package com.scholastic.classmags.migration.models;

/**
 * The Class UserIdCookieData.
 */
public class UserIdCookieData {

    /** The obj U id header identifiers. */
    private UIdHeaderIdentifiers objUIdHeaderIdentifiers;

    /** The obj U id header launch context. */
    private UIdHeaderLaunchContext objUIdHeaderLaunchContext;

    /** The obj U id header org context. */
    private UIdHeaderOrgContext objUIdHeaderOrgContext;

    /**
     * Gets the obj U id header identifiers.
     *
     * @return the objUIdHeaderIdentifiers
     */
    public UIdHeaderIdentifiers getObjUIdHeaderIdentifiers() {
        return objUIdHeaderIdentifiers;
    }

    /**
     * Sets the obj U id header identifiers.
     *
     * @param objUIdHeaderIdentifiers
     *            the objUIdHeaderIdentifiers to set
     */
    public void setObjUIdHeaderIdentifiers( UIdHeaderIdentifiers objUIdHeaderIdentifiers ) {
        this.objUIdHeaderIdentifiers = objUIdHeaderIdentifiers;
    }

    /**
     * Gets the obj U id header launch context.
     *
     * @return the objUIdHeaderLaunchContext
     */
    public UIdHeaderLaunchContext getObjUIdHeaderLaunchContext() {
        return objUIdHeaderLaunchContext;
    }

    /**
     * Sets the obj U id header launch context.
     *
     * @param objUIdHeaderLaunchContext
     *            the objUIdHeaderLaunchContext to set
     */
    public void setObjUIdHeaderLaunchContext( UIdHeaderLaunchContext objUIdHeaderLaunchContext ) {
        this.objUIdHeaderLaunchContext = objUIdHeaderLaunchContext;
    }

    /**
     * Gets the obj U id header org context.
     *
     * @return the objUIdHeaderOrgContext
     */
    public UIdHeaderOrgContext getObjUIdHeaderOrgContext() {
        return objUIdHeaderOrgContext;
    }

    /**
     * Sets the obj U id header org context.
     *
     * @param objUIdHeaderOrgContext
     *            the objUIdHeaderOrgContext to set
     */
    public void setObjUIdHeaderOrgContext( UIdHeaderOrgContext objUIdHeaderOrgContext ) {
        this.objUIdHeaderOrgContext = objUIdHeaderOrgContext;
    }
}
