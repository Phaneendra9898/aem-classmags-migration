package com.scholastic.classmags.migration.services;

import java.util.List;
import java.util.Map;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.ResourcesObject;

/**
 * The Interface ContentTileFlagDataService.
 */
public interface ContentTileFlagDataService {

    /**
     * Configure article page and flag data.
     *
     * @param issuePagePath
     *            the issue page path
     * @param resources
     *            the resources
     * @throws ClassmagsMigrationBaseException
     */
    public void configureArticlePageAndFlagData( String issuePagePath,
            Map< String, List< ResourcesObject > > resources ) throws ClassmagsMigrationBaseException;
}
