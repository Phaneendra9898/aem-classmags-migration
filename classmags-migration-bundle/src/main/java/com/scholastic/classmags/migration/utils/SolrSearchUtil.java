package com.scholastic.classmags.migration.utils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.solr.client.solrj.response.IntervalFacet;

/**
 * SolrSearchUtil Class
 * 
 *
 */
public final class SolrSearchUtil {

	/**
	 * Solr OR query contruction
	 * 
	 * @param params
	 * @return String
	 */
	public static String solrOrConstruction(String... params) {
		StringBuilder sb = new StringBuilder();
		if (params != null && params.length > 0) {
			sb.append("(");
			for (int i = 0; i < params.length; i++) {
				if (i == 0) {
					sb.append(params[0]);
				} else {
					sb.append(" OR " + params[i]);

				}
			}
			sb.append(")");
		}
		return sb.toString();

	}

	/**
	 * Escapes Solr special characters
	 * 
	 * @param text
	 * @return String
	 */
	public static String escapeSolrCharacters(String text) {
		Set<String> groups = new HashSet<String>();
		Pattern p = Pattern
				.compile(SolrSearchClassMagsConstants.SORL_ESPECIAL_CHARACTERS_PATTERN);
		Matcher m = p.matcher(text);
		while (m.find()) {
			groups.add(m.group());

		}
		String escapedText = text.replace(SolrSearchClassMagsConstants.BACKSLASH,
                SolrSearchClassMagsConstants.BACKSLASH + SolrSearchClassMagsConstants.BACKSLASH);
		for (String string : groups) {
		    escapedText = escapedText.replace(string, SolrSearchClassMagsConstants.BACKSLASH + string);

		}
		return escapedText;

	}

	public static JSONArray getIntervalFacetCountByField(
			List<IntervalFacet.Count> intervalFacetEntries)
			throws JSONException {

		JSONObject attrInterval;
		JSONArray intervalFacetField = new JSONArray();

		for (IntervalFacet.Count iFcount : intervalFacetEntries) {
			attrInterval = new JSONObject();
			attrInterval.put("key", iFcount.getKey());
			attrInterval.put("count", iFcount.getCount());
			intervalFacetField.put(attrInterval);
		}

		return intervalFacetField;
	}

	public static String escapeChars(String text) {
		// Escape the following characters String escapeString = "+ - && || - ( ) { } [ ] ^ \" ~ * ? : \\";
		text.replace("&", "\\&");
		text.replace("\"", "\\\"");
		text.replace("+", "\\+");
		text.replace("(", "\\(");
		text.replace(")", "\\)");
		text.replace("{", "\\{");
		text.replace("}", "\\}");
		text.replace("[", "\\[");
		text.replace("]", "\\]");
		text.replace("^", "\\^");
		text.replace("~", "\\~");
		text.replace("*", "\\*");
		text.replace("?", "\\?");
		text.replace(":", "\\:");

		return text;

	}

}
