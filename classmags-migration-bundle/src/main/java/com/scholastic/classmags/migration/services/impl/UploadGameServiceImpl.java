package com.scholastic.classmags.migration.services.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.UnsupportedRepositoryOperationException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.AssetManager;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.commons.jcr.JcrUtil;
import com.scholastic.classmags.migration.services.UploadGameService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class UploadGameServiceImpl.
 */
@Service( value = UploadGameService.class )
@Component( immediate = true, metatype = true, label = "Game Upload Service", description = "This service allows to upload a zip file and uncompress it "
        + "in the DAM, creating the game structure and unzipping files and folders " + "for HTML5 or Flash games" )
public class UploadGameServiceImpl implements UploadGameService {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( UploadGameServiceImpl.class );

    /** Size of the buffer to read/write data. */
    private static final int BUFFER_SIZE = 5 * 1024;

    private static final String ERROR_UNZIP_GAME = "Error unzipping game: ";

    private static final String ERROR_CREATE_FILE = "Error creating files in DAM: ";

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /**
     * Open the zip file uploaded by the user, extracts the files and folders in
     * the zip file and creates the files/folders (keeping the same structure)
     * in the DAM.
     * 
     * @param gameFile
     *            The zip file uploaded by the user
     * @param damPath
     *            The dam path where the file will be uncompressed
     * @param gameName
     *            The name of the zip file to create the main folder for the
     *            game in DAM
     * @return The response message with the result of the operation
     */
    @Override
    public String uploadGame( InputStream gameFile, String damPath, String gameName ) {
        String responseMessage = null;
        ZipInputStream zipInputStream = null;

        try {
            ResourceResolver resourceResolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                    ClassMagsMigrationConstants.WRITE_SERVICE );
            // Get the session
            Session session = resourceResolver.adaptTo( Session.class );
            AssetManager assetManager = resourceResolver.adaptTo( AssetManager.class );
            if ( session != null ) {
                String name = gameName.substring( 0, gameName.lastIndexOf( '.' ) ).replace( " ", "-" )
                        .replace( ".", "-" ).toLowerCase();

                Node jcrContentNode;

                StringBuilder gamePath = new StringBuilder( damPath );
                gamePath.append( "/" ).append( name );
                Node gameFolder = JcrUtil.createPath( gamePath.toString(), "sling:Folder", session );
                jcrContentNode = getJcrContentNode( gameFolder, JcrConstants.NT_UNSTRUCTURED );
                jcrContentNode.setProperty( "isGame", true );

                zipInputStream = new ZipInputStream( gameFile );
                ZipEntry zipEntry = zipInputStream.getNextEntry();

                // iterates over entries in the zip file
                buildGameNodes( zipEntry, zipInputStream, gamePath, session, assetManager );

                // Save the files/folders created in DAM
                session.save();

                responseMessage = "Game created/updated successfully in the DAM";
            }

        } catch ( IOException e ) {
            LOG.error( ERROR_UNZIP_GAME + e );
        } catch ( RepositoryException e ) {
            LOG.error( ERROR_CREATE_FILE + e );
        } finally {
            try {
                if ( zipInputStream != null ) {
                    zipInputStream.close();
                }
            } catch ( IOException e ) {
                LOG.error( "Error closing zipInputStream: " + e );
            }
        }

        return responseMessage;
    }

    /**
     * Builds the game nodes.
     * 
     * @param zipEntry
     *            the zip entry
     * @param zipInputStream
     *            the zip input stream
     * @param gamePath
     *            the game path
     * @param session
     *            the session
     * @param jcrContentNode
     *            the jcr content node
     */
    private void buildGameNodes( ZipEntry zipEntry, ZipInputStream zipInputStream, StringBuilder gamePath,
            Session session, AssetManager assetManager ) {
        String filePath;
        ZipEntry gameNodezipEntry = zipEntry;
        while ( gameNodezipEntry != null ) {
            filePath = gamePath.toString().concat( "/" ).concat( gameNodezipEntry.getName() );

            if ( gameNodezipEntry.isDirectory() ) {
                try {
                    JcrUtil.createPath( filePath, JcrConstants.NT_FOLDER, session );
                } catch ( RepositoryException e ) {
                    LOG.error( "Error creating files in DAM: " + e );
                }
            } else {
                InputStream fileInputStream;
                try {
                    fileInputStream = extractFile( zipInputStream );
                    Binary binaryFile = session.getValueFactory().createBinary( fileInputStream );
                    Node file = JcrUtil.createPath( filePath, JcrConstants.NT_FILE, session );
                    Node jcrContentNode = getJcrContentNode( file, JcrConstants.NT_RESOURCE );
                    jcrContentNode.setProperty( "jcr:data", binaryFile );

                    if ( filePath.contains( "index.html" ) ) {
                        String newFilePath = filePath.replace( "index.html", "index" );
                        assetManager.createAsset( newFilePath, fileInputStream, "text/html", true );
                    }

                } catch ( IOException | UnsupportedRepositoryOperationException e ) {
                    LOG.error( ERROR_UNZIP_GAME + e );
                } catch ( RepositoryException e ) {
                    LOG.error( ERROR_CREATE_FILE + e );
                }

            }

            try {
                zipInputStream.closeEntry();
                gameNodezipEntry = zipInputStream.getNextEntry();
            } catch ( IOException e ) {
                LOG.error( "Error unzipping game: " + e );
            }
            // close while
        }
    }

    /**
     * Gets the "jcr:content" node for the parentNode or creates this node with
     * the "jcr:primaryType" given.
     * 
     * @param parentNode
     *            The node to get or creating the jcr:content node
     * @param jcrContentNodeType
     *            The primary type in case the jcr:content node needs to be
     *            created
     * @return The jcr:content node
     * @throws RepositoryException
     *             the repository exception
     */
    private Node getJcrContentNode( Node parentNode, String jcrContentNodeType ) throws RepositoryException {
        String jcrContent = "jcr:content";
        Node jcrContentNode;

        if ( parentNode.hasNode( jcrContent ) ) {
            jcrContentNode = parentNode.getNode( jcrContent );
        } else {
            jcrContentNode = parentNode.addNode( jcrContent, jcrContentNodeType );
        }
        return jcrContentNode;
    }

    /**
     * Extracts a zip entry (bytes representing a file) and returns the file
     * (InputStream).
     * 
     * @param zipInputStream
     *            The ZipInputStream object representing the zip file
     * @return The file to create in the DAM
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private InputStream extractFile( ZipInputStream zipInputStream ) throws IOException {
        InputStream unzippedFile;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read;
        while ( ( read = zipInputStream.read( bytesIn ) ) != -1 ) {
            byteArrayOutputStream.write( bytesIn, 0, read );
        }
        byteArrayOutputStream.flush();

        unzippedFile = new ByteArrayInputStream( byteArrayOutputStream.toByteArray() );
        return unzippedFile;
    }
}
