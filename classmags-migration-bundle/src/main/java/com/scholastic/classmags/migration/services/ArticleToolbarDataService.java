package com.scholastic.classmags.migration.services;

import java.util.List;

import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;

/**
 * The Interface ArticleToolbarDataService.
 */
public interface ArticleToolbarDataService {

    /**
     * Fetch lexile levels.
     *
     * @param lexileLevelNodePath
     *            the lexile level node path
     * @return the list of lexile levels
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    public List< String > fetchLexileLevels( String lexileLevelNodePath ) throws ClassmagsMigrationBaseException;

    /**
     * Fetch parent issue page path.
     *
     * @param currentPage
     *            the current page
     * @return the string
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    public String fetchParentIssuePagePath( Page currentPage ) throws ClassmagsMigrationBaseException;

    /**
     * Fetchdigital issue link.
     *
     * @param pagePropertiesPath
     *            the page properties path
     * @return the string
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    public String fetchdigitalIssueLink( String pagePropertiesPath ) throws ClassmagsMigrationBaseException;
}
