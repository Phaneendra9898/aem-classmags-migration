package com.scholastic.classmags.migration.social.api;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;

import com.adobe.cq.social.scf.OperationException;

/**
 * The Interface BookmarkService.
 */
public interface BookmarkService {

    /**
     * Gets the bookmark resource.
     *
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @return the bookmark resource
     * @throws OperationException
     *             the operation exception
     */
    Resource getBookmarkResource( SlingHttpServletRequest slingHttpServletRequest ) throws OperationException;

    /**
     * Delete bookmark resource.
     *
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @return the resource
     * @throws OperationException
     *             the operation exception
     */
    Resource deleteBookmarkResource( SlingHttpServletRequest slingHttpServletRequest ) throws OperationException;

    /**
     * Gets the all site bookmark resource.
     *
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @return the all site bookmark resource
     * @throws OperationException
     *             the operation exception
     */
    Resource getAllSiteBookmarkResource( SlingHttpServletRequest slingHttpServletRequest ) throws OperationException;

    /**
     * Gets the my bookmark resource.
     *
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @return the my bookmark resource
     * @throws OperationException
     *             the operation exception
     */
    Resource getMyBookmarksResource( SlingHttpServletRequest slingHttpServletRequest ) throws OperationException;

    /**
     * Delete user bookmark resource.
     *
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @return the resource
     * @throws OperationException
     *             the operation exception
     */
    Resource deleteUserBookmarkResource( SlingHttpServletRequest slingHttpServletRequest ) throws OperationException;
}
