package com.scholastic.classmags.migration.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.email.EmailService;
import com.day.cq.wcm.api.NameConstants;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.ValidationUtils;

@SlingServlet( label = "Email the editor end point", description = "Send an email to editor....", resourceTypes = {
        NameConstants.NT_PAGE }, selectors = "emailEditor", extensions = "html", methods = "POST" )
@Service( Servlet.class )
public class EmailEditorServlet extends SlingAllMethodsServlet {
    private static final long serialVersionUID = 7191701247337903268L;

    private static final Logger LOG = LoggerFactory.getLogger( EmailEditorServlet.class );

    private static final String TEMPLATE_PATH = "/etc/notification/email/html/scholastic/com.scholastic.classmags.migration.emailtheeditor/en.txt";

    /** The magazine props service. */
    @Reference
    private MagazineProps magazineProps;

    @Reference
    private EmailService emailService;

    private String magazineName;

    @Override
    protected void doPost( SlingHttpServletRequest request, SlingHttpServletResponse response )
            throws ServletException, IOException {

        Map< String, String > emailParams = new HashMap< String, String >();
        Resource resource = request.getResource();

        try {
            JSONObject jsonRequestObject = new JSONObject( request.getParameter( "data" ) );

            String subject = jsonRequestObject.optString( "subject" );

            if ( resource != null ) {
                magazineName = magazineProps.getMagazineName( resource.getPath() );
                emailParams.put( "magazineName", magazineName );

                if ( StringUtils.isNotEmpty( subject ) ) {
                    emailParams.put( "subject", subject );
                } else {
                    emailParams.put( "subject", magazineName + " - EMAIL THE EDITOR SUBMISSION" );
                }
            }

            emailParams.put( "senderName", jsonRequestObject.optString( "name" ) );
            emailParams.put( "senderEmailAddress", jsonRequestObject.optString( "email" ) );
            emailParams.put( "body", jsonRequestObject.optString( "message" ) );

            if ( validateParams( emailParams ) ) {
                emailService.sendEmail( TEMPLATE_PATH, emailParams, jsonRequestObject.optString( "to" ) );
            } else {
                response.sendError( SlingHttpServletResponse.SC_BAD_REQUEST );
            }
        }

        catch ( Exception e ) {
            LOG.error( "Email Editor Error", e );
            response.sendError( SlingHttpServletResponse.SC_BAD_REQUEST );

        }
    }

    Boolean validateParams( Map< String, String > emailParams ) {

        for ( Map.Entry< String, String > entry : emailParams.entrySet() ) {
            if ( ValidationUtils.isFieldValid( entry.getValue() ) ) {
                continue;
            } else {
                return false;
            }
        }

        if ( !ValidationUtils.isFieldValidFormat( emailParams.get( "senderEmailAddress" ),
                ValidationUtils.VALID_EMAIL_ADDRESS_REGEX ) ) {
            return false;
        }
        return true;
    }
}