package com.scholastic.classmags.migration.services.impl;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Dictionary;
import java.util.Map;
import java.util.SortedMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.services.LtiLaunchVerificationStrategy;
import com.scholastic.classmags.migration.utils.OAuthUtil;

@Component(label = "Class Magazines - Oauth signature verification service",
description = "Verifies the incoming ouath signature and holds the mapping between the consumer key and the secret key",
immediate = false,
metatype = true)
@Service(LtiLaunchVerificationStrategy.class)
public class LtiLaunchVerificationStrategyImpl implements LtiLaunchVerificationStrategy {

    private static final Logger log = LoggerFactory.getLogger(LtiLaunchVerificationStrategyImpl.class);

    private static long MAX_DIFFERENCE = MILLISECONDS.convert( 1, MINUTES );
    private static final String HTTP_POST_METHOD = "POST";
    private static final String HMAC_SHA1 = "HmacSHA1";
    public static final String USER_ID = "user_id";
    public static final String CUSTOM_DP_LAUNCH = "custom_dp_launch";
    public static final String OAUTH_SIGNATURE = "oauth_signature";

    private static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

    @Property(label = "Configure the Mapping for Consumer Key To Secret Key",
            value = { "l0JkeOfrC2g2ALqB1dtghOjuxP8a=TZKQl9qToKK1dp2G2G4zcKp2meIa" },
            unbounded = PropertyUnbounded.ARRAY,
            cardinality = Integer.MAX_VALUE,
            description = "Configure the Mapping To Secret Key. Example : consumer-key=secret-key, aklsjdliajsdasdlkasjdljasdl=iopooiujojalsjdpoasjdmasndlkmanks. Please respect the order of the mapping. First match always wins")
    public static final String PROP_KEY_MAPPINGS = "cm.oauth.key.mapping";
    
    Map<String, String> keyMapping =null;
    private String ltiSharedSecret;

    @SuppressWarnings("rawtypes")
    @Activate
    protected final void activate(final ComponentContext componentContext) {
        try {
            Dictionary properties = componentContext.getProperties();
            this.keyMapping  = PropertiesUtil.toMap(properties.get(PROP_KEY_MAPPINGS), null);
        } catch (Exception e) {
            log.error("Error initializing the key map {}",e);
        }
    }
    
    @Override
    public boolean performOAuthSignatureValidation(SortedMap<String, String> map, String launchUrl, String signature, String consumerKey) {
        try {
            ltiSharedSecret = getSecretKey( consumerKey );
            log.debug( "Shared Secret Key :{}",ltiSharedSecret );
            log.debug( "Launch URL :{}",launchUrl );
            String oAuthSignature = sign(ltiSharedSecret, HMAC_SHA1, HTTP_POST_METHOD, launchUrl, map);
            log.info( "Generated oAuthSignature : {}",oAuthSignature );
            log.info( "Received OAuthSignature : {}",signature );
            return signature.equals(oAuthSignature);
        } catch (InvalidKeyException | NoSuchAlgorithmException | UnsupportedEncodingException
                | NullPointerException e) {
            log.error("error performing OAuthSignatureValidation", e);
        }
        return false;
    }

    private String sign(String secret, String algorithm, String method, String url,
            SortedMap<String, String> parameters)
            throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
        SecretKeySpec secretKeySpec = new SecretKeySpec((secret.concat(OAuthUtil.AMPERSAND)).getBytes(DEFAULT_CHARSET),
                algorithm);
        Mac mac = Mac.getInstance(secretKeySpec.getAlgorithm());
        mac.init(secretKeySpec);
        StringBuilder signatureBase = new StringBuilder(OAuthUtil.percentEncode(method));
        signatureBase.append(OAuthUtil.AMPERSAND);
        signatureBase.append(OAuthUtil.percentEncode(url));
        signatureBase.append(OAuthUtil.AMPERSAND);
        int count = 0;
        for (Map.Entry<String, String> entry : parameters.entrySet()) {
            count++;
            signatureBase.append(OAuthUtil.percentEncode(OAuthUtil.percentEncode(entry.getKey())));
            signatureBase.append(URLEncoder.encode(OAuthUtil.EQUAL, OAuthUtil.ENCODING));
            signatureBase.append(OAuthUtil.percentEncode(OAuthUtil.percentEncode(entry.getValue())));
            if (count < parameters.size()) {
                signatureBase.append(URLEncoder.encode(OAuthUtil.AMPERSAND, OAuthUtil.ENCODING));
            }
        }
        log.debug( "Final Percent Encoded String : {}",signatureBase );
        byte[] bytes = mac.doFinal(signatureBase.toString().getBytes(DEFAULT_CHARSET));
        byte[] encodedMacBytes = Base64.encodeBase64(bytes);
        return new String(encodedMacBytes, DEFAULT_CHARSET);
    }
    
    public String getSecretKey(String consumerKey) {
        if(null!=keyMapping && !keyMapping.isEmpty()){
            return keyMapping.get( consumerKey );
        }else{
            return StringUtils.EMPTY;
        }
    }

    protected void setLtiSharedSecret(String ltiSharedSecret) {
        this.ltiSharedSecret = ltiSharedSecret;
    }
    
    @Override
    public boolean performOAuthTimeStampValidation( long now, long timestamp ) {
        long difference = now - timestamp;
        if ( difference > MAX_DIFFERENCE ) {
            return false;
        }
        return true;
    }

}