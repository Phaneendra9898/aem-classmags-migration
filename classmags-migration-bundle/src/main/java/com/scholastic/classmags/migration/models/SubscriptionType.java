package com.scholastic.classmags.migration.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscriptionType {

	@SerializedName("newSubscription")
	@Expose
	private Boolean newSubscription;
	@SerializedName("isAutoRenewal")
	@Expose
	private Boolean isAutoRenewal;

	public Boolean getNewSubscription() {
		return newSubscription;
	}

	public void setNewSubscription(Boolean newSubscription) {
		this.newSubscription = newSubscription;
	}

	public Boolean getIsAutoRenewal() {
		return isAutoRenewal;
	}

	public void setIsAutoRenewal(Boolean isAutoRenewal) {
		this.isAutoRenewal = isAutoRenewal;
	}

}
