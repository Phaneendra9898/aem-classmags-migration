package com.scholastic.classmags.migration.utils;

import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.xss.XSSAPI;

/**
 * 
 * @author vivdesai
 *
 */
public final class ValidationUtils {
    
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
            Pattern.CASE_INSENSITIVE);
	
	/**
	 * 
	 * @param fieldValue
	 * @return
	 */
	public static boolean isFieldValid(String fieldValue) {
		boolean valid = false;
		if (StringUtils.isNotBlank(fieldValue)) {
			valid = true;
		}
		return valid;
	}

	/**
	 * 
	 * @param resourceResolver
	 * @param text
	 * @return
	 */
	public static String getFilteredText(ResourceResolver resourceResolver, String text) {
		XSSAPI xssAPI = resourceResolver.adaptTo(XSSAPI.class);
		return xssAPI.encodeForHTML(text);
	}
	
	/**
	 * 
	 * @param fieldValue
	 * @param pattern
	 * @return
	 */
	public static boolean isFieldValidFormat(String fieldValue, Pattern pattern) {
		boolean valid = false;
		if (isFieldValid(fieldValue)) {
			if (pattern.matcher(fieldValue).matches()) {
				valid = true;
			}
		}
		return valid;
	}

}
