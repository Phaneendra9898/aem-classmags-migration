package com.scholastic.classmags.migration.controllers;

import java.util.HashMap;
import java.util.Map;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

public class SubscribeBillingUse extends WCMUsePojo {

	private static final Logger LOG = LoggerFactory.getLogger( SubscribeBillingUse.class );

	private static final String COMPONENT_RESOURCE_TYPE = "scholastic/classroom-magazines-migration/components/content/CM-Marketing/marketing-SubscribeBilling";

	@Override
	public void activate() throws Exception {
		LOG.debug("Inside activate");
		LOG.debug("Exiting activate");

	}

	public ValueMap getValueMap() {
		ValueMap valueMap = null;
		ResourceResolver resolver = getResourceResolver();
		Session session = resolver.adaptTo(Session.class);
		String pagePath = getCurrentPage().getPath();
		QueryBuilder queryBuilder = getSlingScriptHelper().getService(QueryBuilder.class);
		final Map<String, String> map = new HashMap<String, String>();
		map.put("path", pagePath);
		map.put("p.limit", "-1");
		map.put("property", "sling:resourceType");
		map.put("property.value", COMPONENT_RESOURCE_TYPE);
		Query query = queryBuilder.createQuery(PredicateGroup.create(map), session);
		SearchResult result = query.getResult();
		for (Hit hit : result.getHits()) {
			try {
				Resource subscriptionResource = hit.getResource();
				valueMap = subscriptionResource.getValueMap();
			} catch (RepositoryException e) {
				LOG.error("Repository exception", e);
			}
		}
		return valueMap;
		
	}

}


