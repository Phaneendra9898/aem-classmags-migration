package com.scholastic.classmags.migration.models;

/**
 * The Class UIdOrgContextSubHeaderEntitlements.
 */
public class UIdOrgContextSubHeaderEntitlements {

    /** The application code. */
    private String applicationCode;

    /** The status. */
    private String status;

    /** The group code. */
    private String groupCode;

    /** The id. */
    private String id;

    /** The name. */
    private String name;

    /** The thumbnail. */
    private String thumbnail;

    /** The url. */
    private String url;

    /**
     * Gets the application code.
     *
     * @return the applicationCode
     */
    public String getApplicationCode() {
        return applicationCode;
    }

    /**
     * Sets the application code.
     *
     * @param applicationCode
     *            the applicationCode to set
     */
    public void setApplicationCode( String applicationCode ) {
        this.applicationCode = applicationCode;
    }

    /**
     * Gets the status.
     *
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the status.
     *
     * @param status
     *            the status to set
     */
    public void setStatus( String status ) {
        this.status = status;
    }

    /**
     * Gets the group code.
     *
     * @return the groupCode
     */
    public String getGroupCode() {
        return groupCode;
    }

    /**
     * Sets the group code.
     *
     * @param groupCode
     *            the groupCode to set
     */
    public void setGroupCode( String groupCode ) {
        this.groupCode = groupCode;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the id to set
     */
    public void setId( String id ) {
        this.id = id;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name
     *            the name to set
     */
    public void setName( String name ) {
        this.name = name;
    }

    /**
     * Gets the thumbnail.
     *
     * @return the thumbnail
     */
    public String getThumbnail() {
        return thumbnail;
    }

    /**
     * Sets the thumbnail.
     *
     * @param thumbnail
     *            the thumbnail to set
     */
    public void setThumbnail( String thumbnail ) {
        this.thumbnail = thumbnail;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the url.
     *
     * @param url
     *            the url to set
     */
    public void setUrl( String url ) {
        this.url = url;
    }

}
