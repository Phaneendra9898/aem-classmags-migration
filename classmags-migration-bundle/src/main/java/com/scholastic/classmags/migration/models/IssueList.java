package com.scholastic.classmags.migration.models;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

public class IssueList {

    private List< Issue > issue = new ArrayList< >();

    /**
     * 
     * @return The issue
     */
    public List< Issue > getIssue() {
        return issue;
    }

    /**
     * 
     * @param issue
     *            The issue
     */
    public void setIssue( List< Issue > issue ) {
        this.issue = issue;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString( this );
    }
}
