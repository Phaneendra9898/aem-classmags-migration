package com.scholastic.classmags.migration.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.jsoup.Jsoup;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ArticleSideBar {

	@Inject
	@Named("sectionText")
	private String sectionText;

	@Inject
	@Named("title")
	private String sectionTitle;

	@Self
	private Resource resource;

	@PostConstruct
	public final void init() {
		this.sectionText = getPlaintext(this.sectionText);
	}

	public String getPlaintext(String text) {
		String plaintext = StringUtils.EMPTY;
		if (StringUtils.isNotBlank(text)) {
			plaintext = Jsoup.parse(text.trim()).text().concat(" ");
		}
		return plaintext;
	}

	public String getSectionText() {
		return sectionText;
	}

	public void setSectionText(String sectionText) {
		this.sectionText = sectionText;
	}

	public String getSectionTitle() {
		return sectionTitle;
	}

	public void setSectionTitle(String sectionTitle) {
		this.sectionTitle = sectionTitle;
	}

	
}
