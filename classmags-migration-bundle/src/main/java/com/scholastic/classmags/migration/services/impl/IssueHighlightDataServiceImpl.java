package com.scholastic.classmags.migration.services.impl;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONException;
import org.osgi.framework.Constants;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.ClassmagsMigrationErrorCodes;
import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.services.IssueHighlightDataService;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.services.TeachingResourcesService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class IssueHighlightDataServiceImpl.
 */
@Component( immediate = true, metatype = false )
@Service( IssueHighlightDataService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Issue Highlight Data Service" ) })
public class IssueHighlightDataServiceImpl implements IssueHighlightDataService {

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /** The property config service. */
    @Reference
    private PropertyConfigService propertyConfigService;

    /** The resource path config service. */
    @Reference
    private ResourcePathConfigService resourcePathConfigService;
    
    /** The magazine props service. */
    @Reference
    private MagazineProps magazineProps;
    
    /** The teaching resources service. */
    @Reference
    private TeachingResourcesService teachingResourcesService;
    
    @Override
    public List< ResourcesObject > fetchFeaturedResources( SlingHttpServletRequest request, String currentPath ) throws JSONException {

        Resource resource = CommonUtils
                .getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE )
                .getResource( currentPath.concat( "/featuredResources" ) );
        
        return teachingResourcesService.getFeaturedResource( request, resource );
    }

    @Override
    public List< ValueMap > fetchAssetData( String currentPath ) {

        Resource resource = CommonUtils
                .getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE )
                .getResource( currentPath.concat( "/issueKeyAssets" ) );

        return CommonUtils.fetchMultiFieldData( resource );
    }

    @Override
    public String fetchIssueSortingDate( String currentPath ) throws ClassmagsMigrationBaseException {

        try {
            String magazineName = magazineProps.getMagazineName( currentPath );
            return CommonUtils.getSortingDate( CommonUtils.fetchPagePropertyMap( currentPath, resourceResolverFactory ),
                    propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                            CommonUtils.getDataPath( resourcePathConfigService, magazineName ) ) );
        } catch ( LoginException e ) {
            throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.LOGIN_ERROR, e );
        }
    }
    
    @Override
    public String fetchIssueDisplayDate( String currentPath ) throws ClassmagsMigrationBaseException {

        try {
            String magazineName = magazineProps.getMagazineName( currentPath );
            return CommonUtils.getDisplayDate( CommonUtils.fetchPagePropertyMap( currentPath, resourceResolverFactory ),
                    propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                            CommonUtils.getDataPath( resourcePathConfigService, magazineName ) ) );
        } catch ( LoginException e ) {
            throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.LOGIN_ERROR, e );
        }
    }
}
