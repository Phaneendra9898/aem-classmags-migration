package com.scholastic.classmags.migration.servlets;

import static com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants.NN_TOPIC;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.OptingServlet;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.email.EmailService;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.NameConstants;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.ValidationUtils;

@SlingServlet(label = "Classroom Magazines - Contact Us end point", description = "Sends out an email message to site admin", resourceTypes = {
		NameConstants.NT_PAGE }, selectors = "contactussubmit", extensions = "html", methods = "POST")
@Service(Servlet.class)
public class ContactUsServlet extends SlingAllMethodsServlet implements OptingServlet {

	private static final long serialVersionUID = -3388534400856787909L;

	/* Logger for the class */
	private static Logger log = LoggerFactory.getLogger(ContactUsServlet.class);

	private static final String JK_ISSUE = "issue";
	private static final String JK_MESSAGE = "message";
	private static final String JK_NAME = "name";
	private static final String JK_SCHOOL_NAME = "schoolName";
	private static final String JK_SCHOOLSTATE = "schoolState";
	private static final String JK_GRADE = "grade";
	private static final String JK_EMAIL = "email";
	private static final String JK_RECEPIENT = "recepient";
	private static final String JK_CONTACT_DETAILS = "contactDetails";
	private static final String EMAIL_SUBJECT = "emailSubject";
	private static final String JK_SUB_TOPIC = "subTopic";
	private static final String SENDER_EMAIL = "senderEmailAddress";
	private static final String ERROR_MSG = "EMAIL_VALIDATION_FAILED";

	@Reference
	private MagazineProps magazineProps;

	@Reference
	private EmailService emailService;

	private String emailSubject;
	private String magazineName;
	private String topic;
	private String subTopic;
	private String message;
	private String recepient;
	private String name;
	private String email;
	private String school;
	private String schoolState;
	private String gradeTaught;

	private static final String TEMPLATE_PATH = "/etc/notification/email/html/scholastic/com.scholastic.classmags.migration.contactus/en.txt";

	private static final String PAGE_RT = "scholastic/classroom-magazines-migration/components/page/base-page";
	
	private static final String OTHER="Other";

	@Override
	protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws javax.servlet.ServletException, java.io.IOException {
		try {
			String requestData = request.getParameter("data");
			Map<String, String> emailParams = new HashMap<String, String>();
			JSONObject data = new JSONObject(requestData);
			log.debug("data is {}", data.toString());
			topic = data.optString(NN_TOPIC);
			topic = StringUtils.isNotBlank(topic)?topic:OTHER;
			subTopic = data.optString(JK_ISSUE);
			subTopic = StringUtils.isNotBlank(subTopic)?subTopic:OTHER;
			recepient = data.optString(JK_RECEPIENT);
			JSONObject contactDetails = data.optJSONObject(JK_CONTACT_DETAILS);
			if (contactDetails != null) {
				log.info("contactDetails is not null");
				message = contactDetails.optString(JK_MESSAGE);
				name = contactDetails.optString(JK_NAME);
				school = contactDetails.optString(JK_SCHOOL_NAME);
				schoolState = contactDetails.optString(JK_SCHOOLSTATE);
				gradeTaught = contactDetails.optString(JK_GRADE);
				email = contactDetails.optString(JK_EMAIL);
				magazineName = magazineProps.getMagazineName(request.getResource().getPath());
				emailSubject = magazineName + ": " + topic + " - " + subTopic;
				log.debug("Email Subject : {}",
						ValidationUtils.getFilteredText(request.getResourceResolver(), emailSubject));
				emailParams.put(EMAIL_SUBJECT,
						ValidationUtils.getFilteredText(request.getResourceResolver(), emailSubject));
				emailParams.put(NN_TOPIC, ValidationUtils.getFilteredText(request.getResourceResolver(), topic));
				emailParams.put(JK_SUB_TOPIC, ValidationUtils.getFilteredText(request.getResourceResolver(), subTopic));
				emailParams.put(JK_MESSAGE, ValidationUtils.getFilteredText(request.getResourceResolver(), message));
				emailParams.put(JK_NAME, ValidationUtils.getFilteredText(request.getResourceResolver(), name));
				emailParams.put("school", ValidationUtils.getFilteredText(request.getResourceResolver(), school));
				emailParams.put(JK_SCHOOLSTATE,
						ValidationUtils.getFilteredText(request.getResourceResolver(), schoolState));
				emailParams.put(JK_GRADE, ValidationUtils.getFilteredText(request.getResourceResolver(), gradeTaught));
				emailParams.put(SENDER_EMAIL, ValidationUtils.getFilteredText(request.getResourceResolver(), email));
				if (validateFields(emailParams)) {
					emailService.sendEmail(TEMPLATE_PATH, emailParams, recepient);
				} else {
					log.debug("validation failed");
					response.sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_MSG);
				}
			} else {
				log.debug("contactDetails JSON is null");
				response.sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_MSG);
			}
		} catch (JSONException e) {
			log.error("JSON Exception : {}", e);
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, ERROR_MSG);
		}
	}

	private boolean validateFields(Map<String, String> params) {
		boolean pass = true;
		for (String key : params.keySet()) {
			switch (key) {
			case JK_GRADE:
			case JK_MESSAGE:
				break;
			case SENDER_EMAIL:
				if (!ValidationUtils.isFieldValidFormat(params.get(key), ValidationUtils.VALID_EMAIL_ADDRESS_REGEX)) {
					log.debug("Key failed : {}, {}", key, params.get(key));
					return false;
				}
				break;
			default:
				if (!ValidationUtils.isFieldValid(params.get(key))) {
					log.debug("Key failed : {}, {}", key, params.get(key));
					return false;
				}
				break;
			}
		}
		return pass;
	}

	@Override
	public boolean accepts(SlingHttpServletRequest request) {
		final Resource jcrContentResource = request.getResource().getChild(JcrConstants.JCR_CONTENT);
		return (jcrContentResource != null && jcrContentResource.getResourceType().endsWith(PAGE_RT)
				&& !request.getParameterMap().isEmpty());
	}

}
