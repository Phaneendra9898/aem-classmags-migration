package com.scholastic.classmags.migration.controllers;

import java.util.List;

import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.utils.CommonUtils;

public class SocialLinksConfigUse extends WCMUsePojo {

    private List< ValueMap > socialIcons;

    @Override
    public void activate() {

        socialIcons = CommonUtils.fetchMultiFieldData( getResource(), "social-links" );

    }

    public List< ValueMap > getSocialIcons() {
        return socialIcons;
    }

}
