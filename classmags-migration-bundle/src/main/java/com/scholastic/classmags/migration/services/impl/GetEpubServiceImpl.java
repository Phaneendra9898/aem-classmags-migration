package com.scholastic.classmags.migration.services.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.services.GetEpubService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class GetEpubServiceImpl.
 */
@Component( immediate = true, metatype = true, label = "Get EPub Service." )
@Service( GetEpubService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Get EPub Service" ) })
public class GetEpubServiceImpl implements GetEpubService {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( GetEpubServiceImpl.class );

    private static final String CONTENT_OPF_PATH = "/OPS/content.opf/jcr:content/renditions/original/jcr:content";

    @Reference
    ResourceResolverFactory resourceResolverFactory;

    @Override
    public List< String > getPageIds( String eReaderPath ) throws ClassmagsMigrationBaseException {
        List< String > pageIds = new ArrayList< >();
        ResourceResolver resourceResolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );
        Resource eReaderResource = ( null != resourceResolver )
                ? resourceResolver.getResource( eReaderPath.concat( CONTENT_OPF_PATH ) ) : null;

        Node node = ( null != eReaderResource ) ? eReaderResource.adaptTo( Node.class ) : null;

        if ( null != node ) {
            pageIds = createPageIds( node );
        }

        return pageIds;
    }

    private List< String > createPageIds( Node node ) throws ClassmagsMigrationBaseException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;

        List< String > pageIds = new ArrayList< >();

        try {
            db = dbf.newDocumentBuilder();
            Document document = db.parse( node.getProperty( "jcr:data" ).getBinary().getStream() );
            NodeList nodeList = document.getElementsByTagName( "itemref" );
            int size = nodeList.getLength();
            for ( int x = 0; x < size; x++ ) {
                pageIds.add( nodeList.item( x ).getAttributes().getNamedItem( "idref" ).getNodeValue() );
            }
        } catch ( ParserConfigurationException | SAXException | IOException | RepositoryException e ) {
            LOG.error( "Error while parsing EPub asset", e );
            throw new ClassmagsMigrationBaseException();
        }

        return pageIds;

    }
}
