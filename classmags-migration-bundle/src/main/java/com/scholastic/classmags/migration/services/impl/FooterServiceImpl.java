package com.scholastic.classmags.migration.services.impl;

import com.scholastic.classmags.migration.models.FooterObject;
import com.scholastic.classmags.migration.models.LinkObject;
import com.scholastic.classmags.migration.models.SocialLinks;
import com.scholastic.classmags.migration.services.FooterService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.*;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Footer service.
 */
@Component( immediate = true, metatype = false )
@Service( FooterService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Footer Service" ) })
public class FooterServiceImpl implements FooterService {

    @Reference
    private ResourcePathConfigService resourcePathConfigService;

    private static final String MARKETING_IDENTIFIER = "CM-Marketing";
    
    private static final String  MARKETING_APP_NAME= "_mkt";
    

    @Override
    public List< FooterObject > getFooterObjects( Resource resource, String appName ) {
        List< FooterObject > columnList = new ArrayList< >();
        Resource footerContainer = null;
        ResourceResolver resourceResolver = resource.getResourceResolver();
        String resourceTypeValue = resource.getResourceType();
        
        if ( null != resourceResolver ) {
            if(StringUtils.contains(resourceTypeValue, MARKETING_IDENTIFIER)){
            	String marketingAppName = appName.concat(MARKETING_APP_NAME);
                footerContainer = resourceResolver.getResource( CommonUtils.getGlobalFooterPath( resourcePathConfigService, marketingAppName ) + "/marketing-global-footer-par" );
            }else{
                footerContainer = resourceResolver.getResource( CommonUtils.getGlobalFooterPath( resourcePathConfigService, appName ) + "/global-footer-par" );
            }
        }
        if ( null != footerContainer ) {
            Iterator< Resource > footerColumns = footerContainer.listChildren();
            while ( footerColumns.hasNext() ) {
                FooterObject column = new FooterObject();
                Resource columnResource = footerColumns.next();
                ValueMap properties = columnResource.getValueMap();
                column.setHeadingTitle( properties.get( "headingTitle", String.class ) );
                column.setPhoneNumber( properties.get( "phoneNumber", String.class ) );
                column.setLiveChat( properties.get( "liveChat", String.class ) );
                column.setLiveChatLink(
                        InternalURLFormatter.formatURL( resourceResolver, properties.get( "liveChatLink", String.class ) ) );
                List< ValueMap > linkList = CommonUtils.fetchMultiFieldData( columnResource, "footer-links" );
                List<LinkObject> links = createLinkObject(resourceResolver, linkList);
                column.setFooterLinks( links );
                columnList.add( column );
            }
        }
        return columnList;
    }

    private List< LinkObject > createLinkObject(ResourceResolver resourceResolver, List< ValueMap > linkList ) {
        
        List<LinkObject> footerLinks = new ArrayList<>();
        
        for ( ValueMap link : linkList ) {
            LinkObject footerLink = new LinkObject();
            footerLink.setLabel( link.get( "linkText", String.class ) );
            footerLink.setUserType( link.get( "userType", String.class ) );
            footerLink.setPath(
                    InternalURLFormatter.formatURL( resourceResolver, link.get( "linkPath", String.class ) ) );
            footerLinks.add( footerLink );
        }
        return footerLinks;
    }

    @Override
    public SocialLinks getSocialLinks( Resource resource, String appName ) {
        SocialLinks socialLinks = null;
        Resource globalDataPageResource = null;
        Resource socialLinksResource = null;
        List< ValueMap > links;
        ResourceResolver resourceResolver = resource.getResourceResolver();
        String resourceTypeValue = resource.getResourceType();
        if ( null != resourceResolver ) {
            if(StringUtils.contains(resourceTypeValue, MARKETING_IDENTIFIER)){
            	String marketingAppName = appName.concat(MARKETING_APP_NAME);
                globalDataPageResource = resourceResolver.getResource( CommonUtils.getGlobalFooterPath( resourcePathConfigService, marketingAppName ));
            }else{
                globalDataPageResource = resourceResolver.getResource( CommonUtils.getGlobalFooterPath( resourcePathConfigService, appName ));
            }
        }

        if ( null != globalDataPageResource ) {
            socialLinksResource = globalDataPageResource.getChild( "social-links" );
        }
        if ( null != socialLinksResource ) {
            socialLinks = new SocialLinks();
            ValueMap properties = socialLinksResource.getValueMap();
            if ( null != properties ) {
                if ( properties.containsKey( "sectionHeading" ) ) {
                    socialLinks.setSectionHeading( properties.get( "sectionHeading", String.class ) );
                }
                socialLinks.setDisabledForStudents( true );
                if ( properties.containsKey( "disabledForStudents" ) ) {
                    socialLinks.setDisabledForStudents( properties.get( "disabledForStudents", Boolean.class ) );
                }
            }
            links = CommonUtils.fetchMultiFieldData( socialLinksResource, "social-links" );
            socialLinks.setLinks( links );
        }

        return socialLinks;
    }
}
