package com.scholastic.classmags.migration.servlets;

import static com.scholastic.classmags.migration.utils.CmSDMConstants.CUSTOM_STATE_PARAM;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_APPLICATION;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_APPLICATION_CODE;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_LAUNCH_CONTEXT;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_ROLE;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_URL;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.REQ_ATTR_CUSTOM_DP_LAUNCH;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.REQ_ATTR_CUSTOM_NAV_CTX;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.REQ_ATTR_OAUTH_CONSUMER_KEY;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.REQ_ATTR_OAUTH_SIGNATURE;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.REQ_ATTR_OAUTH_TIMESTAMP;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.SDM_NAV_COOKIE_NAME;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.SW_AUTH_COOKIE_NAME;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.SW_USER_ID_COOKIE_NAME;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.google.gson.Gson;
import com.scholastic.classmags.migration.models.UserIdCookieData;
import com.scholastic.classmags.migration.services.LtiLaunchVerificationStrategy;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.CookieUtil;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * The Class CMSDMServlet.
 */
@Component( label = "Class Mags SDM Servlet", description = "Scholastic School Mags SDMM Servlet", metatype = false )
@Service( Servlet.class )
@Properties( { @Property( name = "sling.servlet.paths", value = "/bin/classmags/migration/credManager" ) })
public class CMSDMServlet extends SlingAllMethodsServlet {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( CMSDMServlet.class );

    /** The Constant DOMAIN. */
    private static final String DOMAIN = ".scholastic.com";

    /** The Constant THOUSAND. */
    private static final int THOUSAND = 1000;

    /** The logged in home. */
    private String loggedInHome;

    /** The logged out home. */
    private String loggedOutHome;

    /** The log out url. */
    private String logOutUrl;

    /** The application code. */
    private String applicationCode;

    /** The property config service. */
    @Reference
    private PropertyConfigService propertyConfigService;

    /** The resource path config service. */
    @Reference
    private ResourcePathConfigService resourcePathConfigService;

    /** The lti launch strategy. */
    @Reference
    private LtiLaunchVerificationStrategy ltiLaunchStrategy;

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache
     * .sling.api.SlingHttpServletRequest,
     * org.apache.sling.api.SlingHttpServletResponse)
     */
    @Override
    protected void doPost( SlingHttpServletRequest request, SlingHttpServletResponse response )
            throws ServletException, IOException {
        LOG.debug( "Processing post request..." );
        Map< String, String > ltiParameters = getLtiParams( request.getRequestParameterMap() );
        String url = StringUtils.EMPTY;
        String customDplaunch = request.getParameter( REQ_ATTR_CUSTOM_DP_LAUNCH );
        String customDdmNavCtx = request.getParameter( REQ_ATTR_CUSTOM_NAV_CTX );
        String oauthConsumerKey = request.getParameter( REQ_ATTR_OAUTH_CONSUMER_KEY );
        String timeStamp = request.getParameter( REQ_ATTR_OAUTH_TIMESTAMP );
        String role = StringUtils.EMPTY;
        applicationCode = StringUtils.EMPTY;
        String redirectUrl = request.getParameter( CUSTOM_STATE_PARAM );
        UserIdCookieData userIdCookieData = null;
        if ( StringUtils.isNotBlank( redirectUrl ) ) {
            redirectUrl = URLDecoder.decode( redirectUrl, "UTF-8" );
        }
        LOG.trace("******************************************************************");
        LOG.trace("Custom Launch : {}",customDplaunch);
        LOG.trace("******************************************************************");        
        try {
            JSONObject json = new JSONObject( customDplaunch );
            role = json.optJSONObject( JSON_KEY_LAUNCH_CONTEXT ).optString( JSON_KEY_ROLE );
            url = json.optJSONObject( JSON_KEY_LAUNCH_CONTEXT ).optJSONObject( JSON_KEY_APPLICATION )
                    .optString( JSON_KEY_URL );
            applicationCode = json.optJSONObject( JSON_KEY_LAUNCH_CONTEXT ).optJSONObject( JSON_KEY_APPLICATION )
                    .optString( JSON_KEY_APPLICATION_CODE );
            initPaths(request.getResourceResolver());
            userIdCookieData = CookieUtil.mapRequestParamCustomDpLaunch( customDplaunch );
            LOG.debug( "Base URL : {}", url );
        } catch ( JSONException e ) {
            LOG.error( "Failed to parse URL from custom_dp_launch json", e );
        }

        long oauthTimeStamp;

        try {
            oauthTimeStamp = Long.parseLong( timeStamp );
        } catch ( NumberFormatException e ) {
            LOG.error( "Failed to parse the oauth timestamp, {}", e );
            oauthTimeStamp = 0l;
        }

        if ( performOAuthValidation( ltiParameters, oauthConsumerKey, oauthTimeStamp, url ) ) {
            LOG.debug( "OAuth Signature is valid !" );
            if ( StringUtils.isNotBlank( customDdmNavCtx ) ) {
                // leave sdm_nav_ctx cookies as is since this cookie will be
                // used by nav widget
                Cookie cookie = CookieUtil.createCookie( SDM_NAV_COOKIE_NAME, customDdmNavCtx, DOMAIN, "/", true,
                        false );
                response.addCookie( cookie );
                LOG.debug( "SDM Cookie  : {}", cookie.toString() );
            }
            if ( StringUtils.isNotBlank( role ) ) {
                // encrypt sw_auth cookie for security
                Cookie cookie = CookieUtil.createCookie( SW_AUTH_COOKIE_NAME, role, DOMAIN, "/", true, true );
                response.addCookie( cookie );
                LOG.debug( "swAuthCookie Cookie : {}", cookie.toString() );
            }
            if ( null != userIdCookieData ) {
                Gson gson = new Gson();
                Cookie cookie = CookieUtil.createCookie( SW_USER_ID_COOKIE_NAME, gson.toJson( userIdCookieData ),
                        DOMAIN, "/", true, true );
                response.addCookie( cookie );
                LOG.debug( "sw_user_id_cookie Cookie : {}", cookie.toString() );
            }
            if ( StringUtils.isNotBlank( redirectUrl ) ) {
                LOG.debug( "Redirecting to custom url '{}'", redirectUrl );
                response.setStatus( HttpServletResponse.SC_MOVED_PERMANENTLY );
                response.setHeader( "Location", redirectUrl );
            } else {
                LOG.debug( "Redirecting to home '{}'", loggedInHome );
                response.setStatus( HttpServletResponse.SC_MOVED_PERMANENTLY );
                response.setHeader( "Location", loggedInHome );
            }
        } else {
            LOG.debug( "OAuth Signature is invalid !" );
            response.sendRedirect( logOutUrl );
        }
    }

    /**
     * Perform O auth validation.
     *
     * @param basicLtiParameters
     *            the basic lti parameters
     * @param consumerKey
     *            the consumer key
     * @param timeStamp
     *            the time stamp
     * @param url
     *            the url
     * @return true, if successful
     */
    private boolean performOAuthValidation( Map< String, String > basicLtiParameters, String consumerKey,
            long timeStamp, String url ) {
        Set< String > keys = basicLtiParameters.keySet();
        Iterator< String > itr = keys.iterator();
        SortedMap< String, String > sortedMap = new TreeMap< >();
        String signature = null;
        while ( itr.hasNext() ) {
            String key = itr.next();
            String value = basicLtiParameters.get( key );
            if ( !key.equals( REQ_ATTR_OAUTH_SIGNATURE ) ) {
                if ( value.indexOf( '\r' ) > -1 ) {
                    LOG.debug( "value includes carriage return, removing it" );
                    value = value.replaceAll( "(?:\\r)", "" );
                }
                sortedMap.put( key, value );
            } else {
                signature = value;
            }
        }
        LOG.debug( "Sorted Map Size : {}", sortedMap.size() );
        LOG.debug( "Sorted Map Keys : {}", sortedMap.keySet() );
        boolean isValidSignature = ltiLaunchStrategy.performOAuthSignatureValidation( sortedMap, url, signature,
                consumerKey );
        boolean isValidTimeStamp = ltiLaunchStrategy
                .performOAuthTimeStampValidation( System.currentTimeMillis() / THOUSAND, timeStamp );
        LOG.debug( "LTI Signature is Valid :{}", isValidSignature );
        LOG.debug( "LTI TimeStamp is Valid :{}", isValidTimeStamp );
        return isValidSignature && isValidTimeStamp;
    }

    /**
     * Gets the lti params.
     *
     * @param params
     *            the params
     * @return the lti params
     */
    private Map< String, String > getLtiParams( final Map< String, RequestParameter[] > params ) {
        Map< String, String > ltiParameters = new HashMap< >();
        for ( final Map.Entry< String, RequestParameter[] > pairs : params.entrySet() ) {
            final String k = pairs.getKey();
            final RequestParameter[] pArr = pairs.getValue();
            final RequestParameter param = pArr[ 0 ];
            ltiParameters.put( k, param.getString() );
        }
        return ltiParameters;
    }

    /**
     * Inits the paths.
     */
    public void initPaths(ResourceResolver resourceResolver) {
        try {
            LOG.debug( "Application Code ::" + applicationCode );
            loggedOutHome = InternalURLFormatter.formatURL( resourceResolver,
                    propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.HOMEPAGE_LOGGEDOUT,
                            CommonUtils.getDataPath( resourcePathConfigService, applicationCode ) ) );
            loggedOutHome = resourceResolver.adaptTo( Externalizer.class ).externalLink( resourceResolver,
                    applicationCode, loggedOutHome );
            loggedOutHome = resourceResolver.map( loggedOutHome );
            LOG.debug( "Home page logged out ::" + loggedOutHome );

            loggedInHome = InternalURLFormatter.formatURL( resourceResolver,
                    propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.HOMEPAGE_LOGGEDIN,
                            CommonUtils.getDataPath( resourcePathConfigService, applicationCode ) ) );
            LOG.trace( "Home page logged in ::" + loggedInHome );
            loggedInHome = resourceResolver.adaptTo( Externalizer.class ).externalLink( resourceResolver,
                    applicationCode, loggedInHome );
            LOG.trace( "Home page logged external ::" + loggedInHome );
            loggedInHome = resourceResolver.map( loggedInHome );
            LOG.debug( "Home page logged in map ::" + loggedInHome );

            logOutUrl = propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.SDM_LOGOUT,
                    CommonUtils.getDataPath( resourcePathConfigService, applicationCode ) );
            LOG.debug( "SDM logout URL ::" + logOutUrl );
        } catch ( Exception e ) {
            LOG.error( "Error initializing the properties {}", e );
        }
    }

}