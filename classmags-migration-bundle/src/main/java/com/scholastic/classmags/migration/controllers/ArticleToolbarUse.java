package com.scholastic.classmags.migration.controllers;

import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.services.ArticleToolbarDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * Use Class for Article Toolbar.
 */
public class ArticleToolbarUse extends WCMUsePojo {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( ArticleToolbarUse.class );

    /** The lexile levels. */
    private List< String > lexileLevels;

    /** The parent issue page path. */
    private String parentIssuePagePath;

    /** The digital issue link. */
    private String digitalIssueLink;

    /** The bookmark path. */
    private String bookmarkPath;

    /**
     * Gets the parent issue page path.
     *
     * @return the parentIssuePagePath
     */
    public String getParentIssuePagePath() {
        return parentIssuePagePath;
    }

    /**
     * Gets the lexile levels.
     *
     * @return the lexileLevels
     */
    public List< String > getLexileLevels() {
        return lexileLevels;
    }

    /**
     * Gets the digital issue link.
     *
     * @return the digitalIssueLink
     */
    public String getDigitalIssueLink() {
        return digitalIssueLink;
    }

    /**
     * Gets the bookmark path.
     *
     * @return the bookmarkPath
     */
    public String getBookmarkPath() {
        return bookmarkPath;
    }

    /**
     * Sets the bookmark path.
     *
     * @param bookmarkPath
     *            the bookmarkPath to set
     */
    public void setBookmarkPath( String bookmarkPath ) {
        this.bookmarkPath = bookmarkPath;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() {
        SlingHttpServletRequest request = getRequest();
        Resource currentResource = request.getResource();
        PageManager currentPageManager = currentResource.getResourceResolver().adaptTo( PageManager.class );
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        if ( null != currentPageManager && null != slingScriptHelper ) {
            Page currentPage = currentPageManager.getContainingPage( currentResource );
            String currentPath = currentPage.getPath();
            setBookmarkPath( currentPath );
            ArticleToolbarDataService articleToolbarDataService = slingScriptHelper
                    .getService( ArticleToolbarDataService.class );
            if ( null != articleToolbarDataService ) {
                try {
                    lexileLevels = articleToolbarDataService
                            .fetchLexileLevels( currentPath.concat( ClassMagsMigrationConstants.LEXILE_LEVEL_NODE ) );
                    parentIssuePagePath = InternalURLFormatter.formatURL( getResourceResolver(),
                            articleToolbarDataService.fetchParentIssuePagePath( currentPage ) );
                    digitalIssueLink = InternalURLFormatter.formatURL( getResourceResolver(),
                            articleToolbarDataService.fetchdigitalIssueLink(
                                    currentPath.concat( ClassMagsMigrationConstants.ARTICLE_CONFIGURATION_PATH ) ) );
                } catch ( ClassmagsMigrationBaseException e ) {
                    LOG.error( e.getMessage(), e );
                }
            }
        }
    }
}
