package com.scholastic.classmags.migration.models;

/**
 * The Class Page Info.
 */
public class PageInfo {

    /** The page name. */
    private String pageName;
    
    /** The user type. */
    private String userType;

    /**
     * Gets the page name.
     * 
     * @return the page name
     */
    public String getPageName() {
        return pageName;
    }

    /**
     * Sets the page name.
     * 
     * @param pageName the page name.
     */
    public void setPageName( String pageName ) {
        this.pageName = pageName;
    }

    /**
     * Gets the user type.
     * 
     * @return the user type
     */
    public String getUserType() {
        return userType;
    }

    /**
     * Sets the user type.
     * 
     * @param userType the user type.
     */
    public void setUserType( String userType ) {
        this.userType = userType;
    }

}
