package com.scholastic.classmags.migration.utils;

/**
 * Search Constants
 * 
 *
 */
public final class SolrSearchQueryConstants {

    // Request values
    public static final String KEYWORD = "keyword";
    public static final String START = "start";
    public static final String ROWS = "rows";
    public static final String SORT = "sort";
    public static final String FILTERS = "filters";
    public static final String PREFILTER = "preFilter";
    public static final String FILTER_NAME = "filtername";
    public static final String MONTH = "month";
    public static final String COMMA = ",";
    public static final String SITE_ID = "siteId";
    public static final String ROLE = "role";
    public static final String SUBJECTS = "subjects";
    public static final String SUBJECTS_FILTERS = "subjectFilters";
    public static final String TYPES = "contentTypes";
    public static final String TEXT = "text";
    public static final String SUBJECTS_OPERATOR = "subjectOperator";
    public static final String CONTENT_TYPES_OPERATOR = "contentOperator";
    public static final String QUERY_TYPE_DISMAX = "dismax";

    // QUERY constants
    public static final String SOLR_QUERY_AND = " AND ";
    public static final String SOLR_QUERY_OR = " OR ";
    public static final String SOLR_QUERY_PARENTHESIS_LEFT = "(";
    public static final String SOLR_QUERY_PARENTHESIS_RIGHT = ")";
    public static final String DESC_SUFIX = "_desc";
    public static final String SOLR_QUERY_FACET_SORT_BY_NAME = "name";
    public static final String SORT_OPTION_TITLE = "title";
    public static final String SORT_OPTION_PUBLISHED = "published";
    public static final String SORT_OPTION_RELEVANCE = "relevance";
    public static final String SOLR_SCORE="score";
    /**
     * to -do update the field
     */
    public static final String SOLR_QUERY_SORT_TITLE = "search_title_s";
    // Solr default values
    public static final int DEFAULT_START_VALUE = 0;
    public static final int DEFAULT_ROWS_VALUE = 12;
    public static final int MAX_ROWS_VALUE = 18;
    public static final int MIN_ROWS_VALUE = 6;
    public static final int TYPEAHEAD_START_VALUE = 0;
    public static final int TYPEAHEAD_ROWS_VALUE = 5;
    public static final String DEFAULT_HTML_EXTENSION_PATH = ".html";
    public static final String ROOT_PATH_AEM = "/content/";

    public static final String SOLR_QUERY_FQ_TYPE_ISSUE = "type_s:\"Issues\"";
    public static final String SOLR_QUERY_FQ_TYPE_ARTICLES = "type_s:\"Article\"";
    public static final String SOLR_QUERY_FQ_TYPE_SKILL_SHEET = "type_s:\"Skills Sheet\"";
    public static final String SOLR_QUERY_FQ_TYPE_GAME = "type_s:\"Game\"";
    public static final String SOLR_QUERY_FQ_TYPE_VIDEO = "type_s:\"Video\"";
    public static final String SOLR_QUERY_FQ_TYPE_LESSON_PLANS = "type_s:\"Lesson Plan\"";
    public static final String SOLR_QUERY_FQ_TYPE_DOWNLOAD = "type_s:\"Download\"";
    public static final String SOLR_QUERY_FQ_TYPE_POLL = "type_s:\"Poll\"";
    public static final String SOLR_QUERY_FQ_TYPE_BLOG = "type_s:\"Blog\"";

    public static final String[] SOLR_FACET_FIELD = { "type_s", "subject_tags_ss" };
    
    public static final String SQ_FIELD_TITLE = "title_t";
    public static final String SQ_FIELD_DESCRIPTION = "description_t";
    public static final String SQ_FIELD_KEYWORDS = "keywords_t";
    public static final String SQ_FIELD_ARTICLE_TITLE = "article_title_t";
    public static final String SQ_FIELD_ARTICLE_CONTENT = "article_content_t";
    public static final String SQ_FIELD_ARTICLE_CAPTION = "article__text_caption_t";
    public static final String SQ_FIELD_ARTICLE_CREDIT = "article_text_credit_t";
    public static final String SQ_FIELD_TAGS = "tags_txt";
    public static final String SQ_FIELD_SITE_ID = "site_id_s";
    public static final String SQ_FIELD_PUBLISHED_DATE = "published_date_t";
    public static final String SQ_FIELD_USER_TYPE = "user_type_s";
    public static final String SQ_FIELD_SUBJECT_TAGS = "subject_tags_ss";
    public static final String SQ_FIELD_ID = "id";
    public static final String SQ_FIELD_BOOKMARK_PATH = "bookmarkPath_s";
    public static final String SQ_FIELD_VIDEO_ID = "video_id_s";
    public static final String SQ_FIELD_CONTAINER_URL = "container_url_s";
    public static final String SQ_FIELD_THUMBNAIL_URL = "thumbnail_s";
    public static final String SQ_FIELD_TYPE = "type_s";
    public static final String SQ_FIELD_SHARE_URL = "share_url_s";
    public static final String SQ_FIELD_DOWNLOADABLE = "downloadable_b";
    public static final String SQ_FIELD_SHAREABLE = "shareable_b";
    public static final String SQ_FIELD_GAME_TYPE = "game_type_s";
    public static final String SQ_FIELD_GAME_PATH = "game_path_s";
    public static final String SQ_FIELD_FLASH_CONTAINER = "flash_container_s";
    public static final String SQ_FIELD__UNIQUE_ID = "unique_id_s";
    public static final String SQ_FIELD_VIDEO_DURATION = "video_duration_s";
    
    public static final String SQ_FIELD_SECTION_TITLE = "section_title_t";
    public static final String SQ_FIELD_SECTION_TEXT = "section_text_t";
    public static final String SQ_FIELD_TAG_IDS = "tags_ss";
    
    /**
     * Fields that will be returned as part of query response
     */
    public static final String SQ_RESP_FIELDS_LIST =SQ_FIELD_ID + " " + SQ_FIELD_TITLE + " " + SQ_FIELD_DESCRIPTION + " " + SQ_FIELD_PUBLISHED_DATE +
            " " +SQ_FIELD_USER_TYPE + " " + SQ_FIELD_SUBJECT_TAGS + " " + SQ_FIELD_VIDEO_ID + " "+ SQ_FIELD_CONTAINER_URL + " " + 
            SQ_FIELD_THUMBNAIL_URL + " " + SQ_FIELD_TYPE + " " + SQ_FIELD_SHARE_URL + " " + SQ_FIELD_DOWNLOADABLE + " " + SQ_FIELD_SHAREABLE + " " +
            SQ_FIELD_GAME_TYPE + " " + SQ_FIELD_GAME_PATH + " " + SQ_FIELD_FLASH_CONTAINER + " " + SQ_FIELD__UNIQUE_ID + " " + SQ_FIELD_VIDEO_DURATION + " " + SQ_FIELD_BOOKMARK_PATH + " " + SQ_FIELD_TAG_IDS;
    
    public static final String SQ_FIELD_TITLE_WT = "^10.0";
    public static final String SQ_FIELD_DESCRIPTION_WT = "^4.0";
    public static final String SQ_FIELD_KEYWORDS_WT = "^6.0";
    public static final String SQ_FIELD_ARTICLE_TITLE_WT = "^2.0";
    public static final String SQ_FIELD_ARTICLE_CONTENT_WT = "^2.0";
    public static final String SQ_FIELD_ARTICLE_CAPTION_WT = "^0.5";
    public static final String SQ_FIELD_ARTICLE_CREDIT_WT = "^0.5";
    public static final String SQ_FIELD_TAGS_WT = "^8.0";
    public static final String SQ_FIELD_PUBLISHED_DATE_WT ="^1.0";
    
    public static final String SQ_BOOST_QUERY_FOR_CONTENT_ORDER = "(type_s:\"Articles\"^1000.0 type_s:\"Videos\"^110.0 type_s:\"Games\"^30.0 type_s:\"Skills Sheets\"^100.0"+
    															" type_s:\"Lesson Plans\"^30.0 type_s:*)";
    public static final String SQ_BOOST_QUERY_FOR_CONTENT_HUB = "(type_s:\"Articles\"^1000.0 type_s:\"Videos\"^110.0 type_s:\"Games\"^30.0 type_s:\"Skills Sheets\"^100.0"+
			" type_s:\"Lesson Plans\"^30.0 type_s:*) -(type_s:\"Issues\")";
    
    public static final String SQ_FIELD_SORT_DATE ="sort_date_dt";
    
    public static final String SOLR_QUERY_FQ_SITE_ID = "site_id_s:\"scienceworld\"";
    
    public static final String FLD_TYPE_VAL_ARTICLE="Article";
    public static final String FLD_TYPE_VAL_ISSUE="Issue";
    
    public static final String FLD_TYPE_CONTENTHUB = "contenthub";
    
    private SolrSearchQueryConstants() {

    }
}