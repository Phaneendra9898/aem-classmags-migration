package com.scholastic.classmags.migration.services.impl;

import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.MagazinePropsConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.*;
import org.osgi.framework.Constants;

import java.util.Map;

/**
 * Resource Path Configuration Service.
 */
@Component( immediate = true, metatype = true )
@Service( ResourcePathConfigService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Resource Path Configuration Service" ) })
public class ResourcePathConfigServiceImpl implements ResourcePathConfigService {

    /** The Constant DATA_NODE_PATH. */
    public static final String DATA_NODE_PATH = "/jcr:content/data-page-par/global_config";
    
    /** The Constant FOOTER_NODE_PATH. */
    public static final String FOOTER_NODE_PATH = "/jcr:content/data-page-par/global_footer_contai";

    /** The Constant MARKETING_FOOTER_NODE_PATH. */
    public static final String MARKETING_FOOTER_NODE_PATH = "/jcr:content/data-page-par/marketing_global_foo";

    /** The Constant GLOBAL_RESOURCE_PATH_DEFAULT. */
    public static final String GLOBAL_RESOURCE_PATH_DEFAULT = "/content/classroom_magazines/admin/global-cm-data-page";

    /** The Constant GLOBAL_RESOURCE_PATH. */
    @Property( label = "Global Data Path", description = "Default: " + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String GLOBAL_RESOURCE_PATH = "classmags-global-resource-path" ;

    /** The Constant SCIENCE_WORLD_RESOURCE_PATH. */
    @Property( label = "Science World", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String SCIENCE_WORLD_RESOURCE_PATH = "classmags-scienceworld-resource-path";
    
    /** The Constant SUPER_SCIENCE_RESOURCE_PATH. */
    @Property( label = "Super Science", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String SUPER_SCIENCE_RESOURCE_PATH = "classmags-superscience-resource-path";
    
    /** The Constant ACTION_RESOURCE_PATH. */
    @Property( label = "Action", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String ACTION_RESOURCE_PATH = "classmags-action-resource-path";
    
    /** The Constant ART_RESOURCE_PATH. */
    @Property( label = "Art", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String ART_RESOURCE_PATH = "classmags-art-resource-path";
    
    /** The Constant CHOICES_RESOURCE_PATH. */
    @Property( label = "Choices", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String CHOICES_RESOURCE_PATH = "classmags-choices-resource-path";
    
    /** The Constant DYNA_MATH_RESOURCE_PATH. */
    @Property( label = "Dyna Math", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String DYNA_MATH_RESOURCE_PATH = "classmags-dynamath-resource-path";
    
    /** The Constant GEOGRAPHY_SPIN_RESOURCE_PATH. */
    @Property( label = "Geography", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String GEOGRAPHY_SPIN_RESOURCE_PATH = "classmags-geographyspin-resource-path";
    
    /** The Constant JUNIOR_SCHOLASTIC_RESOURCE_PATH. */
    @Property( label = "Junior Scholastic", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String JUNIOR_SCHOLASTIC_RESOURCE_PATH = "classmags-junior-resource-path";
    
    /** The Constant LETS_FIND_OUT_RESOURCE_PATH. */
    @Property( label = "Lets Find Out", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String LETS_FIND_OUT_RESOURCE_PATH = "classmags-letsfindout-resource-path";
    
    /** The Constant MATH_RESOURCE_PATH. */
    @Property( label = "Math", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String MATH_RESOURCE_PATH = "classmags-math-resource-path";
    
    /** The Constant MY_BIG_WORLD_RESOURCE_PATH. */
    @Property( label = "My Big World", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String MY_BIG_WORLD_RESOURCE_PATH = "classmags-mybigworld-resource-path";

    /** The Constant SCHOLASTIC_NEWS_ONE_RESOURCE_PATH. */
    @Property( label = "Scholastic News 1", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String SCHOLASTIC_NEWS_ONE_RESOURCE_PATH = "classmags-scholasticnews1-resource-path";
    
    /** The Constant SCHOLASTIC_NEWS_TWO_RESOURCE_PATH. */
    @Property( label = "Scholastic News 2", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String SCHOLASTIC_NEWS_TWO_RESOURCE_PATH = "classmags-scholasticnews2-resource-path";
    
    /** The Constant SCHOLASTIC_NEWS_THREE_RESOURCE_PATH. */
    @Property( label = "Scholastic News 3", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String SCHOLASTIC_NEWS_THREE_RESOURCE_PATH = "classmags-scholasticnews3-resource-path";
    
    /** The Constant SCHOLASTIC_NEWS_FOUR_RESOURCE_PATH. */
    @Property( label = "Scholastic News 4", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String SCHOLASTIC_NEWS_FOUR_RESOURCE_PATH = "classmags-scholasticnews4-resource-path";
    
    /** The Constant SCHOLASTIC_NEWS_FIVE_SIX_RESOURCE_PATH. */
    @Property( label = "Scholastic News 5/6", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String SCHOLASTIC_NEWS_FIVE_SIX_RESOURCE_PATH = "classmags-scholasticnews56-resource-path";
    
    /** The Constant SCIENCE_SPIN_K_ONE_RESOURCE_PATH. */
    @Property( label = "Science Spin K-1", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String SCIENCE_SPIN_K_ONE_RESOURCE_PATH = "classmags-sciencespink1-resource-path";
    
    /** The Constant SCIENCE_SPIN_TWO_RESOURCE_PATH. */
    @Property( label = "Science Spin 2", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String SCIENCE_SPIN_TWO_RESOURCE_PATH = "classmags-sciencespin2-resource-path";
    
    /** The Constant SCIENCE_SPIN_THREE_TO_SIX_RESOURCE_PATH. */
    @Property( label = "Science Spin 3-6", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String SCIENCE_SPIN_THREE_TO_SIX_RESOURCE_PATH = "classmags-sciencespin36-resource-path";
    
    /** The Constant SCOPE_RESOURCE_PATH. */
    @Property( label = "Scope", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String SCOPE_RESOURCE_PATH = "classmags-scope-resource-path";
    
    /** The Constant STORYWORKS_RESOURCE_PATH. */
    @Property( label = "Storyworks", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String STORYWORKS_RESOURCE_PATH = "classmags-storyworks-resource-path";
    
    /** The Constant STORYWORKS_JUNIOR_PATH. */
    @Property( label = "Storyworks Junior", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String STORYWORKS_JUNIOR_RESOURCE_PATH = "classmags-storyworksjr-resource-path";
    
    /** The Constant UPFRONT_RESOURCE_PATH. */
    @Property( label = "Upfront", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String UPFRONT_RESOURCE_PATH = "classmags-upfront-resource-path";
    
    /** The Constant SCHOLASTIC NEWS */
    @Property( label = "Scholastic News", description = "Default: "
            + GLOBAL_RESOURCE_PATH_DEFAULT )
    public static final String SCHOLASTIC_NEWS_RESOURCE_PATH = "classmags-scholasticnews-resource-path";
    
    /** The global resource path. */
    private String globalResourcePath;
    
    /** The science world resource path. */
    private String scienceWorldResourcePath;
    
    /** The super science resource path. */
    private String superScienceResourcePath;
    
    /** The action resource path. */
    private String actionResourcePath;
    
    /** The art resource path. */
    private String artResourcePath;

    /** The choices resource path. */
    private String choicesResourcePath;
    
    /** The dynamath resource path. */
    private String dynaMathResourcePath;
    
    /** The geography spin resource path. */
    private String geographySpinResourcePath;
    
    /** The junior scholastic resource path. */
    private String juniorScholasticResourcePath;
    
    /** The lets find out resource path. */
    private String letsFindOutResourcePath;
    
    /** The math resource path. */
    private String mathResourcePath;
    
    /** The my big world resource path. */
    private String myBigWorldResourcePath;
    
    /** The scholastic news one resource path. */
    private String scholasticNewsOneResourcePath;
    
    /** The scholastic news two resource path. */
    private String scholasticNewsTwoResourcePath;
    
    /** The scholastic news three resource path. */
    private String scholasticNewsThreeResourcePath;
    
    /** The scholastic news four resource path. */
    private String scholasticNewsFourResourcePath;
    
    /** The scholastic news five six resource path. */
    private String scholasticNewsFiveSixResourcePath;
    
    /** The science spin one resource path. */
    private String scienceSpinKOneResourcePath;
    
    /** The science spin two resource path. */
    private String scienceSpinTwoResourcePath;
    
    /** The science spin three to six resource path. */
    private String scienceSpinThreeToSixResourcePath;
    
    /** The scope resource path. */
    private String scopeResourcePath;
    
    /** The storyworks resource path. */
    private String storyworksResourcePath;
    
    /** The storyworks junior resource path. */
    private String storyworksJrResourcePath;
    
    /** The upfront resource path. */
    private String upfrontResourcePath;
    
    /** The scholastic news. */
    private String scholasticNewsResourcePath;
    /**
     * Activate.
     *
     * @param properties
     *            the properties
     */
    @Activate
    protected void activate( final Map< String, Object > properties ) {
        globalResourcePath = StringUtils.defaultIfBlank( ( String ) properties.get( GLOBAL_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        scienceWorldResourcePath = StringUtils.defaultIfBlank( ( String ) properties.get( SCIENCE_WORLD_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        superScienceResourcePath = StringUtils.defaultIfBlank( (String) properties.get( SUPER_SCIENCE_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        actionResourcePath = StringUtils.defaultIfBlank( (String) properties.get( ACTION_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        artResourcePath = StringUtils.defaultIfBlank( (String) properties.get( ART_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        choicesResourcePath = StringUtils.defaultIfBlank( (String) properties.get( CHOICES_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        dynaMathResourcePath = StringUtils.defaultIfBlank( (String) properties.get( DYNA_MATH_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        geographySpinResourcePath = StringUtils.defaultIfBlank( (String) properties.get( GEOGRAPHY_SPIN_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        juniorScholasticResourcePath = StringUtils.defaultIfBlank( (String) properties.get( JUNIOR_SCHOLASTIC_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        letsFindOutResourcePath = StringUtils.defaultIfBlank( (String) properties.get( LETS_FIND_OUT_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        mathResourcePath = StringUtils.defaultIfBlank( (String) properties.get( MATH_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        myBigWorldResourcePath = StringUtils.defaultIfBlank( (String) properties.get( MY_BIG_WORLD_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        scholasticNewsOneResourcePath = StringUtils.defaultIfBlank( (String) properties.get( SCHOLASTIC_NEWS_ONE_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        scholasticNewsTwoResourcePath = StringUtils.defaultIfBlank( (String) properties.get( SCHOLASTIC_NEWS_TWO_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        scholasticNewsThreeResourcePath = StringUtils.defaultIfBlank( (String) properties.get( SCHOLASTIC_NEWS_THREE_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        scholasticNewsFourResourcePath = StringUtils.defaultIfBlank( (String) properties.get( SCHOLASTIC_NEWS_FOUR_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        scholasticNewsFiveSixResourcePath = StringUtils.defaultIfBlank( (String) properties.get( SCHOLASTIC_NEWS_FIVE_SIX_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        scienceSpinKOneResourcePath = StringUtils.defaultIfBlank( (String) properties.get( SCIENCE_SPIN_K_ONE_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        scienceSpinTwoResourcePath = StringUtils.defaultIfBlank( (String) properties.get( SCIENCE_SPIN_TWO_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        scienceSpinThreeToSixResourcePath = StringUtils.defaultIfBlank( (String) properties.get( SCIENCE_SPIN_THREE_TO_SIX_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        scopeResourcePath = StringUtils.defaultIfBlank( (String) properties.get( SCOPE_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        storyworksResourcePath = StringUtils.defaultIfBlank( (String) properties.get( STORYWORKS_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        storyworksJrResourcePath = StringUtils.defaultIfBlank( (String) properties.get( STORYWORKS_JUNIOR_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        upfrontResourcePath = StringUtils.defaultIfBlank( (String) properties.get( UPFRONT_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        scholasticNewsResourcePath = StringUtils.defaultIfBlank( (String) properties.get( SCHOLASTIC_NEWS_RESOURCE_PATH ),
                GLOBAL_RESOURCE_PATH_DEFAULT );
        
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.scholastic.classmags.migration.services.ResourcePathConfigService#
     * getGlobalResourcePath()
     */
    @Override
    public String getGlobalResourcePath() {
        return globalResourcePath.concat( DATA_NODE_PATH );
    }

    @Override
    public String getGlobalFooterResourcePath() {
        return globalResourcePath.concat( FOOTER_NODE_PATH );
    }

    @Override
    public String getGlobalConfigResourcePath( String magazineName ) {
        String globalConfigResourcePath;
        switch ( magazineName ) {
        case MagazinePropsConstants.SCIENCE_WORLD:
            globalConfigResourcePath = scienceWorldResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.SUPERSCIENCE:
            globalConfigResourcePath = superScienceResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.ACTION:
            globalConfigResourcePath = actionResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.ART:
            globalConfigResourcePath = artResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.CHOICES:
            globalConfigResourcePath = choicesResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.DYNA_MATH:
            globalConfigResourcePath = dynaMathResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.GEOGRAPHY_SPIN:
            globalConfigResourcePath = geographySpinResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.JUNIOR_SCHOLASTIC:
            globalConfigResourcePath = juniorScholasticResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.LETS_FIND_OUT:
            globalConfigResourcePath = letsFindOutResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.MATH:
            globalConfigResourcePath = mathResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.MY_BIG_WORLD:
            globalConfigResourcePath = myBigWorldResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.SCHOLASTIC_NEWS_ONE:
            globalConfigResourcePath = scholasticNewsOneResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.SCHOLASTIC_NEWS_TWO:
            globalConfigResourcePath = scholasticNewsTwoResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.SCHOLASTIC_NEWS_THREE:
            globalConfigResourcePath = scholasticNewsThreeResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.SCHOLASTIC_NEWS_FOUR:
            globalConfigResourcePath = scholasticNewsFourResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.SCHOLASTIC_NEWS_FIVE_SIX:
            globalConfigResourcePath = scholasticNewsFiveSixResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.SCIENCE_SPIN_K_ONE:
            globalConfigResourcePath = scienceSpinKOneResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.SCIENCE_SPIN_TWO:
            globalConfigResourcePath = scienceSpinTwoResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.SCIENCE_SPIN_THREE_TO_SIX:
            globalConfigResourcePath = scienceSpinThreeToSixResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.SCOPE:
            globalConfigResourcePath = scopeResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.STORYWORKS:
            globalConfigResourcePath = storyworksResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.STORYWORKS_JUNIOR:
            globalConfigResourcePath = storyworksJrResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.UPFRONT:
            globalConfigResourcePath = upfrontResourcePath.concat( DATA_NODE_PATH );
            break;
        case MagazinePropsConstants.SCHOLASTIC_NEWS:
            globalConfigResourcePath = scholasticNewsResourcePath.concat( DATA_NODE_PATH );
            break;
        default:
            globalConfigResourcePath = globalResourcePath.concat( DATA_NODE_PATH );
        }
        return globalConfigResourcePath;
    }
    
    @Override
    public String getFooterConfigResourcePath( String magazineName ) {
        String footerConfigResourcePath;
        switch ( magazineName ) {
        case MagazinePropsConstants.SCIENCE_WORLD:
            footerConfigResourcePath = scienceWorldResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.SUPERSCIENCE:
            footerConfigResourcePath = superScienceResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.ACTION:
            footerConfigResourcePath = actionResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.ART:
            footerConfigResourcePath = artResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.CHOICES:
            footerConfigResourcePath = choicesResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.DYNA_MATH:
            footerConfigResourcePath = dynaMathResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.GEOGRAPHY_SPIN:
            footerConfigResourcePath = geographySpinResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.JUNIOR_SCHOLASTIC:
            footerConfigResourcePath = juniorScholasticResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.LETS_FIND_OUT:
            footerConfigResourcePath = letsFindOutResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.MATH:
            footerConfigResourcePath = mathResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.MY_BIG_WORLD:
            footerConfigResourcePath = myBigWorldResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.SCHOLASTIC_NEWS_ONE:
            footerConfigResourcePath = scholasticNewsOneResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.SCHOLASTIC_NEWS_TWO:
            footerConfigResourcePath = scholasticNewsTwoResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.SCHOLASTIC_NEWS_THREE:
            footerConfigResourcePath = scholasticNewsThreeResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.SCHOLASTIC_NEWS_FOUR:
            footerConfigResourcePath = scholasticNewsFourResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.SCHOLASTIC_NEWS_FIVE_SIX:
            footerConfigResourcePath = scholasticNewsFiveSixResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.SCIENCE_SPIN_K_ONE:
            footerConfigResourcePath = scienceSpinKOneResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.SCIENCE_SPIN_TWO:
            footerConfigResourcePath = scienceSpinTwoResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.SCIENCE_SPIN_THREE_TO_SIX:
            footerConfigResourcePath = scienceSpinThreeToSixResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.SCOPE:
            footerConfigResourcePath = scopeResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.STORYWORKS:
            footerConfigResourcePath = storyworksResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.STORYWORKS_JUNIOR:
            footerConfigResourcePath = storyworksJrResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.UPFRONT:
            footerConfigResourcePath = upfrontResourcePath.concat( FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.SCIENCE_WORLD_MKT:
            footerConfigResourcePath = scienceWorldResourcePath.concat(MARKETING_FOOTER_NODE_PATH);
            break;
        case MagazinePropsConstants.JUNIOR_SCHOLASTIC_MKT:
            footerConfigResourcePath = juniorScholasticResourcePath.concat(MARKETING_FOOTER_NODE_PATH);
            break;
        case MagazinePropsConstants.SCHOLASTIC_NEWS_THREE_MKT:
            footerConfigResourcePath = scholasticNewsThreeResourcePath.concat( MARKETING_FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.SCHOLASTIC_NEWS_FOUR_MKT:
            footerConfigResourcePath = scholasticNewsFourResourcePath.concat( MARKETING_FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.SCHOLASTIC_NEWS_FIVE_SIX_MKT:
            footerConfigResourcePath = scholasticNewsFiveSixResourcePath.concat( MARKETING_FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.DYNA_MATH_NAME_MKT:
            footerConfigResourcePath = dynaMathResourcePath.concat( MARKETING_FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.SCHOLASTIC_NEWS_MKT:
            footerConfigResourcePath = scholasticNewsResourcePath.concat( MARKETING_FOOTER_NODE_PATH );
            break;
        case MagazinePropsConstants.SCHOLASTIC_NEWS:
            footerConfigResourcePath = scholasticNewsResourcePath.concat( FOOTER_NODE_PATH );
            break;
		case MagazinePropsConstants.MATH_MKT:
            footerConfigResourcePath = mathResourcePath.concat( MARKETING_FOOTER_NODE_PATH );
            break;
		case MagazinePropsConstants.ACTION_MKT:
            footerConfigResourcePath = actionResourcePath.concat( MARKETING_FOOTER_NODE_PATH );
            break;
		case MagazinePropsConstants.SUPERSCIENCE_MKT:
            footerConfigResourcePath = superScienceResourcePath.concat( MARKETING_FOOTER_NODE_PATH );
            break;
        default:
            footerConfigResourcePath = globalResourcePath.concat( FOOTER_NODE_PATH );
        }
        return footerConfigResourcePath;
    }
}
