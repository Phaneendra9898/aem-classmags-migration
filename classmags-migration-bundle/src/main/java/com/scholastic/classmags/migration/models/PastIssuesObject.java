package com.scholastic.classmags.migration.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * The Class PastIssuesObject.
 */
public class PastIssuesObject {

    /** The issue list. */
    private IssueList issueList;

    /**
     * Gets the issue list.
     * 
     * @return The issueList
     */
    public IssueList getIssueList() {
        return issueList;
    }

    /**
     * Sets the issue list.
     * 
     * @param issueList
     *            The issueList
     */
    public void setIssueList( IssueList issueList ) {
        this.issueList = issueList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString( this );
    }

}
