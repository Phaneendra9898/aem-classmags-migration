package com.scholastic.classmags.migration.services;

import java.util.List;

import org.apache.sling.api.resource.ValueMap;

/**
 * The Interface PromotMagazineDataService.
 */
public interface PromotMagazineDataService {

    /**
     * Fetch magazine data.
     *
     * @param currentPath
     *            the current path
     * @return the list
     */
    public List< ValueMap > fetchMagazineData( String currentPath );
}
