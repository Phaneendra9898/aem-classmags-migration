package com.scholastic.classmags.migration.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.ClassmagsMigrationErrorCodes;
import com.scholastic.classmags.migration.services.ArticleToolbarDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * Service to fetch Article Toolbar Data.
 */
@Component( immediate = true, metatype = false )
@Service( ArticleToolbarDataService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Article Toolbar Data Service" ) })
public class ArticleToolbarDataServiceImpl implements ArticleToolbarDataService {

    /** The Constant ISSUE_PAGE_PATH. */
    private static final String ISSUE_PAGE_PATH = "/apps/scholastic/classroom-magazines-migration/templates/issue-page";

    /** The Constant INPUT_KEY. */
    private static final String INPUT_KEY = "linkToEReader";

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /** The query builder. */
    @Reference
    private QueryBuilder queryBuilder;

    /**
     * Sets the resource resolver factory.
     *
     * @param resourceResolverFactory
     *            the resourceResolverFactory to set
     */
    public void setResourceResolverFactory( ResourceResolverFactory resourceResolverFactory ) {
        this.resourceResolverFactory = resourceResolverFactory;
    }

    /**
     * Sets the query builder.
     *
     * @param queryBuilder
     *            the queryBuilder to set
     */
    public void setQueryBuilder( QueryBuilder queryBuilder ) {
        this.queryBuilder = queryBuilder;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.scholastic.classmags.migration.services.ArticleToolbarDataService#
     * fetchLexileLevels(java.lang.String)
     */
    @Override
    public List< String > fetchLexileLevels( String lexileLevelNodePath ) throws ClassmagsMigrationBaseException {

        List< String > levels = new ArrayList< >();
        ResourceResolver resourceResolver;
        Session session;
        Map< String, String > queryMap = new HashMap< >();
        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver( param );
        } catch ( LoginException le ) {
            throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.LOGIN_ERROR, le );
        }
        session = resourceResolver.adaptTo( Session.class );

        queryMap.put( ClassMagsMigrationConstants.QUERY_PATH, lexileLevelNodePath );
        queryMap.put( ClassMagsMigrationConstants.RESULTS_LIMIT, ClassMagsMigrationConstants.QUERY_LOWER_BOUND );
        queryMap.put( ClassMagsMigrationConstants.QUERY_ORDER_BY, ClassMagsMigrationConstants.QUERY_PATH );
        queryMap.put( ClassMagsMigrationConstants.QUERY_ORDER_BY_SORT, ClassMagsMigrationConstants.ASC );
        Query query = queryBuilder.createQuery( PredicateGroup.create( queryMap ), session );

        SearchResult searchResult = query.getResult();
        for ( Hit hit : searchResult.getHits() ) {
            String level = StringUtils.EMPTY;
            try {
                level = hit.getProperties().get( ClassMagsMigrationConstants.ARTICLE_LEXILE_LEVEL, String.class );
            } catch ( RepositoryException e ) {
                throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.REPOSITORY_ERROR, e );
            }
            if ( StringUtils.isNotBlank( level ) ) {
                levels.add( level );
            }
        }
        return levels;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.scholastic.classmags.migration.services.ArticleToolbarDataService#
     * fetchParentIssuePagePath(com.day.cq.wcm.api.Page)
     */
    @Override
    public String fetchParentIssuePagePath( Page currentPage ) throws ClassmagsMigrationBaseException {

        String queryPath = currentPage.getParent().getPath();
        String parentPagePath = null;

        ResourceResolver resourceResolver;
        Session session;
        Map< String, String > queryMap = new HashMap< >();
        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver( param );
        } catch ( LoginException le ) {
            throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.LOGIN_ERROR, le );
        }
        session = resourceResolver.adaptTo( Session.class );

        queryMap.put( ClassMagsMigrationConstants.QUERY_PATH, queryPath );
        queryMap.put( ClassMagsMigrationConstants.RESULTS_LIMIT, ClassMagsMigrationConstants.QUERY_LOWER_BOUND );
        queryMap.put( ClassMagsMigrationConstants.QUERY_PARAM_1, ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG );
        queryMap.put( ClassMagsMigrationConstants.QUERY_PARAM_1_VALUE, ISSUE_PAGE_PATH );

        Query query = queryBuilder.createQuery( PredicateGroup.create( queryMap ), session );

        SearchResult searchResult = query.getResult();
        if ( searchResult.getHits().size() == 1 ) {
            parentPagePath = queryPath;
        }
        return parentPagePath;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.scholastic.classmags.migration.services.ArticleToolbarDataService#
     * fetchdigitalIssueLink(java.lang.String)
     */
    @Override
    public String fetchdigitalIssueLink( String pagePropertiesPath ) throws ClassmagsMigrationBaseException {

        String propertyValue = null;
        ResourceResolver resourceResolver;
        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver( param );
        } catch ( LoginException le ) {
            throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.LOGIN_ERROR, le );
        }
        Resource res = resourceResolver.getResource( pagePropertiesPath );
        if ( null != res ) {
            ValueMap properties = res.adaptTo( ValueMap.class );
            propertyValue = properties != null ? properties.get( INPUT_KEY, String.class ) : null;
        }
        return propertyValue;
    }
}
