package com.scholastic.classmags.migration.controllers;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.models.GameModel;
import com.scholastic.classmags.migration.services.GameService;

public class GameComponentUse extends WCMUsePojo {

    private GameModel data;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( GameComponentUse.class );

    @Override
    public void activate() throws Exception {
        Resource resource = getResource();
        if ( null != resource ) {
            SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
            if ( slingScriptHelper != null ) {
                GameService gameService = slingScriptHelper.getService( GameService.class );
                try {
                    setData( gameService.fetchGameData( resource ) );
                } catch ( Exception e ) {
                    LOG.error( "Error in fetching Game Data" + e );
                }
            }
        }
    }

    public GameModel getData() {
        return data;
    }

    private void setData( GameModel data ) {
        this.data = data;
    }
}
