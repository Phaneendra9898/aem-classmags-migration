package com.scholastic.classmags.migration.utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFactory;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.QueryBuilder;

@Component(label = "Scholastic Classroom Magazines migration Servlet", description = "Scholastic Classroom Magazines content migration Servlet", metatype = true)
@Service(Servlet.class)
@Properties({ @Property(name = "sling.servlet.paths", value = "/bin/classmags/migrate", propertyPrivate = true),
		@Property(label = "Content URL Mappings", name = "cm.migration.url.mapping", value = {}, unbounded = PropertyUnbounded.ARRAY, cardinality = Integer.MAX_VALUE, description = "Configure URL Mappings. Example : /content/upfront/en/issues/04_24_17/russia-friend-enemy-or-frenemy=/content/classroom_magazines/upfront/issues/2016-17/042417/russia--friend--enemy--or-frenemy- . "
				+ " Please respect the order of the mapping. First match always wins") })
public class ContentMigrationServlet extends SlingSafeMethodsServlet {

	private static final String ISSUE_TEACHING_RESOURCES_RT = "scholastic/classroom-magazines-migration/components/content/issue-teaching-resources";

	private static final String ISSUE_ARTICLE_AGGREGATOR_RT = "scholastic/classroom-magazines-migration/components/content/issue-article-aggregator";

	private static final String TABS_RT = "scholastic/classroom-magazines-migration/components/content/tabs";

	private static final String ISSUE_HIGHLIGHT_RT = "scholastic/classroom-magazines-migration/components/content/issue-highlight";

	private static final String ISSUE_PAGE_RT = "scholastic/classroom-magazines-migration/components/page/issue-page";

	private static final String ARTICLE_SIDEBAR_RT = "scholastic/classroom-magazines-migration/components/content/article-sidebar";

	private static final String ARTICLE_RESOURCES_RT = "scholastic/classroom-magazines-migration/components/content/article-resources";

	private static final String ARTICLE_TOOLBAR_RT = "scholastic/classroom-magazines-migration/components/content/article-toolbar";

	private static final String COL_CTRL_COL0 = "col-ctrl/col0";

	private static final String COL_CTRL_COL2 = "col-ctrl/col2";

	private static final String PAR = "par";

	private static final String JCR_DESCRIPTION = "jcr:description";

	private static final String KEYWORDS = "keywords";

	private static final String JCR_CONTENT = "jcr:content";

	private static final String WCM_FOUNDATION_COMPONENTS_PARSYS = "wcm/foundation/components/parsys";

	private static final String ARTICLE_PAGE_RT = "scholastic/classroom-magazines-migration/components/page/article-page";

	private static final long serialVersionUID = 12L;

	private static final Logger LOG = LoggerFactory.getLogger(ContentMigrationServlet.class);

	private static final String H1 = "h1";

	private static final String H2_TITLE_SUBHEAD = "h2/title_subhead";

	private static final String ARTICLE_TITLE_STYLES = "articleTitleStyles";

	private static final String DIV_TITLE_DEK = "div/title_dek";

	private static final String TYPE = "type";

	private static final String RTEFIELD = "rtefield";

	private static final String TOP = "top";

	private static final String JCR_TITLE = "jcr:title";

	private static final String ARTICLE_TEXT_IMAGE_POS = "articleTextImagePos";

	private static final String TEXT_IS_RICH = "textIsRich";

	private static final String SLING_RESOURCE_TYPE = "sling:resourceType";

	private static final String ARTICLE_TITLE_PREFIX = "article_title_";

	private static final String TEXT_HTML = "text/html";

	private static final String OLD_TITLE_COMPONENTS_RT = "scholastic/classroom-magazines/components/title";

	private static final String OLD_TEXT_COMPONENTS_RT = "scholastic/classroom-magazines/components/text";

	private static final String TITLE = "title";

	private static final String TEXT = "text";

	private static final String MIGRATED = "migrated";

	private static final String NT_UNSTRUCTURED = "nt:unstructured";

	private static final String NEW_ARTICLE_TITLE_RT = "scholastic/classroom-magazines-migration/components/content/article-title";

	private static final String SUBHEAD = "subhead";

	public static final String CM_MIGRATION_URL_MAPPING = "cm.migration.url.mapping";

	private Map<String, String> urlMappings;

	private static Map<String, String> componentMappings;

	private static final String TITLE_COMP_REL_PATH = "jcr:content/col-ctrl/col1";

	@Reference
	private QueryBuilder queryBuilder;

	@SuppressWarnings("rawtypes")
	@Activate
	protected final void activate(final ComponentContext componentContext) {
		try {
			Dictionary properties = componentContext.getProperties();
			this.urlMappings = (LinkedHashMap<String, String>) PropertiesUtil
					.toMap(properties.get(CM_MIGRATION_URL_MAPPING), ArrayUtils.EMPTY_STRING_ARRAY);
		} catch (Exception e) {
			LOG.error("Error initializing the url map" + e);
		}
	}

	static {
		componentMappings = new HashMap<>();
		componentMappings.put(TITLE, OLD_TITLE_COMPONENTS_RT);
		componentMappings.put(TEXT, OLD_TEXT_COMPONENTS_RT);
	}

	/**
	 * Method doGet
	 */
	@Override
	public void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType(TEXT_HTML);
		boolean simulate = true;
		if (StringUtils.isNotBlank(request.getParameter("simulate"))) {
			simulate = Boolean.valueOf(request.getParameter("simulate"));
		}
		if (simulate) {
			response.getWriter().write("<hr/> <b>Simulating Content Migration</b> <hr/>");
		} else {
			response.getWriter().write("<hr/> <b>Content Migration Started</b> <hr/>");
		}
		migrateComponent(request, response, simulate);
		response.getWriter().write("<hr/> <b>Migration Complete</b> <hr/>");
	}

	public void migrateComponent(SlingHttpServletRequest request, SlingHttpServletResponse response, boolean simulate)
			throws IOException {
		LOG.debug("urlMappings Size : {}", urlMappings.size());
		for (String key : urlMappings.keySet()) {
			LOG.debug("Old URL : {}", key);
			LOG.debug("New URL : {}", urlMappings.get(key));
			Random random = new Random();
			ResourceResolver resourceResolver = request.getResourceResolver();
			Resource newResource = resourceResolver.getResource(urlMappings.get(key));
			Resource oldResource = resourceResolver.getResource(key);
			response.getWriter().write("<ul><li><b>" + urlMappings.get(key) + "<b/></li>");
			boolean success = true;
			if (!simulate) {
				success = updatePageProperties(newResource, oldResource);
			}
			if (success) {
				response.getWriter().write("<ul><li>Page Properties Migrated successfully.</li></ul>");
			} else {
				response.getWriter().write("<ul><li style=\"color:red\">Error Updating properties.</li></ul>");
			}
			success = true;
			if (!simulate) {
				success = addDefaultComponents(newResource);
			}
			if (success) {
				response.getWriter().write("<ul><li>Added default components successfully.</li></ul>");
			} else {
				response.getWriter().write("<ul><li style=\"color:red\">Error Adding default components.</li></ul>");
			}
			success = true;
			Resource col1 = newResource.getChild(TITLE_COMP_REL_PATH);
			if (col1 != null) {
				if (!col1.adaptTo(ValueMap.class).containsKey(MIGRATED)) {
					LOG.debug("------------------------------------------------------------");
					LOG.debug("Content not migrated... proceede with migration...");
					Iterator<Resource> oldResources = getOldComponents(key, resourceResolver);
					if (oldResources != null) {
						Node rootNode = col1.adaptTo(Node.class);
						boolean migrated = false;
						boolean error = false;
						int text = 0;
						int title = 0;
						int errorCount = 0;
						while (oldResources.hasNext()) {
							ValueMap vm = oldResources.next().adaptTo(ValueMap.class);
							String resourceType = vm.get(SLING_RESOURCE_TYPE, String.class);
							LOG.debug("Old component is {}", resourceType);
							switch (resourceType) {
							case OLD_TITLE_COMPONENTS_RT:
								if (!simulate) {
									migrated = addTitleComponent(vm, rootNode, random.nextLong());
									if (migrated) {
										title++;
									} else {
										error = true;
										errorCount++;
									}
								} else {
									migrated = true;
								}
								break;
							case OLD_TEXT_COMPONENTS_RT:
								if (!simulate) {
									migrated = addTextComponent(vm, rootNode, random.nextLong());
									if (migrated) {
										text++;
									} else {
										error = true;
										errorCount++;
									}
								} else {
									migrated = true;
								}
								text++;
								break;
							}
						}
						if (migrated && !error) {
							if (!simulate) {
								success = setMigratedProperty(rootNode);
								if (success) {
									response.getWriter().write("<ul><li>" + text + " Text Components & " + title
											+ " Title Components Migrated successfully.</li></ul>");
								} else {
									response.getWriter().write(
											"<ul><li style=\"color:red\">Error setting migrated property to true.</li></ul>");
								}
							}
						} else {
							if (error) {
								if (!simulate) {
									success = setMigratedProperty(rootNode);
									if (success) {
										response.getWriter().write("<ul><li>" + text + " Text Components & " + title
												+ " Title Components Migrated successfully.</li></ul>");
										response.getWriter().write("<ul><li style=\"color:red\">" + errorCount
												+ " Exception(s) Occurred while migrating content.</li></ul>");
									} else {
										response.getWriter().write(
												"<ul><li style=\"color:red\">Error setting migrated property to true.</li></ul>");
									}
								}
							} else {
								response.getWriter().write(
										"<ul><li>No component found to migrate or an error occured while migrating.</li></ul>");
							}
						}
					}
				} else {
					response.getWriter().write("<ul><li>Already migrated!</li></ul>");
				}
			}
			response.getWriter().write("</ul>");
		}
	}

	public boolean updatePageProperties(Resource newResource, Resource oldResource) {
		Resource newPageContent = newResource.getChild(JCR_CONTENT);
		Resource oldPageContent = oldResource.getChild(JCR_CONTENT);
		if (newPageContent != null && oldPageContent != null) {
			ValueMap vm = oldPageContent.adaptTo(ValueMap.class);
			ModifiableValueMap newPage = newPageContent.adaptTo(ModifiableValueMap.class);
			if (StringUtils.isNotBlank((CharSequence) vm.get(KEYWORDS))) {
				newPage.put(KEYWORDS, vm.get(KEYWORDS));
			}
			if (StringUtils.isNotBlank((CharSequence) vm.get(JCR_DESCRIPTION))) {
				newPage.put(JCR_DESCRIPTION, vm.get(JCR_DESCRIPTION));
			}
			try {
				newPageContent.getResourceResolver().commit();
			} catch (PersistenceException e) {
				LOG.error("Error updating page peroperties : {}", e);
				return false;
			}
		}
		return true;
	}

	public boolean addDefaultComponents(Resource newResource) {
		Resource content = newResource.getChild(JCR_CONTENT);
		String rt = content.adaptTo(ValueMap.class).get(SLING_RESOURCE_TYPE, String.class);
		switch (rt) {
		case ARTICLE_PAGE_RT:
			return addComponent(newResource, "article_toolbar", ARTICLE_TOOLBAR_RT, COL_CTRL_COL0,
					Collections.EMPTY_MAP)
					& addComponent(newResource, "article_resources", ARTICLE_RESOURCES_RT, PAR, Collections.EMPTY_MAP)
					& addComponent(newResource, "subjects_covered",
							"scholastic/classroom-magazines-migration/components/content/subjects-covered",
							COL_CTRL_COL2, Collections.EMPTY_MAP);

		case ISSUE_PAGE_RT:
			Map<String, List<String>> map = new HashMap<String, List<String>>();
			List<String> values = new ArrayList<>();
			values.add("Articles");
			values.add("Teaching Resources");
			map.put("tabs", values);
			return addComponent(newResource, "issue_highlight",
					ISSUE_HIGHLIGHT_RT, PAR,
					Collections.EMPTY_MAP)
					& addComponent(newResource, "tabs",
							TABS_RT, PAR, map)
					& addComponent(newResource, "tab0",
							WCM_FOUNDATION_COMPONENTS_PARSYS, "par/tabs", Collections.EMPTY_MAP)
					& addComponent(newResource, "tab1",
							WCM_FOUNDATION_COMPONENTS_PARSYS, "par/tabs", Collections.EMPTY_MAP)
					& addComponent(newResource, "issue_article_aggreg",
							ISSUE_ARTICLE_AGGREGATOR_RT, "par/tabs/tab0", Collections.EMPTY_MAP)
					& addComponent(newResource, "issue_teaching_resou",
							ISSUE_TEACHING_RESOURCES_RT, "par/tabs/tab1", Collections.EMPTY_MAP);
					
		default:
			return true;
		}
	}

	public boolean addComponent(Resource newResource, String componentName, String resourceType, String relPath,
			Map<String, List<String>> values) {
		boolean success = true;
		Resource content = newResource.getChild(JCR_CONTENT);
			Resource parResource = content.getChild(relPath);
			try {
				if (parResource != null) {
					Node rootNode = parResource.adaptTo(Node.class);
					if (!rootNode.hasNode(componentName)) {
						Node title = rootNode.addNode(componentName);
						title.setProperty(SLING_RESOURCE_TYPE, resourceType);
						if (!values.isEmpty()) {
							for (String key : values.keySet()) {
								Value[] values1 = new Value[values.get(key).size()];
								int i = 0;
								for (String s : values.get(key)) {
									ValueFactory valueFactory = newResource.getResourceResolver().adaptTo(Session.class)
											.getValueFactory();
									values1[i] = valueFactory.createValue(s);
									i++;
								}
								title.setProperty(key, values1);
							}
						}
						rootNode.getSession().save();
					}
				} else {
					Node contentNode = content.adaptTo(Node.class);
					Node parNode = contentNode.addNode(relPath, NT_UNSTRUCTURED);
					parNode.setProperty(SLING_RESOURCE_TYPE, WCM_FOUNDATION_COMPONENTS_PARSYS);
					parNode.getSession().save();
					if (!parNode.hasNode(componentName)) {
						Node title = parNode.addNode(componentName);
						title.setProperty(SLING_RESOURCE_TYPE, resourceType);
						parNode.getSession().save();
					}
				}
			} catch (RepositoryException e) {
				LOG.error("Error Adding components : {}", e);
				return false;
			}
		return success;

	}

	boolean addTitleComponent(ValueMap vm, Node rootNode, long random) {
		boolean migrated = false;
		try {
			Node title = rootNode.addNode(ARTICLE_TITLE_PREFIX + random, NT_UNSTRUCTURED);
			title.setProperty(SLING_RESOURCE_TYPE, NEW_ARTICLE_TITLE_RT);
			title.setProperty(TEXT_IS_RICH, true);
			title.setProperty(ARTICLE_TEXT_IMAGE_POS, TOP);
			if (null != vm.get(JCR_TITLE)) {
				title.setProperty(RTEFIELD, vm.get(JCR_TITLE).toString());
			}
			if (null != vm.get(TYPE)) {
				switch (vm.get(TYPE).toString()) {
				case DIV_TITLE_DEK:
					title.setProperty(ARTICLE_TITLE_STYLES, "dek");
					break;
				case H1:
					title.setProperty(ARTICLE_TITLE_STYLES, "headline");
					break;
				case H2_TITLE_SUBHEAD:
					title.setProperty(ARTICLE_TITLE_STYLES, SUBHEAD);
					break;
				default:
					title.setProperty(ARTICLE_TITLE_STYLES, SUBHEAD);
				}
			} else {
				title.setProperty(ARTICLE_TITLE_STYLES, SUBHEAD);
			}
			title.getSession().save();
		} catch (RepositoryException e) {
			LOG.debug("Error adding Title component : {}", e);
			return migrated;
		}
		migrated = true;
		return migrated;
	}

	boolean addTextComponent(ValueMap vm, Node rootNode, long random) {
		boolean migrated = false;
		try {
			if (StringUtils.isNotBlank(vm.get(TEXT, String.class))) {
				Node text = rootNode.addNode("article_sidebar" + random, NT_UNSTRUCTURED);
				text.setProperty(SLING_RESOURCE_TYPE, ARTICLE_SIDEBAR_RT);
				text.setProperty(TEXT_IS_RICH, true);
				text.setProperty("lexileLevel", "default");
				text.setProperty("textAlignment", "left");
				text.setProperty("sectionText", vm.get(TEXT).toString());
				text.getSession().save();
				migrated = true;
			}
		} catch (RepositoryException e) {
			LOG.debug("Error adding Text component : {}", e);
			return migrated;
		}
		return migrated;
	}

	boolean setMigratedProperty(Node rootNode) {
		boolean success = true;
		try {
			rootNode.setProperty(MIGRATED, true).getSession().save();
		} catch (RepositoryException e) {
			LOG.error("Error setting migrated peoperty {}", e);
			success = false;
		}
		return success;
	}

	public Iterator<Resource> getOldComponents(String path, ResourceResolver resourceResolver) {
		String newPath = path + "/jcr:content/article/par_art";
		Resource resource = resourceResolver.getResource(newPath);
		return resource.listChildren();
	}

}