package com.scholastic.classmags.migration.services;

import javax.jcr.RepositoryException;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.GameModel;

/**
 * The Interface GameService.
 */
public interface GameService {

    /**
     * Fetch game data.
     *
     * @param assetPath
     *            the asset path
     * @return the game data
     */
    public GameModel fetchGameData( Resource resource) throws ClassmagsMigrationBaseException, RepositoryException, LoginException;
}
