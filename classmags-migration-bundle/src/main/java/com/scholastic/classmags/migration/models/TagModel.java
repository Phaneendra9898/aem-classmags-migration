package com.scholastic.classmags.migration.models;

import com.day.cq.tagging.Tag;

/**
 * The Class TagModel.
 */
public class TagModel {

    /** The tag **/
    private Tag tag;
    
    public TagModel(Tag tag){
        this.tag = tag;
    }

    /**
     * Gets the tag.
     *
     * @return the tag
     */
    public Tag getTag() {
        return tag;
    }

    /**
     * Sets the tag.
     * 
     * @param tag The tag
     *            
     */
    public final void setTag( final Tag tag ) {
        this.tag = tag;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ( ( tag == null ) ? 0 : tag.hashCode() );
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( this == obj )
            return true;
        if ( obj == null )
            return false;
        if ( getClass() != obj.getClass() )
            return false;
        TagModel other = ( TagModel ) obj;
        if ( tag == null ) {
            if ( other.tag != null )
                return false;
        } else if ( !tag.getName().equals( other.tag.getName() ) )
            return false;
        return true;
    }
    
}
