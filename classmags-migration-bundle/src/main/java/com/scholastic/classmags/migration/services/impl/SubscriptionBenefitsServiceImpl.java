package com.scholastic.classmags.migration.services.impl;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.services.SubscriptionBenefitsService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

@Component( immediate = true, metatype = false )
@Service( SubscriptionBenefitsService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Subsciption Benefits Data Service" ) } )
public class SubscriptionBenefitsServiceImpl implements SubscriptionBenefitsService {

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Override
    public List< ValueMap > fetchSubscriptionBenefits( String currentPath ) throws ClassmagsMigrationBaseException {
        Resource resource = CommonUtils
                .getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE )
                .getResource( currentPath.concat( ClassMagsMigrationConstants.JCR_SUBSCRIPTION_BENEFITS_PATH ) );
        return CommonUtils.fetchMultiFieldData( resource );
    }
}
