/**
 * 
 */
package com.scholastic.classmags.migration.models;

import java.util.Map;

/**
 * The Class VoteData.
 */
public class VoteData {

    /** The user role. */
    private String userRole;

    /** The user identifier. */
    private String userIdentifier;

    /** The question UUID. */
    private String questionUUID;

    /** The answer options. */
    private Map< String, String > answerOptions;

    /** The vote page path. */
    private String votePagePath;

    /** The disable voting date in milli sec. */
    private String disableVotingDateInMilliSec;

    /**
     * Gets the user identifier.
     *
     * @return the userIdentifier
     */
    public String getUserIdentifier() {
        return userIdentifier;
    }

    /**
     * Sets the user identifier.
     *
     * @param userIdentifier
     *            the userIdentifier to set
     */
    public void setUserIdentifier( String userIdentifier ) {
        this.userIdentifier = userIdentifier;
    }

    /**
     * Gets the question UUID.
     *
     * @return the questionUUID
     */
    public String getQuestionUUID() {
        return questionUUID;
    }

    /**
     * Sets the question UUID.
     *
     * @param questionUUID
     *            the questionUUID to set
     */
    public void setQuestionUUID( String questionUUID ) {
        this.questionUUID = questionUUID;
    }

    /**
     * Gets the answer options.
     *
     * @return the answerOptions
     */
    public Map< String, String > getAnswerOptions() {
        return answerOptions;
    }

    /**
     * Sets the answer options.
     *
     * @param answerOptions
     *            the answerOptions to set
     */
    public void setAnswerOptions( Map< String, String > answerOptions ) {
        this.answerOptions = answerOptions;
    }

    /**
     * Gets the vote page path.
     *
     * @return the votePagePath
     */
    public String getVotePagePath() {
        return votePagePath;
    }

    /**
     * Sets the vote page path.
     *
     * @param votePagePath
     *            the votePagePath to set
     */
    public void setVotePagePath( String votePagePath ) {
        this.votePagePath = votePagePath;
    }

    /**
     * Gets the user role.
     *
     * @return the userRole
     */
    public String getUserRole() {
        return userRole;
    }

    /**
     * Sets the user role.
     *
     * @param userRole
     *            the userRole to set
     */
    public void setUserRole( String userRole ) {
        this.userRole = userRole;
    }

    /**
     * Gets the disable voting date in milli sec.
     *
     * @return the disableVotingDateInMilliSec
     */
    public String getDisableVotingDateInMilliSec() {
        return disableVotingDateInMilliSec;
    }

    /**
     * Sets the disable voting date in milli sec.
     *
     * @param disableVotingDateInMilliSec
     *            the disableVotingDateInMilliSec to set
     */
    public void setDisableVotingDateInMilliSec( String disableVotingDateInMilliSec ) {
        this.disableVotingDateInMilliSec = disableVotingDateInMilliSec;
    }
}
