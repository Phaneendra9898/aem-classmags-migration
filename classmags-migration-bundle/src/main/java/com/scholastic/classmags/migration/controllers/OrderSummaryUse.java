package com.scholastic.classmags.migration.controllers;

import java.io.IOException;
import java.util.Collections;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.adobe.cq.sightly.WCMUsePojo;

public class OrderSummaryUse extends WCMUsePojo  {


	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger( OrderSummaryUse.class );

	private ConfigurationAdmin configAdmin;

	private String priceNodePath = "/jcr:content/data-page-par/marketing_subscribep";

	private String UNIT_PRICE = "unitPrice";
	
	private String SHIPPING_HANDLING = "shippingAndHandling";

	private ResourceResolver resourceResolver;

	@Override
	public void activate() throws Exception {
		// TODO Auto-generated method stub
		LOG.debug("Inside activate");
		resourceResolver = getResourceResolver();
		LOG.debug("Exiting activate");

	}

	/**
	 * Returns magazine prices for each magazine
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String,String> getMagazinePrices()
	{
		Map <String,String> magazinePrices = new HashMap<>();
		SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
		configAdmin = slingScriptHelper.getService(ConfigurationAdmin.class);
		try {
			Configuration conf = configAdmin.getConfiguration("com.scholastic.classmags.migration.services.impl.ResourcePathConfigServiceImpl");
			Dictionary<String, Object> properties = conf.getProperties();
			for(String key : Collections.list(properties.keys())){
				String path = properties.get(key) + priceNodePath ;
				Resource resource = resourceResolver.resolve(path);
				if(resource != null && !ResourceUtil.isNonExistingResource(resource)){
					ValueMap vm = resource.getValueMap();
					if(vm != null && vm.containsKey(UNIT_PRICE)){
						magazinePrices.put(key, vm.get(UNIT_PRICE).toString());
					}
				}
			}
		} catch (IOException e) {
			LOG.error("Exception while getting prices", e);
		}
		return magazinePrices;
	}
	
	
	/**
	 * Returns Shipping price for each magazine
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Map<String,String> getShippingPrice()
	{
		Map <String,String> magazinePrices = new HashMap<>();
		SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
		configAdmin = slingScriptHelper.getService(ConfigurationAdmin.class);
		try {
			Configuration conf = configAdmin.getConfiguration("com.scholastic.classmags.migration.services.impl.ResourcePathConfigServiceImpl");
			Dictionary<String, Object> properties = conf.getProperties();
			for(String key : Collections.list(properties.keys())){
				String path = properties.get(key) + priceNodePath ;
				Resource resource = resourceResolver.resolve(path);
				if(resource != null && !ResourceUtil.isNonExistingResource(resource)){
					ValueMap vm = resource.getValueMap();
					if(vm != null && vm.containsKey(SHIPPING_HANDLING)){
						magazinePrices.put(key, vm.get(SHIPPING_HANDLING).toString());
					}
				}
			}
		} catch (IOException e) {
			LOG.error("Exception while getting prices", e);
		}
		return magazinePrices;
	}
	

}
