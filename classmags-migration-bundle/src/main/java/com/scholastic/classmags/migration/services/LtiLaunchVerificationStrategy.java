package com.scholastic.classmags.migration.services;

import java.util.SortedMap;

public interface LtiLaunchVerificationStrategy {

    /**
     * 
     * @param map A map of LTI launch parameters 
     * @param launchUrl The application launch url
     * @param signature The OAuth signature received, to match against the generated signature
     * @param consumerKey The OAuth consumer key (retrieve the private key using this key)
     * @return
     */
    boolean performOAuthSignatureValidation(SortedMap<String, String> map, String launchUrl, String signature, String consumerKey);
    
    /**
     * 
     * @param now
     * @param timeStamp
     * @return
     */
    boolean performOAuthTimeStampValidation(long now, long timeStamp);
}