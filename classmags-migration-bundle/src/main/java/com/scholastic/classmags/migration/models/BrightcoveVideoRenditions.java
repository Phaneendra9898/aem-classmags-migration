package com.scholastic.classmags.migration.models;

/**
 * The Class BrightcoveVideoRenditions.
 */
public class BrightcoveVideoRenditions {

    /** The display name. */
    String displayName;

    /** The frame height. */
    Short frameHeight;

    /** The frame width. */
    Short frameWidth;

    /** The size. */
    String size;

    /** The video container. */
    String videoContainer;
    
    String videoDuration;

    /**
     * Gets the display name.
     * 
     * @return the display name
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Sets the display name.
     * 
     * @param displayName
     *            the new display name
     */
    public void setDisplayName( String displayName ) {
        this.displayName = displayName;
    }

    /**
     * Gets the frame height.
     * 
     * @return the frame height
     */
    public Short getFrameHeight() {
        return frameHeight;
    }

    /**
     * Sets the frame height.
     * 
     * @param frameHeight
     *            the new frame height
     */
    public void setFrameHeight( Short frameHeight ) {
        this.frameHeight = frameHeight;
    }

    /**
     * Gets the frame width.
     * 
     * @return the frame width
     */
    public Short getFrameWidth() {
        return frameWidth;
    }

    /**
     * Sets the frame width.
     * 
     * @param frameWidth
     *            the new frame width
     */
    public void setFrameWidth( Short frameWidth ) {
        this.frameWidth = frameWidth;
    }

    /**
     * Gets the size.
     * 
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * Sets the size.
     * 
     * @param size
     *            the new size
     */
    public void setSize( String size ) {
        this.size = size;
    }

    /**
     * Gets the video container.
     * 
     * @return the video container
     */
    public String getVideoContainer() {
        return videoContainer;
    }

    /**
     * Sets the video container.
     * 
     * @param videoContainer
     *            the new video container
     */
    public void setVideoContainer( String videoContainer ) {
        this.videoContainer = videoContainer;
    }

    /**
     * @return the videoDuration
     */
    public String getVideoDuration() {
        return videoDuration;
    }

    /**
     * @param videoDuration the videoDuration to set
     */
    public void setVideoDuration( String videoDuration ) {
        this.videoDuration = videoDuration;
    }

}
