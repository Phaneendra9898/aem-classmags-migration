package com.scholastic.classmags.migration.controllers;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.RecentBlogPostsData;
import com.scholastic.classmags.migration.services.RecentBlogPostsDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * The Class RecentBlogPostsUse.
 */
public class RecentBlogPostsUse extends WCMUsePojo {

    /** The recent blog posts data list. */
    private List< RecentBlogPostsData > recentBlogPostsDataList;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( RecentBlogPostsUse.class );

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() {

        String currentPath = get( ClassMagsMigrationConstants.URL, String.class );
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();

        if ( null != slingScriptHelper && StringUtils.isNotBlank( currentPath ) ) {
            RecentBlogPostsDataService recentBlogPostsDataService = slingScriptHelper
                    .getService( RecentBlogPostsDataService.class );
            try {
                setRecentBlogPostsDataList( recentBlogPostsDataService.fetchRecentBlogPostsData( currentPath ) );
            } catch ( ClassmagsMigrationBaseException e ) {
                LOG.error( e.getMessage(), e );
            }
        }
    }

    /**
     * Gets the recent blog posts data list.
     *
     * @return the recentBlogPostsDataList
     */
    public List< RecentBlogPostsData > getRecentBlogPostsDataList() {
        return recentBlogPostsDataList;
    }

    /**
     * Sets the recent blog posts data list.
     *
     * @param recentBlogPostsDataList
     *            the recentBlogPostsDataList to set
     */
    public void setRecentBlogPostsDataList( List< RecentBlogPostsData > recentBlogPostsDataList ) {
        this.recentBlogPostsDataList = recentBlogPostsDataList;
    }
}
