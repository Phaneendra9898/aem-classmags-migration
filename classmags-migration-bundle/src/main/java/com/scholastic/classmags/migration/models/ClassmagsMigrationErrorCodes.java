package com.scholastic.classmags.migration.models;

/**
 * ClassmagsMigration ErrorCodes.
 */
public enum ClassmagsMigrationErrorCodes {

    /** The email notification error. */
    EMAIL_NOTIFICATION_ERROR( "1001", "Error in Email Notification Service" ),

    /** The node creation error. */
    NODE_CREATION_ERROR( "1002", "Error in creation of node" ),

    /** The data storage error. */
    DATA_STORAGE_ERROR( "1003", "Error while storing the data" ),

    /** The reverse replication error. */
    REVERSE_REPLICATION_ERROR( "1004", "Error in Reverse Replication" ),

    /** The node generation error. */
    NODE_GENERATION_ERROR( "1005", "Error while generating Node Structure" ),

    /** The access denied error. */
    ACCESS_DENIED_ERROR( "1006", "Error while updating Node due to Access Denied" ),

    /** The referential integrity error. */
    REFERENTIAL_INTEGRITY_ERROR( "1007", "Error while updating Node due violation of Referential Integrity" ),

    /** The constraint violation error. */
    CONSTRAINT_VIOLATION_ERROR( "1008", "Error while updating Node due constraint violation" ),

    /** The invalid item state error. */
    INVALID_ITEM_STATE_ERROR( "1009", "Error while updating Node due invalid item state" ),

    /** The version error. */
    VERSION_ERROR( "1010", "Error while updating Node due to incorrect verion" ),

    /** The lock error. */
    LOCK_ERROR( "1011", "Error while updating Node due to lock" ),

    /** The no such node type error. */
    NO_SUCH_NODE_TYPE_ERROR( "1012", "Error due to unavailablitly of Node" ),

    /** The repository error. */
    REPOSITORY_ERROR( "1013", "Error while updating Node due to repository error" ),

    /** The login error. */
    LOGIN_ERROR( "1014", "Error due to authorization or authentication failed" ),

    /** The unsupported encoding error. */
    UNSUPPORTED_ENCODING_ERROR( "1015", "Error due to encoding format not supported" ),

    /** The add bookmark error message. */
    ADD_BOOKMARK_ERROR_MESSAGE( "500", "Unable to Add Bookmark" ),

    /** The delete bookmark error message. */
    DELETE_BOOKMARK_ERROR_MESSAGE( "500", "Unable to Delete Bookmark" ),

    /** The invalid user exception message. */
    INVALID_USER_EXCEPTION_MESSAGE( "400", "Invalid User - ANONYMOUS User Access" ),

    /** The permission denied message. */
    PERMISSION_DENIED_MESSAGE( "400", "Do not have permission to Add Or Delete Data" ),

    /** The vote submission limit message. */
    VOTE_SUBMISSION_LIMIT_MESSAGE( "400", "Vote Submission Limit Exceeded" ),

    /** The submit vote error message. */
    SUBMIT_VOTE_ERROR_MESSAGE( "500", "Unable to Submit Vote" ),

    /** The get vote data error message. */
    GET_VOTE_DATA_ERROR_MESSAGE( "500", "Unable to Load Vote Data" ),

    /** The json conversion error. */
    JSON_CONVERSION_ERROR( "1016", "Error due to JSON data conversion" );

    /** The error code. */
    private final String errorCode;

    /** The error description. */
    private final String errorDescription;

    /**
     * Instantiates a new classmags migration error codes.
     *
     * @param errorCode
     *            the error code
     * @param errorDescription
     *            the error description
     */
    private ClassmagsMigrationErrorCodes( String errorCode, String errorDescription ) {
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
    }

    /**
     * Gets the error code.
     *
     * @return the errorCode
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Gets the error description.
     *
     * @return the errorDescription
     */
    public String getErrorDescription() {
        return errorDescription;
    }

    /**
     * Gets the error code int.
     *
     * @return the error code int
     */
    public int getErrorCodeInt() {
        return Integer.parseInt( errorCode );
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return errorCode + ": " + errorDescription;
    }
}
