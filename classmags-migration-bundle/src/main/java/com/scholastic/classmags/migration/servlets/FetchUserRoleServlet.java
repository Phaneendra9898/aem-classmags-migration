package com.scholastic.classmags.migration.servlets;

import static com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.scholastic.classmags.migration.models.UIdOrgContextSubHeaderEntitlements;
import com.scholastic.classmags.migration.models.UserIdCookieData;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.CookieUtil;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * Servlet for returning role of the current user.
 */
@SlingServlet( paths = "/bin/classmags/getuserrole", methods = "GET" )
public class FetchUserRoleServlet extends SlingSafeMethodsServlet {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6872745186558524128L;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( FetchUserRoleServlet.class );

    /** The user role. */
    private String userRole = USER_TYPE_ANONYMOUS;

    /** The role JSON. */
    private JSONObject roleJSON;

    /** The user data. */
    private JSONObject userData;

    /** The entitlements array object. */
    private JSONArray entitlementsArrayObject;

    @Reference
    ResourcePathConfigService resourcePathConfigService;

    @Reference
    PropertyConfigService propertyConfigService;
    
    @Reference
    Externalizer externalizer;
    
    @Reference
    ResourceResolverFactory resourceResolverFactory;

    /**
     * Method doGet.
     *
     * @param request
     *            the request
     * @param response
     *            the response
     * @throws ServletException
     *             the servlet exception
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    @Override
    public void doGet( SlingHttpServletRequest request, SlingHttpServletResponse response )
            throws ServletException, IOException {

        try {
            userRole = RoleUtil.getUserRole( request );

            roleJSON = new JSONObject();
            roleJSON.put( "role", userRole );

            // set user related data
            if ( StringUtils.equals( ClassMagsMigrationConstants.USER_TYPE_STUDENT, userRole ) ) {
                userData = new JSONObject();

            } else {
                setUserData( request );
            }

            roleJSON.put( "user", userData );
            setEntitlementsArrayObject( populateEntitlementsArrayObject( request ) );
            roleJSON.put( "entitlements", getEntitlementsArrayObject() );

        } catch ( Exception e ) {
            LOG.error( "Error fetching user information {}", e );
        } finally {
            LOG.debug( "User is {}", userRole );
            response.setContentType( "application/json" );
            response.setCharacterEncoding( "UTF-8" );
            response.getWriter().write( roleJSON.toString() );
        }
    }

    /**
     * Populate entitlements array object.
     *
     * @param request
     *            the request
     * @return the JSON array
     * @throws JSONException
     *             the JSON exception
     * @throws LoginException
     */
    private JSONArray populateEntitlementsArrayObject( SlingHttpServletRequest request )
            throws JSONException, LoginException {

        UserIdCookieData userIdCookie = CookieUtil.fetchUserIdCookieData( request );
        entitlementsArrayObject = new JSONArray();
        
        ResourceResolver resourceResolver = CommonUtils.getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE );

        if ( null != userIdCookie ) {
            for ( UIdOrgContextSubHeaderEntitlements entitlement : userIdCookie.getObjUIdHeaderOrgContext()
                    .getObjListUIdOrgContextSubHeaderEntitlements() ) {

                JSONObject entitlementJSONObject = new JSONObject();
                entitlementJSONObject.put( "applicationCode", entitlement.getApplicationCode() );
                entitlementJSONObject.put( "status", entitlement.getStatus() );
                entitlementJSONObject.put( "groupCode", entitlement.getGroupCode() );
                entitlementJSONObject.put( "id", entitlement.getId() );
                entitlementJSONObject.put( "name", entitlement.getName() );
                entitlementJSONObject.put( "thumbnail", entitlement.getThumbnail() );

                String page=propertyConfigService.getDataConfigProperty(
                        ClassMagsMigrationConstants.HOMEPAGE_LOGGEDIN,
                        CommonUtils.getDataPath( resourcePathConfigService, entitlement.getApplicationCode() ) );
                if(StringUtils.isNotBlank(page) && StringUtils.isNotBlank(entitlement.getApplicationCode())){
                	String homePageLoggedIn = externalizer.externalLink( resourceResolver, entitlement.getApplicationCode(), page);
                	if(!homePageLoggedIn.endsWith(ClassMagsMigrationConstants.HTML_SUFFIX)){
                		homePageLoggedIn = homePageLoggedIn.concat(ClassMagsMigrationConstants.HTML_SUFFIX);
                	}
                	entitlementJSONObject.put( "url", homePageLoggedIn );
                }

                entitlementsArrayObject.put( entitlementJSONObject );
                LOG.debug( "Entitlement Data: " + entitlementJSONObject );
            }
        }
        return entitlementsArrayObject;
    }

    /**
     * Sets the user data.
     *
     * @param request
     *            the new user data
     */
    /*
     * Sets the user data.
     * 
     * @param request The sling HTTP servlet request.
     */
    private void setUserData( SlingHttpServletRequest request ) {

        UserIdCookieData userIdCookie = CookieUtil.fetchUserIdCookieData( request );
        String magazineName = request.getParameter( "magazineName" );
        boolean loginStatus = false;
        String userId = USER_TYPE_ANONYMOUS;
        String orgId = StringUtils.EMPTY;
        List< String > subscriptions = new ArrayList< >();
        boolean subscriptionStatus = false;

        if ( null != userIdCookie && StringUtils.equals( ClassMagsMigrationConstants.USER_TYPE_TEACHER, userRole ) ) {
            loginStatus = true;
            userId = userIdCookie.getObjUIdHeaderIdentifiers().getStaffId();
            orgId = userIdCookie.getObjUIdHeaderOrgContext().getObjUIdOrgContextSubHeaderOrganization().getOrgId();
            for ( UIdOrgContextSubHeaderEntitlements entitlement : userIdCookie.getObjUIdHeaderOrgContext()
                    .getObjListUIdOrgContextSubHeaderEntitlements() ) {
                subscriptions.add( entitlement.getApplicationCode() );
                subscriptionStatus = getSubscriptionStatus( magazineName, entitlement.getApplicationCode(),
                        entitlement.getStatus() );
            }
        }
        try {
            userData = new JSONObject();
            userData.put( "loginStatus", loginStatus );
            userData.put( "id", userId );
            userData.put( "type", userRole );
            userData.put( "orgId", orgId );
            userData.put( "subscriptionStatus", subscriptionStatus );
            userData.put( "subscription", subscriptions.toString() );
        } catch ( JSONException e ) {
            LOG.error( "Error fetching user information {}", e );
        }

    }

    /**
     * Gets the subscription status.
     *
     * @param magazineName
     *            the magazine name
     * @param applicationCode
     *            the application code
     * @param entitlementStatus
     *            the entitlement status
     * @return the subscription status
     */
    /*
     * Gets the subscription status.
     * 
     * @param magazineName The magazine name.
     * 
     * @param applicationCode The application code.
     * 
     * @param entitlementStatus The entitlement status.
     * 
     * @return The subscription status.
     */
    private boolean getSubscriptionStatus( String magazineName, String applicationCode, String entitlementStatus ) {
        boolean subscriptionStatus = false;
        if ( StringUtils.equals( magazineName, applicationCode ) ) {
            subscriptionStatus = StringUtils.equals( "true", entitlementStatus ) ? true : false;
        }
        return subscriptionStatus;
    }

    /**
     * Sets the entitlements array object.
     *
     * @param entitlementsArrayObject
     *            the entitlementsArrayObject to set
     */
    public void setEntitlementsArrayObject( JSONArray entitlementsArrayObject ) {
        this.entitlementsArrayObject = entitlementsArrayObject;
    }

    /**
     * Gets the entitlements array object.
     *
     * @return the entitlementsArrayObject
     */
    public JSONArray getEntitlementsArrayObject() {
        return entitlementsArrayObject;
    }

}

