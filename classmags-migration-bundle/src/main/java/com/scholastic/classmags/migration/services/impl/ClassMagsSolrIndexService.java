package com.scholastic.classmags.migration.services.impl;

import java.util.Map;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.solr.client.solrj.SolrClient;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.services.AbstractSolrIndexService;

@Component( name = "com.scholastic.classmags.migration.services.impl.ClassMagazineSolrIndexService", label = "AEM Solr Search - Class Mags Solr Index Service", description = "A service for creating Solr index", immediate = true, metatype = true )
@Service( ClassMagsSolrIndexService.class )
@Properties( { @Property( name = Constants.SERVICE_VENDOR, value = "Solr" ),
        @Property( name = Constants.SERVICE_DESCRIPTION, value = "A Solr indexing service for the Class Mags Site" ) })
/**
 *
 */
public class ClassMagsSolrIndexService extends AbstractSolrIndexService {

    private static final Logger LOG = LoggerFactory.getLogger( ClassMagsSolrIndexService.class );

    @Reference
    SolrClassMagsConfigService solrConfigService;

    @Activate
    protected void activate( final Map< String, String > config ) {
        resetService( config );
    }

    @Modified
    protected void modified( final Map< String, String > config ) {
        resetService( config );
    }

    @Override
    protected SolrClient getSolrIndexClient() {
        LOG.debug( "Retrieving SolrClient for Indexing" );
        return solrConfigService.getSolrClient();
    }

    @Override
    protected SolrClient getSolrQueryClient() {
        LOG.debug( "Retrieving SolrClient for Querying" );
        return solrConfigService.getSolrClient();
    }

    private void resetService( final Map< String, String > config ) {
        LOG.info( "Resetting Solr index service using configuration: " + config );
        solrConfigService.resetSolrServerClients();
    }
    
    public void resetSolrClient( ) {
        LOG.info( "Index Serviice Resetting the solr clients... ");
        solrConfigService.resetSolrServerClients();
    }

    @Override
    public String getCoreName() {
        return solrConfigService.getCoreName();
    }

}
