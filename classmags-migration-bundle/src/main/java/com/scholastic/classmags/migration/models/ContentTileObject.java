package com.scholastic.classmags.migration.models;

import java.util.Date;

/**
 * The Class ContentTileObject.
 */
public class ContentTileObject {

    /** The associated image. */
    private String associatedImage;

    /** The content tile title. */
    private String contentTileTitle;

    /** The content tile desc. */
    private String contentTileDesc;

    /** The tags. */
    private String tags;

    /** The sorting date. */
    private String sortingDate;

    /** The custom content link. */
    private String customContentLink;

    /** The custom content date. */
    private Date customContentDate;

    /** The video duration. */
    private String videoDuration;

    /** The page path. */
    private String pagePath;

    /** The video id. */
    private long videoId;

    /** The game model. */
    private GameModel gameModel;
    
    /** The bookmark path. */
    private String bookmarkPath;
    
    /** The Content Type. */
    private String contentType;


    /** The user type. */
    private String userType;
    
    /** The Asset Label. */
    private String assetLabel;
    
    /** The Description. */
    private String contentDescription;
    
    /** The New Window. */
    private String newWindow;
    
  	

	/**
     * Gets the user type.
     * 
     * @return the userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * Sets the user type.
     * 
     * @param userType
     *            the userType to set
     */
    public void setUserType( String userType ) {
        this.userType = userType;
    }

    /**
     * Gets the game model.
     * 
     * @return the gameModel
     */
    public GameModel getGameModel() {
        return gameModel;
    }

    /**
     * Sets the game model.
     * 
     * @param gameModel
     *            the gameModel to set
     */
    public void setGameModel( GameModel gameModel ) {
        this.gameModel = gameModel;
    }

    /**
     * Gets the associated image.
     * 
     * @return the associatedImage
     */
    public String getAssociatedImage() {
        return associatedImage;
    }

    /**
     * Sets the associated image.
     * 
     * @param associatedImage
     *            the associatedImage to set
     */
    public void setAssociatedImage( String associatedImage ) {
        this.associatedImage = associatedImage;
    }

    /**
     * Gets the content tile title.
     * 
     * @return the contentTileTitle
     */
    public String getContentTileTitle() {
        return contentTileTitle;
    }

    /**
     * Gets the content tile desc.
     * 
     * @return the contentTileDesc
     */
    public String getContentTileDesc() {
        return contentTileDesc;
    }

    /**
     * Sets the content tile title.
     * 
     * @param contentTileTitle
     *            the contentTileTitle to set
     */
    public void setContentTileTitle( String contentTileTitle ) {
        this.contentTileTitle = contentTileTitle;
    }

    /**
     * Sets the content tile desc.
     * 
     * @param contentTileDesc
     *            the contentTileDesc to set
     */
    public void setContentTileDesc( String contentTileDesc ) {
        this.contentTileDesc = contentTileDesc;
    }

    /**
     * Gets the sorting date.
     * 
     * @return the sortingDate
     */
    public String getSortingDate() {
        return sortingDate;
    }

    /**
     * Sets the sorting date.
     * 
     * @param sortingDate
     *            the sortingDate to set
     */
    public void setSortingDate( String sortingDate ) {
        this.sortingDate = sortingDate;
    }

    /**
     * Gets the custom content link.
     * 
     * @return the customContentLink
     */
    public String getCustomContentLink() {
        return customContentLink;
    }

    /**
     * Sets the custom content link.
     * 
     * @param customContentLink
     *            the customContentLink to set
     */
    public void setCustomContentLink( String customContentLink ) {
        this.customContentLink = customContentLink;
    }

    /**
     * Gets the custom content date.
     * 
     * @return the customContentDate
     */
    public Date getCustomContentDate() {
        return customContentDate;
    }

    /**
     * Sets the custom content date.
     * 
     * @param customContentDate
     *            the customContentDate to set
     */
    public void setCustomContentDate( Date customContentDate ) {
        this.customContentDate = customContentDate;
    }

    /**
     * Gets the video duration.
     * 
     * @return the videoDuration
     */
    public String getVideoDuration() {
        return videoDuration;
    }

    /**
     * Sets the video duration.
     * 
     * @param videoDuration
     *            the videoDuration to set
     */
    public void setVideoDuration( String videoDuration ) {
        this.videoDuration = videoDuration;
    }

    /**
     * Gets the tags.
     *
     * @return the tags
     */
    public String getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     *
     * @param tags
     *            the tags to set
     */
    public void setTags( String tags ) {
        this.tags = tags;
    }

    /**
     * Gets the page path.
     *
     * @return the pagePath
     */
    public String getPagePath() {
        return pagePath;
    }

    /**
     * Sets the page path.
     *
     * @param pagePath
     *            the pagePath to set
     */
    public void setPagePath( String pagePath ) {
        this.pagePath = pagePath;
    }

    /**
     * Gets the video id.
     *
     * @return the videoId
     */
    public long getVideoId() {
        return videoId;
    }

    /**
     * Sets the video id.
     *
     * @param videoId
     *            the videoId to set
     */
    public void setVideoId( long videoId ) {
        this.videoId = videoId;
    }
    
    /**
     * Gets the Content Type.
     *
     * @return the contentType
     */
    public String getContentType() {
		return contentType;
	}
    
    /**
     * Sets the Content Type.
     *
     * @param contentType
     *            the contentType to set
     */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

    /**
     * Gets the bookmark path.
     *
     * @return the bookmarkPath
     */
    public String getBookmarkPath() {
        return bookmarkPath;
    }

    /**
     * Sets the bookmark path.
     *
     * @param bookmarkPath
     *            the bookmarkPath to set
     */
    public void setBookmarkPath( String bookmarkPath ) {
        this.bookmarkPath = bookmarkPath;
    }
    
    /**
     * Gets the Asset Label.
     *
     * @return the assetLabel
     */
    public String getAssetLabel() {
		return assetLabel;
	}
    
    /**
     * Sets the Asset Label.
     *
     * @param assetLabel
     *            the assetLabel to set
     */
	public void setAssetLabel(String assetLabel) {
		this.assetLabel = assetLabel;
	}
	
	/**
     * Gets the content Description.
     *
     * @return the contentDescription
     */
	public String getContentDescription() {
		return contentDescription;
	}
	
	/**
     * Sets the content Description.
     *
     * @param contentDeescription
     *            the contentDescription to set
     */
	public void setContentDescription(String contentDescription) {
		this.contentDescription = contentDescription;
	}
	
	/**
     * Gets the boolean value.
     *
     * @return the booleanValue
     */
	public String getNewWindow() {
		return newWindow;
	}
	
	/**
     * Sets the Boolean Value.
     *
     * @param boolean value
     *            the booleanValue to set
     */
	public void setNewWindow(String newWindow) {
		this.newWindow = newWindow;
	}

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append( "ContentTileObject [associatedImage=" );
        builder.append( associatedImage );
        builder.append( ", contentTileTitle=" );
        builder.append( contentTileTitle );
        builder.append( ", contentTileDesc=" );
        builder.append( contentTileDesc );
        builder.append( ", tags=" );
        builder.append( tags );
        builder.append( ", sortingDate=" );
        builder.append( sortingDate );
        builder.append( ", customContentLink=" );
        builder.append( customContentLink );
        builder.append( ", customContentDate=" );
        builder.append( customContentDate );
        builder.append( ", videoDuration=" );
        builder.append( videoDuration );
        builder.append( ", videoId=" );
        builder.append( videoId );
        builder.append( ", pagePath=" );
        builder.append( pagePath );
        builder.append( ", bookmarkPath=" );
        builder.append( bookmarkPath );
        builder.append( "]" );
        return builder.toString();
    }

}
