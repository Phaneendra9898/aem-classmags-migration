package com.scholastic.classmags.migration.social.impl.operations;

import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.servlets.post.PostOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.social.scf.OperationException;
import com.adobe.cq.social.scf.SocialComponentFactoryManager;
import com.adobe.cq.social.scf.SocialOperationResult;
import com.adobe.cq.social.scf.core.operations.AbstractSocialOperation;
import com.scholastic.classmags.migration.social.api.BookmarkService;
import com.scholastic.classmags.migration.social.api.BookmarkSocialComponent;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;

/**
 * A POST Endpoint for AddBookmarkOperation that accepts requests for adding
 * bookmarks. This class responds to all POST requests with a
 * :operation=social:bookmark:addbookmark parameter. For example, curl
 * http://localhost:4502/content/classroom-magazines/testissuepage/_jcr_content/
 * par/bookmark.social.json -uadmin:admin -v POST -H "Accept:application/json"
 * --data ":operation=social:bookmark:addbookmark
 * &bookmarkPagePath=/content/test_folder/scienceworld/issues/2016-17/090516/
 * invisible-train.html
 * &bookmarkedTeachingResourcePath=/content/test_folder/scienceworld/issues/2016
 * -17/090516/invisible-train.pdf&appName=scienceworld"
 */
@Component( immediate = true )
@Service
@Property( name = PostOperation.PROP_OPERATION_NAME, value = ClassMagsMigrationASRPConstants.SOCIAL_ADD_BOOKMARK_OPERATION )
public class AddBookmarkOperation extends AbstractSocialOperation {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( AddBookmarkOperation.class );

    /** The bookmark service. */
    @Reference
    private BookmarkService bookmarkService;

    /** The scf manager. */
    @Reference
    private SocialComponentFactoryManager scfManager;

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.social.scf.core.operations.AbstractSocialOperation#
     * performOperation(org.apache.sling.api.SlingHttpServletRequest)
     */
    @Override
    protected SocialOperationResult performOperation( SlingHttpServletRequest slingHttpServletRequest )
            throws OperationException {

        LOG.info( ClassMagsMigrationASRPConstants.OPERATION_START
                + ClassMagsMigrationASRPConstants.SOCIAL_ADD_BOOKMARK_OPERATION );

        final Resource bookmarkResource = this.bookmarkService.getBookmarkResource( slingHttpServletRequest );

        SocialOperationResult addSocialOperationResult = SocialASRPUtils.getSocialOperationResult(
                slingHttpServletRequest, HttpServletResponse.SC_OK,
                ClassMagsMigrationASRPConstants.HTTP_SUCCESS_MESSAGE, bookmarkResource, this.scfManager,
                BookmarkSocialComponent.BOOKMARK_RESOURCE_TYPE );

        LOG.info( ClassMagsMigrationASRPConstants.OPERATION_END );

        return addSocialOperationResult;
    }
}
