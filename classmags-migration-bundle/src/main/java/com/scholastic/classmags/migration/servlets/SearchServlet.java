package com.scholastic.classmags.migration.servlets;

import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.FLD_TYPE_VAL_ARTICLE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.FLD_TYPE_VAL_ISSUE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.PREFILTER;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.QUERY_TYPE_DISMAX;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.ROLE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.ROWS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SITE_ID;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SORT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.START;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SUBJECTS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.TEXT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.TYPES;

import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.models.SearchResponse;
import com.scholastic.classmags.migration.services.ClassMagsQueryService;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * Servlet for Search
 * 
 *
 */
@SlingServlet( paths = "/bin/classmags/migration/search", methods = "GET" )
public class SearchServlet extends SlingSafeMethodsServlet {
    private static final Logger LOG = LoggerFactory.getLogger( SearchServlet.class );
    private static final long serialVersionUID = 7343119428931025552L;

    private static final String PAGE_PATH = "path";

    private static final String A_TYPES = "atypes";

    private static final String I_SUBJECTS = "isubjects";
    private static final String A_SUBJECTS = "asubjects";

    private static final String I_SORT = "isort";
    private static final String I_ROWS = "irows";
    private static final String I_START = "istart";

    private static final String A_SORT = "asort";
    private static final String A_ROWS = "arows";
    private static final String A_START = "astart";

    @Reference
    private ClassMagsQueryService queryService;

    @Reference
    private MagazineProps magazineProps;

    protected boolean includeFacet = true;

    /**
     * Method doGet
     */
    @Override
    public void doGet( SlingHttpServletRequest request, SlingHttpServletResponse response )
            throws ServletException, IOException {
        List< SearchResponse > resultList = new ArrayList< >();

        Map< String, String > issueParams = new HashMap< >();
        Map< String, String > articleParams = new HashMap< >();

        // get queryParam
        String text = request.getParameter( TEXT );
        String pagePath = request.getParameter( PAGE_PATH );
        String role = RoleUtil.getUserRole( request );

        String siteId = magazineProps.getMagazineName( pagePath );
        LOG.info( "Site Id : {}", siteId );
        String lowerCaseText = StringUtils.EMPTY;
        if ( StringUtils.isNotBlank(text) ) {
            lowerCaseText = text.trim().toLowerCase();
            lowerCaseText = URLDecoder.decode(lowerCaseText,java.nio.charset.StandardCharsets.UTF_8.toString());
        }
        issueParams.put( TEXT, lowerCaseText );
        issueParams.put( START, request.getParameter( I_START ) );
        issueParams.put( ROWS, request.getParameter( I_ROWS ) );
        issueParams.put( SORT, request.getParameter( I_SORT ) );
        issueParams.put( SUBJECTS, request.getParameter( I_SUBJECTS ) );
        issueParams.put( SITE_ID, siteId );
        issueParams.put( ROLE, role );
        issueParams.put( PREFILTER, FLD_TYPE_VAL_ISSUE );

        articleParams.put( TEXT, lowerCaseText );
        articleParams.put( START, request.getParameter( A_START ) );
        articleParams.put( ROWS, request.getParameter( A_ROWS ) );
        articleParams.put( SORT, request.getParameter( A_SORT ) );
        articleParams.put( SUBJECTS, request.getParameter( A_SUBJECTS ) );
        articleParams.put( TYPES, request.getParameter( A_TYPES ) );
        articleParams.put( SITE_ID, siteId );
        articleParams.put( ROLE, role );
        articleParams.put( PREFILTER, FLD_TYPE_VAL_ARTICLE );

        try {
            SearchResponse issueResults = queryService.getSearchResults( issueParams, true, QUERY_TYPE_DISMAX );
            if ( null != issueResults ) {
                resultList.add( issueResults );
            }

            SearchResponse articleResults = queryService.getSearchResults( articleParams, true, QUERY_TYPE_DISMAX );

            if ( null != issueResults ) {
                resultList.add( articleResults );
            }

        } catch ( Exception e ) {
            LOG.error( "Error Search Services", e );
        } finally {
            // render data in JSON format
            response.setContentType( "application/json" );
            response.setCharacterEncoding( "UTF-8" );
            // respond back
            if ( !resultList.isEmpty() ) {
                response.getWriter().write( queryService.getResultsAsJSONArrayString( resultList ) );
            } else {
                response.getWriter().write( "{}" );
            }

        }

    }

}
