package com.scholastic.classmags.migration.servlets;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.services.PastIssuesService;

/**
 * Servlet used to get the past issues and related metadata and return it in
 * either json or object form.
 */
@SlingServlet( paths = { "/bin/classmags/migration/pastissues" } )
public class PastIssuesServlet extends SlingAllMethodsServlet {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1496871066150117433L;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( PastIssuesServlet.class );

    /** The past issues service. */
    @Reference
    private PastIssuesService pastIssuesService;

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.sling.api.servlets.SlingSafeMethodsServlet#doGet(org.apache
     * .sling.api.SlingHttpServletRequest,
     * org.apache.sling.api.SlingHttpServletResponse)
     */
    @Override
    protected void doGet( SlingHttpServletRequest request, SlingHttpServletResponse response ) throws IOException {
        doPost( request, response );
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache
     * .sling.api.SlingHttpServletRequest,
     * org.apache.sling.api.SlingHttpServletResponse)
     */
    @Override
    protected void doPost( SlingHttpServletRequest request, SlingHttpServletResponse response ) throws IOException {
        LOG.info( ":::: Inside doPost method of PastIssuesServlet ::::" );

        String pastIssuesJson = StringUtils.EMPTY;
        String currentPagePath = request.getParameter( "path" );

        if ( StringUtils.isNotBlank( currentPagePath ) ) {
            pastIssuesJson = pastIssuesService.getPastIssuesJson( currentPagePath );
        }
        // Write the response
        response.setContentType( "application/json" );
        response.getWriter().write( pastIssuesJson );
    }
}
