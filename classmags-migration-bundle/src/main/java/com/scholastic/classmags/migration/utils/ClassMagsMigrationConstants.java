package com.scholastic.classmags.migration.utils;

import java.util.Arrays;
import java.util.List;

/**
 * The Class ClassMagsMigrationConstants.
 */
public class ClassMagsMigrationConstants {

    /** The Constant array ASSET_EXTENTIONS. */
    public static final List< String > ASSET_EXTENTIONS = Arrays.asList( "pdf" );

    /** The Constant VOCAB_WORD. */
    public static final String VOCAB_WORD = "vocabWord";

    /** The Constant VOCAB_WORD_DEFINITION. */
    public static final String VOCAB_WORD_DEFINITION = "definition";

    /** The Constant VOCAB_WORD_IMAGE_PATH. */
    public static final String VOCAB_WORD_IMAGE_PATH = "imagePath";
    
    /** The Constant VOCAB_WORD_IMAGE_CREDITS. */
    public static final String VOCAB_WORD_IMAGE_CREDITS = "imageCredits";

    /** The Constant VOCAB_WORD_AUDIO_PATH. */
    public static final String VOCAB_WORD_AUDIO_PATH = "audioPath";

    /** The Constant VOCAB_WORD_P_TEXT. */
    public static final String VOCAB_WORD_P_TEXT = "pronunciation-text";

    /** The Constant VOCAB_NODE_PATH. */
    public static final String VOCAB_NODE_PATH = "/vocabulary-words";

    /** The Constant ARTICLE_TEXT_TITLE. */
    public static final String ARTICLE_TEXT_TITLE = "title";

    /** The Constant ARTICLE_LEXILE_LEVEL. */
    public static final String ARTICLE_LEXILE_LEVEL = "readingLevel";

    /** The Constant QUERY_PATH. */
    public static final String QUERY_PATH = "path";

    /** The Constant RESULTS_LIMIT. */
    public static final String RESULTS_LIMIT = "p.limit";

    /** The Constant QUERY_LOWER_BOUND. */
    public static final String QUERY_LOWER_BOUND = "-1";

    /** The Constant QUERY_ORDER_BY. */
    public static final String QUERY_ORDER_BY = "orderby";
    
    /** The Constant QUERY_ORDER_BY_SORT. */
    public static final String QUERY_ORDER_BY_SORT = "orderby.sort";
    
    /** The Constant ASC. */
    public static final String ASC = "asc";

    /** The Constant QUERY_NODE_NAME. */
    public static final String QUERY_NODE_NAME = "nodename";

    /** The Constant QUERY_PARAM_1. */
    public static final String QUERY_PARAM_1 = "1_property";

    /** The Constant QUERY_PARAM_1_VALUE. */
    public static final String QUERY_PARAM_1_VALUE = "1_property.value";

    /** The Constant QUERY_TEMPLATE_TAG. */
    public static final String QUERY_TEMPLATE_TAG = "cq:template";

    /** The Constant QUERY_RESOURCE_TYPE_TAG. */
    public static final String QUERY_RESOURCE_TYPE_TAG = "sling:resourceType";

    /** The Constant ARTICLE_CONFIG_RESOURCE_TYPE_PATH. */
    public static final String ARTICLE_CONFIG_RESOURCE_TYPE_PATH = "scholastic/classroom-magazines-migration/components/content/article-configuration";

    /** The Constant LEXILE_LEVEL_NODE. */
    public static final String LEXILE_LEVEL_NODE = "/jcr:content/article-configuration/lexileSettings";

    /** The Constant JCR_CONTENT_PATH. */
    public static final String ARTICLE_CONFIGURATION_PATH = "/jcr:content/article-configuration";

    /** The Constant SORTING_DATE_FORMAT. */
    public static final String JCR_SORTING_DATE_FORMAT = "jcr:sortingDateFormat";

    /** The Constant GLOBAL_NAV_DATA_PATH. */
    public static final String GLOBAL_NAV_DATA_PATH = "/jcr:content/global-navigation-container/global-nav-par";

    /** The Constant JCR_CONTENT_NODE. */
    public static final String JCR_CONTENT_NODE = "/jcr:content";

    /** The Constant JCR_ISSUE_ASSETS_PATH. */
    public static final String JCR_ISSUE_ASSETS_PATH = "/jcr:content/par/issue_highlight/issueKeyAssets";
    
    /** The Constant JCR_ISSUE_ASSETS_PATH. */
    public static final String JCR_MARKETING_ASSETS_PATH = "/jcr:content/par/marketing_sampleissu/issueKeyAssets";

    /** The Constant SORTING_DATE. */
    public static final String SORTING_DATE = "sortingdate";

    /** The Constant ASSET_SORTING_DATE. */
    public static final String ASSET_SORTING_DATE = "sortingDate";

    /** The Constant DISPLAY_DATE. */
    public static final String DISPLAY_DATE = "displaydate";

    /** The Constant ASSET_DISPLAY_DATE. */
    public static final String ASSET_DISPLAY_DATE = "displayDate";

    /** PAGE PROPERTIES CONSTANTS. */

    /** The Constant PP_SORTING_DATE. */
    public static final String PP_SORTING_DATE = "sortingdate";

    /** The Constant PP_DESCRIPTION. */
    public static final String PP_DESCRIPTION = "jcr:description";
    
    /** The Constant PP_TITLE. */
    public static final String PP_TITLE = "jcr:title";

    /** The Constant ISSUE_CONFIGURATION. */
    public static final String ISSUE_CONFIGURATION = "issue-configuration";

    /** The Constant JCR_ISSUE_CONFIGURATION. */
    public static final String JCR_ISSUE_CONFIGURATION = "/jcr:content/issue-configuration";

    /** The Constant SKILLS_SHEET. */
    public static final String SKILLS_SHEET = "skillsSheet";

    /** The Constant VIDEO. */
    public static final String VIDEO = "video";

    /** The Constant GAME. */
    public static final String GAME = "game";
    
    /** The Constant NO IMAGE. */
    public static final String NO_IMAGE = "noimage";

    /** The Constant SLIDESHOW. */
    public static final String SLIDESHOW = "slideshow";

    /** The Constant LESSON_PLAN. */
    public static final String LESSON_PLAN = "lessonPlan";
    
    /** The Constant EXPERIMENT. */
    public static final String EXPERIMENT = "experiment";

    /** The Constant DOWNLOAD. */
    public static final String DOWNLOAD = "download";

    /** The Constant POLL. */
    public static final String POLL = "poll";

    /** The Constant BLOG. */
    public static final String BLOG = "blog";

    /** The Constant ARTICLE. */
    public static final String ARTICLE = "article";

    /** The Constant ASSET_METADATA_NODE. */
    public static final String ASSET_METADATA_NODE = "jcr:content/metadata";

    /** The Constant READ_SERVICE. */
    public static final String READ_SERVICE = "readservice";

    /** The Constant WRITE_SERVICE. */
    public static final String WRITE_SERVICE = "writeservice";

    /** The Constant ASSET. */
    public static final String ASSET = "asset";

    /** The Constant PAGE. */
    public static final String PAGE = "page";
    
    /** The Constant Component. */
    public static final String COMPONENT = "component";

    /** The Constant FILE_REFERENCE. */
    public static final String FILE_REFERENCE = "fileReference";

    /** The Constant IMAGE_TILE_RENDITION. */
    public static final String IMAGE_TILE_RENDITION = ".transform/content-tile/image.png";

    /** The Constant ISSUE_PAGE_TEMPLATE_PATH. */
    public static final String ISSUE_PAGE_TEMPLATE_PATH = "/apps/scholastic/classroom-magazines-migration/templates/issue-page";
    
    /** The Constant HOME_PAGE_TEMPLATE_PATH. */
    public static final String HOME_PAGE_TEMPLATE_PATH = "/apps/scholastic/classroom-magazines-migration/templates/home-page";

    /** The Constant HYPHEN. */
    public static final String HYPHEN = " - ";

    /** The Constant TYPE_SKILL_SHEET. */
    public static final String TYPE_SKILL_SHEET = "Skills Sheet";

    /** The Constant TYPE_VIDEO. */
    public static final String TYPE_VIDEO = "Video";

    /** The Constant TYPE_GAME. */
    public static final String TYPE_GAME = "Game";

    /** The Constant TYPE_SLIDESHOW. */
    public static final String TYPE_SLIDESHOW = "Slideshow";

    /** The Constant TYPE_LESSON_PLAN. */
    public static final String TYPE_LESSON_PLAN = "Lesson Plan";

    /** The Constant TYPE_DOWNLOAD. */
    public static final String TYPE_DOWNLOAD = "Download";

    /** The Constant TYPE_POLL. */
    public static final String TYPE_POLL = "Poll";

    /** The Constant TYPE_BLOG. */
    public static final String TYPE_BLOG = "Blog";

    /** The Constant DATASOURCE_TEXT. */
    public static final String DATASOURCE_TEXT = "text";

    /** The Constant DATASOURCE_VALUE. */
    public static final String DATASOURCE_VALUE = "value";

    /** The Constant DATASOURCE_NO_CONFIGURATION. */
    public static final String DATASOURCE_NO_CONFIGURATION = "NO_CONFIGURATION";

    /** The Constant ARTICLE_RESOURCE_CUSTOM_MESSAGE. */
    public static final String ARTICLE_RESOURCE_CUSTOM_MESSAGE = "No Issues Found - Please select the Custom Resources";

    /** The Constant ARTICLE_RESOURCES_PATHS. */
    public static final String[] ARTICLE_RESOURCES_PATHS = { "article-configuration/resources",
            "article-configuration/custom-resources" };

    /** The Constant ISSUE_RESOURCES_PATHS. */
    public static final String[] ISSUE_RESOURCES_PATHS = { "issue-configuration/resources",
            "issue-configuration/featuredResources" };

    /** The Constant PN_RESOURCE_TYPE. */
    public static final String PN_RESOURCE_TYPE = "resourceType";

    /** The Constant PN_RESOURCE_PATH. */
    public static final String PN_RESOURCE_PATH = "resourcePath";

    /** The Constant ISSUE_PAGE_RESOURCE_TYPE. */
    public static final String ISSUE_PAGE_RESOURCE_TYPE = "scholastic/classroom-magazines-migration/components/page/issue-page";

    /** The Constant ARTICLE_PAGE_RESOURCE_TYPE. */
    public static final String ARTICLE_PAGE_RESOURCE_TYPE = "scholastic/classroom-magazines-migration/components/page/article-page";

    /** The Constant ARTICLE_TEXT_RESOURCE_TYPE. */
    public static final String ARTICLE_TEXT_RESOURCE_TYPE = "scholastic/classroom-magazines-migration/components/content/article-text";
    
    public static final String ARTICLE_SIDEBAR_RESOURCE_TYPE = "scholastic/classroom-magazines-migration/components/content/article-sidebar";

    /** The Constant ARTICLE_TITLE_RESOURCE_TYPE. */
    public static final String ARTICLE_TITLE_RESOURCE_TYPE = "scholastic/classroom-magazines-migration/components/content/article-title";

    /** The Constant BASE_PAGE_RESOURCE_TYPE. */
    public static final String BASE_PAGE_RESOURCE_TYPE = "scholastic/classroom-magazines-migration/components/page/base-page";
    
    /** The Constant TYPE_ARTICLE. */
    public static final String TYPE_ARTICLE = "Article";

    /** The Constant SUCCESS. */
    public static final String SUCCESS = "success";

    /** The Constant SUCCESS. */
    public static final String ERRORS = "errors";

    /** The Constant SLIDESHOW_META_DATA_NODE. */
    public static final String SLIDESHOW_META_DATA_NODE = "slideshowMetadata";

    /** The Constant DEFAULT_SORTING_DATE_FORMAT. */
    public static final String DEFAULT_SORTING_DATE_FORMAT = "MMMM d, yyyy";

    /** The Constant NO_DATA_AVAILABLE. */
    public static final String NO_DATA_AVAILABLE = "NO DATA AVAILABLE";

    /** The Constant ROLE. */
    public static final String ROLE = "role";

    /** The Constant HOMEPAGE. */
    public static final String HOMEPAGE = "Home";

    /** The Constant HOMEPAGE_LOGGEDIN. */
    public static final String HOMEPAGE_LOGGEDIN = "hmPageLoggedIn";

    /** The Constant HOMEPAGE_LOGGEDOUT. */
    public static final String HOMEPAGE_LOGGEDOUT = "hmPageLoggedOut";

    /** The Constant SDM_TCH_LOGIN. */
    public static final String SDM_CREATE_AC = "sdmCreateAccountUrl";

    /** The Constant SDM_TCH_LOGIN. */
    public static final String SDM_TCH_LOGIN = "sdmTeacherLoginUrl";

    /** The Constant HOMEPAGE_LOGGEDOUT. */
    public static final String SDM_STU_LOGIN = "sdmStudentLoginUrl";

    /** The Constant HOMEPAGE_LOGGEDOUT. */
    public static final String SDM_LOGOUT = "sdmLogOutUrl";

    /** The constant SEARCH RESULTS. */
    public static final String SEARCH_RESULTS = "searchResults";

    /** The Constant CLASSROOM_MAGAZINES_PATH. */
    public static final String CLASSROOM_MAGAZINES_PATH = "/content/classroom_magazines";

    /** The Constant ISSUES. */
    public static final String ISSUES = "issues";

    /** The Constant ISSUE. */
    public static final String ISSUE = "issue";

    /** The Constant SLIDESHOW_IMAGE_PATH. */
    public static final String SLIDESHOW_IMAGE_PATH = "slideshowImagePath";
    
    /** The Constant SLIDESHOW_AUDIO_PATH. */
    public static final String SLIDESHOW_AUDIO_PATH = "slideshowAudioPath";

    /** The Constant SLIDESHOW_TITLE. */
    public static final String SLIDESHOW_TITLE = "slideshowTitle";

    /** The Constant SLIDESHOW_CAPTION. */
    public static final String SLIDESHOW_CAPTION = "slideshowCaption";
    
    /** The Constant SLIDESHOW_CREDITS. */
    public static final String SLIDESHOW_CREDITS = "slideshowCredits";

    /** The Constant FEATURED_ARTICLE_FLAG. */
    public static final String FEATURED_ARTICLE_FLAG = "featuredArticleFlag";

    /** The Constant HERO_IMAGE_NODE. */
    public static final String HERO_IMAGE_NODE = "hero-image";

    /** The Constant HERO_IMAGE_FILE_REFERENCE. */
    public static final String HERO_IMAGE_FILE_REFERENCE = "heroImageFileReference";

    /** The Constant MAGAZINES_DATA. */
    public static final String MAGAZINES_DATA = "magazinesData";

    /** The Constant MAGAZINE_NAME. */
    public static final String MAGAZINE_NAME = "magazineName";
    
    /** The Constant IMAGE_PATH. */
    public static final String IMAGE_PATH = "imagePath";

    /** The Constant MAGAZINE_PATH. */
    public static final String MAGAZINE_PATH = "magazinePath";

    /** The Constant MAGAZINE_DESCRIPTION. */
    public static final String MAGAZINE_DESCRIPTION = "magazineDescription";

    /** The Constant SUBJECT_TAG_DELIMITER_COUNT. */
    public static final int SUBJECT_TAG_DELIMITER_COUNT = 5;

    /** The Constant JCR_SUBSCRIPTION_BENEFITS_PATH. */
    public static final String JCR_SUBSCRIPTION_BENEFITS_PATH = "/jcr:content/par/subscription_benefit/subscriptionBenefits";

    /** The Constant SW_AUTH_COOKIE. */
    public static final String SW_AUTH_COOKIE = "sw_auth_cookie";

    /** The Constant CLASS_MAGS_TAG_NAMESPACE. */
    public static final String CLASS_MAGS_TAG_NAMESPACE = "classroom-magazines";

    /** The Constant CLASS_MAGS_CONTENT_TYPE_TAG_ID. */
    public static final String CLASS_MAGS_CONTENT_TYPE_TAG_ID = "classroom-magazines:global/content-types";

    /** The Constant CLASS_MAGS_TAG_ROOT_PATH. */
    public static final String CLASS_MAGS_TAG_ROOT_PATH = "/etc/tags/classroom-magazines/";

    /** The Constant CLASS_MAGS_TAG_ROOT_PATH. */
    public static final String CLASS_MAGS_CONTENT_TAG_ROOT_PATH = "/etc/tags/classroom-magazines/global/content-types";

    /** The Constant NODE_NAME_CONTENT_TYPES. */
    public static final String NODE_NAME_CONTENT_TYPES = "content-types";

    /** The Constant CONTENT_DAM_ROOT. */
    public static final String CONTENT_DAM_ROOT = "/content/dam";

    /** The Constant SERVICE_ENABLED. */
    public static final String SERVICE_ENABLED = "prop.enabled";

    /** The Constant ASSET_USER_TYPE. */
    public static final String ASSET_USER_TYPE = "userType";

    /** The Constant USER_TYPE_EVERYONE. */
    public static final String USER_TYPE_EVERYONE = "everyone";

    /** The Constant USER_TYPE_TEACHER. */
    public static final String USER_TYPE_TEACHER = "teacher";
    
    /** The Constant USER_TYPE_STUDENT. */
    public static final String USER_TYPE_STUDENT = "student";

    /** The Constant USER_TYPE_ANONYMOUS. */
    public static final String USER_TYPE_ANONYMOUS = "anonymous";

    /** The Constant FLASH_GAME_CONTAINER_PROPERTY. */
    public static final String FLASH_GAME_CONTAINER_PROPERTY = "containerPath";

    /** The Constant RESOURCES. */
    public static final String RESOURCES = "resources";

    /** The Constant FEATURED_RESOURCES. */
    public static final String FEATURED_RESOURCES = "featuredResources";

    /** The Constant CUSTOM_RESOURCES. */
    public static final String CUSTOM_RESOURCES = "custom-resources";

    /** The Constant CONTENT_TYPE_CUSTOM. */
    public static final String CONTENT_TYPE_CUSTOM_PAGE = "customPage";
    
    /** The Constant CONTENT_TYPE_CUSTOM_ASSET. */
    public static final String CONTENT_TYPE_CUSTOM_ASSET = "customAsset";
    
    /** The Constant VIDEO_DURATION. */
    public static final String VIDEO_DURATION = "videoDuration";
    
    /** The Constant VIDEO_ID.*/
    public static final String VIDEO_ID = "videoId";
    
    /** The Constant DEFAULT_DATE_FORMAT. */
    public static final String DEFAULT_DATE_FORMAT = "MMMM yyyy";
    
    /** The Constant HTML_SUFFIX. */
    public static final String HTML_SUFFIX = ".html";

    /** The Constant EREADER_CONFIG. */
    public static final String EREADER_CONFIG = "ereader-configuration";
    
    /**The Constant GAMES. */
    public static final String GAMES = "Games";
    
    /**The Constant GAME_TYPE. */
    public static final String GAME_TYPE = "gameType";
    
    /**The Constant GAME_HTML. */
    public static final String GAME_HTML = "html5";
    
    /**The Constant GAME_FLASH. */
    public static final String GAME_FLASH = "flash";
    
    /**The Constant XML_PATH. */
    public static final String XML_PATH = "xmlPath";
    
    /** The Constant MY_BOOKMARKS_PAGE_URL. */
    public static final String MY_BOOKMARKS_PAGE_URL = "myBookmarksPageUrl";

    /** The Constant QUESTION_ID. */
    public static final String QUESTION_ID = "questionId";
    
    /** The Constant QUESTION. */
    public static final String QUESTION = "question";
    
    /** The Constant QUESTION_LIST. */
    public static final String QUESTION_LIST = "question-list";
    
    /** The Constant COLON. */
    public static final String COLON = ":";
    
    /** The Constant DOT. */
    public static final String DOT = ".";
    
    /** The Constant EMPTY_JSON_ARRAY. */
    public static final String EMPTY_JSON_ARRAY="[]";
    
    /** The Constant NN_TOPIC. */
    public static final String NN_TOPIC = "topic";

    /** The Constant JSON_KEY_TOPIC_TITLE. */
	public static final String JSON_KEY_TOPIC_TITLE = "topicTitle";

	/** The Constant ARTICLE_PAGE_TEMPLATE_PATH. */
    public static final String ARTICLE_PAGE_TEMPLATE_PATH = "/apps/scholastic/classroom-magazines-migration/templates/article-page";
    
    /** The Constant ARCHIVE_PAGE_TEMPLATE_PATH. */
    public static final String ARCHIVE_PAGE_TEMPLATE_PATH = "/apps/scholastic/classroom-magazines-migration/templates/archive-page";
    
    /** The Constant FORWARD_SLASH.*/
    public static final String FORWARD_SLASH = "/";
    
    /** The Constant DELIMITER. */
    public static final String DELIMITER = "************************";
    
    /** The Constant URL. */
    public static final String URL = "url";

    /** The Constant BLOG_POSTS_PATH_NODE. */
    public static final String BLOG_POSTS_PATH_NODE = "blogPostsPath";
    
    /** The Constant BLOG_PATH_PROPERTY. */
    public static final String BLOG_PATH_PROPERTY = "blogPath";
    
    /** The Constant BLOG_CATEGORIES_NODE. */
    public static final String BLOG_CATEGORIES_NODE = "blogCategoriesData";
    
    /** The Constant BLOG_CATEGORY_TITLE. */
    public static final String BLOG_CATEGORY_TITLE = "blogCategoryTitle";
    
    /** The Constant BLOG_CATEGORY_LINK. */
    public static final String BLOG_CATEGORY_LINK = "blogCategoryLink"; 
    
    /** The Constant TAG_ID_DELIMITER_COUNT. */
    public static final int TAG_ID_DELIMITER_COUNT = 2;

    /** The Constant ERROR_PAGE_TEMPLATE_PATH. */
    public static final String ERROR_PAGE_TEMPLATE_PATH = "/apps/scholastic/classroom-magazines-migration/templates/error-page";
    
    
   public static final String SUB_TOTAL_PRICE = "subTotalPrice";
	
	public static final String TOTAL_PRICE = "totalPrice";
	
	public static final String TOTAL_SHIPPING_PRICE = "totalShippingPrice";
	
	public static final String BILLING_OPTION = "billingOption";
	
	public static final String MAGAZINE_TITLE = "magazineTitle";
	
	public static final String QUANTITY = "quantity";
	
	public static final String MAGAZINE_PRICE = "magazinePrice";
	
	public static final String SHIPPING_PERCENTAGE = "shippingPercentage";
	
	public static final String SUB_TOTAL = "subTotal";
	
	public static final String SHIPPING_PRICE = "shippingPrice";
	
	public static final String NEW_SUBSCRIPTION = "newSubscription";
	
	public static final String AUTO_RENEWAL = "isAutoRenewal";
	
	public static final String NAME = "name";
	
	public static final String EMAIL = "email";
	
	public static final String ADDRESS_LINE_1 = "address1";
	
	public static final String ADDRESS_LINE_2 = "address2";
	
	public static final String CITY = "city";
	
	public static final String STATE = "state";
	
	public static final String ZIP = "zip";
	
	public static final String SCHOOL = "school";
	
	public static final String PHONE = "phone";
	
	public static final String EMAIL_SENT = "emailSent";

    
    /**
     * Instantiates a new class mags migration constants.
     */
    private ClassMagsMigrationConstants() {

    }

}
