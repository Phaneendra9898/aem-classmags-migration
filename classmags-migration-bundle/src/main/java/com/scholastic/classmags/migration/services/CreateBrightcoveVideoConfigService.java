package com.scholastic.classmags.migration.services;

/**
 * The Interface CreateBrightcoveVideoConfigService.
 */
public interface CreateBrightcoveVideoConfigService {

    /**
     * Gets the read url.
     * 
     * @return the read url
     */
    String getReadUrl();

    /**
     * Gets the read token.
     * 
     * @return the read token
     */
    String getReadToken();

    /**
     * Gets the upfront any fields.
     * 
     * @return the upfront any fields
     */
    String getUpfrontAnyFields();

    /**
     * Gets the callback.
     * 
     * @return the callback
     */
    String getCallback();

    /**
     * Gets the gets the item count.
     * 
     * @return the gets the item count
     */
    String getGetItemCount();

    /**
     * Gets the sort name.
     * 
     * @return the sort name
     */
    String getSortName();

    /**
     * Gets the sort start date.
     * 
     * @return the sort start date
     */
    String getSortStartDate();

    /**
     * Gets the media delivery.
     * 
     * @return the media delivery
     */
    String getMediaDelivery();

    /**
     * Gets the fields.
     * 
     * @return the fields
     */
    String getFields();

    /**
     * Gets the find by id.
     * 
     * @return the find by id
     */
    String getFindById();

    /**
     * Gets the search videos.
     * 
     * @return the search videos
     */
    String getSearchVideos();

}