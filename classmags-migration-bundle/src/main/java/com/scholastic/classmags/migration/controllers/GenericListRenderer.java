package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.adobe.acs.commons.genericlists.GenericList;
import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;

public class GenericListRenderer extends WCMUsePojo {

    @Override
    public void activate() throws Exception {
        getList();
    }

    private List< GenericList.Item > values = new ArrayList< GenericList.Item >();

    public HashMap< String, String > getList() {
        HashMap< String, String > listValues = new LinkedHashMap< String, String >();
        String listName = get( "listpath", String.class );
        if ( StringUtils.isNotBlank( listName ) ) {
            Page listPage = getPageManager().getPage( listName );
            if ( listPage != null ) {
                GenericList genericList = listPage.adaptTo( GenericList.class );
                setValues( genericList.getItems() );
            }
        }
        return listValues;
    }

    public List< GenericList.Item > getValues() {
        return values;
    }

    public void setValues( List< GenericList.Item > values ) {
        this.values = values;
    }
}