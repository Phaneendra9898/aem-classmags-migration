package com.scholastic.classmags.migration.services;

import java.util.Map;

import org.apache.sling.api.SlingHttpServletRequest;

import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;

/**
 * The Interface LexileLevelsDataService.
 */
public interface LexileLevelsDataService {

    /**
     * Fetch lexile levels.
     *
     * @param slingRequest
     *            the sling request
     * @param currentPage
     *            the current page
     * @return the map
     * @throws ClassmagsMigrationBaseException 
     */
    public Map< String, String > fetchLexileLevels( SlingHttpServletRequest slingRequest, Page currentPage );
}
