package com.scholastic.classmags.migration.controllers;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.scripting.SlingScriptHelper;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * Use Class for global access level.
 */
public class GlobalAccessUse extends WCMUsePojo {

    private boolean visible;
    private boolean shareable;
    private String loginRole;
    private String magazineName;
    
    public void activate() {
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        Page currentPage = getCurrentPage();
        magazineName = StringUtils.EMPTY;
        if ( slingScriptHelper != null && currentPage != null ) {
            MagazineProps magazineProps = slingScriptHelper.getService(MagazineProps.class);
            magazineName = magazineProps.getMagazineName(currentPage.getPath());
        }
        loginRole = ( String ) RoleUtil.getUserRole( getRequest() );
        String accessibleTo = getProperties().get( "userType", "everyone" );
        visible = RoleUtil.shouldRender( loginRole, accessibleTo );
        shareable = RoleUtil.shouldRender(loginRole, "teacher");
    }

    /**
     * Gets the user access level first image.
     *
     * @return the user access level first image
     */
    public Boolean isVisible() {
        return visible;
    }
    
    /**
     * Gets the user access to share.
     *
     * @return the user user access to share
     */
    public Boolean isShareableToTeacher() {
        return shareable;
    }
    

    /**
     * Gets the login role.
     *
     * @return the login role.
     */
    public String getLoginRole(){
        return loginRole;
    }
    
    /**
     * Gets the magazine name.
     *
     * @return the magazine name.
     */
    public String getMagazineName(){
        return magazineName;
    }
}
