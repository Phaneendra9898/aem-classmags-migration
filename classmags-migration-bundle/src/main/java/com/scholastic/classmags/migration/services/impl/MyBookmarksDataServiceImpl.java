package com.scholastic.classmags.migration.services.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.tagging.TagManager;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.BookmarkData;
import com.scholastic.classmags.migration.models.ContentTileFlags;
import com.scholastic.classmags.migration.models.GameModel;
import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.services.MyBookmarksDataService;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * The Class BookmarksDataServiceImpl.
 */
@Component( immediate = true, metatype = false )
@Service( MyBookmarksDataService.class )
@Properties( {
        @Property( name = Constants.SERVICE_DESCRIPTION, value = "This service returns bookmarks and their data" ) })
public class MyBookmarksDataServiceImpl implements MyBookmarksDataService {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( MyBookmarksDataServiceImpl.class );

    /** The resource resolver factory. */
    @Reference
    ResourceResolverFactory resourceResolverFactory;

    /** The property config service. */
    @Reference
    PropertyConfigService propertyConfigService;

    /** The resource path config service. */
    @Reference
    ResourcePathConfigService resourcePathConfigService;

    /** The externalizer. */
    @Reference
    Externalizer externalizer;

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.services.MyBookmarksDataService#
     * getMyBookmarksPageData(java.util.List, java.lang.String)
     */
    @Override
    public List< ResourcesObject > getMyBookmarksPageData( List< BookmarkData > allPageBookmarks,
            String loggedInUserType ) throws ClassmagsMigrationBaseException {

        LOG.debug( "INSIDE MY BOOKMARK PAGE DATA" );

        ResourceResolver resourceResolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );

        List< ResourcesObject > allPagesData = new ArrayList< >();
        for ( BookmarkData bookmarkData : allPageBookmarks ) {

            bookmarkData.setBookmarkPath(
                    StringUtils.removeEnd( bookmarkData.getBookmarkPath(), ClassMagsMigrationConstants.HTML_SUFFIX ) );
            LOG.debug( "PAGE BOOKMARK PATH :" + bookmarkData.getBookmarkPath() );

            Resource pageResource = resourceResolver.resolve( bookmarkData.getBookmarkPath() );
            LOG.debug( "PAGE RESOURCE :" + pageResource );

            Resource jcrContentResource = pageResource.getChild( JcrConstants.JCR_CONTENT );
            LOG.debug( "JCR CONTENT RESOURCE :" + pageResource );

            ValueMap properties = jcrContentResource != null ? jcrContentResource.getValueMap() : null;

            ResourcesObject pageData = createPageData( properties, jcrContentResource, bookmarkData, loggedInUserType,
                    resourceResolver );
            LOG.debug( "PAGE DATA CREATED :" + pageData.toString() );

            if ( StringUtils.isNotBlank( pageData.getPagePath() ) ) {
                allPagesData.add( pageData );
            }
        }
        resourceResolver.close();
        return allPagesData;
    }

    /**
     * Creates the page data.
     *
     * @param properties
     *            the properties
     * @param jcrContentResource
     *            the jcr content resource
     * @param bookmarkData
     *            the bookmark data
     * @param loggedInUserType
     *            the logged in user type
     * @param resourceResolver
     *            the resource resolver
     * @return the resources object
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private ResourcesObject createPageData( ValueMap properties, Resource jcrContentResource, BookmarkData bookmarkData,
            String loggedInUserType, ResourceResolver resourceResolver ) throws ClassmagsMigrationBaseException {

        String pageBookmark = bookmarkData.getBookmarkPath();
        ResourcesObject pageData = new ResourcesObject();
        if ( null != properties ) {
            pageData.setImagePath( properties.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) );
            pageData.setDescription( properties.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) );

            TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );
            List< String > tags = CommonUtils.getSubjectTags( jcrContentResource, tagManager );
            if ( !tags.isEmpty() ) {
                pageData.setTags( tags );
            }
            pageData.setContentType( CommonUtils.getContentTypeTag( jcrContentResource, tagManager ) );
            pageData.setTitle( properties.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) );
            pageData.setMagazineType( bookmarkData.getMagazineType() );

            String dataPagePath = CommonUtils.getDataPath( resourcePathConfigService, bookmarkData.getMagazineType() );
            String dateFormat = CommonUtils.getDateFormat( propertyConfigService, dataPagePath );

            pageData.setSortingDate( CommonUtils.getSortingDate( properties, dateFormat ) );
            pageData.setDisplayDate( CommonUtils.getDisplayDate( properties, dateFormat ) );
            pageData.setUserRole( properties.get( ClassMagsMigrationConstants.ASSET_USER_TYPE, StringUtils.EMPTY ) );

            ContentTileFlags contentTileFlags = new ContentTileFlags();
            contentTileFlags.setDownloadEnabled( false );
            contentTileFlags.setViewArticleEnabled( StringUtils.isNotBlank( bookmarkData.getViewArticleLink() ) );
            contentTileFlags.setShareEnabled( false );
            if ( StringUtils.equalsIgnoreCase( ClassMagsMigrationConstants.USER_TYPE_TEACHER, loggedInUserType ) ) {
                contentTileFlags.setShareEnabled( true );
            }
            pageData.setContentTileFlags( contentTileFlags );
            pageData.setPagePath( InternalURLFormatter.formatURL( resourceResolver, pageBookmark ) );
            pageData.setArticlePagePath(
                    InternalURLFormatter.formatURL( resourceResolver, bookmarkData.getViewArticleLink() ) );
            pageData.setBookmarkDateSaved( bookmarkData.getDateSaved() );
            pageData.setShareLinkUrl( externalizer.externalLink( resourceResolver, bookmarkData.getMagazineType(),
                    InternalURLFormatter.formatURL( resourceResolver, pageBookmark ) ) );
            pageData.setBookmarkPath( pageBookmark );
        }
        return pageData;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.services.MyBookmarksDataService#
     * getMyBookmarksAssetData(java.util.List, java.lang.String)
     */
    @Override
    public List< ResourcesObject > getMyBookmarksAssetData( List< BookmarkData > allAssetBookmarks,
            String loggedInUserType ) throws ClassmagsMigrationBaseException {

        LOG.debug( "INSIDE MY BOOKMARK ASSET DATA" );

        ResourceResolver resourceResolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );

        List< ResourcesObject > allAssetsData = new ArrayList< >();
        for ( BookmarkData bookmarkData : allAssetBookmarks ) {

            LOG.debug( "ASSET BOOKMARK PATH :" + bookmarkData.getBookmarkPath() );

            Resource assetResource = resourceResolver.resolve( bookmarkData.getBookmarkPath() );
            LOG.debug( "ASSET RESOURCE :" + assetResource );

            Asset asset = assetResource.adaptTo( Asset.class );
            if ( asset != null ) {
                ResourcesObject assetData = createAssetData( assetResource, asset, bookmarkData, loggedInUserType,
                        resourceResolver );
                LOG.debug( "ASSET DATA CREATED :" + assetData.toString() );

                if ( StringUtils.isNotBlank( assetData.getPagePath() ) ) {
                    allAssetsData.add( assetData );
                }
            }
        }
        resourceResolver.close();
        return allAssetsData;
    }

    /**
     * Creates the asset data.
     *
     * @param assetResource
     *            the asset resource
     * @param asset
     *            the asset
     * @param bookmarkData
     *            the bookmark data
     * @param loggedInUserType
     *            the logged in user type
     * @param resourceResolver
     *            the resource resolver
     * @return the resources object
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private ResourcesObject createAssetData( Resource assetResource, Asset asset, BookmarkData bookmarkData,
            String loggedInUserType, ResourceResolver resourceResolver ) throws ClassmagsMigrationBaseException {

        String assetBookmark = bookmarkData.getBookmarkPath();
        String contentType;
        ResourcesObject assetData = new ResourcesObject();
        Resource metadataNode = assetResource != null
                ? assetResource.getChild( ClassMagsMigrationConstants.ASSET_METADATA_NODE ) : null;

        assetData.setImagePath( asset.getPath() + ClassMagsMigrationConstants.IMAGE_TILE_RENDITION );
        assetData.setTitle(
                StringUtils.defaultIfBlank( asset.getMetadataValue( DamConstants.DC_TITLE ), asset.getName() ) );
        assetData.setDescription( asset.getMetadataValue( DamConstants.DC_DESCRIPTION ) );
        assetData.setMagazineType( bookmarkData.getMagazineType() );
        assetData.setUserRole( asset.getMetadataValue( ClassMagsMigrationConstants.ASSET_USER_TYPE ) );

        String dataPagePath = CommonUtils.getDataPath( resourcePathConfigService, bookmarkData.getMagazineType() );
        String dateFormat = CommonUtils.getDateFormat( propertyConfigService, dataPagePath );
        assetData.setSortingDate( CommonUtils.getAssetSortingDate( metadataNode, dateFormat ) );
        assetData.setDisplayDate( CommonUtils.getAssetDisplayDate( metadataNode, dateFormat ) );
        
        TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );
        List< String > tags = CommonUtils.getSubjectTags( metadataNode, tagManager );
        if ( !tags.isEmpty() ) {
            assetData.setTags( tags );
        }
        contentType = CommonUtils.getContentTypeTag( metadataNode, tagManager );
        assetData.setContentType( contentType );

        if ( metadataNode != null
                && metadataNode.getValueMap().containsKey( ClassMagsMigrationConstants.VIDEO_DURATION ) ) {
            assetData.setVideoDuration(
                    metadataNode.getValueMap().get( ClassMagsMigrationConstants.VIDEO_DURATION, StringUtils.EMPTY ) );
        }
        if ( metadataNode != null && metadataNode.getValueMap().containsKey( ClassMagsMigrationConstants.VIDEO_ID ) ) {
            assetData.setVideoId( metadataNode.getValueMap().get( ClassMagsMigrationConstants.VIDEO_ID, Long.class ) );
        }

        ContentTileFlags contentTileFlags = new ContentTileFlags();
        contentTileFlags.setDownloadEnabled( CommonUtils.isAShareableAsset( assetBookmark ) );

        contentTileFlags.setViewArticleEnabled( StringUtils.isNotBlank( bookmarkData.getViewArticleLink() ) );
        contentTileFlags.setShareEnabled( false );
        if ( StringUtils.equalsIgnoreCase( ClassMagsMigrationConstants.USER_TYPE_TEACHER, loggedInUserType ) ) {
            contentTileFlags.setShareEnabled( CommonUtils.isAShareableAsset( assetBookmark ) );
        }
        assetData.setContentTileFlags( contentTileFlags );
        assetData.setPagePath( InternalURLFormatter.formatURL( resourceResolver, assetBookmark ) );
        assetData.setArticlePagePath(
                InternalURLFormatter.formatURL( resourceResolver, bookmarkData.getViewArticleLink() ) );
        assetData.setBookmarkPath( assetBookmark );

        if ( contentType != null && contentType.equalsIgnoreCase( ClassMagsMigrationConstants.GAMES ) ) {
            GameModel gameModel = addGameModelData( asset, bookmarkData.getMagazineType(), metadataNode );
            assetData.setGameModel( gameModel );
        }
        assetData.setBookmarkDateSaved( bookmarkData.getDateSaved() );
        assetData.setShareLinkUrl( externalizer.externalLink( resourceResolver, bookmarkData.getMagazineType(),
                InternalURLFormatter.formatURL( resourceResolver, assetBookmark ) ) );

        return assetData;
    }

    /**
     * Adds the game model data.
     *
     * @param asset
     *            the asset
     * @param magazineType
     *            the magazine type
     * @param metadataNode
     *            the metadata node
     * @return the game model
     */
    private GameModel addGameModelData( Asset asset, String magazineType, Resource metadataNode ) {

        GameModel gameModel = new GameModel();
        gameModel.setContainerId( generateId() );

        if ( metadataNode != null && StringUtils.equals(
                metadataNode.getValueMap().get( ClassMagsMigrationConstants.GAME_TYPE, StringUtils.EMPTY ),
                StringUtils.EMPTY ) ) {
            gameModel.setGamePath( asset.getPath().concat( ClassMagsMigrationConstants.HTML_SUFFIX ) );
            gameModel.setGameType( ClassMagsMigrationConstants.GAME_HTML );
        } else {
            String flashvarsValue;

            if ( null != resourcePathConfigService && null != propertyConfigService ) {
                try {
                    gameModel.setContainerPath( propertyConfigService.getDataConfigProperty(
                            ClassMagsMigrationConstants.FLASH_GAME_CONTAINER_PROPERTY,
                            CommonUtils.getDataPath( resourcePathConfigService, magazineType ) ) );
                } catch ( LoginException e ) {
                    LOG.error( "Error in setting flash game container property in getGameContent::::" + e );
                }

                if ( metadataNode != null ) {

                    gameModel.setFlashGameType( metadataNode.getValueMap().get( ClassMagsMigrationConstants.GAME_TYPE,
                            StringUtils.EMPTY ) );
                    gameModel.setXMLPath(
                            metadataNode.getValueMap().get( ClassMagsMigrationConstants.XML_PATH, StringUtils.EMPTY ) );
                }

                if ( gameModel.getXMLPath() != null && StringUtils.isNotBlank( gameModel.getXMLPath())) {
                    flashvarsValue = "gametype=" + gameModel.getFlashGameType() + "&contentXML="
                            + gameModel.getXMLPath() + "&assetSWF=" + asset.getPath();
                } else {
                    flashvarsValue = "gametype=" + gameModel.getFlashGameType() + "&assetSWF=" + asset.getPath();
                }
                gameModel.setGamePath( flashvarsValue );
                gameModel.setGameType( ClassMagsMigrationConstants.GAME_FLASH );
            }
        }
        return gameModel;
    }

    /**
     * Generate id.
     *
     * @return the string
     */
    private String generateId() {
        UUID uniqueKey = UUID.randomUUID();
        String[] idUnique = uniqueKey.toString().split( "-" );
        return idUnique[ 0 ];
    }
}
