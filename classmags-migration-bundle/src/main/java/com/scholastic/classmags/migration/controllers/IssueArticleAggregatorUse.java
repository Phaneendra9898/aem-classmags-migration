package com.scholastic.classmags.migration.controllers;

import java.util.List;

import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.models.ArticleAggregatorObject;
import com.scholastic.classmags.migration.services.ArticleAggregatorService;

/**
 * The Class IssueArticleAggregatorUse.
 */
public class IssueArticleAggregatorUse extends WCMUsePojo {

    /** The article aggregator. */
    private List< ArticleAggregatorObject > articleAggregator;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( IssueArticleAggregatorUse.class );

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() throws Exception {
        LOG.info( "::::Inside activate method of IssueArticleAggregatorUse::::" );
        Page currentPage = getCurrentPage();
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();

        if ( slingScriptHelper != null && currentPage != null ) {
            ArticleAggregatorService articleAggregatorService = slingScriptHelper
                    .getService( ArticleAggregatorService.class );
            if ( articleAggregatorService != null ) {
                setArticleAggregator( articleAggregatorService.getArticleAggregatorData( getRequest(), currentPage ) );
            }
        }
    }

    /**
     * Gets the article aggregator.
     * 
     * @return the articleAggregator
     */
    public List< ArticleAggregatorObject > getArticleAggregator() {
        return articleAggregator;
    }

    /**
     * Sets the article aggregator.
     * 
     * @param articleAggregator
     *            the articleAggregator to set
     */
    public void setArticleAggregator( List< ArticleAggregatorObject > articleAggregator ) {
        this.articleAggregator = articleAggregator;
    }
}
