package com.scholastic.classmags.migration.social.impl;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.xss.XSSAPI;
import org.osgi.framework.Constants;

import com.adobe.cq.social.scf.ClientUtilities;
import com.adobe.cq.social.scf.QueryRequestInfo;
import com.adobe.cq.social.scf.SocialComponent;
import com.adobe.cq.social.scf.SocialComponentFactory;
import com.adobe.cq.social.scf.core.AbstractSocialComponentFactory;
import com.adobe.cq.social.ugc.api.UgcSearch;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.MyBookmarksDataService;
import com.scholastic.classmags.migration.social.api.BookmarkSocialComponent;

/**
 * A factory for creating BookmarkSocialComponent objects.
 */
@SuppressWarnings( "deprecation" )
@Component( immediate = true, metatype = false )
@Service( SocialComponentFactory.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Social Component Factory For Bookmark" ) })
public class BookmarkSocialComponentFactory extends AbstractSocialComponentFactory implements SocialComponentFactory {

    /** The ugc search. */
    @Reference
    private UgcSearch ugcSearch;

    /** The xssapi. */
    @Reference
    private XSSAPI xssapi;

    /** The my bookmarks data service. */
    @Reference
    private MyBookmarksDataService myBookmarksDataService;

    /** The magazine props service. */
    @Reference
    private MagazineProps magazinePropsService;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.adobe.cq.social.scf.SocialComponentFactory#getSocialComponent(org.
     * apache.sling.api.resource.Resource)
     */
    @Override
    public SocialComponent getSocialComponent( Resource resource ) {
        return new BookmarkSocialComponentImpl( resource, this.getClientUtilities( resource.getResourceResolver() ),
                ugcSearch, xssapi, myBookmarksDataService, magazinePropsService );
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.adobe.cq.social.scf.SocialComponentFactory#getSocialComponent(org.
     * apache.sling.api.resource.Resource,
     * org.apache.sling.api.SlingHttpServletRequest)
     */
    @Override
    public SocialComponent getSocialComponent( Resource resource, SlingHttpServletRequest request ) {
        return new BookmarkSocialComponentImpl( resource, this.getClientUtilities( request ), ugcSearch, xssapi,
                myBookmarksDataService, magazinePropsService );
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.adobe.cq.social.scf.SocialComponentFactory#getSocialComponent(org.
     * apache.sling.api.resource.Resource,
     * com.adobe.cq.social.scf.ClientUtilities,
     * com.adobe.cq.social.scf.QueryRequestInfo)
     */
    @Override
    public SocialComponent getSocialComponent( Resource resource, ClientUtilities clientUtils,
            QueryRequestInfo queryRequestInfo ) {
        return new BookmarkSocialComponentImpl( resource, clientUtils, ugcSearch, xssapi, myBookmarksDataService,
                magazinePropsService );
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.adobe.cq.social.scf.SocialComponentFactory#getSupportedResourceType()
     */
    @Override
    public String getSupportedResourceType() {
        return BookmarkSocialComponent.BOOKMARK_RESOURCE_TYPE;
    }
}
