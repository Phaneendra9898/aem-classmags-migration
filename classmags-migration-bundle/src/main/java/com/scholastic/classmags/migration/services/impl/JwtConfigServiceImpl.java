package com.scholastic.classmags.migration.services.impl;

import java.util.Map;

import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.services.JwtConfigService;

/**
 * The Class JwtKeyConfigServiceImpl.
 */
@Component( immediate = true, metatype = true, label = "JWT configurations." )
@Service( JwtConfigService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "JWT Configuration Service" ) })
public class JwtConfigServiceImpl implements JwtConfigService {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( JwtConfigServiceImpl.class );

    private String secretKey;
    
    private String issuer;
    
    @Property( label = "Secret Key", value = "ClassMagazinesPr0d" )
    public static final String SECRET_KEY = "classmags-jwt-secret-key" ;
    
    @Property( label = "Issuer", value = "classmags" )
    public static final String ISSUER = "classmags-jwt-issuer" ;

    @Activate
    protected void activate( final Map< String, Object > properties ) {
        secretKey =  ( String ) properties.get( SECRET_KEY );
        issuer = ( String ) properties.get( ISSUER );
    }
    
    @Override
    public String getSecretKey() {
        return secretKey;
    }
    
    @Override
    public String getIssuer() {
        return issuer;
    }
    
    /**
     * DEACTIVATE.
     */
    protected void deactivate() {
        LOG.debug( "DEACTIVATE METHOD CALLED" );
        this.secretKey = null;
        this.issuer = null;
    }

}
