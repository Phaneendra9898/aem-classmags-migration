package com.scholastic.classmags.migration.social.impl.operations;

import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.servlets.post.PostOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.social.scf.OperationException;
import com.adobe.cq.social.scf.SocialComponentFactoryManager;
import com.adobe.cq.social.scf.SocialOperationResult;
import com.adobe.cq.social.scf.core.operations.AbstractSocialOperation;
import com.scholastic.classmags.migration.social.api.BookmarkService;
import com.scholastic.classmags.migration.social.api.BookmarkSocialComponent;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;

/**
 * A POST Endpoint for CustomDeleteBookmarkOperation that accepts requests for
 * deleting bookmarks data for custom path. This class responds to all POST
 * requests with a :operation=social:bookmark:customDeleteBookmark parameter.
 * For example, curl
 * http://localhost:4502/content/classroom-magazines/testissuepage/_jcr_content/
 * par/bookmark.social.json -uadmin:admin -v POST -H "Accept:application/json"
 * --data ":operation=social:bookmark:customDeleteBookmark&
 * bookmarkPagePath=/content/usergenerated/asi/jcr/content/classroom-magazines/
 * home/users/anonymous/scienceworld &appName=scienceworld""
 */
@Component( immediate = true )
@Service
@Property( name = PostOperation.PROP_OPERATION_NAME, value = ClassMagsMigrationASRPConstants.SOCIAL_CUSTOM_DELETE_BOOKMARK_OPERATION )
public class CustomDeleteBookmarkOperation extends AbstractSocialOperation {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( CustomDeleteBookmarkOperation.class );

    /** The bookmark service. */
    @Reference
    private BookmarkService bookmarkService;

    /** The scf manager. */
    @Reference
    private SocialComponentFactoryManager scfManager;

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.social.scf.core.operations.AbstractSocialOperation#
     * performOperation(org.apache.sling.api.SlingHttpServletRequest)
     */
    @Override
    protected SocialOperationResult performOperation( SlingHttpServletRequest slingHttpServletRequest )
            throws OperationException {

        LOG.info( ClassMagsMigrationASRPConstants.OPERATION_START
                + ClassMagsMigrationASRPConstants.SOCIAL_DELETE_BOOKMARK_OPERATION );

        final Resource bookmarkResource = this.bookmarkService.deleteUserBookmarkResource( slingHttpServletRequest );

        SocialOperationResult deleteSocialOperationResult = SocialASRPUtils.getSocialOperationResult(
                slingHttpServletRequest, HttpServletResponse.SC_OK,
                ClassMagsMigrationASRPConstants.HTTP_SUCCESS_MESSAGE, bookmarkResource, this.scfManager,
                BookmarkSocialComponent.BOOKMARK_RESOURCE_TYPE );

        LOG.info( ClassMagsMigrationASRPConstants.OPERATION_END );

        return deleteSocialOperationResult;
    }
}
