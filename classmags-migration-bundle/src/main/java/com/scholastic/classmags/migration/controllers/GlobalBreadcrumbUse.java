package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.models.BreadCrumbLinks;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * Use Class for Global breadcrumb.
 */
public class GlobalBreadcrumbUse extends WCMUsePojo {

    private static final int HOMEPAGE_DEPTH = 3;

    private Page currentPage;
    private String dateFormat;
    private List< BreadCrumbLinks > breadCrumbLinks;
    private boolean hideBreadcrumb;

    private static final Logger LOG = LoggerFactory.getLogger( GlobalBreadcrumbUse.class );

    @Override
    public void activate() {
        LOG.info( "Inside activate" );
        SlingHttpServletRequest request = getRequest();
        String homePagePath = StringUtils.EMPTY;
        String userRole;
        String magazineName = StringUtils.EMPTY;
        dateFormat = StringUtils.EMPTY;
        breadCrumbLinks = new ArrayList< >();
        this.currentPage = ( Page ) getCurrentPage();

        userRole = ( String ) RoleUtil.getUserRole( request );
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        this.hideBreadcrumb = setHideBreadcrumb( currentPage );
        try {
            if ( null != slingScriptHelper && !hideBreadcrumb ) {

                PropertyConfigService propertyConfigService = slingScriptHelper
                        .getService( PropertyConfigService.class );
                ResourcePathConfigService resourcePathConfigService = slingScriptHelper
                        .getService( ResourcePathConfigService.class );
                MagazineProps magazineProps = slingScriptHelper.getService( MagazineProps.class );
                if ( null != magazineProps ) {
                    magazineName = magazineProps.getMagazineName( currentPage.getPath() );
                }
                if ( propertyConfigService != null && resourcePathConfigService != null ) {
                    dateFormat = propertyConfigService.getDataConfigProperty(
                            ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                            CommonUtils.getDataPath( resourcePathConfigService, magazineName ) );
                    homePagePath = setHomePagePath( propertyConfigService, resourcePathConfigService, userRole,
                            magazineName );
                }

                if ( StringUtils.isNotEmpty( homePagePath ) ) {
                    BreadCrumbLinks homePageBean = new BreadCrumbLinks();
                    homePageBean.setPath( InternalURLFormatter.formatURL( getResourceResolver(), homePagePath ) );
                    homePageBean.setTitle( ClassMagsMigrationConstants.HOMEPAGE );
                    this.breadCrumbLinks.add( homePageBean );
                }
                getBreadCrumb();
            }
        } catch ( LoginException e ) {
            LOG.error( "Login error occured in breadcrumb" + e );
        }
    }

    private String setHomePagePath( PropertyConfigService propertyConfigService,
            ResourcePathConfigService resourcePathConfigService, String userRole, String magazineName )
                    throws LoginException {
        String homePagePath;
        if ( StringUtils.equals( userRole, ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS ) ) {

            homePagePath = propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.HOMEPAGE_LOGGEDOUT,
                    CommonUtils.getDataPath( resourcePathConfigService, magazineName ) );

        } else {
            homePagePath = propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.HOMEPAGE_LOGGEDIN,
                    CommonUtils.getDataPath( resourcePathConfigService, magazineName ) );
        }
        return homePagePath;
    }

    /**
     * Get breadcrumb.
     */
    private void getBreadCrumb() {
        LOG.info( "getBreadCrumb starts **" );
        int currentPageDepth = this.currentPage.getDepth();
        if ( currentPageDepth > HOMEPAGE_DEPTH ) {
            int position = 1;
            int absoluteParentIndex = 2;
            boolean isIssuePage;
            while ( absoluteParentIndex <= currentPageDepth - 1 ) {
                String pageTemplate;
                Page absoluteParent = this.currentPage.getAbsoluteParent( absoluteParentIndex );
                if ( absoluteParent != null ) {
                    ValueMap valueMap = absoluteParent.getProperties();
                    pageTemplate = ( null != valueMap ) ? ( String ) valueMap.get( "cq:template" )
                            : ClassMagsMigrationConstants.NO_DATA_AVAILABLE;
                    isIssuePage = StringUtils.equals( pageTemplate,
                            ClassMagsMigrationConstants.ISSUE_PAGE_TEMPLATE_PATH ) ? true : false;
                    setbreadCrumbData( position, absoluteParent, valueMap, isIssuePage );
                    position++;
                }
                absoluteParentIndex++;
            }
        }
    }

    /**
     * Sets the breadcrumb data.
     * 
     * @param position
     *            The position.
     * @param absoluteParent
     *            The absolute parent.
     * @param valueMap
     *            The properties valuemap.
     * @param isIssuePage
     *            If the page is issuePage.
     */
    private void setbreadCrumbData( int position, Page absoluteParent, ValueMap valueMap, boolean isIssuePage ) {
        LOG.info( "setbreadCrumbData starts ** " );
        BreadCrumbLinks breadCrumbBean;
        LOG.info( "setbreadCrumbData isIssuePage ** " + isIssuePage );
        String title;
        Boolean hideInBreadcrumb = false;
        if ( null != valueMap ) {
            hideInBreadcrumb = valueMap.containsKey( "hideInBreadcrumb" )
                    ? valueMap.get( "hideInBreadcrumb", Boolean.class ) : false;
        }
        if ( !hideInBreadcrumb ) {
            breadCrumbBean = new BreadCrumbLinks();
            title = isIssuePage ? CommonUtils.getDisplayDate( valueMap, dateFormat ) : absoluteParent.getPageTitle();

            if ( StringUtils.isEmpty( title ) ) {
                title = absoluteParent.getTitle();
            }
            LOG.info( "setbreadCrumbData title ** " + title );
            String path = absoluteParent.getPath();
            breadCrumbBean.setPath( InternalURLFormatter.formatURL( getResourceResolver(), path ) );
            breadCrumbBean.setTitle( title );
            breadCrumbBean.setPosition( position );
            this.breadCrumbLinks.add( breadCrumbBean );
        }

        LOG.info( "setbreadCrumbData ends ** " );
    }

    private boolean setHideBreadcrumb( Page currentPage ) {
        this.hideBreadcrumb = false;
        Resource jcrNodeResource = currentPage.getContentResource();
        ValueMap pageProperties = ( null != jcrNodeResource ) ? jcrNodeResource.getValueMap() : null;
        String template = ( null != pageProperties )
                ? pageProperties.get( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, StringUtils.EMPTY )
                : StringUtils.EMPTY;

        if ( StringUtils.equals( ClassMagsMigrationConstants.HOME_PAGE_TEMPLATE_PATH, template )
                || StringUtils.equals( ClassMagsMigrationConstants.ERROR_PAGE_TEMPLATE_PATH, template ) ) {
            hideBreadcrumb = true;
        }
        return hideBreadcrumb;
    }

    public List< BreadCrumbLinks > getBreadCrumbLinks() {
        return breadCrumbLinks;
    }

    public boolean getHideBreadcrumb() {
        return hideBreadcrumb;
    }

}
