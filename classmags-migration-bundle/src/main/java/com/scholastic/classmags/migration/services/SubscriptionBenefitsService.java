package com.scholastic.classmags.migration.services;

import java.util.List;

import org.apache.sling.api.resource.ValueMap;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;

/**
 * The Interface SubscriptionBenefitsService.
 */
public interface SubscriptionBenefitsService {

    /**
     * Fetch subscription benefits.
     *
     * @param currentPath
     *            the current path
     * @return the list
     */
    public List< ValueMap > fetchSubscriptionBenefits( String currentPath ) throws ClassmagsMigrationBaseException;
}
