package com.scholastic.classmags.migration.services;

import java.util.List;

import javax.jcr.RepositoryException;

/**
 * The Interface ArticleTextDataService.
 */
public interface ArticleTextDataService {

    /**
     * Fetch article data.
     *
     * @param currentPath
     *            the current path
     * @return the list
     * @throws RepositoryException
     *             the repository exception
     */
    public List< String > fetchArticleData( String currentPath ) throws RepositoryException;
}
