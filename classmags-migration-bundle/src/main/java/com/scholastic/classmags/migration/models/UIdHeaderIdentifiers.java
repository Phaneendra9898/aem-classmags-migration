package com.scholastic.classmags.migration.models;

/**
 * The Class UIdHeaderIdentifiers.
 */
public class UIdHeaderIdentifiers {

    /** The staff id. */
    private String staffId;

    /** The student id. */
    private String studentId;

    /**
     * Gets the staff id.
     *
     * @return the staffId
     */
    public String getStaffId() {
        return staffId;
    }

    /**
     * Sets the staff id.
     *
     * @param staffId
     *            the staffId to set
     */
    public void setStaffId( String staffId ) {
        this.staffId = staffId;
    }

    /**
     * Gets the student id.
     *
     * @return the studentId
     */
    public String getStudentId() {
        return studentId;
    }

    /**
     * Sets the student id.
     *
     * @param studentId
     *            the studentId to set
     */
    public void setStudentId( String studentId ) {
        this.studentId = studentId;
    }
}
