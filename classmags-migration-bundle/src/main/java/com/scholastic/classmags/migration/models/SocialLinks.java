package com.scholastic.classmags.migration.models;

import java.util.List;

import org.apache.sling.api.resource.ValueMap;

/**
 * The Class SocialLinks.
 */
public class SocialLinks {

    /** The section heading. */
    private String sectionHeading;

    /** The flag disabledForStudents. */
    private boolean disabledForStudents;
    
    /** The links. */
    private List< ValueMap > links;

    /**
     * Gets the section heading.
     *
     * @return the section heading
     */
    public String getSectionHeading() {
        return sectionHeading;
    }

    /**
     * Sets the section heading.
     * 
     * @param sectionHeading The section heading
     *            
     */
    public final void setSectionHeading( final String sectionHeading ) {
        this.sectionHeading = sectionHeading;
    }
    
    /**
     * Gets if social links are disabled for students.
     *
     * @return the flag disabledForStudents
     */
    public boolean isDisabledForStudents() {
        return disabledForStudents;
    }

    /**
     * Sets if social links are disabled for students.
     * 
     * @param disabledForStudents The flag disabledForStudents
     *            
     */
    public final void setDisabledForStudents( final boolean disabledForStudents ) {
        this.disabledForStudents = disabledForStudents;
    }
    
    /**
     * Gets the links.
     *
     * @return the links
     */
    public List< ValueMap > getLinks() {
        return links;
    }

    /**
     * Sets the links.
     * 
     * @param links The links
     *            
     */
    public final void setLinks( final List< ValueMap > links ) {
        this.links = links;
    }
}
