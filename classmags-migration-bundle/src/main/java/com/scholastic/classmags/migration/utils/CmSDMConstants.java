package com.scholastic.classmags.migration.utils;

/**
 * The Class CmSDMConstants.
 */
public class CmSDMConstants {

    /** The Constant JSON_KEY_LAUNCH_CONTEXT. */
    public static final String JSON_KEY_LAUNCH_CONTEXT = "launchContext";

    /** The Constant JSON_KEY_ROLE. */
    public static final String JSON_KEY_ROLE = "role";

    /** The Constant JSON_KEY_APPLICATION. */
    public static final String JSON_KEY_APPLICATION = "application";

    /** The Constant JSON_KEY_URL. */
    public static final String JSON_KEY_URL = "url";

    /** The Constant JSON_KEY_APPLICATION_CODE. */
    public static final String JSON_KEY_APPLICATION_CODE = "applicationCode";

    /** The Constant JSON_KEY_IDENTIFIERS. */
    public static final String JSON_KEY_IDENTIFIERS = "identifiers";

    /** The Constant JSON_KEY_STAFF_ID. */
    public static final String JSON_KEY_STAFF_ID = "staffId";

    /** The Constant JSON_KEY_STUDENT_ID. */
    public static final String JSON_KEY_STUDENT_ID = "studentId";

    /** The Constant REQ_ATTR_CUSTOM_DP_LAUNCH. */
    public static final String REQ_ATTR_CUSTOM_DP_LAUNCH = "custom_dp_launch";

    /** The Constant REQ_ATTR_CUSTOM_NAV_CTX. */
    public static final String REQ_ATTR_CUSTOM_NAV_CTX = "custom_sdm_nav_ctx";

    /** The Constant REQ_ATTR_OAUTH_SIGNATURE. */
    public static final String REQ_ATTR_OAUTH_SIGNATURE = "oauth_signature";

    /** The Constant REQ_ATTR_OAUTH_TIMESTAMP. */
    public static final String REQ_ATTR_OAUTH_TIMESTAMP = "oauth_timestamp";

    /** The Constant REQ_ATTR_OAUTH_CONSUMER_KEY. */
    public static final String REQ_ATTR_OAUTH_CONSUMER_KEY = "oauth_consumer_key";

    /** The Constant SDM_NAV_COOKIE_NAME. */
    public static final String SDM_NAV_COOKIE_NAME = "sdm_nav_ctx";

    /** The Constant SW_AUTH_COOKIE_NAME. */
    public static final String SW_AUTH_COOKIE_NAME = "sw_auth_cookie";

    /** The Constant SW_USER_ID_COOKIE_NAME. */
    public static final String SW_USER_ID_COOKIE_NAME = "sw_user_id_cookie";

    /** The Constant CUSTOM_STATE_PARAM. */
    public static final String CUSTOM_STATE_PARAM = "custom_state";

    /** The Constant CM_LOGGED_IN_HOMEPAGE_PATH. */
    public static final String CM_LOGGED_IN_HOMEPAGE_PATH = "path.home.loggedin";

    /** The Constant CM_LOGGED_OUT_HOMEPAGE_PATH. */
    public static final String CM_LOGGED_OUT_HOMEPAGE_PATH = "path.home.loggedout";

    /** The Constant JSON_KEY_ORG_CONTEXT. */
    public static final String JSON_KEY_ORG_CONTEXT = "orgContext";

    /** The Constant JSON_KEY_ORGANIZATION. */
    public static final String JSON_KEY_ORGANIZATION = "organization";

    /** The Constant JSON_KEY_ID. */
    public static final String JSON_KEY_ID = "id";

    /** The Constant JSON_KEY_ENTITLEMENTS. */
    public static final String JSON_KEY_ENTITLEMENTS = "entitlements";

    /** The Constant JSON_KEY_ACTIVE. */
    public static final String JSON_KEY_ACTIVE = "active";

    /** The Constant JSON_KEY_GROUP_CODE. */
    public static final String JSON_KEY_GROUP_CODE = "groupCode";

    /** The Constant JSON_KEY_NAME. */
    public static final String JSON_KEY_NAME = "name";

    /** The Constant JSON_KEY_THUMBNAIL. */
    public static final String JSON_KEY_THUMBNAIL = "thumbnail";

    /**
     * Instantiates a new cm SDM constants.
     */
    public CmSDMConstants() {
    }
}
