package com.scholastic.classmags.migration.controllers;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.iterators.TransformIterator;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceMetadata;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.api.wrappers.ValueMapDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.adobe.granite.ui.components.ds.ValueMapResource;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.day.crx.JcrConstants;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * Use Class for ContentTagsDropdownUse.
 */
public class ContentTagsDropdownUse extends WCMUsePojo {

    private static final Logger LOG = LoggerFactory.getLogger( ContentTagsDropdownUse.class );
    
    /** The resolver. */
    private ResourceResolver resolver;

    /** The resources map. */
    private Map< String, String > resourcesMap = new HashMap< >();

    /**
     * Gets the dropdown.
     *
     * @return the dropdown
     */
    @SuppressWarnings( "unchecked" )
    public void getDropdown() {

        DataSource datasource = new SimpleDataSource(
                new TransformIterator( resourcesMap.keySet().iterator(), new Transformer() {
                    @Override
                    public Object transform( Object input ) {
                        String title = ( String ) input;
                        ValueMap vm = new ValueMapDecorator( new HashMap< String, Object >() );
                        vm.put( ClassMagsMigrationConstants.DATASOURCE_VALUE, title );
                        vm.put( ClassMagsMigrationConstants.DATASOURCE_TEXT, resourcesMap.get( title ) );
                        return new ValueMapResource( resolver, new ResourceMetadata(), JcrConstants.NT_UNSTRUCTURED,
                                vm );
                    }
                } ) );
        getRequest().setAttribute( DataSource.class.getName(), datasource );
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() {

        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        resolver = getResourceResolver();
        if ( null != slingScriptHelper ) {
            ResourceResolver serviceResolver = getServiceResourceResolver( slingScriptHelper.getService( ResourceResolverFactory.class ));
            if ( null != serviceResolver ) {
                resourcesMap = getContentTypeTagsMap( serviceResolver, ClassMagsMigrationConstants.CLASS_MAGS_CONTENT_TAG_ROOT_PATH );
                getDropdown();
                if(serviceResolver.isLive()){
                    serviceResolver.close();
                }
            }
        }
    }

    
    public Map <String, String> getContentTypeTagsMap(ResourceResolver resourceresolver, String tagPath){
        Map <String, String> tags = new HashMap<String, String>();
        if(resourceresolver!=null){
            TagManager tagManager= resourceresolver.adaptTo( TagManager.class );
            Tag tag = tagManager.resolve( tagPath );
            if(tag!=null){
                Iterator< Tag > it = tag.listChildren();
                while ( it.hasNext() ) {
                    Tag next = it.next();
                    tags.put( next.getTitle(), next.getTitle() );
                }
            }
        }
        return tags;
    }
    
    /**
     * This method will get service resolver for the accessing the tags
     * 
     * @param ResourceResolverFacatory
     * @return service ResourceResolver
     */
    private ResourceResolver getServiceResourceResolver(
            ResourceResolverFactory resourceResolverFactory) {
        ResourceResolver serviceResolver = null;
        final Map<String, Object> authInfo = Collections.singletonMap(
                ResourceResolverFactory.SUBSERVICE,
                (Object) ClassMagsMigrationConstants.READ_SERVICE);
        try {
            serviceResolver = resourceResolverFactory
                    .getServiceResourceResolver(authInfo);
        } catch (LoginException e) {
            LOG.error("Error getting service resource resolver : {}", e);
        }
        return serviceResolver;
    }
    
}
