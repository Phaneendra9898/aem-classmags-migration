package com.scholastic.classmags.migration.controllers;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.commons.Externalizer;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

public class GlobalLinkUse extends WCMUsePojo {
    
    private String formattedUrl;
    
    private String externalizedDomain;
    
    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( GlobalLinkUse.class );

    @Override
    public void activate() {
        LOG.info( "::::In activate method of Global Link Use::::" );
        String magazineName = StringUtils.EMPTY;
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        String currentPath = get( "url", String.class );
        if ( null != slingScriptHelper ) {
            MagazineProps magazineProps = slingScriptHelper.getService( MagazineProps.class );
            if ( null != magazineProps ) {
                magazineName = magazineProps.getMagazineName( getCurrentPage().getPath() );
            }

            Externalizer externalizer = slingScriptHelper.getService( Externalizer.class );
            if ( externalizer != null && StringUtils.isNotBlank( get( "url", String.class ) ) ) {
                this.externalizedDomain = externalizer.externalLink( getResourceResolver(), magazineName, currentPath );
            }
        }
        
        this.formattedUrl = InternalURLFormatter.formatURL( getResourceResolver(), get( "url", String.class ) );
    }

    /**
     * @return the formattedUrl
     */
    public String getFormattedUrl() {
        return formattedUrl;
    }

    /**
     * @return the externalizedDomain
     */
    public String getExternalizedDomain() {
        return externalizedDomain;
    }
}
