package com.scholastic.classmags.migration.services.impl;

import java.util.List;

import javax.jcr.Node;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.services.IssueService;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.ScholasticDataLayerService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class ScholasticDataLayerServiceImpl.
 */
@Service( ScholasticDataLayerService.class )
@Component
public class ScholasticDataLayerServiceImpl implements ScholasticDataLayerService {

    /** Logger instance. */
    private static final Logger LOGGER = LoggerFactory.getLogger( ScholasticDataLayerServiceImpl.class );

    @Reference
    private ResourceResolverFactory resolverFactory;

    @Reference
    private MagazineProps magazineProps;

    @Reference
    private IssueService issueService;

    /**
     * This method is used to create the digital data object.
     *
     * @param request The sling http servlet request.
     * @param currentPage The current page.
     * 
     * @return the data layer.
     */
    public JSONObject createDataLayer( SlingHttpServletRequest request, Page currentPage ) {

        ResourceResolver resourceResolver = null;
        TagManager tagManager;
        String vmap = null;
        String templateType = StringUtils.EMPTY;
        JSONObject digitalData = new JSONObject();
        String currentPath = currentPage.getPath();
        
        ResourceResolver res = CommonUtils.getResourceResolver(resolverFactory, ClassMagsMigrationConstants.READ_SERVICE);
        Resource resource = res.getResource(currentPath + ClassMagsMigrationConstants.FORWARD_SLASH + JcrConstants.JCR_CONTENT);
        if(null != resource && null != resource.getValueMap()){
        	vmap = resource.getValueMap().get("cmmarketing", String.class);
        }
        try {
            Resource pageResource;
            Node node = null;
            
            // create domain json
            if(vmap != null){
            	JSONObject domain = new JSONObject();
                domain.put( "name", "cm:marketing");
                domain.put( "channel", "cm:marketing");
                domain.put( "experienceType", "commerce" );
                domain.put( "experience", "classroommagazines" );
                domain.put( "audience", "Teachers,Educators" );
                digitalData.put( "domain", domain );
           } else{
            	JSONObject domain = new JSONObject();
	            String magazineName = magazineProps.getMagazineName( currentPath );
	            domain.put( "name", "cm:".concat( magazineName ) );
	            domain.put( "channel", "cm:".concat( magazineName ) );
	            domain.put( "experienceType", "Content" );
	            domain.put( "experience", "Content" );
	            domain.put( "audience", "Teachers" );
	            digitalData.put( "domain", domain );
            }

            // create page json
            JSONObject page = new JSONObject();
            resourceResolver = CommonUtils.getResourceResolver( resolverFactory,
                    ClassMagsMigrationConstants.READ_SERVICE );
            tagManager = CommonUtils.getTagManager( resolverFactory );

            pageResource = resourceResolver.getResource( currentPath + ClassMagsMigrationConstants.FORWARD_SLASH + JcrConstants.JCR_CONTENT );
            if ( null != pageResource ) {
                node = pageResource.adaptTo( Node.class );
            }

            String userType = CommonUtils.propertyCheck( ClassMagsMigrationConstants.ASSET_USER_TYPE, node ).isEmpty()
                    ? ClassMagsMigrationConstants.USER_TYPE_EVERYONE
                    : CommonUtils.propertyCheck( ClassMagsMigrationConstants.ASSET_USER_TYPE, node );
            page.put( "access", userType );

            String templatePath = CommonUtils.propertyCheck( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, node );

            if ( StringUtils.equals( ClassMagsMigrationConstants.ISSUE_PAGE_TEMPLATE_PATH, templatePath ) ) {
                templateType = ClassMagsMigrationConstants.ISSUE;
                String schoolYear = StringUtils.substringAfterLast( StringUtils.substringBeforeLast( currentPath, ClassMagsMigrationConstants.FORWARD_SLASH ),
                        ClassMagsMigrationConstants.FORWARD_SLASH );
                String issueDate = StringUtils.substringAfterLast( currentPath, ClassMagsMigrationConstants.FORWARD_SLASH );
                page.put( "name", schoolYear.concat( ClassMagsMigrationConstants.FORWARD_SLASH ).concat( issueDate ) );
            } else if ( StringUtils.equals( ClassMagsMigrationConstants.ARTICLE_PAGE_TEMPLATE_PATH, templatePath ) ) {
                templateType = ClassMagsMigrationConstants.ARTICLE;
                page.put( "name", templateType );
            } else if ( StringUtils.equals( ClassMagsMigrationConstants.ARCHIVE_PAGE_TEMPLATE_PATH, templatePath ) ) {
                templateType = "archive";
                page.put( "name", templateType );
            }else if ( StringUtils.equals( ClassMagsMigrationConstants.HOME_PAGE_TEMPLATE_PATH, templatePath ) ) {
                templateType = "home";
                page.put( "name", templateType );
            }

            page.put( "template", templateType );
            digitalData.put( "page", page );

            // create Article json
            if ( StringUtils.equals( ClassMagsMigrationConstants.ARTICLE, templateType ) ) {
                JSONObject article = new JSONObject();
                String issue = StringUtils.EMPTY;

                String title = StringUtils.substringAfterLast( currentPath, ClassMagsMigrationConstants.FORWARD_SLASH );
                article.put( "title", title );

                // Language to be pulled dynamically from Article Toolbar later.
                article.put( "language", "English" );

                // Reading Level to be pulled by event handling.
                article.put( "readingLevel", "TBD" );

                // Article type (default value added currently)
                article.put( "type", "issue-article" );

                List< String > tags = CommonUtils.getTags( pageResource, tagManager );
                article.put( "subjects", tags );

                digitalData.put( ClassMagsMigrationConstants.ARTICLE, article );

                // Create Issue json for article
                String parentIssuePath = issueService.getParentIssuePath( request, currentPage );
                if ( !parentIssuePath.isEmpty() ) {
                    issue = StringUtils.substringBefore( StringUtils.substringAfterLast( parentIssuePath, ClassMagsMigrationConstants.FORWARD_SLASH ),
                            ".html" );
                }
                digitalData.put( ClassMagsMigrationConstants.ISSUE, issue );
            }

            // create Issue json
            if ( StringUtils.equals( ClassMagsMigrationConstants.ISSUE, templateType ) ) {
                String issue = StringUtils.substringAfterLast( currentPath, ClassMagsMigrationConstants.FORWARD_SLASH );
                digitalData.put( ClassMagsMigrationConstants.ISSUE, issue );
            }

            // create About json
            JSONObject about = new JSONObject();
            about.put( "implementation", "classroommagazines" );
            about.put( "version", "1.0" );
            digitalData.put( "about", about );

        } catch ( JSONException e ) {
            LOGGER.error( "JSONException occured during creating data layer :: " + e );
        }
        return digitalData;
    }

}
