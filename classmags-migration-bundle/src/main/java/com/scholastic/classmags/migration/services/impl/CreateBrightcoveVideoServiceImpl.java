package com.scholastic.classmags.migration.services.impl;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.AccessControlException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.AssetManager;
import com.day.cq.tagging.InvalidTagFormatException;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.scholastic.classmags.migration.models.BrightcoveVideoRenditions;
import com.scholastic.classmags.migration.models.MediaApiEnum;
import com.scholastic.classmags.migration.models.VideoArchive;
import com.scholastic.classmags.migration.models.VideoSearchServiceData;
import com.scholastic.classmags.migration.services.CreateBrightcoveVideoConfigService;
import com.scholastic.classmags.migration.services.CreateBrightcoveVideoService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class CreateBrightcoveVideoServiceImpl.
 */
@Component( immediate = true, metatype = false, label = "Create shell videos in AEM from Brightcove video service", description = "This service creates shell videos in AEM from Brightcove"
        + " in the DAM with the referenced video" )
@Service( value = CreateBrightcoveVideoService.class )
public class CreateBrightcoveVideoServiceImpl implements CreateBrightcoveVideoService {

    /** The Constant NULL_STRING. */
    private static final String NULL_STRING = "null";

    /** The Constant FORWARD_SLASH_STRING. */
    private static final String FORWARD_SLASH_STRING = "/";

    /** The Constant PNG_EXTENSION_STRING. */
    private static final String PNG_EXTENSION_STRING = "png";

    /** The Constant MP4_EXTENSION_STRING. */
    private static final String MP4_EXTENSION_STRING = "mp4";

    /** The Constant FLV_EXTENSION_STRING. */
    private static final String FLV_EXTENSION_STRING = "flv";

    /** The Constant VIDEO_MIME_TYPE_MP4_STRING. */
    private static final String VIDEO_MIME_TYPE_MP4_STRING = "video/mp4";

    /** The Constant VIDEO_MIME_TYPE_FLV_STRING. */
    private static final String VIDEO_MIME_TYPE_FLV_STRING = "video/x-flv";

    /** The Constant QUESTION_STRING. */
    private static final String QUESTION_STRING = "?";

    /** The Constant DOT_STRING. */
    private static final String DOT_STRING = ".";

    /** The Constant UNDERSCORE_STRING. */
    private static final String UNDERSCORE_STRING = "_";

    /** The Constant COLON_STRING. */
    private static final String COLON_STRING = ":";

    /** The Constant BLANK_STRING. */
    private static final String BLANK_STRING = " ";

    /** The Constant DAM_VIDEOID_FIELD. */
    private static final String DAM_VIDEOID_FIELD = "videoId";

    /** The Constant DAM_BRIGHTCOVE_VIDEO_FIELD. */
    private static final String DAM_BRIGHTCOVE_VIDEO_FIELD = "brightcoveVideo";

    /** The Constant DAM_DC_TITLE_FIELD. */
    private static final String DAM_DC_TITLE_FIELD = "dc:title";

    /** The Constant DAM_DC_FORMAT_FIELD. */
    private static final String DAM_DC_FORMAT_FIELD = "dc:format";

    /** The Constant DAM_SHORT_DESCRIPTION_FIELD. */
    private static final String DAM_SHORT_DESCRIPTION_FIELD = "shortDescription";

    /** The Constant DAM_LONG_DESCRIPTION_FIELD. */
    private static final String DAM_LONG_DESCRIPTION_FIELD = "longDescription";

    /** The Constant DAM_CLOSED_CAPTION_FIELD. */
    private static final String DAM_CLOSED_CAPTION_FIELD = "closedCaptionURL";

    /** The Constant DAM_DC_DESCRIPTION_FIELD. */
    private static final String DAM_DC_DESCRIPTION_FIELD = "dc:description";

    /** The Constant DAM_BRIGHTCOVE_TAG_PREFIX. */
    private static final String DAM_BRIGHTCOVE_TAG_PREFIX = "brightcove:";

    /** The Constant BRIGHTCOVE_TAG_STRING. */
    private static final String BRIGHTCOVE_TAG_STRING = "Brightcove";

    /** The Constant JCR_CONTENT_TAG_STRING. */
    private static final String JCR_CONTENT_TAG_STRING = "jcr:content/metadata";

    /** The Constant SUCCESS_CREATING_VIDEO_IN_AEM. */
    public static final String SUCCESS_CREATING_VIDEO_IN_AEM = "Success";

    /** The Constant ERROR_CREATING_VIDEO_IN_AEM. */
    public static final String ERROR_CREATING_VIDEO_IN_AEM = "Error";

    /** The Constant ERROR_CREATING_AEM_VIDEO. */
    public static final String ERROR_CREATING_AEM_VIDEO = "Error creating AEM video from BrightcoveVideo: ";

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( CreateBrightcoveVideoServiceImpl.class );

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;
    
    private static final int FORTY_EIGHT = 48;
    private static final int TWENTY_NINE = 29;
    private static final int ONE_FORTY = 140;
    private static final int NINTY = 90;
    private static final int ZERO = 0;

    /** The create brightcove video config service. */
    @Reference
    private CreateBrightcoveVideoConfigService createBrightcoveVideoConfigService;

    /**
     * Gets the brightcove video.
     * 
     * @param videoID
     *            the video ID
     * @return the brightcove video
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private String getBrightcoveVideo( String videoID ) throws IOException {
        String jsonVideo = null;

        HttpClient httpClient;
        httpClient = new HttpClient();

        VideoSearchServiceData videoSearchServiceData = new VideoSearchServiceData();

        videoSearchServiceData.setUrl( createBrightcoveVideoConfigService.getReadUrl() );
        videoSearchServiceData.setSearchComand( createBrightcoveVideoConfigService.getFindById() );
        videoSearchServiceData.setFieldsSearch( null );
        videoSearchServiceData.setVideoId( videoID );
        videoSearchServiceData.setPageSize( null );
        videoSearchServiceData.setVideoFields( createBrightcoveVideoConfigService.getFields() );
        videoSearchServiceData.setMediaDelivery( createBrightcoveVideoConfigService.getMediaDelivery() );
        videoSearchServiceData.setSortBy( null );
        videoSearchServiceData.setPageNumber( null );
        videoSearchServiceData.setGetItemCount( null );
        videoSearchServiceData.setCallback( createBrightcoveVideoConfigService.getCallback() );
        videoSearchServiceData.setToken( createBrightcoveVideoConfigService.getReadToken() );
        videoSearchServiceData.setSearchVideos( createBrightcoveVideoConfigService.getSearchVideos() );

        // Url Search Videos Service
        URL url = new URL( MediaApiEnum.getSearchServiceURL( videoSearchServiceData ) );

        HttpMethod method = new GetMethod( url.toString() );
        method.addRequestHeader( "Accept", "application/json" );

        int statusCode = httpClient.executeMethod( method );
        if ( statusCode == HttpStatus.SC_OK ) {
            jsonVideo = method.getResponseBodyAsString().replace( "BCL.onSearchResponse(", "" );
            jsonVideo = jsonVideo.replace( ");", "" );
        }
        return jsonVideo;
    }

    /**
     * Creates the video object in AEM, if the video queried in Brightcove could
     * be found.
     * 
     * @param brightcoveVideoID
     *            The ID of the video to create in AEM
     * @param damPath
     *            The DAM path where the video will be created
     * @return Map containing a status message and a response message indicating
     *         success/error
     */
    @Override
    public Map< String, String > createVideoinAEM( String brightcoveVideoID, String damPath ) {
        Map< String, String > responseMessage = new HashMap<>();
        StringBuilder statusMessage = new StringBuilder();

        try {

            String brightcoveVideoJson = getBrightcoveVideo( brightcoveVideoID );
            LOG.info( "brightcoveVideoJson: " + brightcoveVideoJson );

            if ( brightcoveVideoJson.equals( NULL_STRING )
                    || !MediaApiEnum.validateErrorResponse( brightcoveVideoJson, LOG ) ) {

                statusMessage.append( "Please make sure you typed a valid video ID and Brightcove is accesible" );
                responseMessage.put( ERROR_CREATING_VIDEO_IN_AEM, statusMessage.toString() );

            } else {
                LOG.debug( "Brightcove Video found!!!" );

                Gson gson = new GsonBuilder().create();
                VideoArchive brightcoveVideo = gson.fromJson( brightcoveVideoJson, VideoArchive.class );

                if ( brightcoveVideo != null ) {
                    createBrightcoveVideo( brightcoveVideo, damPath );

                    statusMessage.append( "Video successfully created in AEM" );
                    responseMessage.put( SUCCESS_CREATING_VIDEO_IN_AEM, statusMessage.toString() );
                }

            }
        } catch ( HttpException e ) {
            LOG.error( "Error connecting to Brightcove Media API: " + e );
        } catch ( IOException e ) {
            LOG.error( ERROR_CREATING_AEM_VIDEO + e );
        } catch ( AccessControlException e ) {
            LOG.error( "Error creating tags for the video. Current user is not allowed to do so: " + e );
        } catch ( Exception e ) {
            LOG.error( "Exception in createVideoinAEM: " + e );
        }
        return responseMessage;
    }

    /**
     * Creates the brightcove video.
     * 
     * @param brightcoveVideo
     *            the brightcove video
     * @param damPath
     *            the dam path
     */
    private void createBrightcoveVideo( VideoArchive brightcoveVideo, String damPath ) {

        String fileName;
        String videoFormat;
        String videoMimeType = null;
        ResourceResolver resourceResolver;

        final Map< String, Object > param = new HashMap<>();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.WRITE_SERVICE );
        try {
            resourceResolver = resourceResolverFactory.getServiceResourceResolver( param );

            if ( !brightcoveVideo.getRenditions().isEmpty() ) {
                BrightcoveVideoRenditions firstRendition = brightcoveVideo.getRenditions().get( 0 );
                fileName = damPath.concat( JcrUtil.createValidName( firstRendition.getDisplayName() ) );
                videoFormat = firstRendition.getVideoContainer().toLowerCase();
            } else {
                String videoURL = brightcoveVideo.getFlvURL();
                int parametersUrlVideo = videoURL.lastIndexOf( QUESTION_STRING );
                if ( parametersUrlVideo != -1 ){
                    videoURL = videoURL.substring( 0, parametersUrlVideo );
                }
                videoFormat = videoURL.substring( videoURL.lastIndexOf( DOT_STRING ), videoURL.length() ).toLowerCase();

                fileName = damPath.concat( JcrUtil.createValidName( brightcoveVideo.getName().concat( videoFormat ) ) );
            }

            if ( videoFormat.endsWith( MP4_EXTENSION_STRING ) ) {
                videoMimeType = VIDEO_MIME_TYPE_MP4_STRING;
            }
            if ( videoFormat.endsWith( FLV_EXTENSION_STRING ) ) {
                videoMimeType = VIDEO_MIME_TYPE_FLV_STRING;
            }

            InputStream videoThumbnail = downloadFileFromBrightcove( brightcoveVideo.getThumbnailURL() );

            // Use AssetManager to place the file into the AEM DAM
            AssetManager assetMgr = resourceResolver.adaptTo( AssetManager.class );
            Asset videoAsset = assetMgr != null ? assetMgr.createAsset( fileName, videoThumbnail, videoMimeType, true )
                    : null;

            // Add metadata to video object
            if ( videoAsset != null ) {
                addVideoMetadata( videoAsset, brightcoveVideo, videoMimeType, resourceResolver );

                addVideoRendition( videoAsset, brightcoveVideo );
            }
        } catch ( LoginException e ) {
            LOG.error( "Error getting resourceResolver: " + e );
        } catch ( IOException e ) {
            LOG.error( ERROR_CREATING_AEM_VIDEO + e );
        } catch ( InvalidTagFormatException e ) {
            LOG.error( "The tag name is not to use as a valid tag ID: " + e );
        }

    }

    /**
     * Adds the video rendition.
     * 
     * @param videoAsset
     *            the video asset
     * @param brightcoveVideo
     *            the brightcove video
     */
    private void addVideoRendition( Asset videoAsset, VideoArchive brightcoveVideo ) {

        try {
            if ( brightcoveVideo.getVideoStillURL() != null ) {
                InputStream videoStillImage = downloadFileFromBrightcove( brightcoveVideo.getVideoStillURL() );
                videoAsset.addRendition( "cq5dam.thumbnail.319.319.png", videoStillImage, "image/jpeg" );
                videoStillImage = downloadFileFromBrightcove( brightcoveVideo.getVideoStillURL() );
                videoAsset.addRendition( "cq5dam.web.videostill.jpeg", videoStillImage, "image/jpeg" );
            }

            // Create thumbnails for video object
            InputStream damThumbnail = createDamThumbnail( brightcoveVideo.getThumbnailURL(), FORTY_EIGHT, TWENTY_NINE );
            videoAsset.addRendition( "cq5dam.thumbnail.48.48.png", damThumbnail, "image/png" );
            damThumbnail = createDamThumbnail( brightcoveVideo.getThumbnailURL(), ONE_FORTY, NINTY );
            videoAsset.addRendition( "cq5dam.thumbnail.140.100.png", damThumbnail, "image/png" );
            
        } catch ( IOException e ) {
            LOG.error( ERROR_CREATING_AEM_VIDEO + e );
        }
    }

    /**
     * Download a file (thumbnail image or video) from Brightcove.
     * 
     * @param fileURL
     *            The URL for the file to download
     * @return InputStream with the file downloaded
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private InputStream downloadFileFromBrightcove( String fileURL ) throws IOException {
        InputStream fileToDownload;
        URL url = new URL( fileURL );
        fileToDownload = url.openStream();

        return fileToDownload;
    }

    /**
     * Creates the thumbnails needed for the video in AEM, given the image path
     * in Brightcove and size (width and height) of the thumbnail to create.
     * 
     * @param videoThumbnailURL
     *            The image URL to download from Brightcove
     * @param width
     *            The width of the thumbnail to create
     * @param height
     *            The height of the thumbnail to create
     * @return InputStream with the thumbnail created
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    private InputStream createDamThumbnail( String videoThumbnailURL, int width, int height ) throws IOException {
        InputStream videoThumbnailStream = downloadFileFromBrightcove( videoThumbnailURL );
        BufferedImage videoThumbnail = ImageIO.read( videoThumbnailStream );

        // creates output image
        BufferedImage damThumbnail = new BufferedImage( width, height, BufferedImage.TYPE_INT_ARGB );

        // Resize the image and draw it on to the buffered image
        Graphics2D graphics2d = damThumbnail.createGraphics();
        graphics2d.drawImage( videoThumbnail, ZERO, ZERO, width, height, null );
        graphics2d.dispose();

        // Convert the BufferedImage to InputStream
        ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write( damThumbnail, PNG_EXTENSION_STRING, arrayOutputStream );
        videoThumbnailStream = new ByteArrayInputStream( arrayOutputStream.toByteArray() );

        return videoThumbnailStream;
    }

    /**
     * Adds the metadata (taken from Brightcove) to the new video created in
     * AEM.
     * 
     * @param videoAsset
     *            The Asset (video object) created in AEM
     * @param brightcoveVideo
     *            The object with the fields to add to the video
     * @param videoMimeType
     *            the video mime type
     * @param resourceResolver
     *            The resourceResolver to use when adding metadata
     * @throws InvalidTagFormatException
     *             the invalid tag format exception
     * @throws PersistenceException
     *             the persistence exception
     */
    private void addVideoMetadata( Asset videoAsset, VideoArchive brightcoveVideo, String videoMimeType,
            ResourceResolver resourceResolver ) throws InvalidTagFormatException, PersistenceException {
        Resource videoMetadata = videoAsset.adaptTo( Resource.class ).getChild( JCR_CONTENT_TAG_STRING );
        ModifiableValueMap map = videoMetadata.adaptTo( ModifiableValueMap.class );

        if ( !map.containsKey( DAM_DC_FORMAT_FIELD ) ) {
            map.put( DAM_DC_FORMAT_FIELD, videoMimeType );
        }

        map.put( DAM_VIDEOID_FIELD, brightcoveVideo.getId() );
        map.put( DAM_BRIGHTCOVE_VIDEO_FIELD, true );

        String videoName = "";

        if ( brightcoveVideo.getName() != null ) {
            videoName = brightcoveVideo.getName();
        }

        map.put( DAM_DC_TITLE_FIELD, videoName );

        if ( brightcoveVideo.getShortDescription() != null ) {
            map.put( DAM_SHORT_DESCRIPTION_FIELD, brightcoveVideo.getShortDescription() );
        }

        if ( brightcoveVideo.getLongDescription() != null ) {
            map.put( DAM_LONG_DESCRIPTION_FIELD, brightcoveVideo.getLongDescription() );
        }

        if ( brightcoveVideo.getCaptioning() != null ) {
            try {
                map.put( DAM_CLOSED_CAPTION_FIELD, brightcoveVideo.getCaptioning().getCaptionSources().get( 0 )
                        .getUrl() );
            } catch ( NullPointerException e ) {
                LOG.error( "Not closed caption available for Video ID: " + brightcoveVideo.getId() + "::Exception::"
                        + e );
            }
        }

        String videoDescription = brightcoveVideo.getShortDescription() != null ? brightcoveVideo.getShortDescription()
                : brightcoveVideo.getLongDescription();

        if ( videoDescription != null && !videoDescription.isEmpty() ) {
            map.put( DAM_DC_DESCRIPTION_FIELD, videoDescription );
        }

        List< String > brightcoveTags = brightcoveVideo.getTags();

        setTags( brightcoveTags, videoMetadata );

        setVideoDuration( brightcoveVideo, map );

        // Store the metadata in the repository
        // close addVideoMetadata
        resourceResolver.commit();
    }

    /**
     * Sets the video duration.
     * 
     * @param brightcoveVideo
     *            the brightcove video
     * @param map
     *            the map
     */
    private void setVideoDuration( VideoArchive brightcoveVideo, ModifiableValueMap map ) {
        BrightcoveVideoRenditions firstRendition = brightcoveVideo.getRenditions().get( 0 );
        String videoDuration = firstRendition.getVideoDuration();
        if ( StringUtils.isNotBlank( videoDuration ) ) {
            map.put( "videoDuration", convertVideoDurationToMins( videoDuration ) );
        }

    }

    /**
     * Sets the tags.
     * 
     * @param brightcoveTags
     *            the brightcove tags
     * @param videoMetadata
     *            the video metadata
     */
    private void setTags( List< String > brightcoveTags, Resource videoMetadata ) {
        if ( brightcoveTags != null && !brightcoveTags.isEmpty() ) {
            TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );
            if ( tagManager != null ) {
                try {
                    tagManager.createTag( DAM_BRIGHTCOVE_TAG_PREFIX, BRIGHTCOVE_TAG_STRING, null );

                    List< Tag > tagsList = getTagList( brightcoveTags, tagManager );

                    Tag[] videoTags = tagsList.toArray( new Tag[tagsList.size()] );
                    tagManager.setTags( videoMetadata, videoTags );
                } catch ( AccessControlException | InvalidTagFormatException e ) {
                    LOG.error( "Error creating tags for Brightcove video:" + e );
                }

            }
        }
    }

    /**
     * Gets the tag list.
     * 
     * @param brightcoveTags
     *            the brightcove tags
     * @param tagManager
     *            the tag manager
     * @return the tag list
     */
    private List< Tag > getTagList( List< String > brightcoveTags, TagManager tagManager ) {
        List< Tag > tagsList = new ArrayList<>();
        String tagTitle;

        for ( String currentTag : brightcoveTags ) {
            tagTitle = currentTag;
            if ( currentTag.contains( BLANK_STRING ) ) {
                currentTag = currentTag.replaceAll( BLANK_STRING, UNDERSCORE_STRING );
            }
            if ( currentTag.contains( COLON_STRING ) ){
                currentTag = currentTag.replaceAll( COLON_STRING, FORWARD_SLASH_STRING );
            }
            Tag videoTag;
            try {
                videoTag = tagManager.createTag( DAM_BRIGHTCOVE_TAG_PREFIX.concat( currentTag.toLowerCase() ),
                        tagTitle, null );
                tagsList.add( videoTag );
            } catch ( AccessControlException | InvalidTagFormatException e ) {
                LOG.error( "Error creating tags for Brightcove video:" + e );
            }
        }
        return tagsList;
    }

    /**
     * Convert video duration to mins.
     * 
     * @param videoDuration
     *            the video duration
     * @return the string
     */
    private String convertVideoDurationToMins( String videoDuration ) {

        int milliseconds = Integer.parseInt( videoDuration );
        return String.format(
                "%d:%02d",
                TimeUnit.MILLISECONDS.toMinutes( milliseconds ),
                TimeUnit.MILLISECONDS.toSeconds( milliseconds )
                        - TimeUnit.MINUTES.toSeconds( TimeUnit.MILLISECONDS.toMinutes( milliseconds ) ) );

    }
}
