package com.scholastic.classmags.migration.services;

import java.util.Map;

/**
 * The Interface CreateBrightcoveVideoService.
 */
public interface CreateBrightcoveVideoService {

    /**
     * Creates the videoin AEM.
     * 
     * @param brightcoveVideoID
     *            the brightcove video ID
     * @param damPath
     *            the dam path
     * @return the map
     */
    public Map< String, String > createVideoinAEM( String brightcoveVideoID, String damPath );

}
