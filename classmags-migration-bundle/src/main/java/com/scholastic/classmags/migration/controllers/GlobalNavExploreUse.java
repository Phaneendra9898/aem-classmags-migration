package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.models.LinkObject;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

public class GlobalNavExploreUse extends WCMUsePojo {

    private List< LinkObject > exploreLinks;
    
    private LinkObject seeAllLink;
    
    @Override
    public void activate() {
        List<ValueMap> links = CommonUtils.fetchMultiFieldData( getResource(), "explore-links" );
        ResourceResolver resourceResolver = (getResource() != null)? getResource().getResourceResolver(): null;
        exploreLinks = new ArrayList<>();
        for ( ValueMap link : links ) {
            LinkObject exploreLink = new LinkObject();
            exploreLink.setLabel( link.get( "linkLabel", String.class ) );
            exploreLink.setPath(
                    InternalURLFormatter.formatURL( resourceResolver, link.get( "linkPath", String.class ) ) );
            exploreLinks.add( exploreLink );
        }
        
        seeAllLink = new LinkObject();
        seeAllLink.setLabel( getProperties().get( "seeAllLabel", String.class ));
        seeAllLink.setPath( InternalURLFormatter.formatURL( resourceResolver, getProperties().get( "seeAllLink",String.class ) ));
    }
    
    /**
     * Gets the explore links.
     *
     * @return the explore links
     */
    public List< LinkObject > getExploreLinks() {
        return exploreLinks;
    }
    
    /**
     * Gets the see all link.
     *
     * @return the see all link
     */
    public LinkObject getSeeAllLink() {
        return seeAllLink;
    }
}
