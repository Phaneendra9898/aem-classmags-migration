package com.scholastic.classmags.migration.social.impl.operations;

import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.servlets.post.PostOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.social.scf.OperationException;
import com.adobe.cq.social.scf.SocialComponent;
import com.adobe.cq.social.scf.SocialComponentFactoryManager;
import com.adobe.cq.social.scf.SocialOperationResult;
import com.adobe.cq.social.scf.core.operations.AbstractSocialOperation;
import com.scholastic.classmags.migration.social.api.VotingService;
import com.scholastic.classmags.migration.social.api.VotingSocialComponent;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;

/**
 * A POST Endpoint for GetVoteDataOperation that accepts requests for loading
 * votes data. This class responds to all POST requests with a
 * :operation=social:voting:getVoteData parameter. For example, curl
 * http://localhost:4502/content/classroom_magazines/scienceworld/issues/2016-17
 * /090516/testarticle/jcr:content/par/voting.social.json -uadmin:admin -v POST
 * -H "Accept:application/json" --data ":operation=social:voting:getVoteData"
 */
@Component( immediate = true )
@Service
@Property( name = PostOperation.PROP_OPERATION_NAME, value = ClassMagsMigrationASRPConstants.SOCIAL_GET_VOTE_DATA_OPERATION )
public class GetVoteDataOperation extends AbstractSocialOperation {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( GetVoteDataOperation.class );

    @Reference
    private VotingService votingService;

    /** The scf manager. */
    @Reference
    private SocialComponentFactoryManager scfManager;

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.social.scf.core.operations.AbstractSocialOperation#
     * performOperation(org.apache.sling.api.SlingHttpServletRequest)
     */
    @Override
    protected SocialOperationResult performOperation( SlingHttpServletRequest slingHttpServletRequest )
            throws OperationException {

        LOG.info( ClassMagsMigrationASRPConstants.OPERATION_START
                + ClassMagsMigrationASRPConstants.SOCIAL_GET_VOTE_DATA_OPERATION );

        final Resource votingResource = this.votingService.getVoteDataResource( slingHttpServletRequest );

        String resourcePath;
        int httpStatusCode = HttpServletResponse.SC_OK;
        String httpStatusMessage = ClassMagsMigrationASRPConstants.HTTP_SUCCESS_MESSAGE;

        if ( null == votingResource ) {
            resourcePath = slingHttpServletRequest.getResource().getPath();
        } else {
            resourcePath = votingResource.getPath();
        }

        SocialComponent socialComponent = SocialASRPUtils.getSocialComponent( slingHttpServletRequest, votingResource,
                this.scfManager, VotingSocialComponent.VOTING_RESOURCE_TYPE );

        LOG.info( ClassMagsMigrationASRPConstants.OPERATION_END );

        return new SocialOperationResult( socialComponent, httpStatusMessage, httpStatusCode, resourcePath );
    }
}
