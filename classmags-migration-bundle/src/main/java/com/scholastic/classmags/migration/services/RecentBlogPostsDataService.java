package com.scholastic.classmags.migration.services;

import java.util.List;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.RecentBlogPostsData;

/**
 * The Interface RecentBlogPostsDataService.
 */
public interface RecentBlogPostsDataService {

    /**
     * Fetch recent blog posts data.
     *
     * @param currentNodePath
     *            the current node path
     * @return the list
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    public List< RecentBlogPostsData > fetchRecentBlogPostsData( String currentNodePath )
            throws ClassmagsMigrationBaseException;
}
