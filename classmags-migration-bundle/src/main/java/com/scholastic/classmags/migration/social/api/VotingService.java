package com.scholastic.classmags.migration.social.api;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;

import com.adobe.cq.social.scf.OperationException;

/**
 * The Interface VotingService.
 */
public interface VotingService {

    /**
     * Submit and create vote resource.
     *
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @return the resource
     * @throws OperationException
     *             the operation exception
     */
    Resource submitAndCreateVoteResource( SlingHttpServletRequest slingHttpServletRequest ) throws OperationException;

    /**
     * Gets the vote data resource.
     *
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @return the vote data resource
     * @throws OperationException
     *             the operation exception
     */
    Resource getVoteDataResource( SlingHttpServletRequest slingHttpServletRequest ) throws OperationException;
}
