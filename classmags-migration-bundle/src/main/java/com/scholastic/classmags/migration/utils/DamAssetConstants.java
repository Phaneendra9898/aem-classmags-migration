/**
 * 
 */
package com.scholastic.classmags.migration.utils;

/**
 * @author gowthami
 *
 */
public class DamAssetConstants {
	
	//Request Parameters
	public static final String PATH_PARAM = "path";
	public static final String MIME_TYPE_PARAM = "mimeType";
	
	//Request constraints
	public static final String CONTENT_TYPE = "application/json";
	public static final String ENCODING_STANDARD = "UTF-8";
	
	//Response JSON Properties
	public static final String NAME = "Name";
	public static final String PATH = "Path";
	public static final String TITLE = "Title";
	public static final String DESCRIPTION = "Description";
	public static final String FILE_FORMAT = "File Format";
	public static final String TAGS = "Tags";
	public static final String TAG = "Tag";
	public static final String THUMBNAIL_IMAGE_PATH = "ThumbnailImagePath";
	
	//Dam Asset Jcr properties
	public static final String JCR_CONTENT = "jcr:content";
	public static final String METADATA = "metadata";
	public static final String DC_TITLE = "dc:title";
	public static final String DC_DESCRIPTION = "dc:description";
	public static final String DC_FORMAT = "dc:format";
	public static final String CQ_TAGS = "cq:tags";
	public static final String JCR_BRIGHTCOVE_EMBEDDED_PLAYER_ID = "jcr:brightcoveEmbeddedPlayerId";
	public static final String JCR_BRIGHTCOVE_NON_EMBEDDED_PLAYER_ID = "jcr:brightcoveNonEmbeddedPlayerId";
	
	//Video Asset properties
	public static final String VIDEO_FORMAT_MP4 = "mp4";
	public static final String BRIGHTCOVE_VIDEO = "brightcoveVideo";
	public static final String PLAYER_ID = "playerId";
	public static final String VIDEO_ID = "videoId";
	public static final String ACCOUNT_ID = "accountId";
	public static final String BRIGHTCOVE_EMBEDDED_PLAYER_ID = "brightcoveEmbeddedPlayerId";
	public static final String BRIGHTCOVE_NON_EMBEDDED_PLAYER_ID = "brightcoveNonEmbeddedPlayerId";
	
	public static final CharSequence PDF_SUBASSETS_FILTER = "subassets";
	
	//Run modes
	public static final String AUTHOR_RUN_MODE = "author";
	public static final String PUBLISH_RUN_MODE = "publish";
	
	//Content Type
	public static final String MIME_TYPE_AUDIO = "audio";
	public static final String MIME_TYPE_VIDEO = "video";
	public static final String MIME_TYPE_APPLICATION = "application";
	public static final String MIME_TYPE_IMAGE = "image";
	
	//query map constants
	public static final String QUERYMAP_KEY_TYPE = "type";
	public static final String QUERYMAP_VALUE_TYPE = "dam:Asset";
	public static final String QUERYMAP_KEY_PATH = "path";
	public static final String QUERYMAP_KEY_ORDERBYSORT = "orderby.sort";
	public static final String QUERYMAP_VALUE_ORDERBYSORT = "desc";
	public static final String QUERYMAP_KEY_PROPERTYOPERATION = "property.operation";
	public static final String QUERYMAP_VALUE_PROPERTYOPERATION = "like";
	public static final String QUERYMAP_KEY_PROPERTY = "property";
	public static final String QUERYMAP_VALUE_PROPERTY = "jcr:content/metadata/dc:format";
	public static final String QUERYMAP_KEY_PLimit = "p.limit";
	public static final String QUERYMAP_VALUE_PLimit = "-1";
	public static final String QUERYMAP_PROPERTY_VALUE = "property.value";
	
	//Search query strings
	public static final String QUERYMAP_AUDIO_QUERY_STRING = "audio%";
	public static final String QUERYMAP_VIDEO_QUERY_STRING = "video%";
	public static final String QUERYMAP_IMAGE_QUERY_STRING = "image%";
	public static final String QUERYMAP_APPLICATION_QUERY_STRING = "application/pdf";

	//General Constants
	public static final String EMPTY_STRING = "";
	public static final String ACCOUNT_ID_VALUE = "1543299976";
	public static final CharSequence URI = "/content/dam/classroom-magazines/";
	public static final String URL = "content/dam/classroom-magazines/scienceworld/issues/2015-16/090715/danger-zone/SW-090715-Skills-AcidOrBase.pdf";
	public static final String URL_SUBASSETS = "content/dam/classroom-magazines/scienceworld/issues/2015-16/090715/danger-zone/page1.pdf/subassets/SW-090715-Skills-AcidOrBase.pdf";
	
}
