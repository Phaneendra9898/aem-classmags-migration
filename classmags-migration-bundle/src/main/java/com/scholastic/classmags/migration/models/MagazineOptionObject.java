package com.scholastic.classmags.migration.models;

public class MagazineOptionObject {
	
	/** The Title. */
    private String optionDisplayTitle;
    
    /** The Magazine Image Path. */
    private String magazineImagePath;
    
    /** The Magazine Image Path Link. */
    private String magazineImagePathLink;
    
    /** The Button Title. */
    private String buttonTitle;
    
    /** The Button Color. */
    private String buttonColor;
    
    /** The Button Font Color. */
    private String buttonFontColor;
    
    /** The Button Redirect Link. */
    private String buttonRedirectLink;
    
    /**
     * Gets the title.
     * 
     * @return the title
     */
    public String getOptionDisplayTitle() {
		return optionDisplayTitle;
	}
    
    /**
     * Sets the title.
     * 
     * @param optionDisplayTitle
     *            the optionDisplayTitle to set
     */
	public void setOptionDisplayTitle(String optionDisplayTitle) {
		this.optionDisplayTitle = optionDisplayTitle;
	}
	
	/**
     * Gets the magazine image path.
     * 
     * @return the magazineImagePath
     */
	public String getMagazineImagePath() {
		return magazineImagePath;
	}
	
	/**
     * Sets the magazine image path.
     * 
     * @param magazineImagePath
     *            the magazineImagePath to set
     */
	public void setMagazineImagePath(String magazineImagePath) {
		this.magazineImagePath = magazineImagePath;
	}
	
	/**
     * Gets the magazine image path link.
     * 
     * @return the magazineImagePathLink
     */
	public String getMagazineImagePathLink() {
		return magazineImagePathLink;
	}
	
	/**
     * Sets the magazine image path link.
     * 
     * @param magazineImagePathLink
     *            the magazineImagePathLink to set
     */
	public void setMagazineImagePathLink(String magazineImagePathLink) {
		this.magazineImagePathLink = magazineImagePathLink;
	}
	
	/**
     * Gets the button title.
     * 
     * @return the buttonTitle
     */
	public String getButtonTitle() {
		return buttonTitle;
	}
	
	/**
     * Sets the button title.
     * 
     * @param buttonTitle
     *            the buttonTitle to set
     */
	public void setButtonTitle(String buttonTitle) {
		this.buttonTitle = buttonTitle;
	}
	
	/**
     * Gets the button color.
     * 
     * @return the buttonColor
     */
	public String getButtonColor() {
		return buttonColor;
	}
	
	/**
     * Sets the button color.
     * 
     * @param buttonColor
     *            the buttonColor to set
     */
	public void setButtonColor(String buttonColor) {
		this.buttonColor = buttonColor;
	}
	
	/**
     * Gets the button font color.
     * 
     * @return the buttonFontColor
     */
	public String getButtonFontColor() {
		return buttonFontColor;
	}
	
	/**
     * Sets the button font color.
     * 
     * @param buttonFontColor
     *            the buttonFontColor to set
     */
	public void setButtonFontColor(String buttonFontColor) {
		this.buttonFontColor = buttonFontColor;
	}
	
	/**
     * Gets the button redirect link.
     * 
     * @return the buttonRedirectLink
     */
	public String getButtonRedirectLink() {
		return buttonRedirectLink;
	}
	
	/**
     * Sets the button redirect link.
     * 
     * @param buttonRedirectLink
     *            the buttonRedirectLink to set
     */
	public void setButtonRedirectLink(String buttonRedirectLink) {
		this.buttonRedirectLink = buttonRedirectLink;
	}
    

}
