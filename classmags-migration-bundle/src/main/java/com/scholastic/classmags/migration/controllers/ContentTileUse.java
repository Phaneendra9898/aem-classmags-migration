package com.scholastic.classmags.migration.controllers;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.models.ContentTile;
import com.scholastic.classmags.migration.models.ContentTileFlags;
import com.scholastic.classmags.migration.models.ContentTileObject;
import com.scholastic.classmags.migration.services.ContentTileService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class ContentTileUse.
 */
public class ContentTileUse extends WCMUsePojo {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( ContentTileUse.class );

    /** The content tile. */
    private ContentTile contentTile;

    /** The content tile object. */
    private ContentTileObject contentTileObject;

    /** The content tile flags object. */
    private ContentTileFlags contentTileFlags;

    /** The content tile service. */
    private ContentTileService contentTileService;

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() throws Exception {
        SlingHttpServletRequest request = getRequest();
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        contentTileService = slingScriptHelper.getService( ContentTileService.class );
        if ( request != null ) {
            Resource currentResource = request.getResource();
            setContentTile( request.adaptTo( ContentTile.class ) );
            populateContentTileObject( currentResource.getPath() );
            LOG.info( "contentTileFlags::::::::" + contentTileFlags );
        }
        LOG.info( "contentTileObject::::::::" + contentTileObject );
    }

    /**
     * Populate content tile object.
     *
     * @param currentPath
     *            the current path
     */
    private void populateContentTileObject( String currentPath ) {
        switch ( contentTile.getContentTileContentType() ) {
        case ClassMagsMigrationConstants.ARTICLE:
            setContentTileObject( contentTileService.getArticleContent( contentTile, currentPath ) );
            checkContentTileArticlePagePath();
            break;
        case ClassMagsMigrationConstants.ISSUE:
            setContentTileObject( contentTileService.getIssueContent( contentTile, currentPath ) );
            checkContentTileArticlePagePath();
            break;
        case ClassMagsMigrationConstants.VIDEO:
            setContentTileObject( contentTileService.getVideoContent( contentTile, currentPath ) );
            checkContentTileArticlePagePath();
            break;
        case ClassMagsMigrationConstants.SKILLS_SHEET:
        case ClassMagsMigrationConstants.LESSON_PLAN:
            setContentTileObject( contentTileService.getAssetContent( contentTile, currentPath ) );
            checkContentTileArticlePagePath();
            break;
        case ClassMagsMigrationConstants.CONTENT_TYPE_CUSTOM_PAGE:
        case ClassMagsMigrationConstants.CONTENT_TYPE_CUSTOM_ASSET:
            setContentTileObject( contentTileService.getCustomContent( contentTile, currentPath ) );
            checkContentTileArticlePagePath();
            break;
        case ClassMagsMigrationConstants.GAME:
            setContentTileObject( contentTileService.getGameContent( contentTile, currentPath ) );
            checkContentTileArticlePagePath();
            break;
        default:
            break;
        }
    }

    /**
     * Check content tile article page path.
     */
    private void checkContentTileArticlePagePath() {

        if ( StringUtils.isNotBlank( contentTile.getContentTileArticlePagePath() ) ) {
            setContentTileFlags( true, contentTile.getContentTileArticlePagePath() );
        } else {
            setContentTileFlags( false, null );
        }
    }

    /**
     * Sets the content tile flags.
     *
     * @param isViewArticleEnabled
     *            the is view article enabled
     * @param articlePagePath
     *            the article page path
     */
    private void setContentTileFlags( boolean isViewArticleEnabled, String articlePagePath ) {

        contentTileFlags = new ContentTileFlags();
        if ( !StringUtils.equals( contentTile.getContentTileContentType(),
                ClassMagsMigrationConstants.CONTENT_TYPE_CUSTOM_PAGE )
                && !StringUtils.equals( contentTile.getContentTileContentType(),
                        ClassMagsMigrationConstants.CONTENT_TYPE_CUSTOM_ASSET ) ) {
            contentTileFlags.setDownloadEnabled( CommonUtils.isAShareableAsset( contentTile.getContentTileLink() ) );
        }
        contentTileFlags.setViewArticleEnabled( isViewArticleEnabled );
        contentTile.setContentTileArticlePagePath( articlePagePath );
    }

    /**
     * Gets the content tile.
     * 
     * @return the contentTile
     */
    public ContentTile getContentTile() {
        return contentTile;
    }

    /**
     * Gets the content tile object.
     * 
     * @return the contentTileObject
     */
    public ContentTileObject getContentTileObject() {
        return contentTileObject;
    }

    /**
     * Sets the content tile.
     * 
     * @param contentTile
     *            the contentTile to set
     */
    public void setContentTile( ContentTile contentTile ) {
        this.contentTile = contentTile;
    }

    /**
     * Sets the content tile object.
     * 
     * @param contentTileObject
     *            the contentTileObject to set
     */
    public void setContentTileObject( ContentTileObject contentTileObject ) {
        this.contentTileObject = contentTileObject;
    }

    /**
     * Gets the content tile flags.
     * 
     * @return the contentTileFlags
     */
    public ContentTileFlags getContentTileFlags() {
        return contentTileFlags;
    }

    /**
     * Sets the content tile flags.
     * 
     * @param contentTileFlags
     *            the contentTileFlags to set
     */
    public void setContentTileFlags( ContentTileFlags contentTileFlags ) {
        this.contentTileFlags = contentTileFlags;
    }
}
