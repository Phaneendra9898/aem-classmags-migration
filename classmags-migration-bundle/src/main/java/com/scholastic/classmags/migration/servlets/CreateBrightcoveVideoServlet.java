package com.scholastic.classmags.migration.servlets;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.services.CreateBrightcoveVideoService;
import com.scholastic.classmags.migration.services.impl.CreateBrightcoveVideoServiceImpl;

/**
 * The Class CreateBrightcoveVideoServlet.
 */
@SlingServlet( paths = { "/bin/classmags/migration/create/brightcove/video" } )
public class CreateBrightcoveVideoServlet extends SlingAllMethodsServlet {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2908031482310075493L;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( CreateBrightcoveVideoServlet.class );

    /** The Constant BRIGHTCOVE_VIDEO_ID. */
    private static final String BRIGHTCOVE_VIDEO_ID = "brightcoveVideoID";

    /** The Constant DAM_PATH. */
    private static final String DAM_PATH = "damPath";

    /** The Constant VIDEO_CREATION_STATUS. */
    private static final String VIDEO_CREATION_STATUS = "The video ID and DAM path MUST NOT BE EMPTY";

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /** The create video from brightcove service. */
    @Reference
    private CreateBrightcoveVideoService createVideoFromBrightcoveService;

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.apache.sling.api.servlets.SlingAllMethodsServlet#doPost(org.apache
     * .sling.api.SlingHttpServletRequest,
     * org.apache.sling.api.SlingHttpServletResponse)
     */
    @Override
    protected void doPost( SlingHttpServletRequest request, SlingHttpServletResponse response ) {

        try {

            String videoCreationStatus;
            String brightcoveVideoID = request.getParameter( BRIGHTCOVE_VIDEO_ID );
            String damPath = request.getParameter( DAM_PATH );
            LOG.debug( "brightcoveVideoID is " + brightcoveVideoID + " and damPath is " + damPath );

            if ( StringUtils.isBlank( brightcoveVideoID ) && StringUtils.isBlank( damPath ) ) {
                response.setStatus( HttpServletResponse.SC_BAD_REQUEST );
                videoCreationStatus = VIDEO_CREATION_STATUS;
            } else {
                Map< String, String > responseMessage = createVideoFromBrightcoveService.createVideoinAEM(
                        brightcoveVideoID, damPath );
                if ( responseMessage.containsKey( CreateBrightcoveVideoServiceImpl.ERROR_CREATING_VIDEO_IN_AEM ) ) {
                    videoCreationStatus = responseMessage
                            .get( CreateBrightcoveVideoServiceImpl.ERROR_CREATING_VIDEO_IN_AEM );
                    response.setStatus( HttpServletResponse.SC_NOT_FOUND );
                } else {
                    videoCreationStatus = responseMessage
                            .get( CreateBrightcoveVideoServiceImpl.SUCCESS_CREATING_VIDEO_IN_AEM );
                }
            }
            response.getWriter().write( videoCreationStatus );
        } catch ( Exception e ) {
            LOG.error( "Error in CreateBrightcoveVideoServlet: " + e );
        }
    }
}
