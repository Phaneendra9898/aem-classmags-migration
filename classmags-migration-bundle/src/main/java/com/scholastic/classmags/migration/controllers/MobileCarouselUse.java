package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.List;

import com.adobe.cq.sightly.WCMUsePojo;

public class MobileCarouselUse extends WCMUsePojo {
	
	/** The carousel image list. */
	private List<String> mobileCarouselImages;
	

	@Override
	public void activate() throws Exception {
		
		if(getProperties().get("carouselImagePath", String[].class) != null){
		String[] imagePath = getProperties().get("carouselImagePath", String[].class);
		mobileCarouselImages = new ArrayList<>();
		for(String mobileImagePath : imagePath){
			mobileCarouselImages.add(mobileImagePath);
		}
	}
		
		
	}

	/**
     * Gets the mobile image list.
     *
     * @return the mobileCarouselImages
     */
	public List<String> getMobileCarouselImages() {
		return mobileCarouselImages;
	}

	/**
     * Sets the mobile carousel image list.
     *
     *@param mobileCarouselImages
     *	sets the mobileCarouselImages
     */
	public void setMobileCarouselImages(List<String> mobileCarouselImages) {
		this.mobileCarouselImages = mobileCarouselImages;
	}
	
}
