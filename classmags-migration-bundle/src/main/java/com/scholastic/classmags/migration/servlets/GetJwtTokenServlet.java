package com.scholastic.classmags.migration.servlets;

import java.io.IOException;

import javax.servlet.ServletException;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.models.UserIdCookieData;
import com.scholastic.classmags.migration.services.JwtConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.CookieUtil;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * Servlet for getting JWT token.
 * 
 */
@SlingServlet( paths = "/bin/classmags/getjwttoken", methods = "GET" )
public class GetJwtTokenServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = 12L;

    private static final Logger LOG = LoggerFactory.getLogger( GetJwtTokenServlet.class );

    @Reference
    JwtConfigService jwtConfigService;

    /**
     * Method doGet
     */
    @Override
    public void doGet( SlingHttpServletRequest request, SlingHttpServletResponse response )
            throws ServletException, IOException {
        JSONObject jwtToken = new JSONObject();
        String token;
        String secretKey = jwtConfigService.getSecretKey();
        String issuer = jwtConfigService.getIssuer();
        try {
            String userRole = RoleUtil.getUserRole( request );
            String userId = StringUtils.EMPTY;
            UserIdCookieData userIdCookie = CookieUtil.fetchUserIdCookieData( request );
            if ( !StringUtils.equals( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS, userRole )
                    && null != userIdCookie ) {
                if ( StringUtils.equals( ClassMagsMigrationConstants.USER_TYPE_STUDENT, userRole ) ) {
                    userId = userIdCookie.getObjUIdHeaderIdentifiers().getStudentId();
                } else {
                    userId = userIdCookie.getObjUIdHeaderIdentifiers().getStaffId();
                }
                token = CommonUtils.createJwtToken( userId, issuer, secretKey );

            } else {
                String randomKey = CommonUtils.generateUniqueIdentifier();
                token = CommonUtils.createJwtToken( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS, issuer,
                        randomKey );
            }
            jwtToken.put( "accessToken", token );

        } catch ( Exception e ) {
            LOG.error( "Error creating token {}", e );
            response.sendError( SlingHttpServletResponse.SC_BAD_REQUEST );
        } finally {
            LOG.debug( "Token is {}", jwtToken );
            response.setContentType( "application/json" );
            response.setCharacterEncoding( "UTF-8" );
            response.getWriter().write( jwtToken.toString() );
        }

    }

}