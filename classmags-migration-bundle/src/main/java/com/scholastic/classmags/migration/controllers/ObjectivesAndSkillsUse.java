package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.models.ObjectivesAndSkills;
import com.scholastic.classmags.migration.utils.CommonUtils;

public class ObjectivesAndSkillsUse extends WCMUsePojo {
    private List< ObjectivesAndSkills > objectivesAndSkillsList;

    public List< ObjectivesAndSkills > getObjectivesAndSkillsList() {
        return objectivesAndSkillsList;
    }

    public void activate() {

        objectivesAndSkillsList = new ArrayList<>();

        List< ValueMap > objectivesAndSkillsMapList = CommonUtils.fetchMultiFieldData( getResource(),
                "objectivesAndSkills" );
        populateObjectivesAndSkills( objectivesAndSkillsMapList );

    }

    void populateObjectivesAndSkills( List< ValueMap > objectivesAndSkillsMapList ) {

        for ( ValueMap objectivesAndSkillsValueMap : objectivesAndSkillsMapList ) {
            ObjectivesAndSkills objectivesAndSkills = new ObjectivesAndSkills();

            objectivesAndSkills.setTitle( objectivesAndSkillsValueMap.get( "title", StringUtils.EMPTY ) );
            objectivesAndSkills.setDescription( objectivesAndSkillsValueMap.get( "description", StringUtils.EMPTY ) );

            objectivesAndSkillsList.add( objectivesAndSkills );

        }
    }
}
