package com.scholastic.classmags.migration.models;

/**
 * The Class CaptionSource.
 */
public class CaptionSource {

    /** The id. */
    private long id;

    /** The url. */
    private String url;

    /** The display name. */
    private String displayName;

    /** The is remote. */
    private boolean isRemote;

    /** The complete. */
    private boolean complete;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the new id
     */
    public void setId( long id ) {
        this.id = id;
    }

    /**
     * Gets the url.
     * 
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the url.
     * 
     * @param url
     *            the new url
     */
    public void setUrl( String url ) {
        this.url = url;
    }

    /**
     * Gets the display name.
     * 
     * @return the display name
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * Sets the display name.
     * 
     * @param displayName
     *            the new display name
     */
    public void setDisplayName( String displayName ) {
        this.displayName = displayName;
    }

    /**
     * Checks if is remote.
     * 
     * @return true, if is remote
     */
    public boolean isRemote() {
        return isRemote;
    }

    /**
     * Sets the remote.
     * 
     * @param isRemote
     *            the new remote
     */
    public void setRemote( boolean isRemote ) {
        this.isRemote = isRemote;
    }

    /**
     * Checks if is complete.
     * 
     * @return true, if is complete
     */
    public boolean isComplete() {
        return complete;
    }

    /**
     * Sets the complete.
     * 
     * @param complete
     *            the new complete
     */
    public void setComplete( boolean complete ) {
        this.complete = complete;
    }

}
