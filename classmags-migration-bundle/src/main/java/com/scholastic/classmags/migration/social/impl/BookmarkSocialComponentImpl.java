package com.scholastic.classmags.migration.social.impl;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.apache.sling.xss.XSSAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.social.scf.ClientUtilities;
import com.adobe.cq.social.scf.core.BaseSocialComponent;
import com.adobe.cq.social.ugc.api.SearchResults;
import com.adobe.cq.social.ugc.api.UgcFilter;
import com.adobe.cq.social.ugc.api.UgcSearch;
import com.adobe.cq.social.ugc.api.ValueConstraint;
import com.adobe.cq.social.ugcbase.SocialUtils;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.BookmarkData;
import com.scholastic.classmags.migration.models.ClassmagsMigrationErrorCodes;
import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.MyBookmarksDataService;
import com.scholastic.classmags.migration.social.api.BookmarkSocialComponent;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * The Class BookmarkSocialComponentImpl.
 */
@SuppressWarnings( "deprecation" )
public class BookmarkSocialComponentImpl extends BaseSocialComponent implements BookmarkSocialComponent {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( BookmarkSocialComponentImpl.class );

    /** The xssapi. */
    private XSSAPI xssapi;

    /** The ugc search. */
    private UgcSearch ugcSearch;

    /** The bookmark enabled. */
    private boolean bookmarkEnabled;

    /** The asset paths. */
    private List< String > assetPaths;

    /** The page paths. */
    private List< String > pagePaths;

    /** The my bookmarks data service. */
    private MyBookmarksDataService myBookmarksDataService;

    /** The magazine props service. */
    private MagazineProps magazinePropsService;

    /** The my bookmarks page data. */
    private List< ResourcesObject > myBookmarksPageData;

    /** The my bookmarks asset data. */
    private List< ResourcesObject > myBookmarksAssetData;

    /** The magazines name map. */
    private Map< String, String > magazinesNameMap = new HashMap< >();

    /**
     * Instantiates a new bookmark social component impl.
     *
     * @param resource
     *            the resource
     * @param clientUtils
     *            the client utils
     * @param ugcSearch
     *            the ugc search
     * @param xssapi
     *            the xssapi
     * @param myBookmarksDataService
     *            the my bookmarks data service
     * @param magazinePropsService
     *            the magazine props service
     */
    public BookmarkSocialComponentImpl( Resource resource, ClientUtilities clientUtils, UgcSearch ugcSearch,
            XSSAPI xssapi, MyBookmarksDataService myBookmarksDataService, MagazineProps magazinePropsService ) {

        super( resource, clientUtils );
        this.xssapi = xssapi;
        this.ugcSearch = ugcSearch;
        this.myBookmarksDataService = myBookmarksDataService;
        this.magazinePropsService = magazinePropsService;

        final String operation = xssapi
                .filterHTML( this.clientUtils.getRequest().getParameter( ClassMagsMigrationASRPConstants.OPERATION ) );

        try {
            switch ( operation ) {
            case ClassMagsMigrationASRPConstants.SOCIAL_GET_ALL_SITE_BOOKMARK_OPERATION:
                performGetAllSiteBookmarkOperationTasks( resource );
                break;
            case ClassMagsMigrationASRPConstants.SOCIAL_GET_MY_BOOKMARKS_OPERATION:
                populateBookmarksData( resource );
                break;
            case ClassMagsMigrationASRPConstants.SOCIAL_ADD_BOOKMARK_OPERATION:
            case ClassMagsMigrationASRPConstants.SOCIAL_DELETE_BOOKMARK_OPERATION:
                setBookmarkEnabled( checkBookmarkFlag( resource ) );
                break;
            default:
                break;
            }
        } catch ( ClassmagsMigrationBaseException e ) {
            LOG.error( e.getMessage(), e );
        }
    }

    /**
     * Perform get all site bookmark operation tasks.
     *
     * @param resource
     *            the resource
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private void performGetAllSiteBookmarkOperationTasks( Resource resource ) throws ClassmagsMigrationBaseException {

        assetPaths = new ArrayList< >();
        pagePaths = new ArrayList< >();
        setBookmarkEnabled( checkPageBookmarkFlag( resource ) );
        setAssetPaths(
                populateBookmarkPaths( resource, ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_ASSET, assetPaths ) );
        setPagePaths(
                populateBookmarkPaths( resource, ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_PAGE, pagePaths ) );
    }

    /**
     * Populate bookmarks data.
     *
     * @param resource
     *            the resource
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private void populateBookmarksData( Resource resource ) throws ClassmagsMigrationBaseException {

        int searchOffset = ClassMagsMigrationASRPConstants.SOCIAL_SEARCH_OFFSET;
        int searchLimit = ClassMagsMigrationASRPConstants.SOCIAL_SEARCH_INTIAL_LIMIT;
        Long resultsCount;
        List< BookmarkData > allPageBookmarks = new ArrayList< >();
        List< BookmarkData > allAssetBookmarks = new ArrayList< >();
        SearchResults< Resource > results = null;
        final UgcFilter filter = new UgcFilter();
        String userRole = RoleUtil.getUserRole( this.clientUtils.getRequest() );

        filter.addConstraint( new ValueConstraint< String >( JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY,
                BookmarkSocialComponent.BOOKMARK_RESOURCE_TYPE ) );
        filter.addConstraint( new ValueConstraint< String >( SocialUtils.PN_PARENTID, resource.getPath() ) );

        try {
            results = this.ugcSearch.find( null, resource.getResourceResolver(), filter, searchOffset, searchLimit,
                    true );
            processResults( allPageBookmarks, allAssetBookmarks, results );
            resultsCount = results.getTotalNumberOfResults();

            if ( searchLimit < resultsCount ) {
                searchOffset = searchLimit;
                int newLimit = ( int ) ( resultsCount - searchLimit );
                results = this.ugcSearch.find( null, resource.getResourceResolver(), filter, searchOffset, newLimit,
                        true );
                processResults( allPageBookmarks, allAssetBookmarks, results );
            }
        } catch ( RepositoryException e ) {
            throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.REPOSITORY_ERROR, e );
        }
        setMyBookmarksPageData( myBookmarksDataService.getMyBookmarksPageData( allPageBookmarks, userRole ) );
        setMyBookmarksAssetData( myBookmarksDataService.getMyBookmarksAssetData( allAssetBookmarks, userRole ) );
    }

    /**
     * Process results.
     *
     * @param allPageBookmarks
     *            the all page bookmarks
     * @param allAssetBookmarks
     *            the all asset bookmarks
     * @param results
     *            the results
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private void processResults( List< BookmarkData > allPageBookmarks, List< BookmarkData > allAssetBookmarks,
            SearchResults< Resource > results ) throws ClassmagsMigrationBaseException {

        for ( Resource ugcResource : results.getResults() ) {
            populateMagazinesMap( ugcResource );
            populateBookmarkData( ugcResource, ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_PAGE, allPageBookmarks );
            populateBookmarkData( ugcResource, ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_ASSET, allAssetBookmarks );
        }
    }

    /**
     * Populate magazines map.
     *
     * @param ugcResource
     *            the ugc resource
     */
    private void populateMagazinesMap( Resource ugcResource ) {
        magazinesNameMap.put( ugcResource.getName(),
                magazinePropsService.getMagazineActualName( ugcResource.getName() ) );
    }

    /**
     * Populate bookmark paths.
     *
     * @param ugcResource
     *            the ugc resource
     * @param bookmarkType
     *            the bookmark type
     * @param bookmarkList
     *            the bookmark list
     * @return the list
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private List< String > populateBookmarkPaths( Resource ugcResource, String bookmarkType,
            List< String > bookmarkList ) throws ClassmagsMigrationBaseException {

        List< BookmarkData > bookmarkDataList = new ArrayList< >();
        populateBookmarkData( ugcResource, bookmarkType, bookmarkDataList );
        for ( BookmarkData bookmarkData : bookmarkDataList ) {
            bookmarkList.add( bookmarkData.getBookmarkPath() );
        }
        return bookmarkList;
    }

    /**
     * Populate bookmark data.
     *
     * @param ugcResource
     *            the ugc resource
     * @param bookmarkType
     *            the bookmark type
     * @param bookmarkDataList
     *            the bookmark data list
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private void populateBookmarkData( Resource ugcResource, String bookmarkType,
            List< BookmarkData > bookmarkDataList ) throws ClassmagsMigrationBaseException {

        ModifiableValueMap propertyMap = ugcResource.adaptTo( ModifiableValueMap.class );
        LOG.info( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.UGC_RESOURCE_PROPERTIES + propertyMap ) );

        if ( null != propertyMap && !propertyMap.isEmpty() ) {
            for ( String propertyKey : propertyMap.keySet() ) {
                if ( propertyKey.startsWith( bookmarkType ) ) {
                    String jsonString = propertyMap.get( propertyKey ).toString();
                    try {
                        JSONObject bookmarkJsonObject = new JSONObject( jsonString );
                        BookmarkData bookmarkData = new BookmarkData();
                        bookmarkData.setBookmarkPath( bookmarkJsonObject
                                .optString( ClassMagsMigrationASRPConstants.JSON_KEY_BOOKMARK_PATH ) );
                        bookmarkData.setDateSaved(
                                bookmarkJsonObject.optString( ClassMagsMigrationASRPConstants.JSON_KEY_DATE_SAVED ) );
                        bookmarkData.setViewArticleLink( bookmarkJsonObject
                                .optString( ClassMagsMigrationASRPConstants.JSON_KEY_VIEW_ARTICLE_LINK ) );
                        bookmarkData.setMagazineType( ugcResource.getName() );
                        bookmarkDataList.add( bookmarkData );
                    } catch ( JSONException e ) {
                        throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.JSON_CONVERSION_ERROR,
                                e );
                    }
                }
            }
        }
    }

    /**
     * Check page bookmark flag.
     *
     * @param ugcResource
     *            the ugc resource
     * @return true, if successful
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private boolean checkPageBookmarkFlag( Resource ugcResource ) throws ClassmagsMigrationBaseException {

        final String currentPagePath = xssapi.filterHTML(
                this.clientUtils.getRequest().getParameter( ClassMagsMigrationASRPConstants.BOOKMARK_PAGE_PATH ) );

        return processBookmarkFlagData( ugcResource, currentPagePath,
                ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_PAGE );
    }

    /**
     * Check bookmark flag.
     *
     * @param ugcResource
     *            the ugc resource
     * @return true, if successful
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private boolean checkBookmarkFlag( Resource ugcResource ) throws ClassmagsMigrationBaseException {

        final String bookmarkPagePath = xssapi.filterHTML(
                this.clientUtils.getRequest().getParameter( ClassMagsMigrationASRPConstants.BOOKMARK_PAGE_PATH ) );

        final String bookmarkedTeachingResourcePath = xssapi.filterHTML( this.clientUtils.getRequest()
                .getParameter( ClassMagsMigrationASRPConstants.BOOKMARK_TEACHING_RESOURCE_PATH ) );

        if ( StringUtils.isNotBlank( bookmarkPagePath ) ) {
            return processBookmarkFlagData( ugcResource, bookmarkPagePath,
                    ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_PAGE );
        } else {
            return processBookmarkFlagData( ugcResource, bookmarkedTeachingResourcePath,
                    ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_ASSET );
        }
    }

    /**
     * Process bookmark flag data.
     *
     * @param ugcResource
     *            the ugc resource
     * @param bookmarkPath
     *            the bookmark path
     * @param bookmarkType
     *            the bookmark type
     * @return true, if successful
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private boolean processBookmarkFlagData( Resource ugcResource, final String bookmarkPath, String bookmarkType )
            throws ClassmagsMigrationBaseException {

        ModifiableValueMap propertyMap = ugcResource.adaptTo( ModifiableValueMap.class );
        LOG.info( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.UGC_RESOURCE_PROPERTIES + propertyMap ) );

        try {
            return StringUtils.isNotBlank( bookmarkPath ) && null != propertyMap
                    && ( propertyMap.containsKey( bookmarkType.concat( SocialASRPUtils.urlEncoder( bookmarkPath ) ) ) );
        } catch ( UnsupportedEncodingException e ) {
            throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.UNSUPPORTED_ENCODING_ERROR, e );
        }
    }

    /**
     * Gets the asset paths.
     *
     * @return the assetPaths
     */
    @Override
    public List< String > getAssetPaths() {
        return assetPaths;
    }

    /**
     * Sets the asset paths.
     *
     * @param assetPaths
     *            the assetPaths to set
     */
    public void setAssetPaths( List< String > assetPaths ) {
        this.assetPaths = assetPaths;
    }

    /**
     * Gets the my bookmarks page data.
     *
     * @return the myBookmarksPageData
     */
    @Override
    public List< ResourcesObject > getMyBookmarksPageData() {
        return myBookmarksPageData;
    }

    /**
     * Sets the my bookmarks page data.
     *
     * @param myBookmarksPageData
     *            the myBookmarksPageData to set
     */
    public void setMyBookmarksPageData( List< ResourcesObject > myBookmarksPageData ) {
        this.myBookmarksPageData = myBookmarksPageData;
    }

    /**
     * Gets the my bookmarks asset data.
     *
     * @return the myBookmarksAssetData
     */
    @Override
    public List< ResourcesObject > getMyBookmarksAssetData() {
        return myBookmarksAssetData;
    }

    /**
     * Sets the my bookmarks asset data.
     *
     * @param myBookmarksAssetData
     *            the myBookmarksAssetData to set
     */
    public void setMyBookmarksAssetData( List< ResourcesObject > myBookmarksAssetData ) {
        this.myBookmarksAssetData = myBookmarksAssetData;
    }

    /**
     * Gets the magazines name map.
     *
     * @return the magazinesNameMap
     */
    @Override
    public Map< String, String > getMagazinesNameMap() {
        return magazinesNameMap;
    }

    /**
     * Gets the page paths.
     *
     * @return the pagePaths
     */
    @Override
    public List< String > getPagePaths() {
        return pagePaths;
    }

    /**
     * Sets the page paths.
     *
     * @param pagePaths
     *            the pagePaths to set
     */
    public void setPagePaths( List< String > pagePaths ) {
        this.pagePaths = pagePaths;
    }

    /**
     * Checks if is bookmark enabled.
     *
     * @return the bookmarkEnabled
     */
    @Override
    public boolean isBookmarkEnabled() {
        return bookmarkEnabled;
    }

    /**
     * Sets the bookmark enabled.
     *
     * @param bookmarkEnabled
     *            the bookmarkEnabled to set
     */
    public void setBookmarkEnabled( boolean bookmarkEnabled ) {
        this.bookmarkEnabled = bookmarkEnabled;
    }
}
