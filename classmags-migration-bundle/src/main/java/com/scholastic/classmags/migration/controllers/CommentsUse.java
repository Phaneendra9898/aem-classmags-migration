package com.scholastic.classmags.migration.controllers;

import org.apache.sling.api.scripting.SlingScriptHelper;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.commons.Externalizer;
import com.livefyre.Livefyre;
import com.livefyre.core.Collection;
import com.livefyre.core.Network;
import com.livefyre.core.Site;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class CommentsUse.
 */
public class CommentsUse extends WCMUsePojo {

    private String networkKey;
    private String networkName;
    private String siteId;
    private String siteKey;
    private String collectionCheckSum;
    private String collectionMetaToken;

    @Override
    public void activate() throws Exception {

        String currentPath = getCurrentPage().getPath();

        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();

        if ( null != slingScriptHelper ) {

            MagazineProps magazineProps = slingScriptHelper.getService( MagazineProps.class );
            PropertyConfigService propertyConfigService = slingScriptHelper.getService( PropertyConfigService.class );
            ResourcePathConfigService resourcePathConfigService = slingScriptHelper
                    .getService( ResourcePathConfigService.class );
            Externalizer externalizer = slingScriptHelper.getService( Externalizer.class );

            if ( null != magazineProps && null != propertyConfigService && null != resourcePathConfigService
                    && null != externalizer ) {

                String magazineName = magazineProps.getMagazineName( currentPath );

                String dataPagePath = CommonUtils.getDataPath( resourcePathConfigService, magazineName );

                networkName = propertyConfigService.getDataConfigProperty( "livefyreNetworkName", dataPagePath );
                networkKey = propertyConfigService.getDataConfigProperty( "livefyreNetworkKey", dataPagePath );
                siteId = propertyConfigService.getDataConfigProperty( "livefyreSiteId", dataPagePath );
                siteKey = propertyConfigService.getDataConfigProperty( "livefyreSiteKey", dataPagePath );

                Network network = Livefyre.getNetwork( networkName, networkKey );

                Site site = network.getSite( siteId, siteKey );

                String externalizedLink = externalizer.externalLink( getResourceResolver(), magazineName, currentPath );
                Collection collection = site.buildCommentsCollection( currentPath, getResource().getPath(),
                        externalizedLink );

                collectionMetaToken = collection.buildCollectionMetaToken();
                collectionCheckSum = collection.buildChecksum();
            }
        }
    }

    public String getArticleId() {
        return "'" + getResource().getPath() + "'";
    }

    public String getNetworkName() {
        return "'" + networkName + "'";
    }

    public String getSiteId() {
        return "'" + siteId + "'";
    }

    public String getMetaToken() {
        return "'" + collectionMetaToken + "'";
    }

    public String getCheckSum() {
        return "'" + collectionCheckSum + "'";
    }
}