package com.scholastic.classmags.migration.services;

import java.io.InputStream;

/**
 * The Interface UploadGameService.
 */
public interface UploadGameService {

    /**
     * Upload game.
     * 
     * @param gameFile
     *            the game file
     * @param damPath
     *            the dam path
     * @param gameName
     *            the game name
     * @return the string
     */
    public String uploadGame( InputStream gameFile, String damPath, String gameName );

}
