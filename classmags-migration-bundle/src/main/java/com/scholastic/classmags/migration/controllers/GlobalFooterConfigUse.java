package com.scholastic.classmags.migration.controllers;

import java.util.List;

import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.utils.CommonUtils;

public class GlobalFooterConfigUse extends WCMUsePojo {

    List< ValueMap > footerLinks;

    @Override
    public void activate() {

        footerLinks = CommonUtils.fetchMultiFieldData( getResource(), "footer-links" );

    }

    public List< ValueMap > getFooterLinks() {
        return footerLinks;
    }

}
