package com.scholastic.classmags.migration.controllers;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.RoleUtil;

public class EReaderUse extends WCMUsePojo {

    private static final String EREADER_PROPERTY = "linkToEReaderNew";
    private static final String EREADER_USER_TYPE = "userType";

    private String newEReaderpath;
    private Boolean showMagazineLink;
    private String ePubISBN;

    @Override
    public void activate() {
        Page page = getCurrentPage();
        if ( page != null ) {
            Resource resource = page.getContentResource().getChild( ClassMagsMigrationConstants.EREADER_CONFIG );
            if ( resource != null ) {
                newEReaderpath = resource.getValueMap().get( EREADER_PROPERTY, StringUtils.EMPTY );
                String loginRole = ( String ) RoleUtil.getUserRole( getRequest() );
                String accessibleTo = resource.getValueMap().get( EREADER_USER_TYPE, StringUtils.EMPTY );
                showMagazineLink = RoleUtil.shouldRender( loginRole, accessibleTo );
                if ( !newEReaderpath.isEmpty() ) {
                    ePubISBN = StringUtils.substringBetween( newEReaderpath, "epub/", "/" );
                }
            }
        }
    }

    /*
     * Gets the New E-reader path.
     * 
     * @return The new E-reader path.
     */
    public String getNewEReaderPath() {
        return newEReaderpath;
    }

    /*
     * Gets if magazine link is to be shown.
     * 
     * @return The show magazine link flag.
     */
    public Boolean getShowMagazineLink() {
        return showMagazineLink;
    }
    
    /*
     * Gets the ePub ISBN.
     * 
     * @return The ePub ISBN.
     */
    public String getEPubISBN() {
        return ePubISBN;
    }

}
