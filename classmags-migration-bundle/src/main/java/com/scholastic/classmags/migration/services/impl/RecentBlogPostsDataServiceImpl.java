package com.scholastic.classmags.migration.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.ClassmagsMigrationErrorCodes;
import com.scholastic.classmags.migration.models.RecentBlogPostsData;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.RecentBlogPostsDataService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class RecentBlogPostsDataServiceImpl.
 */
@Component( immediate = true, metatype = false )
@Service( RecentBlogPostsDataService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Recent Blog Posts Data Service" ) } )
public class RecentBlogPostsDataServiceImpl implements RecentBlogPostsDataService {

    /** The resource resolver factory. */
    @Reference
    ResourceResolverFactory resourceResolverFactory;

    /** The blog posts path list. */
    List< ValueMap > blogPostsPathList;

    /** The property config service. */
    @Reference
    PropertyConfigService propertyConfigService;

    /** The resource path config service. */
    @Reference
    private ResourcePathConfigService resourcePathConfigService;

    /** The magazine props. */
    @Reference
    private MagazineProps magazineProps;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.scholastic.classmags.migration.services.RecentBlogPostsDataService#
     * fetchRecentBlogPostsData(java.lang.String)
     */
    @Override
    public List< RecentBlogPostsData > fetchRecentBlogPostsData( String currentNodePath )
            throws ClassmagsMigrationBaseException {

        List< RecentBlogPostsData > recentBlogPostsDataList = new ArrayList<>();

        ResourceResolver resourceResolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );

        Resource recentBlogPostsResource = resourceResolver.getResource( currentNodePath );

        blogPostsPathList = CommonUtils.fetchMultiFieldData( recentBlogPostsResource,
                ClassMagsMigrationConstants.BLOG_POSTS_PATH_NODE );

        for ( ValueMap blogPostsPathMap : blogPostsPathList ) {
            try {
                populateRecentBlogPostsData( blogPostsPathMap, recentBlogPostsDataList );
            } catch ( LoginException e ) {
                throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.LOGIN_ERROR, e );
            }
        }
        return recentBlogPostsDataList;
    }

    /**
     * Populate recent blog posts data.
     *
     * @param blogPostsPathMap
     *            the blog posts path map
     * @param recentBlogPostsDataList
     *            the recent blog posts data list
     * @throws LoginException
     *             the login exception
     */
    private void populateRecentBlogPostsData( ValueMap blogPostsPathMap,
            List< RecentBlogPostsData > recentBlogPostsDataList ) throws LoginException {

        String blogPagePath = blogPostsPathMap.get( ClassMagsMigrationConstants.BLOG_PATH_PROPERTY, StringUtils.EMPTY );
        String magazineName = magazineProps.getMagazineName( blogPagePath );
        ValueMap blogPagePropertyMap = CommonUtils.fetchPagePropertyMap( blogPagePath, resourceResolverFactory );

        RecentBlogPostsData recentBlogPostsData = new RecentBlogPostsData();
        recentBlogPostsData
                .setBlogPageTitle( blogPagePropertyMap.get( ClassMagsMigrationConstants.PP_TITLE, StringUtils.EMPTY ) );

        recentBlogPostsData
                .setBlogPageDate( CommonUtils.getDisplayDate( blogPagePropertyMap,
                        propertyConfigService.getDataConfigProperty(
                                ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT, CommonUtils
                                        .getDataPath( resourcePathConfigService, magazineName ) ) ) );

        recentBlogPostsDataList.add( recentBlogPostsData );
    }
}
