package com.scholastic.classmags.migration.services;

/**
 * The Interface MagazineProps.
 */
public interface MagazineProps {

    /**
     * Gets the magazine name.
     * 
     * @param currentPagePath
     *            the current page path
     * @return the magazine name
     */
    String getMagazineName( final String currentPagePath );

    /**
     * Gets the magazine actual name.
     *
     * @param magazineNameKey
     *            the magazine name key
     * @return the magazine actual name
     */
    public String getMagazineActualName( final String magazineNameKey );
}
