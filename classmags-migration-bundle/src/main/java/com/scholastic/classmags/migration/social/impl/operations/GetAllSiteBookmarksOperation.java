package com.scholastic.classmags.migration.social.impl.operations;

import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.servlets.post.PostOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.social.scf.OperationException;
import com.adobe.cq.social.scf.SocialComponent;
import com.adobe.cq.social.scf.SocialComponentFactoryManager;
import com.adobe.cq.social.scf.SocialOperationResult;
import com.adobe.cq.social.scf.core.operations.AbstractSocialOperation;
import com.scholastic.classmags.migration.social.api.BookmarkService;
import com.scholastic.classmags.migration.social.api.BookmarkSocialComponent;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;

/**
 * A POST Endpoint for GetAllSiteBookmarksOperation that accepts requests for
 * getting all bookmarks available for a particular site on page load. This
 * class responds to all POST requests with a
 * :operation=social:bookmark:getAllSite parameter. For example, curl
 * http://localhost:4502/content/classroom-magazines/testissuepage/_jcr_content/
 * par/bookmark.social.json -uadmin:admin -v POST -H "Accept:application/json"
 * --data ":operation=social:bookmark:getAllSite
 * &bookmarkPagePath=/content/test_folder/scienceworld/issues/2016-17/090516/
 * invisible-train.html &appName=scienceworld"
 */
@Component( immediate = true )
@Service
@Property( name = PostOperation.PROP_OPERATION_NAME, value = ClassMagsMigrationASRPConstants.SOCIAL_GET_ALL_SITE_BOOKMARK_OPERATION )
public class GetAllSiteBookmarksOperation extends AbstractSocialOperation {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( GetAllSiteBookmarksOperation.class );

    /** The bookmark service. */
    @Reference
    private BookmarkService bookmarkService;

    /** The scf manager. */
    @Reference
    private SocialComponentFactoryManager scfManager;

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.social.scf.core.operations.AbstractSocialOperation#
     * performOperation(org.apache.sling.api.SlingHttpServletRequest)
     */
    @Override
    protected SocialOperationResult performOperation( SlingHttpServletRequest slingHttpServletRequest )
            throws OperationException {

        LOG.info( ClassMagsMigrationASRPConstants.OPERATION_START
                + ClassMagsMigrationASRPConstants.SOCIAL_GET_ALL_SITE_BOOKMARK_OPERATION );

        final Resource bookmarkResource = this.bookmarkService.getAllSiteBookmarkResource( slingHttpServletRequest );

        String resourcePath;
        int httpStatusCode = HttpServletResponse.SC_OK;
        String httpStatusMessage = ClassMagsMigrationASRPConstants.HTTP_SUCCESS_MESSAGE;

        if ( null == bookmarkResource ) {
            resourcePath = slingHttpServletRequest.getResource().getPath();
        } else {
            resourcePath = bookmarkResource.getPath();
        }

        SocialComponent socialComponent = SocialASRPUtils.getSocialComponent( slingHttpServletRequest, bookmarkResource,
                this.scfManager, BookmarkSocialComponent.BOOKMARK_RESOURCE_TYPE );

        LOG.info( ClassMagsMigrationASRPConstants.OPERATION_END );

        return new SocialOperationResult( socialComponent, httpStatusMessage, httpStatusCode, resourcePath );
    }
}
