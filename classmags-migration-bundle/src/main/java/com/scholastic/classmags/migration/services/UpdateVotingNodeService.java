package com.scholastic.classmags.migration.services;

import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;

/**
 * The Interface UpdateVotingNodeService.
 */
public interface UpdateVotingNodeService {

    /**
     * Updates the voting node.
     *
     * @param resource The resource. 
     * 
     * @throws PersistenceException 
     */
    public void updateVotingNode( Resource resource ) throws PersistenceException;

}
