package com.scholastic.classmags.migration.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.scholastic.classmags.migration.models.TagModel;
import com.scholastic.classmags.migration.services.SubjectsCoveredService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class SubjectsCoveredServiceImpl.
 */
@Component( immediate = true, metatype = false )
@Service( SubjectsCoveredService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Subjects Covered Service" ) })
public class SubjectsCoveredServiceImpl implements SubjectsCoveredService {

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;
    
    @Override
    public Map< TagModel, List< TagModel > > getSubjectsCovered( Resource resource, String currentPath,
            String magazineName ) {
        ResourceResolver resourceResolver = resource.getResourceResolver();
        TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );
        Resource jcrNodeResource = null;
        Map< TagModel, List< TagModel > > subjectsMap = new HashMap< >();
        if ( null != resourceResolver ) {
            jcrNodeResource = resourceResolver.getResource( currentPath + "/" + JcrConstants.JCR_CONTENT );
        }
        if ( null != jcrNodeResource ) {
            Tag[] tags = tagManager.getTags( jcrNodeResource );
            Tag[] magazineSpecificTags = getMagazineSpecificTags( tags, magazineName );
            subjectsMap = createSubjectsMap( tagManager, magazineSpecificTags );
        }
        return subjectsMap;
    }

    /**
     * Gets magazine specific tags.
     * 
     * @param tags The tags.
     * @param magazineName The magazine name.
     * 
     * @return magazineSpecificTags The magazine specific tags.
     */
    private Tag[] getMagazineSpecificTags( Tag[] tags, String magazineName ) {
        ArrayList< Tag > magazineSpecificTags = new ArrayList< >();
        for ( Tag tag : tags ) {
            if ( StringUtils.containsIgnoreCase( tag.getPath(), magazineName ) ) {
                magazineSpecificTags.add( tag );
            }
        }
        return magazineSpecificTags.toArray( new Tag[magazineSpecificTags.size()] );
    }

    /**
     * Create subjects map.
     * 
     * @param tagManager The tag manager.
     * @param tags The tags.
     * 
     * @return subjectsMap The subjects map.
     */
    private Map< TagModel, List< TagModel > > createSubjectsMap( TagManager tagManager, Tag[] tags ) {
        Map< TagModel, List< TagModel > > subjectsMap = new HashMap< >();
        for ( Tag tag : tags ) {
            TagModel tagModel = new TagModel( tag );
            if ( ClassMagsMigrationConstants.SUBJECT_TAG_DELIMITER_COUNT == StringUtils.countMatches( tag.getPath(),
                    "/" ) && !subjectsMap.containsKey( tagModel ) ) {
                subjectsMap.put( tagModel, Collections.< TagModel > emptyList() );
            }
            if ( ClassMagsMigrationConstants.SUBJECT_TAG_DELIMITER_COUNT < StringUtils.countMatches( tag.getPath(),
                    "/" ) ) {
                String parentPath = StringUtils.substringBeforeLast( tag.getPath(), "/" );
                Tag subjectTag = tagManager.resolve( parentPath );
                TagModel subjectTagModel = new TagModel( subjectTag );
                updateSubjectsMap( subjectsMap, subjectTagModel, tagModel );
            }
        }
        return subjectsMap;
    }

    /**
     * Update subjects map.
     * 
     * @param subjectsMap The subjects map.
     * @param subjectTagModel The subject tag model.
     * @param tagModel The tag model.
     */
    private void updateSubjectsMap( Map< TagModel, List< TagModel > > subjectsMap, TagModel subjectTagModel,
            TagModel tagModel ) {
        if ( subjectsMap.containsKey( subjectTagModel ) ) {
            List< TagModel > subjectList = subjectsMap.get( subjectTagModel );
            if ( subjectList.isEmpty() ) {
                subjectList = new ArrayList< >();
            }
            subjectList.add( tagModel );
            subjectsMap.put( subjectTagModel, subjectList );
        } else {
            List< TagModel > subjectList = new ArrayList< >();
            subjectList.add( tagModel );
            subjectsMap.put( subjectTagModel, subjectList );
        }
    }

}
