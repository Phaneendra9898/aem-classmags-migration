/**
 * 
 */
package com.scholastic.classmags.migration.servlets;

import java.io.IOException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.search.QueryBuilder;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.DamAssetConstants;
import com.scholastic.classmags.migration.utils.DamAssetUtils;

/**
 * @author gowthami
 *
 */
@SlingServlet( paths = {"/bin/hurix-kitaboo/getassets"}, methods = "GET" )
public class FetchDamAssetsServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 1L;
    private static final Logger LOG = LoggerFactory.getLogger(FetchDamAssetsServlet.class);
    
    /** The sling settings. */
    @Reference
    private SlingSettingsService slingSettings;
    
    @Reference
    private QueryBuilder queryBuilder;
    
    /** The resource path config service. */
    @Reference
    private ResourcePathConfigService resourcePathConfigService;
    
    /** The prop config service. */
    @Reference
    PropertyConfigService propertyConfigService;
    
	@Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) {
    	LOG.info("Entering @FetchDamAssetsServlet.doGet ");
    	//set response parameters
        response.setContentType(DamAssetConstants.CONTENT_TYPE);
        response.setCharacterEncoding(DamAssetConstants.ENCODING_STANDARD);

        try{
          //fetch path param
        	String pathParam = request.getParameter(DamAssetConstants.PATH_PARAM );
          //Process only if the path is not class-mags base path
        	if(DamAssetUtils.isValidPath(pathParam)) {
        		//Print the response
				response.getWriter().print(DamAssetUtils.getAssetsResponse(request, queryBuilder, 
						propertyConfigService, resourcePathConfigService));
        	} else {
        		LOG.info("@FetchDamAssetsServlet.doGet : the path is not valid " + pathParam);
        	}
        } catch(IOException e) {
        	LOG.error("@FetchDamAssetsServlet.doGet: Exception while fetching the data : ", e);
        }
	        LOG.info("Exciting @FetchDamAssetsServlet.doPost ");
    }
}
