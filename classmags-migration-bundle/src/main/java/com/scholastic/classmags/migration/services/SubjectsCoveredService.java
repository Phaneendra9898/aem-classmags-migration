package com.scholastic.classmags.migration.services;

import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;

import com.scholastic.classmags.migration.models.TagModel;

/**
 * The Interface Subjects Covered Service.
 */
public interface SubjectsCoveredService {

    /**
     * Fetch subjects covered.
     *
     * @param resource The current resource.
     * @param currentPath The current path.
     * @param magazineName The magazine name.
     * 
     * @return the subjects covered map
     * 
     */
    public Map<TagModel, List<TagModel>> getSubjectsCovered(Resource resource ,String currentPath, String magazineName );

}
