package com.scholastic.classmags.migration.utils;

import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.CONTENT_TYPES_OPERATOR;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.FLD_TYPE_CONTENTHUB;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.FLD_TYPE_VAL_ARTICLE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.FLD_TYPE_VAL_ISSUE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.ROLE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.ROWS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SITE_ID;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_FACET_FIELD;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_AND;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FACET_SORT_BY_NAME;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_ARTICLES;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_BLOG;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_DOWNLOAD;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_GAME;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_ISSUE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_LESSON_PLANS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_POLL;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_SKILL_SHEET;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_VIDEO;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_OR;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_SORT_TITLE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SORT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_ARTICLE_CONTENT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_ARTICLE_TITLE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_DESCRIPTION;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_KEYWORDS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_PUBLISHED_DATE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_SECTION_TEXT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_SECTION_TITLE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_SITE_ID;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_SORT_DATE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_SUBJECT_TAGS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_TAGS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_TITLE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_TYPE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_USER_TYPE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.START;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SUBJECTS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SUBJECTS_FILTERS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SUBJECTS_OPERATOR;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.TEXT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.TYPES;

import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.models.SearchRequest;

public class SolrClassMagsQueryBuilder {
    private static final Logger LOG = LoggerFactory.getLogger(SolrClassMagsQueryBuilder.class);

    private SolrClassMagsQueryBuilder() {
    }

    public static SolrQuery generateSolrFilterQuery(String preFilter, SearchRequest searchRequest, SolrQuery solrQuery,
            boolean includeFacet) {

        StringBuilder sb = new StringBuilder();
        if (StringUtils.isNotBlank( preFilter )) {
            switch(preFilter){
            case FLD_TYPE_VAL_ARTICLE:
                sb.append("(").append(SQ_FIELD_SITE_ID).append( ":\"" ).append( searchRequest.getSiteId() ).append( "\"" ).append(")").append(SOLR_QUERY_AND).append( "NOT(" )
                .append("type_s:Issue*").append(")");
                break;
            case FLD_TYPE_VAL_ISSUE:
                sb.append("(").append(SQ_FIELD_SITE_ID).append( ":\"" ).append( searchRequest.getSiteId() ).append( "\"" ).append(")").append(SOLR_QUERY_AND)
                .append("(").append("type_s:Issue*").append(")");
                break;
            case FLD_TYPE_CONTENTHUB:
                sb.append("(").append(SQ_FIELD_SITE_ID).append( ":\"" ).append( searchRequest.getSiteId() ).append( "\"" ).append(")");
                break;
            }
        } else {
            sb.append("(").append(SQ_FIELD_SITE_ID).append( ":\"" ).append( searchRequest.getSiteId() ).append( "\"" ).append(")").append(SOLR_QUERY_AND)
            .append("(")
            .append("(").append(SOLR_QUERY_FQ_TYPE_ARTICLES).append(")").append(SOLR_QUERY_OR)
            .append("(").append(SOLR_QUERY_FQ_TYPE_GAME).append(")").append(SOLR_QUERY_OR)
            .append("(").append(SOLR_QUERY_FQ_TYPE_VIDEO).append(")").append(SOLR_QUERY_OR)
            .append("(").append(SOLR_QUERY_FQ_TYPE_DOWNLOAD).append(")").append(SOLR_QUERY_OR)
            .append("(").append(SOLR_QUERY_FQ_TYPE_POLL).append(")").append(SOLR_QUERY_OR)
            .append("(").append(SOLR_QUERY_FQ_TYPE_BLOG).append(")").append(SOLR_QUERY_OR)
            .append("(").append(SOLR_QUERY_FQ_TYPE_SKILL_SHEET).append(")").append(SOLR_QUERY_OR)
            .append("(").append(SOLR_QUERY_FQ_TYPE_ISSUE).append(")").append(SOLR_QUERY_OR)
            .append("(").append(SOLR_QUERY_FQ_TYPE_LESSON_PLANS).append(")").append(" ) ");
        }
        //add role filter
        appendRoleFilterQuery( searchRequest, sb );
        // add pre-filter
        if (searchRequest.getFilters() != null && !"".equals(searchRequest.getFilters())) {
            LOG.debug( "Filters : {}",searchRequest.getFilters() );
            solrQuery.addFilterQuery(searchRequest.getFilters(), sb.toString());
        } else {
            solrQuery.addFilterQuery(sb.toString());
        }
        // add less relevance fields

        LOG.debug("queryFilter: " + solrQuery.toQueryString());

        solrQuery.setFacet(includeFacet);
        solrQuery.setFacetMinCount(1);
        solrQuery.setFacetSort(SOLR_QUERY_FACET_SORT_BY_NAME);
        solrQuery.setFacetLimit(10000);
        solrQuery.addFacetField(SOLR_FACET_FIELD);
        
        if (searchRequest.getSort() != null) {
            if (searchRequest.getSort().equals(SolrSearchQueryConstants.SORT_OPTION_TITLE)) {
                solrQuery.addSort(SOLR_QUERY_SORT_TITLE, SolrQuery.ORDER.asc);
            } else if(searchRequest.getSort().equals(SolrSearchQueryConstants.SORT_OPTION_PUBLISHED)){
                solrQuery.setSort(SQ_FIELD_SORT_DATE, SolrQuery.ORDER.desc);
            }
        }
        solrQuery.setStart(searchRequest.getStart());
        solrQuery.setRows(searchRequest.getRows());
        return solrQuery;
    }
    
    /**
     * Constructs & returns SearchRequest o
     * @param params
     * @return
     */
    public static SearchRequest generateSearchRequest(Map<String, String> params) {
        String text = params.get( TEXT );
        String start = params.get( START );
        String rows = params.get( ROWS );
        String sort = params.get( SORT );
        String contentTypes = params.get( TYPES );
        String subjects = params.get( SUBJECTS );
        String subjectFilters = params.get( SUBJECTS_FILTERS );
        String siteId = params.get( SITE_ID );
        String role = params.get( ROLE );
        String subjectOperator = params.get( SUBJECTS_OPERATOR );
        String contentOperator = params.get( CONTENT_TYPES_OPERATOR );
        SearchRequest searchRequest = new SearchRequest();
        searchRequest.setValidRequest(true);
        String contentTypesQuery = null;
        String subjectsQuery = null;
        String additionalSubjectsQuery = null;
        if (text != null && !text.isEmpty()) {
            searchRequest.setText(SolrSearchUtil.escapeSolrCharacters(text.trim()));
        } else {
            searchRequest.setText(SolrSearchClassMagsConstants.SOLR_QUERY_WILDCARD);
        }

        if (sort != null && !"".equals(sort.trim())) {

            if (sort.contains(SolrSearchQueryConstants.DESC_SUFIX)) {
                searchRequest.setSortOrder(SolrQuery.ORDER.desc);
            }

            searchRequest.setSort(sort.trim().replace(SolrSearchQueryConstants.DESC_SUFIX, ""));
        }

        if (StringUtils.isNotBlank( subjects ) ) {
            String[] types =  subjects.split( "," );
            if(ArrayUtils.isNotEmpty( types )){
                subjectsQuery = createFieldQuery( types, SQ_FIELD_SUBJECT_TAGS, StringUtils.isNotBlank( subjectOperator )?subjectOperator:SOLR_QUERY_OR );
            }
            searchRequest.setFilters( subjectsQuery );
         }
        
        if (StringUtils.isNotBlank( subjectFilters ) ) {
            String[] types =  subjectFilters.split( "," );
            if(ArrayUtils.isNotEmpty( types )){
                additionalSubjectsQuery = createFieldQuery( types, SQ_FIELD_SUBJECT_TAGS, SOLR_QUERY_OR );
            }
            if(StringUtils.isNotBlank( searchRequest.getFilters() )){
                StringBuilder sb = new StringBuilder();
                sb.append("(").append(subjectsQuery).append(")").append(SOLR_QUERY_AND).append("(").append(additionalSubjectsQuery).append(")");
                searchRequest.setFilters( sb.toString() );
                
            }else{
                searchRequest.setFilters( additionalSubjectsQuery );
            }
        }
        
        if (StringUtils.isNotBlank( contentTypes ) ) {
            String[] types =  contentTypes.split( "," );
            if(ArrayUtils.isNotEmpty( types )){
                contentTypesQuery = createFieldQuery( types, SQ_FIELD_TYPE, StringUtils.isNotBlank( contentOperator )?contentOperator:SOLR_QUERY_OR );
            }
            if(StringUtils.isNotBlank( searchRequest.getFilters() )){
                StringBuilder sb = new StringBuilder();
                sb.append("(").append(searchRequest.getFilters()).append(")").append(SOLR_QUERY_AND).append("(").append(contentTypesQuery).append(")");
                searchRequest.setFilters( sb.toString() );
            }else{
                searchRequest.setFilters( contentTypesQuery );
            }
        }
        
        if(StringUtils.isNotBlank( siteId )){
            searchRequest.setSiteId( siteId );
        }

        try {
            searchRequest.setStart(Integer.parseInt(start));
        } catch (NumberFormatException e) {
            searchRequest.setStart(SolrSearchQueryConstants.DEFAULT_START_VALUE);
        }

        try {
            searchRequest.setRows(Integer.parseInt(rows));
            if (searchRequest.getRows() > SolrSearchQueryConstants.MAX_ROWS_VALUE
                    || searchRequest.getRows() < SolrSearchQueryConstants.MIN_ROWS_VALUE) {
                searchRequest.setRows(SolrSearchQueryConstants.DEFAULT_ROWS_VALUE);
            }
        } catch (NumberFormatException e) {
            searchRequest.setRows(SolrSearchQueryConstants.DEFAULT_ROWS_VALUE);
        }

        searchRequest.setRole( role );
        return searchRequest;

    }

    /**
     * Generates Solr Query
     * 
     * @param text
     * @return String
     */
    public static String sanitizeSearchTerm(String text) {
        String inputText = SolrSearchUtil.escapeSolrCharacters(text);
        return inputText;
    }

    
    /**
     * previous relevance weights
	    sb.append(SQ_FIELD_TITLE).append(SQ_FIELD_TITLE_WT).append(" ");
	    sb.append(SQ_FIELD_TAGS).append(SQ_FIELD_TAGS_WT).append(" ");
	    sb.append(SQ_FIELD_KEYWORDS).append(SQ_FIELD_KEYWORDS_WT).append(" ");
	    sb.append(SQ_FIELD_DESCRIPTION).append(SQ_FIELD_DESCRIPTION_WT).append(" ");
	    sb.append(SQ_FIELD_ARTICLE_TITLE).append(SQ_FIELD_ARTICLE_TITLE_WT).append(" ");
	    sb.append(SQ_FIELD_ARTICLE_CONTENT).append(SQ_FIELD_ARTICLE_CONTENT_WT).append(" ");
	    sb.append(SQ_FIELD_PUBLISHED_DATE).append(SQ_FIELD_PUBLISHED_DATE_WT);
     * 
     * 
     * @param param
     * @return
     */
    public static String getQueryFields(String param) {
        StringBuilder sb = new StringBuilder();
        
        sb.append(SQ_FIELD_TITLE).append(" ");
	    sb.append(SQ_FIELD_TAGS).append(" ");
	    sb.append(SQ_FIELD_KEYWORDS).append(" ");
	    sb.append(SQ_FIELD_DESCRIPTION).append(" ");
	    sb.append(SQ_FIELD_ARTICLE_TITLE).append(" ");
	    sb.append(SQ_FIELD_ARTICLE_CONTENT).append(" ");
	    sb.append(SQ_FIELD_SECTION_TITLE).append(" ");
	    sb.append(SQ_FIELD_SECTION_TEXT).append(" ");
	    sb.append(SQ_FIELD_PUBLISHED_DATE);
        
        LOG.debug( "QF / PF Query String : {}",sb.toString() );
        return sb.toString();
    }

    /**
     * 
     * @param param
     * @return
     */
    public static String createFieldQuery(String[] param, String field, String operator) {
        String queryString = StringUtils.EMPTY;
        StringBuilder sb = new StringBuilder();
        for(String s : param){
            sb.append("(").append(field).append(":\"").append(s.trim().replaceAll( "-", " " ))
            .append("\")").append(operator);
        }
        queryString = sb.toString();
        if(queryString.endsWith( operator )){
            queryString = queryString.subSequence( 0, sb.lastIndexOf( operator ) ).toString();
        }
        return queryString;
    }
    
    /**
     * 
     * @param param
     * @return
     */
    public static String createFieldQuery(String[] param, String field) {
        String queryString = StringUtils.EMPTY;
        StringBuilder sb = new StringBuilder();
        for(String s : param){
            sb.append("(").append(field).append(":\"").append(s.trim().replaceAll( "-", " " ))
            .append("\")").append(SOLR_QUERY_OR);
        }
        queryString = sb.toString();
        if(queryString.endsWith( SOLR_QUERY_OR )){
            queryString = queryString.subSequence( 0, sb.lastIndexOf( SOLR_QUERY_OR ) ).toString();
        }
        return queryString;
    }
    
    public static void appendRoleFilterQuery(SearchRequest request, StringBuilder sb) {
        sb.append(SOLR_QUERY_AND).append("(");
        switch(request.getRole()){
        case RoleUtil.ROLE_TYPE_TEACHER:
        case RoleUtil.ROLE_TYPE_STUDENT:
            sb.append( SQ_FIELD_USER_TYPE ).append( ":\"" ).append( request.getRole() ).append( "\"" ).append(SOLR_QUERY_OR);
            sb.append( SQ_FIELD_USER_TYPE ).append( ":\"" ).append( RoleUtil.ROLE_TYPE_TEACHER_OR_STUDENT ).append( "\"" ).append(SOLR_QUERY_OR);
            sb.append( SQ_FIELD_USER_TYPE ).append( ":\"" ).append( RoleUtil.ROLE_TYPE_EVERYONE ).append( "\"" );
            break;
        case ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS:
            sb.append( SQ_FIELD_USER_TYPE ).append( ":" ).append( "*" );
            break;
        default:
            sb.append( SQ_FIELD_USER_TYPE ).append( ":\"" ).append( request.getRole() ).append( "\"" );
        }
        sb.append(")");
    }

}
