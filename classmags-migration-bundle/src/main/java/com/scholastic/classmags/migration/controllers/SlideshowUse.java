package com.scholastic.classmags.migration.controllers;

import java.util.UUID;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.services.SlideshowDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * Use Class for Slideshow.
 */
public class SlideshowUse extends WCMUsePojo {

    /** The slideshow json array. */
    private JSONArray slideshowJsonArray;
    
    /** The unique id. */
    private String uniqueId;

    /**
     * Gets the slideshow json array.
     *
     * @return the slideshowJsonArray
     */
    public String getSlideshowJsonArray() {
        return slideshowJsonArray.toString();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() throws JSONException {

        Resource currentResource = getResource();
        currentResource = currentResource.getChild( ClassMagsMigrationConstants.SLIDESHOW_META_DATA_NODE );
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
      
        if ( null != slingScriptHelper ) {
            SlideshowDataService slideshowDataService = slingScriptHelper.getService( SlideshowDataService.class );
            if ( null != slideshowDataService ) {
                slideshowJsonArray = slideshowDataService
                        .convertListToJSONArray( CommonUtils.fetchMultiFieldData( currentResource ) );
            }
        }

        uniqueId = UUID.randomUUID().toString();
    }

    /**
     * Gets the unique id.
     *
     * @return the unique id.
     */
    public String getUniqueId() {
        return uniqueId;
    }
}
