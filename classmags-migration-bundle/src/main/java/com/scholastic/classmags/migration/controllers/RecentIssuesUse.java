package com.scholastic.classmags.migration.controllers;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.models.Issue;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.RecentIssuesService;
import com.scholastic.classmags.migration.utils.CommonUtils;

public class RecentIssuesUse extends WCMUsePojo {

    private List< Issue > recentIssues;

    @Override
    public void activate() {

        Resource issueListResource = getResource().getChild( "issue-paths" );
        List< ValueMap > issuePaths = CommonUtils.fetchMultiFieldData( issueListResource );
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        Page currentPage = getCurrentPage();
        if ( null != slingScriptHelper && null != currentPage ) {
            MagazineProps magazineProps = slingScriptHelper.getService( MagazineProps.class );
            String magazineName = ( null != magazineProps ) ? magazineProps.getMagazineName( currentPage.getPath() )
                    : StringUtils.EMPTY;

            RecentIssuesService recentIssuesService = slingScriptHelper.getService( RecentIssuesService.class );
            if ( null != recentIssuesService ) {
                recentIssues = recentIssuesService.getRecentIssues( magazineName, issuePaths );
            }
        }
    }

    public List< Issue > getRecentIssues() {
        return recentIssues;
    }

}
