package com.scholastic.classmags.migration.models;

import java.util.List;
import java.util.Map;

/**
 * The Class ArticleAggregatorObject.
 */
public class ArticleAggregatorObject {

    /** The page object. */
    private ResourcesObject resourcesObject;

    /** The article resources. */
    private Map< String, List< ResourcesObject > > articleResources;

    /** The featured article flag. */
    private boolean featuredArticleFlag;
    
    /** The article lexile levels */
    private String articleLexileLevels;

    /**
     * Gets the article lexile levels.
     *
     * @return the articleLexileLevels
     */
    public String getArticleLexileLevels() {
        return articleLexileLevels;
    }

    /**
     * Sets the article lexile levels.
     *
     * @param articleLexileLevels
     *            the articleLexileLevels to set
     */
    public void setArticleLexileLevels( String articleLexileLevels ) {
        this.articleLexileLevels = articleLexileLevels;
    }
    
    /**
     * Checks if is featured article flag.
     *
     * @return the featuredArticleFlag
     */
    public boolean isFeaturedArticleFlag() {
        return featuredArticleFlag;
    }

    /**
     * Sets the featured article flag.
     *
     * @param featuredArticleFlag
     *            the featuredArticleFlag to set
     */
    public void setFeaturedArticleFlag( boolean featuredArticleFlag ) {
        this.featuredArticleFlag = featuredArticleFlag;
    }

    /**
     * Gets the article resources.
     * 
     * @return the articleResources
     */
    public Map< String, List< ResourcesObject > > getArticleResources() {
        return articleResources;
    }

    /**
     * Sets the article resources.
     * 
     * @param articleResources
     *            the articleResources to set
     */
    public void setArticleResources( Map< String, List< ResourcesObject > > articleResources ) {
        this.articleResources = articleResources;
    }

    /**
     * Gets the resources object.
     *
     * @return the resourcesObject
     */
    public ResourcesObject getResourcesObject() {
        return resourcesObject;
    }

    /**
     * Sets the resources object.
     *
     * @param resourcesObject
     *            the resourcesObject to set
     */
    public void setResourcesObject( ResourcesObject resourcesObject ) {
        this.resourcesObject = resourcesObject;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append( "ArticleAggregatorObject [resourcesObject=" );
        builder.append( resourcesObject );
        builder.append( ", articleResources=" );
        builder.append( articleResources );
        builder.append( "]" );
        return builder.toString();
    }

}
