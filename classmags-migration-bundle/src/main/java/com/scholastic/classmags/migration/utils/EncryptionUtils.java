package com.scholastic.classmags.migration.utils;

import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Encryption utils.
 */
public final class EncryptionUtils {

    public static final Logger log = LoggerFactory.getLogger( EncryptionUtils.class );
    
    private static final int SIXTEEN=16;
    
    private static final int THIRTYTWO=32;

    /**
     * message digest algorithm (must be sufficiently long to provide the key
     * and initialization vector)
     */
    private static final String DIGEST_ALGORITHM = "SHA-256";

    /** key algorithm (must be compatible with CIPHER_ALGORITHM) */
    private static final String KEY_ALGORITHM = "AES";

    /** cipher algorithm (must be compatible with KEY_ALGORITHM) */
    private static final String CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";

    private static final String SALT_KEY = "HXP5elbUR6tHVIwDLn@VoA5wXOsalYE";

    private static final byte[] keyAes = new byte[SIXTEEN];
    private static final byte[] ivAes = new byte[SIXTEEN];

    private EncryptionUtils() {
        /* to not to initialize utility class */
    }

    /**
     * Generates hashed value using SHA256 algorithm and assigns it to secretkey
     * and initialization vector.
     */
    private static void generateKey() {

        try {
            MessageDigest messageDigest = MessageDigest.getInstance( DIGEST_ALGORITHM );
            byte[] hashedBytes = messageDigest.digest( SALT_KEY.getBytes( StandardCharsets.UTF_8 ) );

            // Transfer the first 16 character value to the secret key and
            // next 16 character value to internal initialization vector
            for ( int count = 0; count < SIXTEEN; count++ ) {
                keyAes[ count ] = hashedBytes[ count ];
            }

            for ( int count = SIXTEEN; count < THIRTYTWO; count++ ) {
                ivAes[ count - SIXTEEN ] = hashedBytes[ count ];
            }

        } catch ( NoSuchAlgorithmException e ) {
            log.error( "No Such Algorithm Exception Exception {}", e );
        }

    }

    /**
     * This method encrypts the plain text provided using AEM algorithm.
     * 
     * @param strPlain
     * @return encrypted string
     */
    public static String encrypt( final String strPlain ) {
        log.debug( "Begin :: Encrypt method in :: " );
        byte[] encrypted = null;
        byte[] toEncrypt;
        String encodedText = null;
        Cipher cipher;

        if ( StringUtils.isNotBlank( strPlain ) ) {
            try {
                toEncrypt = strPlain.getBytes( StandardCharsets.UTF_8 );
                generateKey();
                final AlgorithmParameterSpec paramSpec = new IvParameterSpec( ivAes );
                // Generate the key specs.
                final SecretKeySpec skeySpec = new SecretKeySpec( keyAes, KEY_ALGORITHM );
                // Instantiate the cipher
                cipher = Cipher.getInstance( CIPHER_ALGORITHM );
                cipher.init( Cipher.ENCRYPT_MODE, skeySpec, paramSpec );
                encrypted = cipher.doFinal( toEncrypt );
            } catch ( GeneralSecurityException e ) {
                log.error( "No Such Algorithm Exception {}", e );
            }
            encodedText = urlTokenEncode( encrypted );
        }
        log.debug( "Exiting EncryptAes method in :: " );
        return encodedText;
    }

    /**
     * A method to decrypt string with AES.
     * 
     * @param strEncrypted
     *            the str encrypted
     * @return plain text string
     */

    public static String decrypt( String strEncrypted ) {
        log.debug( "Begin :: decrypt method in :: " );
        byte[] toPlain = urlTokenDecode( strEncrypted );
        String originalString = null;
        Cipher cipher;
        byte[] original;
        if ( StringUtils.isBlank( strEncrypted ) || toPlain == null ) {
            return strEncrypted;
        }
        try {
            generateKey();
            final AlgorithmParameterSpec paramSpec = new IvParameterSpec( ivAes );
            // Generate the key specs.
            final SecretKeySpec skeySpec = new SecretKeySpec( keyAes, KEY_ALGORITHM );
            // Instantiate the cipher
            cipher = Cipher.getInstance( CIPHER_ALGORITHM );
            cipher.init( Cipher.DECRYPT_MODE, skeySpec, paramSpec );
            original = cipher.doFinal( toPlain );
            originalString = new String( original );
        } catch ( GeneralSecurityException e ) {
            log.error( "General Security Exception {}", e );
        }
        log.debug( "Exiting decryptAes method in :: " );
        return originalString;
    }

    /**
     * This method replicates the logic which URlTokenEncode method does in
     * .Net. Provides extensibility hooks for custom encoding
     * 
     * @param input
     *            encrypted input
     * @return string
     */
    private static String urlTokenEncode( byte[] input ) {
        if ( input.length < 1 ){
            return StringUtils.EMPTY;
        }
        String base64Str;
        int endPos;
        char[] base64Chars;
        ////////////////////////////////////////////////////////
        // Step 1: Do a Base64 encoding
        base64Str = Base64.encodeBase64String( input );
        if ( base64Str == null ){
            return null;
        }
        ////////////////////////////////////////////////////////
        // Step 2: Find how many padding chars are present in the end
        for ( endPos = base64Str.length(); endPos > 0; endPos-- ) {
            if ( base64Str.charAt( endPos - 1 ) != '=' ) // Found a non-padding char!
            {
                break; // Stop here
            }
        }
        ////////////////////////////////////////////////////////
        // Step 3: Create char array to store all non-padding chars,
        // plus a char to indicate how many padding chars are needed
        base64Chars = new char[endPos + 1];
        // Store a char at the end, to indicate how many padding chars are
        // needed
        base64Chars[ endPos ] = ( char ) ( ( int ) '0' + base64Str.length() - endPos );
        convertToBaseChars( base64Str, endPos, base64Chars );
        return new String( base64Chars );
    }

    private static void convertToBaseChars( String base64Str, int endPos, char[] base64Chars ) {
        ////////////////////////////////////////////////////////
        // Step 3: Copy in the other chars. Transform the "+" to "-", and "/" to
        //////////////////////////////////////////////////////// "_"
        for ( int iter = 0; iter < endPos; iter++ ) {
            char c = base64Str.charAt( iter );

            switch ( c ) {
            case '+':
                base64Chars[ iter ] = '-';
                break;

            case '/':
                base64Chars[ iter ] = '_';
                break;

            case '=':
            default:
                base64Chars[ iter ] = c;
                break;
            }
        }
    }

    /**
     * This method replicates the logic which URlTokenDecode method does in
     * .Net. Provides extensibility hooks for custom decoding
     * 
     * @param input
     * @return byte array
     */
    private static byte[] urlTokenDecode( String input ) {

        int len = input.length();
        if ( len < 1 ){
            return new byte[0];
        }

        ///////////////////////////////////////////////////////////////////
        // Step 1: Calculate the number of padding chars to append to this
        /////////////////////////////////////////////////////////////////// string.
        // The number of padding chars to append is stored in the last char of
        /////////////////////////////////////////////////////////////////// the
        /////////////////////////////////////////////////////////////////// string.
        int numPadChars = ( int ) input.charAt( len - 1 ) - ( int ) '0';
        if ( numPadChars < 0 || numPadChars > 10 ){
            return new byte[0];
        }
        ///////////////////////////////////////////////////////////////////
        // Step 2: Create array to store the chars (not including the last char)
        // and the padding chars
        char[] base64Chars = new char[len - 1 + numPadChars];

        ////////////////////////////////////////////////////////
        // Step 3: Copy in the chars. Transform the "-" to "+", and "*" to "/"
        for ( int iter = 0; iter < len - 1; iter++ ) {
            char c = input.charAt( iter );

            switch ( c ) {
            case '-':
                base64Chars[ iter ] = '+';
                break;

            case '_':
                base64Chars[ iter ] = '/';
                break;

            default:
                base64Chars[ iter ] = c;
                break;
            }
        }

        ////////////////////////////////////////////////////////
        // Step 4: Add padding chars
        for ( int iter = len - 1; iter < base64Chars.length; iter++ ) {
            base64Chars[ iter ] = '=';
        }

        // Do the actual conversion
        String assembledString = String.copyValueOf( base64Chars );
        return Base64.decodeBase64( assembledString );

    }

}