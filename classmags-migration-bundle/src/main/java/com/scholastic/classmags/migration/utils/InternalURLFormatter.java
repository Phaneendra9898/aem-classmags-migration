package com.scholastic.classmags.migration.utils;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.wcm.api.NameConstants;

/**
 * The Class InternalURLFormatter.
 */
public final class InternalURLFormatter {

    private static final Logger LOG = LoggerFactory.getLogger( InternalURLFormatter.class );
    private static final String URL_EXTENSION = ".html";
    
    /**
     * Instantiates a new internal URL formatter.
     */
    private InternalURLFormatter() {
        
    }
    
    /**
     * Format an internal URL.
     *
     * @param url
     *            the url
     * @return formated url.
     */
    public static String formatURL( ResourceResolver resourceResolver, String path ) {
        
        LOG.debug("Transform Path : " + path );

        String mappedPath;
        
        if ( null != resourceResolver && isUrlInternal( resourceResolver, path ) ) {
            Resource resource = resourceResolver.resolve( path );
            mappedPath = getMappedPath( resource, resourceResolver, path );
        } else {
            mappedPath = path;
        }
        
        LOG.debug("Mapped Path : " + mappedPath );
        
        return mappedPath;
    }
    
    public static boolean isUrlInternal (ResourceResolver resourceResolver, String path) {
        // return true only if resource is internal to CQ and is not "sling:nonexisting"
        boolean isInternal = false;
        Resource resource = null;
        try {
            if ( null != resourceResolver && null != path ) {
                resource = resourceResolver.resolve( path );
            }
            if (resource != null && !ResourceUtil.isNonExistingResource(resource)) {
                isInternal = true;
            }
        } catch (Exception e) {
            LOG.error("Exception resolving path", e);
        }
        
        return isInternal;
    }
    
    private static String getMappedPath( Resource resource, ResourceResolver resourceResolver, String path ) {
        String mappedPath;
        
        if(resource.isResourceType( NameConstants.NT_PAGE )){
            mappedPath = resourceResolver.map(path)+URL_EXTENSION;
        } else {
            mappedPath = resourceResolver.map(path);
        }
        return mappedPath;
    }

}
