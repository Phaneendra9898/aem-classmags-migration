package com.scholastic.classmags.migration.models;

import java.util.List;

/**
 * The Class FooterObject.
 */
public class FooterObject {

    /** The heading title. */
    private String headingTitle;
    
    /** The live chat text. */
    private String liveChat;
    

    /** The live chat link. */
    private String liveChatLink;


    /** The phone number. */
    private String phoneNumber;

    /** The footer links. */
    private List<LinkObject> footerLinks; 

    /**
     * Gets the heading title.
     *
     * @return the heading title
     */
    public String getHeadingTitle() {
        return headingTitle;
    }
    
    /**
     * Sets the heading title.
     * 
     * @param headingTitle The heading title.
     */
    public final void setHeadingTitle( final String headingTitle ) {
        this.headingTitle = headingTitle;
    }
    
    /**
     * Gets the phone number.
     *
     * @return the phone number
     */
    public String getPhoneNumber() {
        return phoneNumber;
    }
    
    /**
     * Sets the phone number.
     * 
     * @param phoneNumber The phone number.
     */
    public final void setPhoneNumber( final String phoneNumber ) {
        this.phoneNumber = phoneNumber;
    }
    
    /**
     * Gets the footer links.
     *
     * @return the footer links
     */
    public List<LinkObject> getFooterLinks() {
        return footerLinks;
    }
    
    /**
     * Sets the footer links.
     * 
     * @param footerLinks The footer links.
     */
    public final void setFooterLinks( final List< LinkObject > footerLinks ) {
        this.footerLinks = footerLinks;
    }
    /**
     * Gets the live chat text.
     *
     * @return the live chat text
     */
    public String getLiveChat() {
        return liveChat;
    }
    /**
     * Sets the live chat text.
     * 
     * @param liveChat The live chat text.
     */
    public void setLiveChat(String liveChat) {
        this.liveChat = liveChat;
    }
    /**
     * Gets the live chat link.
     *
     * @return the live chat link
     */
    public String getLiveChatLink() {
        return liveChatLink;
    }
    /**
     * Sets the live chat link.
     * 
     * @param liveChatLink The live chat link.
     */
    public void setLiveChatLink(String liveChatLink) {
        this.liveChatLink = liveChatLink;
    }
}
