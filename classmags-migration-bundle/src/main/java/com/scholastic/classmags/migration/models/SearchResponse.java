package com.scholastic.classmags.migration.models;

import org.apache.sling.commons.json.JSONObject;
import org.apache.solr.common.SolrDocumentList;

/**
 * Represents Search Response
 * 
 *
 */
public class SearchResponse {
    
    private String type;
    private SolrDocumentList solrDocumentList;
    private long total;
    private JSONObject facets;
    private JSONObject intervalFacets;

    public SearchResponse(SolrDocumentList solrDocumentList) {
        this.solrDocumentList = solrDocumentList;
    }

    public SolrDocumentList getSolrDocumentList() {
        return solrDocumentList;
    }

    public void setSolrDocumentList(SolrDocumentList solrDocumentList) {
        this.solrDocumentList = solrDocumentList;
    }

    public JSONObject getFacets() {
        return facets;
    }

    public void setFacets(JSONObject facets) {
        this.facets = facets;
    }

    public JSONObject getIntervalFacets() {
        return intervalFacets;
    }

    public void setIntervalFacets(JSONObject intervalFacets) {
        this.intervalFacets = intervalFacets;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public String getType() {
        return type;
    }

    public void setType( String type ) {
        this.type = type;
    }
}
