package com.scholastic.classmags.migration.social.api;

import java.util.Map;

import com.adobe.cq.social.scf.SocialComponent;

/**
 * The Interface VotingSocialComponent.
 */
public interface VotingSocialComponent extends SocialComponent {

    /** The Constant VOTING_RESOURCE_TYPE. */
    public static final String VOTING_RESOURCE_TYPE = "scholastic/classroom-magazines-migration/components/content/voting";

    /**
     * Gets the question UUID.
     *
     * @return the question UUID
     */
    public String getQuestionUUID();

    /**
     * Gets the answer options map.
     *
     * @return the answer options map
     */
    public Map< String, String > getAnswerOptionsMap();

    /**
     * Checks if is voting enabled.
     *
     * @return true, if is voting enabled
     */
    public boolean isVotingEnabled();
}
