package com.scholastic.classmags.migration.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.ClassmagsMigrationErrorCodes;
import com.scholastic.classmags.migration.models.SearchFilter;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * The Class CommonUtils.
 */
public final class CommonUtils {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( CommonUtils.class );

    /**
     * Instantiates a new common utils.
     */
    private CommonUtils() {
    }

    /**
     * Gets the resource resolver.
     * 
     * @param resolverFactory
     *            the resolver factory
     * @param service
     *            the service
     * @return the resource resolver
     */
    public static ResourceResolver getResourceResolver( ResourceResolverFactory resolverFactory, String service ) {

        ResourceResolver resolver = null;
        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, service );
        try {
            resolver = resolverFactory.getServiceResourceResolver( param );
        } catch ( LoginException e ) {
            LOG.error( "Excepton in getResourceResolver method of CommonUtils::::" + e );
        }
        return resolver;
    }

    /**
     * Gets the sorting date.
     * 
     * @param resource
     *            the resource
     * @param dateFormat
     *            the date format
     * @return the sorting date
     */
    public static String getSortingDate( Resource resource, String dateFormat ) {

        String date = StringUtils.EMPTY;
        if ( resource != null ) {
            date = getSortingDate( resource.adaptTo( ValueMap.class ), dateFormat );
        }
        return date;
    }

    /**
     * Gets the sorting date.
     * 
     * @param properties
     *            the properties
     * @param dateFormat
     *            the date format
     * @return the sorting date
     */
    public static String getSortingDate( ValueMap properties, String dateFormat ) {

        String finalDateFormat = checkDateFormat( dateFormat );
        String date = StringUtils.EMPTY;
        Date sortingDate = properties.get( ClassMagsMigrationConstants.SORTING_DATE ) != null ? properties.get(
                ClassMagsMigrationConstants.SORTING_DATE, Date.class ) : null;
        String displayDate = properties.get( ClassMagsMigrationConstants.DISPLAY_DATE ) != null ? properties.get(
                ClassMagsMigrationConstants.DISPLAY_DATE, String.class ) : null;
        if ( sortingDate != null ) {
            SimpleDateFormat format = new SimpleDateFormat( finalDateFormat );
            date = DateFormatter.formatSortingDate( finalDateFormat, format.format( sortingDate ) );
        } else if ( StringUtils.isNotBlank( displayDate ) ) {
            date = displayDate;
        }
        return date;
    }

    /**
     * Gets the asset sorting date.
     * 
     * @param resource
     *            the resource
     * @param dateFormat
     *            the date format
     * @return the asset sorting date
     */
    public static String getAssetSortingDate( Resource resource, String dateFormat ) {
        String date = StringUtils.EMPTY;
        if ( null != resource ) {
            date = getAssetSortingDate( resource.adaptTo( ValueMap.class ), dateFormat );
        }
        return date;
    }

    /**
     * Gets the asset sorting date.
     * 
     * @param map
     *            the map
     * @param dateFormat
     *            the date format
     * @return the asset sorting date
     */
    public static String getAssetSortingDate( ValueMap map, String dateFormat ) {

        String finalDateFormat = checkDateFormat( dateFormat );
        String date = StringUtils.EMPTY;
        Date sortingDate = map.get( ClassMagsMigrationConstants.ASSET_SORTING_DATE, Date.class );
        String displayDate = map.get( ClassMagsMigrationConstants.ASSET_DISPLAY_DATE, StringUtils.EMPTY );
        if ( sortingDate != null ) {
            SimpleDateFormat format = new SimpleDateFormat( finalDateFormat );
            date = DateFormatter.formatSortingDate( finalDateFormat, format.format( sortingDate ) );
        } else if ( StringUtils.isNotBlank( displayDate ) ) {
            date = displayDate;
        }
        return date;
    }

    /**
     * Check date format.
     *
     * @param dateFormat
     *            the date format
     * @return the string
     */
    private static String checkDateFormat( String dateFormat ) {
        return ( null == dateFormat || StringUtils.equalsIgnoreCase( ClassMagsMigrationConstants.NO_DATA_AVAILABLE,
                dateFormat ) ) ? ClassMagsMigrationConstants.DEFAULT_SORTING_DATE_FORMAT : dateFormat;
    }

    /**
     * Gets the data path.
     * 
     * @param resourcePathConfigService
     *            the resource path config service
     * @param appName
     *            the app name
     * @return the data path
     */
    public static String getDataPath( ResourcePathConfigService resourcePathConfigService, String appName ) {
        return resourcePathConfigService.getGlobalConfigResourcePath( appName );
    }

    /**
     * Fetch page property value.
     * 
     * @param propertyName
     *            the property name
     * @param pagePath
     *            the page path
     * @param resourceResolverFactory
     *            the resource resolver factory
     * @return the string
     */
    public static String fetchPagePropertyValue( String propertyName, String pagePath,
            ResourceResolverFactory resourceResolverFactory ) {

        String propertyValue = null;

        ResourceResolver resourceResolver = getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );
        Resource res = resourceResolver.getResource( pagePath.concat( ClassMagsMigrationConstants.JCR_CONTENT_NODE ) );
        if ( null != res ) {
            ValueMap properties = res.getValueMap();
            propertyValue = properties.get( propertyName, StringUtils.EMPTY );
        }
        return propertyValue;
    }

    /**
     * Fetch page property map.
     * 
     * @param pagePath
     *            the page path
     * @param resourceResolverFactory
     *            the resource resolver factory
     * @return the value map
     */
    public static ValueMap fetchPagePropertyMap( String pagePath, ResourceResolverFactory resourceResolverFactory ) {

        ValueMap properties = null;

        ResourceResolver resourceResolver = getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );
        Resource res = resourceResolver.getResource( pagePath.concat( ClassMagsMigrationConstants.JCR_CONTENT_NODE ) );
        if ( null != res ) {
            properties = res.getValueMap();
        }
        return properties;
    }

    /**
     * Fetch multi field data.
     * 
     * @param resource
     *            the resource
     * @return the list
     */
    public static List< ValueMap > fetchMultiFieldData( Resource resource ) {

        List< ValueMap > multiFieldDataList = new ArrayList< >();

        if ( null != resource ) {
            final Iterator< Resource > resourceIterator = resource.listChildren();
            while ( resourceIterator.hasNext() ) {
                ValueMap properties = resourceIterator.next().getValueMap();
                multiFieldDataList.add( properties );
            }
        }
        return multiFieldDataList;
    }

    /**
     * Fetch multi field data.
     * 
     * @param resource
     *            the resource
     * @param nodeName
     *            the node name
     * @return the list
     */
    public static List< ValueMap > fetchMultiFieldData( Resource resource, String nodeName ) {
        ResourceResolver resourceResolver = resource.getResourceResolver();
        Resource multifieldResource = resourceResolver.getResource( resource.getPath() + "/" + nodeName );
        List< ValueMap > multiFieldDataList = new ArrayList< >();

        if ( null != multifieldResource ) {
            final Iterator< Resource > resourceIterator = multifieldResource.listChildren();
            while ( resourceIterator.hasNext() ) {
                ValueMap properties = resourceIterator.next().getValueMap();
                multiFieldDataList.add( properties );
            }
        }
        return multiFieldDataList;
    }

    /**
     * Gets the footer path.
     * 
     * @param resourcePathConfigService
     *            the resource path config service
     * @param appName
     *            the app name
     * @return the footer path
     */
    public static String getGlobalFooterPath( ResourcePathConfigService resourcePathConfigService, String appName ) {

        return resourcePathConfigService.getFooterConfigResourcePath( appName );
    }

    /**
     * Gets the resource.
     * 
     * @param resourceResolverFactory
     *            the resource resolver factory
     * @param serviceType
     *            the service type
     * @param path
     *            the path
     * @return the resource
     */
    public static Resource getResource( ResourceResolverFactory resourceResolverFactory, String serviceType, String path ) {
        ResourceResolver resourceResolver = getResourceResolver( resourceResolverFactory, serviceType );
        return resourceResolver != null ? resourceResolver.getResource( path ) : null;
    }

    /**
     * Gets the tags.
     * 
     * @param resource
     *            the resource
     * @param tagManager
     *            the tag manager
     * @return the tags
     */
    public static List< String > getTags( final Resource resource, final TagManager tagManager ) {
        final List< String > tagList = new ArrayList< >();

        Tag[] tags = tagManager.getTags( resource );
        for ( Tag tag : tags ) {
            tagList.add( tag.getTitle() );
        }

        return tagList;
    }

    /**
     * Gets the tags.
     * 
     * @param resource
     *            the resource
     * @param tagManager
     *            the tag manager
     * @return the subject tags
     */
    public static List< String > getSubjectTags( final Resource resource, final TagManager tagManager ) {
        final List< String > subjectTagList = new ArrayList<>();

        Set< String > subjectTags = getAllSubjectTags( resource, tagManager );
        subjectTagList.addAll( subjectTags );

        return subjectTagList;
    }

    /**
     * Get All Subject Tags.
     *
     * @param resource the resource
     * @param tagManager the tag manager
     * @return the all subject tags
     */
    public static Set< String > getAllSubjectTags( final Resource resource, final TagManager tagManager ) {
        final Set< String > subjectTagList = new HashSet<>();
        Tag[] tags = tagManager.getTags( resource );
        for ( Tag tag : tags ) {
            String[] tagIds = tag.getTagID().split( "/" );
            StringBuilder sb = new StringBuilder();
            for ( String tagId : tagIds ) {
                sb.append( tagId );
                Tag t = tagManager.resolve( sb.toString() );
                if ( ClassMagsMigrationConstants.SUBJECT_TAG_DELIMITER_COUNT == StringUtils
                        .countMatches( t.getPath().toString(), "/" ) && 
                        StringUtils.equals( t.getNamespace().getName(), ClassMagsMigrationConstants.CLASS_MAGS_TAG_NAMESPACE ) && 
                        !t.getTagID().startsWith( ClassMagsMigrationConstants.CLASS_MAGS_CONTENT_TYPE_TAG_ID ) ) {
                    subjectTagList.add( t.getTitle() );
                    LOG.debug( "Tags : {}", subjectTagList );
                    // break the loop once the subject tag is found
                    break;
                }
                sb.append( "/" );
            }
        }
        return subjectTagList;
    }
    
    /**
     * Get Content Type Tag.
     *
     * @param resource the resource
     * @param tagManager the tag manager
     * @return the content type tag
     */
    public static String getContentTypeTag( final Resource resource, final TagManager tagManager ) {
        String contentTypeTag=null;
        Tag[] tags = tagManager.getTags( resource );
        for ( Tag tag : tags ) {
            if ( tag.getTagID().startsWith( ClassMagsMigrationConstants.CLASS_MAGS_CONTENT_TYPE_TAG_ID ) ) {
                contentTypeTag = tag.getTitle();
                // break the loop once the content type tag is found
                break;
            }
        }
        return contentTypeTag;
    }

    /**
     * Gets the admin resource resolver.
     * 
     * @param resourceResolverFactory
     *            the resource resolver factory
     * @return the admin resource resolver
     */
    public static ResourceResolver getAdminResourceResolver( ResourceResolverFactory resourceResolverFactory ) {
        try {
            return resourceResolverFactory.getAdministrativeResourceResolver( null );
        } catch ( Exception e ) {
            LOG.error( "Exception in getAdminResourceResolver::::" + e );
        }
        return null;
    }

    /**
     * Gets the tag manager.
     * 
     * @param resourceResolverFactory
     *            the resource resolver factory
     * @return the tag manager
     */
    public static TagManager getTagManager( ResourceResolverFactory resourceResolverFactory ) {
        return getAdminResourceResolver( resourceResolverFactory ).adaptTo( TagManager.class );
    }

    /**
     * Fetch property from global config component.
     * 
     * @param propertyConfigService
     *            The property config service
     * @param resourcePathConfigService
     *            The resource path config service
     * @param key
     *            The property key.
     * @param appName
     *            The app name.
     * @return the string
     * @throws LoginException
     *             the login exception.
     */
    public static String getGlobalConfigProperty( PropertyConfigService propertyConfigService,
            ResourcePathConfigService resourcePathConfigService, String key, String appName ) throws LoginException {
        return propertyConfigService.getDataConfigProperty( key, getDataPath( resourcePathConfigService, appName ) );
    }

    /**
     * Gets the session.
     *
     * @param resourceResolver
     *            the resource resolver
     * @return the session
     */
    public static Session getSession( ResourceResolver resourceResolver ) {
        Session session = null;
        try {
            session = resourceResolver.adaptTo( Session.class );
        } catch ( Exception e ) {
            LOG.error( "Error Occured : ", e );
        }
        return session;
    }

    /**
     * Property check.
     *
     * @param proprtyName the proprty name
     * @param nodeName the node name
     * @return the string
     */
    public static String propertyCheck( String proprtyName, Node nodeName ) {
        String propValue = StringUtils.EMPTY;
        if ( nodeName != null ) {
            try {
                propValue = nodeName.hasProperty( proprtyName ) ? nodeName.getProperty( proprtyName ).getString() : "";

            } catch ( ValueFormatException e ) {
                LOG.error( "ValueFormatException:: ", e );
            } catch ( RepositoryException e ) {
                LOG.error( "RepositoryException:: ", e );
            }
        }
        return propValue;
    }
    
    /**
     * Checks if is A shareable asset.
     *
     * @param assetPath the asset path
     * @return the boolean
     */
    public static Boolean isAShareableAsset( String assetPath ) {
        String extension = assetPath.substring( assetPath.lastIndexOf( '.' ) + 1 ).toLowerCase();

        return ClassMagsMigrationConstants.ASSET_EXTENTIONS.contains( extension );
    }

    /**
     * Gets the formatted subject tags.
     *
     * @param tags the tags
     * @return the formatted subject tags
     */
    public static String getFormattedSubjectTags( List< String > tags ) {
        StringBuilder formattedSubjectTags = new StringBuilder();

        if ( !tags.isEmpty() ) {
            int position = 0;
            int size = tags.size();

            while ( position < size - 1 ) {
                formattedSubjectTags.append( tags.get( position ) + ", " );
                position = position + 1;
            }
            formattedSubjectTags.append( tags.get( position ) );
        }
        return formattedSubjectTags.toString();
    }
    
    /**
     * Gets the search filters.
     *
     * @param resourceresolver the resourceresolver
     * @param tagPath the tag path
     * @return the search filters
     */
    public static List<SearchFilter> getSearchFilters(ResourceResolver resourceresolver, String tagPath){
        LOG.info( "Tag path is , {}",tagPath );
        List<SearchFilter> filters = new ArrayList<>();
        if(resourceresolver!=null){
            LOG.debug( "Resource Resolver : {}",resourceresolver.getUserID() );
            TagManager tagManager= resourceresolver.adaptTo( TagManager.class );
            Tag tag = tagManager.resolve( tagPath );
            if(tag!=null){
                Iterator< Tag > it = tag.listChildren();
                while ( it.hasNext() ) {
                    Tag next = it.next();
                    SearchFilter filter = new SearchFilter();
                    filter.setContentType( next.getTitle() );
                    filter.setId(  filter.getContentType().replaceAll( " ", "-" ) );
                    filter.setRender( true );
                    filters.add( filter );
                }
            }
        }
        return filters;
    }
    
    /**
     * Gets the content type filters.
     *
     * @param resource the resource
     * @param request the request
     * @param isAuthorMode the is author mode
     * @return the content type filters
     */
    public static List<SearchFilter> getContentTypeFilters(Resource resource, SlingHttpServletRequest request, boolean isAuthorMode){
        List<SearchFilter> contentFilters = new ArrayList<>();
        if(resource!=null && resource.hasChildren()){
            Iterator<Resource> it = resource.listChildren();
            while(it.hasNext()){
                SearchFilter filter = it.next().adaptTo( SearchFilter.class );
                if(filter!=null){
                    boolean render = true;
                    if(!isAuthorMode){
                        render = RoleUtil.shouldRender( RoleUtil.getUserRole( request ), filter.getUserType() );
                    }
                    filter.setRender( render );
                    contentFilters.add( filter );
                }
            }
        }
        return contentFilters;
    }
    
    /**
     * Gets the date format.
     *
     * @param propertyConfigService the property config service
     * @param dataPagePath the data page path
     * @return the date format
     * @throws ClassmagsMigrationBaseException the classmags migration base exception
     */
    public static String getDateFormat( PropertyConfigService propertyConfigService, String dataPagePath ) throws ClassmagsMigrationBaseException {

        String dateFormat = StringUtils.EMPTY;
        try {
            dateFormat = propertyConfigService.getDataConfigProperty(
                    ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT, dataPagePath ) ;
            if ( StringUtils.equalsIgnoreCase( ClassMagsMigrationConstants.NO_DATA_AVAILABLE, dateFormat ) ) {
                dateFormat = ClassMagsMigrationConstants.DEFAULT_DATE_FORMAT;
            }
        } catch ( LoginException e ) {
            throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.LOGIN_ERROR, e );
        }
        return dateFormat;
    }
    
    /**
     * Generate a unique identifier.
     *
     * @return unique key.
     */
    public static String generateUniqueIdentifier() {
        UUID uniqueKey = UUID.randomUUID();
        return uniqueKey.toString();
    }
    
    /**
     * Checks if is author mode.
     *
     * @param runModes the run modes
     * @return true, if is author mode
     */
    /*
     * Returns if author mode is true.
     */
    public static boolean isAuthorMode( Set< String > runModes ) {
        return isRunMode( runModes, "author" );
    }
    
    /**
     * Checks if is run mode.
     *
     * @param runModes the run modes
     * @param runMode the run mode
     * @return true, if is run mode
     */
    public static boolean isRunMode( Set< String > runModes , String runMode) {
        boolean mode;
        if ( runModes.contains( runMode ) ) {
            mode = true;
        } else {
            mode = false;
        }
        return mode;
    }
    
    /**
     * Creates the jwt token.
     *
     * @param userId the user id
     * @param issuer the issuer
     * @param secretKey the secret key
     * @return the string
     */
    /*
     * Creates JWT Token.
     * 
     * @param userId The user id.
     * @param issuer The issuer.
     * @param secretKey The secret key.
     * 
     * @return The JWT token.
     */
    public static String createJwtToken( String userId, String issuer, String secretKey ) {

        Calendar currentTime = Calendar.getInstance();
        currentTime.add( Calendar.HOUR_OF_DAY, 2 );

        Date expirationTime = currentTime.getTime();

        return Jwts.builder().claim( "userId", userId ).setExpiration( expirationTime ).setSubject( userId )
                .setIssuer( issuer )
                .signWith( SignatureAlgorithm.HS512, secretKey.getBytes( java.nio.charset.StandardCharsets.UTF_8 ) )
                .compact();

    }
    
    /**
     * Adds the delimiter.
     *
     * @param inputData
     *            the input data
     * @return the string
     */
    public static String addDelimiter( String inputData ) {
        return ClassMagsMigrationConstants.DELIMITER + inputData + ClassMagsMigrationConstants.DELIMITER;
    }
    
    /**
     * Gets the display date.
     * 
     * @param resource
     *            the resource
     * @param dateFormat
     *            the date format
     * @return the display date
     */
    public static String getDisplayDate( Resource resource, String dateFormat ) {

        String date = StringUtils.EMPTY;
        if ( resource != null ) {
            date = getDisplayDate( resource.adaptTo( ValueMap.class ), dateFormat );
        }
        return date;
    }
    
    /**
     * Gets the display date.
     * 
     * @param properties
     *            the properties
     * @param dateFormat
     *            the date format
     * @return the display date
     */
    public static String getDisplayDate( ValueMap properties, String dateFormat ) {

        String date = StringUtils.EMPTY;
        String displayDate = properties.get( ClassMagsMigrationConstants.DISPLAY_DATE, String.class ) != null
                ? properties.get( ClassMagsMigrationConstants.DISPLAY_DATE, String.class ) : null;
        if ( null != displayDate ) {
            date = displayDate;
        } else {
            String finalDateFormat = checkDateFormat( dateFormat );
            Date sortingDate = properties.get( ClassMagsMigrationConstants.SORTING_DATE ) != null
                    ? properties.get( ClassMagsMigrationConstants.SORTING_DATE, Date.class ) : null;
            if ( null != sortingDate ) {
                SimpleDateFormat format = new SimpleDateFormat( finalDateFormat );
                date = DateFormatter.formatSortingDate( finalDateFormat, format.format( sortingDate ) );
            }
        }
        return date;
    }
   
    /**
     * Gets the asset display date.
     * 
     * @param resource
     *            the resource
     * @param dateFormat
     *            the date format
     * @return the asset display date
     */
    public static String getAssetDisplayDate( Resource resource, String dateFormat ) {
        String date = StringUtils.EMPTY;
        if ( null != resource ) {
            date = getAssetDisplayDate( resource.adaptTo( ValueMap.class ), dateFormat );
        }
        return date;
    }

    /**
     * Gets the asset display date.
     * 
     * @param map
     *            the map
     * @param dateFormat
     *            the date format
     * @return the asset display date
     */
    public static String getAssetDisplayDate( ValueMap map, String dateFormat ) {

        String date = StringUtils.EMPTY;
        String displayDate = map.get( ClassMagsMigrationConstants.ASSET_DISPLAY_DATE, String.class ) != null
                ? map.get( ClassMagsMigrationConstants.ASSET_DISPLAY_DATE, String.class ) : null;

        if ( null != displayDate ) {
            date = displayDate;
        } else {
            String finalDateFormat = checkDateFormat( dateFormat );
            Date sortingDate = map.get( ClassMagsMigrationConstants.ASSET_SORTING_DATE ) != null
                    ? map.get( ClassMagsMigrationConstants.ASSET_SORTING_DATE, Date.class ) : null;
            if ( null != sortingDate ) {
                SimpleDateFormat format = new SimpleDateFormat( finalDateFormat );
                date = DateFormatter.formatSortingDate( finalDateFormat, format.format( sortingDate ) );
            }
        }

        return date;
    }
    
    /**
     * Gets the all tag ids.
     *
     * @param resource
     *            the resource
     * @param tagManager
     *            the tag manager
     * @return the all tag ids
     */
    public static Set< String > getAllTagIds( final Resource resource, final TagManager tagManager ) {

        final Set< String > tagIdSet = new HashSet<>();
        Tag[] tags = tagManager.getTags( resource );

        for ( Tag tag : tags ) {
            String tagId = tag.getTagID();
            if ( !StringUtils.startsWith( tagId, SolrSearchClassMagsConstants.CM_GLOBAL_TAG ) ) {
                String aggregateTagId = populateTagIdSet( tagId );
                if ( StringUtils.isNotBlank( aggregateTagId ) ) {
                    tagIdSet.add( aggregateTagId );
                }
            }
        }
        return tagIdSet;
    }

    /**
     * Populate tag id set.
     *
     * @param tagId
     *            the tag id
     * @return the string
     */
    private static String populateTagIdSet( String tagId ) {

        String[] tagArray = tagId
                .replaceAll( ClassMagsMigrationConstants.COLON, ClassMagsMigrationConstants.FORWARD_SLASH )
                .split( ClassMagsMigrationConstants.FORWARD_SLASH );

        if ( StringUtils.isNotBlank( tagArray[ NumberUtils.INTEGER_ZERO ] )
                && StringUtils.isNotBlank( tagArray[ NumberUtils.INTEGER_ONE ] )
                && StringUtils.isNotBlank( tagArray[ ClassMagsMigrationConstants.TAG_ID_DELIMITER_COUNT ] ) ) {
            for ( int tagCounter = ClassMagsMigrationConstants.TAG_ID_DELIMITER_COUNT; tagCounter < tagArray.length; tagCounter++ ) {
                return tagArray[ NumberUtils.INTEGER_ZERO ] + ClassMagsMigrationConstants.FORWARD_SLASH
                        + tagArray[ NumberUtils.INTEGER_ONE ] + ClassMagsMigrationConstants.FORWARD_SLASH
                        + tagArray[ tagCounter ];
            }

        }
        return StringUtils.EMPTY;
    }
}
