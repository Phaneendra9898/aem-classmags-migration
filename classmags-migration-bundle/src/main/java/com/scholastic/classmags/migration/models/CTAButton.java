package com.scholastic.classmags.migration.models;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * Sling Model for CTAButton.
 */
@Model( adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL )
public class CTAButton extends ClassmagsMigrationBaseModel {

    /** The button hyperlink. */
    @Inject
    @Via( "resource" )
    private String buttonHyperlink;

    /**
     * Instantiates a new CTA button.
     *
     * @param request
     *            the request
     */
    public CTAButton( SlingHttpServletRequest request ) {
        super( request );
    }

    /**
     * Gets the button hyperlink.
     *
     * @return the buttonHyperlink
     */
    public String getButtonHyperlink() {
        return InternalURLFormatter.formatURL( getRequest().getResourceResolver(), buttonHyperlink );
    }
}
