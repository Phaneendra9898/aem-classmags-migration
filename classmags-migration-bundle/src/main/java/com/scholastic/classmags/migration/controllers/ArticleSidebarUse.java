package com.scholastic.classmags.migration.controllers;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * Use Class for article sidebar.
 */
public class ArticleSidebarUse extends WCMUsePojo {

    private String uuid;

    public void activate() {
        uuid = CommonUtils.generateUniqueIdentifier();
    }

    /**
     * Gets the UUID.
     *
     * @return the UUID.
     */
    public String getUuid() {
        return uuid;
    }

}
