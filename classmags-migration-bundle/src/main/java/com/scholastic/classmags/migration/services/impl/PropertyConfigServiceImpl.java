package com.scholastic.classmags.migration.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;

import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * Property Configuration Service.
 */
@Component( immediate = true, metatype = true )
@Service( PropertyConfigService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Property Configuration Service" ) })
public class PropertyConfigServiceImpl implements PropertyConfigService {

    /** The Constant NO_DATA_AVAILABLE. */
    private static final String NO_DATA_AVAILABLE = "NO DATA AVAILABLE";

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /** The resource path config service. */
    @Reference
    private ResourcePathConfigService resourcePathConfigService;

    /**
     * Sets the resource resolver factory.
     *
     * @param resourceResolverFactory
     *            the resourceResolverFactory to set
     */
    public void setResourceResolverFactory( ResourceResolverFactory resourceResolverFactory ) {
        this.resourceResolverFactory = resourceResolverFactory;
    }

    /**
     * Sets the resource path config service.
     *
     * @param resourcePathConfigService
     *            the resourcePathConfigService to set
     */
    public void setResourcePathConfigService( ResourcePathConfigService resourcePathConfigService ) {
        this.resourcePathConfigService = resourcePathConfigService;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.services.PropertyConfigService#
     * getDataConfigProperty(java.lang.String, java.lang.String)
     */
    @Override
    public String getDataConfigProperty( String inputkey, String dataPath ) throws LoginException {

        String propertyValue;

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        ResourceResolver resourceResolver = resourceResolverFactory.getServiceResourceResolver( param );
        Resource res = resourceResolver.getResource( dataPath );
        propertyValue = getPropertyValue( inputkey, res );

        if ( null == propertyValue || StringUtils.equals( NO_DATA_AVAILABLE, propertyValue ) ) {
            res = resourceResolver.getResource( resourcePathConfigService.getGlobalResourcePath() );
            propertyValue = getPropertyValue( inputkey, res );
        }

        resourceResolver.close();

        return propertyValue;
    }

    /**
     * Gets the property value.
     *
     * @param inputkey
     *            the inputkey
     * @param res
     *            the res
     * @return the property value
     */
    private String getPropertyValue( String inputkey, Resource res ) {
        String propertyValue = NO_DATA_AVAILABLE;
        if ( null != res ) {
            ValueMap properties = res.adaptTo( ValueMap.class );
            propertyValue = properties != null ? properties.get( inputkey, String.class ) : NO_DATA_AVAILABLE;
        }
        return propertyValue;
    }
}
