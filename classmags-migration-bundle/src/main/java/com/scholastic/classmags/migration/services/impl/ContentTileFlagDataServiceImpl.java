package com.scholastic.classmags.migration.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.ArticleResourcesData;
import com.scholastic.classmags.migration.models.ClassmagsMigrationErrorCodes;
import com.scholastic.classmags.migration.models.ContentTileFlags;
import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.services.ContentTileFlagDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * The Class ContentTileFlagDataServiceImpl.
 */
@Component( immediate = true, metatype = false )
@Service( ContentTileFlagDataService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Content Tile Flag Data Service" ) })
public class ContentTileFlagDataServiceImpl implements ContentTileFlagDataService {

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /** The query builder. */
    @Reference
    private QueryBuilder queryBuilder;

    /** The resource resolver. */
    private ResourceResolver resourceResolver;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.scholastic.classmags.migration.services.ContentTileFlagDataService#
     * configureArticlePageAndFlagData(java.lang.String, java.util.Map)
     */
    @Override
    public void configureArticlePageAndFlagData( String issuePagePath,
            Map< String, List< ResourcesObject > > resources ) throws ClassmagsMigrationBaseException {

        Session session;
        Map< String, String > queryMap = new HashMap< >();

        resourceResolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );
        session = resourceResolver.adaptTo( Session.class );

        queryMap.put( ClassMagsMigrationConstants.QUERY_PATH, issuePagePath );
        queryMap.put( ClassMagsMigrationConstants.RESULTS_LIMIT, ClassMagsMigrationConstants.QUERY_LOWER_BOUND );
        queryMap.put( ClassMagsMigrationConstants.QUERY_PARAM_1, ClassMagsMigrationConstants.QUERY_RESOURCE_TYPE_TAG );
        queryMap.put( ClassMagsMigrationConstants.QUERY_PARAM_1_VALUE,
                ClassMagsMigrationConstants.ARTICLE_CONFIG_RESOURCE_TYPE_PATH );
        Query query = queryBuilder.createQuery( PredicateGroup.create( queryMap ), session );

        SearchResult searchResult = query.getResult();
        List< Hit > articleConfigurationNodesList = searchResult.getHits();
        traverseArticleAndTeachingResourceData( articleConfigurationNodesList, resources );
        resourceResolver.close();
    }

    /**
     * Traverse article and teaching resource data.
     *
     * @param articleConfigurationNodesList
     *            the article configuration nodes list
     * @param resources
     *            the resources
     * @throws ClassmagsMigrationBaseException
     */
    private void traverseArticleAndTeachingResourceData( List< Hit > articleConfigurationNodesList,
            Map< String, List< ResourcesObject > > resources ) throws ClassmagsMigrationBaseException {

        List< ArticleResourcesData > articleResourceDataList = new ArrayList< >();

        if ( null != articleConfigurationNodesList && !articleConfigurationNodesList.isEmpty() ) {
            prepareArticleResourceData( articleConfigurationNodesList, articleResourceDataList );
        }
        compareArticleAndTeachingResourceData( resources, articleResourceDataList );
    }

    /**
     * Prepare article resource data.
     *
     * @param articleConfigurationNodesList
     *            the article configuration nodes list
     * @param articleResourceDataList
     *            the article resource data list
     * @throws ClassmagsMigrationBaseException
     */
    private void prepareArticleResourceData( List< Hit > articleConfigurationNodesList,
            List< ArticleResourcesData > articleResourceDataList ) throws ClassmagsMigrationBaseException {

        for ( Hit articleConfigurationNode : articleConfigurationNodesList ) {
            try {
                Resource articleConfigRes = articleConfigurationNode.getResource();
                Resource childResource = null != articleConfigRes
                        ? articleConfigRes.getChild( ClassMagsMigrationConstants.RESOURCES ) : null;
                Iterator< Resource > resourceIterator = null != childResource ? childResource.listChildren() : null;
                while ( null != resourceIterator && resourceIterator.hasNext() ) {
                    ArticleResourcesData articleResourcesData = new ArticleResourcesData();
                    Resource iteratorNodeResource = resourceIterator.next();
                    articleResourcesData.setArticleResourceType( getResourceType( iteratorNodeResource ) );
                    articleResourcesData.setArticlePagePath( getArticlePagePath( articleConfigRes ) );
                    articleResourceDataList.add( articleResourcesData );
                }
            } catch ( RepositoryException e ) {
                throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.REPOSITORY_ERROR, e );
            }
        }
    }

    /**
     * Compare article and teaching resource data.
     *
     * @param resources
     *            the resources
     * @param articleResourceDataList
     *            the article resource data list
     */
    private void compareArticleAndTeachingResourceData( Map< String, List< ResourcesObject > > resources,
            List< ArticleResourcesData > articleResourceDataList ) {

        if ( null != resources ) {
            for ( String teachingResourceKey : resources.keySet() ) {
                for ( ResourcesObject resourcesObject : resources.get( teachingResourceKey ) ) {
                    addContentTileFlagData( articleResourceDataList, teachingResourceKey, resourcesObject );
                }
            }
        }
    }

    /**
     * Adds the content tile flag data.
     *
     * @param articleResourceDataList
     *            the article resource data list
     * @param teachingResourceKey
     *            the teaching resource key
     * @param resourcesObject
     *            the resources object
     */
    private void addContentTileFlagData( List< ArticleResourcesData > articleResourceDataList,
            String teachingResourceKey, ResourcesObject resourcesObject ) {

        ContentTileFlags contentTileFlags = new ContentTileFlags();
        setDowndloadEnabledFlag( contentTileFlags, resourcesObject );
        if ( null != articleResourceDataList && !articleResourceDataList.isEmpty() ) {
            for ( ArticleResourcesData articleResourcesData : articleResourceDataList ) {
                if ( StringUtils.equals(
                        prepareTeachingResourceData( teachingResourceKey, resourcesObject.getPagePath() ),
                        articleResourcesData.getArticleResourceType() ) ) {
                    resourcesObject.setArticlePagePath( InternalURLFormatter.formatURL( resourceResolver,
                            articleResourcesData.getArticlePagePath() ) );
                    contentTileFlags.setViewArticleEnabled( true );
                    break;
                }
            }
        }
        resourcesObject.setContentTileFlags( contentTileFlags );
    }

    /**
     * Sets the downdload enabled flag.
     *
     * @param contentTileFlags
     *            the content tile flags
     * @param resourcesObject
     *            the resources object
     */
    private void setDowndloadEnabledFlag( ContentTileFlags contentTileFlags, ResourcesObject resourcesObject ) {
        contentTileFlags.setDownloadEnabled( CommonUtils.isAShareableAsset( resourcesObject.getPagePath() ) );
    }

    /**
     * Prepare teaching resource data.
     *
     * @param teachingResourceKey
     *            the teaching resource key
     * @param resourcePath
     *            the resource path
     * @return the string
     */
    private String prepareTeachingResourceData( String teachingResourceKey, String resourcePath ) {
        return teachingResourceKey.concat( ClassMagsMigrationConstants.HYPHEN ).concat( resourcePath );
    }

    /**
     * Gets the article page path.
     *
     * @param articleConfigRes
     *            the article config res
     * @return the article page path
     */
    private String getArticlePagePath( Resource articleConfigRes ) {

        Resource parentJcrResourceNode = articleConfigRes.getParent();
        Resource articleResourceNode = null != parentJcrResourceNode ? parentJcrResourceNode.getParent() : null;
        return null != articleResourceNode ? articleResourceNode.getPath() : StringUtils.EMPTY;
    }

    /**
     * Gets the resource type.
     *
     * @param iteratorNodeResource
     *            the iterator node resource
     * @return the resource type
     */
    private String getResourceType( Resource iteratorNodeResource ) {

        ValueMap resourceProperties = iteratorNodeResource != null ? iteratorNodeResource.getValueMap() : null;
        return resourceProperties != null
                ? resourceProperties.get( ClassMagsMigrationConstants.PN_RESOURCE_TYPE, StringUtils.EMPTY )
                : StringUtils.EMPTY;
    }
}
