package com.scholastic.classmags.migration.models;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * The Class CurrentIssueHero.
 */
@Model( adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL )
public class CurrentIssueHero extends ClassmagsMigrationBaseModel {

    /** The current issue path. */
    @Inject
    @Via( "resource" )
    private String currentIssuePath;

    /**
     * Instantiates a new current issue hero.
     *
     * @param request
     *            the request
     */
    public CurrentIssueHero( SlingHttpServletRequest request ) {
        super( request );
    }
    
    /**
     * Gets the current issue path.
     *
     * @return the currentIssuePath
     */
    public String getCurrentIssuePath() {
        return InternalURLFormatter.formatURL( getRequest().getResourceResolver(), currentIssuePath );
    }

}
