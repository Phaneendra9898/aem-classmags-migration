package com.scholastic.classmags.migration.models;

import org.apache.sling.api.SlingHttpServletRequest;

import com.day.cq.wcm.api.WCMMode;
import com.day.cq.wcm.api.designer.Style;
import com.day.cq.wcm.commons.WCMUtils;

/**
 * The Class ClassmagsMigrationBaseModel.
 */
public class ClassmagsMigrationBaseModel {

    /** The request. */
    private SlingHttpServletRequest request;

    /**
     * Instantiates a new classmags migration base model.
     *
     * @param request
     *            the request
     */
    public ClassmagsMigrationBaseModel( SlingHttpServletRequest request ) {
        this.request = request;
    }

    /**
     * Gets the request.
     *
     * @return the request
     */
    public SlingHttpServletRequest getRequest() {
        return request;
    }
    
    public Style getCurrentStyle() {
        return WCMUtils.getStyle(request);
    }

    public boolean isEditMode() {
        return WCMMode.DISABLED != WCMMode.fromRequest(request) && WCMMode.PREVIEW != WCMMode
                .fromRequest(request);
    }
}
