package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.models.MagazineOptionObject;
import com.scholastic.classmags.migration.utils.CommonUtils;

public class MagazineOptionDisplayUse extends WCMUsePojo {
	
	/** The constant metadata node name **/
    private static final String JCR_NODE = "/jcr:content/par/marketing_magazineop/magazineList";
    
    /** The Magazine object. */
	private List<MagazineOptionObject> magazineObject;
	
	/** The Constant LOG. */
    private static final Logger LOGGER = LoggerFactory.getLogger( MagazineOptionDisplayUse.class );
    
	
	@Override
	public void activate() throws Exception {
		
		List< ValueMap > assetList;
		Page currentPage = getCurrentPage();
		String currentPath = currentPage.getPath().concat(JCR_NODE);
		
		Resource resource = getResourceResolver().getResource(currentPath);
		
		assetList = CommonUtils.fetchMultiFieldData(resource);
		setMagazineObject(populateMagazineOptionObject(assetList));
		
	}
	
	private List<MagazineOptionObject> populateMagazineOptionObject( List< ValueMap > assetDataList ) {
		MagazineOptionObject magazineOptionObject;
		List<MagazineOptionObject> objectList = new ArrayList<>();
		for(ValueMap assetDataMap : assetDataList){
			magazineOptionObject =  new MagazineOptionObject();
						
			magazineOptionObject.setOptionDisplayTitle(assetDataMap.get("optionDisplayTitle", String.class));
			magazineOptionObject.setMagazineImagePath(assetDataMap.get("magazineImagePath", String.class));
			magazineOptionObject.setMagazineImagePathLink(assetDataMap.get("magazineImagePathLink", String.class));
			magazineOptionObject.setButtonTitle(assetDataMap.get("buttonTitle", String.class));
			magazineOptionObject.setButtonColor(assetDataMap.get("buttonColor", String.class));
			magazineOptionObject.setButtonFontColor(assetDataMap.get("buttonFontColor", String.class));
			magazineOptionObject.setButtonRedirectLink(assetDataMap.get("buttonRedirectLink", String.class));
			
			objectList.add(magazineOptionObject);

		}
		return objectList;
	}
	
	/**
     * Gets the magazine object.
     * 
     * @return the magazineObject
     */	 
	 public List<MagazineOptionObject> getMagazineObject() {
			return magazineObject;
		}
	 
	 /**
	     * Sets the Magazine object.
	     * 
	     * @param magazineObject
	     *            the magazineObject to set
	     */
	 public void setMagazineObject(List<MagazineOptionObject> magazineObject) {
			this.magazineObject = magazineObject;
		}

}
