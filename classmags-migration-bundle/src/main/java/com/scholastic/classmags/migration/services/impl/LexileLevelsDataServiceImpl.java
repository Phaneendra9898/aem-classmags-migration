package com.scholastic.classmags.migration.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;

import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.services.LexileLevelsDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class LexileLevelsDataServiceImpl.
 */
@Component( immediate = true, metatype = false )
@Service( LexileLevelsDataService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Lexile Levels Data Service" ) })
public class LexileLevelsDataServiceImpl implements LexileLevelsDataService {

    /** The reading levels map. **/
    Map< String, String > readingLevelsMap;

    @Override
    public Map< String, String > fetchLexileLevels( SlingHttpServletRequest slingRequest, Page currentPage ) {
        readingLevelsMap = new HashMap< >();

        Resource jcrContent = currentPage.getContentResource();
        Resource articleConfigResource = ( null != jcrContent ) ? jcrContent.getChild( "article-configuration" ) : null;
        Resource lexileLevelResource = ( null != articleConfigResource )
                ? articleConfigResource.getChild( "lexileSettings" ) : null;
        List< ValueMap > lexileLevels = CommonUtils.fetchMultiFieldData( lexileLevelResource );
        for ( ValueMap lexileLevel : lexileLevels ) {
            String readingLevel = lexileLevel.get( ClassMagsMigrationConstants.ARTICLE_LEXILE_LEVEL, String.class );
            readingLevelsMap.put( readingLevel,
                    "Reading Level".concat( ClassMagsMigrationConstants.HYPHEN ).concat( readingLevel ) );
        }
        if ( readingLevelsMap.isEmpty() ) {
            readingLevelsMap.put( "default", "Default" );
        }
        return readingLevelsMap;
    }
}
