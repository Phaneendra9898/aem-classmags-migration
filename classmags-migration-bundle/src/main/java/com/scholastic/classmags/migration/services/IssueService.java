package com.scholastic.classmags.migration.services;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;

import com.day.cq.wcm.api.Page;

/**
 * The Interface IssueService.
 */
public interface IssueService {

    /**
     * Gets the parent issue path of an article.
     *
     */
    String getParentIssuePath( SlingHttpServletRequest request, Page currentPage );

    /**
     * Gets the parent issue date.
     * 
     * @throws LoginException
     */
    String getParentIssueDate( SlingHttpServletRequest request, Page currentPage, String appName )
            throws LoginException;
}
