package com.scholastic.classmags.migration.services;

/**
 * The Interface ResourcePathConfigService.
 */
public interface ResourcePathConfigService {

    /**
     * Gets the global resource path.
     *
     * @return the global resource path
     */
    String getGlobalResourcePath();
    
    /**
     * Gets the global footer resource path.
     *
     * @return the global footer resource path
     */
    String getGlobalFooterResourcePath();
    
    /**
     * Gets the global config resource path.
     *
     * @return the global config resource path
     */
    String getGlobalConfigResourcePath(String magazineName);

    /**
     * Gets the global footer resource path.
     *
     * @return the global footer resource path
     */
    String getFooterConfigResourcePath(String magazineName);
    
}
