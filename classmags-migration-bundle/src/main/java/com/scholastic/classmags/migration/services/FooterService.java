package com.scholastic.classmags.migration.services;

import java.util.List;

import org.apache.sling.api.resource.Resource;

import com.scholastic.classmags.migration.models.FooterObject;
import com.scholastic.classmags.migration.models.SocialLinks;

/**
 * The Interface FooterService.
 */
public interface FooterService {

    /**
     * Gets the footer objects.
     *
     */
    List< FooterObject > getFooterObjects( Resource resource, String appName );

    /**
     * Gets the social links.
     *
     */
    SocialLinks getSocialLinks( Resource resource, String appName );
}
