package com.scholastic.classmags.migration.services.impl;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.MagazinePropsConstants;

/**
 * The Class MagazinePropsImpl.
 */
@Component( immediate = true, metatype = true, label = "Configure magazine names" )
@Service( value = MagazineProps.class )
@Properties( {

        @Property( label = MagazinePropsConstants.SCIENCE_WORLD_NAME, name = MagazinePropsConstants.SCIENCE_WORLD, value = MagazinePropsConstants.SCIENCE_WORLD ),
        @Property( label = MagazinePropsConstants.SUPERSCIENCE_NAME, name = MagazinePropsConstants.SUPERSCIENCE, value = MagazinePropsConstants.SUPERSCIENCE ),
        @Property( label = MagazinePropsConstants.ACTION_NAME, name = MagazinePropsConstants.ACTION, value = MagazinePropsConstants.ACTION ),
        @Property( label = MagazinePropsConstants.ART_NAME, name = MagazinePropsConstants.ART, value = MagazinePropsConstants.ART ),
        @Property( label = MagazinePropsConstants.CHOICES_NAME, name = MagazinePropsConstants.CHOICES, value = MagazinePropsConstants.CHOICES ),
        @Property( label = MagazinePropsConstants.DYNA_MATH_NAME, name = MagazinePropsConstants.DYNA_MATH, value = MagazinePropsConstants.DYNA_MATH ),
        @Property( label = MagazinePropsConstants.GEOGRAPHY_SPIN_NAME, name = MagazinePropsConstants.GEOGRAPHY_SPIN, value = MagazinePropsConstants.GEOGRAPHY_SPIN ),
        @Property( label = MagazinePropsConstants.JUNIOR_SCHOLASTIC_NAME, name = MagazinePropsConstants.JUNIOR_SCHOLASTIC, value = MagazinePropsConstants.JUNIOR_SCHOLASTIC ),
        @Property( label = MagazinePropsConstants.LETS_FIND_OUT_NAME, name = MagazinePropsConstants.LETS_FIND_OUT, value = MagazinePropsConstants.LETS_FIND_OUT ),
        @Property( label = MagazinePropsConstants.MATH_NAME, name = MagazinePropsConstants.MATH, value = MagazinePropsConstants.MATH ),
        @Property( label = MagazinePropsConstants.MY_BIG_WORLD_NAME, name = MagazinePropsConstants.MY_BIG_WORLD, value = MagazinePropsConstants.MY_BIG_WORLD ),
        @Property( label = MagazinePropsConstants.SCHOLASTIC_NEWS_ONE_NAME, name = MagazinePropsConstants.SCHOLASTIC_NEWS_ONE, value = MagazinePropsConstants.SCHOLASTIC_NEWS_ONE ),
        @Property( label = MagazinePropsConstants.SCHOLASTIC_NEWS_TWO_NAME, name = MagazinePropsConstants.SCHOLASTIC_NEWS_TWO, value = MagazinePropsConstants.SCHOLASTIC_NEWS_TWO ),
        @Property( label = MagazinePropsConstants.SCHOLASTIC_NEWS_THREE_NAME, name = MagazinePropsConstants.SCHOLASTIC_NEWS_THREE, value = MagazinePropsConstants.SCHOLASTIC_NEWS_THREE ),
        @Property( label = MagazinePropsConstants.SCHOLASTIC_NEWS_FOUR_NAME, name = MagazinePropsConstants.SCHOLASTIC_NEWS_FOUR, value = MagazinePropsConstants.SCHOLASTIC_NEWS_FOUR ),
        @Property( label = MagazinePropsConstants.SCHOLASTIC_NEWS_FIVE_SIX_NAME, name = MagazinePropsConstants.SCHOLASTIC_NEWS_FIVE_SIX, value = MagazinePropsConstants.SCHOLASTIC_NEWS_FIVE_SIX ),
        @Property( label = MagazinePropsConstants.SCIENCE_SPIN_K_ONE_NAME, name = MagazinePropsConstants.SCIENCE_SPIN_K_ONE, value = MagazinePropsConstants.SCIENCE_SPIN_K_ONE ),
        @Property( label = MagazinePropsConstants.SCIENCE_SPIN_TWO_NAME, name = MagazinePropsConstants.SCIENCE_SPIN_TWO, value = MagazinePropsConstants.SCIENCE_SPIN_TWO ),
        @Property( label = MagazinePropsConstants.SCIENCE_SPIN_THREE_TO_SIX_NAME, name = MagazinePropsConstants.SCIENCE_SPIN_THREE_TO_SIX, value = MagazinePropsConstants.SCIENCE_SPIN_THREE_TO_SIX ),
        @Property( label = MagazinePropsConstants.SCOPE_NAME, name = MagazinePropsConstants.SCOPE, value = MagazinePropsConstants.SCOPE ),
        @Property( label = MagazinePropsConstants.STORYWORKS_NAME, name = MagazinePropsConstants.STORYWORKS, value = MagazinePropsConstants.STORYWORKS ),
        @Property( label = MagazinePropsConstants.STORYWORKS_JUNIOR_NAME, name = MagazinePropsConstants.STORYWORKS_JUNIOR, value = MagazinePropsConstants.STORYWORKS_JUNIOR ),
        @Property( label = MagazinePropsConstants.SCHOLASTIC_NEWS, name = MagazinePropsConstants.SCHOLASTIC_NEWS, value = MagazinePropsConstants.SCHOLASTIC_NEWS ),
        @Property( label = MagazinePropsConstants.UPFRONT_NAME, name = MagazinePropsConstants.UPFRONT, value = MagazinePropsConstants.UPFRONT ) })
public class MagazinePropsImpl implements MagazineProps {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( MagazinePropsImpl.class );

    /** The magazine props map. */
    private Map< String, String > magazinePropsMap;

    /** The magazine name map. */
    private Map< String, String > magazineNameMap;

    /**
     * Activate.
     * 
     * @param componentContext
     *            the component context
     */
    protected final void activate( final ComponentContext componentContext ) {
        LOG.debug( "************************ACTIVATE METHOD CALLED*******************************" );

        Dictionary< ?, ? > props = componentContext.getProperties();

        populateMagazinePropsMap( props );
        populateMagazineNameMap( props );
    }

    /**
     * Populate magazine name map.
     *
     * @param props
     *            the props
     */
    private void populateMagazineNameMap( Dictionary< ?, ? > props ) {

        magazineNameMap = new HashMap< >();

        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.SCIENCE_WORLD ),
                MagazinePropsConstants.SCIENCE_WORLD_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.SUPERSCIENCE ),
                MagazinePropsConstants.SUPERSCIENCE_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.ACTION ),
                MagazinePropsConstants.ACTION_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.ART ), MagazinePropsConstants.ART_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.CHOICES ),
                MagazinePropsConstants.CHOICES_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.DYNA_MATH ),
                MagazinePropsConstants.DYNA_MATH_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.GEOGRAPHY_SPIN ),
                MagazinePropsConstants.GEOGRAPHY_SPIN_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.JUNIOR_SCHOLASTIC ),
                MagazinePropsConstants.JUNIOR_SCHOLASTIC_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.LETS_FIND_OUT ),
                MagazinePropsConstants.LETS_FIND_OUT_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.MATH ), MagazinePropsConstants.MATH_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.MY_BIG_WORLD ),
                MagazinePropsConstants.MY_BIG_WORLD_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.SCHOLASTIC_NEWS_ONE ),
                MagazinePropsConstants.SCHOLASTIC_NEWS_ONE_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.SCHOLASTIC_NEWS_TWO ),
                MagazinePropsConstants.SCHOLASTIC_NEWS_TWO_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.SCHOLASTIC_NEWS_THREE ),
                MagazinePropsConstants.SCHOLASTIC_NEWS_THREE_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.SCHOLASTIC_NEWS_FOUR ),
                MagazinePropsConstants.SCHOLASTIC_NEWS_FOUR_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.SCHOLASTIC_NEWS_FIVE_SIX ),
                MagazinePropsConstants.SCHOLASTIC_NEWS_FIVE_SIX_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.SCIENCE_SPIN_K_ONE ),
                MagazinePropsConstants.SCIENCE_SPIN_K_ONE_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.SCIENCE_SPIN_TWO ),
                MagazinePropsConstants.SCIENCE_SPIN_TWO_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.SCIENCE_SPIN_THREE_TO_SIX ),
                MagazinePropsConstants.SCIENCE_SPIN_THREE_TO_SIX_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.SCOPE ), MagazinePropsConstants.SCOPE_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.STORYWORKS ),
                MagazinePropsConstants.STORYWORKS_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.STORYWORKS_JUNIOR ),
                MagazinePropsConstants.STORYWORKS_JUNIOR_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.UPFRONT ),
                MagazinePropsConstants.UPFRONT_NAME );
        magazineNameMap.put( ( String ) props.get( MagazinePropsConstants.SCHOLASTIC_NEWS ),
                MagazinePropsConstants.SCHOLASTIC_NEWS );
    }

    /**
     * Populate magazine props map.
     *
     * @param props
     *            the props
     */
    private void populateMagazinePropsMap( Dictionary< ?, ? > props ) {

        magazinePropsMap = new HashMap< >();

        magazinePropsMap.put( MagazinePropsConstants.SCIENCE_WORLD,
                ( String ) props.get( MagazinePropsConstants.SCIENCE_WORLD ) );
        magazinePropsMap.put( MagazinePropsConstants.SUPERSCIENCE,
                ( String ) props.get( MagazinePropsConstants.SUPERSCIENCE ) );
        magazinePropsMap.put( MagazinePropsConstants.ACTION, ( String ) props.get( MagazinePropsConstants.ACTION ) );
        magazinePropsMap.put( MagazinePropsConstants.ART, ( String ) props.get( MagazinePropsConstants.ART ) );
        magazinePropsMap.put( MagazinePropsConstants.CHOICES, ( String ) props.get( MagazinePropsConstants.CHOICES ) );
        magazinePropsMap.put( MagazinePropsConstants.DYNA_MATH,
                ( String ) props.get( MagazinePropsConstants.DYNA_MATH ) );
        magazinePropsMap.put( MagazinePropsConstants.GEOGRAPHY_SPIN,
                ( String ) props.get( MagazinePropsConstants.GEOGRAPHY_SPIN ) );
        magazinePropsMap.put( MagazinePropsConstants.JUNIOR_SCHOLASTIC,
                ( String ) props.get( MagazinePropsConstants.JUNIOR_SCHOLASTIC ) );
        magazinePropsMap.put( MagazinePropsConstants.LETS_FIND_OUT,
                ( String ) props.get( MagazinePropsConstants.LETS_FIND_OUT ) );
        magazinePropsMap.put( MagazinePropsConstants.MATH, ( String ) props.get( MagazinePropsConstants.MATH ) );
        magazinePropsMap.put( MagazinePropsConstants.MY_BIG_WORLD,
                ( String ) props.get( MagazinePropsConstants.MY_BIG_WORLD ) );
        magazinePropsMap.put( MagazinePropsConstants.SCHOLASTIC_NEWS_ONE,
                ( String ) props.get( MagazinePropsConstants.SCHOLASTIC_NEWS_ONE ) );
        magazinePropsMap.put( MagazinePropsConstants.SCHOLASTIC_NEWS_TWO,
                ( String ) props.get( MagazinePropsConstants.SCHOLASTIC_NEWS_TWO ) );
        magazinePropsMap.put( MagazinePropsConstants.SCHOLASTIC_NEWS_THREE,
                ( String ) props.get( MagazinePropsConstants.SCHOLASTIC_NEWS_THREE ) );
        magazinePropsMap.put( MagazinePropsConstants.SCHOLASTIC_NEWS_FOUR,
                ( String ) props.get( MagazinePropsConstants.SCHOLASTIC_NEWS_FOUR ) );
        magazinePropsMap.put( MagazinePropsConstants.SCHOLASTIC_NEWS_FIVE_SIX,
                ( String ) props.get( MagazinePropsConstants.SCHOLASTIC_NEWS_FIVE_SIX ) );
        magazinePropsMap.put( MagazinePropsConstants.SCIENCE_SPIN_K_ONE,
                ( String ) props.get( MagazinePropsConstants.SCIENCE_SPIN_K_ONE ) );
        magazinePropsMap.put( MagazinePropsConstants.SCIENCE_SPIN_TWO,
                ( String ) props.get( MagazinePropsConstants.SCIENCE_SPIN_TWO ) );
        magazinePropsMap.put( MagazinePropsConstants.SCIENCE_SPIN_THREE_TO_SIX,
                ( String ) props.get( MagazinePropsConstants.SCIENCE_SPIN_THREE_TO_SIX ) );
        magazinePropsMap.put( MagazinePropsConstants.SCOPE, ( String ) props.get( MagazinePropsConstants.SCOPE ) );
        magazinePropsMap.put( MagazinePropsConstants.STORYWORKS,
                ( String ) props.get( MagazinePropsConstants.STORYWORKS ) );
        magazinePropsMap.put( MagazinePropsConstants.STORYWORKS_JUNIOR,
                ( String ) props.get( MagazinePropsConstants.STORYWORKS_JUNIOR ) );
        magazinePropsMap.put( MagazinePropsConstants.UPFRONT, ( String ) props.get( MagazinePropsConstants.UPFRONT ) );
        magazinePropsMap.put( ( String ) props.get( MagazinePropsConstants.SCHOLASTIC_NEWS ),
                MagazinePropsConstants.SCHOLASTIC_NEWS );
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.scholastic.classmags.migration.services.MagazineProps#getMagazineName
     * (java.lang.String)
     */
    @Override
    public String getMagazineName( final String currentPagePath ) {
        for ( Map.Entry< String, String > entry : magazinePropsMap.entrySet() ) {
            if ( StringUtils.contains( currentPagePath, "/" + entry.getValue() ) ) {
                return entry.getKey();
            }
        }
        return StringUtils.EMPTY;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.services.MagazineProps#
     * getMagazineActualName(java.lang.String)
     */
    @Override
    public String getMagazineActualName( final String magazineNameKey ) {
        return magazineNameMap.get( magazineNameKey );
    }

    /**
     * DEACTIVATE.
     */
    protected void deactivate() {
        LOG.debug( "************************DEACTIVATE METHOD CALLED*******************************" );
        this.magazinePropsMap = null;
    }
}
