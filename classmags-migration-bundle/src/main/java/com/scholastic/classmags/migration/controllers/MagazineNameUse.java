package com.scholastic.classmags.migration.controllers;

import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.settings.SlingSettingsService;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * Use Class for Magazine Name.
 */
public class MagazineNameUse extends WCMUsePojo {

    private String magazineName;

    private boolean prod;
    
    @Override
    public void activate() throws Exception {
        String currentPath = get( "currentPath", String.class );
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        if ( null != slingScriptHelper ) {
            MagazineProps magazineProps = slingScriptHelper.getService( MagazineProps.class );
            SlingSettingsService slingSettingsService = slingScriptHelper.getService( SlingSettingsService.class );
            prod = CommonUtils.isRunMode(slingSettingsService.getRunModes(), "prod");
            if ( null != magazineProps ) {
                magazineName = magazineProps.getMagazineName( currentPath );
            }
        }
    }

    /**
     * Gets the magazine name.
     *
     * @return the magazine name
     */
    public String getMagazineName() {
        return magazineName;
    }

	public boolean isProd() {
		return prod;
	}

}