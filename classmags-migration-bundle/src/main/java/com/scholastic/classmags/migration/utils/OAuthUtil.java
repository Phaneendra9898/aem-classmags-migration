package com.scholastic.classmags.migration.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import com.scholastic.classmags.migration.exception.OAuthException;

public class OAuthUtil {

    public static final String CONSUMER_KEY_PARAM = "oauth_consumer_key";
    public static final String TOKEN_PARAM = "oauth_token";
    public static final String SIGNATURE_METHOD_PARAM = "oauth_signature_method";
    public static final String TIMESTAMP_PARAM = "oauth_timestamp";
    public static final String NONCE_PARAM = "oauth_nonce";
    public static final String VERSION_PARAM = "oauth_version";
    public static final String SIGNATURE_PARAM = "oauth_signature";
    public static final String BODY_HASH_PARAM = "oauth_body_hash";

    public static final String AUTH_SCHEME = "OAuth";
    public static final String ENCODING = "UTF-8";
    public static final String AMPERSAND = "&";
    public static final String EQUAL = "=";

    public static final String OAUTH_POST_BODY_PARAMETER = "oauth_body_hash";

    private static final Map<String, String> algorithms;
    
    private static String defaultEncoding;

    static {
        algorithms = new HashMap<>();
        algorithms.put("HMAC-SHA1", "HmacSHA1");
    }

    private OAuthUtil() {
    }

    public static String percentEncode(String string) {
        if (string == null) {
            return "";
        }
        try {
            return URLEncoder.encode(string, getDefaultEncoding())
                    // OAuth encodes some characters differently:
                    .replace("+", "%20").replace("*", "%2A").replace("%7E", "~");
        } catch (UnsupportedEncodingException uee) {
            throw new OAuthException(uee.getMessage(), uee);
        }
    }

    public static String getDefaultEncoding() {
        if (defaultEncoding == null) {
            defaultEncoding = ENCODING;
        }
        return defaultEncoding;
    }

    public static void setDefaultEncoding(String encoding) {
        OAuthUtil.defaultEncoding = encoding;
    }

}