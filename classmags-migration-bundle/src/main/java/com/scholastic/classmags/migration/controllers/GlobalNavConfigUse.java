package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.models.NavigationLevel;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

public class GlobalNavConfigUse extends WCMUsePojo {

    private static final String NAVIGATION_TYPE_SECONDARY = "secondary";
    private static final String NAVIGATION_TYPE_TERTIARY = "tertiary";
    
    private String primaryNavLink;
    private Map< String, List< NavigationLevel > > secondaryLevelsMap;
    private List< NavigationLevel > secondaryLevels;

    @Override
    public void activate() {
        secondaryLevels = new ArrayList< >();
        List< NavigationLevel > allLevels = new ArrayList< >();
        secondaryLevelsMap = new HashMap< >();

        final Resource resource = getResource();
        final Resource childNavResource = resource.getChild( "childNavSettings" );
        final ResourceResolver resourceResolver = resource.getResourceResolver();
        String primaryNavLinkUnmapped = getProperties().get("primaryNavLink", String.class);
        if ( null != primaryNavLinkUnmapped && !StringUtils.isEmpty( primaryNavLinkUnmapped ) ) {
            primaryNavLink = InternalURLFormatter.formatURL( resourceResolver, primaryNavLinkUnmapped );
        }
        if(null!=childNavResource){
            final Iterator< Resource > iterator = childNavResource.listChildren();

            while ( iterator.hasNext() ) {
                Resource item = iterator.next();
                NavigationLevel nav = createNavigationLevel( item );
                allLevels.add( nav );
                if ( StringUtils.equals( nav.getNavigationType(), NAVIGATION_TYPE_SECONDARY ) ) {
                    secondaryLevels.add( nav );
                }
            }
        }
        final ListIterator< NavigationLevel > navLevelIterator = allLevels.listIterator();
        while ( navLevelIterator.hasNext() ) {
            NavigationLevel currentItem = navLevelIterator.next();
            if ( StringUtils.equals( currentItem.getNavigationType(), NAVIGATION_TYPE_SECONDARY ) ) {
                ListIterator< NavigationLevel > nextIterator = allLevels.listIterator( navLevelIterator.nextIndex() );
                List< NavigationLevel > tertiaryItems = createTertiaryItems( nextIterator );
                secondaryLevelsMap.put( currentItem.getLinkTitle(), tertiaryItems );
            }
        }
    }

    /**
     * Creates the list of tertiary items.
     *
     * @param nextIterator The list iterator.
     *
     * @return the tertiary items list
     */
    private List< NavigationLevel > createTertiaryItems( ListIterator< NavigationLevel > nextIterator ) {
        List< NavigationLevel > tertiaryItems = new ArrayList< >();
        while ( nextIterator.hasNext() ) {
            NavigationLevel nextItem = nextIterator.next();
            if ( StringUtils.equals( nextItem.getNavigationType(), NAVIGATION_TYPE_TERTIARY ) ) {
                tertiaryItems.add( nextItem );
            } else {
                break;
            }
        }
        return tertiaryItems;
    }
    
    /**
     * Creates the navigation level.
     *
     * @param res
     *            The resource.
     *
     * @return the navigation level
     */
    private NavigationLevel createNavigationLevel( Resource res ) {

        ValueMap itemProperties = res.getValueMap();
        NavigationLevel nav = new NavigationLevel();
        nav.setLinkTitle( itemProperties.get( "childTitle", String.class ) );
        ResourceResolver resourceResolver = res.getResourceResolver();
        String unmappedPath = itemProperties.get( "childLink", String.class );
        if( null != unmappedPath && !StringUtils.isEmpty( unmappedPath )){
        String mappedPath = InternalURLFormatter.formatURL(resourceResolver, unmappedPath);
        nav.setLinkPath( mappedPath );
        }
        nav.setNavigationType( itemProperties.get( "navType", String.class ) );

        return nav;
    }

    /**
     * Gets the secondary levels map.
     *
     * @return the secondary levels map
     */
    public final Map< String, List< NavigationLevel > > getSecondaryLevelsMap() {
        return secondaryLevelsMap;
    }

    /**
     * Gets the secondary levels.
     *
     * @return the secondary levels.
     */
    public final List< NavigationLevel > getSecondaryLevels() {
        return secondaryLevels;
    }
    
    /**
     * Gets the primary nav link.
     *
     * @return the primary nav link.
     */
    public final String getPrimaryNavLink() {
        return primaryNavLink;
    }
}
