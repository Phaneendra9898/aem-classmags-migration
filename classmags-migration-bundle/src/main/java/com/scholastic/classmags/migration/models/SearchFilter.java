package com.scholastic.classmags.migration.models;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;

@Model( adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL )
public class SearchFilter {

    @Inject
    private String contentType;

    @Inject
    private String userType;
    
    private String id;
    
    private boolean render;
    
    @PostConstruct
    public final void init() {
        setId( contentType.replaceAll( " ", "-" ) );
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType( String contentType ) {
        this.contentType = contentType;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType( String userType ) {
        this.userType = userType;
    }

    public String getId() {
        return id;
    }

    public void setId( String id ) {
        this.id = id;
    }

    public boolean getRender() {
        return render;
    }

    public void setRender( boolean render ) {
        this.render = render;
    }

    @Override
    public String toString() {
        return "SearchFilter [contentType=" + contentType + ", userType=" + userType + ", id=" + id + ", render="
                + render + "]";
    }
    
}