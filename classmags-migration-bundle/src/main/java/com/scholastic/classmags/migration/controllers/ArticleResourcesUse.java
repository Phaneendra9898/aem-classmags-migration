package com.scholastic.classmags.migration.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.services.TeachingResourcesService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * Use Class for Article Resources.
 */
public class ArticleResourcesUse extends WCMUsePojo {

    /** The teaching resources. */
    private Map< String, List< ResourcesObject > > teachingResources = new HashMap< >();
    
    @Override
    public void activate() {
        TeachingResourcesService teachingResourcesService;
        Map< String, List< ResourcesObject > > customTeachingResources;
        Resource currentPageResource;
        Resource articleConfigResource = null;
        Page currentPage = getCurrentPage();

        currentPageResource = currentPage.getContentResource();
        if ( null != currentPageResource ) {
            articleConfigResource = currentPageResource.getChild( "article-configuration" );
        }
        if ( null != articleConfigResource ) {
            teachingResourcesService = getSlingScriptHelper().getService( TeachingResourcesService.class );
            this.teachingResources = ( teachingResourcesService != null )
                    ? teachingResourcesService.getTeachingResources( getRequest(), articleConfigResource, ClassMagsMigrationConstants.RESOURCES ) : null;
            customTeachingResources = ( teachingResourcesService != null )
                    ? teachingResourcesService.getTeachingResources( getRequest(), articleConfigResource, ClassMagsMigrationConstants.CUSTOM_RESOURCES )
                    : null;

            if ( null != customTeachingResources ) {
                this.teachingResources = teachingResourcesService.createResourcesMap( teachingResources,
                        customTeachingResources );
            }
        }
        
    }

    /**
     * Gets the teaching resources.
     * 
     * @return the teachingResources
     */
    public Map< String, List< ResourcesObject > > getTeachingResources() {
        return teachingResources;
    }

}
