package com.scholastic.classmags.migration.services.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.osgi.framework.Constants;

import com.scholastic.classmags.migration.services.SlideshowDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

@Component( immediate = true, metatype = false )
@Service( SlideshowDataService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Slideshow Data Service" ) } )
public class SlideshowDataServiceImpl implements SlideshowDataService {
    
    @Reference
    private ResourceResolverFactory resourceResolverFactory;
    
    /**
     * Convert list to JSON array.
     *
     * @param fetchMultiFieldData
     *            the fetch multi field data
     * @throws JSONException
     *             the JSON exception
     */
    @Override
    public JSONArray convertListToJSONArray( List< ValueMap > fetchMultiFieldData ) throws JSONException {
        JSONArray slideshowJsonArray = new JSONArray();
        String propertyValue = null;

        for ( ValueMap properties : fetchMultiFieldData ) {
            JSONObject slideshowJsonObject = new JSONObject();
            slideshowJsonObject.put( ClassMagsMigrationConstants.SLIDESHOW_IMAGE_PATH,
                    properties != null ? InternalURLFormatter.formatURL(
                            CommonUtils.getResourceResolver( resourceResolverFactory,
                                    ClassMagsMigrationConstants.READ_SERVICE ),
                            properties.get( ClassMagsMigrationConstants.SLIDESHOW_IMAGE_PATH, StringUtils.EMPTY ) )
                            : propertyValue );
            slideshowJsonObject.put( ClassMagsMigrationConstants.SLIDESHOW_TITLE,
                    properties != null
                            ? properties.get( ClassMagsMigrationConstants.SLIDESHOW_TITLE, StringUtils.EMPTY )
                            : propertyValue );
            slideshowJsonObject.put( ClassMagsMigrationConstants.SLIDESHOW_CAPTION,
                    properties != null
                            ? properties.get( ClassMagsMigrationConstants.SLIDESHOW_CAPTION, StringUtils.EMPTY )
                            : propertyValue );
            slideshowJsonObject.put( ClassMagsMigrationConstants.SLIDESHOW_CREDITS,
                    properties != null
                            ? properties.get( ClassMagsMigrationConstants.SLIDESHOW_CREDITS, StringUtils.EMPTY )
                            : propertyValue );
            slideshowJsonObject.put( ClassMagsMigrationConstants.SLIDESHOW_AUDIO_PATH,
                    properties != null
                            ? properties.get( ClassMagsMigrationConstants.SLIDESHOW_AUDIO_PATH, StringUtils.EMPTY )
                            : propertyValue );
            slideshowJsonArray.put( slideshowJsonObject );
        }

        return slideshowJsonArray;
    }

}
