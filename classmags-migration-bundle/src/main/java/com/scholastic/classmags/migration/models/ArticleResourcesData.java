package com.scholastic.classmags.migration.models;

/**
 * The Class ArticleResourcesData.
 */
public class ArticleResourcesData {

    /** The article resource type. */
    private String articleResourceType;

    /** The article page path. */
    private String articlePagePath;

    /**
     * Gets the article resource type.
     *
     * @return the articleResourceType
     */
    public String getArticleResourceType() {
        return articleResourceType;
    }

    /**
     * Sets the article resource type.
     *
     * @param articleResourceType
     *            the articleResourceType to set
     */
    public void setArticleResourceType( String articleResourceType ) {
        this.articleResourceType = articleResourceType;
    }

    /**
     * Gets the article page path.
     *
     * @return the articlePagePath
     */
    public String getArticlePagePath() {
        return articlePagePath;
    }

    /**
     * Sets the article page path.
     *
     * @param articlePagePath
     *            the articlePagePath to set
     */
    public void setArticlePagePath( String articlePagePath ) {
        this.articlePagePath = articlePagePath;
    }
}
