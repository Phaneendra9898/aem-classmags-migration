package com.scholastic.classmags.migration.models;

import java.util.Iterator;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.jsoup.Jsoup;

@Model( adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL )
public class ArticleText {

    @Inject
    @Named( "articleTextCredits" )
    private String articleTextCredits;

    @Inject
    @Named( "articleTextcaption" )
    private String articleTextcaption;

    private String content;

    @Self
    private Resource resource;

    @PostConstruct
    public final void init() {
        content = StringUtils.EMPTY;
        if ( null != resource ) {
            Iterator< Resource > it = resource.listChildren();
            while ( it.hasNext() ) {
                Resource textResource = it.next();
                String[] text = textResource.adaptTo( ValueMap.class ).get( "title", new String[] {} );
                this.content = this.content.concat( getPlaintext( text ));
            }
        }
    }
    
    public String getPlaintext(String[] text){
        StringBuilder sb = new StringBuilder();
        if ( ArrayUtils.isNotEmpty( text ) ) {
            for ( String t : text ) {
                    sb.append( getPlaintext( t ));
            }
        }
        return sb.toString();
    }
    
    public String getPlaintext(String text){
        String plaintext = StringUtils.EMPTY;
        if(StringUtils.isNotBlank( text ) ){
            plaintext = Jsoup.parse(text.trim()).text().concat( " " );
        }
        return plaintext;
    }

    @Override
    public String toString() {
        return "ArticleText [articleTextCredits=" + getArticleTextCredits() + ", articleTextcaption=" + getArticleTextcaption()
                + ", articleContent=" + content + ", resource=" + resource + "]";
    }

    public String getArticleTextcaption() {
        return articleTextcaption;
    }

    public void setArticleTextcaption( String articleTextcaption ) {
        this.articleTextcaption = articleTextcaption;
    }

    public String getArticleTextCredits() {
        return articleTextCredits;
    }

    public void setArticleTextCredits( String articleTextCredits ) {
        this.articleTextCredits = articleTextCredits;
    }

    public String getContent() {
        return content;
    }

    public void setContent( String content ) {
        this.content = content;
    }

}
