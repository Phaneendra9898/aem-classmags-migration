package com.scholastic.classmags.migration.models;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * The Class IssueHighlight.
 */
@Model( adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL )
public class IssueHighlight extends ClassmagsMigrationBaseModel {

    /** The issue link to E reader. */
    @Inject
    @Via( "resource" )
    private String issueLinkToEReader;

    /**
     * Instantiates a new issue highlight.
     *
     * @param request
     *            the request
     */
    public IssueHighlight( SlingHttpServletRequest request ) {
        super( request );
    }

    /**
     * Gets the issue link to E reader.
     *
     * @return the issueLinkToEReader
     */
    public String getIssueLinkToEReader() {
        return InternalURLFormatter.formatURL( getRequest().getResourceResolver(), issueLinkToEReader );
    }
}
