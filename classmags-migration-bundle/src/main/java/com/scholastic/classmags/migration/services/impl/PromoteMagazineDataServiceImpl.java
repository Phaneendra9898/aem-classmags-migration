package com.scholastic.classmags.migration.services.impl;

import java.util.List;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;

import com.scholastic.classmags.migration.services.PromotMagazineDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class PromoteMagazineDataServiceImpl.
 */
@Component( immediate = true, metatype = false )
@Service( PromotMagazineDataService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Promote Magazine Data Service" ) })
public class PromoteMagazineDataServiceImpl implements PromotMagazineDataService {

    /** The pm resource resolver factory. */
    @Reference
    private ResourceResolverFactory pmResourceResolverFactory;

    /**
     * Sets the pm resource resolver factory.
     *
     * @param pmResourceResolverFactory
     *            the pmResourceResolverFactory to set
     */
    public void setPmResourceResolverFactory( ResourceResolverFactory pmResourceResolverFactory ) {
        this.pmResourceResolverFactory = pmResourceResolverFactory;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.scholastic.classmags.migration.services.PromotMagazineDataService#
     * fetchMagazineData(java.lang.String)
     */
    @Override
    public List< ValueMap > fetchMagazineData( String currentPath ) {

        Resource resource = CommonUtils
                .getResourceResolver( pmResourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE )
                .getResource( currentPath );

        return CommonUtils.fetchMultiFieldData(
                resource != null ? resource.getChild( ClassMagsMigrationConstants.MAGAZINES_DATA ) : null );
    }
}
