package com.scholastic.classmags.migration.services;

/**
 * The Interface PastIssuesService.
 */
public interface PastIssuesService {

    /**
     * Gets the past issues json.
     * 
     * @param currentPagePath
     *            the current page path
     * @return the past issues json
     */
    String getPastIssuesJson( String currentPagePath );

}
