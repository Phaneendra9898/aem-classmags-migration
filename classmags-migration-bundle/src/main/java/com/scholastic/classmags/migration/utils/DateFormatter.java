package com.scholastic.classmags.migration.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * The Class DateFormatter.
 */
public final class DateFormatter {

    /** The Constant ABBREVIATED_MONTH. */
    private static final String ABBREVIATED_MONTH = "MMM YYYY";

    /** The Constant ABBREVIATED_MONTH_DAY. */
    private static final String ABBREVIATED_MONTH_DAY = "MMM d, YYYY";

    /** The Constant BEGIN_INDEX. */
    private static final int BEGIN_INDEX = 0;

    /** The Constant END_INDEX. */
    private static final int END_INDEX = 3;

    /** The Constant INPUT_ABB_MONTHS. */
    private static final String[] INPUT_ABB_MONTHS = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep",
            "Oct", "Nov", "Dec" };

    /** The Constant CUSTOM_ABB_MONTHS. */
    private static final String[] CUSTOM_ABB_MONTHS = { "Jan", "Feb", "March", "April", "May", "June", "July", "Aug",
            "Sept", "Oct", "Nov", "Dec" };

    /**
     * Instantiates a new date formatter.
     */
    private DateFormatter() {
    }
    
    /**
     * Format sorting date.
     *
     * @param dateFormat
     *            the date format
     * @param inputDate
     *            the input date
     * @return the string
     */
    public static String formatSortingDate( String dateFormat, String inputDate ) {

        String formattedSortingDate = inputDate;

        String customMonth;

        if ( StringUtils.equals( ABBREVIATED_MONTH, dateFormat )
                || StringUtils.equals( ABBREVIATED_MONTH_DAY, dateFormat ) ) {

            customMonth = formatAbbreviatedMonth( inputDate );
            formattedSortingDate = customMonth.concat( inputDate.substring( END_INDEX ) );
        }
        return formattedSortingDate;
    }

    /**
     * Format abbreviated month.
     *
     * @param inputDate
     *            the input date
     * @return the string
     */
    private static String formatAbbreviatedMonth( String inputDate ) {
        String extractedMonth = inputDate.substring( BEGIN_INDEX, END_INDEX );
        for ( int i = 0; i < INPUT_ABB_MONTHS.length; i++ ) {
            if ( StringUtils.equals( INPUT_ABB_MONTHS[ i ], extractedMonth ) ) {
                extractedMonth = CUSTOM_ABB_MONTHS[ i ];
            }
        }
        return extractedMonth;
    }
}
