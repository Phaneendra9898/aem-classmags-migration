package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.models.BlogCategoriesData;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class BlogCategoriesUse.
 */
public class BlogCategoriesUse extends WCMUsePojo {

    /** The blog categories data list. */
    private List< BlogCategoriesData > blogCategoriesDataList;

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() {

        String currentPath = get( ClassMagsMigrationConstants.URL, String.class );

        if ( StringUtils.isNotBlank( currentPath ) ) {
            Resource blogCategoriesResource = getResourceResolver().getResource( currentPath );

            List< ValueMap > blogCategoriesList = CommonUtils.fetchMultiFieldData( blogCategoriesResource,
                    ClassMagsMigrationConstants.BLOG_CATEGORIES_NODE );

            blogCategoriesDataList = new ArrayList<>();

            for ( ValueMap blogCategoriesMap : blogCategoriesList ) {
                BlogCategoriesData blogCategoriesData = new BlogCategoriesData();
                blogCategoriesData.setBlogCategoryTitle(
                        blogCategoriesMap.get( ClassMagsMigrationConstants.BLOG_CATEGORY_TITLE, StringUtils.EMPTY ) );
                blogCategoriesData.setBlogCategoryLink(
                        blogCategoriesMap.get( ClassMagsMigrationConstants.BLOG_CATEGORY_LINK, StringUtils.EMPTY ) );
                blogCategoriesDataList.add( blogCategoriesData );
            }
        }
    }

    /**
     * Gets the blog categories data list.
     *
     * @return the blogCategoriesDataList
     */
    public List< BlogCategoriesData > getBlogCategoriesDataList() {
        return blogCategoriesDataList;
    }
}
