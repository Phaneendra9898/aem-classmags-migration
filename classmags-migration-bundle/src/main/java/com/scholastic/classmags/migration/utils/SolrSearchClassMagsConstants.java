package com.scholastic.classmags.migration.utils;

/**
 * Search Constants
 * 
 * @author diego.vilchis
 *
 */
public class SolrSearchClassMagsConstants {
    /**
     * Solr configuration service
     * com.scholastic.foundation.services.impl.SolrConfigurationService
     * constants
     **/
    public static final String PROTOCOL = "solr.protocol";
    public static final String SERVER_NAME = "solr.server.name";
    public static final String SERVER_PORT = "solr.server.port";
    public static final String ZOOKEEPER = "solr.zookeeper";
    public static final String CONTEXT_PATH = "solr.context.path";
    public static final String PROXY_ENABLED = "solr.proxy.enabled";
    public static final String PROXY_URL = "solr.proxy.url";
    public static final String AEM_SOLR_CORE = "aem.solr.core";
    public static final String DEFAULT_PROTOCOL = "http";
    public static final String DEFAULT_SERVER_NAME = "localhost";
    public static final String DEFAULT_SERVER_PORT = "2181";
    public static final String DEFAULT_CONTEXT_PATH = "/solr";
    public static final boolean DEFAULT_PROXY_ENABLED = false;
    public static final String DEFAULT_PROXY_URL = "http://localhost:4502/apps/solr/proxy";
    public static final String DEFAULT_AEM_SOLR_CORE = "global";
    public static final String CONNECTION_TIME_OUT = "solr.connection.timeout";
    public static final String DEFAULT_CONNECTION_TIME_OUT ="60000";

    public static final String ENABLED = "com.scholastic.classmags.migration.services.impl.DefaultSolrIndexListenerService.enabled";
    public static final String OBSERVED_PATHS = "com.scholastic.classmags.migration.services.impl.DefaultSolrIndexListenerService.observed.paths";

    public static final String CONSTANT_COLON = ":";
    public static final String CONSTANT_COMMA = ",";
    
    public static final String FIELD_TYPE_ARTICLE = "Article";
    public static final String FIELD_TYPE_ISSUE = "Issue";
    public static final String FIELD_TYPE_PAGE = "Page";
    public static final String FIELD_TYPE_SKILL_SHEET = "Skills Sheets";
    public static final String FIELD_TYPE_VIDEO = "Video";
    public static final String FIELD_TYPE_LESSON_PLAN = "Lesson Plan";
    
    
    public static final String SORL_ESPECIAL_CHARACTERS_PATTERN = "(\\+|-|&&|\\|\\||\\!|\\(|\\)|\\{|\\}|\\[|\\]|\\^|\"|~|\\*|\\?|:|\\/)";
    public static final String BACKSLASH = "\\";
    public static final String INPUT_TEXT_REG_EX = "\\s+";
    public static final String SOLR_QUERY_WILDCARD = "*";
    public static final String SOLR_QUERY_AND = "AND";
    public static final String SOLR_QUERY_OR= "OR";
    public static final String SOLR_QUERY_NOT = "-";
    
    public static final String OP_INDEX_UPDATE = "update";
    public static final String OP_INDEX_DELETE = "delete";
    
    public static final String CM_GLOBAL_TAG = "classroom-magazines:global";
    
}
