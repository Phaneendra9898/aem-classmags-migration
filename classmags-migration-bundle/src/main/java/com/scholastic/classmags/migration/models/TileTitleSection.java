package com.scholastic.classmags.migration.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * The Class Tile Title Section.
 */
public class TileTitleSection {

    /** The tiletitletext. */
    private String tiletitletext;

    /** The tiletitlelinkto. */
    private String tiletitlelinkto;

    public String getTiletitletext() {
        return tiletitletext;
    }

    public void setTiletitletext( String tiletitletext ) {
        this.tiletitletext = tiletitletext;
    }
    
    /**
     * @return the tiletitlelinkto
     */
    public String getTiletitlelinkto() {
        return tiletitlelinkto;
    }

    /**
     * @param tiletitlelinkto the tiletitlelinkto to set
     */
    public void setTiletitlelinkto( String tiletitlelinkto ) {
        this.tiletitlelinkto = tiletitlelinkto;
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString( this );
    }

}
