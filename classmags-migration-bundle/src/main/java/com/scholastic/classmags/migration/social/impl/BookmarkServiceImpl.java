package com.scholastic.classmags.migration.social.impl;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.apache.sling.xss.XSSAPI;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.social.scf.OperationException;
import com.adobe.cq.social.scf.SocialComponentFactoryManager;
import com.adobe.cq.social.srp.SocialResourceProvider;
import com.adobe.cq.social.ugcbase.CollabUser;
import com.adobe.cq.social.ugcbase.SocialUtils;
import com.day.cq.commons.jcr.JcrConstants;
import com.scholastic.classmags.migration.models.ClassmagsMigrationErrorCodes;
import com.scholastic.classmags.migration.social.api.BookmarkService;
import com.scholastic.classmags.migration.social.api.BookmarkSocialComponent;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.RoleUtil;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;

/**
 * The Class BookmarkServiceImpl.
 */
@SuppressWarnings( "deprecation" )
@Component( immediate = true, metatype = false )
@Service( BookmarkService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Bookmark Service" ) })
public class BookmarkServiceImpl implements BookmarkService {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( BookmarkServiceImpl.class );

    /** The resource resolver factory. */
    @Reference( cardinality = ReferenceCardinality.MANDATORY_UNARY, policy = ReferencePolicy.STATIC )
    private ResourceResolverFactory resourceResolverFactory;

    /** The social utils. */
    @Reference
    private SocialUtils socialUtils;

    /** The scf manager. */
    @Reference
    private SocialComponentFactoryManager scfManager;

    /** The xssapi. */
    @Reference
    private XSSAPI xssapi;

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.social.api.BookmarkService#
     * getBookmarkResource(org.apache.sling.api.SlingHttpServletRequest)
     */
    @Override
    public Resource getBookmarkResource( SlingHttpServletRequest slingHttpServletRequest ) throws OperationException {

        String userRole = RoleUtil.getUserRole( slingHttpServletRequest );
        String userId = SocialASRPUtils.fetchUserId( slingHttpServletRequest, userRole );

        if ( !SocialASRPUtils.isValidUser( userRole, userId ) ) {
            throw new OperationException(
                    ClassmagsMigrationErrorCodes.INVALID_USER_EXCEPTION_MESSAGE.getErrorDescription(),
                    ClassmagsMigrationErrorCodes.INVALID_USER_EXCEPTION_MESSAGE.getErrorCodeInt() );
        }

        final Resource resource = slingHttpServletRequest.getResource();
        final ResourceResolver resolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE );

        return createBookmarkData( resource, slingHttpServletRequest, resolver, userRole.concat( userId ) );
    }

    /**
     * Creates the bookmark data.
     *
     * @param resource
     *            the resource
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @param resolver
     *            the resolver
     * @param userInfo
     *            the user info
     * @return the resource
     * @throws OperationException
     *             the operation exception
     */
    private Resource createBookmarkData( Resource resource, SlingHttpServletRequest slingHttpServletRequest,
            ResourceResolver resolver, String userInfo ) throws OperationException {

        try {
            if ( !socialUtils.mayAccessUGC( resolver ) ) {
                throw new OperationException(
                        userInfo + ClassmagsMigrationErrorCodes.PERMISSION_DENIED_MESSAGE.getErrorDescription(),
                        ClassmagsMigrationErrorCodes.PERMISSION_DENIED_MESSAGE.getErrorCodeInt() );
            }
            return createOrUpdateUGCResource( slingHttpServletRequest, resource, userInfo, resolver );

        } catch ( PersistenceException | UnsupportedEncodingException | JSONException e ) {
            LOG.error( ClassmagsMigrationErrorCodes.ADD_BOOKMARK_ERROR_MESSAGE.getErrorDescription(), e );
            throw new OperationException( ClassmagsMigrationErrorCodes.ADD_BOOKMARK_ERROR_MESSAGE.getErrorDescription(),
                    ClassmagsMigrationErrorCodes.ADD_BOOKMARK_ERROR_MESSAGE.getErrorCodeInt() );
        }
    }

    /**
     * Creates the or update UGC resource.
     *
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @param resource
     *            the resource
     * @param userInfo
     *            the user info
     * @param resolver
     *            the resolver
     * @return the resource
     * @throws OperationException
     *             the operation exception
     * @throws PersistenceException
     *             the persistence exception
     * @throws UnsupportedEncodingException
     *             the unsupported encoding exception
     * @throws JSONException
     *             the JSON exception
     */
    private Resource createOrUpdateUGCResource( SlingHttpServletRequest slingHttpServletRequest, Resource resource,
            String userInfo, ResourceResolver resolver )
                    throws OperationException, PersistenceException, UnsupportedEncodingException, JSONException {

        final String bookmarkedPagePath = xssapi.filterHTML(
                slingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.BOOKMARK_PAGE_PATH ) );
        final String bookmarkedTeachingResourcePath = xssapi.filterHTML( slingHttpServletRequest
                .getParameter( ClassMagsMigrationASRPConstants.BOOKMARK_TEACHING_RESOURCE_PATH ) );
        final String viewArticleLinkPath = xssapi.filterHTML(
                slingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.VIEW_ARTICLE_LINK_PATH ) );

        LOG.info( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.RESOURCE_RESOLVER_OBJECT + resolver ) );

        SocialResourceProvider srp = socialUtils.getSocialResourceProvider( resource );
        srp.setConfig( socialUtils.getDefaultStorageConfig() );

        createDefaultPathNode( srp, userInfo, resolver );
        String path = getMagsNodeSocialPath( slingHttpServletRequest, userInfo );
        LOG.info( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.SOCIAL_PATH + path ) );

        Resource ugcResource = SocialASRPUtils.getUGCResourceFromPath( resolver, path, srp );
        if ( null == ugcResource ) {
            Map< String, Object > props = new HashMap< >();
            props.put( CollabUser.PROP_NAME, userInfo );
            props.put( JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY,
                    BookmarkSocialComponent.BOOKMARK_RESOURCE_TYPE );
            props.put( JcrConstants.JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED );
            addBookmarkProperty( bookmarkedPagePath, props, ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_PAGE,
                    viewArticleLinkPath );
            addBookmarkProperty( bookmarkedTeachingResourcePath, props,
                    ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_ASSET, viewArticleLinkPath );
            ugcResource = srp.create( resolver, path, props );
        } else if ( ugcResource.isResourceType( BookmarkSocialComponent.BOOKMARK_RESOURCE_TYPE ) ) {
            updateResourceForAdd( ugcResource, bookmarkedPagePath, bookmarkedTeachingResourcePath,
                    viewArticleLinkPath );
        }
        srp.commit( resolver );
        resolver.commit();
        return ugcResource;
    }

    /**
     * Creates the default path node.
     *
     * @param srp
     *            the srp
     * @param userInfo
     *            the user info
     * @param resolver
     *            the resolver
     * @throws PersistenceException
     *             the persistence exception
     */
    private void createDefaultPathNode( SocialResourceProvider srp, String userInfo, ResourceResolver resolver )
            throws PersistenceException {

        SocialASRPUtils.createSocialNode( srp, resolver, getDefaultNodeSocialPath() );
        SocialASRPUtils.createSocialNode( srp, resolver, getUserNodeSocialPath( userInfo ) );
    }

    /**
     * Gets the mags node social path.
     *
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @param userInfo
     *            the user info
     * @return the mags node social path
     */
    private String getMagsNodeSocialPath( SlingHttpServletRequest slingHttpServletRequest, String userInfo ) {
        return getDefaultNodeSocialPath() + "/" + userInfo + "/"
                + xssapi.filterHTML( slingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.APP_NAME ) );
    }

    /**
     * Gets the user node social path.
     *
     * @param userInfo
     *            the user info
     * @return the user node social path
     */
    private String getUserNodeSocialPath( String userInfo ) {
        return getDefaultNodeSocialPath() + "/" + userInfo;
    }

    /**
     * Gets the default node social path.
     *
     * @return the default node social path
     */
    private String getDefaultNodeSocialPath() {
        return SocialUtils.SRP_CLOUD_PATH + ClassMagsMigrationASRPConstants.DEFAULT_PATH_FOR_USER_DATA;
    }

    /**
     * Adds the bookmark property.
     *
     * @param bookmarkedPath
     *            the bookmarked path
     * @param props
     *            the props
     * @param bookmarkType
     *            the bookmark type
     * @param viewArticleLinkPath
     *            the view article link path
     * @throws UnsupportedEncodingException
     *             the unsupported encoding exception
     * @throws JSONException
     *             the JSON exception
     */
    private void addBookmarkProperty( final String bookmarkedPath, Map< String, Object > props, String bookmarkType,
            String viewArticleLinkPath ) throws UnsupportedEncodingException, JSONException {

        if ( StringUtils.isNotBlank( bookmarkedPath ) ) {
            JSONObject bookmarkJsonObject = new JSONObject();
            bookmarkJsonObject.put( ClassMagsMigrationASRPConstants.JSON_KEY_BOOKMARK_PATH, bookmarkedPath );
            bookmarkJsonObject.put( ClassMagsMigrationASRPConstants.JSON_KEY_DATE_SAVED,
                    Calendar.getInstance().getTime().getTime() );
            bookmarkJsonObject.put( ClassMagsMigrationASRPConstants.JSON_KEY_VIEW_ARTICLE_LINK, viewArticleLinkPath );
            props.put( bookmarkType.concat( SocialASRPUtils.urlEncoder( bookmarkedPath ) ),
                    bookmarkJsonObject.toString() );
        }
    }

    /**
     * Update resource for add.
     *
     * @param ugcResource
     *            the ugc resource
     * @param bookmarkedPagePath
     *            the bookmarked page path
     * @param bookmarkedTeachingResourcePath
     *            the bookmarked teaching resource path
     * @param viewArticleLinkPath
     *            the view article link path
     * @throws UnsupportedEncodingException
     *             the unsupported encoding exception
     * @throws JSONException
     *             the JSON exception
     */
    private void updateResourceForAdd( Resource ugcResource, String bookmarkedPagePath,
            String bookmarkedTeachingResourcePath, String viewArticleLinkPath )
                    throws UnsupportedEncodingException, JSONException {

        ModifiableValueMap propertyMap = ugcResource.adaptTo( ModifiableValueMap.class );
        LOG.info( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.UGC_RESOURCE_PROPERTIES + propertyMap ) );
        if ( StringUtils.isNotBlank( bookmarkedPagePath ) ) {
            addBookmarkProperty( bookmarkedPagePath, propertyMap, ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_PAGE,
                    viewArticleLinkPath );
        }
        if ( StringUtils.isNotBlank( bookmarkedTeachingResourcePath ) ) {
            addBookmarkProperty( bookmarkedTeachingResourcePath, propertyMap,
                    ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_ASSET, viewArticleLinkPath );
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.social.api.BookmarkService#
     * deleteBookmarkResource(org.apache.sling.api.SlingHttpServletRequest)
     */
    @Override
    public Resource deleteBookmarkResource( SlingHttpServletRequest slingHttpServletRequest )
            throws OperationException {

        String userRole = RoleUtil.getUserRole( slingHttpServletRequest );
        String userId = SocialASRPUtils.fetchUserId( slingHttpServletRequest, userRole );

        if ( !SocialASRPUtils.isValidUser( userRole, userId ) ) {
            throw new OperationException(
                    ClassmagsMigrationErrorCodes.INVALID_USER_EXCEPTION_MESSAGE.getErrorDescription(),
                    ClassmagsMigrationErrorCodes.INVALID_USER_EXCEPTION_MESSAGE.getErrorCodeInt() );
        }

        final Resource resource = slingHttpServletRequest.getResource();
        final ResourceResolver resolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE );

        return deleteBookmarkData( resource, slingHttpServletRequest, resolver, userRole.concat( userId ) );
    }

    /**
     * Delete bookmark data.
     *
     * @param resource
     *            the resource
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @param resolver
     *            the resolver
     * @param userInfo
     *            the user info
     * @return the resource
     * @throws OperationException
     *             the operation exception
     */
    private Resource deleteBookmarkData( Resource resource, SlingHttpServletRequest slingHttpServletRequest,
            ResourceResolver resolver, String userInfo ) throws OperationException {

        try {
            if ( !socialUtils.mayAccessUGC( resolver ) ) {
                throw new OperationException(
                        userInfo + ClassmagsMigrationErrorCodes.PERMISSION_DENIED_MESSAGE.getErrorDescription(),
                        ClassmagsMigrationErrorCodes.PERMISSION_DENIED_MESSAGE.getErrorCodeInt() );
            }
            return deleteOrUpdateUGCResource( slingHttpServletRequest, resource, userInfo, resolver );

        } catch ( PersistenceException | UnsupportedEncodingException e ) {
            LOG.error( ClassmagsMigrationErrorCodes.DELETE_BOOKMARK_ERROR_MESSAGE.getErrorDescription(), e );
            throw new OperationException(
                    ClassmagsMigrationErrorCodes.DELETE_BOOKMARK_ERROR_MESSAGE.getErrorDescription(),
                    ClassmagsMigrationErrorCodes.DELETE_BOOKMARK_ERROR_MESSAGE.getErrorCodeInt() );
        }
    }

    /**
     * Delete or update UGC resource.
     *
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @param resource
     *            the resource
     * @param userInfo
     *            the user info
     * @param resolver
     *            the resolver
     * @return the resource
     * @throws UnsupportedEncodingException
     *             the unsupported encoding exception
     * @throws PersistenceException
     *             the persistence exception
     */
    private Resource deleteOrUpdateUGCResource( SlingHttpServletRequest slingHttpServletRequest, Resource resource,
            String userInfo, ResourceResolver resolver ) throws UnsupportedEncodingException, PersistenceException {

        final String deleteBookmarkedPagePath = xssapi.filterHTML(
                slingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.BOOKMARK_PAGE_PATH ) );
        final String deleteBookmarkedTeachingResourcePath = xssapi.filterHTML( slingHttpServletRequest
                .getParameter( ClassMagsMigrationASRPConstants.BOOKMARK_TEACHING_RESOURCE_PATH ) );

        LOG.info( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.RESOURCE_RESOLVER_OBJECT + resolver ) );

        String path = getMagsNodeSocialPath( slingHttpServletRequest, userInfo );
        LOG.info( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.SOCIAL_PATH + path ) );

        SocialResourceProvider srp = socialUtils.getSocialResourceProvider( resource );
        srp.setConfig( socialUtils.getDefaultStorageConfig() );

        Resource ugcResource = SocialASRPUtils.getUGCResourceFromPath( resolver, path, srp );
        Resource parentResource = null != ugcResource ? ugcResource.getParent() : null;

        deleteBookmarkProperty( deleteBookmarkedPagePath, SocialASRPUtils.getUGCResourceFromPath( resolver, path, srp ),
                ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_PAGE, resolver, srp );
        deleteBookmarkProperty( deleteBookmarkedTeachingResourcePath,
                SocialASRPUtils.getUGCResourceFromPath( resolver, path, srp ),
                ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_ASSET, resolver, srp );

        if ( deleteMagsNode( SocialASRPUtils.getUGCResourceFromPath( resolver, path, srp ) ) ) {
            srp.delete( resolver, path );
            srp.commit( resolver );
            resolver.commit();
            ugcResource = deleteUserNode( parentResource, srp, resolver );
        } else {
            ugcResource = SocialASRPUtils.getUGCResourceFromPath( resolver, path, srp );
        }
        srp.commit( resolver );
        resolver.commit();
        return ugcResource;
    }

    /**
     * Delete user node.
     *
     * @param ugcResource
     *            the ugc resource
     * @param srp
     *            the srp
     * @param resolver
     *            the resolver
     * @return the resource
     * @throws PersistenceException
     *             the persistence exception
     */
    private Resource deleteUserNode( Resource ugcResource, SocialResourceProvider srp, ResourceResolver resolver )
            throws PersistenceException {

        Resource currentUGCResource = ugcResource;
        if ( !ugcResource.hasChildren() ) {
            Resource parentResource = ugcResource.getParent();
            srp.delete( resolver, ugcResource.getPath() );
            srp.commit( resolver );
            resolver.commit();
            currentUGCResource = parentResource;
        }
        return currentUGCResource;
    }

    /**
     * Delete user node.
     *
     * @param ugcResource
     *            the ugc resource
     * @return true, if successful
     */
    private boolean deleteMagsNode( Resource ugcResource ) {

        for ( String propertyKey : ugcResource.getValueMap().keySet() ) {
            if ( propertyKey.startsWith( ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_PAGE )
                    || propertyKey.startsWith( ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_ASSET ) ) {
                return false;
            }
        }
        return true;
    }

    /**
     * Delete bookmark property.
     *
     * @param deleteBookmarkedPath
     *            the delete bookmarked path
     * @param ugcResource
     *            the ugc resource
     * @param bookmarkType
     *            the bookmark type
     * @param resolver
     *            the resolver
     * @param srp
     *            the srp
     * @throws UnsupportedEncodingException
     *             the unsupported encoding exception
     * @throws PersistenceException
     *             the persistence exception
     */
    private void deleteBookmarkProperty( String deleteBookmarkedPath, Resource ugcResource, String bookmarkType,
            ResourceResolver resolver, SocialResourceProvider srp )
                    throws UnsupportedEncodingException, PersistenceException {

        if ( null != ugcResource ) {
            ModifiableValueMap propertyMap = ugcResource.adaptTo( ModifiableValueMap.class );
            LOG.info(
                    CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.UGC_RESOURCE_PROPERTIES + propertyMap ) );

            if ( StringUtils.isNotBlank( deleteBookmarkedPath ) && null != propertyMap && ( propertyMap
                    .containsKey( bookmarkType.concat( SocialASRPUtils.urlEncoder( deleteBookmarkedPath ) ) ) ) ) {
                propertyMap.remove( bookmarkType.concat( SocialASRPUtils.urlEncoder( deleteBookmarkedPath ) ) );
                srp.commit( resolver );
                resolver.commit();
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.social.api.BookmarkService#
     * getAllSiteBookmarkResource(org.apache.sling.api.SlingHttpServletRequest)
     */
    @Override
    public Resource getAllSiteBookmarkResource( SlingHttpServletRequest slingHttpServletRequest )
            throws OperationException {

        String userRole = RoleUtil.getUserRole( slingHttpServletRequest );
        String userId = SocialASRPUtils.fetchUserId( slingHttpServletRequest, userRole );

        if ( !SocialASRPUtils.isValidUser( userRole, userId ) ) {
            throw new OperationException(
                    ClassmagsMigrationErrorCodes.INVALID_USER_EXCEPTION_MESSAGE.getErrorDescription(),
                    ClassmagsMigrationErrorCodes.INVALID_USER_EXCEPTION_MESSAGE.getErrorCodeInt() );
        }

        final Resource resource = slingHttpServletRequest.getResource();
        final ResourceResolver resolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE );

        return getAllSiteBookmarkUGCResource( resource, slingHttpServletRequest, resolver, userRole.concat( userId ) );
    }

    /**
     * Gets the all site bookmark UGC resource.
     *
     * @param resource
     *            the resource
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @param resolver
     *            the resolver
     * @param userInfo
     *            the user info
     * @return the all site bookmark UGC resource
     */
    private Resource getAllSiteBookmarkUGCResource( Resource resource, SlingHttpServletRequest slingHttpServletRequest,
            ResourceResolver resolver, String userInfo ) {

        SocialResourceProvider srp = socialUtils.getSocialResourceProvider( resource );
        srp.setConfig( socialUtils.getDefaultStorageConfig() );

        String path = getMagsNodeSocialPath( slingHttpServletRequest, userInfo );
        LOG.info( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.SOCIAL_PATH + path ) );

        return SocialASRPUtils.getUGCResourceFromPath( resolver, path, srp );
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.social.api.BookmarkService#
     * getMyBookmarksResource(org.apache.sling.api.SlingHttpServletRequest)
     */
    @Override
    public Resource getMyBookmarksResource( SlingHttpServletRequest slingHttpServletRequest )
            throws OperationException {

        String userRole = RoleUtil.getUserRole( slingHttpServletRequest );
        String userId = SocialASRPUtils.fetchUserId( slingHttpServletRequest, userRole );

        if ( !SocialASRPUtils.isValidUser( userRole, userId ) ) {
            throw new OperationException(
                    ClassmagsMigrationErrorCodes.INVALID_USER_EXCEPTION_MESSAGE.getErrorDescription(),
                    ClassmagsMigrationErrorCodes.INVALID_USER_EXCEPTION_MESSAGE.getErrorCodeInt() );
        }

        final Resource resource = slingHttpServletRequest.getResource();
        final ResourceResolver resolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE );

        return getMyBookmarksUGCResource( resource, resolver, userRole.concat( userId ) );
    }

    /**
     * Gets the my bookmarks UGC resource.
     *
     * @param resource
     *            the resource
     * @param resolver
     *            the resolver
     * @param userInfo
     *            the user info
     * @return the my bookmarks UGC resource
     */
    private Resource getMyBookmarksUGCResource( Resource resource, ResourceResolver resolver, String userInfo ) {

        SocialResourceProvider srp = socialUtils.getSocialResourceProvider( resource );
        srp.setConfig( socialUtils.getDefaultStorageConfig() );

        String path = getUserNodeSocialPath( userInfo );
        LOG.info( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.SOCIAL_PATH + path ) );

        return SocialASRPUtils.getUGCResourceFromPath( resolver, path, srp );
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.social.api.BookmarkService#
     * deleteUserBookmarkResource(org.apache.sling.api.SlingHttpServletRequest)
     */
    @Override
    public Resource deleteUserBookmarkResource( SlingHttpServletRequest slingHttpServletRequest )
            throws OperationException {

        final Resource resource = slingHttpServletRequest.getResource();
        final ResourceResolver resolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE );

        try {
            return deleteUserUGCResource( slingHttpServletRequest, resource, resolver );

        } catch ( PersistenceException e ) {
            LOG.error( ClassmagsMigrationErrorCodes.DELETE_BOOKMARK_ERROR_MESSAGE.getErrorDescription(), e );
            throw new OperationException(
                    ClassmagsMigrationErrorCodes.DELETE_BOOKMARK_ERROR_MESSAGE.getErrorDescription(),
                    ClassmagsMigrationErrorCodes.DELETE_BOOKMARK_ERROR_MESSAGE.getErrorCodeInt() );
        }
    }

    /**
     * Delete user UGC resource.
     *
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @param resource
     *            the resource
     * @param resolver
     *            the resolver
     * @return the resource
     * @throws PersistenceException
     *             the persistence exception
     */
    private Resource deleteUserUGCResource( SlingHttpServletRequest slingHttpServletRequest, Resource resource,
            ResourceResolver resolver ) throws PersistenceException {

        String path = xssapi.filterHTML(
                slingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.BOOKMARK_PAGE_PATH ) );
        LOG.info( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.SOCIAL_PATH + path ) );

        SocialResourceProvider srp = socialUtils.getSocialResourceProvider( resource );
        srp.setConfig( socialUtils.getDefaultStorageConfig() );

        Resource ugcResource = SocialASRPUtils.getUGCResourceFromPath( resolver, path, srp );
        Resource parentResource = null != ugcResource ? ugcResource.getParent() : null;

        ugcResource = parentResource;
        srp.delete( resolver, path );
        srp.commit( resolver );
        resolver.commit();

        return ugcResource;
    }
}
