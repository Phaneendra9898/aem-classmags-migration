package com.scholastic.classmags.migration.models;

/**
 * The Class ContentTileFlags.
 */
public class ContentTileFlags {

    /** The share enabled. */
    private boolean shareEnabled;

    /** The download enabled. */
    private boolean downloadEnabled;

    /** The view article enabled. */
    private boolean viewArticleEnabled;

    /**
     * Checks if is share enabled.
     *
     * @return the shareEnabled
     */
    public boolean isShareEnabled() {
        return shareEnabled;
    }

    /**
     * Sets the share enabled.
     *
     * @param shareEnabled
     *            the shareEnabled to set
     */
    public void setShareEnabled( boolean shareEnabled ) {
        this.shareEnabled = shareEnabled;
    }

    /**
     * Checks if is download enabled.
     *
     * @return the downloadEnabled
     */
    public boolean isDownloadEnabled() {
        return downloadEnabled;
    }

    /**
     * Sets the download enabled.
     *
     * @param downloadEnabled
     *            the downloadEnabled to set
     */
    public void setDownloadEnabled( boolean downloadEnabled ) {
        this.downloadEnabled = downloadEnabled;
    }

    /**
     * Checks if is view article enabled.
     *
     * @return the viewArticleEnabled
     */
    public boolean isViewArticleEnabled() {
        return viewArticleEnabled;
    }

    /**
     * Sets the view article enabled.
     *
     * @param viewArticleEnabled
     *            the viewArticleEnabled to set
     */
    public void setViewArticleEnabled( boolean viewArticleEnabled ) {
        this.viewArticleEnabled = viewArticleEnabled;
    }

    /**
     * Gets the string value of share enabled.
     *
     * @return the string value of share enabled
     */
    public String getStringValueOfShareEnabled() {
        return String.valueOf( shareEnabled );
    }

    /**
     * Gets the string value of download enabled.
     *
     * @return the string value of download enabled
     */
    public String getStringValueOfDownloadEnabled() {
        return String.valueOf( downloadEnabled );
    }

    /**
     * Gets the string value of view article enabled.
     *
     * @return the string value of view article enabled
     */
    public String getStringValueOfViewArticleEnabled() {
        return String.valueOf( viewArticleEnabled );
    }
}
