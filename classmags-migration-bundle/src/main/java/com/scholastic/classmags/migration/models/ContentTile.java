package com.scholastic.classmags.migration.models;

import java.util.Calendar;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * The Class ContentTile.
 */
@Model( adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL )
public class ContentTile extends ClassmagsMigrationBaseModel {

    /** The content tile content type. */
    @Inject
    @Via( "resource" )
    @Default( values = { "article" } )
    private String contentTileContentType;

    /** The content tile image pos. */
    @Inject
    @Via( "resource" )
    @Default( values = { "horizontal" } )
    private String contentTileImagePos;

    /** The custom content title. */
    @Inject
    @Via( "resource" )
    private String customContentTitle;

    /** The custom content date. */
    @Inject
    @Via( "resource" )
    private Calendar customContentDate;

    /** The custom content desc text. */
    @Inject
    @Via( "resource" )
    private String customContentDescText;

    /** The file reference. */
    @Inject
    @Via( "resource" )
    private String fileReference;

    /** The custom content link. */
    @Inject
    @Via( "resource" )
    private String customContentLink;

    /** The content tile link. */
    @Inject
    @Via( "resource" )
    private String contentTileLink;

    /** The content tile article page path. */
    @Inject
    @Via( "resource" )
    private String contentTileArticlePagePath;

    /**
     * Instantiates a new content tile.
     * 
     * @param request
     *            the request
     */
    public ContentTile( SlingHttpServletRequest request ) {
        super( request );
    }

    /**
     * Gets the content tile content type.
     * 
     * @return the contentTileContentType
     */
    public String getContentTileContentType() {
        return contentTileContentType;
    }

    /**
     * Gets the content tile image pos.
     * 
     * @return the contentTileImagePos
     */
    public String getContentTileImagePos() {
        return contentTileImagePos;
    }

    /**
     * Gets the custom content title.
     * 
     * @return the customContentTitle
     */
    public String getCustomContentTitle() {
        return customContentTitle;
    }

    /**
     * Gets the custom content date.
     * 
     * @return the customContentDate
     */
    public Calendar getCustomContentDate() {
        return customContentDate;
    }

    /**
     * Gets the custom content desc text.
     * 
     * @return the customContentDescText
     */
    public String getCustomContentDescText() {
        return customContentDescText;
    }

    /**
     * Gets the file reference.
     * 
     * @return the fileReference
     */
    public String getFileReference() {
        return fileReference;
    }

    /**
     * Gets the custom content link.
     * 
     * @return the customContentLink
     */
    public String getCustomContentLink() {
        return customContentLink;
    }

    /**
     * Gets the content tile link.
     * 
     * @return the contentTileLink
     */
    public String getContentTileLink() {
        return contentTileLink;
    }

    /**
     * Gets the content tile article page path.
     *
     * @return the contentTileArticlePagePath
     */
    public String getContentTileArticlePagePath() {
        return contentTileArticlePagePath;
    }

    /**
     * Sets the content tile article page path.
     *
     * @param contentTileArticlePagePath
     *            the contentTileArticlePagePath to set
     */
    public void setContentTileArticlePagePath( String contentTileArticlePagePath ) {
        this.contentTileArticlePagePath = InternalURLFormatter.formatURL( getRequest().getResourceResolver(),
                contentTileArticlePagePath );
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ContentTile [contentTileContentType=" + contentTileContentType + ", contentTileImagePos="
                + contentTileImagePos + ", customContentTitle=" + customContentTitle + ", customContentDate="
                + customContentDate + ", customContentDescText=" + customContentDescText + ", fileReference="
                + fileReference + ", customContentLink=" + customContentLink + ", contentTileLink=" + contentTileLink
                + "]";
    }

}
