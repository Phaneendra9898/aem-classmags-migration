package com.scholastic.classmags.migration.services;

import java.io.IOException;
import java.util.List;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.beans.DocumentObjectBinder;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.models.PageContent;

/**
 * AbstractSolrIndexService provides basic support for indexing Solr documents.
 * This implementation creates a shared instance of SolrServer. Extend this
 * class and override getSolrServer(getCoreName()) if you need to communicate
 * with multiple Solr servers or cores.
 */
public abstract class AbstractSolrIndexService {

    private static final Logger LOG = LoggerFactory.getLogger( AbstractSolrIndexService.class );

    /** Retrieve a particular instance of SolrClient for Indexing. */
    protected abstract SolrClient getSolrIndexClient();

    /** Retrieve a particular instance of SolrClient for Querying. */
    protected abstract SolrClient getSolrQueryClient();

    public abstract String getCoreName();

    /**
     * Adds a document to the index, then commits it.
     *
     * @param doc
     */
    public void addDocAndCommit( SolrInputDocument doc ) {
        addDoc( doc );
        commit();
    }
    
    /**
     * Adds a List of documents to the index, then commits it.
     *
     * @param doc
     */
    public UpdateResponse addBeansAndCommit( List<PageContent> docs ) {
        addBeans( docs );
        return commit();
    }

    /**
     * Deletes a document by ID and performs commit.
     *
     * @param docId
     *            Solr document ID
     */
    public void deleteAndCommit( String docId ) {
        delete( docId );
        commit();
    }

    /**
     * Deletes the solr documents based on the query results and then performs the commit.
     *
     * @param query
     *            Solr query
     */
    public void deleteByQueryAndCommit( String query ) {
        deleteByQuery( query );
        commit();
    }

    /**
     * Adds a document to the index. This method does not perform a commit. You
     * may call commit() yourself after add(), or you may use the addAndCommit()
     * method.
     *
     * @param doc
     */
    public void addBean( Object doc ) {

        try {
            getSolrIndexClient().addBean( doc );
        } catch ( SolrServerException e ) {
            LOG.error( "Error indexing: {}", doc, e );
        } catch ( IOException e ) {
            LOG.error( "Error indexing: {}", doc, e );
        }
    }
    
    /**
     * Adds a document to the index. This method does not perform a commit. You
     * may call commit() yourself after add(), or you may use the addAndCommit()
     * method.
     *
     * @param doc
     */
    public void addDoc( SolrInputDocument doc ) {

        try {
            getSolrIndexClient().add( doc );
        } catch ( SolrServerException e ) {
            LOG.error( "Error indexing: {}", doc, e );
        } catch ( IOException e ) {
            LOG.error( "Error indexing: {}", doc, e );
        }
    }
    
    /**
     * Adds a List of documents to the index. This method does not perform a commit. You
     * may call commit() yourself after add(), or you may use the addAndCommit()
     * method.
     *
     * @param doc
     */
    public void addBeans( List<PageContent> docs ) {
        DocumentObjectBinder binder = new DocumentObjectBinder();
        for(Object d: docs ){
            addDoc(binder.toSolrInputDocument( d ));
        }
        //getSolrIndexClient().addBeans( docs );
    }

    /**
     * Deletes a document by ID.
     *
     * @param docId
     *            Solr document ID
     */
    public void delete( String docId ) {

        try {
            getSolrIndexClient().deleteById( docId );

        } catch ( SolrServerException e ) {
            LOG.error( "Error deleting document: {}", docId, e );
        } catch ( IOException e ) {
            LOG.error( "Error deleting document: {}", docId, e );
        }
    }

    /**
     * Deletes the solr documents based on the query results.
     *
     * @param query
     *            Solr query
     */
    public void deleteByQuery( String query ) {

        try {

            LOG.info( "Query for Deleting documents is : '{}'", query );

            getSolrIndexClient().deleteByQuery( query );

        } catch ( SolrServerException e ) {
            LOG.error( "Error deleting document: {}", query, e );
        } catch ( IOException e ) {
            LOG.error( "Error deleting document: {}", query, e );
        }
    }

    /**
     * Commits any pending index changes.
     */
    public UpdateResponse commit() {
        UpdateResponse response=null;
        try {
            response=getSolrIndexClient().commit();
        } catch ( SolrServerException e ) {
            LOG.error( "Error committing change to index", e );
        } catch ( IOException e ) {
            LOG.error( "Error committing change to index", e );
        }
        return response;
    }

}
