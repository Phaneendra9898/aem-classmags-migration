package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * The Class TopicsUse.
 */
public class TopicsUse extends WCMUsePojo {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(TopicsUse.class);

	private List<String> topics = new ArrayList<String>();

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.adobe.cq.sightly.WCMUsePojo#activate()
	 */
	@Override
	public void activate() throws Exception {

		String[] topicsProp = getProperties().get(ClassMagsMigrationConstants.NN_TOPIC, String[].class);

		if (ArrayUtils.isNotEmpty(topicsProp)) {
			for (String s : topicsProp) {
				JSONObject object = new JSONObject(s);
				String topicName = object.optString(ClassMagsMigrationConstants.JSON_KEY_TOPIC_TITLE);
				if (StringUtils.isNotBlank(topicName)) {
					topics.add(topicName);
				}
			}
		}

	}

	public List<String> getTopics() {
		return topics;
	}

	public void setTopics(List<String> topics) {
		this.topics = topics;
	}

}
