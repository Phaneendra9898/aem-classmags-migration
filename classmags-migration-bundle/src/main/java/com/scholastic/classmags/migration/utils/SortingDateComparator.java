package com.scholastic.classmags.migration.utils;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.models.Issue;

/**
 * The Class SortingDateComparator.
 */
public class SortingDateComparator implements Comparator< Issue >, Serializable {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( SortingDateComparator.class );

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3126844466497789788L;

    /*
     * (non-Javadoc)
     * 
     * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
     */
    @Override
    public int compare( Issue element1, Issue element2 ) {

        String sortingDate1 = element1.getIssuedate();
        String sortingDate2 = element2.getIssuedate();

        if ( StringUtils.isNotBlank( sortingDate1 ) && StringUtils.isNotBlank( sortingDate2 ) ) {
            String dateFormat = element1.getDateFormat();
            DateFormat format = new SimpleDateFormat( dateFormat );
            Date date1;
            Date date2;
            try {
                date1 = format.parse( sortingDate1 );
                date2 = format.parse( sortingDate2 );

                if ( date1.before( date2 ) ) {
                    return -1;
                } else if ( date1.after( date2 ) ) {
                    return 1;
                } else {
                    return 0;
                }
            } catch ( ParseException e ) {
                LOG.error( "::::ParseException in SortingDateComparator::::" + e );
            }
        }
        return -1;
    }
}
