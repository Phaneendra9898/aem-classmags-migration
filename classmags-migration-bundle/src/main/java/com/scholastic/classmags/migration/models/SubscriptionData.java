package com.scholastic.classmags.migration.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubscriptionData {

	@SerializedName("billing")
	@Expose
	private Billing billing;
	@SerializedName("shipping")
	@Expose
	private Shipping shipping;
	@SerializedName("billingOption")
	@Expose
	private String billingOption;
	@SerializedName("cartData")
	@Expose
	private List<CartData> cartData = null;
	@SerializedName("subscriptionType")
	@Expose
	private SubscriptionType subscriptionType;
	@SerializedName("subTotalPrice")
	@Expose
	private Integer subTotalPrice;
	@SerializedName("totalPrice")
	@Expose
	private Integer totalPrice;
	@SerializedName("totalShippingPrice")
	@Expose
	private Integer totalShippingPrice;

	public Billing getBilling() {
		return billing;
	}

	public void setBilling(Billing billing) {
		this.billing = billing;
	}

	public Shipping getShipping() {
		return shipping;
	}

	public void setShipping(Shipping shipping) {
		this.shipping = shipping;
	}

	public String getBillingOption() {
		return billingOption;
	}

	public void setBillingOption(String billingOption) {
		this.billingOption = billingOption;
	}

	public List<CartData> getCartData() {
		return cartData;
	}

	public void setCartData(List<CartData> cartData) {
		this.cartData = cartData;
	}

	public SubscriptionType getSubscriptionType() {
		return subscriptionType;
	}

	public void setSubscriptionType(SubscriptionType subscriptionType) {
		this.subscriptionType = subscriptionType;
	}

	public Integer getSubTotalPrice() {
		return subTotalPrice;
	}

	public void setSubTotalPrice(Integer subTotalPrice) {
		this.subTotalPrice = subTotalPrice;
	}

	public Integer getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Integer totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Integer getTotalShippingPrice() {
		return totalShippingPrice;
	}

	public void setTotalShippingPrice(Integer totalShippingPrice) {
		this.totalShippingPrice = totalShippingPrice;
	}

}
