package com.scholastic.classmags.migration.models;

import org.apache.commons.lang3.StringUtils;

import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * The Enum ResourcesType.
 */
public enum ResourcesType {

    /** The skills sheet. */
    SKILLS_SHEET( ClassMagsMigrationConstants.SKILLS_SHEET, ClassMagsMigrationConstants.TYPE_SKILL_SHEET ),

    /** The video. */
    VIDEO( ClassMagsMigrationConstants.VIDEO, ClassMagsMigrationConstants.TYPE_VIDEO ),

    /** The game. */
    GAME( ClassMagsMigrationConstants.GAME, ClassMagsMigrationConstants.TYPE_GAME ),

    /** The slideshow. */
    SLIDESHOW( ClassMagsMigrationConstants.SLIDESHOW, ClassMagsMigrationConstants.SLIDESHOW ),

    /** The lesson plan. */
    LESSON_PLAN( ClassMagsMigrationConstants.LESSON_PLAN, ClassMagsMigrationConstants.TYPE_LESSON_PLAN ),

    /** The download. */
    DOWNLOAD( ClassMagsMigrationConstants.DOWNLOAD, ClassMagsMigrationConstants.TYPE_DOWNLOAD ),

    /** The poll. */
    POLL( ClassMagsMigrationConstants.POLL, ClassMagsMigrationConstants.TYPE_POLL ),

    /** The blog. */
    BLOG( ClassMagsMigrationConstants.BLOG, ClassMagsMigrationConstants.TYPE_BLOG ),
    
    /** The blog. */
    ARTICLE( ClassMagsMigrationConstants.ARTICLE, ClassMagsMigrationConstants.TYPE_ARTICLE );

    /** The Resource type. */
    private final String resourceType;

    /** The content type. */
    private final String contentType;

    /**
     * Instantiates a new resources type.
     *
     * @param resourceType the resource type
     * @param contentType the content type
     */
    private ResourcesType( final String resourceType, final String contentType ) {
        this.resourceType = resourceType;
        this.contentType = contentType;
    }

    /**
     * Gets the resource type.
     *
     * @return the resourceType
     */
    public String getResourceType() {
        return resourceType;
    }

    /**
     * Gets the content type.
     * 
     * @return the contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Gets the resource content type.
     * 
     * @param resourceType
     *            the resource type
     * @return the resource content type
     */
    public static String getResourceContentType( String resourceType ) {
        for ( ResourcesType resource : ResourcesType.values() ) {
            if ( StringUtils.equalsIgnoreCase( resource.getResourceType(), resourceType ) ) {
                return resource.getContentType();
            }
        }
        return StringUtils.EMPTY;
    }
}
