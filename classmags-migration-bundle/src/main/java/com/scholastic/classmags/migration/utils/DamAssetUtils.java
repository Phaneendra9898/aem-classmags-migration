/**
 * 
 */
package com.scholastic.classmags.migration.utils;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;

/**
 * @author gowthami
 *
 */
public class DamAssetUtils {

    private static final Logger LOG = LoggerFactory.getLogger(DamAssetUtils.class);

	
	public static JSONArray getAssetsResponse(SlingHttpServletRequest request, QueryBuilder queryBuilder,
			PropertyConfigService propertyConfigService, ResourcePathConfigService resourcePathConfigService) {
		LOG.info("Entering @DamAssetUtils.getAssetsResponse ");

        JSONArray metaDataJSONArray = new JSONArray();
        Session currentSession = null;
        try {
            currentSession = request.getResourceResolver().adaptTo(Session.class);
            
            if(currentSession != null) {
            Map<String, String> queryBuilderMap = buildQueryMap(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM),
    				request.getParameter(DamAssetConstants.PATH_PARAM));
            Query query = queryBuilder.createQuery(PredicateGroup.create(queryBuilderMap),currentSession);
            SearchResult searchResult = query.getResult();
            //Process the result for iterating the node list to get asset metadata
                for ( Hit hit : searchResult.getHits() ) {
                	Node hitNode = hit.getNode();
                    JSONObject metaDataJSONObject = new JSONObject();

                    //Asset Name
                    metaDataJSONObject.put(DamAssetConstants.NAME, hitNode.getName());

                    //Asset Path
                    metaDataJSONObject.put(DamAssetConstants.PATH, getPath(request, hitNode.getPath()));
                    
                    Node assetNode = hitNode.getNode(DamAssetConstants.JCR_CONTENT).getNode(DamAssetConstants.METADATA);
                    
                    //Asset Title
                    if(assetNode.hasProperty(DamAssetConstants.DC_TITLE)) {
                    	metaDataJSONObject.put(DamAssetConstants.TITLE, assetNode.getProperty(DamAssetConstants.DC_TITLE).getValue().getString());
                    }
                    
                    //Asset Description
                    if(assetNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)) {
                    	metaDataJSONObject.put(DamAssetConstants.DESCRIPTION, assetNode.getProperty(DamAssetConstants.DC_DESCRIPTION).getValue().getString());
                    }
                    
                    //Asset Format/Type
                    if(assetNode.hasProperty(DamAssetConstants.DC_FORMAT)) {
                    	metaDataJSONObject.put(DamAssetConstants.FILE_FORMAT, assetNode.getProperty(DamAssetConstants.DC_FORMAT).getValue().getString());
                    }
                    
                  //Asset Tags
                    if(assetNode.hasProperty(DamAssetConstants.CQ_TAGS)) {
                    	Property tagProperty = assetNode.getProperty(DamAssetConstants.CQ_TAGS);
                    	if(tagProperty.isMultiple()) {
                    		Value[] tagValues =  tagProperty.getValues();
                    		JSONArray tagPropertyJSONArray = new JSONArray();

                    		for(Value tagValue : tagValues) {
                    			tagPropertyJSONArray.put(tagValue.toString());
                    		}
                    		if(tagPropertyJSONArray.length() > 0)
                    			metaDataJSONObject.put(DamAssetConstants.TAGS, tagPropertyJSONArray);
                    	} else {
                    		if(!tagProperty.getString().equalsIgnoreCase(DamAssetConstants.EMPTY_STRING)) {
                    			metaDataJSONObject.put(DamAssetConstants.TAG, tagProperty.getString());
                    		}
                    	}
                    	
                    }
                    
                    if(metaDataJSONObject.getString(DamAssetConstants.FILE_FORMAT).contains(DamAssetConstants.MIME_TYPE_VIDEO)) {
                    	//Video Asset Brightcove Video
                        if(assetNode.hasProperty(DamAssetConstants.BRIGHTCOVE_VIDEO)) {
                        	metaDataJSONObject.put(DamAssetConstants.BRIGHTCOVE_VIDEO, assetNode.getProperty(DamAssetConstants.BRIGHTCOVE_VIDEO).getValue().getString());
                        }
                      //Video Asset player Id
                        JSONObject playerIDJsonObject = getPlayerID(propertyConfigService, resourcePathConfigService, request.getParameter(DamAssetConstants.PATH_PARAM));
                        if(playerIDJsonObject.length() > 0)
                        	metaDataJSONObject.put(DamAssetConstants.PLAYER_ID, playerIDJsonObject);
                      //Video Asset Video Id
                        if(assetNode.hasProperty(DamAssetConstants.VIDEO_ID)) {
                        	metaDataJSONObject.put(DamAssetConstants.VIDEO_ID, assetNode.getProperty(DamAssetConstants.VIDEO_ID).getValue().getString());
                        }
                        //Video Asset Account Id
                        metaDataJSONObject.put(DamAssetConstants.ACCOUNT_ID, DamAssetConstants.ACCOUNT_ID_VALUE);
                    }
                    
                    if(metaDataJSONObject.getString(DamAssetConstants.FILE_FORMAT).contains(DamAssetConstants.MIME_TYPE_VIDEO) || 
                    		metaDataJSONObject.getString(DamAssetConstants.FILE_FORMAT).contains(DamAssetConstants.MIME_TYPE_IMAGE))
                    metaDataJSONObject.put(DamAssetConstants.THUMBNAIL_IMAGE_PATH, getPath(request, hitNode.getPath()+ClassMagsMigrationConstants.IMAGE_TILE_RENDITION));
                    
                    //Create an array response
                    if(!(metaDataJSONObject.get(DamAssetConstants.PATH).toString().contains(DamAssetConstants.PDF_SUBASSETS_FILTER)))
                    					metaDataJSONArray.put(metaDataJSONObject);
                }
            }
        } catch (RepositoryException | JSONException e) {
            LOG.error("@DamAssetUtils.getAssetsResponse: Exception while fetching the data : ", e);
		} finally {
            if(currentSession != null) {
                currentSession.logout();
            }
        }
        LOG.info("Exiting @DamAssetUtils.getAssetsResponse ");
		return metaDataJSONArray;
	}

	private static JSONObject getPlayerID(PropertyConfigService propertyConfigService,
			ResourcePathConfigService resourcePathConfigService, String pathParam) {
		LOG.info("Entering @DamAssetUtils.getPlayerID ");
		JSONObject playerIDJsonObject = new JSONObject();
		try {
			String playerID = getPlayerIDFromDataPage(propertyConfigService, resourcePathConfigService, pathParam, DamAssetConstants.JCR_BRIGHTCOVE_EMBEDDED_PLAYER_ID);
			if(playerID != null && !playerID.equalsIgnoreCase(DamAssetConstants.EMPTY_STRING))
				playerIDJsonObject.put(DamAssetConstants.BRIGHTCOVE_EMBEDDED_PLAYER_ID, playerID);
			playerID = getPlayerIDFromDataPage(propertyConfigService, resourcePathConfigService, pathParam, DamAssetConstants.JCR_BRIGHTCOVE_NON_EMBEDDED_PLAYER_ID);
			if(playerID != null && !playerID.equalsIgnoreCase(DamAssetConstants.EMPTY_STRING))
				playerIDJsonObject.put(DamAssetConstants.BRIGHTCOVE_NON_EMBEDDED_PLAYER_ID, playerID);
		} catch (JSONException e) {
			LOG.error("@DamAssetUtils.getPlayerID: Exception while fetching the data : ", e);
		}
		LOG.info("Exiting @DamAssetUtils.getPlayerID ");
		return playerIDJsonObject;
	}

	private static String getPlayerIDFromDataPage(PropertyConfigService propertyConfigService,
			ResourcePathConfigService resourcePathConfigService, String pathParam, String playerIdProperty) {
		LOG.info("Entering @DamAssetUtils.getPlayerIDFromDataPage ");
		String playerID = DamAssetConstants.EMPTY_STRING;
		try {
			String path = CommonUtils.getDataPath( resourcePathConfigService, getMagazineName(pathParam) );
			playerID = propertyConfigService.getDataConfigProperty(playerIdProperty,path);
		} catch (LoginException e) {
			LOG.error("@DamAssetUtils.getPlayerIDFromDataPage: Exception while fetching the data : ", e);
		}
		LOG.info("Exiting @DamAssetUtils.getPlayerIDFromDataPage ");
		return playerID;
	}

	private static String getPath(SlingHttpServletRequest request, String contentPath) {
		LOG.info("Entering @DamAssetUtils.getPath ");
		ResourceResolver resourceResolver = request.getResourceResolver();
        Externalizer externalizer = resourceResolver.adaptTo(Externalizer.class);
        LOG.info("Exiting @DamAssetUtils.getPath ");
        return externalizer.externalLink(resourceResolver, getMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM)) ,contentPath);
	}

	public static boolean isValidPath(String pathParam) {
		LOG.info("Entering @DamAssetUtils.isValidPath ");
		if(pathParam == null) {
			LOG.info("Exiting @DamAssetUtils.isValidPath ");
			return false;
		}
		String pathParamLiterals[] = pathParam.split("/");
		if(pathParamLiterals.length < 5 || !pathParam.contains(DamAssetConstants.URI)) {
			LOG.info("Exiting @DamAssetUtils.isValidPath ");
			return false;
		}
		LOG.info("Exiting @DamAssetUtils.isValidPath ");
		return true;
	}
	
	private static Map<String, String> buildQueryMap(String mimeType, String pathParam) {
		LOG.info("Entering @DamAssetUtils.buildQueryString ");
		
		Map<String, String> queryMap = new HashMap<String, String>();
		queryMap.put(DamAssetConstants.QUERYMAP_KEY_TYPE, DamAssetConstants.QUERYMAP_VALUE_TYPE);
		queryMap.put(DamAssetConstants.QUERYMAP_KEY_PATH, pathParam);
		queryMap.put(DamAssetConstants.QUERYMAP_KEY_ORDERBYSORT, DamAssetConstants.QUERYMAP_VALUE_ORDERBYSORT );
		queryMap.put(DamAssetConstants.QUERYMAP_KEY_PROPERTYOPERATION, DamAssetConstants.QUERYMAP_VALUE_PROPERTYOPERATION );
		queryMap.put(DamAssetConstants.QUERYMAP_KEY_PROPERTY, DamAssetConstants.QUERYMAP_VALUE_PROPERTY);
		queryMap.put(DamAssetConstants.QUERYMAP_KEY_PLimit, DamAssetConstants.QUERYMAP_VALUE_PLimit);
		
		if(mimeType.equalsIgnoreCase(DamAssetConstants.MIME_TYPE_AUDIO)) {
			queryMap.put(DamAssetConstants.QUERYMAP_PROPERTY_VALUE, DamAssetConstants.QUERYMAP_AUDIO_QUERY_STRING );
		} else if(mimeType.equalsIgnoreCase(DamAssetConstants.MIME_TYPE_VIDEO)) {
			queryMap.put(DamAssetConstants.QUERYMAP_PROPERTY_VALUE, DamAssetConstants.QUERYMAP_VIDEO_QUERY_STRING );
		} else if(mimeType.equalsIgnoreCase(DamAssetConstants.MIME_TYPE_APPLICATION)) {
			queryMap.put(DamAssetConstants.QUERYMAP_PROPERTY_VALUE, DamAssetConstants.QUERYMAP_APPLICATION_QUERY_STRING );
		} else if(mimeType.equalsIgnoreCase(DamAssetConstants.MIME_TYPE_IMAGE)) {
			queryMap.put(DamAssetConstants.QUERYMAP_PROPERTY_VALUE, DamAssetConstants.QUERYMAP_IMAGE_QUERY_STRING );
		}
		LOG.info("Exiting @DamAssetUtils.buildQueryString ");
		return queryMap;
	}

	private static String getMagazineName(String pathParam) {
		LOG.info("Entering @DamAssetUtils.getMagazineName ");
		return pathParam.split("/")[4];
	}
}
