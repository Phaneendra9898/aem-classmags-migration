package com.scholastic.classmags.migration.controllers;

import java.util.HashMap;
import java.util.Map;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.scripting.SlingScriptHelper;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.models.VocabData;
import com.scholastic.classmags.migration.services.VocabDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * Use Class for Vocab Data.
 */
public class VocabularyUse extends WCMUsePojo {

    /** The vocab data map. */
    private Map< String, VocabData > vocabDataMap = new HashMap< >();

    /**
     * Gets the vocab data map.
     *
     * @return the vocabDataMap
     */
    public Map< String, VocabData > getVocabDataMap() {
        return vocabDataMap;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() throws Exception {

        SlingHttpServletRequest request = getRequest();
        Resource currentResource = request.getResource();
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        if ( null != slingScriptHelper ) {
            VocabDataService vocabDataService = slingScriptHelper.getService( VocabDataService.class );
            if ( null != vocabDataService ) {
                vocabDataMap = vocabDataService
                        .fetchVocabData( ( currentResource.getPath() ).concat( ClassMagsMigrationConstants.VOCAB_NODE_PATH ) );
            }
        }
    }
}
