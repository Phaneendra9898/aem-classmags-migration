package com.scholastic.classmags.migration.controllers;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.services.UpdateVotingNodeService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class VotingUse.
 */
public class VotingUse extends WCMUsePojo {

    /** The answer choices. */
    private List< ValueMap > answerOptions;

    /** The disable voting date. */
    private String disableVotingDate;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( VotingUse.class );

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() throws ClassmagsMigrationBaseException {

        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        disableVotingDate = StringUtils.EMPTY;
        if ( null != slingScriptHelper ) {
            // Updating voting node with new question.
            updateVotingNode( slingScriptHelper );
        }
        Resource choicesResource = getResource().getChild( ClassMagsMigrationASRPConstants.ANSWER_OPTIONS );
        setAnswerOptions( CommonUtils.fetchMultiFieldData( choicesResource ) );

        ValueMap properties = getResource().getValueMap();
        Date disableVoting = properties.get( ClassMagsMigrationASRPConstants.JSON_KEY_DISABLE_VOTING_DATE,
                Date.class ) != null
                        ? properties.get( ClassMagsMigrationASRPConstants.JSON_KEY_DISABLE_VOTING_DATE, Date.class )
                        : null;
        setDisableVotingDate( null != disableVoting ? String.valueOf( disableVoting.getTime() ) : StringUtils.EMPTY );
    }

    /**
     * Updates voting node.
     *
     * @param slingScriptHelper
     *            the sling script helper
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private void updateVotingNode( SlingScriptHelper slingScriptHelper ) throws ClassmagsMigrationBaseException {
        try {
            LOG.debug( "UpdateVotingNodeService called." );
            UpdateVotingNodeService updateVotingNodeService = slingScriptHelper
                    .getService( UpdateVotingNodeService.class );
            SlingSettingsService slingSettingsService = slingScriptHelper.getService( SlingSettingsService.class );
            boolean isAuthor = ( null != slingSettingsService )
                    ? CommonUtils.isAuthorMode( slingSettingsService.getRunModes() ) : false;
            if ( isAuthor && null != updateVotingNodeService ) {
                updateVotingNodeService.updateVotingNode( getResource() );
            }
        } catch ( PersistenceException e ) {
            LOG.error( "Error while updating voting node", e );
            throw new ClassmagsMigrationBaseException();
        }

    }

    /**
     * Gets the disable voting date.
     *
     * @return the disable voting date
     */
    public String getDisableVotingDate() {
        return disableVotingDate;
    }

    /**
     * Sets the disable voting date.
     *
     * @param disableVotingDate
     *            the disableVotingDate to set
     */
    public void setDisableVotingDate( String disableVotingDate ) {
        this.disableVotingDate = disableVotingDate;
    }

    /**
     * @return the answerOptions
     */
    public List< ValueMap > getAnswerOptions() {
        return answerOptions;
    }

    /**
     * @param answerOptions
     *            the answerOptions to set
     */
    public void setAnswerOptions( List< ValueMap > answerOptions ) {
        this.answerOptions = answerOptions;
    }
}
