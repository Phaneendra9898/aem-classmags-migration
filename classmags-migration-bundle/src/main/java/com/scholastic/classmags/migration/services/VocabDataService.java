package com.scholastic.classmags.migration.services;

import java.util.Map;

import javax.jcr.RepositoryException;

import com.scholastic.classmags.migration.models.VocabData;

/**
 * The Interface VocabDataService.
 */
public interface VocabDataService {

    /**
     * Fetch vocab data.
     *
     * @param currentPath
     *            the current path
     * @return the map
     * @throws RepositoryException
     *             the repository exception
     */
    public Map< String, VocabData > fetchVocabData( String currentPath ) throws RepositoryException;
}
