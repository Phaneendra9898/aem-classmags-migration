package com.scholastic.classmags.migration.models;
/**
 * The Class LinkObject.
 */
public class LinkObject {

    /** The link label. */
    private String label;
    
    /** The link path. */
    private String path;
    
    /** The user type. */
    private String userType;

    /**
     * Gets the label.
     *
     * @return the label
     */
    public String getLabel() {
        return label;
    }
    
    /**
     * Sets the label.
     * 
     * @param label The label.
     */
    public final void setLabel( final String label ) {
        this.label = label;
    }

    /**
     * Gets the path.
     *
     * @return the path
     */
    public String getPath() {
        return path;
    }
    
    /**
     * Sets the path.
     * 
     * @param path The path.
     */
    public final void setPath( final String path ) {
        this.path = path;
    }
    
    /**
     * Gets the user type.
     *
     * @return the user type
     */
    public String getUserType() {
        return userType;
    }
    
    /**
     * Sets the user type.
     * 
     * @param userType The user type.
     */
    public final void setUserType( final String userType ) {
        this.userType = userType;
    }
}
