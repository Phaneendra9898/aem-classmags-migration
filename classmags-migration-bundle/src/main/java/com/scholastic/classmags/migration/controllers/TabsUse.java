package com.scholastic.classmags.migration.controllers;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUsePojo;

/**
 * The Class TabsUse.
 */
public class TabsUse extends WCMUsePojo {
    
    private static int randomAlphaNum = 6;

    /** The tabs map. */
    private Map< String, String > tabs = new HashMap<String, String >();

    /* (non-Javadoc)
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() throws Exception {
        String[] tabTitles = ArrayUtils.EMPTY_STRING_ARRAY;
        if(null!=getResource()){
            ValueMap props = getResource().adaptTo( ValueMap.class );
            tabTitles = props.get( "tabs", new String[] {} );
        }
        constructMapOfTitleAndIds(tabTitles);
    }

    /**
     * Construct a map of tab titles and a unique random strings for tab Ids.
     */
    private void constructMapOfTitleAndIds(String[] tabTitles) {
        if ( ArrayUtils.isNotEmpty( tabTitles ) ) {
            for ( String t : tabTitles ) {
                tabs.put( t, RandomStringUtils.randomAlphanumeric( randomAlphaNum ) );
            }
        }
    }

    /**
     * Getter for the tabs map
     * @return
     */
    public Map< String, String > getTabs() {
        return tabs;
    }

}
