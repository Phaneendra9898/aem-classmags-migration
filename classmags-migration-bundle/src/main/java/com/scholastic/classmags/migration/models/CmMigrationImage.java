package com.scholastic.classmags.migration.models;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.scholastic.classmags.migration.utils.InternalURLFormatter;

@Model( adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL )
public class CmMigrationImage extends ClassmagsMigrationBaseModel {

    @Inject
    @Via( "resource" )
    private String hyperlink;

    public CmMigrationImage( SlingHttpServletRequest request ) {
        super( request );
    }

    /**
     * @return the hyperlink
     */
    public String getHyperlink() {
        return InternalURLFormatter.formatURL( getRequest().getResourceResolver(), hyperlink );
    }

}
