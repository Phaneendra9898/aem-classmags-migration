package com.scholastic.classmags.migration.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.ClassmagsMigrationErrorCodes;
import com.scholastic.classmags.migration.services.FAQQuestionsDataService;
import com.scholastic.classmags.migration.services.GetMetadataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class FAQQuestionsDataServiceImpl.
 */
@Component( immediate = true, metatype = false )
@Service( FAQQuestionsDataService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "FAQ Questions Data Service" ) })
public class FAQQuestionsDataServiceImpl implements FAQQuestionsDataService {

    private static final String FAQ_QUESTION_SET_RESOURCE_TYPE = "scholastic/classroom-magazines-migration/components/content/question-set-faq";

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /** The query builder. */
    @Reference
    QueryBuilder queryBuilder;

    /** The get metadata service. */
    @Reference
    private GetMetadataService getMetadataService;

    Map< String, String > questionsMap;

    @Override
    public Map< String, String > fetchFAQQuestionsData( SlingHttpServletRequest slingRequest, Page currentPage )
            throws ClassmagsMigrationBaseException {
        questionsMap = new HashMap< >();

        String currentPagePath = currentPage.getPath();

        Map< String, String > queryMap = new HashMap< >();
        ResourceResolver resourceResolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );
        Session session = resourceResolver.adaptTo( Session.class );

        queryMap.put( ClassMagsMigrationConstants.QUERY_PATH, currentPagePath );
        queryMap.put( ClassMagsMigrationConstants.RESULTS_LIMIT, ClassMagsMigrationConstants.QUERY_LOWER_BOUND );
        queryMap.put( ClassMagsMigrationConstants.QUERY_PARAM_1, ClassMagsMigrationConstants.QUERY_RESOURCE_TYPE_TAG );
        queryMap.put( ClassMagsMigrationConstants.QUERY_PARAM_1_VALUE, FAQ_QUESTION_SET_RESOURCE_TYPE );
        Query query = queryBuilder.createQuery( PredicateGroup.create( queryMap ), session );

        SearchResult searchResult = query.getResult();
        List< Hit > questionSetNodeList = searchResult.getHits();
        createQuestionsMap( questionSetNodeList );

        return questionsMap;

    }

    /*
     * Creates the questions map.
     * 
     * @param questionSetNodeList The list of all nodes of type question set.
     * @throws ClassMagsMigrationBaseException The classmags base exception.
     */
    private void createQuestionsMap( List< Hit > questionSetNodeList ) throws ClassmagsMigrationBaseException {

        for ( Hit questionSetNode : questionSetNodeList ) {
            Resource questionSet;
            try {
                questionSet = questionSetNode.getResource();
                Resource questionListResource = questionSet.getChild( ClassMagsMigrationConstants.QUESTION_LIST );
                List< ValueMap > questionList = CommonUtils.fetchMultiFieldData( questionListResource );
                for ( ValueMap question : questionList ) {
                    String questionId = question.get( ClassMagsMigrationConstants.QUESTION_ID, StringUtils.EMPTY );
                    String questionsMapValue = question.get( ClassMagsMigrationConstants.QUESTION, StringUtils.EMPTY );
                    String questionsMapKey = questionId.concat( ClassMagsMigrationConstants.COLON )
                            .concat( questionsMapValue );
                    questionsMap.put( questionsMapKey, questionsMapValue );
                }
            } catch ( RepositoryException e ) {
                throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.REPOSITORY_ERROR, e );
            }
        }
    }
}
