package com.scholastic.classmags.migration.models;

import org.apache.sling.commons.json.JSONArray;

public class Slideshow {

    private JSONArray jsonData;
    private String coverImagePath;
    private String numberColor;
    private Boolean disableNumber;
    private String uniqueId;

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId( String uniqueId ) {
        this.uniqueId = uniqueId;
    }

    public String getCoverImagePath() {
        return coverImagePath;
    }

    public void setCoverImagePath( String coverImagePath ) {
        this.coverImagePath = coverImagePath;
    }

    public String getDisableNumber() {
        return disableNumber.toString();
    }

    public void setDisableNumber( Boolean disableNumber ) {
        this.disableNumber = disableNumber;
    }

    public String getNumberColor() {
        return numberColor;
    }

    public void setNumberColor( String numberColor ) {
        this.numberColor = numberColor;
    }

    /**
     * Gets the json data.
     * 
     * @return the jsonData
     */
    public String getJsonData() {
        return jsonData.toString();
    }

    /**
     * Sets the json data.
     * 
     * @param jsonData
     *            the jsonData to set
     */
    public void setJsonData( JSONArray jsonData ) {
        this.jsonData = jsonData;
    }
}
