package com.scholastic.classmags.migration.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.scholastic.classmags.migration.models.VocabData;
import com.scholastic.classmags.migration.services.VocabDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * Vocab Data Service.
 */
@Component( immediate = true, metatype = true )
@Service( VocabDataService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Vocab Data Service" ) })
public class VocabDataServiceImpl implements VocabDataService {

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /** The query builder. */
    @Reference
    private QueryBuilder queryBuilder;

    /**
     * Sets the resource resolver factory.
     *
     * @param resourceResolverFactory
     *            the resourceResolverFactory to set
     */
    public void setResourceResolverFactory( ResourceResolverFactory resourceResolverFactory ) {
        this.resourceResolverFactory = resourceResolverFactory;
    }

    /**
     * Sets the query builder.
     *
     * @param queryBuilder
     *            the queryBuilder to set
     */
    public void setQueryBuilder( QueryBuilder queryBuilder ) {
        this.queryBuilder = queryBuilder;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.services.VocabDataService#
     * fetchVocabData(java.lang.String)
     */
    @Override
    public Map< String, VocabData > fetchVocabData( String vocabDataPath ) throws RepositoryException {

        ResourceResolver resourceResolver;
        Node node;
        Session session;
        VocabData vocabData;
        Map< String, VocabData > vocabDataMap = new HashMap< >();
        Map< String, String > queryMap = new HashMap< >();

        resourceResolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );
        session = resourceResolver.adaptTo( Session.class );

        queryMap.put( ClassMagsMigrationConstants.QUERY_PATH, vocabDataPath );
        queryMap.put( ClassMagsMigrationConstants.RESULTS_LIMIT, ClassMagsMigrationConstants.QUERY_LOWER_BOUND );
        Query query = queryBuilder.createQuery( PredicateGroup.create( queryMap ), session );

        SearchResult searchResult = query.getResult();
        List< Hit > vocabNodes = searchResult.getHits();
        if ( null != vocabNodes && !vocabNodes.isEmpty() ) {

            for ( Hit hit : vocabNodes ) {

                node = hit.getNode();
                vocabData = new VocabData();

                checkBasicVocabData( node, vocabData );
                checkAdditionalVocabData( node, vocabData );

                vocabDataMap.put( vocabData.getVocabWord(), vocabData );
            }
        }
        return vocabDataMap;
    }

    /**
     * Check additional vocab data.
     *
     * @param node
     *            the node
     * @param vocabData
     *            the vocab data
     * @throws RepositoryException
     *             the repository exception
     */
    private void checkAdditionalVocabData( Node node, VocabData vocabData ) throws RepositoryException {
        if ( node.hasProperty( ClassMagsMigrationConstants.VOCAB_WORD_IMAGE_PATH ) ) {
            vocabData.setWordImagePath( InternalURLFormatter.formatURL(
                    CommonUtils.getResourceResolver( resourceResolverFactory,
                            ClassMagsMigrationConstants.READ_SERVICE ),
                    node.getProperty( ClassMagsMigrationConstants.VOCAB_WORD_IMAGE_PATH ).getString() ) );
        }
        if ( node.hasProperty( ClassMagsMigrationConstants.VOCAB_WORD_AUDIO_PATH ) ) {
            vocabData.setWordAudioPath( InternalURLFormatter.formatURL(
                    CommonUtils.getResourceResolver( resourceResolverFactory,
                            ClassMagsMigrationConstants.READ_SERVICE ),
                    node.getProperty( ClassMagsMigrationConstants.VOCAB_WORD_AUDIO_PATH ).getString() ) );
        }
        if ( node.hasProperty( ClassMagsMigrationConstants.VOCAB_WORD_P_TEXT ) ) {
            vocabData.setWordPronunciationText(
                    node.getProperty( ClassMagsMigrationConstants.VOCAB_WORD_P_TEXT ).getString() );
        }
        if ( node.hasProperty( ClassMagsMigrationConstants.VOCAB_WORD_IMAGE_CREDITS ) ) {
            vocabData.setWordImageCredits(
                    node.getProperty( ClassMagsMigrationConstants.VOCAB_WORD_IMAGE_CREDITS ).getString() );
        }
    }

    /**
     * Check basic vocab data.
     *
     * @param node
     *            the node
     * @param vocabData
     *            the vocab data
     * @throws RepositoryException
     *             the repository exception
     */
    private void checkBasicVocabData( Node node, VocabData vocabData ) throws RepositoryException {
        if ( node.hasProperty( ClassMagsMigrationConstants.VOCAB_WORD ) ) {
            vocabData.setVocabWord( node.getProperty( ClassMagsMigrationConstants.VOCAB_WORD ).getString() );
        }
        if ( node.hasProperty( ClassMagsMigrationConstants.VOCAB_WORD_DEFINITION ) ) {
            vocabData.setWordDefinition(
                    node.getProperty( ClassMagsMigrationConstants.VOCAB_WORD_DEFINITION ).getString() );
        }
    }
}
