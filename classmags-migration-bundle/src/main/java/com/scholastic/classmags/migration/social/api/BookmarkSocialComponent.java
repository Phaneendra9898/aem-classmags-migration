package com.scholastic.classmags.migration.social.api;

import java.util.List;
import java.util.Map;

import com.adobe.cq.social.scf.SocialComponent;
import com.scholastic.classmags.migration.models.ResourcesObject;

/**
 * The Interface BookmarkSocialComponent.
 */
public interface BookmarkSocialComponent extends SocialComponent {

    /** The Constant BOOKMARK_RESOURCE_TYPE. */
    public static final String BOOKMARK_RESOURCE_TYPE = "scholastic/classroom-magazines-migration/components/content/bookmark";

    /**
     * Checks if is bookmark enabled.
     *
     * @return true, if is bookmark enabled
     */
    public boolean isBookmarkEnabled();

    /**
     * Gets the asset paths.
     *
     * @return the asset paths
     */
    public List< String > getAssetPaths();

    /**
     * Gets the my bookmarks page data.
     *
     * @return the my bookmarks page data
     */
    public List< ResourcesObject > getMyBookmarksPageData();

    /**
     * Gets the my bookmarks asset data.
     *
     * @return the my bookmarks asset data
     */
    public List< ResourcesObject > getMyBookmarksAssetData();

    /**
     * Gets the magazines name map.
     *
     * @return the magazines name map
     */
    public Map< String, String > getMagazinesNameMap();

    /**
     * Gets the page paths.
     *
     * @return the page paths
     */
    public List< String > getPagePaths();
}
