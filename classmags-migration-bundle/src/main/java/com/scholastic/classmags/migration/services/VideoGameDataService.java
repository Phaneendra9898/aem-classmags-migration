package com.scholastic.classmags.migration.services;

import java.util.List;

import org.apache.sling.api.resource.ValueMap;

import com.scholastic.classmags.migration.models.ContentTileObject;

public interface VideoGameDataService {

	/**
     * Fetch asset data.
     *
     * @param currentPath
     *            the current path
     * @return the list
     */
    public List< ValueMap > fetchAssetData( String currentPath );
    
    /**
     * Gets the video content.
     * 
     * @param videoPath of The content tile.
     *
     * 
     * @return the video content
     */
    ContentTileObject getVideoContent( String videoPath );

    /**
     * Gets the asset content.
     * 
     * 
     * @param assetPath.
     * 
     * @return the asset content
     */
    ContentTileObject getAssetContent( String assetPath );

    /**
     * Gets the game content
     *
     * @param gamePath
     *
     * @return game content object
     */
    ContentTileObject getGameContent(String gamePath);

	
}
