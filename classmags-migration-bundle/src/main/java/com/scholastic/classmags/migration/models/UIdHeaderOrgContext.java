package com.scholastic.classmags.migration.models;

import java.util.List;

/**
 * The Class UIdHeaderOrgContext.
 */
public class UIdHeaderOrgContext {

    /** The obj U id org context sub header organization. */
    private UIdOrgContextSubHeaderOrganization objUIdOrgContextSubHeaderOrganization;

    /** The obj list U id org context sub header entitlements. */
    private List< UIdOrgContextSubHeaderEntitlements > objListUIdOrgContextSubHeaderEntitlements;

    /**
     * Gets the obj U id org context sub header organization.
     *
     * @return the objUIdOrgContextSubHeaderOrganization
     */
    public UIdOrgContextSubHeaderOrganization getObjUIdOrgContextSubHeaderOrganization() {
        return objUIdOrgContextSubHeaderOrganization;
    }

    /**
     * Sets the obj U id org context sub header organization.
     *
     * @param objUIdOrgContextSubHeaderOrganization
     *            the objUIdOrgContextSubHeaderOrganization to set
     */
    public void setObjUIdOrgContextSubHeaderOrganization(
            UIdOrgContextSubHeaderOrganization objUIdOrgContextSubHeaderOrganization ) {
        this.objUIdOrgContextSubHeaderOrganization = objUIdOrgContextSubHeaderOrganization;
    }

    /**
     * Gets the obj list U id org context sub header entitlements.
     *
     * @return the objListUIdOrgContextSubHeaderEntitlements
     */
    public List< UIdOrgContextSubHeaderEntitlements > getObjListUIdOrgContextSubHeaderEntitlements() {
        return objListUIdOrgContextSubHeaderEntitlements;
    }

    /**
     * Sets the obj list U id org context sub header entitlements.
     *
     * @param objListUIdOrgContextSubHeaderEntitlements
     *            the objListUIdOrgContextSubHeaderEntitlements to set
     */
    public void setObjListUIdOrgContextSubHeaderEntitlements(
            List< UIdOrgContextSubHeaderEntitlements > objListUIdOrgContextSubHeaderEntitlements ) {
        this.objListUIdOrgContextSubHeaderEntitlements = objListUIdOrgContextSubHeaderEntitlements;
    }
}
