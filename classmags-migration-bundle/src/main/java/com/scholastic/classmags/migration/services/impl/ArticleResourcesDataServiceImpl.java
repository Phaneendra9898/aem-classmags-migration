package com.scholastic.classmags.migration.services.impl;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.models.ResourcesType;
import com.scholastic.classmags.migration.models.TeachingResourcesType;
import com.scholastic.classmags.migration.services.ArticleResourceDataService;
import com.scholastic.classmags.migration.services.GetMetadataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class ArticleResourcesDataServiceImpl.
 */
@Component( immediate = true, metatype = false )
@Service( ArticleResourceDataService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Article Resource Data Service" ) })
public class ArticleResourcesDataServiceImpl implements ArticleResourceDataService {

    /** The Constant RESOURCES. */
    private static final String RESOURCES = "resources";

    /** The Constant RESOURCE_TYPE. */
    private static final String RESOURCE_TYPE = "resourceType";

    /** The Constant RESOURCE_PATH. */
    private static final String RESOURCE_PATH = "resourcePath";

    /** The Constant NO_TITLE. */
    private static final String NO_TITLE = "NO TITLE AVAILABLE";

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /** The get metadata service. */
    @Reference
    private GetMetadataService getMetadataService;

    /**
     * @param getMetadataService the getMetadataService to set
     */
    public void setGetMetadataService( GetMetadataService getMetadataService ) {
        this.getMetadataService = getMetadataService;
    }

    /**
     * Sets the resource resolver factory.
     *
     * @param resourceResolverFactory
     *            the resourceResolverFactory to set
     */
    public void setResourceResolverFactory( ResourceResolverFactory resourceResolverFactory ) {
        this.resourceResolverFactory = resourceResolverFactory;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.scholastic.classmags.migration.services.ArticleResourceDataService#
     * fetchArticleResourceData(org.apache.sling.api.SlingHttpServletRequest,
     * com.day.cq.wcm.api.Page)
     */
    @Override
    public Map< String, String > fetchArticleResourceData( SlingHttpServletRequest slingRequest, Page currentPage ) {

        String parentIssuePagePath = currentPage.getParent().getPath();

        if ( checkParentIsIssue( parentIssuePagePath ) ) {
            return fetchIssuePageResources( slingRequest, parentIssuePagePath );
        } else {
            return displayCustomMessage();
        }

    }

    /**
     * Fetch static resources.
     *
     * @return the map
     */
    private Map< String, String > displayCustomMessage() {

        Map< String, String > resourcesMap = new HashMap< >();

        resourcesMap.put( ClassMagsMigrationConstants.DATASOURCE_NO_CONFIGURATION,
                ClassMagsMigrationConstants.ARTICLE_RESOURCE_CUSTOM_MESSAGE );
        return resourcesMap;
    }

    /**
     * Fetch issue page resources.
     *
     * @param slingRequest
     *            the sling request
     * @param parentIssuePagePath
     *            the parent issue page path
     * @return the map
     */
    private Map< String, String > fetchIssuePageResources( SlingHttpServletRequest slingRequest,
            String parentIssuePagePath ) {

        Map< String, String > resourcesMap = new HashMap< >();

        String resourceMapKey;
        String resourceMapValue;
        String resourceTitle;
        String resourceType;
        String resourcePath;

        Resource resource = CommonUtils
                .getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE )
                .getResource( parentIssuePagePath.concat( ClassMagsMigrationConstants.JCR_ISSUE_CONFIGURATION ) );

        if ( null != resource ) {
            resource = resource.getChild( RESOURCES );
            if ( null != resource ) {
                final Iterator< Resource > resourceIterator = resource.listChildren();
                while ( resourceIterator.hasNext() ) {
                    ValueMap properties = resourceIterator.next().getValueMap();

                    resourceType = properties.get( RESOURCE_TYPE, StringUtils.EMPTY );
                    resourcePath = properties.get( RESOURCE_PATH, StringUtils.EMPTY );
                    
                    
                    String contentType = TeachingResourcesType.getResourceContentType( resourceType );

                    resourceTitle = getResourceTitle(contentType, slingRequest, resourcePath);
                    
                    resourceMapKey = resourceType.concat( ClassMagsMigrationConstants.HYPHEN ).concat( resourcePath );
                    resourceMapValue = ResourcesType.getResourceContentType( resourceType )
                            .concat( ClassMagsMigrationConstants.HYPHEN )
                            .concat( null != resourceTitle ? resourceTitle : NO_TITLE );
                    resourcesMap.put( resourceMapKey, resourceMapValue );
                }
            }
        }
        return resourcesMap;
    }

    private String getResourceTitle(String contentType, SlingHttpServletRequest slingRequest, String resourcePath) {
        String resourceTitle = org.apache.commons.lang3.StringUtils.EMPTY;
        if ( StringUtils.equalsIgnoreCase( contentType, ClassMagsMigrationConstants.ASSET ) ) {
            resourceTitle = getMetadataService.getDAMProperty( slingRequest, DamConstants.DC_TITLE, resourcePath );
        } else if ( StringUtils.equalsIgnoreCase( contentType, ClassMagsMigrationConstants.PAGE ) ) {
            resourceTitle = getMetadataService.getPageProperty( slingRequest, JcrConstants.JCR_TITLE, resourcePath );
        }
        return resourceTitle;
    }

    /**
     * Check parent is issue.
     *
     * @param parentIssuePagePath
     *            the parent issue page path
     * @return true, if successful
     */
    private boolean checkParentIsIssue( String parentIssuePagePath ) {

        String propertyValue = null;

        Resource resource = CommonUtils
                .getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE )
                .getResource( parentIssuePagePath.concat( ClassMagsMigrationConstants.JCR_CONTENT_NODE ) );

        if ( null != resource ) {
            ValueMap properties = resource.adaptTo( ValueMap.class );
            propertyValue = properties != null
                    ? properties.get( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, StringUtils.EMPTY ) : null;
        }
        return StringUtils.equals( ClassMagsMigrationConstants.ISSUE_PAGE_TEMPLATE_PATH, propertyValue ) ? true : false;
    }
}
