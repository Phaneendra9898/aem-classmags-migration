
package com.scholastic.classmags.migration.social.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.jcr.api.SlingRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.acs.commons.email.EmailService;
import com.adobe.cq.social.scf.OperationException;
import com.adobe.cq.social.srp.SocialResourceProvider;
import com.adobe.cq.social.ugcbase.SocialUtils;
import com.day.cq.commons.jcr.JcrConstants;
import com.google.gson.Gson;
import com.scholastic.classmags.migration.models.Billing;
import com.scholastic.classmags.migration.models.CartData;
import com.scholastic.classmags.migration.models.Shipping;
import com.scholastic.classmags.migration.models.SubscriptionData;
import com.scholastic.classmags.migration.models.SubscriptionType;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.social.api.OrderConfirmationService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

@Component
@Service
public class OrderConfirmationServiceImpl implements OrderConfirmationService {

	private static final String SUBSCRIPTION_DATA = "subscriptionData";

	public static final String PROJECT_RESOURCE_TYPE = "scholastic/classroom-magazines-migration/components/content/CM-Marketing/marketing-OrderSummary";

	private static final String TEMPLATE_PATH = "/etc/notification/email/html/scholastic/com.scholastic.classmags.migration.emailtheeditor/en.txt";
	
	private static final String PATH = "/content/classroom_magazines";
	
	
	@Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY, policy = ReferencePolicy.STATIC)
	private SlingRepository repository;

	@Reference
	private SocialUtils socialUtils;

	@Reference
	private EmailService emailService;
	
	@Reference
	private MagazineProps magazineProps;

	/**
	 * Resource Resolver used for the request.
	 */
	@Reference(cardinality = ReferenceCardinality.MANDATORY_UNARY, policy = ReferencePolicy.STATIC)
	protected ResourceResolverFactory resourceResolverFactory;

	/** Logger for this class. */
	private static final Logger LOG = LoggerFactory.getLogger(OrderConfirmationServiceImpl.class);

	@Override
	public Resource createOrder(SlingHttpServletRequest request) throws OperationException {
		final Resource orderResource = request.getResource();
		final String subscriptionData = request.getParameter(SUBSCRIPTION_DATA);
		return createOrder(orderResource, subscriptionData, request.getResourceResolver());
	}

	@SuppressWarnings("deprecation")
	public Resource createOrder(Resource orderResource, String subscriptionData, ResourceResolver resolver)
			throws OperationException {
		// perform basic validation and thrown an OperationException if request is invalid. Use response code of 400
		// to indicate "bad request"
		if (StringUtils.isEmpty(subscriptionData)) {
			throw new OperationException("subscriptionData must be present", 400);
		}

		try {

			// if there is no shadow path already defined for this component, this has the side effect of setting
			// things up so ACLs inherit from what is already there.
			socialUtils.getUGCResource(orderResource);
			SocialResourceProvider srp = socialUtils.getSocialResourceProvider(orderResource);
			srp.setConfig(socialUtils.getDefaultStorageConfig());
			if (!socialUtils.mayPost(resolver, orderResource)) {
				throw new OperationException("Order Data can not be created", 400);
			}
			Gson gson = new Gson();
			SubscriptionData data = gson.fromJson(subscriptionData, SubscriptionData.class); 
			
			//First Sending email to avoid updating ASRP
			Map< String, String > emailParams = new HashMap< String, String >();
			emailParams.put( "senderName", "test" );
			emailParams.put( "body", "test" );
			emailParams.put( "senderEmailAddress", "noreply@scholastic.com");
			List<String> failureAddress = emailService.sendEmail( TEMPLATE_PATH, emailParams, data.getBilling().getEmail());
			Map<String, Object> props = new HashMap<String, Object>();
			if(failureAddress.size()>0){
				for(String address : failureAddress){
					props.put(ClassMagsMigrationConstants.EMAIL_SENT, false);
				}
			}
			else{
				props.put(ClassMagsMigrationConstants.EMAIL_SENT, true);
			}
			props.put(ClassMagsMigrationConstants.SUB_TOTAL_PRICE, data.getSubTotalPrice());
			props.put(ClassMagsMigrationConstants.TOTAL_PRICE, data.getTotalPrice());
			props.put(ClassMagsMigrationConstants.TOTAL_SHIPPING_PRICE, data.getTotalShippingPrice());
			props.put(ClassMagsMigrationConstants.BILLING_OPTION, data.getBillingOption());
			props.put("sling:resourceType", PROJECT_RESOURCE_TYPE);
			props.put(JcrConstants.JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED);

			// use the Resource API to create resource to make this data store agnostic
			Resource orderASRPResource =
					srp.create(resolver, socialUtils.resourceToUGCStoragePath(resolver.getResource(PATH)) + "/" + magazineProps.getMagazineName(orderResource.getPath()) + "/"
							+ data.getBilling().getEmail(), props);
			srp.commit( resolver );
			resolver.commit();
			createBillingNode(srp,resolver,orderASRPResource, data);
			createShippingNodes(srp,resolver,orderASRPResource, data);
			createRenewalNodes(srp,resolver,orderASRPResource, data);
			createCartNodes(srp,resolver,orderASRPResource, data);
			return orderASRPResource;
		} catch (PersistenceException e) {
			LOG.error("Unable to create Order", e);
			throw new OperationException("Unable to create Order", 500);
		}
	}

	private void createCartNodes(SocialResourceProvider srp, ResourceResolver resolver, Resource orderASRPResource,
			SubscriptionData data) throws PersistenceException {
		Map<String, Object> props;
		if(data.getCartData() != null && data.getCartData().size() > 0){
			List<CartData> cartList = data.getCartData();
			props = new HashMap<String, Object>();
			props.put(JcrConstants.JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED);
			Resource cart = srp.create(resolver, orderASRPResource.getPath() + "/" + "cartData", props);
			if(cart != null){
				for(CartData cartData : cartList){
					props = new HashMap<String, Object>();
					props.put(JcrConstants.JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED);
					props.put(ClassMagsMigrationConstants.MAGAZINE_PATH, cartData.getMagazinePath());
					props.put(ClassMagsMigrationConstants.MAGAZINE_NAME, cartData.getMagazineName());
					props.put(ClassMagsMigrationConstants.MAGAZINE_TITLE, cartData.getMagazineTitle());
					props.put(ClassMagsMigrationConstants.QUANTITY, cartData.getQuantity());
					props.put(ClassMagsMigrationConstants.MAGAZINE_PRICE, cartData.getMagazinePrice());
					props.put(ClassMagsMigrationConstants.SHIPPING_PERCENTAGE, cartData.getShippingPercentage());
					props.put(ClassMagsMigrationConstants.SUB_TOTAL, cartData.getSubTotal());
					props.put(ClassMagsMigrationConstants.SHIPPING_PRICE, cartData.getShippingPrice());
					Resource cartDetails = srp.create(resolver, cart.getPath() + "/" + cartData.getMagazineTitle(), props);
				}
			}
			srp.commit( resolver );
			resolver.commit();	
		}

	}

	private void createRenewalNodes(SocialResourceProvider srp, ResourceResolver resolver, Resource orderASRPResource,
			SubscriptionData data) throws PersistenceException {
		Map<String, Object> props;
		if(data.getSubscriptionType() != null){
			props = new HashMap<String, Object>();
			SubscriptionType subscriptionType = data.getSubscriptionType();
			props.put(JcrConstants.JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED);
			props.put(ClassMagsMigrationConstants.NEW_SUBSCRIPTION, subscriptionType.getNewSubscription());
			props.put(ClassMagsMigrationConstants.AUTO_RENEWAL, subscriptionType.getIsAutoRenewal());
			Resource renewal = srp.create(resolver, orderASRPResource.getPath() + "/" + "renewal", props);
			srp.commit( resolver );
			resolver.commit();
		}

	}

	private void createShippingNodes(SocialResourceProvider srp, ResourceResolver resolver, Resource orderASRPResource,
			SubscriptionData data) throws PersistenceException {
		Map<String, Object> props;
		if(data.getShipping() != null)
		{
			props = new HashMap<String, Object>();
			Shipping shippingDetails = data.getShipping();
			props.put(ClassMagsMigrationConstants.NAME, shippingDetails.getName());
			props.put(ClassMagsMigrationConstants.EMAIL, shippingDetails.getEmail());
			props.put(ClassMagsMigrationConstants.ADDRESS_LINE_1, shippingDetails.getAddress1());
			props.put(ClassMagsMigrationConstants.ADDRESS_LINE_2, shippingDetails.getAddress2());
			props.put(ClassMagsMigrationConstants.CITY, shippingDetails.getCity());
			props.put(ClassMagsMigrationConstants.ZIP, shippingDetails.getZip());
			props.put(ClassMagsMigrationConstants.PHONE, shippingDetails.getPhone());
			props.put(ClassMagsMigrationConstants.SCHOOL, shippingDetails.getSchool());
			props.put(ClassMagsMigrationConstants.STATE, shippingDetails.getState());
			props.put(JcrConstants.JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED);
			Resource shippingResource = srp.create(resolver, orderASRPResource.getPath() + "/" + "shipping", props);
			srp.commit( resolver );
			resolver.commit();
		}

	}

	private void createBillingNode(SocialResourceProvider srp, ResourceResolver resolver, Resource orderASRPResource,
			SubscriptionData data) throws PersistenceException {
		Map<String, Object> props;
		if(data.getBilling() != null)
		{
			props = new HashMap<String, Object>();
			Billing billingDetails = data.getBilling();
			props.put(ClassMagsMigrationConstants.NAME, billingDetails.getName());
			props.put(ClassMagsMigrationConstants.EMAIL, billingDetails.getEmail());
			props.put(ClassMagsMigrationConstants.ADDRESS_LINE_1, billingDetails.getAddress1());
			props.put(ClassMagsMigrationConstants.ADDRESS_LINE_2, billingDetails.getAddress2());
			props.put(ClassMagsMigrationConstants.CITY, billingDetails.getCity());
			props.put(ClassMagsMigrationConstants.ZIP, billingDetails.getZip());
			props.put(ClassMagsMigrationConstants.PHONE, billingDetails.getPhone());
			props.put(ClassMagsMigrationConstants.SCHOOL, billingDetails.getSchool());
			props.put(ClassMagsMigrationConstants.STATE, billingDetails.getState());
			props.put(JcrConstants.JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED);
			Resource billingResource = srp.create(resolver, orderASRPResource.getPath() + "/" + "billing", props);
			srp.commit( resolver );
			resolver.commit();
		}

	}

}
