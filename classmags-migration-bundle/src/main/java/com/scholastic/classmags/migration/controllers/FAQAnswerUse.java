package com.scholastic.classmags.migration.controllers;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

public class FAQAnswerUse extends WCMUsePojo {

    /** The Constant TWO. */
    private static final int TWO = 2;

    private String questionId;
    private String question;

    @Override
    public void activate() {
        final ValueMap properties = getProperties();
        String questionDetails = properties.get( ClassMagsMigrationConstants.QUESTION, String.class );
        if ( !StringUtils.isEmpty( questionDetails ) ) {
            String[] questionArray = StringUtils.split( questionDetails, ClassMagsMigrationConstants.COLON, TWO );
            questionId = questionArray[ 0 ];
            question = questionArray[ 1 ];
        }

    }
   
    /**
     * Gets the question id.
     *
     * @return the question id
     */
    public String getQuestionId() {
        return questionId;
    }
    
    /**
     * Gets the question.
     *
     * @return the question
     */
    public String getQuestion() {
        return question;
    }

}
