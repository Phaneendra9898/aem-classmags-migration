package com.scholastic.classmags.migration.models;

/**
 * The Class UIdLaunchContextSubHeaderApplication.
 */
public class UIdLaunchContextSubHeaderApplication {

    /** The application code. */
    private String applicationCode;

    /**
     * Gets the application code.
     *
     * @return the applicationCode
     */
    public String getApplicationCode() {
        return applicationCode;
    }

    /**
     * Sets the application code.
     *
     * @param applicationCode
     *            the applicationCode to set
     */
    public void setApplicationCode( String applicationCode ) {
        this.applicationCode = applicationCode;
    }
}
