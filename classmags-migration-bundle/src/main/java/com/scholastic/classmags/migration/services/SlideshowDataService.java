package com.scholastic.classmags.migration.services;

import java.util.List;

import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;

/**
 * The Interface SlideshowDataService.
 */
public interface SlideshowDataService {

    /**
     * Convert list to JSON array.
     *
     * @param fetchMultiFieldData
     *            the fetch multi field data
     * @throws JSONException
     *             the JSON exception
     */
    public JSONArray convertListToJSONArray( List< ValueMap > fetchMultiFieldData ) throws JSONException;
}