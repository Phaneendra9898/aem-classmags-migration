package com.scholastic.classmags.migration.models;

import java.util.List;

import org.apache.solr.client.solrj.SolrQuery;

/**
 * Represents Search Request
 *
 */
public class SearchRequest {
    private String text;
    private String filters;
    private String sort;
    private SolrQuery.ORDER sortOrder = SolrQuery.ORDER.asc;
    private int start;
    private int rows;
    private boolean isValidRequest;
    private List< String > id;
    private String siteId;
    private String role;

    public String getText() {
        return text;
    }

    public void setText( String text ) {
        this.text = text;
    }

    public int getStart() {
        return start;
    }

    public void setStart( int start ) {
        this.start = start;
    }

    public int getRows() {
        return rows;
    }

    public void setRows( int rows ) {
        this.rows = rows;
    }

    public String getSort() {
        return sort;
    }

    public void setSort( String sort ) {
        this.sort = sort;
    }

    public String getFilters() {
        return filters;
    }

    public void setFilters( String filters ) {
        this.filters = filters;
    }

    public boolean isValidRequest() {
        return isValidRequest;
    }

    public void setValidRequest( boolean isValidRequest ) {
        this.isValidRequest = isValidRequest;
    }

    public SolrQuery.ORDER getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder( SolrQuery.ORDER sortOrder ) {
        this.sortOrder = sortOrder;
    }

    public List< String > getIdList() {
        return id;
    }

    public void setIdList( List< String > id ) {
        this.id = id;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId( String siteId ) {
        this.siteId = siteId;
    }

    public String getRole() {
        return role;
    }

    public void setRole( String role ) {
        this.role = role;
    }

}
