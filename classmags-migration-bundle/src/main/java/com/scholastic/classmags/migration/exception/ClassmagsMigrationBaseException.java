package com.scholastic.classmags.migration.exception;

import com.scholastic.classmags.migration.models.ClassmagsMigrationErrorCodes;

/**
 * Base Exception Class for ClassmagsMigration.
 */
public class ClassmagsMigrationBaseException extends Exception {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4423361889339405147L;

    /**
     * Instantiates a new classmags migration base exception.
     */
    public ClassmagsMigrationBaseException() {
        super();
    }

    /**
     * Instantiates a new classmags migration base exception.
     *
     * @param message
     *            the message
     * @param cause
     *            the cause
     * @param enableSuppression
     *            the enable suppression
     * @param writableStackTrace
     *            the writable stack trace
     */
    public ClassmagsMigrationBaseException( String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace ) {
        super( message, cause, enableSuppression, writableStackTrace );
    }

    /**
     * Instantiates a new classmags migration base exception.
     *
     * @param message
     *            the message
     * @param cause
     *            the cause
     */
    public ClassmagsMigrationBaseException( String message, Throwable cause ) {
        super( message, cause );
    }

    /**
     * Instantiates a new classmags migration base exception.
     *
     * @param message
     *            the message
     */
    public ClassmagsMigrationBaseException( String message ) {
        super( message );
    }

    /**
     * Instantiates a new classmags migration base exception.
     *
     * @param cause
     *            the cause
     */
    public ClassmagsMigrationBaseException( Throwable cause ) {
        super( cause );
    }

    /**
     * Instantiates a new classmags migration base exception.
     *
     * @param errorCode
     *            the error code
     * @param cause
     *            the cause
     */
    public ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes errorCode, Throwable cause ) {
        super( errorCode.toString(), cause );
    }

    /**
     * Instantiates a new classmags migration base exception.
     *
     * @param errorCode
     *            the error code
     */
    public ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes errorCode ) {
        super( errorCode.toString() );
    }
}
