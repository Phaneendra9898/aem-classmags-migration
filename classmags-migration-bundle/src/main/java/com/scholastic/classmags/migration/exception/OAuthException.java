package com.scholastic.classmags.migration.exception;

@SuppressWarnings("serial")
public class OAuthException extends RuntimeException {

    public OAuthException(String message, Throwable cause) {
        super(message, cause);
    }
}
