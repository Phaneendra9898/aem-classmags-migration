package com.scholastic.classmags.migration.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * The Class BrightcoveVideoCaption.
 */
public class BrightcoveVideoCaption {

    /** The id. */
    private long id;

    /** The caption sources. */
    @SerializedName( "caption_sources" )
    private List< CaptionSource > captionSources;

    /**
     * Gets the id.
     * 
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id.
     * 
     * @param id
     *            the new id
     */
    public void setId( long id ) {
        this.id = id;
    }

    /**
     * Gets the caption sources.
     * 
     * @return the caption sources
     */
    public List< CaptionSource > getCaptionSources() {
        return captionSources;
    }

    /**
     * Sets the caption sources.
     * 
     * @param captionSources
     *            the new caption sources
     */
    public void setCaptionSources( List< CaptionSource > captionSources ) {
        this.captionSources = captionSources;
    }

}
