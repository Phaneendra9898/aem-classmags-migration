package com.scholastic.classmags.migration.models;

import java.util.List;

import org.apache.sling.commons.json.JSONArray;

/**
 * The Class ResourcesObject.
 */
public class ResourcesObject {

    /** The title. */
    private String title;

    /** The description. */
    private String description;

    /** The image path. */
    private String imagePath;

    /** The video id. */
    private long videoId;

    /** The tags. */
    private List< String > tags;

    /** The page path. */
    private String pagePath;

    /** The video duration. */
    private String videoDuration;

    /** The user role. */
    private String userRole;

    /** The article page path. */
    private String articlePagePath;

    /** The content tile flags. */
    private ContentTileFlags contentTileFlags;

    /** The formatted tags. */
    private String formattedTags;

    /** The magazine type. */
    private String magazineType;

    /** The content type. */
    private String contentType;

    /** The sorting date. */
    private String sortingDate;

    /** The sorting date. */
    private GameModel gameModel;

    /** The bookmark date saved. */
    private String bookmarkDateSaved;

    /** The share link url. */
    private String shareLinkUrl;
    
    /** The bookmark path. */
    private String bookmarkPath;
    
    /** The display date. */
    private String displayDate;
    
    /** The slide show. */
    private Slideshow slideshow;
    
    /**
     * Gets the slideshow.
     * 
     * @return the slideshow
     */
    public Slideshow getSlideshow() {
        return slideshow;
    }

    /**
     * Sets the slideshow.
     * 
     * @param slideshow
     *            the slideshow to set
     */
    public void setSlideshow( Slideshow slideshow ) {
        this.slideshow = slideshow;
    }
    
    /**
     * Gets the game model.
     * 
     * @return the gameModel
     */
    public GameModel getGameModel() {
        return gameModel;
    }

    /**
     * Sets the game model.
     * 
     * @param gameModel
     *            the gameModel to set
     */
    public void setGameModel( GameModel gameModel ) {
        this.gameModel = gameModel;
    }

    /**
     * Gets the title.
     * 
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets the description.
     * 
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Gets the image path.
     * 
     * @return the imagePath
     */
    public String getImagePath() {
        return imagePath;
    }

    /**
     * Gets the video id.
     * 
     * @return the videoId
     */
    public long getVideoId() {
        return videoId;
    }

    /**
     * Sets the title.
     * 
     * @param title
     *            the title to set
     */
    public void setTitle( String title ) {
        this.title = title;
    }

    /**
     * Sets the description.
     * 
     * @param description
     *            the description to set
     */
    public void setDescription( String description ) {
        this.description = description;
    }

    /**
     * Sets the image path.
     * 
     * @param imagePath
     *            the imagePath to set
     */
    public void setImagePath( String imagePath ) {
        this.imagePath = imagePath;
    }

    /**
     * Sets the video id.
     * 
     * @param videoId
     *            the videoId to set
     */
    public void setVideoId( long videoId ) {
        this.videoId = videoId;
    }

    /**
     * Gets the tags.
     * 
     * @return the tags
     */
    public List< String > getTags() {
        return tags;
    }

    /**
     * Sets the tags.
     * 
     * @param tags
     *            the tags to set
     */
    public void setTags( List< String > tags ) {
        this.tags = tags;
    }

    /**
     * Gets the page path.
     * 
     * @return the pagePath
     */
    public String getPagePath() {
        return pagePath;
    }

    /**
     * Sets the page path.
     * 
     * @param pagePath
     *            the pagePath to set
     */
    public void setPagePath( String pagePath ) {
        this.pagePath = pagePath;
    }

    /**
     * Gets the video duration.
     *
     * @return the videoDuration
     */
    public String getVideoDuration() {
        return videoDuration;
    }

    /**
     * Sets the video duration.
     *
     * @param videoDuration
     *            the videoDuration to set
     */
    public void setVideoDuration( String videoDuration ) {
        this.videoDuration = videoDuration;
    }

    /**
     * Gets the user role.
     *
     * @return the userRole
     */
    public String getUserRole() {
        return userRole;
    }

    /**
     * Sets the user role.
     *
     * @param userRole
     *            the userRole to set
     */
    public void setUserRole( String userRole ) {
        this.userRole = userRole;
    }

    /**
     * Gets the article page path.
     *
     * @return the articlePagePath
     */
    public String getArticlePagePath() {
        return articlePagePath;
    }

    /**
     * Sets the article page path.
     *
     * @param articlePagePath
     *            the articlePagePath to set
     */
    public void setArticlePagePath( String articlePagePath ) {
        this.articlePagePath = articlePagePath;
    }

    /**
     * Gets the content tile flags.
     *
     * @return the contentTileFlags
     */
    public ContentTileFlags getContentTileFlags() {
        return contentTileFlags;
    }

    /**
     * Sets the content tile flags.
     *
     * @param contentTileFlags
     *            the contentTileFlags to set
     */
    public void setContentTileFlags( ContentTileFlags contentTileFlags ) {
        this.contentTileFlags = contentTileFlags;
    }

    /**
     * Gets the formatted tags.
     *
     * @return the formattedTags
     */
    public String getFormattedTags() {
        return formattedTags;
    }

    /**
     * Sets the formatted tags.
     *
     * @param formattedTags
     *            the formattedTags to set
     */
    public void setFormattedTags( String formattedTags ) {
        this.formattedTags = formattedTags;
    }

    /**
     * Gets the magazine type.
     *
     * @return the magazineType
     */
    public String getMagazineType() {
        return magazineType;
    }

    /**
     * Sets the magazine type.
     *
     * @param magazineType
     *            the magazineType to set
     */
    public void setMagazineType( String magazineType ) {
        this.magazineType = magazineType;
    }

    /**
     * Gets the content type.
     *
     * @return the contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Sets the content type.
     *
     * @param contentType
     *            the contentType to set
     */
    public void setContentType( String contentType ) {
        this.contentType = contentType;
    }

    /**
     * Gets the sorting date.
     *
     * @return the sorting date
     */
    public String getSortingDate() {
        return sortingDate;
    }

    /**
     * Sets the sorting date.
     *
     * @param sortingDate
     *            the sortingDate to set
     */
    public void setSortingDate( String sortingDate ) {
        this.sortingDate = sortingDate;
    }

    /**
     * Gets the bookmark date saved.
     *
     * @return the bookmarkDateSaved
     */
    public String getBookmarkDateSaved() {
        return bookmarkDateSaved;
    }

    /**
     * Sets the bookmark date saved.
     *
     * @param bookmarkDateSaved
     *            the bookmarkDateSaved to set
     */
    public void setBookmarkDateSaved( String bookmarkDateSaved ) {
        this.bookmarkDateSaved = bookmarkDateSaved;
    }

    /**
     * Gets the share link url.
     *
     * @return the shareLinkUrl
     */
    public String getShareLinkUrl() {
        return shareLinkUrl;
    }

    /**
     * Sets the share link url.
     *
     * @param shareLinkUrl
     *            the shareLinkUrl to set
     */
    public void setShareLinkUrl( String shareLinkUrl ) {
        this.shareLinkUrl = shareLinkUrl;
    }

    /**
     * Gets the bookmark path.
     *
     * @return the bookmarkPath
     */
    public String getBookmarkPath() {
        return bookmarkPath;
    }

    /**
     * Sets the bookmark path.
     *
     * @param bookmarkPath
     *            the bookmarkPath to set
     */
    public void setBookmarkPath( String bookmarkPath ) {
        this.bookmarkPath = bookmarkPath;
    }
    
    /**
     * Gets the display date.
     *
     * @return the display date
     */
    public String getDisplayDate() {
        return displayDate;
    }

    /**
     * Sets the display date.
     *
     * @param displayDate The display date.
     */
    public void setDisplayDate( String displayDate ) {
        this.displayDate = displayDate;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append( "ResourcesObject [title=" );
        builder.append( title );
        builder.append( ", description=" );
        builder.append( description );
        builder.append( ", imagePath=" );
        builder.append( imagePath );
        builder.append( ", videoId=" );
        builder.append( videoId );
        builder.append( ", tags=" );
        builder.append( tags );
        builder.append( ", pagePath=" );
        builder.append( pagePath );
        builder.append( ", articlePagePath=" );
        builder.append( articlePagePath );
        builder.append( ", magazineType=" );
        builder.append( magazineType );
        builder.append( ", contentType=" );
        builder.append( contentType );
        builder.append( ", sortingDate" );
        builder.append( sortingDate );
        builder.append( ", bookmarkDateSaved" );
        builder.append( bookmarkDateSaved );
        builder.append( ", shareLinkUrl" );
        builder.append( shareLinkUrl );
        builder.append( ", bookmarkPath" );
        builder.append( bookmarkPath );        
        builder.append( "]" );
        return builder.toString();
    }
}
