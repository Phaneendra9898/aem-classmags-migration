package com.scholastic.classmags.migration.models;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * The Class Tile.
 */
public class Tile {

    /** The tileimgsrc. */
    private String tileimgsrc;

    /** The tilelinkto. */
    private String tilelinkto;

    /** The tilevideoduration. */
    private String tilevideoduration;
    
    /** The tilevideoduration. */
    private TileTitleSection tiletitlesection;

    /** The tilevideoduration. */
    private List<String> tilesubtitletext;
    
    /** The tilevideoduration. */
    private String tiledesctext;
    
    /** The videoId. */
    private Long videoId;

    /** The tileType. */
    private String tileType;
    
    public String getTileimgsrc() {
        return tileimgsrc;
    }

    public void setTileimgsrc( String tileimgsrc ) {
        this.tileimgsrc = tileimgsrc;
    }

    public String getTilelinkto() {
        return tilelinkto;
    }

    public void setTilelinkto( String tilelinkto ) {
        this.tilelinkto = tilelinkto;
    }

    public String getTilevideoduration() {
        return tilevideoduration;
    }

    public void setTilevideoduration( String tilevideoduration ) {
        this.tilevideoduration = tilevideoduration;
    }

    public TileTitleSection getTiletitlesection() {
        return tiletitlesection;
    }

    public void setTiletitlesection( TileTitleSection tiletitlesection ) {
        this.tiletitlesection = tiletitlesection;
    }

    public List< String > getTilesubtitletext() {
        return tilesubtitletext;
    }

    public void setTilesubtitletext( List< String > tilesubtitletext ) {
        this.tilesubtitletext = tilesubtitletext;
    }
    
    public String getTiledesctext() {
        return tiledesctext;
    }

    public void setTiledesctext( String tiledesctext ) {
        this.tiledesctext = tiledesctext;
    }
    
    public Long getVideoId() {
        return videoId;
    }

    public void setVideoId( Long videoId ) {
        this.videoId = videoId;
    }
    
    public String getTileType() {
        return tileType;
    }

    public void setTileType( String tileType ) {
        this.tileType = tileType;
    }
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString( this );
    }

}
