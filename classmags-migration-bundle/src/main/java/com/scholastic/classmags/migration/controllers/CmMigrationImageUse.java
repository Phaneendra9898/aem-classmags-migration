package com.scholastic.classmags.migration.controllers;

import java.util.UUID;

import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.models.CmMigrationImage;

/**
 * Use Class for CmMigrationImageUse.
 */
public class CmMigrationImageUse extends WCMUsePojo {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( CmMigrationImageUse.class );

    /** The cm migration image. */
    private CmMigrationImage cmMigrationImage;
    
    /** The unique id. */
    private String uniqueId;

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() {
        LOG.info( "Inside the Activate Method for CmMigrationImageUse" );
        SlingHttpServletRequest request = getRequest();
        LOG.info( "Request Details: " + request );
        if ( request != null ) {
            cmMigrationImage = request.adaptTo( CmMigrationImage.class );
            LOG.info( "CmMigrationImage Details: " + cmMigrationImage );
        }
        uniqueId = UUID.randomUUID().toString();
    }

    /**
     * Gets the cm migration image.
     *
     * @return the cm migration image
     */
    public CmMigrationImage getCmMigrationImage() {
        return cmMigrationImage;
    }

    /**
     * Sets the cm migration image.
     *
     * @param cmMigrationImage
     *            the new cm migration image
     */
    public void setCmMigrationImage( CmMigrationImage cmMigrationImage ) {
        this.cmMigrationImage = cmMigrationImage;
    }
    
    /**
     * Gets the unique id.
     *
     * @return the unique id.
     */
    public String getUniqueId() {
        return uniqueId;
    }

}
