package com.scholastic.classmags.migration.utils;

import static com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS;

import javax.servlet.http.Cookie;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Role Util class.
 */
public final class RoleUtil {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( RoleUtil.class );

    /** The Constant ROLE_TYPE_EVERYONE. */
    public static final String ROLE_TYPE_EVERYONE = "everyone";

    /** The Constant ROLE_TYPE_TEACHER_OR_STUDENT. */
    public static final String ROLE_TYPE_TEACHER_OR_STUDENT = "teacherOrStudent";

    /** The Constant ROLE_TYPE_TEACHER. */
    public static final String ROLE_TYPE_TEACHER = "teacher";

    /** The Constant ROLE_TYPE_STUDENT. */
    public static final String ROLE_TYPE_STUDENT = "student";

    /**
     * Instantiates a new role utils.
     */
    private RoleUtil() {
    }

    /**
     * Should render.
     *
     * @param loginRole
     *            the login role
     * @param accessibleTo
     *            the accessible to
     * @return true, if successful
     */
    public static boolean shouldRender( final String loginRole, final String accessibleTo ) {

        if ( StringUtils.isNotBlank( accessibleTo ) && accessibleTo.equalsIgnoreCase( ROLE_TYPE_EVERYONE ) ) {
            return true;
        }
        if ( StringUtils.isNotBlank( accessibleTo ) && accessibleTo.equalsIgnoreCase( ROLE_TYPE_TEACHER_OR_STUDENT )
                && ( loginRole.equalsIgnoreCase( ROLE_TYPE_TEACHER )
                        || loginRole.equalsIgnoreCase( ROLE_TYPE_STUDENT ) ) ) {
            return true;
        }
        if ( StringUtils.isNotBlank( accessibleTo ) && accessibleTo.equalsIgnoreCase( loginRole ) ) {
            return true;
        }
        return false;
    }

    /**
     * Gets the user role.
     *
     * @param request
     *            the request
     * @return the user role
     */
    public static String getUserRole( SlingHttpServletRequest request ) {
        String role = USER_TYPE_ANONYMOUS;
        if ( request != null ) {
            Cookie swAuthCookie = request.getCookie( CmSDMConstants.SW_AUTH_COOKIE_NAME );
            Cookie sdmNavCTXCookie = request.getCookie( CmSDMConstants.SDM_NAV_COOKIE_NAME );
            if ( swAuthCookie != null && sdmNavCTXCookie != null ) {
                LOG.debug( "Encrypted swAuthCookie cookie value is {}", swAuthCookie.getValue() );
                String decryptedCookieValue = EncryptionUtils.decrypt( swAuthCookie.getValue() );
                LOG.debug( "Decrypted swAuthCookie cookie value is {}", decryptedCookieValue );
                if ( StringUtils.isNotBlank( decryptedCookieValue ) ) {
                    role = decryptedCookieValue;
                }
            }
        }
        LOG.debug( "User Role is {}", role );
        return role;
    }
}