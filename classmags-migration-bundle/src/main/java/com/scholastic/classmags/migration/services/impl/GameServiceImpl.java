package com.scholastic.classmags.migration.services.impl;

import java.util.UUID;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.GameModel;
import com.scholastic.classmags.migration.services.GameService;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

@Component( immediate = true, metatype = false )
@Service( GameService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Game Service" ) } )
public class GameServiceImpl implements GameService {
    
    private static final String RESOURCE_TYPE_GAME = "scholastic/classroom-magazines-migration/components/content/game-html5";
    private static final String IMAGE_RENDITION=".transform/game/image.png";
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /** The prop config service. */
    @Reference
    PropertyConfigService propertyConfigService;

    /** The resource path config service. */
    @Reference
    private ResourcePathConfigService resourcePathConfigService;

    /** The magazine props service. */
    @Reference
    private MagazineProps magazineProps;

    @Override
    public GameModel fetchGameData( Resource resource )
            throws ClassmagsMigrationBaseException, RepositoryException, LoginException {

        String flashvarsValue;

        Node node = resource.adaptTo( Node.class );
        String assetPath = CommonUtils.propertyCheck( "gamePath", node );
        Resource assetResource = CommonUtils.getResource( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE, assetPath );
        Asset asset = assetResource != null ? assetResource.adaptTo( Asset.class ) : null;
        Resource metadataRes = assetResource != null ? assetResource.getChild( "jcr:content/metadata" ) : null;
        
        GameModel data = new GameModel();

        if ( asset != null ) {

            data.setContainerId( CommonUtils.generateUniqueIdentifier() );

            data.setTitle( CommonUtils.propertyCheck( "title", node ).isEmpty()
                    ? StringUtils.defaultIfBlank( asset.getMetadataValue( DamConstants.DC_TITLE ), asset.getName() )
                    : CommonUtils.propertyCheck( "title", node ) );

            data.setDescription( CommonUtils.propertyCheck( "description", node ).isEmpty()
                    ? asset.getMetadataValue( DamConstants.DC_DESCRIPTION )
                    : CommonUtils.propertyCheck( "description", node ) );
            
            if ( metadataRes != null ) {
                data.setUserType( metadataRes.getValueMap().get( "userType", StringUtils.EMPTY ) );
            }

            data.setThumbnailImg( CommonUtils.propertyCheck( "fileReference", node ).isEmpty()
                    ? asset.getPath() + IMAGE_RENDITION : CommonUtils.propertyCheck( "fileReference", node ) );

            if ( StringUtils.equals( resource.getResourceType(), RESOURCE_TYPE_GAME ) ) {
                data.setGamePath( assetPath.concat( ".html" ) );
            } else {
                if ( null != magazineProps && null != resourcePathConfigService && null != propertyConfigService ) {
                    String magazineName = magazineProps.getMagazineName( resource.getPath() );
                    data.setContainerPath( propertyConfigService.getDataConfigProperty(
                            ClassMagsMigrationConstants.FLASH_GAME_CONTAINER_PROPERTY,
                            CommonUtils.getDataPath( resourcePathConfigService, magazineName ) ) );

                    if ( metadataRes != null ) {
                        data.setGameType( metadataRes.getValueMap().get( "gameType", StringUtils.EMPTY ) );
                        data.setXMLPath( metadataRes.getValueMap().get( "xmlPath", StringUtils.EMPTY ) );
                    }

                    if ( data.getXMLPath() != null && data.getXMLPath() != StringUtils.EMPTY ) {
                        flashvarsValue = "gametype=" + data.getGameType() + "&contentXML=" + data.getXMLPath()
                                + "&assetSWF=" + CommonUtils.propertyCheck( "gamePath", node );
                    } else {
                        flashvarsValue = "gametype=" + data.getGameType() + "&assetSWF="
                                + CommonUtils.propertyCheck( "gamePath", node );
                    }
                    data.setGamePath( flashvarsValue );
                }
            }
        }
        return data;
    }
}
