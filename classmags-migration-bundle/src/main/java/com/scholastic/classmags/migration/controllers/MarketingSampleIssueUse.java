package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.models.ContentTileObject;
import com.scholastic.classmags.migration.services.SampleIssueDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

public class MarketingSampleIssueUse extends WCMUsePojo {

    
    /** The content tile object. */
    private ContentTileObject contentTileObject;
    
    /** The Button Value. */
    private String buttonValue;
    
    /** The Button Link. */
    private String buttonHyperLink;

   	/** The Constant ISSUE_KEY_ASSETS_LABEL. */
    private static final String CONTENT_TYPE_LABEL = "contentTileContentType";

    /** The Constant ISSUE_KEY_ASSETS_PATH. */
    private static final String CONTENT_TILE_LINK = "contentTileLink";
    
    /** The Constant Asset Label. */
    private static final String ASSET_LABEL = "issueKeyAssetsLabel";
    
    /** The new window Label. */
    private static final String NEW_WINDOW = "newWindow";

  
    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( MarketingSampleIssueUse.class );
    
    /** The asset data list. */
	private List<ContentTileObject> tileObject;
	
	private ResourceResolver resourceResolver;

	/**
     * Gets the asset data list.
     *
     * @return the assetDataList
     */
    public List<ContentTileObject> getTileObject() {
		return tileObject;
	}
    
    /**
     * Sets the asset data list.
     *
     *@param tileObject
     *	sets the contentTile object
     */
	public void setTileObject(ArrayList<ContentTileObject> tileObject) {
		this.tileObject = tileObject;
	}
	
	/**
     * Gets the Upper Case Button Value.
     *
     * @return the buttonValue
     */
	public String getButtonValue() {
		return buttonValue;
	}
	
	/**
     * Sets the Button Value in Upper Case.
     *
     *@param buttonValue
     *	sets the Button Value
     */
	public void setButtonValue(String buttonValue) {
		this.buttonValue = buttonValue;
	}
	
	/**
     * Gets the  Button Link.
     *
     * @return the buttonValue
     */
	 public String getButtonHyperLink() {
			return buttonHyperLink;
		}
	 /**
	     * Sets the Button Link.
	     *
	     *@param buttonHyperLink
	     *	sets the Button Link
	     */
	public void setButtonHyperLink(String buttonHyperLink) {
			this.buttonHyperLink = buttonHyperLink;
		}
	
	/** The Sample Issue service. */
    SampleIssueDataService sampleIssueDataService;

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() {
    	resourceResolver = getResourceResolver();
    	if(getProperties().get("buttonTitle", String.class) != null){
			buttonValue = getProperties().get("buttonTitle", String.class);
    	}
    	if(getProperties().get("buttonLink", String.class) != null){
        	String hyperLink = getProperties().get("buttonLink", String.class);
        	buttonHyperLink = InternalURLFormatter.formatURL(resourceResolver,hyperLink );
        }
    	
    	
        List< ValueMap > assetDataMapList;
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        Page currentPage = getCurrentPage();
        if ( null != slingScriptHelper ) {
            String currentPath = currentPage.getPath();
            try {
            	sampleIssueDataService = slingScriptHelper
                        .getService( SampleIssueDataService.class );
                if ( null != sampleIssueDataService ) {
                    assetDataMapList = sampleIssueDataService.fetchAssetData( currentPath );
                    
                    populateContentTileObject(assetDataMapList);
                }	
            } catch ( Exception e ) {
                LOG.error( e.getMessage(), e );
            }
        }
    }

    /**
     * Adds the items to asset data list.
     *
     * @param assetDataMapList
     *            the asset data map list
     */
    private void populateContentTileObject( List< ValueMap > assetDataMapList ) {
    	tileObject = new ArrayList<>();
    	for(ValueMap assetDataMap : assetDataMapList){
    		
    		String videoPath = assetDataMap.get(CONTENT_TILE_LINK, StringUtils.EMPTY);
    		String assetPath = assetDataMap.get(CONTENT_TILE_LINK, StringUtils.EMPTY);
    		String contentType = assetDataMap.get(CONTENT_TYPE_LABEL, StringUtils.EMPTY);
    		String assetLabel = assetDataMap.get(ASSET_LABEL, StringUtils.EMPTY);
    		String newWindow = assetDataMap.get(NEW_WINDOW, StringUtils.EMPTY);
    		
    		
    		switch ( assetDataMap.get(CONTENT_TYPE_LABEL, StringUtils.EMPTY) ) {
            
            case ClassMagsMigrationConstants.VIDEO:
                setContentTileObject( sampleIssueDataService.getVideoContent(videoPath) );
                contentTileObject.setContentType(contentType);
                contentTileObject.setAssetLabel(assetLabel);
                contentTileObject.setNewWindow(newWindow);
                tileObject.add(contentTileObject);
                break;
            case ClassMagsMigrationConstants.SKILLS_SHEET:
            case ClassMagsMigrationConstants.LESSON_PLAN:
            case ClassMagsMigrationConstants.EXPERIMENT:
                setContentTileObject( sampleIssueDataService.getAssetContent( assetPath) );
                contentTileObject.setContentType(contentType);
                contentTileObject.setAssetLabel(assetLabel);
                contentTileObject.setNewWindow(newWindow);
                tileObject.add(contentTileObject);
                break;
            default:
                break;
            }
    	}
    }
    /**
     * Gets the content tile object.
     * 
     * @return the contentTileObject
     */
    public ContentTileObject getContentTileObject() {
        return contentTileObject;
    }
    /**
     * Sets the content tile object.
     * 
     * @param contentTileObject
     *            the contentTileObject to set
     */
    public void setContentTileObject( ContentTileObject contentTileObject ) {
        this.contentTileObject = contentTileObject;
    }

}
