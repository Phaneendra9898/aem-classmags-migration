package com.scholastic.classmags.migration.models;

import org.apache.commons.lang3.StringUtils;

import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * The Enum TeachingResourcesType.
 */
public enum TeachingResourcesType {

    /** The skills sheet. */
    SKILLS_SHEET( ClassMagsMigrationConstants.SKILLS_SHEET, ClassMagsMigrationConstants.ASSET ),

    /** The video. */
    VIDEO( ClassMagsMigrationConstants.VIDEO, ClassMagsMigrationConstants.ASSET ),

    /** The game. */
    GAME( ClassMagsMigrationConstants.GAME, ClassMagsMigrationConstants.ASSET ),

    /** The slideshow. */
    SLIDESHOW( ClassMagsMigrationConstants.SLIDESHOW, ClassMagsMigrationConstants.COMPONENT ),

    /** The lesson plan. */
    LESSON_PLAN( ClassMagsMigrationConstants.LESSON_PLAN, ClassMagsMigrationConstants.ASSET ),

    /** The download. */
    DOWNLOAD( ClassMagsMigrationConstants.DOWNLOAD, ClassMagsMigrationConstants.ASSET ),

    /** The poll. */
    POLL( ClassMagsMigrationConstants.POLL, ClassMagsMigrationConstants.PAGE ),

    /** The blog. */
    BLOG( ClassMagsMigrationConstants.BLOG, ClassMagsMigrationConstants.PAGE ),
    
    /** The article. */
    ARTICLE( ClassMagsMigrationConstants.ARTICLE, ClassMagsMigrationConstants.PAGE );

    /** The teaching resource type. */
    private final String teachingResourceType;

    /** The content type. */
    private final String contentType;

    /**
     * Instantiates a new teaching resources type.
     * 
     * @param teachingResourceType
     *            the teaching resource type
     * @param contentType
     *            the content type
     */
    private TeachingResourcesType( final String teachingResourceType, final String contentType ) {
        this.teachingResourceType = teachingResourceType;
        this.contentType = contentType;
    }

    /**
     * Gets the teaching resource type.
     * 
     * @return the teachingResourceType
     */
    public String getTeachingResourceType() {
        return teachingResourceType;
    }

    /**
     * Gets the content type.
     * 
     * @return the contentType
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Gets the resource content type.
     * 
     * @param resourceType
     *            the resource type
     * @return the resource content type
     */
    public static String getResourceContentType( String resourceType ) {
        for ( TeachingResourcesType resource : TeachingResourcesType.values() ) {
            if ( StringUtils.equalsIgnoreCase( resource.getTeachingResourceType(), resourceType ) ) {
                return resource.getContentType();
            }
        }
        return StringUtils.EMPTY;
    }
}
