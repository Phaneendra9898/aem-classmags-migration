package com.scholastic.classmags.migration.models;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.ChildResource;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.apache.solr.client.solrj.beans.Field;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.TagManager;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.SolrSearchQueryConstants;

@Model( adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL )
public class PageContent {

    private static final Logger log = LoggerFactory.getLogger( PageContent.class );

    @Inject
    private ResourceResolver resourceResolver;

    /** The property config service. */
    @OSGiService
    private PropertyConfigService propertyConfigService;

    /** The resource path config service. */
    @OSGiService
    private ResourcePathConfigService resourcePathConfigService;

    @Field( "id" )
    private String id;

    @Field( "site_id_s" )
    private String siteId;

    @Field( "type_s" )
    private String type;

    @Field( "title_t" )
    @Inject
    @Named( "jcr:title" )
    private String title;
    
    @Field( "search_title_s" )
    @Inject
    @Named( "jcr:title" )
    private String searchTitle;

    @Field( "description_t" )
    @Inject
    @Named( "jcr:description" )
    private String description;

    @Field( "keywords_t" )
    @Inject
    @Named( "keywords" )
    private String keyWords;

    @Inject
    @Named( "cq:tags" )
    private String[] cqTags;

    @Field( "thumbnail_s" )
    @Inject
    @Named( "fileReference" )
    private String thumbnail;
    
    @Field( "user_type_s" )
    @Inject
    @Named( "userType" )
    private String userType;

    @ChildResource( name = "col-ctrl/col1" )
    private Resource articleTextColumn;

    @Self
    private Resource resource;

    @Field( "published_date_t" )
    private String publishedDate;

    @Field( "tags_txt" )
    private List< String > tags;

    @Field( "subject_tags_ss" )
    private Set< String > subjectTags;

    @Field( "article_title_t" )
    private String articleTitle;

    @Field( "article_content_t" )
    private String articleTexts;

    @Field( "article__text_caption_t" )
    private String articleCaption;

    @Field( "article_text_credit_t" )
    private String articleCredit;

    @Field( "container_url_s" )
    private String containerUrl;
    
    @Field("sort_date_dt")
    private Date sortDate;

    @Field("video_id_s")
    private String videoId;
    
    @Field("share_url_s")
    private String shareUrl;

    @Field("downloadable_b")
    private boolean downloadable;
    
    @Field("shareable_b")
    private boolean shareable;
    
    @Field("game_type_s")
    private String gameType;
    
    @Field("game_path_s")
    private String gamePath;
    
    @Field("flash_container_s")
    private String flashContainer;
    
    @Field("unique_id_s")
    private String uniqueId;

    @Field("video_duration_s")
    private String videoDuration;

    @Field( "section_text_t" )
    private String sectionText;

    @Field( "section_title_t" )
    private String sectionTitle;
    
    @Field( "tags_ss" )
    private Set< String > tagIds;
    
    @Field( "bookmarkPath_s" )
    private String bookmarkPath;

    
    /**
     * default constructor
     */
    public PageContent() {

    }

    /**
     * Construct page content from dam metada
     * @param metadata
     */
    public PageContent( DAMMetadata metadata ) {
        this.id = metadata.getId();
        this.bookmarkPath =  metadata.getId();
        this.publishedDate = metadata.getPublishedDate();
        this.tags = metadata.getTags();
        this.subjectTags=metadata.getSubjectTags();
        this.title = metadata.getTitle();
        this.searchTitle = metadata.getSearchTitle();
        this.description = metadata.getDescription();
        this.thumbnail = metadata.getThumbnailPath();
        this.type = metadata.getType();
        this.containerUrl = metadata.getArticlePath();
        this.siteId = metadata.getSiteId();
        this.keyWords=metadata.getKeyWords();
        this.sortDate=metadata.getSortDate();
        this.videoId= metadata.getVideoId();
        this.userType = metadata.getUserType();
        this.shareUrl = metadata.getShareUrl();
        this.downloadable = metadata.isDownloadable();
        this.shareable = metadata.isShareable();
        this.gamePath=metadata.getGamePath();
        this.gameType=metadata.getGameType();
        this.flashContainer=metadata.getFlashContainer();
        this.uniqueId=CommonUtils.generateUniqueIdentifier();
        this.videoDuration = metadata.getVideoDuration();
    }

    public Iterator< Resource > getChildren() {
        if ( articleTextColumn != null ){
            return articleTextColumn.listChildren();
        }
        return null;
    }

    @PostConstruct
    public final void init() {
        if(StringUtils.isNotBlank( searchTitle )){
            this.searchTitle = searchTitle.toLowerCase();
        }
        // include article text in the index
        addArticleContent();
        // add user friendly tags to index
        addTags();

        convertAllFieldsToPlainText();
    }

    private void addArticleContent() {
    	this.sectionText = StringUtils.EMPTY;
    	this.sectionTitle = StringUtils.EMPTY;
        Iterator< Resource > it = getChildren();
        while ( null != it && it.hasNext() ) {
            Resource childResource = it.next();
            if ( childResource != null && childResource.getResourceType()
                    .equalsIgnoreCase( ClassMagsMigrationConstants.ARTICLE_TEXT_RESOURCE_TYPE ) ) {
                ArticleText articleTxtModel = childResource.adaptTo( ArticleText.class );
                if ( articleTxtModel != null ) {
                    this.setArticleCaption( articleTxtModel.getArticleTextcaption() );
                    this.setArticleCredit( articleTxtModel.getArticleTextCredits() );
                    this.articleTexts = articleTxtModel.getContent();
                }
            } else if ( childResource != null && childResource.getResourceType()
                    .equalsIgnoreCase( ClassMagsMigrationConstants.ARTICLE_TITLE_RESOURCE_TYPE ) ) {
                ValueMap properties = childResource.adaptTo( ValueMap.class );
                this.articleTitle = properties.get( "rtefield", String.class );
            } else if ( childResource != null && childResource.getResourceType()
                    .equalsIgnoreCase( ClassMagsMigrationConstants.ARTICLE_SIDEBAR_RESOURCE_TYPE ) ) {
            	log.debug("Found article side bar.............");
                
                ArticleSideBar articleTxtModel = childResource.adaptTo( ArticleSideBar.class );
                if ( articleTxtModel != null ) {
                	if(StringUtils.isNotBlank(articleTxtModel.getSectionTitle())){
                		this.sectionTitle = this.sectionTitle.concat(articleTxtModel.getSectionTitle().concat(" "));
                	}
                	if(StringUtils.isNotBlank(articleTxtModel.getSectionText())){
                		this.sectionText = this.sectionText.concat(articleTxtModel.getSectionText().concat(" "));
                	}
                }
            }
        }
        log.debug("sectionTitle : {}",sectionTitle);
        log.debug("sectionText : {}",sectionText);
    }
    
    private void addTags() {
        this.tags = CommonUtils.getTags( resource, resourceResolver.adaptTo( TagManager.class ) );
        this.subjectTags = CommonUtils.getAllSubjectTags( resource, resourceResolver.adaptTo( TagManager.class ) );
        log.info( "Subject Tags : {}",subjectTags );
        this.tagIds = CommonUtils.getAllTagIds( resource, resourceResolver.adaptTo( TagManager.class ) );
        log.info( "Tag Ids : {}", tagIds );
    }

    public void convertAllFieldsToPlainText() {
        this.type = toPlainText(type);
        this.title = toPlainText(title);
        this.searchTitle = toPlainText(searchTitle);
        this.description = toPlainText(description);
        this.keyWords = toPlainText(keyWords);
        
        this.articleTitle = toPlainText(articleTitle);
        this.articleCaption = toPlainText(articleCaption);
        this.articleCredit = toPlainText(articleCredit);
    }
    
    public String toPlainText(String text){
        String plainText=StringUtils.EMPTY;
        if(StringUtils.isNotBlank( text )){
            plainText = Jsoup.parse(text.trim()).text();
        }
        return plainText;
    }

    public String getId() {
        return id;
    }

    public void setId( String id ) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle( String title ) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription( String description ) {
        this.description = description;
    }

    public String[] getCqTags() {
        return cqTags;
    }

    public void setCqTags( String[] cqTags ) {
        this.cqTags = cqTags;
    }

    public Resource getArticleTextColumn() {
        return articleTextColumn;
    }

    public void setArticleTextColumn( Resource articleTextColumn ) {
        this.articleTextColumn = articleTextColumn;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate( String publishedDate ) {
        this.publishedDate = publishedDate;
    }

    public List< String > getTags() {
        return tags;
    }

    public void setTags( List< String > tags ) {
        this.tags = tags;
    }

    public String getArticleTexts() {
        return articleTexts;
    }

    public void setArticleTexts( String articleTexts ) {
        this.articleTexts = articleTexts;
    }

    public String getType() {
        return type;
    }

    public void setType( String type ) {
        this.type = type;
    }

    public String getArticleCaption() {
        return articleCaption;
    }

    public void setArticleCaption( String articleCaption ) {
        this.articleCaption = articleCaption;
    }

    public String getArticleCredit() {
        return articleCredit;
    }

    public void setArticleCredit( String articleCredit ) {
        this.articleCredit = articleCredit;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId( String siteId ) {
        this.siteId = siteId;
    }
    
    public String getVideoId() {
        return videoId;
    }

    public void setVideoId( String videoId ) {
        this.videoId = videoId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType( String userType ) {
        this.userType = userType;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl( String shareUrl ) {
        this.shareUrl = shareUrl;
    }

    public boolean isDownloadable() {
        return downloadable;
    }

    public void setDownloadable( boolean downloadable ) {
        this.downloadable = downloadable;
    }

    public boolean isShareable() {
        return shareable;
    }

    public void setShareable( boolean shareable ) {
        this.shareable = shareable;
    }

	public String getGameType() {
		return gameType;
	}

	public void setGameType(String gameType) {
		this.gameType = gameType;
	}

	public String getGamePath() {
		return gamePath;
	}

	public void setGamePath(String gamePath) {
		this.gamePath = gamePath;
	}

	public String getFlashContainer() {
		return flashContainer;
	}

	public void setFlashContainer(String flashContainer) {
		this.flashContainer = flashContainer;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	public Date getSortDate() {
		return sortDate;
	}

	public void setSortDate(Date sortDate) {
		this.sortDate = sortDate;
	}
	
	public String getSolrIdQuery() {
		return SolrSearchQueryConstants.SQ_FIELD_ID+":"+ "\""+id+"\"";
	}
	
	public String getSectionText() {
		return sectionText;
	}

	public void setSectionText(String sectionText) {
		this.sectionText = sectionText;
	}

	public String getSectionTitle() {
		return sectionTitle;
	}

	public void setSectionTitle(String sectionTitle) {
		this.sectionTitle = sectionTitle;
	}

	/**
     * @return the tagIds
     */
    public Set< String > getTagIds() {
        return tagIds;
    }

    /**
     * @param tagIds the tagIds to set
     */
    public void setTagIds( Set< String > tagIds ) {
        this.tagIds = tagIds;
    }

    /**
     * @return the bookmarkPath
     */
    public String getBookmarkPath() {
        return bookmarkPath;
    }

    /**
     * @param bookmarkPath the bookmarkPath to set
     */
    public void setBookmarkPath( String bookmarkPath ) {
        this.bookmarkPath = bookmarkPath;
    }

    @Override
    public String toString() {
        return "PageContent [id=" + id + ", siteId=" + siteId + ", type=" + type + ", title=" + title + ", searchTitle="
                + searchTitle + ", description=" + description + ", keyWords=" + keyWords + ", thumbnail=" + thumbnail
                + ", userType=" + userType + ", articleTextColumn=" + articleTextColumn + ", publishedDate="
                + publishedDate + ", tags=" + tags + ", subjectTags=" + subjectTags + ", tagIds=" + tagIds
                + ", articleTitle=" + articleTitle + ", bookmarkPath=" + bookmarkPath + ", articleTexts=" + articleTexts
                + ", articleCaption=" + articleCaption + ", articleCredit=" + articleCredit + ", containerUrl="
                + containerUrl + ", sortDate=" + sortDate + ", videoId=" + videoId + ", shareUrl=" + shareUrl
                + ", downloadable=" + downloadable + ", shareable=" + shareable + ", gameType=" + gameType
                + ", gamePath=" + gamePath + ", flashContainer=" + flashContainer + ", uniqueId=" + uniqueId
                + ", videoDuration=" + videoDuration + ", sectionText=" + sectionText + ", sectionTitle=" + sectionTitle
                + "]";
    }

}