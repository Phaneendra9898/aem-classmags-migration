package com.scholastic.classmags.migration.services;

import java.util.List;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;

/**
 * The Interface Get Epub Service.
 */
public interface GetEpubService {

    /**
     * Gets the page ids.
     * 
     * @param eReaderPath The path to the eReader.
     * 
     * @return The page ids.
     * 
     * @throws ClassmagsMigrationBaseException 
     * 
     */
    List< String > getPageIds( String eReaderPath ) throws ClassmagsMigrationBaseException;
}
