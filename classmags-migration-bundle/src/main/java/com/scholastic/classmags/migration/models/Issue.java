package com.scholastic.classmags.migration.models;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * The Class Issue.
 */
public class Issue {

    /** The issuedate. */
    private String issuedate;

    /** The imgsrc. */
    private String imgsrc;

    /** The linkto. */
    private String linkto;

    /** The date format. */
    private String dateFormat;
    
    /** The issue display date. */
    private String issueDisplayDate;
    
    /** The featured content. */
    private List<Tile> tiles;

    /**
     * Gets the issuedate.
     * 
     * @return The issuedate
     */
    public String getIssuedate() {
        return issuedate;
    }

    /**
     * Sets the issuedate.
     * 
     * @param issuedate
     *            The issuedate
     */
    public void setIssuedate( String issuedate ) {
        this.issuedate = issuedate;
    }

    /**
     * Gets the imgsrc.
     * 
     * @return The imgsrc
     */
    public String getImgsrc() {
        return imgsrc;
    }

    /**
     * Sets the imgsrc.
     * 
     * @param imgsrc
     *            The imgsrc
     */
    public void setImgsrc( String imgsrc ) {
        this.imgsrc = imgsrc;
    }

    /**
     * Gets the linkto.
     * 
     * @return The linkto
     */
    public String getLinkto() {
        return linkto;
    }

    /**
     * Sets the linkto.
     * 
     * @param linkto
     *            The linkto
     */
    public void setLinkto( String linkto ) {
        this.linkto = linkto;
    }

    /**
     * Gets the date format.
     * 
     * @return the dateFormat
     */
    public String getDateFormat() {
        return dateFormat;
    }

    /**
     * Sets the date format.
     * 
     * @param dateFormat
     *            the dateFormat to set
     */
    public void setDateFormat( String dateFormat ) {
        this.dateFormat = dateFormat;
    }

    /**
     * Gets the featured tiles.
     * 
     * @return the tiles
     */
    public List<Tile> getTiles() {
        return tiles;
    }

    /**
     * Sets the featured tiles.
     * 
     * @param tiles
     *            the tiles to set
     */
    public void setTiles( List<Tile> tiles ) {
        this.tiles = tiles;
    }
    
    /**
     * Gets the issue display date.
     * 
     * @return issueDisplayDate The issue display date.
     */
    public String getIssueDisplayDate() {
        return issueDisplayDate;
    }

    /**
     * Sets the issue dispay date.
     * 
     * @param issue display date
     *            The issue display date
     */
    public void setIssueDisplayDate( String issueDisplayDate ) {
        this.issueDisplayDate = issueDisplayDate;
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString( this );
    }
}
