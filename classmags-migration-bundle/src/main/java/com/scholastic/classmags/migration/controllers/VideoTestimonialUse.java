package com.scholastic.classmags.migration.controllers;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.models.ContentTileObject;
import com.scholastic.classmags.migration.services.SampleIssueDataService;

public class VideoTestimonialUse extends WCMUsePojo {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(VideoTestimonialUse.class);
	
	/** The Constant Video Link. */
	 private static final String VIDEO_CONTENT_PATH = "videoContentLink";

	 private ContentTileObject contentObject;
	  
	 /** The Sample Issue service. */
	 SampleIssueDataService sampleIssueDataService;

	@Override
	public void activate() throws Exception {
		
		SlingHttpServletRequest request = getRequest();
        Resource currentResource = request.getResource();
        ValueMap componentProperties = currentResource.getValueMap();
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();

        if (null != componentProperties) {
            try {
                sampleIssueDataService = slingScriptHelper.getService(SampleIssueDataService.class);
                if (null != sampleIssueDataService) {
                    populateVideoGameObjects(componentProperties);
                }
            } catch (Exception e) {
                LOG.error(e.getMessage(), e);
            }
        }

        LOG.info("End of activate method");  
	  		
	}
	
	private void populateVideoGameObjects(ValueMap componentProps) {
        
        String videoContentPath = componentProps.get(VIDEO_CONTENT_PATH, StringUtils.EMPTY);
        setContentObject(sampleIssueDataService.getVideoContent(videoContentPath));
        
        LOG.info("Content objects populated");

    }

	/**
     * Gets the content tile object.
     * 
     * @return the contentTileObject
     */
	public ContentTileObject getContentObject() {
		return contentObject;
	}
	
	/**
     * Sets the content tile object.
     * 
     * @param contentTileObject
     *            the contentTileObject to set
     */
	public void setContentObject(ContentTileObject contentObject) {
		this.contentObject = contentObject;
	}


}
