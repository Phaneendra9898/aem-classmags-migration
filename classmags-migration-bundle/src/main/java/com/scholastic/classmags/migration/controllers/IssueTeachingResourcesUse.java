package com.scholastic.classmags.migration.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.services.ContentTileFlagDataService;
import com.scholastic.classmags.migration.services.TeachingResourcesService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * The Class IssueTeachingResourcesUse.
 */
public class IssueTeachingResourcesUse extends WCMUsePojo {

    /** The teaching resources. */
    private Map< String, List< ResourcesObject > > teachingResources = new HashMap< >();

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( IssueTeachingResourcesUse.class );

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() {
        LOG.info( "::::In activate method of IssueTeachingResourcesUse::::" );
        Page currentPage = getCurrentPage();
        Resource jcrContent = currentPage != null ? currentPage.getContentResource() : null;
        Resource issueConfig = ( jcrContent != null
                && jcrContent.getChild( ClassMagsMigrationConstants.ISSUE_CONFIGURATION ) != null )
                        ? jcrContent.getChild( ClassMagsMigrationConstants.ISSUE_CONFIGURATION ) : null;
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        if ( issueConfig != null ) {
            TeachingResourcesService teachingResourcesService = slingScriptHelper
                    .getService( TeachingResourcesService.class );
            setTeachingResources( teachingResourcesService != null
                    ? teachingResourcesService.getTeachingResources( getRequest(), issueConfig, "resources" ) : null );
            try {
                setContentTileFlags( slingScriptHelper, currentPage.getPath() );
            } catch ( ClassmagsMigrationBaseException e ) {
                LOG.error( e.getMessage(), e );
            }
        }
    }

    /**
     * Sets the content tile flags.
     *
     * @param slingScriptHelper
     *            the sling script helper
     * @param issuePath
     *            the issue path
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private void setContentTileFlags( SlingScriptHelper slingScriptHelper, String issuePath )
            throws ClassmagsMigrationBaseException {

        ContentTileFlagDataService contentTileFlagDataService = slingScriptHelper
                .getService( ContentTileFlagDataService.class );
        if ( null != contentTileFlagDataService ) {
            contentTileFlagDataService.configureArticlePageAndFlagData( issuePath, getTeachingResources() );
        }
    }

    /**
     * Gets the teaching resources.
     * 
     * @return the teachingResources
     */
    public Map< String, List< ResourcesObject > > getTeachingResources() {
        return teachingResources;
    }

    /**
     * Sets the teaching resources.
     * 
     * @param teachingResources
     *            the teachingResources to set
     */
    public void setTeachingResources( Map< String, List< ResourcesObject > > teachingResources ) {
        this.teachingResources = teachingResources;
    }
}
