package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.commons.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.IssueHighlight;
import com.scholastic.classmags.migration.models.KeyAssets;
import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.services.IssueHighlightDataService;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * Use Class for Issue Highlights.
 */
public class IssueHighlightsUse extends WCMUsePojo {

    /** The sorting date. */
    private String sortingDate;

    /** The featured resources. */
    private List< ResourcesObject > featuredResources = new ArrayList();

    /** The asset data list. */
    private List< KeyAssets > assetDataList;

    /** The issue highlight. */
    private IssueHighlight issueHighlight;

    /** The issue bookmark path. */
    private String issueBookmarkPath;

    /** The user type *. */
    private String userType;

    /** The display date. */
    private String displayDate;

    /** The magazine name. */
    private String magazineName;

    /** The Constant ISSUE_KEY_ASSETS_LABEL. */
    private static final String ISSUE_KEY_ASSETS_LABEL = "issueKeyAssetsLabel";

    /** The Constant ISSUE_KEY_ASSETS_PATH. */
    private static final String ISSUE_KEY_ASSETS_PATH = "issueKeyAssetsLabelPath";

    /** The Constant ISSUE_KEY_USER_TYPE. */
    private static final String ISSUE_KEY_USER_TYPE = "userType";

    /** The Constant ISSUE_KEY_USER_TYPE. */
    private static final String ISSUE_KEY_ASSETS_DESCRIPTION = "description";

    /** The Constant ISSUE_KEY_LABEL_COLOR. */
    private static final String ISSUE_KEY_LABEL_COLOR = "labelColor";

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( IssueHighlightsUse.class );

    /**
     * Sets the issue highlight.
     *
     * @param issueHighlight
     *            the issueHighlight to set
     */
    public void setIssueHighlight( IssueHighlight issueHighlight ) {
        this.issueHighlight = issueHighlight;
    }

    /**
     * Gets the issue highlight.
     *
     * @return the issueHighlight
     */
    public IssueHighlight getIssueHighlight() {
        return issueHighlight;
    }

    /**
     * Gets the asset data list.
     *
     * @return the assetDataList
     */
    public List< KeyAssets > getAssetDataList() {
        return assetDataList;
    }

    /**
     * Gets the sorting date.
     *
     * @return the sortingDate
     */
    public String getSortingDate() {
        return sortingDate;
    }

    /**
     * Gets the user type.
     *
     * @return the userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * Gets the issue bookmark path.
     *
     * @return the issueBookmarkPath
     */
    public String getIssueBookmarkPath() {
        return issueBookmarkPath;
    }

    /**
     * Sets the issue bookmark path.
     *
     * @param issueBookmarkPath
     *            the issueBookmarkPath to set
     */
    public void setIssueBookmarkPath( String issueBookmarkPath ) {
        this.issueBookmarkPath = issueBookmarkPath;
    }

    /**
     * Gets the display date.
     *
     * @return the display date.
     */
    public String getDisplayDate() {
        return displayDate;
    }

    /**
     * Gets the magazine name.
     *
     * @return the magazineName
     */
    public String getMagazineName() {
        return magazineName;
    }

    /**
     * Gets the featured resources.
     * 
     * @return the featuredResources
     */
    public List< ResourcesObject > getFeaturedResources() {
        return featuredResources;
    }

    /**
     * Sets the featured resources.
     * 
     * @param featuredResources
     *            the featuredResources to set
     */
    public void setFeaturedResources(List< ResourcesObject > featuredResources ) {
        this.featuredResources = featuredResources;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() throws JSONException {

        SlingHttpServletRequest request = getRequest();
        Resource resource = request.getResource();
        List< ValueMap > assetDataMapList;
        assetDataList = new ArrayList< >();
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        userType = ( String ) RoleUtil.getUserRole( getRequest() );
        Page currentPage = getCurrentPage();
        issueHighlight = request.adaptTo( IssueHighlight.class );
        if ( null != slingScriptHelper ) {
            String currentPath = currentPage.getPath();
            setIssueBookmarkPath( currentPath );
            try {
                IssueHighlightDataService issueHighlightDataService = slingScriptHelper
                        .getService( IssueHighlightDataService.class );
                MagazineProps magazineProps = slingScriptHelper.getService( MagazineProps.class );
                if ( null != issueHighlightDataService ) {
                    sortingDate = issueHighlightDataService.fetchIssueSortingDate( currentPath );
                    displayDate = issueHighlightDataService.fetchIssueDisplayDate( currentPath );

                    setFeaturedResources( issueHighlightDataService.fetchFeaturedResources( request, resource.getPath() ) );
                    assetDataMapList = issueHighlightDataService.fetchAssetData( resource.getPath() );
                    addItemsToAssetDataList( assetDataMapList );
                }
                if ( null != magazineProps ) {
                    magazineName = magazineProps.getMagazineName( currentPath );
                }
            } catch ( ClassmagsMigrationBaseException e ) {
                LOG.error( e.getMessage(), e );
            }
        }
    }

    /**
     * Adds the items to asset data list.
     *
     * @param assetDataMapList
     *            the asset data map list
     */
    private void addItemsToAssetDataList( List< ValueMap > assetDataMapList ) {
        for ( ValueMap assetDataMap : assetDataMapList ) {
            KeyAssets keyAssets = new KeyAssets();
            keyAssets.setIssueKeyAssetsLabel( assetDataMap.get( ISSUE_KEY_ASSETS_LABEL, StringUtils.EMPTY ) );
            keyAssets.setIssueKeyAssetsLabelPath( InternalURLFormatter.formatURL( getResourceResolver(),
                    assetDataMap.get( ISSUE_KEY_ASSETS_PATH, StringUtils.EMPTY ) ) );
            keyAssets.setUserType( assetDataMap.get( ISSUE_KEY_USER_TYPE, StringUtils.EMPTY ) );
            keyAssets.setDescrition( assetDataMap.get( ISSUE_KEY_ASSETS_DESCRIPTION, StringUtils.EMPTY ) );
            keyAssets.setIssueKeyAssetsIsShareable(
                    CommonUtils.isAShareableAsset( assetDataMap.get( ISSUE_KEY_ASSETS_PATH, StringUtils.EMPTY ) ) );
            keyAssets.setLabelColor( assetDataMap.get( ISSUE_KEY_LABEL_COLOR, StringUtils.EMPTY ) );
            keyAssets.setIssueKeyAssetsBookmarkPath( assetDataMap.get( ISSUE_KEY_ASSETS_PATH, StringUtils.EMPTY ) );
            assetDataList.add( keyAssets );
        }
    }
}
