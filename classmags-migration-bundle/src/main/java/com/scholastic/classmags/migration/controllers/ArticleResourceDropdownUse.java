package com.scholastic.classmags.migration.controllers;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.iterators.TransformIterator;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ResourceMetadata;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.api.wrappers.ValueMapDecorator;

import com.adobe.cq.sightly.WCMUsePojo;
import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.adobe.granite.ui.components.ds.ValueMapResource;
import com.day.cq.wcm.api.Page;
import com.day.crx.JcrConstants;
import com.scholastic.classmags.migration.services.ArticleResourceDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.DropdownUtils;

/**
 * Use Class for ArticleResourceDropdown.
 */
public class ArticleResourceDropdownUse extends WCMUsePojo {

    /** The resolver. */
    private ResourceResolver resolver;

    /** The resources map. */
    private Map< String, String > resourcesMap = new HashMap< >();

    /**
     * Gets the dropdown.
     *
     * @return the dropdown
     */
    @SuppressWarnings( "unchecked" )
    public void getDropdown() {

        DataSource datasource = new SimpleDataSource(
                new TransformIterator( resourcesMap.keySet().iterator(), new Transformer() {
                    @Override
                    public Object transform( Object input ) {
                        String title = ( String ) input;
                        ValueMap vm = new ValueMapDecorator( new HashMap< String, Object >() );
                        vm.put( ClassMagsMigrationConstants.DATASOURCE_VALUE, title );
                        vm.put( ClassMagsMigrationConstants.DATASOURCE_TEXT, resourcesMap.get( title ) );
                        return new ValueMapResource( resolver, new ResourceMetadata(), JcrConstants.NT_UNSTRUCTURED,
                                vm );
                    }
                } ) );
        getRequest().setAttribute( DataSource.class.getName(), datasource );
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() {

        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        SlingHttpServletRequest slingRequest = getRequest();
        resolver = getResourceResolver();
        Page currentPage = DropdownUtils.getContentPage( slingRequest );
        if ( null != slingScriptHelper && null != currentPage ) {
            ArticleResourceDataService articleResourceDataService = slingScriptHelper
                    .getService( ArticleResourceDataService.class );
            if ( null != articleResourceDataService ) {
                resourcesMap = articleResourceDataService.fetchArticleResourceData( slingRequest, currentPage );
                getDropdown();
            }
        }
    }
}
