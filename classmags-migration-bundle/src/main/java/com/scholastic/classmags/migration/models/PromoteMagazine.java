package com.scholastic.classmags.migration.models;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * The Class PromoteMagazine.
 */
@Model( adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL )
public class PromoteMagazine extends ClassmagsMigrationBaseModel {

    /** The promote mags path. */
    @Inject
    @Via( "resource" )
    private String promoteMagsPath;

    /**
     * Instantiates a new promote magazine.
     *
     * @param request
     *            the request
     */
    public PromoteMagazine( SlingHttpServletRequest request ) {
        super( request );
    }

    /**
     * Gets the promote mags path.
     *
     * @return the promoteMagsPath
     */
    public String getPromoteMagsPath() {
        return InternalURLFormatter.formatURL( getRequest().getResourceResolver(), promoteMagsPath );
    }

}
