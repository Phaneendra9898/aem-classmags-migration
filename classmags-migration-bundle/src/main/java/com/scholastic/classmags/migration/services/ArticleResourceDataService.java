package com.scholastic.classmags.migration.services;

import java.util.Map;

import org.apache.sling.api.SlingHttpServletRequest;

import com.day.cq.wcm.api.Page;

/**
 * The Interface ArticleResourceDataService.
 */
public interface ArticleResourceDataService {

    /**
     * Fetch article resource data.
     *
     * @param slingRequest
     *            the sling request
     * @param currentPage
     *            the current page
     * @return the map
     */
    public Map< String, String > fetchArticleResourceData( SlingHttpServletRequest slingRequest, Page currentPage );
}
