package com.scholastic.classmags.migration.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.impl.CloudSolrClient;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.utils.SolrSearchClassMagsConstants;

@Component( label = "AEM Solr Class Magazines Search - Solr Configuration Service", description = "A service for configuring Class Magazines Solr", immediate = true, metatype = true )
@Service( SolrClassMagsConfigService.class )
@Properties( { @Property( name = Constants.SERVICE_VENDOR, value = "Solr" ),
        @Property( name = SolrSearchClassMagsConstants.ZOOKEEPER, label = "Zookeeper host List", value = {
                "" },unbounded=PropertyUnbounded.ARRAY ),
        @Property( name = Constants.SERVICE_DESCRIPTION, value = "Solr configuration service" ),
        @Property( name = SolrSearchClassMagsConstants.SERVER_PORT, value = SolrSearchClassMagsConstants.DEFAULT_SERVER_PORT, label = "Server Port", description = "Server port" ),
        @Property( name = SolrSearchClassMagsConstants.CONNECTION_TIME_OUT, label = "Zookeeper connection timeout", value = "60000" ),
        @Property( name = SolrSearchClassMagsConstants.PROXY_ENABLED, boolValue = SolrSearchClassMagsConstants.DEFAULT_PROXY_ENABLED, label = "Enable Proxy", description = "Enable Proxy. Must be either 'true' or 'false'" ),
        @Property( name = SolrSearchClassMagsConstants.PROXY_URL, value = SolrSearchClassMagsConstants.DEFAULT_PROXY_URL, label = "Proxy URL", description = "Absolute proxy URL" ),
        @Property( name = SolrSearchClassMagsConstants.AEM_SOLR_CORE, value = SolrSearchClassMagsConstants.DEFAULT_AEM_SOLR_CORE, label = "AEM Solr Core", description = "Solr Core" ) })
/**
 * Solr ConfigurationService provides a services for setting and getting Solr
 * configuration information.
 */
public class SolrClassMagsConfigService {

    private static final Logger LOG = LoggerFactory.getLogger( SolrClassMagsConfigService.class );
    private String serverPort;
    private String proxyUrl;
    private boolean proxyEnabled;

    private String[] zookeeperArray;
    private int connectionTimeOut;

    private String solrCoreConfigured;
    /** the map **/
    private static final Map< String, SolrClient > solrServerByCore = new HashMap< String, SolrClient >();

    @Activate
    protected void activate( final Map< String, Object > config ) {
        readConfig( config );
        resetService( config );
    }

    @Modified
    protected void modified( final Map< String, Object > config ) {
        readConfig( config );
        resetService( config );
    }

    /**
     * Reset the solr service using configuration provided
     * 
     * @param config
     */
    private void readConfig( final Map< String, Object > config ) {
        LOG.info( "Resetting Solr configuration service using configuration: " + config );

        serverPort = config.containsKey( SolrSearchClassMagsConstants.SERVER_PORT )
                ? ( String ) config.get( SolrSearchClassMagsConstants.SERVER_PORT )
                : SolrSearchClassMagsConstants.DEFAULT_SERVER_PORT;

        String connectionTimeString = config.containsKey( SolrSearchClassMagsConstants.CONNECTION_TIME_OUT )
                ? ( String ) config.get( SolrSearchClassMagsConstants.CONNECTION_TIME_OUT )
                : SolrSearchClassMagsConstants.DEFAULT_CONNECTION_TIME_OUT;
        connectionTimeOut = Integer.parseInt( connectionTimeString );

        proxyUrl = config.containsKey( SolrSearchClassMagsConstants.PROXY_URL )
                ? ( String ) config.get( SolrSearchClassMagsConstants.PROXY_URL )
                : SolrSearchClassMagsConstants.DEFAULT_PROXY_URL;

        proxyEnabled = config.containsKey( SolrSearchClassMagsConstants.PROXY_ENABLED )
                ? ( Boolean ) config.get( SolrSearchClassMagsConstants.PROXY_ENABLED )
                : SolrSearchClassMagsConstants.DEFAULT_PROXY_ENABLED;

        solrCoreConfigured = config.containsKey( SolrSearchClassMagsConstants.AEM_SOLR_CORE )
                ? ( String ) config.get( SolrSearchClassMagsConstants.AEM_SOLR_CORE )
                : SolrSearchClassMagsConstants.DEFAULT_AEM_SOLR_CORE;
        zookeeperArray = PropertiesUtil.toStringArray( config.containsKey( SolrSearchClassMagsConstants.ZOOKEEPER )
                ? config.get( SolrSearchClassMagsConstants.ZOOKEEPER )
                : ArrayUtils.EMPTY_STRING_ARRAY );
    }

    /**
     * 
     * @return
     */
    public String getCoreName() {
        return solrCoreConfigured;
    }

    /**
     * Retrieve a particular instance of SolrServer identified by the core name.
     */
    public SolrClient getSolrClient() {
        return getSolrClient( getCoreName() );
    }

    /**
     * Retrieve a particular instance of SolrServer identified by the core name.
     * 
     * @param solrCore
     * @return
     */
    public SolrClient getSolrClient( String solrCore ) {

        final SolrClient existingSolrServer = solrServerByCore.get( solrCore );
        if ( null != existingSolrServer ) {
            LOG.info( "Returning existing instance of Solr Server: {}", existingSolrServer.toString() );
            return existingSolrServer;
        } else {
            synchronized ( solrServerByCore ) {
                // Double check existence while in synchronized block.
                if ( solrServerByCore.containsKey( solrCore ) ) {
                    return solrServerByCore.get( solrCore );
                } else {
                    String zkHostString = StringUtils.EMPTY;
                    for ( String zookeeper : zookeeperArray ) {
                        zkHostString = zkHostString + zookeeper + SolrSearchClassMagsConstants.CONSTANT_COLON
                                + serverPort + SolrSearchClassMagsConstants.CONSTANT_COMMA;
                    }
                    zkHostString = StringUtils.removeEnd( zkHostString, SolrSearchClassMagsConstants.CONSTANT_COMMA );
                    LOG.info( "Zookeeper String " + zkHostString );

                    CloudSolrClient solrClient = new CloudSolrClient( zkHostString );
                    solrClient.setDefaultCollection( solrCore );
                    solrClient.setZkClientTimeout( connectionTimeOut );
                    solrClient.setZkConnectTimeout( connectionTimeOut );
                    LOG.info( "newSolrServer : {}", solrClient.toString() );
                    solrServerByCore.put( solrCore, solrClient );
                    return solrClient;
                }
            }
        }
    }

    /**
     * 
     * @param config
     */
    private void resetService( Map< String, Object > config ) {
        LOG.info( "Resetting Solr index service using configuration: " + config );
        resetSolrServerClients();
    }

    /**
     * Retrieve a particular instance of SolrServer identified by the core name.
     */
    public void resetSolrServerClients() {
        synchronized ( solrServerByCore ) {
            for ( SolrClient server : solrServerByCore.values() ) {
                try {
                    LOG.info( "Config service shutting down Server {}",server.toString() );
                    server.close();
                } catch ( Exception e ) {
                    LOG.warn( "Exception while shutting down Solr Server instance.", e );
                }
            }
            solrServerByCore.clear();
        }
    }

    /**
     * 
     * @return
     */
    public String getProxyUrl() {
        return proxyUrl;
    }

    /**
     * 
     * @return
     */
    public boolean isProxyEnabled() {
        return proxyEnabled;
    }
}