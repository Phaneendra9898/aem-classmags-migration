package com.scholastic.classmags.migration.filters;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingFilter;
import org.apache.felix.scr.annotations.sling.SlingFilterScope;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.commons.util.DamUtil;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.RoleUtil;

@SlingFilter( label = "Class Magazines Asset Authorization Filter", 
    description = "Request filter that determines wether the user has the permissions to view the requested Asset.", 
    generateComponent = true, generateService = true, order = 0, scope = SlingFilterScope.REQUEST, metatype=true )
@Properties(
        @Property( name = ClassMagsMigrationConstants.SERVICE_ENABLED, 
        boolValue = false, 
        label = "Enabled", 
        description = "Enable the asset authorization filter." ))
public class AuthRequestFilter implements Filter {
    private static final Logger LOG = LoggerFactory.getLogger( AuthRequestFilter.class );
    private static final String CONTENT_TILE_IMAGE_PATH = "/content-tile/image.png";
    private static final String GAME_IMAGE_PATH = "/game/image.png";
    private boolean serviceEnabled = false;
    
    @Reference
    private ResourceResolverFactory resourceResolverFactory; 
    
    @Activate
    protected void activate( final Map< String, Object > config ) {
        serviceEnabled = config.containsKey( ClassMagsMigrationConstants.SERVICE_ENABLED )
                ? ( Boolean ) config.get( ClassMagsMigrationConstants.SERVICE_ENABLED ) : false;
        LOG.debug( "Asset Filter is  {}", serviceEnabled );        
    }

    @Override
    public void init( FilterConfig filterConfig ) throws ServletException {

    }

    @Override
    public final void doFilter( ServletRequest request, ServletResponse response, FilterChain chain )
            throws IOException, ServletException {
        if(!serviceEnabled){
            chain.doFilter( request, response );
        }else{
            final SlingHttpServletResponse slingResponse = ( SlingHttpServletResponse ) response;
            final SlingHttpServletRequest slingRequest = ( SlingHttpServletRequest ) request;
            String suffix = slingRequest.getRequestPathInfo().getSuffix();
            boolean isContentTileImage = false;
            if(StringUtils.isNotBlank( suffix )){
                isContentTileImage = CONTENT_TILE_IMAGE_PATH.equalsIgnoreCase( suffix )
                        || GAME_IMAGE_PATH.equalsIgnoreCase( suffix );
            }
            final Resource resource = slingRequest.getResource();
            boolean isAsset = false;
            if ( resource == null ) {
                chain.doFilter( request, response );
            } else {
                isAsset = DamUtil.isAsset( resource );
                LOG.debug( "Resource is Asset {}", isAsset );
                if ( !isAsset ) {
                    chain.doFilter( request, response );
                } else {
                    if( isContentTileImage ){
                        chain.doFilter( request, response );
                    }else{
                        ResourceResolver serviceResolver = getServiceResourceResolver( resourceResolverFactory );
                        String userRole = RoleUtil.getUserRole( slingRequest );
                        String accessibleTo = StringUtils.EMPTY;
                        if ( serviceResolver != null ) {
                            Resource metadata = serviceResolver.getResource(
                                    resource.getPath() + "/" + ClassMagsMigrationConstants.ASSET_METADATA_NODE );
                            if ( metadata != null ) {
                                ValueMap asset = metadata.adaptTo( ValueMap.class );
                                LOG.info( "asset.getMetadata() {}", asset.toString() );
                                accessibleTo = asset.get( "userType", RoleUtil.ROLE_TYPE_EVERYONE ).toString();
                                if ( RoleUtil.shouldRender( userRole, accessibleTo ) ) {
                                    chain.doFilter( request, response );
                                } else {
                                    LOG.debug( "Resource '{}' is only accesible to '{}' not '{}'", resource.getPath(),
                                            accessibleTo, userRole );
                                    slingResponse.sendError( HttpServletResponse.SC_NOT_FOUND );
                                    return;
                                }
                            }
                            if ( serviceResolver.isLive() ) {
                                serviceResolver.close();
                            }
    
                        }
                    }
                }
            }
        }
    }

    @Override
    public void destroy() {

    }
    
    /**
     * This method will get service resolver for the accessing the asset meta-data
     * 
     * @param ResourceResolverFacatory
     * @return service ResourceResolver
     */
    private ResourceResolver getServiceResourceResolver(
            ResourceResolverFactory resourceResolverFactory) {
        ResourceResolver serviceResolver = null;
        final Map<String, Object> authInfo = Collections.singletonMap(
                ResourceResolverFactory.SUBSERVICE,
                (Object) ClassMagsMigrationConstants.READ_SERVICE);
        try {
            serviceResolver = resourceResolverFactory
                    .getServiceResourceResolver(authInfo);
        } catch (LoginException e) {
            LOG.error("Error getting service resource resolver : {}", e);
        }
        return serviceResolver;
    }
}