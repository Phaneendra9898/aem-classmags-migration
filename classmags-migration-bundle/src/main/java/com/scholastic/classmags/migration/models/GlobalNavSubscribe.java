package com.scholastic.classmags.migration.models;

import javax.inject.Inject;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Via;

import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * Sling Model for GlobalNavSubscribe.
 */
@Model( adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL )
public class GlobalNavSubscribe extends ClassmagsMigrationBaseModel {

    /** The global nav subscribe path. */
    @Inject
    @Via( "resource" )
    private String globalNavSubscribePath;

    /**
     * Instantiates a new global nav subscribe.
     *
     * @param request
     *            the request
     */
    public GlobalNavSubscribe( SlingHttpServletRequest request ) {
        super( request );
    }

    /**
     * Gets the global nav subscribe path.
     *
     * @return the globalNavSubscribePath
     */
    public String getGlobalNavSubscribePath() {
        return InternalURLFormatter.formatURL( getRequest().getResourceResolver(), globalNavSubscribePath );
    }
}
