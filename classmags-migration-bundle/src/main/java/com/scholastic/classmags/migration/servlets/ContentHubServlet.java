package com.scholastic.classmags.migration.servlets;

import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.PREFILTER;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.ROLE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.ROWS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SITE_ID;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SORT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.START;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SUBJECTS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SUBJECTS_OPERATOR;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.TYPES;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.FLD_TYPE_CONTENTHUB;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SUBJECTS_FILTERS;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.models.SearchResponse;
import com.scholastic.classmags.migration.services.ClassMagsQueryService;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * Servlet for Search
 * 
 *
 */
@SlingServlet( paths = "/bin/classmags/migration/contenthub", methods = "GET" )
public class ContentHubServlet extends SlingSafeMethodsServlet {

    private static final long serialVersionUID = -4359205700108886710L;

    private static final Logger LOG = LoggerFactory.getLogger( ContentHubServlet.class );

    private static final String PAGE_PATH = "path";

    @Reference
    private ClassMagsQueryService queryService;

    @Reference
    private MagazineProps magazineProps;

    protected boolean includeFacet = true;

    /**
     * Method doGet
     */
    @Override
    public void doGet( SlingHttpServletRequest request, SlingHttpServletResponse response )
            throws ServletException, IOException {
        Map< String, String > params = new HashMap< String, String >();
        String pagePath = request.getParameter( PAGE_PATH );
        String role = RoleUtil.getUserRole( request );
        String siteId = magazineProps.getMagazineName( pagePath );
        LOG.info( "Site Id : {}", siteId );

        params.put( START, request.getParameter( START ) );
        params.put( ROWS, request.getParameter( ROWS ) );
        params.put( SORT, request.getParameter( SORT ) );
        params.put( TYPES, request.getParameter( TYPES ) );
        params.put( SUBJECTS, request.getParameter( SUBJECTS ) );
        params.put( SUBJECTS_OPERATOR, request.getParameter( SUBJECTS_OPERATOR ) );
        params.put( ROLE, role );
        params.put( SITE_ID, siteId );
        params.put( PREFILTER, FLD_TYPE_CONTENTHUB );
        params.put( SUBJECTS_FILTERS, request.getParameter( SUBJECTS_FILTERS ) );
        SearchResponse results = null;
        try {
            results = queryService.getSearchResults( params, true, "" );

        } catch ( Exception e ) {
            LOG.error( "Error Search Services", e );
        } finally {
            // render data in JSON format
            response.setContentType( "application/json" );
            response.setCharacterEncoding( "UTF-8" );
            // respond back
            if ( results != null ) {
                response.getWriter().write( queryService.getResultsAsJSONString( results ) );
            } else {
                response.getWriter().write( "{}" );
            }

        }

    }

}