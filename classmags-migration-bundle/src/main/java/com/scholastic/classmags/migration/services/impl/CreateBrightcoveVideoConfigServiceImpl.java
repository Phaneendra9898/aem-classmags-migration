package com.scholastic.classmags.migration.services.impl;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.services.CreateBrightcoveVideoConfigService;

/**
 * The Class CreateBrightcoveVideoConfigServiceImpl.
 */
@Component( immediate = true, metatype = true )
@Service( CreateBrightcoveVideoConfigService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Brightcove player configuration service" ) } )
public class CreateBrightcoveVideoConfigServiceImpl implements CreateBrightcoveVideoConfigService {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( CreateBrightcoveVideoConfigServiceImpl.class );

    /** The Constant BRIGHTCOVE_URL_DEFAULT. */
    public static final String BRIGHTCOVE_URL_DEFAULT = "http://api.brightcove.com/services/library";

    /** The Constant SEARCH_VIDEOS_DEFAULT. */
    public static final String SEARCH_VIDEOS_DEFAULT = "search_videos";

    /** The Constant FIND_BY_ID_DEFAULT. */
    public static final String FIND_BY_ID_DEFAULT = "find_video_by_id";

    /** The Constant FIELDS_DEFAULT. */
    public static final String FIELDS_DEFAULT = "id,name,shortDescription,longDescription,creationDate,publishedDate,startDate,tags,videoStillURL,thumbnailURL,FLVURL,renditions,captioning";

    /** The Constant MEDIA_DELIVERY_DEFAULT. */
    public static final String MEDIA_DELIVERY_DEFAULT = "http";

    /** The Constant SORT_START_DATE_DEFAULT. */
    public static final String SORT_START_DATE_DEFAULT = "START_DATE:DESC";

    /** The Constant SORT_NAME_DEFAULT. */
    public static final String SORT_NAME_DEFAULT = "DISPLAY_NAME:ASC";

    /** The Constant GET_ITEM_COUNT_DEFAULT. */
    public static final String GET_ITEM_COUNT_DEFAULT = "true";

    /** The Constant CALLBACK_DEFAULT. */
    public static final String CALLBACK_DEFAULT = "BCL.onSearchResponse";

    /** The Constant UPFRONT_ANY_FIELDS_DEFAULT. */
    public static final String UPFRONT_ANY_FIELDS_DEFAULT = "reference_id:upfront_";

    /** The Constant READ_TOKEN_DEFAULT. */
    public static final String READ_TOKEN_DEFAULT = "OeeaxEzV-gG5MwbjniJa4ywAyIhKO3i4oZaTjBk_C04.";

    /** The Constant BRIGHTCOVE_URL. */
    @Property( label = "Brightcove read url ", description = "Default: " + BRIGHTCOVE_URL_DEFAULT )
    public static final String BRIGHTCOVE_URL = "readUrl";

    /** The Constant SEARCH_VIDEOS. */
    @Property( label = "Brightcove search videos ", description = "Default: " + SEARCH_VIDEOS_DEFAULT )
    public static final String SEARCH_VIDEOS = "searchVideos";

    /** The Constant FIND_BY_ID. */
    @Property( label = "Brightcove find by id ", description = "Default: " + FIND_BY_ID_DEFAULT )
    public static final String FIND_BY_ID = "findById";

    /** The Constant FIELDS. */
    @Property( label = "Brightcove fields ", description = "Default: " + FIELDS_DEFAULT )
    public static final String FIELDS = "fields";

    /** The Constant MEDIA_DELIVERY. */
    @Property( label = "Brightcove media delivery ", description = "Default: " + MEDIA_DELIVERY_DEFAULT )
    public static final String MEDIA_DELIVERY = "mediaDelivery";

    /** The Constant SORT_START_DATE. */
    @Property( label = "Brightcove sort start date ", description = "Default: " + SORT_START_DATE_DEFAULT )
    public static final String SORT_START_DATE = "sortStartDate";

    /** The Constant SORT_NAME. */
    @Property( label = "Brightcove sort name ", description = "Default: " + SORT_NAME_DEFAULT )
    public static final String SORT_NAME = "sortName";

    /** The Constant GET_ITEM_COUNT. */
    @Property( label = "Brightcove get item count ", description = "Default: " + GET_ITEM_COUNT_DEFAULT )
    public static final String GET_ITEM_COUNT = "getItemCount";

    /** The Constant CALLBACK. */
    @Property( label = "Brightcove callback ", description = "Default: " + CALLBACK_DEFAULT )
    public static final String CALLBACK = "callback";

    /** The Constant UPFRONT_ANY_FIELDS. */
    @Property( label = "Brightcove upfront any fields ", description = "Default: " + UPFRONT_ANY_FIELDS_DEFAULT )
    public static final String UPFRONT_ANY_FIELDS = "upfrontAnyFields";

    /** The Constant READ_TOKEN. */
    @Property( label = "Brightcove read token ", description = "Default: " + READ_TOKEN_DEFAULT )
    public static final String READ_TOKEN = "readToken";

    /** The read url. */
    private String readUrl = BRIGHTCOVE_URL_DEFAULT;

    /** The search videos. */
    private String searchVideos = SEARCH_VIDEOS_DEFAULT;

    /** The find by id. */
    private String findById = FIND_BY_ID_DEFAULT;

    /** The fields. */
    private String fields = FIELDS_DEFAULT;

    /** The media delivery. */
    private String mediaDelivery = MEDIA_DELIVERY_DEFAULT;

    /** The sort start date. */
    private String sortStartDate = SORT_START_DATE_DEFAULT;

    /** The sort name. */
    private String sortName = SORT_NAME_DEFAULT;

    /** The get item count. */
    private String getItemCount = GET_ITEM_COUNT_DEFAULT;

    /** The callback. */
    private String callback = CALLBACK_DEFAULT;

    /** The upfront any fields. */
    private String upfrontAnyFields = UPFRONT_ANY_FIELDS_DEFAULT;

    /** The read token. */
    private String readToken = READ_TOKEN_DEFAULT;

    /**
     * Activate.
     * 
     * @param properties
     *            the properties
     */
    @Activate
    protected void activate( final Map< String, Object > properties ) {

        readUrl = StringUtils.defaultIfBlank( ( String ) properties.get( BRIGHTCOVE_URL ), BRIGHTCOVE_URL_DEFAULT );
        searchVideos = StringUtils.defaultIfBlank( ( String ) properties.get( SEARCH_VIDEOS ), SEARCH_VIDEOS_DEFAULT );
        findById = StringUtils.defaultIfBlank( ( String ) properties.get( FIND_BY_ID ), FIND_BY_ID_DEFAULT );
        fields = StringUtils.defaultIfBlank( ( String ) properties.get( FIELDS ), FIELDS_DEFAULT );
        mediaDelivery = StringUtils
                .defaultIfBlank( ( String ) properties.get( MEDIA_DELIVERY ), MEDIA_DELIVERY_DEFAULT );
        sortStartDate = StringUtils.defaultIfBlank( ( String ) properties.get( SORT_START_DATE ),
                SORT_START_DATE_DEFAULT );
        sortName = StringUtils.defaultIfBlank( ( String ) properties.get( SORT_NAME ), SORT_NAME_DEFAULT );
        getItemCount = StringUtils.defaultIfBlank( ( String ) properties.get( GET_ITEM_COUNT ), GET_ITEM_COUNT_DEFAULT );
        callback = StringUtils.defaultIfBlank( ( String ) properties.get( CALLBACK ), CALLBACK_DEFAULT );
        upfrontAnyFields = StringUtils.defaultIfBlank( ( String ) properties.get( UPFRONT_ANY_FIELDS ),
                UPFRONT_ANY_FIELDS_DEFAULT );
        readToken = StringUtils.defaultIfBlank( ( String ) properties.get( READ_TOKEN ), READ_TOKEN_DEFAULT );

        LOG.info( "Configure service using readUrl: " + readUrl + ", searchVideos: " + searchVideos + ", findById: "
                + findById + ", fields: " + fields + ", mediaDelivery: " + mediaDelivery + ", sortStartDate: "
                + sortStartDate + ", sortName: " + sortName + ", getItemCount: " + getItemCount + ", callback: "
                + callback + ", upfrontAnyFields: " + upfrontAnyFields + ", readToken: " + readToken );
    }

    /**
     * Gets the read url.
     * 
     * @return the readUrl
     */
    @Override
    public String getReadUrl() {
        return readUrl;
    }

    /**
     * Gets the search videos.
     * 
     * @return the searchVideos
     */
    @Override
    public String getSearchVideos() {
        return searchVideos;
    }

    /**
     * Gets the find by id.
     * 
     * @return the findById
     */
    @Override
    public String getFindById() {
        return findById;
    }

    /**
     * Gets the fields.
     * 
     * @return the fields
     */
    @Override
    public String getFields() {
        return fields;
    }

    /**
     * Gets the media delivery.
     * 
     * @return the mediaDelivery
     */
    @Override
    public String getMediaDelivery() {
        return mediaDelivery;
    }

    /**
     * Gets the sort start date.
     * 
     * @return the sortStartDate
     */
    @Override
    public String getSortStartDate() {
        return sortStartDate;
    }

    /**
     * Gets the sort name.
     * 
     * @return the sortName
     */
    @Override
    public String getSortName() {
        return sortName;
    }

    /**
     * Gets the gets the item count.
     * 
     * @return the getItemCount
     */
    @Override
    public String getGetItemCount() {
        return getItemCount;
    }

    /**
     * Gets the callback.
     * 
     * @return the callback
     */
    @Override
    public String getCallback() {
        return callback;
    }

    /**
     * Gets the upfront any fields.
     * 
     * @return the upfrontAnyFields
     */
    @Override
    public String getUpfrontAnyFields() {
        return upfrontAnyFields;
    }

    /**
     * Gets the read token.
     * 
     * @return the readToken
     */
    @Override
    public String getReadToken() {
        return readToken;
    }

}
