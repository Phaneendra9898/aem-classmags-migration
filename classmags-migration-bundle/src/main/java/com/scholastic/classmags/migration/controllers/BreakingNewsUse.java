package com.scholastic.classmags.migration.controllers;

import java.util.Date;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUsePojo;

public class BreakingNewsUse extends WCMUsePojo {

    private static final String LAST_MODIFIED = "jcr:lastModified";

    private Long lastModified;

    @Override
    public void activate() {
        Resource resource = getResource();
        if ( null != resource ) {
            ValueMap properties = resource.getValueMap();
            if ( null != properties ) {
                Date lastModifiedDate = properties.containsKey( LAST_MODIFIED )
                        ? properties.get( LAST_MODIFIED, Date.class ) : null;
                lastModified = ( null != lastModifiedDate ) ? lastModifiedDate.getTime() : null;

            }
        }
    }
    
    /**
     * Gets the last modified date.
     * 
     * @return lastModified The last modified date.
     */
    public Long getLastModified() {
        return lastModified;
    }
}
