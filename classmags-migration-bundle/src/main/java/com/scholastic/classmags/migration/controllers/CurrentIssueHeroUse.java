package com.scholastic.classmags.migration.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.commons.jcr.JcrConstants;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.CurrentIssueHero;
import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.services.ContentTileFlagDataService;
import com.scholastic.classmags.migration.services.CurrentIssueHeroDataService;
import com.scholastic.classmags.migration.services.TeachingResourcesService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * Use Class for CurrentIssueHero.
 */
public class CurrentIssueHeroUse extends WCMUsePojo {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( CurrentIssueHeroUse.class );

    /** The sorting date. */
    private String issueDate;

    /** The issue description. */
    private String issueDescription;

    /** The issue image. */
    private String issueImage;

    /** The current issue hero. */
    private CurrentIssueHero currentIssueHero;
    
    /** The ereader config path. */
    private String eReaderConfigPath;
    
    /** The ereader access. */
    private String eReaderAccess;

    /** The featured resources. */
    private Map< String, List< ResourcesObject > > featuredResources = new HashMap< >();

    /**
     * Gets the current issue hero.
     * 
     * @return the currentIssueHero
     */
    public CurrentIssueHero getCurrentIssueHero() {
        return currentIssueHero;
    }

    /**
     * Sets the current issue hero.
     * 
     * @param currentIssueHero
     *            the currentIssueHero to set
     */
    public void setCurrentIssueHero( CurrentIssueHero currentIssueHero ) {
        this.currentIssueHero = currentIssueHero;
    }

    /**
     * Gets the featured resources.
     * 
     * @return the featuredResources
     */
    public Map< String, List< ResourcesObject > > getFeaturedResources() {
        return featuredResources;
    }

    /**
     * Sets the featured resources.
     * 
     * @param featuredResources
     *            the featuredResources to set
     */
    public void setFeaturedResources( Map< String, List< ResourcesObject > > featuredResources ) {
        this.featuredResources = featuredResources;
    }

    /**
     * Gets the issue date.
     * 
     * @return the issueDate
     */
    public String getIssueDate() {
        return issueDate;
    }

    /**
     * Gets the issue description.
     * 
     * @return the issueDescription
     */
    public String getIssueDescription() {
        return issueDescription;
    }

    /**
     * Gets the issue image.
     * 
     * @return the issueImage
     */
    public String getIssueImage() {
        return issueImage;
    }
    
    /**
     * Gets the ereader config path.
     * 
     * @return the eReaderConfigPath
     */
    public String getEReaderConfigPath() {
        return eReaderConfigPath;
    }
    
    /**
     * Gets the ereader access.
     * 
     * @return the eReaderAccess
     */
    public String getEReaderAccess() {
        return eReaderAccess;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() {

        ValueMap properties;
        String inputKey;
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        SlingHttpServletRequest request = getRequest();
        currentIssueHero = request.adaptTo( CurrentIssueHero.class );
        String issuePath = getIssuePath();
        Resource resource = request.getResource();
        String currentPagePath = resource.getPath();
        if ( null != slingScriptHelper && StringUtils.isNotBlank( issuePath ) ) {
            CurrentIssueHeroDataService currentIssueHeroDataService = slingScriptHelper
                    .getService( CurrentIssueHeroDataService.class );
            try {
                if ( null != currentIssueHeroDataService ) {
                    properties = currentIssueHeroDataService.fetchIssueData( issuePath );
                    inputKey = ClassMagsMigrationConstants.PP_DESCRIPTION;
                    issueDescription = fetchValue( inputKey, properties );
                    inputKey = ClassMagsMigrationConstants.FILE_REFERENCE;
                    issueImage = fetchValue( inputKey, properties );
                    issueDate = currentIssueHeroDataService.fetchIssueDate( issuePath, currentPagePath );
                }

                Resource currentIssue = request.getResourceResolver().getResource( issuePath );

                Resource jcrContent = currentIssue != null ? currentIssue.getChild( JcrConstants.JCR_CONTENT ) : null;
                Resource issueConfig = ( jcrContent != null
                        && jcrContent.getChild( ClassMagsMigrationConstants.ISSUE_CONFIGURATION ) != null )
                                ? jcrContent.getChild( ClassMagsMigrationConstants.ISSUE_CONFIGURATION ) : null;
                if ( issueConfig != null ) {
                    TeachingResourcesService teachingResourcesService = slingScriptHelper
                            .getService( TeachingResourcesService.class );
                    setFeaturedResources( teachingResourcesService != null
                            ? teachingResourcesService.getTeachingResources( getRequest(), issueConfig,
                                    ClassMagsMigrationConstants.FEATURED_RESOURCES )
                            : null );
                    setContentTileFlags( slingScriptHelper, issuePath );
                }

            } catch ( ClassmagsMigrationBaseException e ) {
                LOG.error( e.getMessage(), e );
            }

            Resource jcrContentResource = ( null != getCurrentPage() ) ? getCurrentPage().getContentResource() : null;
            Resource eReaderConfig = null;
            if ( null != jcrContentResource ) {
                eReaderConfig = jcrContentResource.getChild( "eReader" );
            }
            if ( null != eReaderConfig ) {
                ValueMap eReaderProperties = eReaderConfig.getValueMap();
                eReaderConfigPath = InternalURLFormatter.formatURL( getResourceResolver(),
                        eReaderProperties.get( "linkToEReaderNew", String.class ) );
                eReaderAccess = eReaderProperties.get( "userType", String.class );
            }
        }
    }

    /**
     * Sets the content tile flags.
     *
     * @param slingScriptHelper
     *            the sling script helper
     * @param issuePath
     *            the issue path
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private void setContentTileFlags( SlingScriptHelper slingScriptHelper, String issuePath )
            throws ClassmagsMigrationBaseException {

        ContentTileFlagDataService contentTileFlagDataService = slingScriptHelper
                .getService( ContentTileFlagDataService.class );
        if ( null != contentTileFlagDataService ) {
            contentTileFlagDataService.configureArticlePageAndFlagData( issuePath, getFeaturedResources() );
        }
    }

    /**
     * Gets the issue path.
     * 
     * @return the issue path
     */
    private String getIssuePath() {
        Resource resource = getResource();
        ValueMap currentIssueProps = resource != null ? resource.getValueMap() : null;
        return currentIssueProps != null ? currentIssueProps.get( "currentIssuePath", StringUtils.EMPTY )
                : StringUtils.EMPTY;
    }

    /**
     * Fetcht value.
     * 
     * @param inputKey
     *            the input key
     * @param properties
     *            the properties
     * @return the string
     */
    private String fetchValue( String inputKey, ValueMap properties ) {
        return properties.get( inputKey, StringUtils.EMPTY );
    }
}