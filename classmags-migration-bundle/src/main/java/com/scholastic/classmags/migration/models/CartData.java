package com.scholastic.classmags.migration.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CartData {

	@SerializedName("magazinePath")
	@Expose
	private String magazinePath;
	@SerializedName("magazineName")
	@Expose
	private String magazineName;
	@SerializedName("magazineTitle")
	@Expose
	private String magazineTitle;
	@SerializedName("quantity")
	@Expose
	private Integer quantity;
	@SerializedName("magazinePrice")
	@Expose
	private String magazinePrice;
	@SerializedName("shippingPercentage")
	@Expose
	private String shippingPercentage;
	@SerializedName("subTotal")
	@Expose
	private Integer subTotal;
	@SerializedName("shippingPrice")
	@Expose
	private Integer shippingPrice;

	public String getMagazinePath() {
		return magazinePath;
	}

	public void setMagazinePath(String magazinePath) {
		this.magazinePath = magazinePath;
	}

	public String getMagazineName() {
		return magazineName;
	}

	public void setMagazineName(String magazineName) {
		this.magazineName = magazineName;
	}

	public String getMagazineTitle() {
		return magazineTitle;
	}

	public void setMagazineTitle(String magazineTitle) {
		this.magazineTitle = magazineTitle;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getMagazinePrice() {
		return magazinePrice;
	}

	public void setMagazinePrice(String magazinePrice) {
		this.magazinePrice = magazinePrice;
	}

	public String getShippingPercentage() {
		return shippingPercentage;
	}

	public void setShippingPercentage(String shippingPercentage) {
		this.shippingPercentage = shippingPercentage;
	}

	public Integer getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Integer subTotal) {
		this.subTotal = subTotal;
	}

	public Integer getShippingPrice() {
		return shippingPrice;
	}

	public void setShippingPrice(Integer shippingPrice) {
		this.shippingPrice = shippingPrice;
	}

}
