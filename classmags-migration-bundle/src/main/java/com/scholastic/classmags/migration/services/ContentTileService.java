package com.scholastic.classmags.migration.services;

import com.scholastic.classmags.migration.models.ContentTile;
import com.scholastic.classmags.migration.models.ContentTileObject;

/**
 * The Interface ContentTileService.
 */
public interface ContentTileService {

    /**
     * Gets the article content.
     * 
     * @param contentTile The content tile.
     * @param currentPagePath The current page path.
     * 
     * @return the article content
     */
    ContentTileObject getArticleContent( ContentTile contentTile, String currentPagePath );

    /**
     * Gets the issue content.
     * 
     * @param contentTile The content tile.
     * @param currentPagePath The current page path.
     * 
     * @return the issue content
     */
    ContentTileObject getIssueContent( ContentTile contentTile, String currentPagePath );

    /**
     * Gets the video content.
     * 
     * @param contentTile The content tile.
     * @param currentPagePath The current page path.
     * 
     * @return the video content
     */
    ContentTileObject getVideoContent( ContentTile contentTile, String currentPagePath );

    /**
     * Gets the asset content.
     * 
     * @param contentTile The content tile.
     * @param currentPagePath The current page path.
     * 
     * @return the asset content
     */
    ContentTileObject getAssetContent( ContentTile contentTile, String currentPagePath );

    /**
     * Gets the custom content.
     * 
     * @param contentTile The content tile.
     * @param currentPagePath The current page path.
     * 
     * @return the custom content
     */
    ContentTileObject getCustomContent( ContentTile contentTile, String currentPagePath );
    
    /**
     * Gets the game content.
     * 
     * @param contentTile The content tile.
     * @param currentPagePath The current page path.
     * 
     * @return the game content
     */
    ContentTileObject getGameContent( ContentTile contentTile, String currentPath );

}
