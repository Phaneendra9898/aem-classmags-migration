package com.scholastic.classmags.migration.services.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.services.UpdateVotingNodeService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * Update voting node service.
 */
@Component( immediate = true, metatype = false )
@Service( UpdateVotingNodeService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Update voting node Service" ) })
public class UpdateVotingNodeServiceImpl implements UpdateVotingNodeService {

    /** The Constant PROPERTY_NAME_UUID **/
    private static final String PROPERTY_NAME_UUID = "uuid";

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( UpdateVotingNodeServiceImpl.class );

    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    @Override
    public void updateVotingNode( Resource resource ) throws PersistenceException {

        ModifiableValueMap map = resource.adaptTo( ModifiableValueMap.class );
        ValueMap properties = resource.getValueMap();
        String currentQuestion = properties.get( "question", String.class );
        try {
            if ( null != currentQuestion && StringUtils.isNotBlank( currentQuestion ) && null != map ) {
                if ( map.containsKey( PROPERTY_NAME_UUID ) ) {
                    String uuid = map.get( PROPERTY_NAME_UUID, String.class );
                    String finalQuestion = null != uuid ? map.get( uuid, String.class ) : StringUtils.EMPTY;
                    String encodedCurrentQuestion;
                    String encodedFinalQuestion;

                    encodedCurrentQuestion = URLEncoder.encode( currentQuestion,
                            ClassMagsMigrationASRPConstants.ENCODE_FORMAT );
                    encodedFinalQuestion = URLEncoder.encode( finalQuestion,
                            ClassMagsMigrationASRPConstants.ENCODE_FORMAT );
                    if ( !StringUtils.equals( encodedCurrentQuestion, encodedFinalQuestion ) ) {
                        String newUuid = CommonUtils.generateUniqueIdentifier();
                        map.remove( uuid );
                        map.replace( PROPERTY_NAME_UUID, newUuid );
                        map.put( newUuid, currentQuestion );
                    }

                } else {
                    String uuid = CommonUtils.generateUniqueIdentifier();
                    map.put( PROPERTY_NAME_UUID, uuid );
                    map.put( uuid, currentQuestion );

                }
            }
        } catch ( UnsupportedEncodingException e ) {
            LOG.error( "Error while encoding", e );
        } finally {
            resource.getResourceResolver().commit();
        }
    }
}
