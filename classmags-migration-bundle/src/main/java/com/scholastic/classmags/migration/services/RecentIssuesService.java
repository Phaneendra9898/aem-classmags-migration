package com.scholastic.classmags.migration.services;

import java.util.List;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ValueMap;

import com.scholastic.classmags.migration.models.Issue;

/**
 * The Interface RecentIssuesService.
 */
public interface RecentIssuesService {

    /**
     * Gets the issue content.
     * 
     * @param issuePaths The issue paths.
     * 
     * @return the issue content
     * @throws LoginException 
     */
    List< Issue > getRecentIssues( String magazineName, List< ValueMap > issuePaths );

}
