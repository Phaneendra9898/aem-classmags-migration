package com.scholastic.classmags.migration.models;

public class ObjectivesAndSkills {

    /* The Objective and Skills title. */
    private String title;

    /* The Objective and Skills description. */
    private String description;

    /**
     * Gets the objectives and skills title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the objectives and skills title.
     *
     * @param title
     *            the objectives and skills title to set
     */
    public void setTitle( String title ) {
        this.title = title;
    }

    /**
     * Gets the objectives and skills description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the objectives and skills description.
     *
     * @param description
     *            the objectives and skills description to set
     */
    public void setDescription( String description ) {
        this.description = description;
    }
}
