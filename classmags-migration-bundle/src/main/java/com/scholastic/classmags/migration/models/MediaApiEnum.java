package com.scholastic.classmags.migration.models;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Enum MediaApiEnum.
 */
public enum MediaApiEnum {

    /** The command. */
    COMMAND( "command" ),
    /** The page size. */
    PAGE_SIZE( "page_size" ),
    /** The field any search. */
    FIELD_ANY_SEARCH( "any" ),
    /** The video id. */
    VIDEO_ID( "video_id" ),
    /** The video fields. */
    VIDEO_FIELDS( "video_fields" ),
    /** The sort by. */
    SORT_BY( "sort_by" ),
    /** The page number. */
    PAGE_NUMBER( "page_number" ),
    /** The get item count. */
    GET_ITEM_COUNT( "get_item_count" ),
    /** The callback. */
    CALLBACK( "callback" ),
    /** The token. */
    TOKEN( "token" ),
    /** The media delivery. */
    MEDIA_DELIVERY( "media_delivery" ),
    /** The default page size. */
    DEFAULT_PAGE_SIZE( "10" ),
    /** The default page number. */
    DEFAULT_PAGE_NUMBER( "0" ),
    /** The default sort by. */
    DEFAULT_SORT_BY( "START_DATE:DESC" );

    /** The value. */
    private String value;

    /** The Constant NINE. */
    private static final int NINE = 9;

    /** The Constant ZERO. */
    private static final int ZERO = 0;

    /** The Constant EIGHT. */
    private static final int EIGHT = 8;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( MediaApiEnum.class );

    /**
     * Instantiates a new media api enum.
     * 
     * @param value
     *            the value
     */
    private MediaApiEnum( String value ) {
        this.value = value;
    }

    /**
     * Returns the url for the Brightcove Media API.
     * 
     * @param videoSearchServiceData
     *            the video search service data
     * @return String
     */
    public static String getSearchServiceURL( VideoSearchServiceData videoSearchServiceData ) {

        String url = videoSearchServiceData.getUrl();
        String searchComand = videoSearchServiceData.getSearchComand();
        String fieldsSearch = videoSearchServiceData.getFieldsSearch();
        String videoId = videoSearchServiceData.getVideoId();
        String pageSize = videoSearchServiceData.getPageSize();
        String videoFields = videoSearchServiceData.getVideoFields();
        String mediaDelivery = videoSearchServiceData.getMediaDelivery();
        String sortBy = videoSearchServiceData.getSortBy();
        String pageNumber = videoSearchServiceData.getPageNumber();
        String getItemCount = videoSearchServiceData.getGetItemCount();
        String callback = videoSearchServiceData.getCallback();
        String token = videoSearchServiceData.getToken();
        String searchVideos = videoSearchServiceData.getSearchVideos();

        StringBuilder sb = new StringBuilder();
        sb.append( url + "?" );
        sb.append( COMMAND.value + "=" + searchComand + "&" );

        if ( StringUtils.isNotBlank( fieldsSearch ) ) {
            sb.append( FIELD_ANY_SEARCH.value + "=" + fieldsSearch + "&" );
        }

        if ( StringUtils.isNotBlank( videoId ) ) {
            sb.append( VIDEO_ID.value + "=" + videoId + "&" );
        }

        if ( StringUtils.isNotBlank( pageSize ) ) {
            sb.append( PAGE_SIZE.value + "=" + pageSize + "&" );
        } else if ( searchComand.equals( searchVideos ) ) {
            sb.append( PAGE_SIZE.value + "=" + DEFAULT_PAGE_SIZE.value + "&" );
        }

        sb.append( VIDEO_FIELDS.value + "=" + videoFields + "&" );
        sb.append( MEDIA_DELIVERY.value + "=" + mediaDelivery + "&" );

        if ( StringUtils.isNotBlank( pageNumber ) ) {
            try {
                sb.append( PAGE_NUMBER.value + "=" + Integer.parseInt( pageNumber ) + "&" );
            } catch ( Exception e ) {
                sb.append( PAGE_NUMBER.value + "=" + DEFAULT_PAGE_NUMBER.value + "&" );
                LOG.error( "Exception in getSearchServiceURL: " + e );
            }
        } else if ( searchComand.equals( searchVideos ) ) {
            sb.append( PAGE_NUMBER.value + "=" + DEFAULT_PAGE_NUMBER.value + "&" );
        }

        if ( StringUtils.isNotBlank( sortBy ) ) {
            sb.append( SORT_BY.value + "=" + sortBy + "&" );
        } else if ( searchComand.equals( searchVideos ) ) {
            sb.append( SORT_BY.value + "=" + DEFAULT_SORT_BY.value + "&" );
        }

        sb.append( GET_ITEM_COUNT.value + "=" + Boolean.parseBoolean( getItemCount ) + "&" );
        sb.append( CALLBACK.value + "=" + callback + "&" );
        sb.append( TOKEN.value + "=" + token );

        return sb.toString();

    }

    /**
     * Validate error response.
     * 
     * @param json
     *            the json
     * @param log
     *            the log
     * @return true, if successful
     */

    public static boolean validateErrorResponse( String json, Logger log ) {
        if ( ( json.length() > NINE ) && StringUtils.equalsIgnoreCase( "{\"error\"", json.substring( ZERO, EIGHT ) ) ) {
            log.error( "Brightcove Media API Error: " + json );
            return false;
        }
        return true;
    }

}
