package com.scholastic.classmags.migration.models;

/**
 * The Class UIdOrgContextSubHeaderOrganization.
 */
public class UIdOrgContextSubHeaderOrganization {

    /** The org id. */
    private String orgId;

    /**
     * Gets the org id.
     *
     * @return the orgId
     */
    public String getOrgId() {
        return orgId;
    }

    /**
     * Sets the org id.
     *
     * @param orgId
     *            the orgId to set
     */
    public void setOrgId( String orgId ) {
        this.orgId = orgId;
    }
}
