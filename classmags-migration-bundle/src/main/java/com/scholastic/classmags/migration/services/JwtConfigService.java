package com.scholastic.classmags.migration.services;

/**
 * The Interface JWT Config Service.
 */
public interface JwtConfigService {

    /**
     * Gets the JWT secret key.
     * 
     * @return The JWT secret key.
     */
    String getSecretKey( );
    
    /**
     * Gets the JWT issuer id.
     * 
     * @return The JWT issuer id.
     */
    String getIssuer();
}
