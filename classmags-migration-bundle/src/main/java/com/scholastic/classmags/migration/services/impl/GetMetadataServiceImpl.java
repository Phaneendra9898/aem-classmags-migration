package com.scholastic.classmags.migration.services.impl;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;

import com.day.cq.commons.jcr.JcrConstants;
import com.scholastic.classmags.migration.services.GetMetadataService;

/**
 * Get metadata service.
 */
@Component( immediate = true, metatype = true )
@Service( GetMetadataService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Get Metadata Service" ) })
public class GetMetadataServiceImpl implements GetMetadataService {

    private static final String DAM_NODE_PATH = "/jcr:content/metadata";

    @Override
    public String getDAMProperty( SlingHttpServletRequest request, String key, String path ) {

        String propertyValue = "";
        Resource res = request.getResourceResolver().getResource( path + DAM_NODE_PATH );

        if ( null != res ) {
            ValueMap properties = res.adaptTo( ValueMap.class );
            propertyValue = properties != null ? properties.get( key, String.class ) : StringUtils.EMPTY;
        }
        return propertyValue;
    }

    @Override
    public String getPageProperty( SlingHttpServletRequest request, String key, String path ) {

        String propertyValue = "";
        Resource res = request.getResourceResolver().getResource( path + "/" + JcrConstants.JCR_CONTENT );

        if ( null != res ) {
            ValueMap properties = res.adaptTo( ValueMap.class );
            propertyValue = properties != null ? properties.get( key, String.class ) : StringUtils.EMPTY;
        }
        return propertyValue;
    }

}
