package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.List;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.scripting.SlingScriptHelper;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.services.ArticleTextDataService;

/**
 * Use Class for Article Text Data.
 */
public class ArticleTextUse extends WCMUsePojo {

    /** The article data list. */
    private List< String > articleDataList = new ArrayList< >();

    /**
     * Gets the article data list.
     *
     * @return the articleDataList
     */
    public List< String > getArticleDataList() {
        return articleDataList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() throws Exception {

        SlingHttpServletRequest request = getRequest();
        Resource currentResource = request.getResource();
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        if ( null != slingScriptHelper ) {
            ArticleTextDataService articleTextDataService = slingScriptHelper
                    .getService( ArticleTextDataService.class );
            if ( null != articleTextDataService ) {
                articleDataList = articleTextDataService.fetchArticleData( currentResource.getPath() );
            }
        }
    }
}
