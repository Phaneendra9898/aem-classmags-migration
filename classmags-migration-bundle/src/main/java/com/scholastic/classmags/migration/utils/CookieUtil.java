package com.scholastic.classmags.migration.utils;

import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_ACTIVE;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_APPLICATION;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_APPLICATION_CODE;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_ENTITLEMENTS;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_ID;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_IDENTIFIERS;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_LAUNCH_CONTEXT;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_ORGANIZATION;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_ORG_CONTEXT;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_ROLE;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_STAFF_ID;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_STUDENT_ID;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_GROUP_CODE;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_NAME;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_THUMBNAIL;
import static com.scholastic.classmags.migration.utils.CmSDMConstants.JSON_KEY_URL;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.scholastic.classmags.migration.models.UIdHeaderIdentifiers;
import com.scholastic.classmags.migration.models.UIdHeaderLaunchContext;
import com.scholastic.classmags.migration.models.UIdHeaderOrgContext;
import com.scholastic.classmags.migration.models.UIdLaunchContextSubHeaderApplication;
import com.scholastic.classmags.migration.models.UIdOrgContextSubHeaderEntitlements;
import com.scholastic.classmags.migration.models.UIdOrgContextSubHeaderOrganization;
import com.scholastic.classmags.migration.models.UserIdCookieData;

/**
 * The Class CookieUtil.
 */
public class CookieUtil {

    /** The Constant log. */
    private static final Logger LOGGER = LoggerFactory.getLogger( CookieUtil.class );

    /**
     * Instantiates a new cookie util.
     */
    private CookieUtil() {
    }

    /**
     * Creates the cookie.
     *
     * @param name
     *            the name
     * @param payload
     *            the payload
     * @param domain
     *            the domain
     * @param path
     *            the path
     * @param secure
     *            the secure
     * @param encrypt
     *            the encrypt
     * @return the cookie
     */
    public static Cookie createCookie( final String name, String payload, String domain, String path, boolean secure,
            boolean encrypt ) {
        Cookie cookie;
        String cookieValue = payload;
        if ( encrypt ) {
            cookieValue = EncryptionUtils.encrypt( payload );
        } else {
            try {
                cookieValue = URLEncoder.encode( payload, "UTF-8" );
                cookieValue = cookieValue.replaceAll( "\\+", "%20" );
            } catch ( UnsupportedEncodingException e ) {
                LOGGER.error( "Error encoding cookie value, {}", e );
            }
        }
        cookie = new Cookie( name, cookieValue );
        cookie.setPath( path );
        cookie.setDomain( domain );
        cookie.setSecure( secure );
        return cookie;
    }

    /**
     * Fetch user id cookie data.
     *
     * @param request
     *            the request
     * @return the user id cookie data
     */
    public static UserIdCookieData fetchUserIdCookieData( SlingHttpServletRequest request ) {

        UserIdCookieData objUserIdCookieData = null;
        if ( null != request ) {
            Cookie swUserIdCookie = request.getCookie( CmSDMConstants.SW_USER_ID_COOKIE_NAME );
            if ( null != swUserIdCookie ) {
                LOGGER.debug( "Encrypted swUserIdCookie cookie value is {}", swUserIdCookie.getValue() );

                String decryptedCookieValue = EncryptionUtils.decrypt( swUserIdCookie.getValue() );
                LOGGER.debug( "Decrypted swUserIdCookie cookie value is {}", decryptedCookieValue );

                if ( StringUtils.isNotBlank( decryptedCookieValue ) ) {
                    Gson gson = new Gson();
                    objUserIdCookieData = gson.fromJson( decryptedCookieValue, UserIdCookieData.class );
                }
            }
        }

        LOGGER.debug( "UserIdCookieData from Cookie - sw_user_id_cookie :" + objUserIdCookieData );

        return objUserIdCookieData;
    }

    /**
     * Map request param custom dp launch.
     *
     * @param jsonCustomDplaunch
     *            the json custom dplaunch
     * @return the user id cookie data
     * @throws JSONException
     *             the JSON exception
     */
    public static UserIdCookieData mapRequestParamCustomDpLaunch( String jsonCustomDplaunch ) throws JSONException {

        JSONObject jsonObjCustomDpLaunch = new JSONObject( jsonCustomDplaunch );

        UserIdCookieData objUserIdCookieData = new UserIdCookieData();
        objUserIdCookieData.setObjUIdHeaderIdentifiers( fetchUIdHeaderIdentifiers( jsonObjCustomDpLaunch ) );
        objUserIdCookieData.setObjUIdHeaderLaunchContext( fetchUIdHeaderLaunchContext( jsonObjCustomDpLaunch ) );
        objUserIdCookieData.setObjUIdHeaderOrgContext( fetchUIdHeaderOrgContext( jsonObjCustomDpLaunch ) );
        LOGGER.debug( "UserIdCookieData :" + objUserIdCookieData );

        return objUserIdCookieData;
    }

    /**
     * Fetch U id header org context.
     *
     * @param jsonObjCustomDpLaunch
     *            the json obj custom dp launch
     * @return the u id header org context
     */
    private static UIdHeaderOrgContext fetchUIdHeaderOrgContext( JSONObject jsonObjCustomDpLaunch ) {

        UIdHeaderOrgContext objUIdHeaderOrgContext = new UIdHeaderOrgContext();

        objUIdHeaderOrgContext.setObjUIdOrgContextSubHeaderOrganization(
                fetchUIdOrgContextSubHeaderOrganization( jsonObjCustomDpLaunch ) );
        objUIdHeaderOrgContext.setObjListUIdOrgContextSubHeaderEntitlements(
                fetchObjListUIdOrgContextSubHeaderEntitlements( jsonObjCustomDpLaunch ) );

        LOGGER.debug( "UIdHeaderOrgContext :" + objUIdHeaderOrgContext );

        return objUIdHeaderOrgContext;
    }

    /**
     * Fetch obj list U id org context sub header entitlements.
     *
     * @param jsonObjCustomDpLaunch
     *            the json obj custom dp launch
     * @return the list
     */
    private static List< UIdOrgContextSubHeaderEntitlements > fetchObjListUIdOrgContextSubHeaderEntitlements(
            JSONObject jsonObjCustomDpLaunch ) {

        JSONArray listOfEntitlements = jsonObjCustomDpLaunch.optJSONObject( JSON_KEY_ORG_CONTEXT )
                .optJSONArray( JSON_KEY_ENTITLEMENTS );
        List< UIdOrgContextSubHeaderEntitlements > objListUIdOrgContextSubHeaderEntitlements = new ArrayList<>();

        for ( int index = 0; index < listOfEntitlements.length(); index++ ) {

            JSONObject objEntitlements = listOfEntitlements.optJSONObject( index );
            UIdOrgContextSubHeaderEntitlements objUIdOrgContextSubHeaderEntitlements = new UIdOrgContextSubHeaderEntitlements();
            objUIdOrgContextSubHeaderEntitlements
                    .setApplicationCode( objEntitlements.optString( JSON_KEY_APPLICATION_CODE ) );
            objUIdOrgContextSubHeaderEntitlements.setStatus( objEntitlements.optString( JSON_KEY_ACTIVE ) );
            objUIdOrgContextSubHeaderEntitlements.setGroupCode( objEntitlements.optString( JSON_KEY_GROUP_CODE ) );
            objUIdOrgContextSubHeaderEntitlements.setId( objEntitlements.optString( JSON_KEY_ID ) );
            objUIdOrgContextSubHeaderEntitlements.setName( objEntitlements.optString( JSON_KEY_NAME ) );
            objUIdOrgContextSubHeaderEntitlements.setThumbnail( objEntitlements.optString( JSON_KEY_THUMBNAIL ) );
            objUIdOrgContextSubHeaderEntitlements.setUrl( objEntitlements.optString( JSON_KEY_URL ) );
            objListUIdOrgContextSubHeaderEntitlements.add( objUIdOrgContextSubHeaderEntitlements );
        }

        LOGGER.debug( "List of UIdOrgContextSubHeaderEntitlements :" + objListUIdOrgContextSubHeaderEntitlements );

        return objListUIdOrgContextSubHeaderEntitlements;
    }

    /**
     * Fetch U id org context sub header organization.
     *
     * @param jsonObjCustomDpLaunch
     *            the json obj custom dp launch
     * @return the u id org context sub header organization
     */
    private static UIdOrgContextSubHeaderOrganization fetchUIdOrgContextSubHeaderOrganization(
            JSONObject jsonObjCustomDpLaunch ) {

        UIdOrgContextSubHeaderOrganization objUIdOrgContextSubHeaderOrganization = new UIdOrgContextSubHeaderOrganization();
        objUIdOrgContextSubHeaderOrganization.setOrgId( jsonObjCustomDpLaunch.optJSONObject( JSON_KEY_ORG_CONTEXT )
                .optJSONObject( JSON_KEY_ORGANIZATION ).optString( JSON_KEY_ID ) );

        LOGGER.debug( "UIdOrgContextSubHeaderOrganization :" + objUIdOrgContextSubHeaderOrganization );

        return objUIdOrgContextSubHeaderOrganization;
    }

    /**
     * Fetch U id header launch context.
     *
     * @param jsonObjCustomDpLaunch
     *            the json obj custom dp launch
     * @return the u id header launch context
     */
    private static UIdHeaderLaunchContext fetchUIdHeaderLaunchContext( JSONObject jsonObjCustomDpLaunch ) {

        UIdHeaderLaunchContext objUIdHeaderLaunchContext = new UIdHeaderLaunchContext();

        UIdLaunchContextSubHeaderApplication objUIdLaunchContextSubHeaderApplication = new UIdLaunchContextSubHeaderApplication();
        objUIdLaunchContextSubHeaderApplication
                .setApplicationCode( jsonObjCustomDpLaunch.optJSONObject( JSON_KEY_LAUNCH_CONTEXT )
                        .optJSONObject( JSON_KEY_APPLICATION ).optString( JSON_KEY_APPLICATION_CODE ) );

        objUIdHeaderLaunchContext.setObjUIdLaunchContextSubHeaderApplication( objUIdLaunchContextSubHeaderApplication );
        objUIdHeaderLaunchContext
                .setRole( jsonObjCustomDpLaunch.optJSONObject( JSON_KEY_LAUNCH_CONTEXT ).optString( JSON_KEY_ROLE ) );

        LOGGER.debug( "UIdHeaderLaunchContext :" + objUIdHeaderLaunchContext );

        return objUIdHeaderLaunchContext;
    }

    /**
     * Fetch U id header identifiers.
     *
     * @param jsonObjCustomDpLaunch
     *            the json obj custom dp launch
     * @return the u id header identifiers
     */
    private static UIdHeaderIdentifiers fetchUIdHeaderIdentifiers( JSONObject jsonObjCustomDpLaunch ) {

        UIdHeaderIdentifiers objUIdHeaderIdentifiers = new UIdHeaderIdentifiers();

        objUIdHeaderIdentifiers.setStaffId(
                jsonObjCustomDpLaunch.optJSONObject( JSON_KEY_IDENTIFIERS ).optString( JSON_KEY_STAFF_ID ) );
        objUIdHeaderIdentifiers.setStudentId(
                jsonObjCustomDpLaunch.optJSONObject( JSON_KEY_IDENTIFIERS ).optString( JSON_KEY_STUDENT_ID ) );

        LOGGER.debug( "UIdHeaderIdentifiers :" + objUIdHeaderIdentifiers );

        return objUIdHeaderIdentifiers;
    }
}
