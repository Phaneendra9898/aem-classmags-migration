package com.scholastic.classmags.migration.controllers;


import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * Use Class for Setting role information in request attribute
 */
public class PageAccessUse extends WCMUsePojo {

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() throws Exception {

        String userRole = getRequest().getAttribute( "role" ).toString();
        String pageRole = getCurrentPage().getProperties().get( "userType", "everyone" );
        boolean shouldRender = RoleUtil.shouldRender( userRole, pageRole );
        if ( !shouldRender ) {
            getResponse().getWriter().write( "You do not have the permissions to access this page!" );
            getResponse().sendError( 403 );
        }
    }
}
