package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.adobe.acs.commons.widgets.MultiFieldPanelFunctions;
import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.models.ProductDisplay;

public class ProductDisplayUse extends WCMUsePojo {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger( ProductDisplayUse.class );

	/** The book resource */
	private Resource resource;
	/** The Constant links */
	private static final String MAGAZINE_DETAILS = "magazineDetails";

	private static final String MAGAZINE_TITLE = "magazineTitle";

	private static final String MAGAZINE_LINK = "magazinePath";

	private static final String MAGAZINE_DESCRIPTION = "magazineDescription";

	private static final String LEARN_MORE_LABEL = "learnMoreLabel";

	private static final String LEARN_MORE_LINK = "learnMoreLink";

	private static final String LEARN_MORE_DESCRIPTION = "learnMoreDescription";
	
	private static final String MAGAZINE_NAME = "magazineName";


	@Override
	public void activate() throws Exception {
		// TODO Auto-generated method stub
		LOG.debug("Inside activate");
		resource = getResource();
		LOG.debug("Exiting activate");

	}


	public List<ProductDisplay> getProductDisplay() {
		List<Map<String, String>> magazineDetails = null;
		List<ProductDisplay> magazineList = new ArrayList<>();
		if (null != resource && !ResourceUtil.isNonExistingResource(resource)) {
			magazineDetails = MultiFieldPanelFunctions.getMultiFieldPanelValues(resource, MAGAZINE_DETAILS);
			if (!magazineDetails.isEmpty()) {
				createMagazineList(magazineDetails, magazineList);
			}
		}
		return magazineList;
	}

	private void createMagazineList(List<Map<String, String>> magazineDetails, List<ProductDisplay> magazineList) {
		ProductDisplay product;
		for (Map<String, String> map : magazineDetails) {
			product = new ProductDisplay();
			product.setMagazineTitle(map.get(MAGAZINE_TITLE));
			product.setMagazineDescription(map.get(MAGAZINE_DESCRIPTION));
			product.setMagazinePath(map.get(MAGAZINE_LINK));
			product.setLearnMorelabel(map.get(LEARN_MORE_LABEL));
			product.setLearnMoreLink(map.get(LEARN_MORE_LINK));
			product.setLearnMorePopup(map.get(LEARN_MORE_DESCRIPTION));
			product.setMagazineName(map.get(MAGAZINE_NAME));
			magazineList.add(product);
		}
	}

}
