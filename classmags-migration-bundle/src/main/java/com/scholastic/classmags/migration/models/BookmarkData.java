package com.scholastic.classmags.migration.models;

/**
 * The Class BookmarkData.
 */
public class BookmarkData {

    /** The bookmark path. */
    private String bookmarkPath;

    /** The date saved. */
    private String dateSaved;

    /** The view article link. */
    private String viewArticleLink;

    /** The magazine type. */
    private String magazineType;

    /**
     * Gets the bookmark path.
     *
     * @return the bookmarkPath
     */
    public String getBookmarkPath() {
        return bookmarkPath;
    }

    /**
     * Sets the bookmark path.
     *
     * @param bookmarkPath
     *            the bookmarkPath to set
     */
    public void setBookmarkPath( String bookmarkPath ) {
        this.bookmarkPath = bookmarkPath;
    }

    /**
     * Gets the date saved.
     *
     * @return the dateSaved
     */
    public String getDateSaved() {
        return dateSaved;
    }

    /**
     * Sets the date saved.
     *
     * @param dateSaved
     *            the dateSaved to set
     */
    public void setDateSaved( String dateSaved ) {
        this.dateSaved = dateSaved;
    }

    /**
     * Gets the view article link.
     *
     * @return the viewArticleLink
     */
    public String getViewArticleLink() {
        return viewArticleLink;
    }

    /**
     * Sets the view article link.
     *
     * @param viewArticleLink
     *            the viewArticleLink to set
     */
    public void setViewArticleLink( String viewArticleLink ) {
        this.viewArticleLink = viewArticleLink;
    }

    /**
     * Gets the magazine type.
     *
     * @return the magazineType
     */
    public String getMagazineType() {
        return magazineType;
    }

    /**
     * Sets the magazine type.
     *
     * @param magazineType
     *            the magazineType to set
     */
    public void setMagazineType( String magazineType ) {
        this.magazineType = magazineType;
    }
}
