package com.scholastic.classmags.migration.models;

/**
 * Model Class VocabData.
 */
public class VideoSearchServiceData {

    /** The url. */
    private String url;

    /** The search comand. */
    private String searchComand;

    /** The fields search. */
    private String fieldsSearch;

    /** The video id. */
    private String videoId;

    /** The page size. */
    private String pageSize;

    /** The video fields. */
    private String videoFields;

    /** The media delivery. */
    private String mediaDelivery;

    /** The sort by. */
    private String sortBy;

    /** The page number. */
    private String pageNumber;

    /** The get item count. */
    private String getItemCount;

    /** The callback. */
    private String callback;

    /** The token. */
    private String token;

    /** The search videos. */
    private String searchVideos;

    /**
     * Gets the url.
     * 
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Gets the search comand.
     * 
     * @return the searchComand
     */
    public String getSearchComand() {
        return searchComand;
    }

    /**
     * Gets the fields search.
     * 
     * @return the fieldsSearch
     */
    public String getFieldsSearch() {
        return fieldsSearch;
    }

    /**
     * Gets the video id.
     * 
     * @return the videoId
     */
    public String getVideoId() {
        return videoId;
    }

    /**
     * Gets the page size.
     * 
     * @return the pageSize
     */
    public String getPageSize() {
        return pageSize;
    }

    /**
     * Gets the video fields.
     * 
     * @return the videoFields
     */
    public String getVideoFields() {
        return videoFields;
    }

    /**
     * Gets the media delivery.
     * 
     * @return the mediaDelivery
     */
    public String getMediaDelivery() {
        return mediaDelivery;
    }

    /**
     * Gets the sort by.
     * 
     * @return the sortBy
     */
    public String getSortBy() {
        return sortBy;
    }

    /**
     * Gets the page number.
     * 
     * @return the pageNumber
     */
    public String getPageNumber() {
        return pageNumber;
    }

    /**
     * Gets the gets the item count.
     * 
     * @return the getItemCount
     */
    public String getGetItemCount() {
        return getItemCount;
    }

    /**
     * Gets the callback.
     * 
     * @return the callback
     */
    public String getCallback() {
        return callback;
    }

    /**
     * Gets the token.
     * 
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * Gets the search videos.
     * 
     * @return the searchVideos
     */
    public String getSearchVideos() {
        return searchVideos;
    }

    /**
     * Sets the url.
     * 
     * @param url
     *            the url to set
     */
    public void setUrl( String url ) {
        this.url = url;
    }

    /**
     * Sets the search comand.
     * 
     * @param searchComand
     *            the searchComand to set
     */
    public void setSearchComand( String searchComand ) {
        this.searchComand = searchComand;
    }

    /**
     * Sets the fields search.
     * 
     * @param fieldsSearch
     *            the fieldsSearch to set
     */
    public void setFieldsSearch( String fieldsSearch ) {
        this.fieldsSearch = fieldsSearch;
    }

    /**
     * Sets the video id.
     * 
     * @param videoId
     *            the videoId to set
     */
    public void setVideoId( String videoId ) {
        this.videoId = videoId;
    }

    /**
     * Sets the page size.
     * 
     * @param pageSize
     *            the pageSize to set
     */
    public void setPageSize( String pageSize ) {
        this.pageSize = pageSize;
    }

    /**
     * Sets the video fields.
     * 
     * @param videoFields
     *            the videoFields to set
     */
    public void setVideoFields( String videoFields ) {
        this.videoFields = videoFields;
    }

    /**
     * Sets the media delivery.
     * 
     * @param mediaDelivery
     *            the mediaDelivery to set
     */
    public void setMediaDelivery( String mediaDelivery ) {
        this.mediaDelivery = mediaDelivery;
    }

    /**
     * Sets the sort by.
     * 
     * @param sortBy
     *            the sortBy to set
     */
    public void setSortBy( String sortBy ) {
        this.sortBy = sortBy;
    }

    /**
     * Sets the page number.
     * 
     * @param pageNumber
     *            the pageNumber to set
     */
    public void setPageNumber( String pageNumber ) {
        this.pageNumber = pageNumber;
    }

    /**
     * Sets the gets the item count.
     * 
     * @param getItemCount
     *            the getItemCount to set
     */
    public void setGetItemCount( String getItemCount ) {
        this.getItemCount = getItemCount;
    }

    /**
     * Sets the callback.
     * 
     * @param callback
     *            the callback to set
     */
    public void setCallback( String callback ) {
        this.callback = callback;
    }

    /**
     * Sets the token.
     * 
     * @param token
     *            the token to set
     */
    public void setToken( String token ) {
        this.token = token;
    }

    /**
     * Sets the search videos.
     * 
     * @param searchVideos
     *            the searchVideos to set
     */
    public void setSearchVideos( String searchVideos ) {
        this.searchVideos = searchVideos;
    }
}
