package com.scholastic.classmags.migration.services;

import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;

import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.models.ArticleAggregatorObject;

/**
 * The Interface ArticleAggregatorService.
 */
public interface ArticleAggregatorService {

    /**
     * Gets the article aggregator data.
     * 
     * @param currentPage
     *            the current page
     * @return the article aggregator data
     * @throws LoginException
     */
    List< ArticleAggregatorObject > getArticleAggregatorData(SlingHttpServletRequest request, Page currentPage );

}
