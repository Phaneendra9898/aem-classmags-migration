package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.models.MagazinesMetaData;
import com.scholastic.classmags.migration.models.PromoteMagazine;
import com.scholastic.classmags.migration.services.PromotMagazineDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * Use Class for PromoteMagazines.
 */
public class PromoteMagazinesUse extends WCMUsePojo {
	
	/** The mags data list. */
    private List< MagazinesMetaData > magsDataList;

    /** The promote magazine. */
    private PromoteMagazine promoteMagazine;

    /**
     * Gets the mags data list.
     *
     * @return the magsDataList
     */
    public List< MagazinesMetaData > getMagsDataList() {
        return magsDataList;
    }

    /**
     * Gets the promote magazine.
     *
     * @return the promoteMagazine
     */
    public PromoteMagazine getPromoteMagazine() {
        return promoteMagazine;
    }

    /**
     * Sets the promote magazine.
     *
     * @param promoteMagazine
     *            the promoteMagazine to set
     */
    public void setPromoteMagazine( PromoteMagazine promoteMagazine ) {
        this.promoteMagazine = promoteMagazine;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() {

        SlingHttpServletRequest request = getRequest();
        List< ValueMap > magsDataMapList;
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        Resource currentResource = request.getResource();
        promoteMagazine = request.adaptTo( PromoteMagazine.class );
        if ( null != slingScriptHelper ) {
            String currentPath = currentResource.getPath();
            PromotMagazineDataService promotMagazineDataService = slingScriptHelper
                    .getService( PromotMagazineDataService.class );
            if ( null != promotMagazineDataService ) {
                magsDataMapList = promotMagazineDataService.fetchMagazineData( currentPath );
                addItemsToMagsDataList( magsDataMapList );
            }
        }
    }

    /**
     * Adds the items to mags data list.
     *
     * @param magsDataMapList
     *            the mags data map list
     */
    private void addItemsToMagsDataList( List< ValueMap > magsDataMapList ) {

        magsDataList = new ArrayList< >();

        for ( ValueMap magsDataMap : magsDataMapList ) {

            MagazinesMetaData magazinesMetaData = new MagazinesMetaData();
            magazinesMetaData
                    .setMagazineName( magsDataMap.get( ClassMagsMigrationConstants.MAGAZINE_NAME, StringUtils.EMPTY ) );
            magazinesMetaData.setMagazinePath( InternalURLFormatter.formatURL( getResourceResolver(),
                    magsDataMap.get( ClassMagsMigrationConstants.MAGAZINE_PATH, StringUtils.EMPTY ) ) );
            magazinesMetaData.setMagazineDescription(
                    magsDataMap.get( ClassMagsMigrationConstants.MAGAZINE_DESCRIPTION, StringUtils.EMPTY ) );
            magazinesMetaData.setImagePath(
            		magsDataMap.get(ClassMagsMigrationConstants.IMAGE_PATH, StringUtils.EMPTY));
           

            magsDataList.add( magazinesMetaData );
        }
    }
}
