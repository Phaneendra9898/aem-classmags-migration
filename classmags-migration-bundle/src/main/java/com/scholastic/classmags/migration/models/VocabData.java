package com.scholastic.classmags.migration.models;

/**
 * Model Class VocabData.
 */
public class VocabData {

    /** The vocab word. */
    String vocabWord;

    /** The word definition. */
    String wordDefinition;

    /** The word audio path. */
    String wordAudioPath;

    /** The word image path. */
    String wordImagePath;

    /** The word pronunciation text. */
    String wordPronunciationText;

    /** The word image credits. */
    String wordImageCredits;

    /**
     * Gets the vocab word.
     *
     * @return the vocabWord
     */
    public String getVocabWord() {
        return vocabWord;
    }

    /**
     * Sets the vocab word.
     *
     * @param vocabWord
     *            the vocabWord to set
     */
    public void setVocabWord( String vocabWord ) {
        this.vocabWord = vocabWord;
    }

    /**
     * Gets the word definition.
     *
     * @return the wordDefinition
     */
    public String getWordDefinition() {
        return wordDefinition;
    }

    /**
     * Sets the word definition.
     *
     * @param wordDefinition
     *            the wordDefinition to set
     */
    public void setWordDefinition( String wordDefinition ) {
        this.wordDefinition = wordDefinition;
    }

    /**
     * Gets the word audio path.
     *
     * @return the wordAudioPath
     */
    public String getWordAudioPath() {
        return wordAudioPath;
    }

    /**
     * Sets the word audio path.
     *
     * @param wordAudioPath
     *            the wordAudioPath to set
     */
    public void setWordAudioPath( String wordAudioPath ) {
        this.wordAudioPath = wordAudioPath;
    }

    /**
     * Gets the word image path.
     *
     * @return the wordImagePath
     */
    public String getWordImagePath() {
        return wordImagePath;
    }

    /**
     * Sets the word image path.
     *
     * @param wordImagePath
     *            the wordImagePath to set
     */
    public void setWordImagePath( String wordImagePath ) {
        this.wordImagePath = wordImagePath;
    }

    /**
     * Gets the word pronunciation text.
     *
     * @return the wordPronunciationText
     */
    public String getWordPronunciationText() {
        return wordPronunciationText;
    }

    /**
     * Sets the word pronunciation text.
     *
     * @param wordPronunciationText
     *            the wordPronunciationText to set
     */
    public void setWordPronunciationText( String wordPronunciationText ) {
        this.wordPronunciationText = wordPronunciationText;
    }

    /**
     * Gets the word image credits.
     *
     * @return the wordImageCredits
     */
    public String getWordImageCredits() {
        return wordImageCredits;
    }

    /**
     * Sets the word image credits.
     *
     * @param wordImageCredits
     *            the wordImageCredits to set
     */
    public void setWordImageCredits( String wordImageCredits ) {
        this.wordImageCredits = wordImageCredits;
    }
}
