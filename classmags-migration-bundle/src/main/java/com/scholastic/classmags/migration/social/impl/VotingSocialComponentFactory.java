package com.scholastic.classmags.migration.social.impl;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.xss.XSSAPI;
import org.osgi.framework.Constants;

import com.adobe.cq.social.scf.ClientUtilities;
import com.adobe.cq.social.scf.QueryRequestInfo;
import com.adobe.cq.social.scf.SocialComponent;
import com.adobe.cq.social.scf.SocialComponentFactory;
import com.adobe.cq.social.scf.core.AbstractSocialComponentFactory;
import com.adobe.cq.social.ugc.api.UgcSearch;
import com.scholastic.classmags.migration.social.api.VotingSocialComponent;

/**
 * A factory for creating VotingSocialComponent objects.
 */
@SuppressWarnings( "deprecation" )
@Component( immediate = true, metatype = false )
@Service( SocialComponentFactory.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Social Component Factory For Voting" ) })
public class VotingSocialComponentFactory extends AbstractSocialComponentFactory implements SocialComponentFactory {

    /** The ugc search. */
    @Reference
    private UgcSearch ugcSearch;

    /** The xssapi. */
    @Reference
    private XSSAPI xssapi;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.adobe.cq.social.scf.SocialComponentFactory#getSocialComponent(org.
     * apache.sling.api.resource.Resource)
     */
    @Override
    public SocialComponent getSocialComponent( Resource resource ) {
        return new VotingSocialComponentImpl( resource, this.getClientUtilities( resource.getResourceResolver() ),
                ugcSearch, xssapi, this.getSocialUtils() );
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.adobe.cq.social.scf.SocialComponentFactory#getSocialComponent(org.
     * apache.sling.api.resource.Resource,
     * org.apache.sling.api.SlingHttpServletRequest)
     */
    @Override
    public SocialComponent getSocialComponent( Resource resource, SlingHttpServletRequest request ) {
        return new VotingSocialComponentImpl( resource, this.getClientUtilities( request ), ugcSearch, xssapi,
                this.getSocialUtils() );
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.adobe.cq.social.scf.SocialComponentFactory#getSocialComponent(org.
     * apache.sling.api.resource.Resource,
     * com.adobe.cq.social.scf.ClientUtilities,
     * com.adobe.cq.social.scf.QueryRequestInfo)
     */
    @Override
    public SocialComponent getSocialComponent( Resource resource, ClientUtilities clientUtils,
            QueryRequestInfo queryRequestInfo ) {
        return new VotingSocialComponentImpl( resource, clientUtils, ugcSearch, xssapi, this.getSocialUtils() );
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.adobe.cq.social.scf.SocialComponentFactory#getSupportedResourceType()
     */
    @Override
    public String getSupportedResourceType() {
        return VotingSocialComponent.VOTING_RESOURCE_TYPE;
    }
}
