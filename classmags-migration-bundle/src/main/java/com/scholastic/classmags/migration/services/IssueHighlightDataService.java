package com.scholastic.classmags.migration.services;

import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONException;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.ResourcesObject;

/**
 * The Interface IssueHighlightDataService.
 */
public interface IssueHighlightDataService {
    
    /**
     * Fetch featured resources.
     *
     * @param request
     *            the request
     * @param currentPath
     *            the current path
     * @return the list
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     * @throws JSONException 
     */
    public List< ResourcesObject > fetchFeaturedResources( SlingHttpServletRequest request, String currentPath )
            throws ClassmagsMigrationBaseException, JSONException;
    
    /**
     * Fetch issue sorting date.
     *
     * @param currentPath
     *            the current path
     * @return the string
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    public String fetchIssueSortingDate( String currentPath ) throws ClassmagsMigrationBaseException;

    /**
     * Fetch asset data.
     *
     * @param currentPath
     *            the current path
     * @return the list
     */
    public List< ValueMap > fetchAssetData( String currentPath );
    
    /**
     * Fetch issue display date.
     *
     * @param currentPath
     *            the current path
     * @return the display date.
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    public String fetchIssueDisplayDate( String currentPath ) throws ClassmagsMigrationBaseException;
}
