package com.scholastic.classmags.migration.controllers;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.scholastic.classmags.migration.services.IssueService;
import com.scholastic.classmags.migration.services.MagazineProps;

/**
 * Use Class for Issue Link.
 */
public class IssueLinkUse extends WCMUsePojo {

    /** The parent issue page path. */
    private String parentIssuePath;

    /** The issue date. */
    private String issueDate;

    /** The issue service. */
    @Reference
    IssueService issueService;
    
    /** The magazines prop service. */
    MagazineProps magazineProps;

    /**
     * Gets the parent issue path.
     *
     * @return the parent issue path
     */
    public String getParentIssuePath() {
        return parentIssuePath;
    }

    /**
     * Gets the issue date.
     *
     * @return the issue date
     */
    public String getIssueDate() {
        return issueDate;
    }

    @Override
    public void activate() throws Exception {
        SlingHttpServletRequest request = getRequest();
        Resource currentResource = request.getResource();
        String currentPagePath = currentResource.getPath();
        PageManager currentPageManager = currentResource.getResourceResolver().adaptTo( PageManager.class );
        parentIssuePath = StringUtils.EMPTY;
        if ( null != currentPageManager ) {
            Page currentPage = currentPageManager.getContainingPage( currentResource );
            if ( null != currentPage ) {
                issueService = getSlingScriptHelper().getService( IssueService.class );
                magazineProps = getSlingScriptHelper().getService( MagazineProps.class );
                parentIssuePath = issueService.getParentIssuePath( request, currentPage );
                issueDate = issueService.getParentIssueDate( request, currentPage, magazineProps.getMagazineName( currentPagePath ) );
            }
        }
    }

}
