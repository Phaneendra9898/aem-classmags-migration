package com.scholastic.classmags.migration.models;

/**
 * Model Class for KeyAssets.
 */
public class KeyAssets {

    /** The issue key assets label. */
    private String issueKeyAssetsLabel;

    /** The issue key assets label path. */
    private String issueKeyAssetsLabelPath;

    /** The issue key asset description. */
    private String description;

    /** The user type to which asset is available. */
    private String userType;

    /** The issue key assets shareable flag. */
    private Boolean isShareableExtension;

    /** The label color. */
    private String labelColor;

    /** The issue key assets bookmark path. */
    private String issueKeyAssetsBookmarkPath;

    /**
     * Gets the issue key assets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the issue key assets description.
     *
     * @param description
     *            the description to set
     */
    public void setDescrition( String description ) {
        this.description = description;
    }

    /**
     * Gets the isShareableExtension flag.
     *
     * @return the isShareableExtension
     */
    public Boolean getIssueKeyAssetsIsShareable() {
        return isShareableExtension;
    }

    /**
     * Sets the isShareableExtension flag.
     *
     * @param isShareableExtension
     *            the isShareableExtension to set
     */
    public void setIssueKeyAssetsIsShareable( Boolean isShareableExtension ) {
        this.isShareableExtension = isShareableExtension;
    }

    /**
     * Gets the issue key assets label.
     *
     * @return the issueKeyAssetsLabel
     */
    public String getIssueKeyAssetsLabel() {
        return issueKeyAssetsLabel;
    }

    /**
     * Sets the issue key assets label.
     *
     * @param issueKeyAssetsLabel
     *            the issueKeyAssetsLabel to set
     */
    public void setIssueKeyAssetsLabel( String issueKeyAssetsLabel ) {
        this.issueKeyAssetsLabel = issueKeyAssetsLabel;
    }

    /**
     * Gets the issue key assets label path.
     *
     * @return the issueKeyAssetsLabelPath
     */
    public String getIssueKeyAssetsLabelPath() {
        return issueKeyAssetsLabelPath;
    }

    /**
     * Sets the issue key assets label path.
     *
     * @param issueKeyAssetsLabelPath
     *            the issueKeyAssetsLabelPath to set
     */
    public void setIssueKeyAssetsLabelPath( String issueKeyAssetsLabelPath ) {
        this.issueKeyAssetsLabelPath = issueKeyAssetsLabelPath;
    }

    /**
     * Gets the user type.
     *
     * @return the userType
     */
    public String getUserType() {
        return userType;
    }

    /**
     * Sets the user type.
     *
     * @param userType
     *            The user type.
     */
    public void setUserType( String userType ) {
        this.userType = userType;
    }

    /**
     * Gets the label color.
     *
     * @return the labelColor
     */
    public String getLabelColor() {
        return labelColor;
    }

    /**
     * Sets the label color.
     *
     * @param labelColor
     *            The label color.
     */
    public void setLabelColor( String labelColor ) {
        this.labelColor = labelColor;
    }

    /**
     * Gets the issue key assets bookmark path.
     *
     * @return the issueKeyAssetsBookmarkPath
     */
    public String getIssueKeyAssetsBookmarkPath() {
        return issueKeyAssetsBookmarkPath;
    }

    /**
     * Sets the issue key assets bookmark path.
     *
     * @param issueKeyAssetsBookmarkPath
     *            the issueKeyAssetsBookmarkPath to set
     */
    public void setIssueKeyAssetsBookmarkPath( String issueKeyAssetsBookmarkPath ) {
        this.issueKeyAssetsBookmarkPath = issueKeyAssetsBookmarkPath;
    }
}
