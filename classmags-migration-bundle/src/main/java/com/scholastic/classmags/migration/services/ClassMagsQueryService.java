package com.scholastic.classmags.migration.services;

import java.util.List;
import java.util.Map;

import com.scholastic.classmags.migration.models.SearchResponse;

/*
 * The Class Magazines Query Service
 */
public interface ClassMagsQueryService {

    /**
     * 
     * @param params
     * @param includeFacet
     * @param queryType
     * @return
     */
    public SearchResponse getSearchResults( Map<String, String> params, boolean includeFacet, String queryType );
    
    /**
     * 
     * @param searchResponse
     * @return
     */
    public String getResultsAsJSONString( SearchResponse searchResponse );
    
    /**
     * 
     * @param solrResponses
     * @return
     */
    public String getResultsAsJSONArrayString( List< SearchResponse > solrResponses );
}
