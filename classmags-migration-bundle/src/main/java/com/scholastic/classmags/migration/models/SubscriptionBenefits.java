package com.scholastic.classmags.migration.models;

/**
 * Model Class for KeyValues.
 */
public class SubscriptionBenefits {

    /** The subscription benefit icon */
    private String subscriptionBenefitIcon;

    /** The subscription benefit title. */
    private String subscriptionBenefitTitle;

    /** The subscription benefit description. */
    private String subscriptionBenefitDescription;

    /**
     * Sets the subscription benefit icon.
     *
     * @param icons
     *            the subscription benefit icons to set
     */
    public void setSubscriptionBenefitIcon( String subscriptionBenefitIcon ) {
        this.subscriptionBenefitIcon = subscriptionBenefitIcon;
    }

    /**
     * Gets the subscription benefit icon.
     *
     * @return the subscription benefit icon
     */
    public String getSubscriptionBenefitIcon() {
        return subscriptionBenefitIcon;
    }

    /**
     * Sets the subscription benefit title.
     *
     * @param subscriptionBenefitTitle
     *            the subscriptionBenefitTitle to set
     */
    public void setSubscriptionBenefitTitle( String subscriptionBenefitTitle ) {
        this.subscriptionBenefitTitle = subscriptionBenefitTitle;
    }

    /**
     * Gets the subscription benefit title.
     *
     * @return the subscriptionBenefitTitle.
     */
    public String getSubscriptionBenefitTitle() {
        return subscriptionBenefitTitle;
    }

    /**
     * Sets the subscription benefit description.
     *
     * @param subscriptionBenefitDescription
     *            the subscriptionBenefitDescription to set
     */
    public void setSubscriptionBenefitDescription( String subscriptionBenefitDescription ) {
        this.subscriptionBenefitDescription = subscriptionBenefitDescription;
    }

    /**
     * Gets the subscription benefit description.
     *
     * @return the subscriptionBenefitDescription.
     */
    public String getSubscriptionBenefitDescription() {
        return subscriptionBenefitDescription;
    }
}
