package com.scholastic.classmags.migration.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONException;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.WCMMode;
import com.scholastic.classmags.migration.models.GameModel;
import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.models.Slideshow;
import com.scholastic.classmags.migration.models.TeachingResourcesType;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.services.SlideshowDataService;
import com.scholastic.classmags.migration.services.TeachingResourcesService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * The Class TeachingResourcesServiceImpl.
 */
@Component( immediate = true, metatype = false )
@Service( TeachingResourcesService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Teaching resources configuration service" ) })
public class TeachingResourcesServiceImpl implements TeachingResourcesService {

    /** The prop config service. */
    @Reference
    PropertyConfigService propConfigService;

    /** The magazine props service. */
    @Reference
    private MagazineProps magazineProps;

    /** The resource path config service. */
    @Reference
    private ResourcePathConfigService resourcePathConfigService;

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;
    
    /** The slideshow data service. */
    @Reference
    private SlideshowDataService slideshowDataService;

    /** The Constant RESOURCE_TYPE. */
    private static final String RESOURCE_TYPE = "resourceType";

    /** The Constant RESOURCE_TYPE. */
    private static final String RESOURCE_PATH = "resourcePath";

    /** The Constant TWO. */
    private static final int TWO = 2;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( TeachingResourcesServiceImpl.class );

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.scholastic.classmags.migration.services.TeachingResourcesService#
     * getTeachingResources(org.apache.sling.api.resource.Resource)
     */
    @Override
    public Map< String, List< ResourcesObject > > getTeachingResources( SlingHttpServletRequest request,
            Resource resource, String nodeName ) {
        Resource resources = resource.getChild( nodeName );
        if ( resources != null && resources.hasChildren() ) {
            return buildTeachingResources( request, resources );
        }
        return null;
    }
    
    @Override
    public List< ResourcesObject > getFeaturedResource( SlingHttpServletRequest request, Resource resources ) throws JSONException {
        if ( resources != null && resources.hasChildren() ) {
            return buildFeaturedResources( request, resources );
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.scholastic.classmags.migration.services.TeachingResourcesService#
     * createResourcesMap(java.util.Map, java.util.Map)
     */
    @Override
    public Map< String, List< ResourcesObject > > createResourcesMap(
            Map< String, List< ResourcesObject > > issueResourceMap,
            Map< String, List< ResourcesObject > > customResourceMap ) {

        if ( null == issueResourceMap || issueResourceMap.isEmpty() ) {
            return customResourceMap;
        }
        Set< String > customKey = customResourceMap.keySet();
        List< ResourcesObject > customResourceList;
        for ( String key : customKey ) {
            customResourceList = customResourceMap.get( key );
            if ( issueResourceMap.containsKey( key ) ) {
                issueResourceMap.get( key ).addAll( customResourceList );
            } else {
                issueResourceMap.put( key, customResourceList );
            }

        }
        return issueResourceMap;
    }

    /**
     * Builds the teaching resources.
     * 
     * @param resource
     *            the resource
     * @return the map
     */
    private Map< String, List< ResourcesObject > > buildTeachingResources( SlingHttpServletRequest request,
            Resource resource ) {
        Map< String, List< ResourcesObject > > teachingResMap = new HashMap< >();

        Iterator< Resource > resChildItr = resource.getChildren().iterator();

        while ( resChildItr.hasNext() ) {
            Resource childRes = resChildItr.next();

            String resourceType = getResourceType( childRes );

            String contentType = TeachingResourcesType.getResourceContentType( resourceType );

            if ( StringUtils.equalsIgnoreCase( contentType, ClassMagsMigrationConstants.ASSET ) ) {
                teachingResMap = getAssetData( teachingResMap, childRes, resourceType, request );
            } else if ( StringUtils.equalsIgnoreCase( contentType, ClassMagsMigrationConstants.PAGE ) ) {
                teachingResMap = getPageData( teachingResMap, childRes, resourceType, request );
            }
        }
        return teachingResMap;
    }

    private List< ResourcesObject > buildFeaturedResources( SlingHttpServletRequest request, Resource resource )
            throws JSONException {
        List< ResourcesObject > featuredResources = new ArrayList< ResourcesObject >();

        Iterator< Resource > resChildItr = resource.getChildren().iterator();

        while ( resChildItr.hasNext() ) {
            Resource childRes = resChildItr.next();
            String resourceType = getResourceType( childRes );
            String contentType = TeachingResourcesType.getResourceContentType( resourceType );

            String path = getResourcePath( childRes );

            if ( StringUtils.equalsIgnoreCase( contentType, ClassMagsMigrationConstants.PAGE ) ) {
                Resource pageResource = CommonUtils.getResource( resourceResolverFactory,
                        ClassMagsMigrationConstants.READ_SERVICE, path );
                Resource jcrContent = pageResource != null ? pageResource.getChild( JcrConstants.JCR_CONTENT ) : null;
                ValueMap properties = jcrContent != null ? jcrContent.getValueMap() : null;
                if ( properties != null ) {
                    ResourcesObject resourcesObject = setPageMetadata( properties, jcrContent );
                    resourcesObject.setContentType( resourceType );
                    resourcesObject.setPagePath( InternalURLFormatter.formatURL( CommonUtils.getResourceResolver(
                            resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ), path ) );
                    resourcesObject.setBookmarkPath( path );
                    String userRole = RoleUtil.getUserRole( request );
                    if ( RoleUtil.shouldRender( userRole, resourcesObject.getUserRole() )
                            || StringUtils.equals( userRole, ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS ) ) {
                        if ( WCMMode.fromRequest( request ).equals( WCMMode.EDIT ) ) {
                            resourcesObject.setUserRole( ClassMagsMigrationConstants.USER_TYPE_EVERYONE );
                        }
                        featuredResources.add( resourcesObject );
                    }
                }

            } else if ( StringUtils.equalsIgnoreCase( contentType, ClassMagsMigrationConstants.ASSET ) ) {
                Resource assetResource = CommonUtils.getResource( resourceResolverFactory,
                        ClassMagsMigrationConstants.READ_SERVICE, path );
                Asset asset = assetResource != null ? assetResource.adaptTo( Asset.class ) : null;
                Resource metadataNode = assetResource != null
                        ? assetResource.getChild( ClassMagsMigrationConstants.ASSET_METADATA_NODE ) : null;
                if ( asset != null ) {

                    ResourcesObject resourcesObject = setAssetMetadata( asset, metadataNode, resource );
                    String userRole = RoleUtil.getUserRole( request );
                    resourcesObject.setContentType( resourceType );

                    if ( RoleUtil.shouldRender( userRole, resourcesObject.getUserRole() )
                            || StringUtils.equals( userRole, ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS ) ) {
                        if ( WCMMode.fromRequest( request ).equals( WCMMode.EDIT ) ) {
                            resourcesObject.setUserRole( ClassMagsMigrationConstants.USER_TYPE_EVERYONE );
                        }
                        featuredResources.add( resourcesObject );
                    }
                }
            } else if ( StringUtils.equalsIgnoreCase( contentType, ClassMagsMigrationConstants.COMPONENT ) ) {

                Resource componentResource = CommonUtils.getResource( resourceResolverFactory,
                        ClassMagsMigrationConstants.READ_SERVICE, path );

                if ( componentResource != null ) {

                    ValueMap valueMap = componentResource.getValueMap();
                    Resource metadataNode = componentResource
                            .getChild( ClassMagsMigrationConstants.SLIDESHOW_META_DATA_NODE );

                    Slideshow slideshow = new Slideshow();
                    ResourcesObject resourcesObject = new ResourcesObject();

                    slideshow
                            .setCoverImagePath( valueMap.get( "slideshowCoverImageFileReference", StringUtils.EMPTY ) );
                    slideshow.setNumberColor( valueMap.get( "slideshowNumberColor", StringUtils.EMPTY ) );
                    slideshow.setDisableNumber( valueMap.get( "slideshowDisableNumbers", false ) );
                    slideshow.setUniqueId( CommonUtils.generateUniqueIdentifier() );

                    if ( null != metadataNode ) {
                        slideshow.setJsonData( slideshowDataService
                                .convertListToJSONArray( CommonUtils.fetchMultiFieldData( metadataNode ) ) );
                    }
                    
                    resourcesObject.setContentType( resourceType );
                    resourcesObject.setUserRole( "everyone" );
                    resourcesObject.setSlideshow( slideshow );
                    featuredResources.add( resourcesObject );
                }
            }
        }
        return featuredResources;
    }

    /**
     * Gets the resource type.
     * 
     * @param resource
     *            the resource
     * @return the resource type
     */
    private String getResourceType( Resource resource ) {

        String comboResourceType = resource.getValueMap().get( RESOURCE_TYPE, StringUtils.EMPTY );
        String[] resourceArr = StringUtils.split( comboResourceType, "-", TWO );

        String resourceType;
        if ( resourceArr.length > 1 ) {
            resourceType = StringUtils.trim( resourceArr[ 0 ] );
        } else {
            resourceType = resource.getValueMap().get( RESOURCE_TYPE, StringUtils.EMPTY );
        }

        return resourceType;
    }

    /**
     * Gets the page data.
     * 
     * @param map
     *            the map
     * @param resource
     *            the resource
     * @param resourceType
     *            the resource type
     * @return the page data
     */
    private Map< String, List< ResourcesObject > > getPageData( Map< String, List< ResourcesObject > > map,
            Resource resource, String resourceType, SlingHttpServletRequest request ) {

        String path = getResourcePath( resource );

        Resource pageResource = CommonUtils.getResource( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE, path );
        Resource jcrContent = pageResource != null ? pageResource.getChild( JcrConstants.JCR_CONTENT ) : null;
        ValueMap properties = jcrContent != null ? jcrContent.getValueMap() : null;
        if ( properties != null ) {
            ResourcesObject resourcesObject = setPageMetadata( properties, jcrContent );
            resourcesObject.setPagePath( InternalURLFormatter.formatURL( CommonUtils
                    .getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ), path ) );
            resourcesObject.setBookmarkPath( path );
            String userRole = RoleUtil.getUserRole( request );
            if ( RoleUtil.shouldRender( userRole, resourcesObject.getUserRole() )
                    || StringUtils.equals( userRole, ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS ) ) {
                if ( WCMMode.fromRequest( request ).equals( WCMMode.EDIT ) ) {
                    resourcesObject.setUserRole( ClassMagsMigrationConstants.USER_TYPE_EVERYONE );
                }
                updateResourcesMap( map, resourcesObject, resourceType );
            }
        }
        return map;
    }

    /**
     * Gets the resource path.
     * 
     * @param resource
     *            the resource
     * @return the resource path
     */
    private String getResourcePath( Resource resource ) {

        String comboResourceType = resource.getValueMap().get( RESOURCE_TYPE, StringUtils.EMPTY );
        String[] resourceArr = StringUtils.split( comboResourceType, "-", TWO );

        String resourcePath;
        if ( resourceArr.length > 1 ) {
            resourcePath = StringUtils.trim( resourceArr[ 1 ] );
        } else {
            resourcePath = resource.getValueMap().get( RESOURCE_PATH, StringUtils.EMPTY );
        }

        return resourcePath;
    }

    /**
     * Sets the page metadata.
     * 
     * @param properties
     *            the properties
     * @param resource
     *            the resource
     * @return the teaching resources
     */
    private ResourcesObject setPageMetadata( ValueMap properties, Resource resource ) {

        ResourcesObject resourcesObject = new ResourcesObject();

        resourcesObject.setTitle( properties.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) );
        resourcesObject.setDescription( properties.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) );
        resourcesObject.setImagePath( properties.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) );
        resourcesObject.setUserRole( properties.get( ClassMagsMigrationConstants.ASSET_USER_TYPE, StringUtils.EMPTY ) );
        TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );
        List< String > tags = CommonUtils.getSubjectTags( resource, tagManager );
        String formattedTags = CommonUtils.getFormattedSubjectTags( tags );
        resourcesObject.setTags( tags );
        resourcesObject.setFormattedTags( formattedTags );

        return resourcesObject;
    }

    /**
     * Gets the asset data.
     * 
     * @param teachingResMap
     *            the teaching res map
     * @param resource
     *            the resource
     * @param resourceType
     *            the resource type
     * @return the asset data
     */
    private Map< String, List< ResourcesObject > > getAssetData( Map< String, List< ResourcesObject > > teachingResMap,
            Resource resource, String resourceType, SlingHttpServletRequest request ) {

        String path = getResourcePath( resource );

        Resource assetResource = CommonUtils.getResource( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE, path );
        Asset asset = assetResource != null ? assetResource.adaptTo( Asset.class ) : null;
        Resource metadataNode = assetResource != null
                ? assetResource.getChild( ClassMagsMigrationConstants.ASSET_METADATA_NODE ) : null;
        if ( asset != null ) {
            ResourcesObject resourcesObject = setAssetMetadata( asset, metadataNode, resource );
            String userRole = RoleUtil.getUserRole( request );
            if ( RoleUtil.shouldRender( userRole, resourcesObject.getUserRole() )
                    || StringUtils.equals( userRole, ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS ) ) {
                if ( WCMMode.fromRequest( request ).equals( WCMMode.EDIT ) ) {
                    resourcesObject.setUserRole( ClassMagsMigrationConstants.USER_TYPE_EVERYONE );
                }
                updateResourcesMap( teachingResMap, resourcesObject, resourceType );
            }
        }
        return teachingResMap;
    }

    /**
     * Sets the asset metadata.
     * 
     * @param asset
     *            the asset
     * @param metadataNode
     *            the metadata node
     * @param resource
     *            the resource
     * @return the teaching resources
     */
    private ResourcesObject setAssetMetadata( Asset asset, Resource metadataNode, Resource resource ) {

        ResourcesObject teachingResources = new ResourcesObject();

        teachingResources.setPagePath( asset.getPath() );
        teachingResources.setBookmarkPath( asset.getPath() );
        teachingResources.setTitle(
                StringUtils.defaultIfBlank( asset.getMetadataValue( DamConstants.DC_TITLE ), asset.getName() ) );
        teachingResources.setDescription( asset.getMetadataValue( DamConstants.DC_DESCRIPTION ) );
        teachingResources.setImagePath( asset.getPath() + ClassMagsMigrationConstants.IMAGE_TILE_RENDITION );
        if ( metadataNode != null && metadataNode.getValueMap().containsKey( ClassMagsMigrationConstants.VIDEO_ID ) ) {
            teachingResources
                    .setVideoId( metadataNode.getValueMap().get( ClassMagsMigrationConstants.VIDEO_ID, Long.class ) );
        }

        TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );
        List< String > tags = CommonUtils.getSubjectTags( metadataNode, tagManager );
        List< String > contentTags = CommonUtils.getTags( metadataNode, tagManager );
        String formattedTags = CommonUtils.getFormattedSubjectTags( tags );
        teachingResources.setTags( tags );
        teachingResources.setFormattedTags( formattedTags );
        if ( metadataNode != null
                && metadataNode.getValueMap().containsKey( ClassMagsMigrationConstants.VIDEO_DURATION ) ) {
            teachingResources.setVideoDuration(
                    metadataNode.getValueMap().get( ClassMagsMigrationConstants.VIDEO_DURATION, StringUtils.EMPTY ) );
        }
        if ( metadataNode != null ) {
            teachingResources.setUserRole(
                    metadataNode.getValueMap().get( ClassMagsMigrationConstants.ASSET_USER_TYPE, StringUtils.EMPTY ) );
        }

        if ( contentTags != null && contentTags.toString().contains( ClassMagsMigrationConstants.GAMES ) ) {

            GameModel gameModel = new GameModel();
            gameModel.setContainerId( CommonUtils.generateUniqueIdentifier() );

            if ( metadataNode != null && metadataNode.getValueMap().get( ClassMagsMigrationConstants.GAME_TYPE,
                    StringUtils.EMPTY ) == StringUtils.EMPTY ) {
                gameModel.setGamePath( asset.getPath().concat( ClassMagsMigrationConstants.HTML_SUFFIX ) );
                gameModel.setGameType( ClassMagsMigrationConstants.GAME_HTML );
            } else {
                String flashvarsValue;

                if ( null != magazineProps && null != resourcePathConfigService && null != propConfigService ) {
                    String magazineName = magazineProps.getMagazineName( resource.getPath() );
                    try {
                        gameModel.setContainerPath( propConfigService.getDataConfigProperty(
                                ClassMagsMigrationConstants.FLASH_GAME_CONTAINER_PROPERTY,
                                CommonUtils.getDataPath( resourcePathConfigService, magazineName ) ) );
                    } catch ( LoginException e ) {
                        LOG.error( "Error in setting flash game container property in getGameContent::::" + e );
                    }

                    if ( metadataNode != null ) {

                        gameModel.setFlashGameType( metadataNode.getValueMap()
                                .get( ClassMagsMigrationConstants.GAME_TYPE, StringUtils.EMPTY ) );
                        gameModel.setXMLPath( metadataNode.getValueMap().get( ClassMagsMigrationConstants.XML_PATH,
                                StringUtils.EMPTY ) );
                    }

                    if ( gameModel.getXMLPath() != null && gameModel.getXMLPath() != StringUtils.EMPTY ) {
                        flashvarsValue = "gametype=" + gameModel.getFlashGameType() + "&contentXML="
                                + gameModel.getXMLPath() + "&assetSWF=" + asset.getPath();
                    } else {
                        flashvarsValue = "gametype=" + gameModel.getFlashGameType() + "&assetSWF=" + asset.getPath();
                    }
                    gameModel.setGamePath( flashvarsValue );
                    gameModel.setGameType( ClassMagsMigrationConstants.GAME_FLASH );
                }
            }
            teachingResources.setGameModel( gameModel );
        }
        return teachingResources;
    }

    /**
     * Update resources map.
     * 
     * @param map
     *            the map
     * @param resourcesObject
     *            the resources object
     * @param resourceType
     *            the resource type
     * @return the map
     */
    private Map< String, List< ResourcesObject > > updateResourcesMap( Map< String, List< ResourcesObject > > map,
            ResourcesObject resourcesObject, String resourceType ) {

        List< ResourcesObject > teachingResourcesList;

        if ( map.containsKey( resourceType ) ) {
            // the resource type is already present
            teachingResourcesList = map.get( resourceType );
            teachingResourcesList.add( resourcesObject );
            map.replace( resourceType, teachingResourcesList );
        } else {
            // add the first data
            teachingResourcesList = new ArrayList< >();
            teachingResourcesList.add( resourcesObject );
            map.put( resourceType, teachingResourcesList );
        }
        return map;
    }
}
