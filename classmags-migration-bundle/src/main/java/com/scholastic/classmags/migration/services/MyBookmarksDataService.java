package com.scholastic.classmags.migration.services;

import java.util.List;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.BookmarkData;
import com.scholastic.classmags.migration.models.ResourcesObject;

/**
 * The Interface MyBookmarksDataService.
 */
public interface MyBookmarksDataService {

    /**
     * Gets the my bookmarks page data.
     *
     * @param allPageBookmarks
     *            the all page bookmarks
     * @param loggedInUserType
     *            the logged in user type
     * @return the my bookmarks page data
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    List< ResourcesObject > getMyBookmarksPageData( List< BookmarkData > allPageBookmarks, String loggedInUserType )
            throws ClassmagsMigrationBaseException;

    /**
     * Gets the my bookmarks asset data.
     *
     * @param allAssetBookmarks
     *            the all asset bookmarks
     * @param loggedInUserType
     *            the logged in user type
     * @return the my bookmarks asset data
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    List< ResourcesObject > getMyBookmarksAssetData( List< BookmarkData > allAssetBookmarks, String loggedInUserType )
            throws ClassmagsMigrationBaseException;
}
