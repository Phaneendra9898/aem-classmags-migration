package com.scholastic.classmags.migration.social.api;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import com.adobe.cq.social.scf.OperationException;



public interface OrderConfirmationService {

	/**
	 * Create a new order from a Sling Servlet Request.
	 * @param request the request that was made to the service that contains all the necessary parameters
	 * @return the newly created resource
	 */
	Resource createOrder(final SlingHttpServletRequest request) throws OperationException;

}
