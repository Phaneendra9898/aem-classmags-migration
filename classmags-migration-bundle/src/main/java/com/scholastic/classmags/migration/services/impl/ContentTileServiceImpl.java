package com.scholastic.classmags.migration.services.impl;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.tagging.TagManager;
import com.scholastic.classmags.migration.models.ContentTile;
import com.scholastic.classmags.migration.models.ContentTileObject;
import com.scholastic.classmags.migration.models.GameModel;
import com.scholastic.classmags.migration.services.ContentTileService;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * The Class ContentTileServiceImpl.
 */
@Component( immediate = true, metatype = false )
@Service( ContentTileService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Content tile service impl class" ) })
public class ContentTileServiceImpl implements ContentTileService {

    /** The constant metadata node name **/
    private static final String METADATA_NODE = "jcr:content/metadata";

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /** The prop config service. */
    @Reference
    PropertyConfigService propConfigService;

    /** The resource path config service. */
    @Reference
    private ResourcePathConfigService resourcePathConfigService;

    /** The magazine props service. */
    @Reference
    private MagazineProps magazineProps;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( ContentTileServiceImpl.class );

    /** The Constant JCR_SORTING_DATE_FORMAT. */
    private static final String JCR_SORTING_DATE_FORMAT = "jcr:sortingDateFormat";

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.services.ContentTileService#
     * getArticleContent(com.scholastic.classmags.migration.models.ContentTile)
     */
    @Override
    public ContentTileObject getArticleContent( ContentTile contentTile, String currentPagePath ) {
        String articlepath = contentTile.getContentTileLink();
        ContentTileObject contentTileObject = new ContentTileObject();

        if ( StringUtils.isBlank( articlepath ) ) {
            return null;
        }

        Resource resource = getResource( articlepath + "/" + JcrConstants.JCR_CONTENT );
        ValueMap articleProps = resource != null ? resource.getValueMap() : null;
        if ( articleProps != null ) {
            contentTileObject.setAssociatedImage(
                    articleProps.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) );
            contentTileObject.setContentTileTitle( articleProps.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) );
            contentTileObject.setContentTileDesc( articleProps.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) );

            TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );
            List< String > tags = CommonUtils.getSubjectTags( resource, tagManager );
            contentTileObject.setTags( CommonUtils.getFormattedSubjectTags( tags ) );

            contentTileObject.setPagePath( InternalURLFormatter.formatURL( CommonUtils.getResourceResolver(
                    resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ), articlepath ) );
            contentTileObject.setBookmarkPath( articlepath );

            String appName = getMagazineName( currentPagePath );
            try {
                contentTileObject.setSortingDate( CommonUtils.getSortingDate( articleProps,
                        propConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT,
                                CommonUtils.getDataPath( resourcePathConfigService, appName ) ) ) );
            } catch ( Exception e ) {
                LOG.error( "Error in setting sorting date in getArticleContent::::" + e );
            }
            return contentTileObject;
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.services.ContentTileService#
     * getIssueContent(com.scholastic.classmags.migration.models.ContentTile)
     */
    @Override
    public ContentTileObject getIssueContent( ContentTile contentTile, String currentPagePath ) {
        String issuepath = contentTile.getContentTileLink();
        ContentTileObject contentTileObject = new ContentTileObject();
        if ( StringUtils.isBlank( issuepath ) ) {
            return null;
        }

        Resource resource = getResource( issuepath + "/" + JcrConstants.JCR_CONTENT );
        ValueMap issuePageProps = resource != null ? resource.getValueMap() : null;
        if ( issuePageProps != null ) {
            contentTileObject.setAssociatedImage(
                    issuePageProps.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) );
            contentTileObject.setContentTileTitle( issuePageProps.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) );
            contentTileObject
                    .setContentTileDesc( issuePageProps.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) );

            TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );
            List< String > tags = CommonUtils.getSubjectTags( resource, tagManager );
            contentTileObject.setTags( CommonUtils.getFormattedSubjectTags( tags ) );

            contentTileObject.setPagePath( InternalURLFormatter.formatURL( CommonUtils.getResourceResolver(
                    resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ), issuepath ) );
            contentTileObject.setBookmarkPath( issuepath );

            String appName = getMagazineName( currentPagePath );
            try {
                contentTileObject.setSortingDate( CommonUtils.getSortingDate( issuePageProps,
                        propConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT,
                                CommonUtils.getDataPath( resourcePathConfigService, appName ) ) ) );
            } catch ( Exception e ) {
                LOG.error( "Error in setting sorting date in getIssueContent::::" + e );
            }
            return contentTileObject;
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.services.ContentTileService#
     * getVideoContent(com.scholastic.classmags.migration.models.ContentTile)
     */
    @Override
    public ContentTileObject getVideoContent( ContentTile contentTile, String currentPagePath ) {
        String videoPath = contentTile.getContentTileLink();
        Resource resource = getResource( videoPath );
        Asset asset = resource != null ? resource.adaptTo( Asset.class ) : null;
        Resource metadataRes = resource != null ? resource.getChild( METADATA_NODE ) : null;
        ContentTileObject contentTileObject = new ContentTileObject();
        if ( asset != null && metadataRes != null ) {
            contentTileObject.setContentTileTitle( asset.getMetadataValue( DamConstants.DC_TITLE ) );
            contentTileObject.setContentTileDesc( asset.getMetadataValue( DamConstants.DC_DESCRIPTION ) );

            TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );
            List< String > tags = CommonUtils.getSubjectTags( metadataRes, tagManager );
            contentTileObject.setTags( CommonUtils.getFormattedSubjectTags( tags ) );

            contentTileObject.setAssociatedImage( asset.getPath() + ClassMagsMigrationConstants.IMAGE_TILE_RENDITION );

            String appName = getMagazineName( currentPagePath );
            try {
                contentTileObject.setSortingDate( CommonUtils.getAssetSortingDate( metadataRes.getValueMap(),
                        propConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT,
                                CommonUtils.getDataPath( resourcePathConfigService, appName ) ) ) );
            } catch ( Exception e ) {
                LOG.error( "Error in setting sorting date in getVideoContent::::" + e );
            }

            if ( StringUtils.isNotBlank( metadataRes.getValueMap().get( "videoDuration", StringUtils.EMPTY ) ) ) {
                contentTileObject
                        .setVideoDuration( metadataRes.getValueMap().get( "videoDuration", StringUtils.EMPTY ) );
            }

            if ( metadataRes.getValueMap().containsKey( "videoId" ) ) {
                contentTileObject.setVideoId( metadataRes.getValueMap().get( "videoId", Long.class ) );
            }

            contentTileObject.setPagePath( asset.getPath() );
            contentTileObject.setBookmarkPath( asset.getPath() );

            return contentTileObject;
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.services.ContentTileService#
     * getAssetContent(com.scholastic.classmags.migration.models.ContentTile)
     */
    @Override
    public ContentTileObject getAssetContent( ContentTile contentTile, String currentPagePath ) {
        String videoPath = contentTile.getContentTileLink();
        Resource resource = getResource( videoPath );
        Asset asset = resource != null ? resource.adaptTo( Asset.class ) : null;
        Resource metadataRes = resource != null ? resource.getChild( METADATA_NODE ) : null;
        ContentTileObject contentTileObject = new ContentTileObject();
        if ( asset != null && metadataRes != null ) {
            contentTileObject.setContentTileTitle(
                    StringUtils.defaultIfBlank( asset.getMetadataValue( DamConstants.DC_TITLE ), asset.getName() ) );
            contentTileObject.setContentTileDesc( asset.getMetadataValue( DamConstants.DC_DESCRIPTION ) );

            TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );
            List< String > tags = CommonUtils.getSubjectTags( metadataRes, tagManager );
            contentTileObject.setTags( CommonUtils.getFormattedSubjectTags( tags ) );

            contentTileObject.setPagePath( InternalURLFormatter.formatURL( CommonUtils.getResourceResolver(
                    resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ), videoPath ) );
            contentTileObject.setBookmarkPath( videoPath );

            String appName = getMagazineName( currentPagePath );
            try {
                contentTileObject.setSortingDate( CommonUtils.getAssetSortingDate( metadataRes.getValueMap(),
                        propConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT,
                                CommonUtils.getDataPath( resourcePathConfigService, appName ) ) ) );
            } catch ( Exception e ) {
                LOG.error( "Error in setting sorting date in getAssetContent::::" + e );
            }
            
            contentTileObject.setUserType( metadataRes.getValueMap().get( "userType", StringUtils.EMPTY ) );
            contentTileObject.setAssociatedImage( asset.getPath() + ClassMagsMigrationConstants.IMAGE_TILE_RENDITION );

            return contentTileObject;
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.services.ContentTileService#
     * getAssetContent(com.scholastic.classmags.migration.models.ContentTile)
     */
    @Override
    public ContentTileObject getGameContent( ContentTile contentTile, String currentPagePath ) {
        String gamePath = contentTile.getContentTileLink();
        Resource resource = getResource( gamePath );

        Asset asset = resource != null ? resource.adaptTo( Asset.class ) : null;
        Resource metadataRes = resource != null ? resource.getChild( METADATA_NODE ) : null;

        ContentTileObject contentTileObject = new ContentTileObject();

        if ( asset != null && metadataRes != null ) {
            contentTileObject.setContentTileTitle(
                    StringUtils.defaultIfBlank( asset.getMetadataValue( DamConstants.DC_TITLE ), asset.getName() ) );
            contentTileObject.setContentTileDesc( asset.getMetadataValue( DamConstants.DC_DESCRIPTION ) );

            TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );
            List< String > tags = CommonUtils.getSubjectTags( metadataRes, tagManager );
            List< String > contentTags = CommonUtils.getTags( metadataRes, tagManager );
            contentTileObject.setTags( CommonUtils.getFormattedSubjectTags( tags ) );

            String appName = getMagazineName( currentPagePath );
            try {
                contentTileObject.setSortingDate( CommonUtils.getAssetSortingDate( metadataRes.getValueMap(),
                        propConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT,
                                CommonUtils.getDataPath( resourcePathConfigService, appName ) ) ) );
            } catch ( Exception e ) {
                LOG.error( "Error in setting sorting date in getGameContent::::" + e );
            }

            contentTileObject.setUserType( metadataRes.getValueMap().get( "userType", StringUtils.EMPTY ) );
            contentTileObject.setAssociatedImage( asset.getPath() + ClassMagsMigrationConstants.IMAGE_TILE_RENDITION );
            contentTileObject.setPagePath( asset.getPath() );
            contentTileObject.setBookmarkPath( asset.getPath() );

            if ( contentTags != null && contentTags.contains( ClassMagsMigrationConstants.GAMES ) ) {
                GameModel gameModel = new GameModel();
                gameModel.setContainerId( CommonUtils.generateUniqueIdentifier() );

                if ( metadataRes != null && metadataRes.getValueMap().get( ClassMagsMigrationConstants.GAME_TYPE,
                        StringUtils.EMPTY ) == StringUtils.EMPTY ) {
                    gameModel.setGamePath( gamePath.concat( ClassMagsMigrationConstants.HTML_SUFFIX ) );
                    gameModel.setGameType( ClassMagsMigrationConstants.GAME_HTML );
                } else {
                    String flashvarsValue;

                    if ( null != magazineProps && null != resourcePathConfigService && null != propConfigService ) {
                        try {
                            gameModel.setContainerPath( propConfigService.getDataConfigProperty(
                                    ClassMagsMigrationConstants.FLASH_GAME_CONTAINER_PROPERTY,
                                    CommonUtils.getDataPath( resourcePathConfigService, appName ) ) );
                        } catch ( LoginException e ) {
                            LOG.error( "Error in setting flash game container property in getGameContent::::" + e );
                        }

                        if ( metadataRes != null ) {
                            gameModel.setFlashGameType( metadataRes.getValueMap()
                                    .get( ClassMagsMigrationConstants.GAME_TYPE, StringUtils.EMPTY ) );
                            gameModel.setXMLPath( metadataRes.getValueMap().get( ClassMagsMigrationConstants.XML_PATH,
                                    StringUtils.EMPTY ) );
                        }

                        if ( gameModel.getXMLPath() != null && gameModel.getXMLPath() != StringUtils.EMPTY ) {
                            flashvarsValue = "gametype=" + gameModel.getFlashGameType() + "&contentXML="
                                    + gameModel.getXMLPath() + "&assetSWF=" + gamePath;
                        } else {
                            flashvarsValue = "gametype=" + gameModel.getFlashGameType() + "&assetSWF=" + gamePath;
                        }
                        gameModel.setGamePath( flashvarsValue );
                        gameModel.setGameType( ClassMagsMigrationConstants.GAME_FLASH );
                    }
                }
                contentTileObject.setGameModel( gameModel );
            }
            return contentTileObject;
        }
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.services.ContentTileService#
     * getCustomContent(com.scholastic.classmags.migration.models.ContentTile)
     */
    @Override
    public ContentTileObject getCustomContent( ContentTile contentTile, String currentPagePath ) {

        ContentTileObject contentTileObject = new ContentTileObject();
        contentTileObject.setContentTileTitle( contentTile.getCustomContentTitle() );

        ResourceResolver resourceResolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );

        SimpleDateFormat dateFormat = new SimpleDateFormat( getDateFormat( currentPagePath ) );

        if ( null != contentTile.getCustomContentDate() ) {
            dateFormat.setCalendar( contentTile.getCustomContentDate() );
            contentTileObject.setSortingDate( dateFormat.format( contentTile.getCustomContentDate().getTime() ) );
        }

        contentTileObject
                .setPagePath( InternalURLFormatter.formatURL( resourceResolver, contentTile.getCustomContentLink() ) );
        contentTileObject.setBookmarkPath( contentTile.getCustomContentLink() );

        contentTileObject.setContentTileDesc( contentTile.getCustomContentDescText() );
        contentTileObject.setAssociatedImage( contentTile.getFileReference() );
        contentTileObject.setCustomContentLink( contentTile.getCustomContentLink() );

        return contentTileObject;
    }

    /**
     * Gets the resource.
     * 
     * @param path
     *            the path
     * @return the resource
     */
    private Resource getResource( String path ) {
        ResourceResolver resourceResolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );
        return resourceResolver.getResource( path );
    }

    private String getDateFormat( String currentPagePath ) {

        String dateFormat = StringUtils.EMPTY;
        try {
            dateFormat = propConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                    CommonUtils.getDataPath( resourcePathConfigService, getMagazineName( currentPagePath ) ) );
            if ( StringUtils.equalsIgnoreCase( "NO DATA AVAILABLE", dateFormat ) ) {
                dateFormat = "MMMM yyyy";
            }
        } catch ( LoginException e ) {
            LOG.error( "Exception occured while retriving date format, e : {}", e );
        }
        return dateFormat;
    }

    private String getMagazineName( String currentPagePath ) {
        return magazineProps.getMagazineName( currentPagePath );
    }
}
