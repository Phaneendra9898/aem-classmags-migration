package com.scholastic.classmags.migration.services.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.tagging.TagManager;
import com.scholastic.classmags.migration.models.ContentTileObject;
import com.scholastic.classmags.migration.models.GameModel;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.services.VideoGameDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

@Component( immediate = true, metatype = false )
@Service( VideoGameDataService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Marketing Sample Issue Data Service" ) })
public class VideoGameDataServiceImpl implements VideoGameDataService{
	
	/** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;
    
    /** The constant metadata node name **/
    private static final String METADATA_NODE = "jcr:content/metadata";
    
    /** The constant metadata node name **/
    private static final String JCR_NODE = "/jcr:content/par/marketing_videogame/assets";

    /** The property config service. */
    @Reference
    private PropertyConfigService propertyConfigService;

    /** The resource path config service. */
    @Reference
    private ResourcePathConfigService resourcePathConfigService;
    
    /** The magazine props service. */
    @Reference
    private MagazineProps magazineProps;

    /** The LOG Constant */
    private static final Logger LOG = LoggerFactory.getLogger( VideoGameDataServiceImpl.class );

    @Override
    public List< ValueMap > fetchAssetData( String currentPath ) {
    	LOG.debug("Page Path in fetch Asset Data" + currentPath);
        Resource resource = CommonUtils
                .getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE )
                .getResource( currentPath.concat( JCR_NODE ) );
        LOG.debug( "Resource in fetch Asset Data" + resource );
        return CommonUtils.fetchMultiFieldData( resource );
    }
    
    @Override
    public ContentTileObject getVideoContent( String videoPath ) {
        Resource resource = getResource( videoPath );
        Asset asset = resource != null ? resource.adaptTo( Asset.class ) : null;
        Resource metadataRes = resource != null ? resource.getChild( METADATA_NODE ) : null;
        ContentTileObject contentTileObject = new ContentTileObject();
        if ( asset != null && metadataRes != null ) {
            contentTileObject.setContentTileTitle( asset.getMetadataValue( DamConstants.DC_TITLE ) );
            contentTileObject.setContentTileDesc( asset.getMetadataValue( DamConstants.DC_DESCRIPTION ) );

            TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );
            List< String > tags = CommonUtils.getSubjectTags( metadataRes, tagManager );
            contentTileObject.setTags( CommonUtils.getFormattedSubjectTags( tags ) );

            contentTileObject.setAssociatedImage( asset.getPath() + ClassMagsMigrationConstants.IMAGE_TILE_RENDITION );

            

            if ( StringUtils.isNotBlank( metadataRes.getValueMap().get( "videoDuration", StringUtils.EMPTY ) ) ) {
                contentTileObject
                        .setVideoDuration( metadataRes.getValueMap().get( "videoDuration", StringUtils.EMPTY ) );
            }

            if ( metadataRes.getValueMap().containsKey( "videoId" ) ) {
                contentTileObject.setVideoId( metadataRes.getValueMap().get( "videoId", Long.class ) );
            }
            
            contentTileObject.setPagePath( asset.getPath() );

            return contentTileObject;
        }
        return contentTileObject;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.services.ContentTileService#
     * getAssetContent(com.scholastic.classmags.migration.models.ContentTile)
     */
    @Override
    public ContentTileObject getAssetContent( String assetPath) {
        Resource resource = getResource( assetPath );
        Asset asset = resource != null ? resource.adaptTo( Asset.class ) : null;
        Resource metadataRes = resource != null ? resource.getChild( METADATA_NODE ) : null;
        ContentTileObject contentTileObject = new ContentTileObject();
        if ( asset != null && metadataRes != null ) {
            contentTileObject.setContentTileTitle(
                    StringUtils.defaultIfBlank( asset.getMetadataValue( DamConstants.DC_TITLE ), asset.getName() ) );
            contentTileObject.setContentTileDesc( asset.getMetadataValue( DamConstants.DC_DESCRIPTION ) );

            TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );
            List< String > tags = CommonUtils.getSubjectTags( metadataRes, tagManager );
            contentTileObject.setTags( CommonUtils.getFormattedSubjectTags( tags ) );

            contentTileObject.setPagePath( InternalURLFormatter.formatURL( CommonUtils.getResourceResolver(
                    resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ), assetPath ) );

            
            contentTileObject.setAssociatedImage( asset.getPath() + ClassMagsMigrationConstants.IMAGE_TILE_RENDITION );

            return contentTileObject;
        }
        return contentTileObject;
    }

    /**
     * Method to fetch HTML5 game data
     *
     * @param gamePath
     *
     * @return
     */
    @Override
    public ContentTileObject getGameContent(String gamePath) {
        LOG.info("In getGameContent Method with path--->"+gamePath);
        Resource resource = getResource( gamePath );

        Asset asset = resource != null ? resource.adaptTo( Asset.class ) : null;
        Resource metadataRes = resource != null ? resource.getChild( METADATA_NODE ) : null;

        ContentTileObject contentTileObject = new ContentTileObject();

        if ( asset != null && metadataRes != null ) {
            contentTileObject.setContentTileTitle(
                    StringUtils.defaultIfBlank( asset.getMetadataValue( DamConstants.DC_TITLE ), asset.getName() ) );
            contentTileObject.setContentTileDesc( asset.getMetadataValue( DamConstants.DC_DESCRIPTION ) );

            contentTileObject.setUserType( metadataRes.getValueMap().get( "userType", StringUtils.EMPTY ) );
            contentTileObject.setAssociatedImage( asset.getPath() + ClassMagsMigrationConstants.IMAGE_TILE_RENDITION );
            contentTileObject.setPagePath( asset.getPath() );
            contentTileObject.setBookmarkPath( asset.getPath() );

            GameModel gameModel = new GameModel();
            gameModel.setContainerId( CommonUtils.generateUniqueIdentifier() );

            if ( metadataRes != null && metadataRes.getValueMap().get( ClassMagsMigrationConstants.GAME_TYPE,
                    StringUtils.EMPTY ) == StringUtils.EMPTY ) {
                gameModel.setGamePath( gamePath.concat( ClassMagsMigrationConstants.HTML_SUFFIX ) );
                gameModel.setGameType( ClassMagsMigrationConstants.GAME_HTML );
            } else {
                String flashvarsValue;

                if ( null != magazineProps && null != resourcePathConfigService && null != propertyConfigService ) {
                    if ( metadataRes != null ) {
                        gameModel.setFlashGameType( metadataRes.getValueMap()
                                .get( ClassMagsMigrationConstants.GAME_TYPE, StringUtils.EMPTY ) );
                        gameModel.setXMLPath( metadataRes.getValueMap().get( ClassMagsMigrationConstants.XML_PATH,
                                StringUtils.EMPTY ) );
                    }

                    if ( gameModel.getXMLPath() != null && gameModel.getXMLPath() != StringUtils.EMPTY ) {
                        flashvarsValue = "gametype=" + gameModel.getFlashGameType() + "&contentXML="
                                + gameModel.getXMLPath() + "&assetSWF=" + gamePath;
                    } else {
                        flashvarsValue = "gametype=" + gameModel.getFlashGameType() + "&assetSWF=" + gamePath;
                    }
                    gameModel.setGamePath( flashvarsValue );
                    gameModel.setGameType( ClassMagsMigrationConstants.GAME_FLASH );
                }
            }
            contentTileObject.setGameModel( gameModel );

            return contentTileObject;
        }
        return contentTileObject;
    }

    /**
     * Gets the resource.
     * 
     * @param path
     *            the path
     * @return the resource
     */
    private Resource getResource( String path ) {
        ResourceResolver resourceResolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );
        return resourceResolver.getResource( path );
    }

}
