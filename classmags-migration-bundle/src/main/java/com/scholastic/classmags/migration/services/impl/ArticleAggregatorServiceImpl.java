package com.scholastic.classmags.migration.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.models.ArticleAggregatorObject;
import com.scholastic.classmags.migration.models.ContentTileFlags;
import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.services.ArticleAggregatorService;
import com.scholastic.classmags.migration.services.TeachingResourcesService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * The Class ArticleAggregatorServiceImpl.
 */
@Component( immediate = true, metatype = false )
@Service( ArticleAggregatorService.class )
@Properties( {
        @Property( name = Constants.SERVICE_DESCRIPTION, value = "Issue article aggregator service impl class" ) } )
public class ArticleAggregatorServiceImpl implements ArticleAggregatorService {

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( ArticleAggregatorServiceImpl.class );

    /** The Constant ARTICLE_CONFIG_RESOURCES. */
    private static final String ARTICLE_CONFIG_RESOURCES = "article-configuration";

    /** The teaching resources service. */
    @Reference
    private TeachingResourcesService teachingResourcesService;

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.scholastic.classmags.migration.services.ArticleAggregatorService#
     * getArticleAggregatorData(com.day.cq.wcm.api.Page)
     */
    @Override
    public List< ArticleAggregatorObject > getArticleAggregatorData( SlingHttpServletRequest request,
            Page currentPage ) {
        LOG.info( "::::In getArticleAggregatorData method of ArticleAggregatorServiceImpl::::" );

        ArticleAggregatorObject articleAggregator;
        List< ArticleAggregatorObject > articleAggregatorList = new ArrayList<>();

        Iterator< Page > iterator = currentPage.listChildren();
        while ( iterator.hasNext() ) {
            articleAggregator = new ArticleAggregatorObject();

            Page page = iterator.next();

            boolean featuredArticleFlag = checkFeaturedArticleFlag( page.getContentResource() );
            String lexileLevels = fetchLexileLevels( page.getContentResource() );
            ResourcesObject resourcesObject = setPageObject( page.getContentResource(), featuredArticleFlag );
            resourcesObject.setPagePath( InternalURLFormatter.formatURL( CommonUtils.getResourceResolver(
                    resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ), page.getPath() ) );
            resourcesObject.setBookmarkPath( page.getPath() );

            Map< String, List< ResourcesObject > > articleResources;
            articleResources = setArticleResources( page, request );
            articleAggregator.setResourcesObject( resourcesObject );
            articleAggregator.setArticleResources( articleResources );
            articleAggregator.setFeaturedArticleFlag( featuredArticleFlag );
            articleAggregator.setArticleLexileLevels( lexileLevels );
            if ( null != articleResources && null != resourcesObject ) {
                setContentTileFlags( articleResources, resourcesObject );
            }
            articleAggregatorList.add( articleAggregator );
        }
        return swapOnBasisOfFeatureArticle( articleAggregatorList );
    }

    /**
     * Sets the content tile flags.
     * 
     * @param articleResources
     *
     * @param resourcesObject
     *            the new content tile flags
     */
    private void setContentTileFlags( Map< String, List< ResourcesObject > > articleResources,
            ResourcesObject resourcesObject ) {

        for ( String teachingResourceTypeKey : articleResources.keySet() ) {
            for ( ResourcesObject articleTeachingResourceObject : articleResources.get( teachingResourceTypeKey ) ) {
                ContentTileFlags contentTileFlags = new ContentTileFlags();
                contentTileFlags.setDownloadEnabled(
                        CommonUtils.isAShareableAsset( articleTeachingResourceObject.getPagePath() ) );
                if ( StringUtils.isNotBlank( resourcesObject.getPagePath() ) ) {
                    contentTileFlags.setViewArticleEnabled( true );
                    articleTeachingResourceObject.setArticlePagePath( resourcesObject.getPagePath() );
                }
                articleTeachingResourceObject.setContentTileFlags( contentTileFlags );
            }
        }
    }

    /**
     * Swap on basis of feature article.
     * 
     * @param articleAggregatorList
     *            the article aggregator list
     * @return the list
     */
    private List< ArticleAggregatorObject > swapOnBasisOfFeatureArticle(
            List< ArticleAggregatorObject > articleAggregatorList ) {

        for ( ArticleAggregatorObject articleAggregatorObject : articleAggregatorList ) {
            if ( articleAggregatorObject.isFeaturedArticleFlag() ) {
                Collections.swap( articleAggregatorList, 0, articleAggregatorList.indexOf( articleAggregatorObject ) );
            }
        }
        return articleAggregatorList;
    }

    /**
     * Check featured article flag.
     * 
     * @param contentResource
     *            the content resource
     * @return true, if successful
     */
    private boolean checkFeaturedArticleFlag( Resource contentResource ) {

        Resource articleConfiguration = contentResource.getChild( ARTICLE_CONFIG_RESOURCES );
        return ( null != articleConfiguration && StringUtils.isNotEmpty( articleConfiguration.getValueMap()
                .get( ClassMagsMigrationConstants.FEATURED_ARTICLE_FLAG, StringUtils.EMPTY ) ) ) ? true : false;
    }

    /**
     * Get the lexile levels.
     * 
     * @param contentResource
     *            the content resource
     * @return the String, lexile levels
     */
    private String fetchLexileLevels( Resource contentResource ) {

        StringBuilder lexileLevels = new StringBuilder();

        Resource articleConfiguration = contentResource.getChild( ARTICLE_CONFIG_RESOURCES );

        if ( null != articleConfiguration ) {
            Resource lexileSettings = articleConfiguration.getChild( "lexileSettings" );
            if ( lexileSettings != null ) {
                Iterator< Resource > iterator = lexileSettings.listChildren();
                while ( iterator.hasNext() ) {
                    Resource lexileLevel = iterator.next();
                    lexileLevels.append( lexileLevel.getValueMap().get( "readingLevel", StringUtils.EMPTY ) );
                    if ( iterator.hasNext() ) {
                        lexileLevels.append( ", " );

                    }
                }
            }
        }

        return lexileLevels.toString();
    }

    /**
     * Sets the article resources.
     *
     * @param page
     *            the page
     * @param request
     *            the request
     * @return the map
     */
    private Map< String, List< ResourcesObject > > setArticleResources( Page page, SlingHttpServletRequest request ) {
        LOG.info( "::::In setArticleResources method of ArticleAggregatorServiceImpl::::" );

        Resource jcrContent = page.getContentResource();
        Resource articleConfiguration = jcrContent.getChild( ARTICLE_CONFIG_RESOURCES );
        if ( articleConfiguration != null ) {
            Map< String, List< ResourcesObject > > teachingResources = teachingResourcesService
                    .getTeachingResources( request, articleConfiguration, "resources" );
            Map< String, List< ResourcesObject > > customResources = teachingResourcesService
                    .getTeachingResources( request, articleConfiguration, "custom-resources" );
            if ( null != customResources && !customResources.isEmpty() ) {
                teachingResources = teachingResourcesService.createResourcesMap( teachingResources, customResources );
            }
            return teachingResources;
        }
        return null;
    }

    /**
     * Sets the page object.
     * 
     * @param resource
     *            the resource
     * @param featuredArticleFlag
     *            the featured article flag
     * @return the resources object
     */
    private ResourcesObject setPageObject( Resource resource, boolean featuredArticleFlag ) {
        LOG.info( "::::In setPageObject method of ArticleAggregatorServiceImpl::::" );

        ResourcesObject pageObject = new ResourcesObject();

        ValueMap pageProps = resource.getValueMap();

        pageObject.setTitle( pageProps.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) );
        pageObject.setDescription( pageProps.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) );
        TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );
        List< String > tags = CommonUtils.getSubjectTags( resource, tagManager );
        pageObject.setTags( tags );
        if ( featuredArticleFlag ) {
            pageObject.setImagePath( getArticleHeroImage( resource ) );
        } else {
            pageObject.setImagePath( pageProps.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) );
        }
        return pageObject;
    }

    /**
     * Gets the article hero image.
     * 
     * @param resource
     *            the resource
     * @return the article hero image
     */
    private String getArticleHeroImage( Resource resource ) {

        Resource heroImageResource = resource.getChild( ClassMagsMigrationConstants.HERO_IMAGE_NODE );
        return null != heroImageResource ? heroImageResource.getValueMap()
                .get( ClassMagsMigrationConstants.HERO_IMAGE_FILE_REFERENCE, StringUtils.EMPTY ) : StringUtils.EMPTY;
    }
}
