package com.scholastic.classmags.migration.services.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.framework.Constants;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.scholastic.classmags.migration.services.ArticleTextDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * Article Data Service.
 */
@Component( immediate = true, metatype = false )
@Service( ArticleTextDataService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Article Text Data Service" ) })
public class ArticleTextDataServiceImpl implements ArticleTextDataService {

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /** The query builder. */
    @Reference
    private QueryBuilder queryBuilder;

    /**
     * Sets the resource resolver factory.
     *
     * @param resourceResolverFactory
     *            the resourceResolverFactory to set
     */
    public void setResourceResolverFactory( ResourceResolverFactory resourceResolverFactory ) {
        this.resourceResolverFactory = resourceResolverFactory;
    }

    /**
     * Sets the query builder.
     *
     * @param queryBuilder
     *            the queryBuilder to set
     */
    public void setQueryBuilder( QueryBuilder queryBuilder ) {
        this.queryBuilder = queryBuilder;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.services.ArticleTextDataService#
     * fetchArticleData(java.lang.String)
     */
    @Override
    public List< String > fetchArticleData( String currentPath ) throws RepositoryException {

        ResourceResolver resourceResolver;
        Session session;
        List< String > articleDataList = new ArrayList< >();
        Map< String, String > queryMap = new HashMap< >();

        resourceResolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );
        session = resourceResolver.adaptTo( Session.class );

        queryMap.put( ClassMagsMigrationConstants.QUERY_PATH, currentPath );
        queryMap.put( ClassMagsMigrationConstants.RESULTS_LIMIT, ClassMagsMigrationConstants.QUERY_LOWER_BOUND );
        queryMap.put( ClassMagsMigrationConstants.QUERY_ORDER_BY, ClassMagsMigrationConstants.QUERY_NODE_NAME );
        Query query = queryBuilder.createQuery( PredicateGroup.create( queryMap ), session );

        SearchResult searchResult = query.getResult();
        List< Hit > articleLexileLevels = searchResult.getHits();
        if ( null != articleLexileLevels && !articleLexileLevels.isEmpty() ) {
            traverseLexileLevels( articleDataList, articleLexileLevels );
        }
        return articleDataList;
    }

    /**
     * Traverse lexile levels.
     *
     * @param articleDataList
     *            the article data list
     * @param articleLexileLevels
     *            the article lexile levels
     * @throws RepositoryException
     *             the repository exception
     */
    private void traverseLexileLevels( List< String > articleDataList, List< Hit > articleLexileLevels )
            throws RepositoryException {
        Node node;
        Value[] articleData;
        for ( Hit hit : articleLexileLevels ) {
            node = hit.getNode();
            if ( node.hasProperty( ClassMagsMigrationConstants.ARTICLE_TEXT_TITLE ) ) {
                articleData = node.getProperty( ClassMagsMigrationConstants.ARTICLE_TEXT_TITLE ).getValues();
                if ( null != articleData && null != articleData[ 1 ] ) {
                    articleDataList.add( articleData[ 1 ].getString() );
                }
            }
        }
    }
}
