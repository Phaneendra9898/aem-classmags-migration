package com.scholastic.classmags.migration.controllers;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.commons.json.JSONObject;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.scholastic.classmags.migration.services.ScholasticDataLayerService;

/*
 * Use class for Scholastic data layer
 */
public class ScholasticDataLayerUse extends WCMUsePojo {

    /**
     * The digital data.
     */
    private JSONObject digitalData;

    @Override
    public final void activate() {
        SlingHttpServletRequest request = getRequest();
        digitalData = new JSONObject();

        Resource currentResource = request.getResource();
        PageManager currentPageManager = currentResource.getResourceResolver().adaptTo( PageManager.class );
        if ( null != currentPageManager ) {
            Page currentPage = currentPageManager.getContainingPage( currentResource );

            if ( null != currentPage ) {
                ScholasticDataLayerService dataLayerService = getSlingScriptHelper()
                        .getService( ScholasticDataLayerService.class );
                if ( null != dataLayerService ) {
                    digitalData = dataLayerService.createDataLayer( request, currentPage );
                }
            }
        }
    }

    /**
     * Gets the digital data.
     * 
     * @return the digitalData
     */
    public String getDigitalData() {
        return digitalData.toString();
    }

}
