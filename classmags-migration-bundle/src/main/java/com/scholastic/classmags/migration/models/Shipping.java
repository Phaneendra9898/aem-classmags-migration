package com.scholastic.classmags.migration.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shipping {

	@SerializedName("name")
	@Expose
	private String name;
	@SerializedName("email")
	@Expose
	private String email;
	@SerializedName("address1")
	@Expose
	private String address1;
	@SerializedName("address2")
	@Expose
	private String address2;
	@SerializedName("city")
	@Expose
	private String city;
	@SerializedName("zip")
	@Expose
	private String zip;
	@SerializedName("phone")
	@Expose
	private String phone;
	@SerializedName("school")
	@Expose
	private String school;
	@SerializedName("state")
	@Expose
	private String state;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

}