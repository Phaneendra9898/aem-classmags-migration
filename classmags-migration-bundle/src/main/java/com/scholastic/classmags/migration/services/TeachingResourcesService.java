package com.scholastic.classmags.migration.services;

import java.util.List;
import java.util.Map;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.commons.json.JSONException;

import com.scholastic.classmags.migration.models.ResourcesObject;

/**
 * The Interface TeachingResourcesService.
 */
public interface TeachingResourcesService {

    public List< ResourcesObject > getFeaturedResource( SlingHttpServletRequest request, Resource resource ) throws JSONException;
    /**
     * Gets the issue teaching resources.
     * 
     * @param resource the resource
     * @param nodeName the node name
     * @param appName the app name
     * 
     * @return the issue teaching resources
     */
    Map< String, List< ResourcesObject >> getTeachingResources( SlingHttpServletRequest request, Resource resource, String nodeName ) ;
    /**
     * Gets the teaching resources.
     * 
     * @param issueResources the issue resources
     * @param customResources the custom resources
     * 
     * @return the teaching resources
     */
    Map< String, List< ResourcesObject >> createResourcesMap( Map<String, List<ResourcesObject>> issueResources, Map<String, List<ResourcesObject>> customResources );
}
