package com.scholastic.classmags.migration.controllers;

import org.apache.sling.api.SlingHttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.models.CTAButton;

/**
 * Use Class for CTAButton Editor.
 */
public class CTAButtonUse extends WCMUsePojo {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( CTAButtonUse.class );

    /** The cta button. */
    private CTAButton ctaButton;

    /**
     * Sets the cta button.
     *
     * @param ctaButton
     *            the ctaButton to set
     */
    public void setCtaButton( CTAButton ctaButton ) {
        this.ctaButton = ctaButton;
    }

    /**
     * Gets the cta button.
     *
     * @return the ctaButton
     */
    public CTAButton getCtaButton() {
        return ctaButton;
    }

    /**
     * Activate.
     *
     * @return the cTAButton
     * @throws Exception
     *             the exception
     */

    @Override
    public void activate() throws Exception {
        LOG.info( "Inside the Activate Method for CTAButtonUse" );
        SlingHttpServletRequest request = getRequest();
        LOG.info( "Request Details: " + request );
        if ( request != null ) {
            ctaButton = request.adaptTo( CTAButton.class );
            LOG.info( "CTAButton Details: " + ctaButton );
        }
    }
}
