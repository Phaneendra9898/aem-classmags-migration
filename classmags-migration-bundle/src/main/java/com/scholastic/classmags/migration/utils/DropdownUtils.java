package com.scholastic.classmags.migration.utils;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;

import com.adobe.granite.ui.components.Value;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

/**
 * The Class DropdownUtils.
 */
public final class DropdownUtils {

    /**
     * Instantiates a new dropdown utils.
     */
    private DropdownUtils() {
    }

    /**
     * Current content page.
     *
     * @param request The request
     * @return Current content page or null
     */
    public static Page getContentPage( SlingHttpServletRequest request ) {
        SlingHttpServletRequest slingRequest = request;
        Resource contentResource = getContentResource( request );
        if ( contentResource != null ) {
            PageManager pageManager = slingRequest.getResourceResolver().adaptTo( PageManager.class );
            return null != pageManager ? pageManager.getContainingPage( contentResource ) : null;
        } else {
            return null;
        }
    }

    /**
     * Gets the content resource.
     *
     * @param request
     *            the request
     * @return the content resource
     */
    private static Resource getContentResource( SlingHttpServletRequest request ) {
        SlingHttpServletRequest slingRequest = request;
        String contentPath = ( String ) request.getAttribute( Value.CONTENTPATH_ATTRIBUTE );
        if ( contentPath != null ) {
            return slingRequest.getResourceResolver().getResource( contentPath );
        }
        contentPath = request.getRequestPathInfo().getSuffix();
        if ( null != contentPath && StringUtils.isNotEmpty( contentPath ) ) {
            return slingRequest.getResourceResolver().getResource( contentPath );
        }
        return null;
    }
}
