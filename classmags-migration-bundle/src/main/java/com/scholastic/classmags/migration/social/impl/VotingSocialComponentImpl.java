package com.scholastic.classmags.migration.social.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.apache.sling.xss.XSSAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.social.scf.ClientUtilities;
import com.adobe.cq.social.scf.core.BaseSocialComponent;
import com.adobe.cq.social.srp.SocialResourceProvider;
import com.adobe.cq.social.ugc.api.SearchResults;
import com.adobe.cq.social.ugc.api.UgcFilter;
import com.adobe.cq.social.ugc.api.UgcSearch;
import com.adobe.cq.social.ugc.api.ValueConstraint;
import com.adobe.cq.social.ugcbase.SocialUtils;
import com.day.cq.commons.jcr.JcrUtil;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.ClassmagsMigrationErrorCodes;
import com.scholastic.classmags.migration.social.api.VotingSocialComponent;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;

/**
 * The Class VotingSocialComponentImpl.
 */
@SuppressWarnings( "deprecation" )
public class VotingSocialComponentImpl extends BaseSocialComponent implements VotingSocialComponent {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( VotingSocialComponentImpl.class );

    /** The ugc search. */
    private UgcSearch ugcSearch;

    /** The question UUID. */
    private String questionUUID;

    /** The social utils. */
    private SocialUtils socialUtils;

    /** The answer options map. */
    private Map< String, String > answerOptionsMap = new HashMap<>();

    /** The voting enabled. */
    private boolean votingEnabled;

    /**
     * Instantiates a new voting social component impl.
     *
     * @param resource
     *            the resource
     * @param clientUtils
     *            the client utils
     * @param ugcSearch
     *            the ugc search
     * @param xssapi
     *            the xssapi
     * @param socialUtils
     *            the social utils
     */
    public VotingSocialComponentImpl( Resource resource, ClientUtilities clientUtils, UgcSearch ugcSearch,
            XSSAPI xssapi, SocialUtils socialUtils ) {

        super( resource, clientUtils );
        this.ugcSearch = ugcSearch;
        this.socialUtils = socialUtils;

        final String operation = xssapi
                .filterHTML( this.clientUtils.getRequest().getParameter( ClassMagsMigrationASRPConstants.OPERATION ) );

        try {
            JSONObject jsonObjVoteData = new JSONObject(
                    this.clientUtils.getRequest().getParameter( ClassMagsMigrationASRPConstants.VOTE_DATA ) );

            switch ( operation ) {
            case ClassMagsMigrationASRPConstants.SOCIAL_GET_VOTE_DATA_OPERATION:
                populateVotingData( resource );
                setVotingEnabled( checkVotingEnabled(
                        jsonObjVoteData.optString( ClassMagsMigrationASRPConstants.JSON_KEY_DISABLE_VOTING_DATE ) ) );
                break;
            case ClassMagsMigrationASRPConstants.SOCIAL_SUBMIT_VOTE_OPERATION:
                populateVotingData( resource );
                break;
            default:
                break;
            }
        } catch ( ClassmagsMigrationBaseException e ) {
            LOG.error( e.getMessage(), e );
        } catch ( JSONException e ) {
            LOG.error( ClassmagsMigrationErrorCodes.JSON_CONVERSION_ERROR.getErrorDescription(), e );
        }
    }

    /**
     * Check voting enabled.
     *
     * @param disableVotingDate
     *            the disable voting date
     * @return true, if successful
     */
    private boolean checkVotingEnabled( String disableVotingDate ) {
        return StringUtils.isNotBlank( disableVotingDate ) ? new Date().getTime() < Long.parseLong( disableVotingDate )
                : true;
    }

    /**
     * Populate voting data.
     *
     * @param resource
     *            the resource
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private void populateVotingData( Resource resource ) throws ClassmagsMigrationBaseException {

        final long startTime = System.currentTimeMillis();

        String answerOptionNodeName = null;
        int searchOffset = ClassMagsMigrationASRPConstants.SOCIAL_SEARCH_OFFSET;
        int searchLimit = ClassMagsMigrationASRPConstants.SOCIAL_SEARCH_INTIAL_LIMIT;

        ValueMap propertyMap = resource.getValueMap();
        Object[] answerOptions = ( Object[] ) propertyMap
                .get( ClassMagsMigrationASRPConstants.JSON_KEY_ANSWER_OPTIONS );
        setQuestionUUID( ClassMagsMigrationASRPConstants.RESOURCE_TYPE_SOCIAL.equals( resource.getResourceType() )
                || ( ClassMagsMigrationASRPConstants.RESOURCE_TYPE_CLOUD_SOCIAL.equals( resource.getResourceType() ) )
                        ? resource.getName() : StringUtils.EMPTY );

        if ( null != answerOptions ) {
            for ( Object answerOption : answerOptions ) {
                answerOptionNodeName = JcrUtil.createValidName( ( String ) answerOption );
                populateAnswerOptions( resource, answerOptionNodeName, searchOffset, searchLimit,
                        ( String ) answerOption );
            }
        }

        LOG.debug( CommonUtils.addDelimiter( "Method VotingSocialComponentImpl.populateVotingData TimeMillis: "
                + ( System.currentTimeMillis() - startTime ) ) );
    }

    /**
     * Populate answer options.
     *
     * @param resource
     *            the resource
     * @param answerOption
     *            the answer option
     * @param searchOffset
     *            the search offset
     * @param searchLimit
     *            the search limit
     * @param answerOption2
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private void populateAnswerOptions( Resource resource, String answerOptionNodeName, int searchOffset,
            int searchLimit, String answerOption ) throws ClassmagsMigrationBaseException {

        final long startTime = System.currentTimeMillis();

        SearchResults< Resource > results = null;
        final UgcFilter filter = new UgcFilter();

        SocialResourceProvider srp = socialUtils.getSocialResourceProvider( resource );
        srp.setConfig( socialUtils.getDefaultStorageConfig() );
        String ugcResourcePath = resource.getPath().concat( ClassMagsMigrationConstants.FORWARD_SLASH )
                .concat( answerOptionNodeName );
        Resource ugcResource = SocialASRPUtils.getUGCResourceFromPath( resource.getResourceResolver(), ugcResourcePath,
                srp );

        filter.addConstraint( new ValueConstraint< String >( JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY,
                VotingSocialComponent.VOTING_RESOURCE_TYPE ) );
        filter.addConstraint(
                new ValueConstraint< String >( ClassMagsMigrationASRPConstants.JSON_KEY_QUESTION_UUID, questionUUID ) );
        filter.addConstraint( new ValueConstraint< String >( SocialUtils.PN_PARENTID, ugcResource.getPath() ) );

        try {
            results = this.ugcSearch.find( null, ugcResource.getResourceResolver(), filter, searchOffset, searchLimit,
                    true );
            answerOptionsMap.put( answerOption, String.valueOf( results.getTotalNumberOfResults() ) );

        } catch ( RepositoryException e ) {
            throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.REPOSITORY_ERROR, e );
        }

        LOG.debug( CommonUtils.addDelimiter( "Method VotingSocialComponentImpl.populateAnswerOptions TimeMillis: "
                + ( System.currentTimeMillis() - startTime ) ) );
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.social.api.VotingSocialComponent#
     * getQuestionUUID()
     */
    @Override
    public String getQuestionUUID() {
        return questionUUID;
    }

    /**
     * Sets the question UUID.
     *
     * @param questionUUID
     *            the questionUUID to set
     */
    public void setQuestionUUID( String questionUUID ) {
        this.questionUUID = questionUUID;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.social.api.VotingSocialComponent#
     * getAnswerOptionsMap()
     */
    @Override
    public Map< String, String > getAnswerOptionsMap() {
        return answerOptionsMap;
    }

    /**
     * Checks if is voting enabled.
     *
     * @return the votingEnabled
     */
    @Override
    public boolean isVotingEnabled() {
        return votingEnabled;
    }

    /**
     * Sets the voting enabled.
     *
     * @param votingEnabled
     *            the votingEnabled to set
     */
    public void setVotingEnabled( boolean votingEnabled ) {
        this.votingEnabled = votingEnabled;
    }
}
