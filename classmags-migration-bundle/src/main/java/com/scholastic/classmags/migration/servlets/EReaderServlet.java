package com.scholastic.classmags.migration.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.services.GetEpubService;

/**
 * Servlet used to get the page details for ereader from DAM
 * 
 */
@SlingServlet( paths = { "/bin/classmags/ereader" } )
public class EReaderServlet extends SlingAllMethodsServlet {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 149687106615011432L;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( EReaderServlet.class );

    /** The get Epub service. */
    @Reference
    private GetEpubService getEpubService;

    @Override
    protected void doGet( SlingHttpServletRequest request, SlingHttpServletResponse response ) throws IOException {
        LOG.info( ":::: Inside doPost method of EReaderServlet ::::" );

        List< String > pageIds = new ArrayList< >();

        String eReaderPath = request.getParameter( "eReaderPath" );
        JSONObject pages = new JSONObject();

        try {
            if ( StringUtils.isNotBlank( eReaderPath ) ) {
                pageIds = getEpubService.getPageIds( eReaderPath );
            }
            pages.put( "pages", pageIds );
        } catch ( ClassmagsMigrationBaseException | JSONException e ) {
            LOG.error( "Error getting eReader page details {}", e );
            response.sendError( SlingHttpServletResponse.SC_BAD_REQUEST );
        } finally {
            // Write the response
            response.setContentType( "application/json" );
            response.getWriter().write( pages.toString() );
        }
    }
}
