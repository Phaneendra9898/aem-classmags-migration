package com.scholastic.classmags.migration.controllers;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * The Class GlobalNavContainerUse.
 */
public class GlobalNavContainerUse extends WCMUsePojo {

    /** The home page logged in. */
    private String homePageLoggedIn;

    /** The home page logged out. */
    private String homePageLoggedOut;

    /** The sdm create account url. */
    private String sdmCreateAccountUrl;

    /** The sdm teacher login url. */
    private String sdmTeacherLoginUrl;

    /** The sdm student login url. */
    private String sdmStudentLoginUrl;

    /** The sdm log out url. */
    private String sdmLogOutUrl;

    /** The search results url. */
    private String searchResultsUrl;

    /** The my bookmarks page url. */
    private String myBookmarksPageUrl;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( GlobalNavContainerUse.class );

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() throws LoginException {
        LOG.debug( "Inside activate" );
        String magazineName = StringUtils.EMPTY;
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        if ( null != slingScriptHelper ) {
            PropertyConfigService propertyConfigService = slingScriptHelper.getService( PropertyConfigService.class );
            ResourcePathConfigService resourcePathConfigService = slingScriptHelper
                    .getService( ResourcePathConfigService.class );
            MagazineProps magazineProps = slingScriptHelper.getService( MagazineProps.class );
            if ( null != magazineProps ) {
                Page currentPage = getCurrentPage();
                if ( null != currentPage ) {
                    magazineName = magazineProps.getMagazineName( currentPage.getPath() );
                }
            }

            if ( propertyConfigService != null && resourcePathConfigService != null ) {

                homePageLoggedOut = propertyConfigService.getDataConfigProperty(
                        ClassMagsMigrationConstants.HOMEPAGE_LOGGEDOUT,
                        CommonUtils.getDataPath( resourcePathConfigService, magazineName ) );
                homePageLoggedOut = InternalURLFormatter.formatURL( getResourceResolver(), homePageLoggedOut );
                LOG.debug( "Home page logged out  {}", homePageLoggedOut );
                homePageLoggedIn = propertyConfigService.getDataConfigProperty(
                        ClassMagsMigrationConstants.HOMEPAGE_LOGGEDIN,
                        CommonUtils.getDataPath( resourcePathConfigService, magazineName ) );
                homePageLoggedIn = InternalURLFormatter.formatURL( getResourceResolver(), homePageLoggedIn );
                LOG.debug( "Home page logged in :: {}", homePageLoggedIn );

                sdmLogOutUrl = propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.SDM_LOGOUT,
                        CommonUtils.getDataPath( resourcePathConfigService, magazineName ) );
                LOG.debug( "sdmLogOutUrl :: {}", sdmLogOutUrl );

                sdmCreateAccountUrl = propertyConfigService.getDataConfigProperty(
                        ClassMagsMigrationConstants.SDM_CREATE_AC,
                        CommonUtils.getDataPath( resourcePathConfigService, magazineName ) );
                LOG.debug( "sdmCreateAccountUrl : {}", sdmCreateAccountUrl );

                sdmTeacherLoginUrl = propertyConfigService.getDataConfigProperty(
                        ClassMagsMigrationConstants.SDM_TCH_LOGIN,
                        CommonUtils.getDataPath( resourcePathConfigService, magazineName ) );
                LOG.debug( "sdmTeacherLoginUrl :{}", sdmTeacherLoginUrl );

                sdmStudentLoginUrl = propertyConfigService.getDataConfigProperty(
                        ClassMagsMigrationConstants.SDM_STU_LOGIN,
                        CommonUtils.getDataPath( resourcePathConfigService, magazineName ) );
                LOG.debug( "sdmStudentLoginUrl :{}", sdmStudentLoginUrl );

                searchResultsUrl = propertyConfigService.getDataConfigProperty(
                        ClassMagsMigrationConstants.SEARCH_RESULTS,
                        CommonUtils.getDataPath( resourcePathConfigService, magazineName ) );
                if ( StringUtils.isNotBlank( searchResultsUrl ) ) {
                    searchResultsUrl = InternalURLFormatter.formatURL( getResourceResolver(), searchResultsUrl );
                }
                LOG.debug( "Search Results URL: {}", searchResultsUrl );

                setMyBookmarksPageUrl(
                        propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.MY_BOOKMARKS_PAGE_URL,
                                CommonUtils.getDataPath( resourcePathConfigService, magazineName ) ) );
                if ( StringUtils.isNotBlank( myBookmarksPageUrl ) ) {
                    myBookmarksPageUrl = InternalURLFormatter.formatURL( getResourceResolver(), myBookmarksPageUrl );
                }
                LOG.debug( "My Bookmarks Page URL: {}", myBookmarksPageUrl );

            }

        }
    }

    /**
     * Gets the logged in home page .
     *
     * @return the logged in home page
     */
    public final String getHomePageLoggedIn() {
        return homePageLoggedIn;
    }

    /**
     * Gets logged out home page.
     *
     * @return the logged out home page.
     */
    public final String getHomePageLoggedOut() {
        return homePageLoggedOut;
    }

    /**
     * Gets the SDM create account URL.
     *
     * @return the SDM create account URL.
     */
    public String getSdmCreateAccountUrl() {
        return sdmCreateAccountUrl;
    }

    /**
     * Gets the SDM teacher login URL.
     *
     * @return the SDM teacher login URL.
     */
    public String getSdmTeacherLoginUrl() {
        return sdmTeacherLoginUrl;
    }

    /**
     * Gets the SDM student login URL.
     *
     * @return the SDM student login URL.
     */
    public String getSdmStudentLoginUrl() {
        return sdmStudentLoginUrl;
    }

    /**
     * Gets the SDM logout URL.
     *
     * @return the SDM logout URL.
     */
    public String getSdmLogOutUrl() {
        return sdmLogOutUrl;
    }

    /**
     * Gets the search results URL.
     *
     * @return the search results URL.
     */
    public String getSearchResultsUrl() {
        return searchResultsUrl;
    }

    /**
     * Gets the my bookmarks page url.
     *
     * @return the myBookmarksPageUrl
     */
    public String getMyBookmarksPageUrl() {
        return myBookmarksPageUrl;
    }

    /**
     * Sets the my bookmarks page url.
     *
     * @param myBookmarksPageUrl
     *            the myBookmarksPageUrl to set
     */
    public void setMyBookmarksPageUrl( String myBookmarksPageUrl ) {
        this.myBookmarksPageUrl = myBookmarksPageUrl;
    }

}
