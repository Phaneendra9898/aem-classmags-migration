package com.scholastic.classmags.migration.services;

import org.apache.sling.api.SlingHttpServletRequest;

/**
 * The Interface GetMetadataService.
 */
public interface GetMetadataService {

    /**
     * Gets the property from DAM.
     *
     */
    String getDAMProperty(SlingHttpServletRequest request, String key, String path);
    
    /**
     * Gets the property from Page.
     *
     */
    String getPageProperty(SlingHttpServletRequest request, String key, String path);
}
