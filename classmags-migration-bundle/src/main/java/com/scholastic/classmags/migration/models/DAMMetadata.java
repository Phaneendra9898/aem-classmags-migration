package com.scholastic.classmags.migration.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.OSGiService;
import org.apache.sling.models.annotations.injectorspecific.Self;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.tagging.TagManager;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

@Model( adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL )
public class DAMMetadata {

    private static final Logger log = LoggerFactory.getLogger( DAMMetadata.class );

    @Inject
    private ResourceResolver resourceResolver;

    /** The property config service. */
    @OSGiService
    private PropertyConfigService propertyConfigService;

    /** The resource path config service. */
    @OSGiService
    private ResourcePathConfigService resourcePathConfigService;

    private String id;

    @Inject
    @Named( "dc:title" )
    private String title;
    
    @Inject
    @Named( "dc:title" )
    private String searchTitle;

    @Inject
    @Named( "dc:description" )
    private String description;

    @Inject
    @Named( "cq:tags" )
    private String[] cqTags;
    
    @Inject
    @Named( "keywords" )
    private String keyWords;

    @Inject
    @Named( "videoId" )
    private String videoId;
    
    @Inject
    @Named( "userType" )
    private String userType;
    
    @Inject
    @Named( "gameType" )
    private String gameType;
    
    @Inject
    @Named( "xmlPath" )
    private String xmlPath;
    
    @Inject
    @Named( "videoDuration" )
    private String videoDuration;
    
    @Self
    private Resource resource;

    private List< String > tags;
    
    private Set< String > subjectTags;

    private String publishedDate;

    private String articlePath;

    private String thumbnailPath;
    
    private String type;
    
    private Date sortDate;
    
    private String siteId;
    
    private String shareUrl;
    
    private boolean downloadable;
    
    private boolean shareable;

    private String gamePath;
    
    private String flashContainer;
    
    @PostConstruct
    public final void init() {

        if(StringUtils.isNotBlank( searchTitle )){
            this.searchTitle = searchTitle.toLowerCase();
        }
        // add user friendly tags to index
        addTags();
        
    }

    private void addTags() {
        this.tags = CommonUtils.getTags( resource, resourceResolver.adaptTo( TagManager.class ) );
        this.subjectTags = CommonUtils.getAllSubjectTags( resource, resourceResolver.adaptTo( TagManager.class ) );
    }
    
    public String xmlPath() {
        return getXmlPath();
    }
    
    public String gameType() {
        return getGameType();
    }

    public String getId() {
        return id;
    }

    public void setId( String id ) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle( String title ) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription( String description ) {
        this.description = description;
    }

    public List< String > getTags() {
        return tags;
    }

    public void setTags( List< String > tags ) {
        this.tags = tags;
    }

    public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate( String publishedDate ) {
        this.publishedDate = publishedDate;
    }

    public String getArticlePath() {
        return articlePath;
    }

    public void setArticlePath( String articlePath ) {
        this.articlePath = articlePath;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath( String thumbnailPath ) {
        this.thumbnailPath = thumbnailPath;
    }

    public String getType() {
        return type;
    }

    public void setType( String type ) {
        this.type = type;
    }

    public String getKeyWords() {
        return keyWords;
    }

    public void setKeyWords( String keyWords ) {
        this.keyWords = keyWords;
    }

    public Date getSortDate() {
        return sortDate;
    }

    public void setSortDate( Date sortDate ) {
        this.sortDate = sortDate;
    }

    public String getSiteId() {
        return siteId;
    }

    public void setSiteId( String siteId ) {
        this.siteId = siteId;
    }

    public Set< String > getSubjectTags() {
        return subjectTags;
    }

    public void setSubjectTags( Set< String > subjectTags ) {
        this.subjectTags = subjectTags;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId( String videoId ) {
        this.videoId = videoId;
    }

    public String getSearchTitle() {
        return searchTitle;
    }

    public void setSearchTitle( String searchTitle ) {
        this.searchTitle = searchTitle;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType( String userType ) {
        this.userType = userType;
    }

    public String getShareUrl() {
        return shareUrl;
    }

    public void setShareUrl( String shareUrl ) {
        this.shareUrl = shareUrl;
    }

    public boolean isDownloadable() {
        return downloadable;
    }

    public void setDownloadable( boolean downloadable ) {
        this.downloadable = downloadable;
    }

    public boolean isShareable() {
        return shareable;
    }

    public void setShareable( boolean shareable ) {
        this.shareable = shareable;
    }

	public String getGameType() {
		return gameType;
	}

	public void setGameType(String gameType) {
		this.gameType = gameType;
	}

	public String getXmlPath() {
		return xmlPath;
	}

	public void setXmlPath(String xmlPath) {
		this.xmlPath = xmlPath;
	}

	public String getGamePath() {
		return gamePath;
	}

	public void setGamePath(String gamePath) {
		this.gamePath = gamePath;
	}

	public String getFlashContainer() {
		return flashContainer;
	}

	public void setFlashContainer(String flashContainer) {
		this.flashContainer = flashContainer;
	}

	public String getVideoDuration() {
		return videoDuration;
	}

	public void setVideoDuration(String videoDuration) {
		this.videoDuration = videoDuration;
	}

}
