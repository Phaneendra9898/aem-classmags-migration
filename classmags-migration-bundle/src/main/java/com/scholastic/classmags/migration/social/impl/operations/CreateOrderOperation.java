
package com.scholastic.classmags.migration.social.impl.operations;

import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.servlets.post.PostOperation;
import com.adobe.cq.social.scf.OperationException;
import com.adobe.cq.social.scf.SocialComponent;
import com.adobe.cq.social.scf.SocialComponentFactory;
import com.adobe.cq.social.scf.SocialComponentFactoryManager;
import com.adobe.cq.social.scf.SocialOperationResult;
import com.adobe.cq.social.scf.core.operations.AbstractSocialOperation;
import com.scholastic.classmags.migration.social.api.OrderConfirmationService;
import com.scholastic.classmags.migration.social.api.VotingSocialComponent;
import com.scholastic.classmags.migration.social.impl.OrderConfirmationServiceImpl;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;

/**
 * A POST endpoint that accepts requests for creating orders. This class responds to
 * all POST requests with a :operation=social:order:createOrder parameter"
 */
@Component(immediate = true)
@Service
@Property(name = PostOperation.PROP_OPERATION_NAME, value = "social:order:createOrder")
// All SCF operation endpoints should extend the AbstractSocialOperation to leverage the functionality provided by SCF
public class CreateOrderOperation extends AbstractSocialOperation {

    // Use TaskService to actually do the work of creating a project
    @Reference
    private OrderConfirmationService orderService;

    // The SocialComponentFactoryManager lets you access the factories that can give you a SocialComponent for a given
    // resource.
    @Reference
    private SocialComponentFactoryManager scfManager;

    @Override
    // All Social Operation endpoints return a SocialOperationResult.
    protected SocialOperationResult performOperation(SlingHttpServletRequest request) throws OperationException {
        final Resource orderResource = this.orderService.createOrder(request);
        SocialOperationResult orderOperationResult = SocialASRPUtils.getSocialOperationResult(
                request, HttpServletResponse.SC_OK,
                ClassMagsMigrationASRPConstants.HTTP_SUCCESS_MESSAGE, orderResource, this.scfManager,
                OrderConfirmationServiceImpl.PROJECT_RESOURCE_TYPE );
        return orderOperationResult;
 
    }
}
