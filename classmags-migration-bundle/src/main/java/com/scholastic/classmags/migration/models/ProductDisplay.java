package com.scholastic.classmags.migration.models;

public class ProductDisplay {
	
	private String magazineTitle;
	
	private String magazinePath;
	
	private String magazineDescription;
	
	private String learnMorelabel;
	
	private String learnMoreLink;
	
	private String learnMorePopup;
	
	private String magazineName;

	public String getMagazineName() {
		return magazineName;
	}

	public void setMagazineName(String magazineName) {
		this.magazineName = magazineName;
	}

	public String getMagazineTitle() {
		return magazineTitle;
	}

	public void setMagazineTitle(String magazineTitle) {
		this.magazineTitle = magazineTitle;
	}

	public String getMagazinePath() {
		return magazinePath;
	}

	public void setMagazinePath(String magazinePath) {
		this.magazinePath = magazinePath;
	}

	public String getMagazineDescription() {
		return magazineDescription;
	}

	public void setMagazineDescription(String magazineDescription) {
		this.magazineDescription = magazineDescription;
	}

	public String getLearnMorelabel() {
		return learnMorelabel;
	}

	public void setLearnMorelabel(String learnMorelabel) {
		this.learnMorelabel = learnMorelabel;
	}

	public String getLearnMoreLink() {
		return learnMoreLink;
	}

	public void setLearnMoreLink(String learnMoreLink) {
		this.learnMoreLink = learnMoreLink;
	}

	public String getLearnMorePopup() {
		return learnMorePopup;
	}

	public void setLearnMorePopup(String learnMorePopup) {
		this.learnMorePopup = learnMorePopup;
	}
	
	

}
