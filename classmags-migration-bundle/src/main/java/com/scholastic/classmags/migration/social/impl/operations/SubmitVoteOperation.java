package com.scholastic.classmags.migration.social.impl.operations;

import javax.servlet.http.HttpServletResponse;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.servlets.post.PostOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.social.scf.OperationException;
import com.adobe.cq.social.scf.SocialComponentFactoryManager;
import com.adobe.cq.social.scf.SocialOperationResult;
import com.adobe.cq.social.scf.core.operations.AbstractSocialOperation;
import com.scholastic.classmags.migration.social.api.VotingService;
import com.scholastic.classmags.migration.social.api.VotingSocialComponent;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;

/**
 * A POST Endpoint for SubmitVoteOperation that accepts requests for submitting
 * votes. This class responds to all POST requests with a
 * :operation=social:voting:submitVote parameter. For example, curl
 * http://localhost:4502/content/classroom_magazines/scienceworld/issues/2016-17
 * /090516/testarticle/jcr:content/par/voting.social.json -uadmin:admin -v POST
 * -H "Accept:application/json" --data ":operation=social:voting:submitVote"
 */
@Component( immediate = true )
@Service
@Property( name = PostOperation.PROP_OPERATION_NAME, value = ClassMagsMigrationASRPConstants.SOCIAL_SUBMIT_VOTE_OPERATION )
public class SubmitVoteOperation extends AbstractSocialOperation {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( SubmitVoteOperation.class );

    @Reference
    private VotingService votingService;

    /** The scf manager. */
    @Reference
    private SocialComponentFactoryManager scfManager;

    @Override
    protected SocialOperationResult performOperation( SlingHttpServletRequest slingHttpServletRequest )
            throws OperationException {

        LOG.info( ClassMagsMigrationASRPConstants.OPERATION_START
                + ClassMagsMigrationASRPConstants.SOCIAL_SUBMIT_VOTE_OPERATION );

        final Resource votingResource = this.votingService.submitAndCreateVoteResource( slingHttpServletRequest );

        SocialOperationResult submitVoteSocialOperationResult = SocialASRPUtils.getSocialOperationResult(
                slingHttpServletRequest, HttpServletResponse.SC_OK,
                ClassMagsMigrationASRPConstants.HTTP_SUCCESS_MESSAGE, votingResource, this.scfManager,
                VotingSocialComponent.VOTING_RESOURCE_TYPE );

        LOG.info( ClassMagsMigrationASRPConstants.OPERATION_END );

        return submitVoteSocialOperationResult;
    }
}
