package com.scholastic.classmags.migration.controllers;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.models.TagModel;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.SubjectsCoveredService;

/**
 * Use Class for Subjects Covered.
 */
public class SubjectsCoveredUse extends WCMUsePojo {

    /** The subjects map. */
    private Map< TagModel, List< TagModel > > subjects;
    
    private String magazineName;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( SubjectsCoveredUse.class );

    /**
     * Gets the subjects.
     *
     * @return the subjects map.
     */
    public Map< TagModel, List< TagModel > > getSubjects() {
        return subjects;
    }
    
    /**
     * Gets the magazine name.
     *
     * @return the magazineName
     */
    public String getMagazineName() {
        return magazineName;
    }

    @Override
    public void activate() throws LoginException {

        LOG.info( "Inside Activate Method of SubjectsCoveredUse" );
        SlingHttpServletRequest request = getRequest();
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        Page currentPage = getCurrentPage();
        magazineName = StringUtils.EMPTY;

        if ( null != slingScriptHelper ) {

            MagazineProps magazineProps = slingScriptHelper.getService( MagazineProps.class );
            String currentPath = currentPage.getPath();
            if ( null != magazineProps ) {
                magazineName = magazineProps.getMagazineName( currentPath );
            }

            SubjectsCoveredService subjectsCoveredService = slingScriptHelper
                    .getService( SubjectsCoveredService.class );
            if ( null != subjectsCoveredService ) {
                subjects = subjectsCoveredService.getSubjectsCovered( request.getResource(), currentPath,
                        magazineName );
            }
        }
    }
}
