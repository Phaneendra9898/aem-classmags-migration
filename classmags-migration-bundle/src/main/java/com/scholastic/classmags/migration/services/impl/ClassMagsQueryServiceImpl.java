package com.scholastic.classmags.migration.services.impl;

import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_SORT_TITLE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_SORT_DATE;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.IntervalFacet;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.scholastic.classmags.migration.models.SearchRequest;
import com.scholastic.classmags.migration.models.SearchResponse;
import com.scholastic.classmags.migration.services.ClassMagsQueryService;
import com.scholastic.classmags.migration.utils.SolrClassMagsQueryBuilder;
import com.scholastic.classmags.migration.utils.SolrSearchQueryConstants;
import com.scholastic.classmags.migration.utils.SolrSearchUtil;

@Component( metatype = false, label = "Helper service for querying on solr" )
@Service( value = ClassMagsQueryService.class )
public class ClassMagsQueryServiceImpl implements ClassMagsQueryService {

    private static final Logger LOG = LoggerFactory.getLogger( ClassMagsQueryServiceImpl.class );

    @Reference
    private SolrClassMagsConfigService solrConfigurationService;

    public SearchResponse getSearchResults(  Map<String, String> params, boolean includeFacet, String queryType ) {
        SolrQuery solrQuery = new SolrQuery();
        QueryResponse solrResponse = null;
        SearchResponse response=null;
        try {
            SearchRequest searchRequest = SolrClassMagsQueryBuilder.generateSearchRequest( params );
            LOG.debug( "searchRequest = " + searchRequest.getText() );
            // Issue Query
            if ( searchRequest.isValidRequest() ) {
                // prepara json request
                solrQuery = prepareQuery( searchRequest, params.get( "preFilter" ), includeFacet, queryType );

                if ( LOG.isDebugEnabled() ){
                    solrQuery.setShowDebugInfo( true );
                }
                LOG.debug( "********************************************************" );
                LOG.debug( "********************************************************" );
                LOG.debug( "Final Query: {} ",solrQuery );
                LOG.debug( "********************************************************" );
                LOG.debug( "********************************************************" );
                // connect to solr core - zookeeper and query
                solrResponse = performScholasticSolrQuery( solrQuery );

                
                // build json response
                response = processQueryResponse( solrResponse, params.get( "preFilter" ), includeFacet );
            }
        } catch ( Exception e ) {
            LOG.error( "Error getting ressponse : {}", e );
        }
        return response;
    }

    private QueryResponse performScholasticSolrQuery( SolrQuery solrQuery ) {
        QueryResponse solrResponse = null;
        try {
            // connect to solr core - zookeeper and query
            solrResponse = solrConfigurationService.getSolrClient().query( solrQuery );
            if ( LOG.isDebugEnabled() ){
                LOG.debug( "Debug Info Results: {}" + solrResponse.getDebugMap() );
            }
            LOG.debug( "Results: " + solrResponse.getResults().getNumFound() );
        } catch ( SolrServerException e ) {
            LOG.error( "Error getting Solr Documents", e );
        } catch ( IOException e ) {
            LOG.error( "Error getting Solr Documents", e );
        } finally {
            if ( solrConfigurationService != null ) {
                solrConfigurationService.resetSolrServerClients();
                LOG.debug( "Closing the connection with Solr after querying..." );
            }
        }
        return solrResponse;

    }

    private SolrQuery prepareQuery( SearchRequest searchRequest, String preFilter, boolean includeFacet, String queryType ) {

        SolrQuery solrQuery = new SolrQuery();
        solrQuery.setFields( SolrSearchQueryConstants.SQ_RESP_FIELDS_LIST );
        SolrClassMagsQueryBuilder.sanitizeSearchTerm( searchRequest.getText() );
        
        if(queryType=="dismax"){
	        solrQuery.set("defType","edismax");
	        solrQuery.set("q",searchRequest.getText());
	        solrQuery.set("qf",SolrClassMagsQueryBuilder.getQueryFields(null));
	        solrQuery.set("pf",SolrClassMagsQueryBuilder.getQueryFields(null));
	        solrQuery.set( "mm", "100%" );
        }
        if ( StringUtils.isBlank( solrQuery.getQuery() ) ) {
            solrQuery.set("q",searchRequest.getText());
        } 

            // set filter
            solrQuery = SolrClassMagsQueryBuilder.generateSolrFilterQuery( preFilter, searchRequest, solrQuery,
                    includeFacet );
            LOG.debug( "-----------------------------------------------------------" );
            LOG.debug( "Final Solr Query: {}", solrQuery.getQuery() );
            LOG.debug( "Final Filter Query: {}", ArrayUtils.toString( solrQuery.getFilterQueries() ) );
            LOG.debug( "Final Query Sorts: {}", solrQuery.getSorts() );
            LOG.debug( "Final Query Sort Field: {}", solrQuery.getSortField() );
            
            if("sort_date_dt desc".equalsIgnoreCase(solrQuery.getSortField())){
            	String query = StringUtils.EMPTY;
            	if(queryType=="dismax"){
	            	if(StringUtils.isNotBlank(searchRequest.getText())){
	            		query = searchRequest.getText().concat(" AND ").concat(SolrSearchQueryConstants.SQ_BOOST_QUERY_FOR_CONTENT_ORDER);
	            	}else{
	            		query = SolrSearchQueryConstants.SQ_BOOST_QUERY_FOR_CONTENT_ORDER;
	            	}
            	}else{
            		if(StringUtils.isNotBlank(searchRequest.getText())){
	            		query = searchRequest.getText().concat(" AND ").concat(SolrSearchQueryConstants.SQ_BOOST_QUERY_FOR_CONTENT_HUB);
	            	}else{
	            		query = SolrSearchQueryConstants.SQ_BOOST_QUERY_FOR_CONTENT_HUB;
	            	}
            	}
            	solrQuery.set("q", query );
            	solrQuery.addSort(SolrSearchQueryConstants.SOLR_SCORE, SolrQuery.ORDER.desc);
            	LOG.debug( "Final Query after : {}",SolrSearchQueryConstants.SQ_BOOST_QUERY_FOR_CONTENT_ORDER  );
                LOG.debug( "-----------------------------------------------------------" );
            }
                        
            if ( LOG.isDebugEnabled() && solrQuery.getFacetQuery() != null && solrQuery.getFacetFields() != null) {
                for ( String facetQueries : solrQuery.getFacetQuery() ) {
                    LOG.debug( "Solr Facet Queries: {}", facetQueries );
                }
                LOG.debug( "-----------------------------------------------------------" );
                for ( String facetFields : solrQuery.getFacetFields() ) {
                    LOG.debug( "Solr Facet Fields: {}", facetFields );
                }
            }
        
        return solrQuery;
    }

    private SearchResponse processQueryResponse(QueryResponse solrResponse, String preFilter, boolean includeFacet){
        // get result back
        SolrDocumentList solrDocumentList = solrResponse.getResults();
        SearchResponse searchResponse = new SearchResponse(solrDocumentList);
        searchResponse.setType( preFilter );
        searchResponse.setTotal(solrDocumentList.getNumFound());
        if (includeFacet) {
            searchResponse.setIntervalFacets(getIntervalFacets(solrResponse));
            searchResponse.setFacets(getFacetFields(solrResponse));
        }
        return searchResponse;
    }

    public String getResultsAsJSONString( SearchResponse searchResponse ) {
        Gson gson = new Gson();
        // get result back
        String jsonResponse = gson.toJson( searchResponse );
        if ( LOG.isDebugEnabled() ) {
            LOG.debug( jsonResponse );
        }
        return jsonResponse;
    }

    private JSONObject getFacetFields( QueryResponse solrResponse ) {
        JSONObject facetFields = new JSONObject();
        FacetField solrFacets = null;
        try {
            // get all facets
            for ( String solrFacetField : SolrSearchQueryConstants.SOLR_FACET_FIELD ) {
                solrFacets = solrResponse.getFacetField( solrFacetField );
                if ( solrFacets != null && solrFacets.getValues() != null ) {
                    List< FacetField.Count > facetEntries = solrFacets.getValues();
                    JSONArray facetField = getFacetFieldsJSONArray(facetEntries);
                    facetFields.put( solrFacetField, facetField );
                }
            }
        } catch ( JSONException e ) {
            LOG.error( "JSON Error {}", e );
        }
        return facetFields;
    }
    
    private JSONArray getFacetFieldsJSONArray( List< FacetField.Count > facetEntries ) throws JSONException {
        JSONArray facetField = new JSONArray();
        for ( FacetField.Count fcount : facetEntries ) {
            JSONObject attr = new JSONObject();
                attr.put( "value", fcount.getName() );
                attr.put( "count", fcount.getCount() );
                facetField.put( attr );
        }
        return facetField;
    }

    private JSONObject getIntervalFacets( QueryResponse solrResponse ) {
        JSONObject intervalFacetFields = new JSONObject();
        List< IntervalFacet > solrIntervalFacet = null;
        try {
            // get all facet interval
            solrIntervalFacet = solrResponse.getIntervalFacets();
            if ( solrIntervalFacet != null ) {
                for ( IntervalFacet intervalFacet : solrIntervalFacet ) {
                    if ( intervalFacet.getIntervals() != null ) {
                        intervalFacetFields.put( intervalFacet.getField(),
                                SolrSearchUtil.getIntervalFacetCountByField( intervalFacet.getIntervals() ) );
                    }
                }
            }
        } catch ( JSONException e ) {
            LOG.error( "JSON Error {}", e );
        }
        return intervalFacetFields;
    }

    public String getResultsAsJSONArrayString( List< SearchResponse > solrResponses ) {
        Gson gson = new Gson();
        JsonElement element = gson.toJsonTree( solrResponses, new TypeToken< List< SearchResponse > >() {
        }.getType() );
        if ( !element.isJsonArray() ) {
            LOG.error( "Error Parsing JSON" );
            return StringUtils.EMPTY;
        }
        JsonArray jsonArray = element.getAsJsonArray();
        return jsonArray.toString();
    }

}
