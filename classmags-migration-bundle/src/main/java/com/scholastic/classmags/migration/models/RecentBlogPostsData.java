package com.scholastic.classmags.migration.models;

/**
 * The Class RecentBlogPostsData.
 */
public class RecentBlogPostsData {

    /** The blog page title. */
    String blogPageTitle;

    /** The blog page date. */
    String blogPageDate;

    /**
     * Gets the blog page title.
     *
     * @return the blogPageTitle
     */
    public String getBlogPageTitle() {
        return blogPageTitle;
    }

    /**
     * Sets the blog page title.
     *
     * @param blogPageTitle
     *            the blogPageTitle to set
     */
    public void setBlogPageTitle( String blogPageTitle ) {
        this.blogPageTitle = blogPageTitle;
    }

    /**
     * Gets the blog page date.
     *
     * @return the blogPageDate
     */
    public String getBlogPageDate() {
        return blogPageDate;
    }

    /**
     * Sets the blog page date.
     *
     * @param blogPageDate
     *            the blogPageDate to set
     */
    public void setBlogPageDate( String blogPageDate ) {
        this.blogPageDate = blogPageDate;
    }
}
