package com.scholastic.classmags.migration.models;

/**
 * The Class UIdHeaderLaunchContext.
 */
public class UIdHeaderLaunchContext {

    /** The role. */
    private String role;

    /** The obj U id launch context sub header application. */
    private UIdLaunchContextSubHeaderApplication objUIdLaunchContextSubHeaderApplication;

    /**
     * Gets the role.
     *
     * @return the role
     */
    public String getRole() {
        return role;
    }

    /**
     * Sets the role.
     *
     * @param role
     *            the role to set
     */
    public void setRole( String role ) {
        this.role = role;
    }

    /**
     * Gets the obj U id launch context sub header application.
     *
     * @return the objUIdLaunchContextSubHeaderApplication
     */
    public UIdLaunchContextSubHeaderApplication getObjUIdLaunchContextSubHeaderApplication() {
        return objUIdLaunchContextSubHeaderApplication;
    }

    /**
     * Sets the obj U id launch context sub header application.
     *
     * @param objUIdLaunchContextSubHeaderApplication
     *            the objUIdLaunchContextSubHeaderApplication to set
     */
    public void setObjUIdLaunchContextSubHeaderApplication(
            UIdLaunchContextSubHeaderApplication objUIdLaunchContextSubHeaderApplication ) {
        this.objUIdLaunchContextSubHeaderApplication = objUIdLaunchContextSubHeaderApplication;
    }
}
