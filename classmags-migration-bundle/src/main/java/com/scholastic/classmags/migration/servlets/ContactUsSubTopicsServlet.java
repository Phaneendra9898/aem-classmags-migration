package com.scholastic.classmags.migration.servlets;

import static com.day.cq.search.Predicate.PARAM_LIMIT;
import static com.day.cq.search.PredicateConverter.GROUP_PARAMETER_PREFIX;
import static com.day.cq.search.eval.JcrPropertyPredicateEvaluator.PROPERTY;
import static com.day.cq.search.eval.JcrPropertyPredicateEvaluator.VALUE;
import static com.day.cq.search.eval.PathPredicateEvaluator.PATH;
import static com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants.BASE_PAGE_RESOURCE_TYPE;
import static com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants.DOT;
import static com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants.JSON_KEY_TOPIC_TITLE;
import static com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants.NN_TOPIC;
import static com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants.QUERY_LOWER_BOUND;
import static org.apache.sling.jcr.resource.JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.servlets.OptingServlet;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.NameConstants;

@SlingServlet(label = "Classroom Magazines - Contact Us Subtopics end point", description = "Returns the subtopics for a given topic", resourceTypes = {
		NameConstants.NT_PAGE }, selectors = "subtopics", extensions = "html", methods = "GET")
@Service(Servlet.class)
public class ContactUsSubTopicsServlet extends SlingAllMethodsServlet implements OptingServlet {

	private static final long serialVersionUID = -2954675875536881469L;

	/* Logger for the class */
	private static Logger LOG = LoggerFactory.getLogger(ContactUsSubTopicsServlet.class);

	private static final String TOPIC_RT = "scholastic/classroom-magazines-migration/components/content/contact-us-topics";

	private static final String JSON_KEY_SUB_TOPIC = "subTopics";
	
	private static final String DEFAULT_RESPONSE = "[{\"subTopicTitle\":\"Other\"}]";

	@Reference
	private QueryBuilder queryBuilder;

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws javax.servlet.ServletException, java.io.IOException {
		String result = DEFAULT_RESPONSE;
		String[] values = ArrayUtils.EMPTY_STRING_ARRAY;
		try {
			values = getTopics(request);
			LOG.debug("request for topic : {}", request.getParameter(NN_TOPIC));
			if (ArrayUtils.isNotEmpty(values)) {
				for (String s : values) {
					JSONObject object = new JSONObject(s);
					String key = request.getParameter(NN_TOPIC);
					String topicName = object.optString(JSON_KEY_TOPIC_TITLE);
					if (key.equalsIgnoreCase(topicName)) {
						JSONArray subtopics = object.optJSONArray(JSON_KEY_SUB_TOPIC);
						if (subtopics != null) {
							result = subtopics.toString();
						}
						break;
					}
				}
			}
		} catch (RepositoryException | JSONException e) {
			LOG.error("Exception : {}", e);
		}
		response.getWriter().write(result);

	}

	private String[] getTopics(SlingHttpServletRequest request) throws RepositoryException {
		List<String> values = new ArrayList<String>();
		Map<String, String> queryMap = new HashMap<String, String>();
		queryMap.put(PATH, request.getResource().getPath());
		queryMap.put(GROUP_PARAMETER_PREFIX + DOT + PARAM_LIMIT, QUERY_LOWER_BOUND);
		queryMap.put(PROPERTY, SLING_RESOURCE_TYPE_PROPERTY);
		queryMap.put(PROPERTY + DOT + VALUE, TOPIC_RT);
		Query query = queryBuilder.createQuery(PredicateGroup.create(queryMap),
				request.getResourceResolver().adaptTo(Session.class));
		SearchResult searchResult = query.getResult();
		for (Hit hit : searchResult.getHits()) {
			String[] val = hit.getProperties().get(NN_TOPIC, new String[] {});
			if (ArrayUtils.isNotEmpty(val)) {
				values.addAll(Arrays.asList(val));
			}
		}
		return values.toArray(new String[values.size()]);
	}

	@Override
	public boolean accepts(SlingHttpServletRequest request) {
		final Resource jcrContentResource = request.getResource().getChild(JcrConstants.JCR_CONTENT);
		return (jcrContentResource != null && jcrContentResource.getResourceType().endsWith(BASE_PAGE_RESOURCE_TYPE)
				&& StringUtils.isNotBlank(request.getParameter(NN_TOPIC)));
	}

}
