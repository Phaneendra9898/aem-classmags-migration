package com.scholastic.classmags.migration.models;

/**
 * The Class BlogCategoriesData.
 */
public class BlogCategoriesData {

    /** The blog category title. */
    String blogCategoryTitle;

    /** The blog category link. */
    String blogCategoryLink;

    /**
     * Gets the blog category title.
     *
     * @return the blogCategoryTitle
     */
    public String getBlogCategoryTitle() {
        return blogCategoryTitle;
    }

    /**
     * Sets the blog category title.
     *
     * @param blogCategoryTitle
     *            the blogCategoryTitle to set
     */
    public void setBlogCategoryTitle( String blogCategoryTitle ) {
        this.blogCategoryTitle = blogCategoryTitle;
    }

    /**
     * Gets the blog category link.
     *
     * @return the blogCategoryLink
     */
    public String getBlogCategoryLink() {
        return blogCategoryLink;
    }

    /**
     * Sets the blog category link.
     *
     * @param blogCategoryLink
     *            the blogCategoryLink to set
     */
    public void setBlogCategoryLink( String blogCategoryLink ) {
        this.blogCategoryLink = blogCategoryLink;
    }
}
