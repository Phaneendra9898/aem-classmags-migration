package com.scholastic.classmags.migration.social.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.jcr.RepositoryException;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.ReferenceCardinality;
import org.apache.felix.scr.annotations.ReferencePolicy;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingException;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.resource.JcrResourceConstants;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.social.commons.CollabUser;
import com.adobe.cq.social.scf.OperationException;
import com.adobe.cq.social.srp.SocialResourceProvider;
import com.adobe.cq.social.ugc.api.SearchResults;
import com.adobe.cq.social.ugc.api.UgcFilter;
import com.adobe.cq.social.ugc.api.UgcSearch;
import com.adobe.cq.social.ugc.api.ValueConstraint;
import com.adobe.cq.social.ugcbase.SocialUtils;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.commons.jcr.JcrUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.ClassmagsMigrationErrorCodes;
import com.scholastic.classmags.migration.models.VoteData;
import com.scholastic.classmags.migration.social.api.VotingService;
import com.scholastic.classmags.migration.social.api.VotingSocialComponent;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.RoleUtil;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;

/**
 * The Class VotingServiceImpl.
 */
@SuppressWarnings( "deprecation" )
@Component( immediate = true, metatype = false )
@Service( VotingService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Voting Service" ) } )
public class VotingServiceImpl implements VotingService {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( VotingServiceImpl.class );

    /** The resource resolver factory. */
    @Reference( cardinality = ReferenceCardinality.MANDATORY_UNARY, policy = ReferencePolicy.STATIC )
    private ResourceResolverFactory resourceResolverFactory;

    /** The social utils. */
    @Reference
    private SocialUtils socialUtils;

    /** The ugc search. */
    @Reference
    private UgcSearch ugcSearch;

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.social.api.VotingService#
     * submitAndCreateVoteResource(org.apache.sling.api.SlingHttpServletRequest)
     */
    @Override
    public Resource submitAndCreateVoteResource( SlingHttpServletRequest slingHttpServletRequest )
            throws OperationException {

        String userRole = RoleUtil.getUserRole( slingHttpServletRequest );
        String userId = SocialASRPUtils.fetchUserId( slingHttpServletRequest, userRole );

        if ( !SocialASRPUtils.isValidUser( userRole, userId ) ) {
            throw new OperationException(
                    ClassmagsMigrationErrorCodes.INVALID_USER_EXCEPTION_MESSAGE.getErrorDescription(),
                    ClassmagsMigrationErrorCodes.INVALID_USER_EXCEPTION_MESSAGE.getErrorCodeInt() );
        }

        final Resource resource = slingHttpServletRequest.getResource();
        final ResourceResolver resolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE );

        LOG.debug( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.RESOURCE_RESOLVER_OBJECT + resolver ) );

        return checkUserUGCResourceAccessForSubmitOperation( resource, slingHttpServletRequest, resolver, userRole,
                userId );
    }

    /**
     * Check user UGC resource access for submit operation.
     *
     * @param resource
     *            the resource
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @param resolver
     *            the resolver
     * @param userRole
     *            the user role
     * @param userId
     *            the user id
     * @return the resource
     * @throws OperationException
     *             the operation exception
     */
    private Resource checkUserUGCResourceAccessForSubmitOperation( Resource resource,
            SlingHttpServletRequest slingHttpServletRequest, ResourceResolver resolver, String userRole, String userId )
            throws OperationException {

        try {
            if ( !socialUtils.mayAccessUGC( resolver ) ) {
                throw new OperationException(
                        userId + ClassmagsMigrationErrorCodes.PERMISSION_DENIED_MESSAGE.getErrorDescription(),
                        ClassmagsMigrationErrorCodes.PERMISSION_DENIED_MESSAGE.getErrorCodeInt() );
            }
            return createOrUpdateVotingData( slingHttpServletRequest, resource, userRole, userId, resolver );

        } catch ( PersistenceException | JSONException | ClassmagsMigrationBaseException e ) {
            LOG.error( ClassmagsMigrationErrorCodes.SUBMIT_VOTE_ERROR_MESSAGE.getErrorDescription(), e );
            throw new OperationException( ClassmagsMigrationErrorCodes.SUBMIT_VOTE_ERROR_MESSAGE.getErrorDescription(),
                    ClassmagsMigrationErrorCodes.SUBMIT_VOTE_ERROR_MESSAGE.getErrorCodeInt() );
        }
    }

    /**
     * Creates the or update voting data.
     *
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @param resource
     *            the resource
     * @param userRole
     *            the user role
     * @param userId
     *            the user id
     * @param resolver
     *            the resolver
     * @return the resource
     * @throws OperationException
     *             the operation exception
     * @throws PersistenceException
     *             the persistence exception
     * @throws JSONException
     *             the JSON exception
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private Resource createOrUpdateVotingData( SlingHttpServletRequest slingHttpServletRequest, Resource resource,
            String userRole, String userId, ResourceResolver resolver )
            throws OperationException, PersistenceException, JSONException, ClassmagsMigrationBaseException {

        final long startTime = System.currentTimeMillis();

        SocialResourceProvider srp = socialUtils.getSocialResourceProvider( resource );
        srp.setConfig( socialUtils.getDefaultStorageConfig() );

        JSONObject jsonObjVoteData = new JSONObject(
                slingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.VOTE_DATA ) );
        VoteData voteData = populateVoteData( userRole, userId, jsonObjVoteData );

        SocialASRPUtils.createSocialNode( srp, resolver, SocialASRPUtils.pathBuilder( voteData.getVotePagePath() ) );
        Resource questionUgcResource = createOrUpdateQuestionSocialNode( srp, resolver, voteData );
        searchAndCreateAnswerSocialNode( srp, resolver, voteData );

        LOG.debug( CommonUtils.addDelimiter( "Method VotingServiceImpl.createOrUpdateVotingData TimeMillis: "
                + ( System.currentTimeMillis() - startTime ) ) );
        return questionUgcResource;
    }

    /**
     * Populate vote data.
     *
     * @param userRole
     *            the user role
     * @param userId
     *            the user id
     * @param jsonObjVoteData
     *            the json obj vote data
     * @return the vote data
     */
    @SuppressWarnings( "unchecked" )
    private VoteData populateVoteData( String userRole, String userId, JSONObject jsonObjVoteData ) {

        final long startTime = System.currentTimeMillis();

        VoteData voteData = new VoteData();
        voteData.setVotePagePath(
                jsonObjVoteData.optString( ClassMagsMigrationASRPConstants.JSON_KEY_VOTE_PAGE_PATH ) );
        voteData.setQuestionUUID( jsonObjVoteData.optString( ClassMagsMigrationASRPConstants.QUESTION_UUID ) );
        voteData.setAnswerOptions( ( Map< String, String > ) new Gson().fromJson(
                jsonObjVoteData.optString( ClassMagsMigrationASRPConstants.ANSWER_OPTIONS ),
                new TypeToken< Map< String, String > >() {
                }.getType() ) );
        voteData.setUserRole( userRole );
        voteData.setUserIdentifier( userId );
        voteData.setDisableVotingDateInMilliSec(
                jsonObjVoteData.optString( ClassMagsMigrationASRPConstants.JSON_KEY_DISABLE_VOTING_DATE ) );

        LOG.debug( CommonUtils.addDelimiter( "Method VotingServiceImpl.populateVoteData TimeMillis: "
                + ( System.currentTimeMillis() - startTime ) ) );

        return voteData;
    }

    /**
     * Refactor vote answer options.
     *
     * @param voteData
     *            the vote data
     */
    private void refactorVoteAnswerOptions( VoteData voteData ) {

        Map< String, String > answerOptionsMap = voteData.getAnswerOptions();
        Map< String, String > refactoredAnswerOptionsMap = new HashMap<>();
        for ( String answerOptionsKey : answerOptionsMap.keySet() ) {
            refactoredAnswerOptionsMap.put( JcrUtil.createValidName( answerOptionsKey ),
                    answerOptionsMap.get( answerOptionsKey ) );
        }
        voteData.setAnswerOptions( refactoredAnswerOptionsMap );
    }

    /**
     * Creates the or update question social node.
     *
     * @param srp
     *            the srp
     * @param resolver
     *            the resolver
     * @param voteData
     *            the vote data
     * @return the resource
     * @throws PersistenceException
     *             the persistence exception
     */
    private Resource createOrUpdateQuestionSocialNode( SocialResourceProvider srp, ResourceResolver resolver,
            VoteData voteData ) throws PersistenceException {

        final long startTime = System.currentTimeMillis();

        Resource ugcResource = null;
        String questionUgcResourcePath = SocialASRPUtils.pathBuilder( voteData.getVotePagePath(),
                voteData.getQuestionUUID() );
        LOG.debug( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.SOCIAL_PATH ) + questionUgcResourcePath );

        try {
            ugcResource = srp.getResource( resolver, questionUgcResourcePath );
            LOG.debug( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.INTIAL_UGC_RESOURCE + ugcResource ) );
        } catch ( SlingException se ) {
            LOG.error( ClassMagsMigrationASRPConstants.RESOURCE_PATH_ERROR + se );
        }
        if ( null == ugcResource ) {
            Map< String, Object > props = new HashMap<>();
            props.put( JcrConstants.JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED );
            props.put( ClassMagsMigrationASRPConstants.JSON_KEY_ANSWER_OPTIONS,
                    voteData.getAnswerOptions().keySet().toArray() );
            ugcResource = srp.create( resolver, questionUgcResourcePath, props );
        } else {
            ModifiableValueMap propertyMap = ugcResource.adaptTo( ModifiableValueMap.class );
            if ( null != propertyMap ) {
                propertyMap.put( ClassMagsMigrationASRPConstants.JSON_KEY_ANSWER_OPTIONS,
                        voteData.getAnswerOptions().keySet().toArray() );
            }
        }
        srp.commit( resolver );
        resolver.commit();

        LOG.debug( CommonUtils.addDelimiter( "Method VotingServiceImpl.createOrUpdateQuestionSocialNode TimeMillis: "
                + ( System.currentTimeMillis() - startTime ) ) );

        return ugcResource;
    }

    /**
     * Search and create answer social node.
     *
     * @param srp
     *            the srp
     * @param resolver
     *            the resolver
     * @param voteData
     *            the vote data
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     * @throws PersistenceException
     *             the persistence exception
     * @throws OperationException
     *             the operation exception
     */
    private void searchAndCreateAnswerSocialNode( SocialResourceProvider srp, ResourceResolver resolver,
            VoteData voteData ) throws ClassmagsMigrationBaseException, PersistenceException, OperationException {

        final long startTime = System.currentTimeMillis();

        int searchOffset = ClassMagsMigrationASRPConstants.SOCIAL_SEARCH_OFFSET;
        int searchLimit = ClassMagsMigrationASRPConstants.SOCIAL_SEARCH_INTIAL_LIMIT;

        if ( StringUtils.equals( RoleUtil.ROLE_TYPE_TEACHER, voteData.getUserRole() ) ) {
            searchAndCreateTeacherSocialNode( srp, resolver, voteData, searchOffset, searchLimit );
        }
        if ( StringUtils.equals( RoleUtil.ROLE_TYPE_STUDENT, voteData.getUserRole() ) ) {
            searchAndCreateStudentSocialNode( srp, resolver, voteData, searchOffset, searchLimit );
        }

        LOG.debug( CommonUtils.addDelimiter( "Method VotingServiceImpl.searchAndCreateAnswerSocialNode TimeMillis: "
                + ( System.currentTimeMillis() - startTime ) ) );
    }

    /**
     * Search and create teacher social node.
     *
     * @param srp
     *            the srp
     * @param resolver
     *            the resolver
     * @param voteData
     *            the vote data
     * @param searchOffset
     *            the search offset
     * @param searchLimit
     *            the search limit
     * @throws OperationException
     *             the operation exception
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     * @throws PersistenceException
     *             the persistence exception
     */
    private void searchAndCreateTeacherSocialNode( SocialResourceProvider srp, ResourceResolver resolver,
            VoteData voteData, int searchOffset, int searchLimit )
            throws OperationException, ClassmagsMigrationBaseException, PersistenceException {

        final long startTime = System.currentTimeMillis();

        Long teacherVoteCount = searchForTeacherVoteCount( resolver, voteData, searchOffset, searchLimit );

        if ( teacherVoteCount < ClassMagsMigrationASRPConstants.TEACHER_VOTE_LIMIT ) {
            createAnswerSocialNode( srp, resolver, voteData );
        } else {
            throw new OperationException(
                    ClassmagsMigrationErrorCodes.VOTE_SUBMISSION_LIMIT_MESSAGE.getErrorDescription(),
                    ClassmagsMigrationErrorCodes.VOTE_SUBMISSION_LIMIT_MESSAGE.getErrorCodeInt() );
        }

        LOG.debug( CommonUtils.addDelimiter( "Method VotingServiceImpl.searchAndCreateTeacherSocialNode TimeMillis: "
                + ( System.currentTimeMillis() - startTime ) ) );
    }

    /**
     * Search and create student social node.
     *
     * @param srp
     *            the srp
     * @param resolver
     *            the resolver
     * @param voteData
     *            the vote data
     * @param searchOffset
     *            the search offset
     * @param searchLimit
     *            the search limit
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     * @throws PersistenceException
     *             the persistence exception
     * @throws OperationException
     *             the operation exception
     */
    private void searchAndCreateStudentSocialNode( SocialResourceProvider srp, ResourceResolver resolver,
            VoteData voteData, int searchOffset, int searchLimit )
            throws ClassmagsMigrationBaseException, PersistenceException, OperationException {

        final long startTime = System.currentTimeMillis();

        Long studentVoteCount = searchForStudentVoteCount( resolver, voteData, searchOffset, searchLimit );

        if ( studentVoteCount < ClassMagsMigrationASRPConstants.STUDENT_VOTE_LIMIT ) {
            createAnswerSocialNode( srp, resolver, voteData );
        } else {
            throw new OperationException(
                    ClassmagsMigrationErrorCodes.VOTE_SUBMISSION_LIMIT_MESSAGE.getErrorDescription(),
                    ClassmagsMigrationErrorCodes.VOTE_SUBMISSION_LIMIT_MESSAGE.getErrorCodeInt() );
        }

        LOG.debug( CommonUtils.addDelimiter( "Method VotingServiceImpl.searchAndCreateStudentSocialNode TimeMillis: "
                + ( System.currentTimeMillis() - startTime ) ) );
    }

    /**
     * Search for teacher vote count.
     *
     * @param resolver
     *            the resolver
     * @param voteData
     *            the vote data
     * @param searchOffset
     *            the search offset
     * @param searchLimit
     *            the search limit
     * @return the long
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private Long searchForTeacherVoteCount( ResourceResolver resolver, VoteData voteData, int searchOffset,
            int searchLimit ) throws ClassmagsMigrationBaseException {

        final long startTime = System.currentTimeMillis();

        SearchResults< Resource > results = null;
        final UgcFilter filter = new UgcFilter();

        filter.addConstraint( new ValueConstraint< String >( JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY,
                VotingSocialComponent.VOTING_RESOURCE_TYPE ) );
        filter.addConstraint( new ValueConstraint< String >( ClassMagsMigrationASRPConstants.JSON_KEY_QUESTION_UUID,
                voteData.getQuestionUUID() ) );
        filter.addConstraint( new ValueConstraint< String >( CollabUser.PROP_NAME, voteData.getUserIdentifier() ) );
        filter.addConstraint( new ValueConstraint< String >( ClassMagsMigrationASRPConstants.USER_ROLE,
                RoleUtil.ROLE_TYPE_TEACHER ) );

        try {
            results = ugcSearch.find( null, resolver, filter, searchOffset, searchLimit, true );

            LOG.debug( CommonUtils.addDelimiter( "Method VotingServiceImpl.searchForTeacherVoteCount TimeMillis: "
                    + ( System.currentTimeMillis() - startTime ) ) );

            return null != results ? results.getTotalNumberOfResults() : NumberUtils.LONG_ZERO;
        } catch ( RepositoryException e ) {
            throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.REPOSITORY_ERROR, e );
        }
    }

    /**
     * Search for student vote count.
     *
     * @param resolver
     *            the resolver
     * @param voteData
     *            the vote data
     * @param searchOffset
     *            the search offset
     * @param searchLimit
     *            the search limit
     * @return the long
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    private long searchForStudentVoteCount( ResourceResolver resolver, VoteData voteData, int searchOffset,
            int searchLimit ) throws ClassmagsMigrationBaseException {

        final long startTime = System.currentTimeMillis();

        SearchResults< Resource > results = null;
        final UgcFilter filter = new UgcFilter();

        filter.addConstraint( new ValueConstraint< String >( JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY,
                VotingSocialComponent.VOTING_RESOURCE_TYPE ) );
        filter.addConstraint( new ValueConstraint< String >( ClassMagsMigrationASRPConstants.JSON_KEY_QUESTION_UUID,
                voteData.getQuestionUUID() ) );
        filter.addConstraint( new ValueConstraint< String >( CollabUser.PROP_NAME, voteData.getUserIdentifier() ) );
        filter.addConstraint( new ValueConstraint< String >( ClassMagsMigrationASRPConstants.USER_ROLE,
                RoleUtil.ROLE_TYPE_STUDENT ) );

        try {
            results = ugcSearch.find( null, resolver, filter, searchOffset, searchLimit, true );

            LOG.debug( CommonUtils.addDelimiter( "Method VotingServiceImpl.searchForStudentVoteCount TimeMillis: "
                    + ( System.currentTimeMillis() - startTime ) ) );

            return null != results ? results.getTotalNumberOfResults() : NumberUtils.LONG_ZERO;
        } catch ( RepositoryException e ) {
            throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.REPOSITORY_ERROR, e );
        }
    }

    /**
     * Creates the answer social node.
     *
     * @param srp
     *            the srp
     * @param resolver
     *            the resolver
     * @param voteData
     *            the vote data
     * @throws PersistenceException
     *             the persistence exception
     */
    private void createAnswerSocialNode( SocialResourceProvider srp, ResourceResolver resolver, VoteData voteData )
            throws PersistenceException {

        final long startTime = System.currentTimeMillis();

        refactorVoteAnswerOptions( voteData );

        Map< String, String > answerOptionsMap = voteData.getAnswerOptions();
        int answerOptionNodeCounter;
        for ( String answerOptionKey : answerOptionsMap.keySet() ) {
            answerOptionNodeCounter = Integer.parseInt( answerOptionsMap.get( answerOptionKey ) );
            SocialASRPUtils.createSocialNode( srp, resolver, SocialASRPUtils.pathBuilder( voteData.getVotePagePath(),
                    voteData.getQuestionUUID(), answerOptionKey ) );
            for ( int nodeCounter = NumberUtils.INTEGER_ZERO; nodeCounter < answerOptionNodeCounter; nodeCounter++ ) {

                String ugcResourcePath = SocialASRPUtils.pathBuilder( voteData.getVotePagePath(),
                        voteData.getQuestionUUID(), answerOptionKey, UUID.randomUUID().toString() );

                Map< String, Object > props = new HashMap<>();
                props.put( CollabUser.PROP_NAME, voteData.getUserIdentifier() );
                props.put( JcrResourceConstants.SLING_RESOURCE_TYPE_PROPERTY,
                        VotingSocialComponent.VOTING_RESOURCE_TYPE );
                props.put( JcrConstants.JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED );
                props.put( ClassMagsMigrationASRPConstants.JSON_KEY_QUESTION_UUID, voteData.getQuestionUUID() );
                props.put( ClassMagsMigrationASRPConstants.USER_ROLE, voteData.getUserRole() );
                srp.create( resolver, ugcResourcePath, props );
            }
            srp.commit( resolver );
            resolver.commit();
            LOG.debug( CommonUtils.addDelimiter( "Method VotingServiceImpl.createAnswerSocialNode TimeMillis: "
                    + ( System.currentTimeMillis() - startTime ) ) );
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.social.api.VotingService#
     * getVoteDataResource(org.apache.sling.api.SlingHttpServletRequest)
     */
    @Override
    public Resource getVoteDataResource( SlingHttpServletRequest slingHttpServletRequest ) throws OperationException {

        String userRole = RoleUtil.getUserRole( slingHttpServletRequest );
        String userId = SocialASRPUtils.fetchUserId( slingHttpServletRequest, userRole );

        if ( !SocialASRPUtils.isValidUser( userRole, userId ) ) {
            throw new OperationException(
                    ClassmagsMigrationErrorCodes.INVALID_USER_EXCEPTION_MESSAGE.getErrorDescription(),
                    ClassmagsMigrationErrorCodes.INVALID_USER_EXCEPTION_MESSAGE.getErrorCodeInt() );
        }

        final Resource resource = slingHttpServletRequest.getResource();
        final ResourceResolver resolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE );

        LOG.debug( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.RESOURCE_RESOLVER_OBJECT + resolver ) );

        return getVotingUGCResource( resource, slingHttpServletRequest, resolver, userRole, userId );
    }

    /**
     * Gets the voting UGC resource.
     *
     * @param resource
     *            the resource
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @param resolver
     *            the resolver
     * @param userRole
     *            the user role
     * @param userId
     *            the user id
     * @return the voting UGC resource
     * @throws OperationException
     *             the operation exception
     */
    private Resource getVotingUGCResource( Resource resource, SlingHttpServletRequest slingHttpServletRequest,
            ResourceResolver resolver, String userRole, String userId ) throws OperationException {

        final long startTime = System.currentTimeMillis();

        SocialResourceProvider srp = socialUtils.getSocialResourceProvider( resource );
        srp.setConfig( socialUtils.getDefaultStorageConfig() );

        JSONObject jsonObjVoteData;
        try {
            jsonObjVoteData = new JSONObject(
                    slingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.VOTE_DATA ) );
        } catch ( JSONException e ) {
            LOG.error( ClassmagsMigrationErrorCodes.GET_VOTE_DATA_ERROR_MESSAGE.getErrorDescription(), e );
            throw new OperationException(
                    ClassmagsMigrationErrorCodes.GET_VOTE_DATA_ERROR_MESSAGE.getErrorDescription(),
                    ClassmagsMigrationErrorCodes.GET_VOTE_DATA_ERROR_MESSAGE.getErrorCodeInt() );
        }
        VoteData voteData = populateVoteData( userRole, userId, jsonObjVoteData );

        String ugcResourcePath = SocialASRPUtils.pathBuilder( voteData.getVotePagePath(), voteData.getQuestionUUID() );
        LOG.debug( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.SOCIAL_PATH + ugcResourcePath ) );

        LOG.debug( CommonUtils.addDelimiter( "Method VotingServiceImpl.getVotingUGCResource TimeMillis: "
                + ( System.currentTimeMillis() - startTime ) ) );

        return SocialASRPUtils.getUGCResourceFromPath( resolver, ugcResourcePath, srp );
    }
}
