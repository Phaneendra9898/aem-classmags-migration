package com.scholastic.classmags.migration.controllers;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

public class FAQQuestionSetUse extends WCMUsePojo {

    private List< ValueMap > questionList;

    @Override
    public void activate() {
        final Resource resource = getResource();
        final Resource questionListResource = resource.getChild( ClassMagsMigrationConstants.QUESTION_LIST );
        questionList = CommonUtils.fetchMultiFieldData( questionListResource );
    }

    public List< ValueMap > getQuestionList() {
        return questionList;
    }

}
