package com.scholastic.classmags.migration.services;

import org.apache.sling.api.resource.ValueMap;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;

/**
 * The Interface CurrentIssueHeroDataService.
 */
public interface CurrentIssueHeroDataService {

    /**
     * Fetch issue data.
     *
     * @param currentPath
     *            the current path
     * @return the value map
     */
    public ValueMap fetchIssueData( String currentPath );

    /**
     * Fetch issue sorting date.
     *
     * @param issuePath The issue path.
     * @param currentPagePath The current page path.
     * 
     * @return the string
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    public String fetchIssueDate( String issuePath, String currentPagePath ) throws ClassmagsMigrationBaseException;
}
