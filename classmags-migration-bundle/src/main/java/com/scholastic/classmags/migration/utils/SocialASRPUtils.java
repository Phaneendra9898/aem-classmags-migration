package com.scholastic.classmags.migration.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.sling.api.SlingException;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.social.scf.SocialComponent;
import com.adobe.cq.social.scf.SocialComponentFactory;
import com.adobe.cq.social.scf.SocialComponentFactoryManager;
import com.adobe.cq.social.scf.SocialOperationResult;
import com.adobe.cq.social.srp.SocialResourceProvider;
import com.adobe.cq.social.ugcbase.SocialUtils;
import com.day.cq.commons.jcr.JcrConstants;

/**
 * The Class SocialASRPUtils.
 */
@SuppressWarnings( "deprecation" )
public final class SocialASRPUtils {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( SocialASRPUtils.class );

    /**
     * Instantiates a new social ASRP utils.
     */
    private SocialASRPUtils() {
    }

    /**
     * Url encoder.
     *
     * @param path
     *            the path
     * @return the string
     * @throws UnsupportedEncodingException
     *             the unsupported encoding exception
     */
    public static String urlEncoder( final String path ) throws UnsupportedEncodingException {
        return URLEncoder.encode( path, ClassMagsMigrationASRPConstants.ENCODE_FORMAT );
    }

    /**
     * Gets the social operation result.
     *
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @param httpStatusCode
     *            the http status code
     * @param httpStatusMessage
     *            the http status message
     * @param ugcResource
     *            the ugc resource
     * @param scfManager
     *            the scf manager
     * @param ugcResourceType
     *            the ugc resource type
     * @return the social operation result
     */
    public static SocialOperationResult getSocialOperationResult( SlingHttpServletRequest slingHttpServletRequest,
            int httpStatusCode, String httpStatusMessage, final Resource ugcResource,
            SocialComponentFactoryManager scfManager, String ugcResourceType ) {

        final long startTime = System.currentTimeMillis();

        String resourcePath;
        int resulthttpStatusCode = httpStatusCode;
        String resulthttpStatusMessage = httpStatusMessage;

        if ( null == ugcResource ) {
            resulthttpStatusCode = HttpServletResponse.SC_BAD_REQUEST;
            resulthttpStatusMessage = ClassMagsMigrationASRPConstants.HTTP_FAILURE_MESSAGE;
            resourcePath = slingHttpServletRequest.getResource().getPath();
        } else {
            resourcePath = ugcResource.getPath();
        }

        SocialComponent socialComponent = getSocialComponent( slingHttpServletRequest, ugcResource, scfManager,
                ugcResourceType );

        LOG.info( CommonUtils.addDelimiter( "Method SocialASRPUtils.getSocialOperationResult TimeMillis: "
                + ( System.currentTimeMillis() - startTime ) ) );

        return new SocialOperationResult( socialComponent, resulthttpStatusMessage, resulthttpStatusCode,
                resourcePath );
    }

    /**
     * Gets the social component.
     *
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @param ugcResource
     *            the ugc resource
     * @param scfManager
     *            the scf manager
     * @param ugcResourceType
     *            the ugc resource type
     * @return the social component
     */
    public static SocialComponent getSocialComponent( SlingHttpServletRequest slingHttpServletRequest,
            final Resource ugcResource, SocialComponentFactoryManager scfManager, String ugcResourceType ) {

        final long startTime = System.currentTimeMillis();

        SocialComponent socialComponent = null;

        final SocialComponentFactory socialComponentFactory = scfManager.getSocialComponentFactory( ugcResourceType );

        if ( null != socialComponentFactory ) {
            socialComponent = ugcResource != null
                    ? socialComponentFactory.getSocialComponent( ugcResource, slingHttpServletRequest )
                    : socialComponentFactory.getSocialComponent( slingHttpServletRequest.getResource(),
                            slingHttpServletRequest );
        }

        LOG.info( CommonUtils.addDelimiter( "Method SocialASRPUtils.getSocialComponent TimeMillis: "
                + ( System.currentTimeMillis() - startTime ) ) );

        return socialComponent;
    }

    /**
     * Path builder.
     *
     * @param nodes
     *            the nodes
     * @return the string
     */
    public static String pathBuilder( String... nodes ) {

        final long startTime = System.currentTimeMillis();

        StringBuilder nodePath = new StringBuilder( SocialUtils.SRP_CLOUD_PATH );
        if ( nodes != null && nodes.length > NumberUtils.INTEGER_ZERO ) {
            for ( int nodeCounter = NumberUtils.INTEGER_ZERO; nodeCounter < nodes.length; nodeCounter++ ) {
                if ( nodeCounter < nodes.length
                        && !nodes[ nodeCounter ].startsWith( ClassMagsMigrationConstants.FORWARD_SLASH ) ) {
                    nodePath = nodePath.append( ClassMagsMigrationConstants.FORWARD_SLASH )
                            .append( nodes[ nodeCounter ] );
                } else {
                    nodePath = nodePath.append( nodes[ nodeCounter ] );
                }
            }

            LOG.debug( CommonUtils.addDelimiter(
                    "Method SocialASRPUtils.pathBuilder TimeMillis: " + ( System.currentTimeMillis() - startTime ) ) );

            return nodePath.toString();
        }

        LOG.debug( CommonUtils.addDelimiter(
                "Method SocialASRPUtils.pathBuilder TimeMillis: " + ( System.currentTimeMillis() - startTime ) ) );

        return StringUtils.EMPTY;
    }

    /**
     * Creates the social node.
     *
     * @param srp
     *            the srp
     * @param resolver
     *            the resolver
     * @param path
     *            the path
     * @throws PersistenceException
     *             the persistence exception
     */
    public static void createSocialNode( SocialResourceProvider srp, ResourceResolver resolver, String path )
            throws PersistenceException {

        final long startTime = System.currentTimeMillis();

        Resource ugcResource = null;
        try {
            LOG.info( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.SOCIAL_PATH ) + path );
            ugcResource = srp.getResource( resolver, path );
            LOG.info( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.INTIAL_UGC_RESOURCE + ugcResource ) );
        } catch ( SlingException se ) {
            LOG.error( ClassMagsMigrationASRPConstants.RESOURCE_PATH_ERROR + se );
        }
        if ( null == ugcResource ) {
            Map< String, Object > props = new HashMap<>();
            props.put( JcrConstants.JCR_PRIMARYTYPE, JcrConstants.NT_UNSTRUCTURED );
            ugcResource = srp.create( resolver, path, props );
            srp.commit( resolver );
            resolver.commit();
        }

        LOG.debug( CommonUtils.addDelimiter(
                "Method SocialASRPUtils.createSocialNode TimeMillis: " + ( System.currentTimeMillis() - startTime ) ) );

    }

    /**
     * Gets the UGC resource from path.
     *
     * @param resolver
     *            the resolver
     * @param path
     *            the path
     * @param srp
     *            the srp
     * @return the UGC resource from path
     */
    public static Resource getUGCResourceFromPath( ResourceResolver resolver, String path,
            SocialResourceProvider srp ) {

        final long startTime = System.currentTimeMillis();

        Resource ugcResource = null;
        try {
            ugcResource = srp.getResource( resolver, path );
            LOG.info( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.UGC_RESOURCE + ugcResource ) );
        } catch ( SlingException se ) {
            LOG.error( ClassMagsMigrationASRPConstants.RESOURCE_PATH_ERROR + se );
        }

        LOG.debug( CommonUtils.addDelimiter( "Method SocialASRPUtils.getUGCResourceFromPath TimeMillis: "
                + ( System.currentTimeMillis() - startTime ) ) );

        return ugcResource;
    }

    /**
     * Checks if is valid user.
     *
     * @param userRole
     *            the user role
     * @param userId
     *            the user id
     * @return true, if is valid user
     */
    public static boolean isValidUser( String userRole, String userId ) {

        final long startTime = System.currentTimeMillis();

        boolean isValidUser = false;
        if ( StringUtils.isNotBlank( userRole ) && StringUtils.isNotBlank( userId )
                && !StringUtils.equals( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS, userRole ) ) {
            isValidUser = true;
        }

        LOG.debug( CommonUtils.addDelimiter(
                "Method SocialASRPUtils.isValidUser TimeMillis: " + ( System.currentTimeMillis() - startTime ) ) );

        return isValidUser;
    }

    /**
     * Fetch user id.
     *
     * @param slingHttpServletRequest
     *            the sling http servlet request
     * @param userRole
     *            the user role
     * @return the string
     */
    public static String fetchUserId( SlingHttpServletRequest slingHttpServletRequest, String userRole ) {

        final long startTime = System.currentTimeMillis();

        String userId = StringUtils.EMPTY;
        if ( StringUtils.equals( userRole, RoleUtil.ROLE_TYPE_TEACHER ) ) {
            userId = null != CookieUtil.fetchUserIdCookieData( slingHttpServletRequest ) ? CookieUtil
                    .fetchUserIdCookieData( slingHttpServletRequest ).getObjUIdHeaderIdentifiers().getStaffId()
                    : StringUtils.EMPTY;
        }
        if ( StringUtils.equals( userRole, RoleUtil.ROLE_TYPE_STUDENT ) ) {
            userId = null != CookieUtil.fetchUserIdCookieData( slingHttpServletRequest ) ? CookieUtil
                    .fetchUserIdCookieData( slingHttpServletRequest ).getObjUIdHeaderIdentifiers().getStudentId()
                    : StringUtils.EMPTY;
        }
        LOG.info( CommonUtils.addDelimiter( ClassMagsMigrationASRPConstants.USER_ID + userId ) );

        LOG.debug( CommonUtils.addDelimiter(
                "Method SocialASRPUtils.fetchUserId TimeMillis: " + ( System.currentTimeMillis() - startTime ) ) );

        return userId;
    }
}
