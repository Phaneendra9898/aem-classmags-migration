package com.scholastic.classmags.migration.utils;

/**
 * The Class ClassMagsMigrationASRPConstants.
 */
public class ClassMagsMigrationASRPConstants {

    /** The Constant SOCIAL_ADD_BOOKMARK_OPERATION. */
    public static final String SOCIAL_ADD_BOOKMARK_OPERATION = "social:bookmark:addbookmark";

    /** The Constant SOCIAL_DELETE_BOOKMARK_OPERATION. */
    public static final String SOCIAL_DELETE_BOOKMARK_OPERATION = "social:bookmark:deletebookmark";

    /** The Constant SOCIAL_GET_ALL_SITE_BOOKMARK_OPERATION. */
    public static final String SOCIAL_GET_ALL_SITE_BOOKMARK_OPERATION = "social:bookmark:getAllSite";

    /** The Constant SOCIAL_GET_MY_BOOKMARKS_OPERATION. */
    public static final String SOCIAL_GET_MY_BOOKMARKS_OPERATION = "social:bookmark:getMyBookmarks";

    /** The Constant SOCIAL_CUSTOM_DELETE_BOOKMARK_OPERATION. */
    public static final String SOCIAL_CUSTOM_DELETE_BOOKMARK_OPERATION = "social:bookmark:customDeleteBookmark";

    /** The Constant HTTP_SUCCESS_MESSAGE. */
    public static final String HTTP_SUCCESS_MESSAGE = "SUCCESS";

    /** The Constant HTTP_FAILURE_MESSAGE. */
    public static final String HTTP_FAILURE_MESSAGE = "FAILURE";

    /** The Constant OPERATION_START. */
    public static final String OPERATION_START = "########################START OF OPERATION########################";

    /** The Constant OPERATION_END. */
    public static final String OPERATION_END = "########################END OF OPERATION########################";

    /** The Constant DEFAULT_PATH_FOR_USER_DATA. */
    public static final String DEFAULT_PATH_FOR_USER_DATA = "/content/classroom_magazines/home/users";

    /** The Constant BOOKMARK_PAGE_PATH. */
    public static final String BOOKMARK_PAGE_PATH = "bookmarkPagePath";

    /** The Constant BOOKMARK_TEACHING_RESOURCE_PATH. */
    public static final String BOOKMARK_TEACHING_RESOURCE_PATH = "bookmarkedTeachingResourcePath";

    /** The Constant VIEW_ARTICLE_LINK_PATH. */
    public static final String VIEW_ARTICLE_LINK_PATH = "viewArticleLinkPath";

    /** The Constant ENCODE_FORMAT. */
    public static final String ENCODE_FORMAT = "UTF-8";

    /** The Constant RESOURCE_RESOLVER_OBJECT. */
    public static final String RESOURCE_RESOLVER_OBJECT = "Resource Resolver Object";

    /** The Constant RESOURCE_PATH_ERROR. */
    public static final String RESOURCE_PATH_ERROR = "Problem in Accessing Path";

    /** The Constant SOCIAL_PATH. */
    public static final String SOCIAL_PATH = "Path to the Social Resource";

    /** The Constant UGC_RESOURCE. */
    public static final String UGC_RESOURCE = "USER GENERATED RESOURCE OBJECT: ";

    /** The Constant INTIAL_UGC_RESOURCE. */
    public static final String INTIAL_UGC_RESOURCE = "INTIAL UGC OBJECT: ";

    /** The Constant UGC_RESOURCE_PROPERTIES. */
    public static final String UGC_RESOURCE_PROPERTIES = "USER GENERATED RESOURCE PROPERTIES: ";

    /** The Constant APP_NAME. */
    public static final String APP_NAME = "appName";

    /** The Constant BOOKMARK_TYPE_PAGE. */
    public static final String BOOKMARK_TYPE_PAGE = "PAGE";

    /** The Constant BOOKMARK_TYPE_ASSET. */
    public static final String BOOKMARK_TYPE_ASSET = "ASSET";

    /** The Constant UNSUPPORTED_ENCODING. */
    public static final String UNSUPPORTED_ENCODING = "Unable to Encode the given path with format";

    /** The Constant OPERATION. */
    public static final String OPERATION = ":operation";

    /** The Constant SOCIAL_SEARCH_OFFSET. */
    public static final int SOCIAL_SEARCH_OFFSET = 0;

    /** The Constant SOCIAL_SEARCH_INTIAL_LIMIT. */
    public static final int SOCIAL_SEARCH_INTIAL_LIMIT = 100;

    /** The Constant JSON_KEY_BOOKMARK_PATH. */
    public static final String JSON_KEY_BOOKMARK_PATH = "bookmarkPath";

    /** The Constant JSON_KEY_DATE_SAVED. */
    public static final String JSON_KEY_DATE_SAVED = "dateSaved";

    /** The Constant JSON_KEY_VIEW_ARTICLE_LINK. */
    public static final String JSON_KEY_VIEW_ARTICLE_LINK = "viewArticleLink";

    /** The Constant USER_ID. */
    public static final String USER_ID = "userId";

    /** The Constant VOTE_DATA. */
    public static final String VOTE_DATA = "voteData";

    /** The Constant JSON_KEY_VOTE_PAGE_PATH. */
    public static final String JSON_KEY_VOTE_PAGE_PATH = "votePagePath";

    /** The Constant JSON_KEY_QUESTION_UUID. */
    public static final String JSON_KEY_QUESTION_UUID = "questionUUID_t";

    /** The Constant QUESTION_UUID. */
    public static final String QUESTION_UUID = "questionUUID";

    /** The Constant JSON_KEY_ANSWER_OPTIONS. */
    public static final String JSON_KEY_ANSWER_OPTIONS = "answerOptions_ts";

    /** The Constant ANSWER_OPTIONS. */
    public static final String ANSWER_OPTIONS = "answerOptions";

    /** The Constant JSON_KEY_DISABLE_VOTING_DATE. */
    public static final String JSON_KEY_DISABLE_VOTING_DATE = "disableVotingDate";

    /** The Constant SOCIAL_SUBMIT_VOTE_OPERATION. */
    public static final String SOCIAL_SUBMIT_VOTE_OPERATION = "social:voting:submitVote";

    /** The Constant SOCIAL_GET_VOTE_DATA_OPERATION. */
    public static final String SOCIAL_GET_VOTE_DATA_OPERATION = "social:voting:getVoteData";

    /** The Constant USER_ROLE. */
    public static final String USER_ROLE = "userRole_t";

    /** The Constant STUDENT_VOTE_LIMIT. */
    public static final int STUDENT_VOTE_LIMIT = 100;

    /** The Constant TEACHER_VOTE_LIMIT. */
    public static final int TEACHER_VOTE_LIMIT = 200;

    /** The Constant RESOURCE_TYPE_SOCIAL. */
    public static final String RESOURCE_TYPE_SOCIAL = "social:asiResource";

    /** The Constant RESOURCE_TYPE_CLOUD_SOCIAL. */
    public static final String RESOURCE_TYPE_CLOUD_SOCIAL = "nt:adobesocialtype";

    /**
     * Instantiates a new class mags migration ASRP constants.
     */
    private ClassMagsMigrationASRPConstants() {
    }
}
