package com.scholastic.classmags.migration.controllers;

import java.util.List;

import org.apache.felix.scr.annotations.Reference;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.models.FooterObject;
import com.scholastic.classmags.migration.models.SocialLinks;
import com.scholastic.classmags.migration.services.FooterService;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * Use Class for Global Footer.
 */
public class GlobalFooterUse extends WCMUsePojo {

    /** The footer objects. */
    private List< FooterObject > footerObjects;

    /** The social links. */
    private SocialLinks socialLinks;
    
    /** The user type. */
    private String userType;

    /** The footer service. */
    @Reference
    FooterService footerService;
    
    /** The magazine props service. */
    MagazineProps magazineProps;

    /**
     * Gets the list of footer objects.
     *
     * @return the footer objects
     */
    public List< FooterObject > getFooterObjects() {
        return footerObjects;
    }

    /**
     * Gets the social links.
     *
     * @return the social links
     */
    public SocialLinks getSocialLinks() {
        return socialLinks;
    }
    
    /**
     * Gets the user type.
     *
     * @return the user type
     */
    public String getUserType() {
        return userType;
    }

    @Override
    public void activate() throws Exception {
        SlingHttpServletRequest request = getRequest();
        Resource currentResource = request.getResource();
        userType = ( String ) RoleUtil.getUserRole( getRequest() );
        if ( null != currentResource ) {
            footerService = getSlingScriptHelper().getService( FooterService.class );
            magazineProps = getSlingScriptHelper().getService( MagazineProps.class );
            String appName = magazineProps.getMagazineName( currentResource.getPath() );
            footerObjects = footerService.getFooterObjects( currentResource, appName );
            socialLinks = footerService.getSocialLinks( currentResource, appName );
        }
    }
}
