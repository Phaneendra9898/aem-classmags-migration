package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.SubscriptionBenefits;
import com.scholastic.classmags.migration.services.SubscriptionBenefitsService;

/**
 * Use Class for Subscription Benefits.
 */
public class SubscriptionBenefitsUse extends WCMUsePojo {

    private List< SubscriptionBenefits > subscriptionBenefitsDataList;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( SubscriptionBenefitsUse.class );

    /**
     * Gets the subsciptionBenefits list
     *
     * @return the subsciptionBenefits
     */
    public List< SubscriptionBenefits > getSubscriptionBenefitsDataList() {
        return subscriptionBenefitsDataList;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() {

        List< ValueMap > subscriptionBenefitsMapList;
        subscriptionBenefitsDataList = new ArrayList<>();
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        Page currentPage = getCurrentPage();
        if ( null != slingScriptHelper ) {
            try {

                String currentPath = currentPage.getPath();
                SubscriptionBenefitsService subscriptionBenefitsService = slingScriptHelper
                        .getService( SubscriptionBenefitsService.class );
                if ( null != subscriptionBenefitsService ) {
                    subscriptionBenefitsMapList = subscriptionBenefitsService.fetchSubscriptionBenefits( currentPath );
                    addItemsToSubscriptionBenefitsList( subscriptionBenefitsMapList );
                }
            } catch ( ClassmagsMigrationBaseException e ) {
                LOG.error( e.getMessage(), e );
            }
        }
    }

    /**
     * Adds the items to subsciptionBenefits data list.
     *
     * @param subscriptionBenefitsMapList
     *            the subscriptionBenefits map list
     */
    private void addItemsToSubscriptionBenefitsList( List< ValueMap > subscriptionBenefitsMapList ) {
        for ( ValueMap subscriptionBenefitsMap : subscriptionBenefitsMapList ) {
            SubscriptionBenefits subscriptionBenefits = new SubscriptionBenefits();

            subscriptionBenefits
                    .setSubscriptionBenefitIcon( subscriptionBenefitsMap.get( "icons", StringUtils.EMPTY ) );
            subscriptionBenefits.setSubscriptionBenefitTitle(
                    subscriptionBenefitsMap.get( "subscriptionBenefitTitle", StringUtils.EMPTY ) );
            subscriptionBenefits.setSubscriptionBenefitDescription(
                    subscriptionBenefitsMap.get( "subscriptionBenefitDescription", StringUtils.EMPTY ) );

            subscriptionBenefitsDataList.add( subscriptionBenefits );

        }
    }
}
