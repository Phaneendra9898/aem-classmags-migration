package com.scholastic.classmags.migration.services;

import javax.jcr.RepositoryException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.commons.json.JSONObject;

import com.day.cq.wcm.api.Page;

/*
 * The Scholastic Data Layer Service
 */
public interface ScholasticDataLayerService {
    /**
     * This method is used to create data layer using pagePath.
     * 
     * @param pagePath The page path.
     * @return The data layer.
     * @throws RepositoryException The repository exception.
     */
    public JSONObject createDataLayer(SlingHttpServletRequest request, Page currentPage );
}
