package com.scholastic.classmags.migration.services.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.NameConstants;
import com.google.gson.Gson;
import com.scholastic.classmags.migration.models.Issue;
import com.scholastic.classmags.migration.models.IssueList;
import com.scholastic.classmags.migration.models.PastIssuesObject;
import com.scholastic.classmags.migration.models.TeachingResourcesType;
import com.scholastic.classmags.migration.models.Tile;
import com.scholastic.classmags.migration.models.TileTitleSection;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PastIssuesService;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;
import com.scholastic.classmags.migration.utils.SortingDateComparator;

/**
 * The Class PastIssuesServiceImpl.
 */
@Component( immediate = true, metatype = false )
@Service( PastIssuesService.class )
@Properties( {
        @Property( name = Constants.SERVICE_DESCRIPTION, value = "This service returns past issues and their data" ) })
public class PastIssuesServiceImpl implements PastIssuesService {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( PastIssuesServiceImpl.class );

    /** The query builder. */
    @Reference
    QueryBuilder queryBuilder;

    /** The resource resolver factory. */
    @Reference
    ResourceResolverFactory resourceResolverFactory;

    /** The magazine props. */
    @Reference
    MagazineProps magazineProps;
    
    /** The resource path config service. */
    @Reference
    ResourcePathConfigService resourcePathConfigService;

    @Reference
    Externalizer externalizer;

    /** The property config service. */
    @Reference
    PropertyConfigService propertyConfigService;
    
    /*
     * (non-Javadoc)
     * 
     * @see com.scholastic.classmags.migration.services.PastIssuesService#
     * getPastIssuesJson(java.lang.String)
     */
    @Override
    public String getPastIssuesJson( String currentPagePath ) {

        // create query description as hash map (simplest way, same as form
        // post)
        Map< String, String > map = new HashMap< >();

        // create query description as hash map (simplest way, same as form
        // post)
        map.put( "path", getIssuesPath( currentPagePath ) );
        map.put( "type", NameConstants.NT_PAGE );
        map.put( "property", "jcr:content/cq:template" );
        map.put( "property.value", ClassMagsMigrationConstants.ISSUE_PAGE_TEMPLATE_PATH );
        map.put( "p.limit", "-1" );

        ResourceResolver resourceResolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );

        Session session = CommonUtils.getSession( resourceResolver );

        Query query = queryBuilder.createQuery( PredicateGroup.create( map ), session );

        SearchResult searchResult = query.getResult();

        List< Hit > hits = searchResult.getHits();

        return buildPastIssuesGrid( hits, currentPagePath );
    }

    /**
     * Builds the past issues grid.
     * 
     * @param hits
     *            the hits
     * @return the string
     */
    private String buildPastIssuesGrid( List< Hit > hits, String currentPagePath ) {

        ResourceResolver resourceResolver = null;
        Issue issue;
        List< Issue > issueList = new ArrayList< >();

        for ( Hit hit : hits ) {
            issue = new Issue();
            try {
                String issuePath = hit.getPath();
                resourceResolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                        ClassMagsMigrationConstants.READ_SERVICE );
                Resource issueNode = resourceResolver.getResource( issuePath );
                Resource issueJcrNode = issueNode != null ? issueNode.getChild( JcrConstants.JCR_CONTENT ) : null;
                Resource featuredContentResource = issueJcrNode != null ? issueJcrNode.getChild( "issue-configuration" )
                        : null;
                List< ValueMap > featuredContentList = new ArrayList< >();
                if ( featuredContentResource != null ) {
                    featuredContentList = CommonUtils.fetchMultiFieldData( featuredContentResource,
                            "featuredResources" );
                }
                List< Tile > featuredList = new ArrayList< >();
                for ( ValueMap featuredContent : featuredContentList ) {
                    String path = featuredContent.get( "resourcePath", String.class );
                    String resourceType = featuredContent.get( "resourceType", String.class );
                    featuredList.add( buildFeaturedResources( resourceType, path ) );
                }
                ValueMap issueJcrValueMap = ( null != issueJcrNode ) ? issueJcrNode.getValueMap() : null;
                String dataPagePath = CommonUtils.getDataPath( resourcePathConfigService,
                        magazineProps.getMagazineName( currentPagePath ) );
                String dateFormat = CommonUtils.getDateFormat( propertyConfigService, dataPagePath );
                String sortingDate = CommonUtils.getSortingDate( issueJcrNode, dateFormat );
                String displayDate = CommonUtils.getDisplayDate( issueJcrValueMap, dateFormat );
               
                String fileReference = ( null != issueJcrValueMap )
                        ? issueJcrValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY )
                        : StringUtils.EMPTY;

                issue.setIssuedate( sortingDate );
                issue.setImgsrc( InternalURLFormatter.formatURL( resourceResolver, fileReference ) );
                issue.setDateFormat( dateFormat );
                issue.setLinkto( InternalURLFormatter.formatURL( resourceResolver, issuePath ) );
                issue.setTiles( featuredList );
                issue.setIssueDisplayDate( displayDate );

                issueList.add( issue );

            } catch ( Exception e ) {
                LOG.error( "::::Exception::::" + e );
            } finally {
                if ( resourceResolver != null ) {
                    resourceResolver.close();
                }
            }
        }

        Collections.sort( issueList, new SortingDateComparator() );
        Collections.reverse( issueList );

        IssueList issueListObj = new IssueList();
        issueListObj.setIssue( issueList );

        PastIssuesObject pastIssuesObject = new PastIssuesObject();
        pastIssuesObject.setIssueList( issueListObj );

        Gson gson = new Gson();
        return gson.toJson( pastIssuesObject );

    }

    private Tile buildFeaturedResources( String resourceType, String path ) {
        Tile tile = null;
        String contentType = TeachingResourcesType.getResourceContentType( resourceType );
        if ( StringUtils.equalsIgnoreCase( contentType, ClassMagsMigrationConstants.ASSET ) ) {
            tile = getAssetData( path );
        } else if ( StringUtils.equalsIgnoreCase( contentType, ClassMagsMigrationConstants.PAGE ) ) {
            tile = getPageData( path );
        }
        if ( null != tile ) {
            tile.setTileType( resourceType );
        }
        return tile;

    }

    private Tile getPageData( String path ) {
        Tile tileObject = null;
        Resource pageResource = CommonUtils.getResource( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE, path );
        Resource jcrContent = pageResource != null ? pageResource.getChild( JcrConstants.JCR_CONTENT ) : null;
        ValueMap properties = jcrContent != null ? jcrContent.getValueMap() : null;
        if ( properties != null ) {
            tileObject = setPageMetadata( properties, jcrContent, path );
        }
        return tileObject;
    }

    private Tile setPageMetadata( ValueMap properties, Resource resource, String path ) {

        Tile tileObject = new Tile();
        tileObject.setTileimgsrc( properties.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) );
        TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );
        List< String > tags = CommonUtils.getSubjectTags( resource, tagManager );
        if ( !tags.isEmpty() ) {
            tileObject.setTilesubtitletext( tags );
        }
        tileObject.setTiledesctext( properties.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) );
        String tileLinkTo = externalizer.externalLink(
                CommonUtils.getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ),
                magazineProps.getMagazineName( path ), InternalURLFormatter.formatURL( CommonUtils.getResourceResolver( resourceResolverFactory,
                        ClassMagsMigrationConstants.READ_SERVICE ), path ) );
        tileObject.setTilelinkto( tileLinkTo );

        TileTitleSection tileTitleSection = new TileTitleSection();
        tileTitleSection.setTiletitlelinkto( InternalURLFormatter.formatURL(
                CommonUtils.getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ),
                path ) );
        tileTitleSection.setTiletitletext( properties.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) );
        tileTitleSection.setTiletitlelinkto( path );
        tileObject.setTiletitlesection( tileTitleSection );
        tileObject.setTilevideoduration( "" );

        return tileObject;
    }

    private Tile getAssetData( String path ) {
        Tile tileObject = null;
        Resource assetResource = CommonUtils.getResource( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE, path );
        Asset asset = assetResource != null ? assetResource.adaptTo( Asset.class ) : null;
        Resource metadataNode = assetResource != null
                ? assetResource.getChild( ClassMagsMigrationConstants.ASSET_METADATA_NODE ) : null;
        if ( asset != null ) {
            tileObject = setAssetMetadata( asset, metadataNode );
            String tileLinkTo = externalizer.externalLink(
                    CommonUtils.getResourceResolver( resourceResolverFactory,
                            ClassMagsMigrationConstants.READ_SERVICE ),
                    magazineProps.getMagazineName( path ),
                    InternalURLFormatter.formatURL( CommonUtils.getResourceResolver( resourceResolverFactory,
                            ClassMagsMigrationConstants.READ_SERVICE ), path ) );
            tileObject.setTilelinkto( tileLinkTo );
        }
        return tileObject;
    }

    private Tile setAssetMetadata( Asset asset, Resource metadataNode ) {
        Tile tileObject = new Tile();
        tileObject.setTileimgsrc( asset.getPath() + ClassMagsMigrationConstants.IMAGE_TILE_RENDITION );
        tileObject.setTiledesctext( asset.getMetadataValue( DamConstants.DC_DESCRIPTION ) );

        TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );
        List< String > tags = CommonUtils.getSubjectTags( metadataNode, tagManager );
        if ( !tags.isEmpty() ) {
            tileObject.setTilesubtitletext( tags );
        }
        tileObject.setTilevideoduration( "" );
        if ( metadataNode != null && metadataNode.getValueMap().containsKey( "videoDuration" ) ) {
            tileObject.setTilevideoduration( metadataNode.getValueMap().get( "videoDuration", StringUtils.EMPTY ) );
        }
        if ( metadataNode != null && metadataNode.getValueMap().containsKey( "videoId" ) ) {
            tileObject.setVideoId( metadataNode.getValueMap().get( "videoId", Long.class ) );
        }
        TileTitleSection tileTitleSection = new TileTitleSection();
        tileTitleSection.setTiletitletext(
                StringUtils.defaultIfBlank( asset.getMetadataValue( DamConstants.DC_TITLE ), asset.getName() ) );
        tileTitleSection.setTiletitlelinkto( InternalURLFormatter.formatURL(
                CommonUtils.getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ),
                asset.getPath() ) );

        tileObject.setTiletitlesection( tileTitleSection );
        return tileObject;
    }

    /**
     * Gets the issues path.
     * 
     * @param currentPagePath
     *            the current page path
     * @return the issues path
     */
    private String getIssuesPath( String currentPagePath ) {
        String currentMagazineName = magazineProps.getMagazineName( currentPagePath );
        return ClassMagsMigrationConstants.CLASSROOM_MAGAZINES_PATH + "/" + currentMagazineName + "/"
                + ClassMagsMigrationConstants.ISSUES;
    }
}
