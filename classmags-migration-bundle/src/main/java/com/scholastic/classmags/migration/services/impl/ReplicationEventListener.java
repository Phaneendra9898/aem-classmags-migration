package com.scholastic.classmags.migration.services.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Deactivate;
import org.apache.felix.scr.annotations.Modified;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyUnbounded;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ResourceUtil;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.scheduler.ScheduleOptions;
import org.apache.sling.commons.scheduler.Scheduler;
import org.apache.sling.settings.SlingSettingsService;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventConstants;
import org.osgi.service.event.EventHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.Externalizer;
import com.day.cq.dam.commons.util.DamUtil;
import com.day.cq.replication.ReplicationAction;
import com.day.cq.tagging.TagManager;
import com.scholastic.classmags.migration.models.DAMMetadata;
import com.scholastic.classmags.migration.models.PageContent;
import com.scholastic.classmags.migration.models.ResourcesType;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;
import com.scholastic.classmags.migration.utils.SolrSearchClassMagsConstants;
import com.scholastic.classmags.migration.utils.SolrSearchQueryConstants;


/**
 * Event listener class for listening to replication events and triggers a job
 * that updates Solr index.
 *
 * @see EventHandler
 */
@Component( metatype = true, immediate = true, label = "Class Magazines - Event Listener", description = "Event Listener for updating solr index based on the event." )
@Service
@Properties( { @Property( name = EventConstants.EVENT_TOPIC, value = ReplicationAction.EVENT_TOPIC ),
        @Property( name = SolrSearchClassMagsConstants.ENABLED, boolValue = true, label = "Enabled", description = "Enable the default Solr Index Listener" ),
        @Property( name = SolrSearchClassMagsConstants.OBSERVED_PATHS, value = { "/content/classroom_magazines" }, 
        label = "Observed Repository Paths", description = "The paths on which this listener is active", unbounded=PropertyUnbounded.ARRAY ) })
public class ReplicationEventListener implements EventHandler {

    /** The Constant log. */
    private static final Logger LOG = LoggerFactory.getLogger( ReplicationEventListener.class );

    /** The sling settings. */
    @Reference
    private SlingSettingsService slingSettings;
    
    /** The url externalizer. */
    @Reference
    private Externalizer externalizer;

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /** The index service. */
    @Reference
    private ClassMagsSolrIndexService indexService;

    @Reference
    private MagazineProps magazineProps;

    /** The prop config service. */
    @Reference
    private PropertyConfigService propertyConfigService;
    
    /** The resource path config service. */
    @Reference
    private ResourcePathConfigService resourcePathConfigService;
    
    /** The service enabled. */
    private Boolean serviceEnabled;

    /** The observed paths. */
    private String[] observedPaths;

    @Reference
    private Scheduler scheduler;
    
    private static final Calendar DEFAULT_SORTING_DATE = Calendar.getInstance();
    
    static {
    	DEFAULT_SORTING_DATE.set(1999, Calendar.JANUARY, 1, 10, 11, 12);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.osgi.service.event.EventHandler#handleEvent(org.osgi.service.event.
     * Event)
     */
    @Override
    public void handleEvent( Event event ) {

        if ( isAuthorMode() ) {

            if ( null == event || pageIsNotIndexable() ) {
                return;
            }
            ReplicationAction action = ReplicationAction.fromEvent( event );

            String modificationPath = action.getPath();

            if ( pageIsNotInObservedPath( modificationPath ) ) {
                return;
            }
            handleReplicationAction( action, modificationPath );
        }
    }

    /**
     * Handle replication action.
     *
     * @param action
     *            the action
     * @param modificationPath
     *            the modification path
     */
    private void handleReplicationAction( ReplicationAction action, String modificationPath ) {
        if ( null != action.getType() && StringUtils.isNotBlank( modificationPath ) ) {
            String activationPath = action.getPath();
            LOG.debug( "Replication action {} occured on {} ", action.getType().getName(), activationPath );
            LOG.info( "**************************************************************************" );
            LOG.info( "Event : {}, Path : {}", action.getType(), action.getPath() );
            LOG.info( "**************************************************************************" );
            ScheduleOptions options = scheduler.NOW();
            options.canRunConcurrently( false );
            SolrIndexJob job = null;
            String jobName = this.getClass().getSimpleName().toString().replace( ".", "/" );
            
            switch ( action.getType() ) {
            case ACTIVATE:
                // Create a new job object (must be runnable).
                job = new SolrIndexJob( modificationPath, SolrSearchClassMagsConstants.OP_INDEX_UPDATE );
                // Assign a "unique" name for this job
                jobName = jobName +"/"+SolrSearchClassMagsConstants.OP_INDEX_UPDATE+"/" + modificationPath;
                break;
            case DEACTIVATE:
                job = new SolrIndexJob( modificationPath, SolrSearchClassMagsConstants.OP_INDEX_DELETE );
                jobName = jobName +"/"+SolrSearchClassMagsConstants.OP_INDEX_DELETE+"/" + modificationPath;
                break;
            default:
                LOG.debug( "In default" );
                break;
            }
            if ( job != null ) {
                // Create and schedule the job
                options.name( jobName );
                scheduler.schedule( job, options );
            }
        }
    }

    /**
     * Determines whether the page modification is a candidate for indexing.
     *
     * @return <code>true</code> if the page modification should be indexed, and
     *         <code>false</code> if the page should be excluded from indexing.
     */
    protected boolean pageIsIndexable() {

        if ( !serviceEnabled ) {
            LOG.debug( "DefaultSolrIndexListenerService not enabled. Ignoring indexing event" );
            return false;
        }

        return true;
    }

    /**
     * Page is not indexable.
     *
     * @return true, if successful
     */
    protected boolean pageIsNotIndexable() {
        return !pageIsIndexable();
    }

    /**
     * Determines whether the page modification is in the allowed list of
     * observed paths.
     *
     * @param pagePath
     *            Path of the modified page.
     * @return <code>true</code> if the page modification is in the observed
     *         path, and <code>false</code> otherwise.
     */
    protected boolean pageIsInObservedPath( String pagePath ) {

        if ( null == pagePath || null == observedPaths ) {
            return false;
        }

        for ( String observedPath : observedPaths ) {
            if ( pagePath.startsWith( observedPath ) ) {
                LOG.info( "Page '{}' is in observed path '{}'", pagePath, observedPath );
                return true;
            }
        }

        return false;
    }

    /**
     * Page is not in observed path.
     *
     * @param pagePath
     *            the page path
     * @return true, if successful
     */
    protected boolean pageIsNotInObservedPath( String pagePath ) {
        return !pageIsInObservedPath( pagePath );
    }

    /**
     * Activate.
     *
     * @param config
     *            the config
     */
    @Activate
    protected void activate( final Map< String, Object > config ) {
        resetService( config );
    }

    /**
     * Modified.
     *
     * @param config
     *            the config
     */
    @Modified
    protected void modified( final Map< String, Object > config ) {
        resetService( config );
    }

    /**
     * Deactivate.
     *
     * @param config
     *            the config
     */
    @Deactivate
    protected void deactivate( final Map< String, Object > config ) {

    }

    /**
     * Reset service.
     *
     * @param config
     *            the config
     */
    private synchronized void resetService( final Map< String, Object > config ) {
        LOG.info( "Resetting default Solr index listener service using configuration: " + config );

        serviceEnabled = config.containsKey( SolrSearchClassMagsConstants.ENABLED )
                ? ( Boolean ) config.get( SolrSearchClassMagsConstants.ENABLED ) : false;

        if ( config.containsKey( SolrSearchClassMagsConstants.OBSERVED_PATHS ) ) {

            Object observedPathsValue = config.get( SolrSearchClassMagsConstants.OBSERVED_PATHS );
            if ( observedPathsValue instanceof String ) {
                LOG.info( "Observing single path" );
                observedPaths = new String[] { ( String ) observedPathsValue };
            } else if ( observedPathsValue instanceof String[] ) {
                LOG.info( "Observing multiple paths" );
                observedPaths = ( String[] ) observedPathsValue;
            } else {
                LOG.warn( "Unexpected value assigned to observed paths" );
                observedPaths = new String[] {};
            }

        } else {
            LOG.info( "No observed paths defined for listener" );
            observedPaths = new String[] {};
        }
    }

    /**
     * Helper method to check if current mode is authoring mode.
     * 
     * @return - flag whether run mode is author
     */
    private boolean isAuthorMode() {
        Set< String > runmodes = slingSettings.getRunModes();
        return runmodes.contains( "author" ) ? true : false;
    }

    private class SolrIndexJob implements Runnable {

        private final String path;
        private final String operation;
        ResourceResolver resourceResolver = null;

        /**
         * The constructor used to pass the path & type of indexing operation
         * 
         * @param path
         * @param operation
         */
        public SolrIndexJob( String path, String operation ) {
            // Maintain job state
            this.path = path;
            this.operation = operation;
        }

        /**
         * The entry point for initiating the indexing job.
         * 
         */
        @Override
        public void run() {

            final Map< String, Object > authInfo = Collections.singletonMap( ResourceResolverFactory.SUBSERVICE,
                    ( Object ) ClassMagsMigrationConstants.READ_SERVICE );

            try {
                LOG.debug( "Started Indexing JOB.. " );
                resourceResolver = resourceResolverFactory.getServiceResourceResolver( authInfo );
                LOG.debug( "operation is : {}", operation );
                switch ( operation ) {
                case SolrSearchClassMagsConstants.OP_INDEX_UPDATE:
                    List< PageContent > docs = addOrUpdatePage( path );
                    if ( null != docs & !docs.isEmpty() ) {
                        UpdateResponse response = indexService.addBeansAndCommit( docs );
                        LOG.info( "Indexed {} pages / assets with response {}", docs.size(), response );
                    }
                    break;
                case SolrSearchClassMagsConstants.OP_INDEX_DELETE:
                	indexService.deleteByQueryAndCommit( "id:"+path.replaceAll( "/", "\\\\/" ).concat("*") );
                	List< PageContent > docsToBeDeleted = addOrUpdatePage( path );
                	StringBuilder sb = new StringBuilder();
                	String deleteQuery = StringUtils.EMPTY;
                	for(PageContent doc: docsToBeDeleted){
                		sb.append(doc.getSolrIdQuery()).append(SolrSearchQueryConstants.SOLR_QUERY_OR);
                	}
                	int index = sb.lastIndexOf(SolrSearchQueryConstants.SOLR_QUERY_OR);
                	if(index>0){
                		deleteQuery = sb.substring(0, index);
                		LOG.debug("Delete query for associated documents {}",deleteQuery);
                	}
                	if(StringUtils.isNotBlank(deleteQuery)){
                		indexService.deleteByQueryAndCommit( deleteQuery );
                	}
                    break;
                }

            } catch ( LoginException e ) {
                LOG.error( "Could not get service resolver", e );
            } finally {
                // Always close resource resolvers you open
                if ( resourceResolver != null ) {
                    resourceResolver.close();
                }
                indexService.resetSolrClient();
                LOG.debug( "Started Indexing JOB.. " );
            }
        }

        /**
         * Adds the or update page.
         *
         * @param modificationPath
         *            the modification path
         */
        protected List< PageContent > addOrUpdatePage( String modificationPath ) {

            if ( null == resourceResolver ) {
                LOG.warn( "Can't perform indexing operation for '{}', resourceResolver is null", modificationPath );
                return null;
            }
            final Resource resource = resourceResolver.getResource( modificationPath );
            if ( null != resource && ResourceUtil.isNonExistingResource( resource ) ) {
                LOG.warn( "Can't perform indexing operation for '{}'. Resource does not exist.", modificationPath );
                return null;
            }
            Resource pageContent = resource.getChild( "jcr:content" );
            if ( null != pageContent && ResourceUtil.isNonExistingResource( pageContent ) ) {
                LOG.warn( "Can't perform indexing operation for '{}'. Resource does not exist.", modificationPath );
                return null;
            }
            String resourceType = pageContent.adaptTo( ValueMap.class ).get( "sling:resourceType", String.class );
            List< PageContent > docs = new ArrayList< PageContent >();
            switch ( resourceType ) {
            case ClassMagsMigrationConstants.ARTICLE_PAGE_RESOURCE_TYPE:
                docs = getDocumentListToBeIndexed( pageContent, true,
                        ClassMagsMigrationConstants.ARTICLE_RESOURCES_PATHS,
                        SolrSearchClassMagsConstants.FIELD_TYPE_ARTICLE );
                break;
            case ClassMagsMigrationConstants.ISSUE_PAGE_RESOURCE_TYPE:
                docs = getDocumentListToBeIndexed( pageContent, true, ClassMagsMigrationConstants.ISSUE_RESOURCES_PATHS,
                        SolrSearchClassMagsConstants.FIELD_TYPE_ISSUE );
                break;
            default:
                // Uncomment to index rest of the pages 
                /*docs = getDocumentListToBeIndexed( pageContent, false, null,
                        SolrSearchClassMagsConstants.FIELD_TYPE_PAGE );*/
            }
            return docs;
        }

        /**
         * 
         * @param resource
         *            The pageContent resource
         * @param indexAssociatedResources
         *            Indicating weather the associated resources should also be
         *            index
         * @param configurationNodePaths
         *            An array of the paths that point to the root node of
         *            associated resources.
         * @param type
         *            The type of content eg: Article, Issue etc...
         */
        private List< PageContent > getDocumentListToBeIndexed( final Resource resource,
                boolean indexAssociatedResources, String[] configurationNodePaths, String type ) {
            List< PageContent > docs = new ArrayList< PageContent >();
            if ( null != resource ) {
                PageContent pageContent = resource.adaptTo( PageContent.class );
                if ( null != pageContent ) {
                    pageContent.setId(
                            InternalURLFormatter.formatURL( resourceResolver, resource.getParent().getPath() ) );
                    pageContent.setBookmarkPath( resource.getParent().getPath() );
                    pageContent.setType(
                            CommonUtils.getContentTypeTag( resource, resourceResolver.adaptTo( TagManager.class ) ) );
                    pageContent.setSiteId( magazineProps.getMagazineName( pageContent.getId() ) );
                    pageContent.setShareUrl( externalizer.externalLink( resourceResolver,
                            magazineProps.getMagazineName( pageContent.getId() ), pageContent.getId() ) );
                    pageContent.setShareable( true );
                    pageContent.setDownloadable( false );
                    setSortingDate(pageContent, resource);
                    LOG.debug( "Resource is {} : {}", type, pageContent );
                    docs.add( pageContent );
                    if ( indexAssociatedResources && ArrayUtils.isNotEmpty( configurationNodePaths ) ) {
                        String pageType = pageContent.getType();
                        String pageUrl = null;
                        if ( StringUtils.isNotBlank( pageType ) ) {
                            pageUrl = pageType.startsWith( SolrSearchClassMagsConstants.FIELD_TYPE_ARTICLE )
                                    ? pageContent.getId() : null;
                        }
                        docs.addAll( getAssociatedDocumnetLiist( resource, configurationNodePaths, pageUrl,
                                pageContent.getSiteId() ) );
                    }
                }
            }
            return docs;
        }

        /**
         * Returns a List of associated assets ready for indexing, which are
         * associated with an article or an issue
         * 
         * @param resource
         * @param configurationNodePaths
         * @param currentPagePath
         * @return
         */
        private List< PageContent > getAssociatedDocumnetLiist( Resource resource, String[] configurationNodePaths,
                String currentPagePath, String siteId ) {
            List< PageContent > docs = new ArrayList< PageContent >();
            for ( String path : configurationNodePaths ) {
                Resource resources = resource.getChild( path );
                if ( resources != null ) {
                    Iterator< Resource > itr = resources.listChildren();
                    while ( itr.hasNext() ) {
                        Map< String, String > map = getContentMap( itr.next().adaptTo( ValueMap.class ) );
                        // Assume article, blogs will be published separately
                        /*
                         * if ( !map.isEmpty() && !map.values().contains( "Blog"
                         * ) && !map.values().contains( "Article" ) ) {
                         */
                        PageContent associatedResource = mapAssetToDocumnet(
                                ResourcesType.getResourceContentType(
                                        map.get( ClassMagsMigrationConstants.PN_RESOURCE_TYPE ) ),
                                map.get( ClassMagsMigrationConstants.PN_RESOURCE_PATH ), currentPagePath, siteId );
                        if ( associatedResource != null ) {
                            associatedResource.convertAllFieldsToPlainText();
                            docs.add( associatedResource );
                        }
                        // }
                    }
                }
            }
            LOG.info( "Associated resources : {}", docs.size() );
            return docs;
        }

        /**
         * Parses the content type & path from the property & returns a map of
         * resource type & path
         * 
         * @param props
         * @return
         */
        private Map< String, String > getContentMap( ValueMap props ) {
            Map< String, String > map = new HashMap< String, String >();
            String resourceType = StringUtils.EMPTY;
            String combinedProperty = props.get( ClassMagsMigrationConstants.PN_RESOURCE_TYPE, String.class );
            String resourcePath = props.get( ClassMagsMigrationConstants.PN_RESOURCE_PATH, String.class );
            if ( StringUtils.isNotBlank( resourcePath ) && StringUtils.isNotBlank( combinedProperty ) ) {
                resourceType = combinedProperty;
            } else if ( StringUtils.isBlank( resourcePath ) && StringUtils.isNotBlank( combinedProperty ) ) {
                String[] value = combinedProperty.split( "-", 2 );
                if ( ArrayUtils.isNotEmpty( value ) && value.length >= 2 ) {
                    resourceType = value[ 0 ].trim();
                    resourcePath = value[ 1 ].trim();
                }
            }
            map.put( ClassMagsMigrationConstants.PN_RESOURCE_TYPE, resourceType );
            map.put( ClassMagsMigrationConstants.PN_RESOURCE_PATH, resourcePath );
            return map;
        }

        /**
         * Prepare the asset meta data for indexing.
         * 
         * @param resourceType
         * @param resourcePath
         * @param pageUrl
         * @return
         */
        private PageContent mapAssetToDocumnet( String resourceType, String resourcePath, String pageUrl,
                String siteId ) {
            PageContent associatedResource = null;
            if ( StringUtils.isNotBlank( resourceType ) && StringUtils.isNotBlank( resourcePath ) ) {
                LOG.debug( "Path : {}, Type: {}", resourcePath, resourceType );
                Resource asset = null;
                // retrieve the asset and populate its meta-data, return the
                // object for indexing.
                asset = resourceResolver.getResource( resourcePath );
                if ( null != asset && DamUtil.isAsset( asset ) ) {
                    LOG.debug( "Found Asset Resource" );
                    Resource metadata = asset.getChild( ClassMagsMigrationConstants.ASSET_METADATA_NODE );
                    if ( metadata != null ) {
                        DAMMetadata damMetadata = metadata.adaptTo( DAMMetadata.class );
                        damMetadata.setSiteId( siteId );
                        damMetadata.setId( InternalURLFormatter.formatURL( resourceResolver, resourcePath ) );
                        damMetadata.setThumbnailPath(
                                damMetadata.getId().concat( ClassMagsMigrationConstants.IMAGE_TILE_RENDITION ) );
                        damMetadata.setArticlePath( pageUrl );
                        damMetadata.setType( CommonUtils.getContentTypeTag( metadata,
                                resourceResolver.adaptTo( TagManager.class ) ) );
                        if ( null!=damMetadata.getType() && damMetadata.getType().equalsIgnoreCase( ClassMagsMigrationConstants.GAMES ) ) {
                            addGameProperties( damMetadata, siteId );
                        }
                        if ( CommonUtils.isAShareableAsset( resourcePath ) ) {
                        	LOG.debug("Site Id for Externalizer : {}",siteId);
                            damMetadata.setShareUrl( externalizer.externalLink( resourceResolver, siteId , damMetadata.getId() ) );
                            damMetadata.setShareable( true );
                            damMetadata.setDownloadable( true );
                        }
                        associatedResource = new PageContent( damMetadata );
                        setSortingDate(associatedResource, metadata);
                        LOG.debug( "Associated resource is : {}, {}", associatedResource );
                    }
                }
            }
            return associatedResource;
        }
        
        private void addGameProperties( DAMMetadata damMetadata, String siteId ) {
            if ( null == damMetadata.getGameType() ) {
                damMetadata.setGameType( ClassMagsMigrationConstants.GAME_HTML );
                damMetadata.setGamePath( damMetadata.getId().concat( ClassMagsMigrationConstants.HTML_SUFFIX ) );
            } else {
                try {
                    String flashContainer = propertyConfigService.getDataConfigProperty(
                            ClassMagsMigrationConstants.FLASH_GAME_CONTAINER_PROPERTY,
                            CommonUtils.getDataPath( resourcePathConfigService, siteId ) );
                    if ( StringUtils.isNotBlank( flashContainer ) ) {
                        damMetadata.setFlashContainer( flashContainer );
                    }
                } catch ( LoginException e ) {
                    LOG.error( "Login Exception : {}", e );
                }
                String flashVars;
                if ( StringUtils.isNotBlank( damMetadata.getXmlPath() ) ) {
                    flashVars = "gametype=" + damMetadata.getGameType() + "&contentXML=" + damMetadata.getXmlPath()
                            + "&assetSWF=" + damMetadata.getId();
                } else {
                    flashVars = "gametype=" + damMetadata.getGameType() + "&assetSWF=" + damMetadata.getId();
                }
                if ( StringUtils.isNotBlank( flashVars ) ) {
                    damMetadata.setGamePath( flashVars );
                }
                damMetadata.setGameType( ClassMagsMigrationConstants.GAME_FLASH );
            }
        }
        
        private void setSortingDate(PageContent content, Resource resource){
        	String dateFormat = StringUtils.EMPTY;
            try {
                dateFormat = propertyConfigService.getDataConfigProperty(
                        ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                        CommonUtils.getDataPath( resourcePathConfigService, content.getSiteId() ) );
            } catch ( LoginException e ) {
                LOG.error( "Exception occured while retriving date format, e : {}", e );
            }
            LOG.debug("Date formart is {}",dateFormat);
            LOG.debug("Resource is {}",resource.getPath());
            LOG.debug("Resource is of type {}",content.getType());
            String sortingDate;
            if(resource!=null& resource.getPath().startsWith("/content/dam")){
                sortingDate = CommonUtils.getAssetSortingDate( resource, dateFormat );
            	content.setPublishedDate(CommonUtils.getAssetDisplayDate( resource, dateFormat ));
            }else{
                sortingDate = CommonUtils.getSortingDate( resource, dateFormat );
            	content.setPublishedDate(CommonUtils.getDisplayDate( resource, dateFormat ));
            }
            SimpleDateFormat formatter = new SimpleDateFormat(dateFormat.replaceAll( "YYYY", "yyyy" ));
            if(StringUtils.isNotBlank( sortingDate ) ){
                try {
                	content.setSortDate(formatter.parse( sortingDate ));
                } catch ( ParseException e ) {
                    LOG.error( "Error Parsing date : {}",e );
                	content.setSortDate(DEFAULT_SORTING_DATE.getTime());
                }
            }else{
            	content.setSortDate(DEFAULT_SORTING_DATE.getTime());
            }
            
        }

    }

}
