package com.scholastic.classmags.migration.services.impl;

import org.apache.commons.lang.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.NameConstants;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.services.IssueService;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * Issue service.
 */
@Component( immediate = true, metatype = true )
@Service( IssueService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Issue Service" ) })
public class IssueServiceImpl implements IssueService {
    
    private static final String ISSUE_TEMPLATE = "/apps/scholastic/classroom-magazines-migration/templates/issue-page";

    @Reference
    private PropertyConfigService propertyConfigService;

    @Reference
    private ResourcePathConfigService resourcePathConfigService;

    @Override
    public String getParentIssuePath( SlingHttpServletRequest request, Page currentPage ) {

        String parentIssuePath = StringUtils.EMPTY;

        String parentPath = currentPage.getParent().getPath();
        Resource parentResource = request.getResourceResolver().getResource( parentPath + "/"+ JcrConstants.JCR_CONTENT );

        if ( null != parentResource ) {
            ValueMap properties = parentResource.adaptTo( ValueMap.class );
            String parentType = properties.get( NameConstants.PN_TEMPLATE, String.class );
            if ( StringUtils.equalsIgnoreCase( parentType, ISSUE_TEMPLATE ) ) {
                parentIssuePath = InternalURLFormatter.formatURL( request.getResourceResolver(), parentPath );
            }

        }

        return parentIssuePath;
    }

    @Override
    public String getParentIssueDate( SlingHttpServletRequest request, Page currentPage, String appName )
            throws LoginException {

        String issueDate = StringUtils.EMPTY;
        String dateFormat = propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                CommonUtils.getDataPath( resourcePathConfigService, appName ) );
        String parentPath = currentPage.getParent().getPath();
        Resource parentResource = request.getResourceResolver().getResource( parentPath + "/"+ JcrConstants.JCR_CONTENT );

        if ( null != parentResource ) {
            ValueMap properties = parentResource.adaptTo( ValueMap.class );

            issueDate = CommonUtils.getDisplayDate( properties, dateFormat );
        }
        return issueDate;
    }

}
