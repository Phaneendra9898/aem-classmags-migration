package com.scholastic.classmags.migration.models;

/**
 * The Class MagazinesMetaData.
 */
public class MagazinesMetaData {

    /** The magazine name. */
    private String magazineName;

    /** The magazine path. */
    private String magazinePath;

    /** The magazine description. */
    private String magazineDescription;
    
    /** The magazine image path. */
    private String imagePath;

    /**
     * Gets the image path.
     *
     * @return the imagePath
     */
    public String getImagePath() {
		return imagePath;
	}

    /**
     * Sets the image path.
     *
     * @param imagePath
     *            the imagePath to set
     */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
     * Gets the magazine name.
     *
     * @return the magazineName
     */
    public String getMagazineName() {
        return magazineName;
    }

    /**
     * Sets the magazine name.
     *
     * @param magazineName
     *            the magazineName to set
     */
    public void setMagazineName( String magazineName ) {
        this.magazineName = magazineName;
    }

    /**
     * Gets the magazine path.
     *
     * @return the magazinePath
     */
    public String getMagazinePath() {
        return magazinePath;
    }

    /**
     * Sets the magazine path.
     *
     * @param magazinePath
     *            the magazinePath to set
     */
    public void setMagazinePath( String magazinePath ) {
        this.magazinePath = magazinePath;
    }

    /**
     * Gets the magazine description.
     *
     * @return the magazineDescription
     */
    public String getMagazineDescription() {
        return magazineDescription;
    }

    /**
     * Sets the magazine description.
     *
     * @param magazineDescription
     *            the magazineDescription to set
     */
    public void setMagazineDescription( String magazineDescription ) {
        this.magazineDescription = magazineDescription;
    }
}
