package com.scholastic.classmags.migration.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.commons.jcr.JcrConstants;
import com.scholastic.classmags.migration.models.Issue;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.RecentIssuesService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * The Class RecentIssuesServiceImpl.
 */
@Component( immediate = true, metatype = false )
@Service( RecentIssuesService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Recent issues service impl class" ) })
public class RecentIssuesServiceImpl implements RecentIssuesService {

    /** The resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /** The prop config service. */
    @Reference
    PropertyConfigService propConfigService;

    /** The resource path config service. */
    @Reference
    private ResourcePathConfigService resourcePathConfigService;

    /** The magazine props service. */
    @Reference
    private MagazineProps magazineProps;

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( RecentIssuesServiceImpl.class );

    /** The Constant JCR_SORTING_DATE_FORMAT. */
    private static final String JCR_SORTING_DATE_FORMAT = "jcr:sortingDateFormat";

    @Override
    public List< Issue > getRecentIssues( String magazineName, List< ValueMap > issuePaths ) {
        ResourceResolver resourceResolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );
        List< Issue > recentIssues = new ArrayList< >();
        if ( null != resourceResolver ) {
            for ( ValueMap issuePath : issuePaths ) {
                String path = issuePath.get( "issuePath", String.class );
                Resource resource = resourceResolver.getResource( path + "/" + JcrConstants.JCR_CONTENT );
                ValueMap properties = resource != null ? resource.getValueMap() : null;
                Issue issue = createIssue( resourceResolver, properties, magazineName, path );
                recentIssues.add( issue );
            }
        }
        return recentIssues;
    }

    private Issue createIssue( ResourceResolver resourceResolver, ValueMap properties, String magazineName,
            String path ) {
        Issue issue = new Issue();
        try {
            if ( null != properties ) {
                issue.setLinkto( InternalURLFormatter.formatURL( resourceResolver, path ) );
                issue.setImgsrc( properties.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) );
                String displayDate = properties.get( ClassMagsMigrationConstants.DISPLAY_DATE, StringUtils.EMPTY );
                issue.setIssuedate( displayDate );
                if ( displayDate.isEmpty() ) {
                    issue.setIssuedate( CommonUtils.getSortingDate( properties,
                            propConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT,
                                    CommonUtils.getDataPath( resourcePathConfigService, magazineName ) ) ) );
                }
            }
        } catch ( LoginException e ) {
            LOG.debug( "Error while fetching property in recent issues", e );
        }

        return issue;
    }
}
