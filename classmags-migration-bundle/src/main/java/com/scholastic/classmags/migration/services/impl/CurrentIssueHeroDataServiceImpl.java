package com.scholastic.classmags.migration.services.impl;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.osgi.framework.Constants;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.ClassmagsMigrationErrorCodes;
import com.scholastic.classmags.migration.services.CurrentIssueHeroDataService;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class CurrentIssueHeroDataServiceImpl.
 */
@Component( immediate = true, metatype = false )
@Service( CurrentIssueHeroDataService.class )
@Properties( { @Property( name = Constants.SERVICE_DESCRIPTION, value = "Current Issue Hero Data Service" ) })
public class CurrentIssueHeroDataServiceImpl implements CurrentIssueHeroDataService {

    /** The ci resource resolver factory. */
    @Reference
    private ResourceResolverFactory resourceResolverFactory;

    /** The ci property config service. */
    @Reference
    private PropertyConfigService propertyConfigService;

    /** The ci resource path config service. */
    @Reference
    private ResourcePathConfigService resourcePathConfigService;
    
    /** The magazine props service. */
    @Reference
    private MagazineProps magazineProps;

    @Override
    public ValueMap fetchIssueData( String currentPath ) {
        return CommonUtils.fetchPagePropertyMap( currentPath, resourceResolverFactory );
    }

    @Override
    public String fetchIssueDate( String issuePath, String currentPagePath ) throws ClassmagsMigrationBaseException {

        try {
            String magazineName = magazineProps.getMagazineName( currentPagePath );
            return CommonUtils.getDisplayDate( CommonUtils.fetchPagePropertyMap( issuePath, resourceResolverFactory ),
                    propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                            CommonUtils.getDataPath( resourcePathConfigService, magazineName ) ) );
        } catch ( LoginException e ) {
            throw new ClassmagsMigrationBaseException( ClassmagsMigrationErrorCodes.LOGIN_ERROR, e );
        }
    }
}
