package com.scholastic.classmags.migration.models;

public class GameModel {

    private String gamePath;
    private String containerId;
    private String title;
    private String description;
    private String thumbnailImg;
    private String gameType;
    private String xmlPath;
    private String containerPath;
    private String flashGameType;
    private String userType;
    
    public String getFlashGameType() {
        return flashGameType;
    }

    public void setFlashGameType( String flashGameType ) {
        this.flashGameType = flashGameType;
    }
    
    public String getGamePath() {
        return gamePath;
    }

    public void setGamePath( String gamePath ) {
        this.gamePath = gamePath;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId( String containerId ) {
        this.containerId = containerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle( String title ) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription( String description ) {
        this.description = description;
    }

    public String getThumbnailImg() {
        return thumbnailImg;
    }

    public void setThumbnailImg( String thumbnailImg ) {
        this.thumbnailImg = thumbnailImg;
    }
    
    public String getUserType() {
        return userType;
    }

    public void setUserType( String userType ) {
        this.userType = userType;
    }
    
    public String getGameType() {
        return gameType;
    }

    public void setGameType( String gameType ) {
        this.gameType = gameType;
    }
    
    public String getXMLPath() {
        return xmlPath;
    }

    public void setXMLPath( String xmlPath ) {
        this.xmlPath = xmlPath;
    }
    
    public String getContainerPath() {
        return containerPath;
    }

    public void setContainerPath( String containerPath ) {
        this.containerPath = containerPath;
    }
}
