package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.List;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.models.ContentTileObject;
import com.scholastic.classmags.migration.services.VideoGameDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VideoGameUse extends WCMUsePojo {


	/** The Constant ISSUE_KEY_ASSETS_LABEL. */
	private static final String CONTENT_TYPE_LABEL = "contentType";

	/** The Constant ISSUE_KEY_ASSETS_PATH. */
	private static final String CONTENT_TILE_LINK = "contentLink";

	/** The Constant  Label. */
	private static final String CONTENT_TITLE = "contentTitle";

	/** The Constant  Description. */
	private static final String CONTENT_DECRIPTION = "contentDescription";


	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger( MarketingSampleIssueUse.class );

	/** The asset data list. */
	private List<ContentTileObject> tileObject;


	/**
	 * Gets the asset data list.
	 *
	 * @return the assetDataList
	 */


	/** The Sample Issue service. */
	VideoGameDataService videoGameDataService;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.adobe.cq.sightly.WCMUsePojo#activate()
	 */
	@Override
	public void activate() {

		List< ValueMap > assetDataMapList;
		SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
		Page currentPage = getCurrentPage();
		if ( null != slingScriptHelper ) {
			String currentPath = currentPage.getPath();
			try {
				videoGameDataService = slingScriptHelper
						.getService( VideoGameDataService.class );
				if ( null != videoGameDataService ) {
					assetDataMapList = videoGameDataService.fetchAssetData( currentPath );
					setTileObject(populateContentTileObject(assetDataMapList));
				}	
			} catch ( Exception e ) {
				LOG.error( e.getMessage(), e );
			}
		}
	}

	/**
	 * Adds the items to asset data list.
	 *
	 * @param assetDataMapList
	 *            the asset data map list
	 */
	private List<ContentTileObject> populateContentTileObject( List< ValueMap > assetDataMapList ) {
		ContentTileObject contentTileObject;
		List<ContentTileObject> tileObjectList = new ArrayList<>();
		for(ValueMap assetDataMap : assetDataMapList){
			contentTileObject =  new ContentTileObject();
			String videoPath = assetDataMap.get(CONTENT_TILE_LINK, StringUtils.EMPTY);
			String contentType = assetDataMap.get(CONTENT_TYPE_LABEL, StringUtils.EMPTY);
			String contentTitle = assetDataMap.get(CONTENT_TITLE, StringUtils.EMPTY);
			String contentDescription = assetDataMap.get(CONTENT_DECRIPTION, StringUtils.EMPTY);

			if(StringUtils.equalsIgnoreCase(assetDataMap.get(CONTENT_TYPE_LABEL).toString(), ClassMagsMigrationConstants.VIDEO)){
				contentTileObject = videoGameDataService.getVideoContent(videoPath);
			}
			else if(StringUtils.equalsIgnoreCase(assetDataMap.get(CONTENT_TYPE_LABEL).toString(), ClassMagsMigrationConstants.GAME)){
				contentTileObject =  videoGameDataService.getGameContent( videoPath) ;
			}
			contentTileObject.setContentType(contentType);
			contentTileObject.setAssetLabel(contentTitle);
			contentTileObject.setContentDescription(contentDescription);
			tileObjectList.add(contentTileObject);

		}
		return tileObjectList;
	}

	public List<ContentTileObject> getTileObject() {
		return tileObject;
	}

	public void setTileObject(List<ContentTileObject> tileObject) {
		this.tileObject = tileObject;
	}


}
