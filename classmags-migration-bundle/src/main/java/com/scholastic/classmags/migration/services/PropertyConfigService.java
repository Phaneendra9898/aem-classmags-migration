package com.scholastic.classmags.migration.services;

import org.apache.sling.api.resource.LoginException;

/**
 * The Interface PropertyConfigService.
 */
public interface PropertyConfigService {

    /**
     * Gets the data config property.
     *
     * @param inputkey
     *            the inputkey
     * @param dataPath
     *            the data path
     * @return the data config property
     * @throws LoginException
     *             the login exception
     */
    public String getDataConfigProperty( String inputkey, String dataPath ) throws LoginException;

}
