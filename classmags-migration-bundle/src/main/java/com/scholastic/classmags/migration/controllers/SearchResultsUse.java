package com.scholastic.classmags.migration.controllers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.settings.SlingSettingsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.scholastic.classmags.migration.models.SearchFilter;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class SearchResultsUse.
 */
public class SearchResultsUse extends WCMUsePojo {
    
    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( SearchResultsUse.class );

    private List<SearchFilter> subjectFilters = new ArrayList<SearchFilter>();
    
    private List<SearchFilter> contentFilters = new ArrayList<SearchFilter>();
    
    private String currentPagePath;

    private boolean isAuthor;
    
    /* (non-Javadoc)
     * @see com.adobe.cq.sightly.WCMUsePojo#activate()
     */
    @Override
    public void activate() throws Exception {
        
        Set<String> runModes = getSlingScriptHelper().getService( SlingSettingsService.class ).getRunModes();
        
        if(runModes.contains( "author" )){
            isAuthor=true;
        }else{
            isAuthor=false;
        }

        currentPagePath = getCurrentPage().getPath();
        MagazineProps magazineProps= getSlingScriptHelper().getService( MagazineProps.class );
        String name  = magazineProps.getMagazineName( getCurrentPage().getPath() );
        ResourceResolver serviceResolver = getServiceResourceResolver( getSlingScriptHelper().getService( ResourceResolverFactory.class ) );
        //Subject filters
        subjectFilters = CommonUtils.getSearchFilters( serviceResolver, ClassMagsMigrationConstants.CLASS_MAGS_TAG_ROOT_PATH+name );
        LOG.info( "subjectFilters : {}",subjectFilters );
        LOG.info( "ClassMagsMigrationConstants.CLASS_MAGS_TAG_ROOT_PATH+name : {}",ClassMagsMigrationConstants.CLASS_MAGS_TAG_ROOT_PATH+name );
        Resource component = getResource().getChild( ClassMagsMigrationConstants.NODE_NAME_CONTENT_TYPES );
        contentFilters = CommonUtils.getContentTypeFilters( component, getRequest(), isAuthor );
        // Content type filters
        if(serviceResolver!=null && serviceResolver.isLive()){
            serviceResolver.close();
        }
    }

    public List<SearchFilter> getSubjectFilters() {
        return subjectFilters;
    }

    public List<SearchFilter> getContentFilters() {
        return contentFilters;
    }
    
    /**
     * This method will get service resolver for the accessing the tags
     * 
     * @param ResourceResolverFacatory
     * @return service ResourceResolver
     */
    private ResourceResolver getServiceResourceResolver(
            ResourceResolverFactory resourceResolverFactory) {
        ResourceResolver serviceResolver = null;
        final Map<String, Object> authInfo = Collections.singletonMap(
                ResourceResolverFactory.SUBSERVICE,
                (Object) ClassMagsMigrationConstants.READ_SERVICE);
        try {
            serviceResolver = resourceResolverFactory
                    .getServiceResourceResolver(authInfo);
        } catch (LoginException e) {
            LOG.error("Error getting service resource resolver : {}", e);
        }
        return serviceResolver;
    }

    public String getCurrentPagePath() {
        return currentPagePath;
    }

}
