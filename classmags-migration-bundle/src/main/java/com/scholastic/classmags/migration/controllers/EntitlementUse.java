package com.scholastic.classmags.migration.controllers;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.models.UIdOrgContextSubHeaderEntitlements;
import com.scholastic.classmags.migration.models.UserIdCookieData;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.CookieUtil;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * Use Class for Entitlement.
 */
public class EntitlementUse extends WCMUsePojo {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger( EntitlementUse.class );

    private String errorPage;

    private boolean redirect;

    @Override
    public void activate() {
        SlingScriptHelper slingScriptHelper = getSlingScriptHelper();
        ResourceResolver resourceResolver = getResourceResolver();
        String role = RoleUtil.getUserRole( getRequest() );
        String currentPage = getCurrentPage().getPath();
        errorPage = StringUtils.EMPTY;

        if ( !role.equalsIgnoreCase( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS ) ) {
            if ( slingScriptHelper != null ) {

                MagazineProps magazineProps = slingScriptHelper.getService( MagazineProps.class );
                PropertyConfigService propertyConfigService = slingScriptHelper
                        .getService( PropertyConfigService.class );
                ResourcePathConfigService resourcePathConfigService = slingScriptHelper
                        .getService( ResourcePathConfigService.class );

                if ( magazineProps != null ) {
                    try {
                        String magazineName = magazineProps.getMagazineName( getResource().getPath() );

                        errorPage = InternalURLFormatter.formatURL( resourceResolver,
                                propertyConfigService.getDataConfigProperty( "entitlementsErrorPagePath",
                                        CommonUtils.getDataPath( resourcePathConfigService, magazineName ) ) );

                        LOG.debug( "::::: In Magazine Name :::::" + magazineName );
                        LOG.debug( "::::: In Error Page Name: {}" + errorPage );

                        UserIdCookieData userIdCookieData = CookieUtil.fetchUserIdCookieData( getRequest() );

                        if ( userIdCookieData != null ) {
                            LOG.debug( "Cookie is :: {}", userIdCookieData );
                            redirect = true;
                            for ( UIdOrgContextSubHeaderEntitlements object : userIdCookieData
                                    .getObjUIdHeaderOrgContext().getObjListUIdOrgContextSubHeaderEntitlements() ) {
                                LOG.debug( "Entitlement : {}", object );
                                if ( ( null != object && object.getApplicationCode().equalsIgnoreCase( magazineName ) )
                                        || CommonUtils.fetchPagePropertyValue( "userType", currentPage,
                                                slingScriptHelper
                                                        .getService( ResourceResolverFactory.class ) ) == "everyone" ) {
                                    LOG.debug( "Found entitltement, do not redirect." );
                                    redirect = false;
                                }
                            }
                        } else {
                            LOG.debug( "Cookie data is null" );
                            LOG.debug( "Redirecting to  : {}", errorPage );
                            redirect = true;
                        }
                    } catch ( Exception e ) {
                        LOG.error( "Error sending redirect " + e );
                    }
                }
            }
        }

    }

    public String getErrorPage() {
        return errorPage;
    }

    public void setErrorPage( String errorPage ) {
        this.errorPage = errorPage;
    }

    public boolean getRedirect() {
        return redirect;
    }

    public void setRedirect( boolean redirect ) {
        this.redirect = redirect;
    }
}
