package com.scholastic.classmags.migration.services.impl;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

/**
 * The Class CreateBrightcoveVideoServiceImplTest.
 */
public class CreateBrightcoveVideoServiceImplTest {

    /** The config service impl. */
    @InjectMocks
    private CreateBrightcoveVideoServiceImpl configServiceImpl;

    /** The resource resolver factory. */
    @Mock
    private ResourceResolverFactory resourceResolverFactory;

    /** The resource resolver. */
    @Mock
    private ResourceResolver resourceResolver;

    /** The brightcove video ID. */
    private String brightcoveVideoID;

    /** The dam path. */
    private String damPath;

    /** The param. */
    private Map< String, Object > param = new HashMap< String, Object >();

    /**
     * Sets the up.
     * 
     * @throws LoginException
     *             the login exception
     */
    @Before
    public void setUp() throws LoginException {
        MockitoAnnotations.initMocks( this );
        brightcoveVideoID = "12345";
        damPath = "/content";
        param.put( ResourceResolverFactory.SUBSERVICE, "writeservice" );

        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
    }

    /**
     * Test create videoin AEM.
     */
    @Test
    public void testCreateVideoinAEM() {
        // configServiceImpl.createVideoinAEM( brightcoveVideoID, damPath );
    }
}
