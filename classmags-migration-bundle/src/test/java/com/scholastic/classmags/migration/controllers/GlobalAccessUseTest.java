package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;
import static org.mockito.Mockito.mock;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * JUnit for GlobalAccessUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { GlobalAccessUse.class, RoleUtil.class } )
public class GlobalAccessUseTest extends BaseComponentTest {

    private static final String STUDENT_USER_TYPE = "student";
    private static final String TEACHER_USER_TYPE = "teacher";
    private static final String CURRENT_PAGE_PATH = "/content/classroom_magazines/scienceworld/issues/2016-17/090516";
    private static final String MAGAZINE_NAME = "scienceworld";
    
    private GlobalAccessUse globalAccessUse;
    private MagazineProps magazineProps;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        globalAccessUse = Whitebox.newInstance( GlobalAccessUse.class );

        stubCommon( globalAccessUse );

        PowerMockito.mockStatic( RoleUtil.class );
        magazineProps = mock(MagazineProps.class);
        
        when( RoleUtil.getUserRole( request ) ).thenReturn( STUDENT_USER_TYPE );
        when( properties.get( "userType", "everyone" ) ).thenReturn( STUDENT_USER_TYPE );
        when( RoleUtil.shouldRender( STUDENT_USER_TYPE, STUDENT_USER_TYPE ) ).thenReturn( true );
        when( RoleUtil.shouldRender( STUDENT_USER_TYPE, TEACHER_USER_TYPE ) ).thenReturn( false );
        when( slingScriptHelper.getService( MagazineProps.class )).thenReturn( magazineProps );
        when( currentPage.getPath()).thenReturn( CURRENT_PAGE_PATH );
        when( magazineProps.getMagazineName( CURRENT_PAGE_PATH )).thenReturn( MAGAZINE_NAME );
    }

    @Test
    public void testGlobalAccessUse() throws Exception {

        // execute logic
        Whitebox.invokeMethod( globalAccessUse, "activate" );

        // verify logic
        assertTrue( globalAccessUse.isVisible() );
        assertFalse( globalAccessUse.isShareableToTeacher() );
        assertEquals( MAGAZINE_NAME, globalAccessUse.getMagazineName());
        assertEquals( STUDENT_USER_TYPE, globalAccessUse.getLoginRole());
    }
    
    @Test
    public void testGlobalAccessUseWhenSlingScriptHelperIsNull() throws Exception {
        // setup logic ( test-specific )
        stub( PowerMockito.method( globalAccessUse.getClass(), "getSlingScriptHelper" ) )
        .toReturn( null );
        
        // execute logic
        Whitebox.invokeMethod( globalAccessUse, "activate" );

        // verify logic
        assertTrue( globalAccessUse.isVisible() );
        assertFalse( globalAccessUse.isShareableToTeacher() );
        assertTrue( globalAccessUse.getMagazineName().isEmpty());
    }
    
    @Test
    public void testGlobalAccessUseWhenCurrentPageIsNull() throws Exception {
        // setup logic ( test-specific )
        stub( PowerMockito.method( globalAccessUse.getClass(), "getCurrentPage" ) ).toReturn( null );
        
        // execute logic
        Whitebox.invokeMethod( globalAccessUse, "activate" );

        // verify logic
        assertTrue( globalAccessUse.isVisible() );
        assertFalse( globalAccessUse.isShareableToTeacher() );
        assertTrue( globalAccessUse.getMagazineName().isEmpty());
    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final GlobalAccessUse globalAccessUse = new GlobalAccessUse();

        // verify logic
        assertNotNull( globalAccessUse );

    }
}
