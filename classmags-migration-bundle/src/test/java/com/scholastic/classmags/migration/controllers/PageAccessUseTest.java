package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import java.io.Writer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * JUnit for PageAccessUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { PageAccessUse.class, RoleUtil.class } )
public class PageAccessUseTest extends BaseComponentTest {
    private PageAccessUse pageAccessUse;
    private PrintWriter writer;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        pageAccessUse = Whitebox.newInstance( PageAccessUse.class );
        stubCommon( pageAccessUse );

        writer = mock( PrintWriter.class );
        PowerMockito.mockStatic( RoleUtil.class );

        when( request.getAttribute( "role" ) ).thenReturn( "teacher" );
        when( currentPage.getProperties() ).thenReturn( properties );
        when( properties.get( "userType", "everyone" ) ).thenReturn( "teacher" );
        when( RoleUtil.shouldRender( any( String.class ), any( String.class ) ) ).thenReturn( true );
    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final PageAccessUse pageAccessUse = new PageAccessUse();

        // verify logic
        assertNotNull( pageAccessUse );

    }

    @Test
    public void testWhenPagesAccessUseShouldRender() throws Exception {
        // setup logic ( test- specific )

        // execute logic
        Whitebox.invokeMethod( pageAccessUse, "activate" );

        // verify logic
        verify( response, times( 0 ) ).sendError( 403 );
    }

    @Test
    public void testWhenPagesAccessUseShouldNotRender() throws Exception {
        // setup logic ( test- specific )
        when( RoleUtil.shouldRender( any( String.class ), any( String.class ) ) ).thenReturn( false );
        when( response.getWriter() ).thenReturn( writer );

        // execute logic
        Whitebox.invokeMethod( pageAccessUse, "activate" );

        // verify logic
        verify( response, atLeastOnce() ).sendError( 403 );
    }

}
