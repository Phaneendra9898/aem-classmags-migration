package com.scholastic.classmags.migration.social.impl.operations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.adobe.cq.social.scf.SocialComponent;
import com.adobe.cq.social.scf.SocialComponentFactoryManager;
import com.scholastic.classmags.migration.social.api.BookmarkService;
import com.scholastic.classmags.migration.social.api.BookmarkSocialComponent;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;

/**
 * The Class GetAllSiteBookmarksOperationTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { GetAllSiteBookmarksOperation.class, SocialASRPUtils.class } )
public class GetAllSiteBookmarksOperationTest {

    /** The Constant TEST_PATH. */
    private static final String TEST_PATH = "/testPath";

    /** The get all site bookmarks operation. */
    private GetAllSiteBookmarksOperation getAllSiteBookmarksOperation;

    /** The mock sling http servlet request. */
    @Mock
    private SlingHttpServletRequest mockSlingHttpServletRequest;

    /** The mock bookmark service. */
    @Mock
    private BookmarkService mockBookmarkService;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock scf manager. */
    @Mock
    private SocialComponentFactoryManager mockScfManager;

    /** The mock social component. */
    @Mock
    private SocialComponent mockSocialComponent;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        getAllSiteBookmarksOperation = new GetAllSiteBookmarksOperation();

        Whitebox.setInternalState( getAllSiteBookmarksOperation, mockBookmarkService );
        Whitebox.setInternalState( getAllSiteBookmarksOperation, mockScfManager );

        PowerMockito.mockStatic( SocialASRPUtils.class );
        when( SocialASRPUtils.getSocialComponent( mockSlingHttpServletRequest, mockResource, mockScfManager,
                BookmarkSocialComponent.BOOKMARK_RESOURCE_TYPE ) ).thenReturn( mockSocialComponent );

    }

    /**
     * Test perform operation.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPerformOperation() throws Exception {

        when( mockBookmarkService.getAllSiteBookmarkResource( mockSlingHttpServletRequest ) )
                .thenReturn( mockResource );
        when( mockResource.getPath() ).thenReturn( TEST_PATH );

        assertEquals( TEST_PATH,
                getAllSiteBookmarksOperation.performOperation( mockSlingHttpServletRequest ).getPath() );
    }

    /**
     * Test perform operation with no resource.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPerformOperationWithNoResource() throws Exception {

        when( mockBookmarkService.getAllSiteBookmarkResource( mockSlingHttpServletRequest ) ).thenReturn( null );
        when( mockSlingHttpServletRequest.getResource() ).thenReturn( mockResource );
        when( mockResource.getPath() ).thenReturn( TEST_PATH );

        assertEquals( TEST_PATH,
                getAllSiteBookmarksOperation.performOperation( mockSlingHttpServletRequest ).getPath() );
    }
}
