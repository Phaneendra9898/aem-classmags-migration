package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.day.cq.wcm.api.PageManager;
import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.services.TeachingResourcesService;

/**
 * JUnit for ArticleResourcesUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { ArticleResourcesUse.class } )
public class ArticleResourcesUseTest extends BaseComponentTest {

    private static final String VIDEO_RESOURCE = "video";
    private static final String SKILLSHEET_RESOURCE = "skillsheet";
    private static final String BLOG_RESOURCE = "blog";

    // Video resource
    private static final String VIDEO_TITLE = "America's 11 Million";
    private static final String VIDEO_DESCRIPTION = "Scholastic Polls";
    private static final String VIDEO_IMAGE_PATH = "/content/dam/scholastic/videos/UPF-040416-Americas11Million.mp4/jcr:content/renditions/cq5dam.thumbnail.140.100.png";
    private static final long VIDEO_ID = new Long( 123456789 );
    private static final List< String > VIDEO_TAGS = new ArrayList< >( Arrays.asList( "elections", "polls" ) );
    private static final String VIDEO_PAGE_PATH = "/content/dam/scholastic/videos/UPF-040416-Americas11Million.mp4";

    // Skillsheet resource
    private static final String SKILLSHEET_TITLE = "Pups Patrol";
    private static final String SKILLSHEET_DESCRIPTION = "";
    private static final String SKILLSHEET_IMAGE_PATH = "/content/dam/scholastic/skillsheets/SW-090516-LP-PupsPatrol.pdf/jcr:content/renditions/cq5dam.thumbnail.140.100.png";
    private static final Long SKILLSHEET_VIDEO_ID = null;
    private static final List< String > SKILLSHEET_TAGS = new ArrayList< >( Arrays.asList( "Pups", "Patrol" ) );
    private static final String SKILLSHEET_PAGE_PATH = "/content/dam/scholastic/teachers/image/skillsheets/SW-090516-LP-PupsPatrol.pdf";

    // Blog resource
    private static final String BLOG_TITLE = "Test Blog 1";
    private static final String BLOG_DESCRIPTION = "Test Blog Description";
    private static final String BLOG_IMAGE_PATH = "/content/dam/scholastic/classroom-magazines/teacher/teacher1.jpg";
    private static final Long BLOG_VIDEO_ID = null;
    private static final List< String > BLOG_TAGS = null;
    private static final String BLOG_PAGE_PATH = "/content/classroom-magazines/test-page";

    private Map< String, List< ResourcesObject > > teachingResources = new HashMap< >();
    private Map< String, List< ResourcesObject > > issueResources = new HashMap< String, List< ResourcesObject > >();
    private Map< String, List< ResourcesObject > > customResources = new HashMap< String, List< ResourcesObject > >();

    private List< ResourcesObject > videoResourcesList, videoList, customVideoList;
    private List< ResourcesObject > skillsheetList;
    private List< ResourcesObject > blogList;
    private ResourcesObject video, customVideo;
    private ArticleResourcesUse articleResourcesUse;
    private TeachingResourcesService teachingResourcesService;
    private Resource articleConfigResource;

    /**
     * Setup.
     * 
     */
    @Before
    public void setup(){
        articleResourcesUse = Whitebox.newInstance( ArticleResourcesUse.class );

        stubCommon( articleResourcesUse );
        teachingResourcesService = mock( TeachingResourcesService.class );
        articleConfigResource = mock( Resource.class );
        videoResourcesList = new ArrayList< >();
        skillsheetList = new ArrayList< >();
        blogList = new ArrayList< >();
        videoList = new ArrayList< >();
        customVideoList = new ArrayList< >();
        video = createResourcesObject( VIDEO_TITLE, VIDEO_DESCRIPTION, VIDEO_IMAGE_PATH, VIDEO_ID, VIDEO_TAGS,
                VIDEO_PAGE_PATH );
        customVideo = createResourcesObject( VIDEO_TITLE, VIDEO_DESCRIPTION, VIDEO_IMAGE_PATH, VIDEO_ID, null,
                VIDEO_PAGE_PATH );
        skillsheetList.add( createResourcesObject( SKILLSHEET_TITLE, SKILLSHEET_DESCRIPTION, SKILLSHEET_IMAGE_PATH,
                SKILLSHEET_VIDEO_ID, SKILLSHEET_TAGS, SKILLSHEET_PAGE_PATH ) );
        blogList.add( createResourcesObject( BLOG_TITLE, BLOG_DESCRIPTION, BLOG_IMAGE_PATH, BLOG_VIDEO_ID, BLOG_TAGS,
                BLOG_PAGE_PATH ) );

        videoList.add( video );
        customVideoList.add( customVideo );
        videoResourcesList.add( video );
        videoResourcesList.add( customVideo );
        issueResources.put( VIDEO_RESOURCE, videoList );
        issueResources.put( SKILLSHEET_RESOURCE, skillsheetList );
        customResources.put( BLOG_RESOURCE, blogList );
        customResources.put( VIDEO_RESOURCE, customVideoList );
        teachingResources.put( VIDEO_RESOURCE, videoResourcesList );
        teachingResources.put( SKILLSHEET_RESOURCE, skillsheetList );
        teachingResources.put( BLOG_RESOURCE, blogList );

        when( currentPage.getContentResource() ).thenReturn( resource );
        when( resource.getChild( "article-configuration" ) ).thenReturn( articleConfigResource );
        when( request.getResource() ).thenReturn( resource );
        when( resource.getResourceResolver() ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( PageManager.class ) ).thenReturn( pageManager );
        when( pageManager.getContainingPage( resource ) ).thenReturn( currentPage );
        when( slingScriptHelper.getService( TeachingResourcesService.class ) ).thenReturn( teachingResourcesService );
        when( teachingResourcesService.getTeachingResources( request, articleConfigResource, "resources" ) )
                .thenReturn( issueResources );
        when( teachingResourcesService.getTeachingResources( request, articleConfigResource, "custom-resources" ) )
                .thenReturn( customResources );
        when( teachingResourcesService.createResourcesMap( issueResources, customResources ) )
                .thenReturn( teachingResources );

    }

    @Test
    public void testArticleResources() throws Exception {

        // execute logic
        Whitebox.invokeMethod( articleResourcesUse, "activate" );

        // verify logic
        assertEquals( 2, articleResourcesUse.getTeachingResources().get( VIDEO_RESOURCE ).size() );
        assertEquals( videoResourcesList, articleResourcesUse.getTeachingResources().get( VIDEO_RESOURCE ) );
        assertEquals( skillsheetList, articleResourcesUse.getTeachingResources().get( SKILLSHEET_RESOURCE ) );
        assertEquals( blogList, articleResourcesUse.getTeachingResources().get( BLOG_RESOURCE ) );

    }

    @Test
    public void testArticleResourcesWhenCurrentPageResourceIsNull() throws Exception {
        // setup logic ( test-specific )
        when( currentPage.getContentResource() ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( articleResourcesUse, "activate" );

        // verify logic
        assertNull( articleResourcesUse.getTeachingResources() );

    }

    @Test
    public void testArticleResourcesWhenArticleConfigResourceIsNull() throws Exception {
        // setup logic ( test-specific )
        when( resource.getChild( "article-configuration" ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( articleResourcesUse, "activate" );

        // verify logic
        assertNull( articleResourcesUse.getTeachingResources() );

    }

    @Test
    public void testArticleResourcesWhenTeachingResourcesServiceIsNull() throws Exception {
        // setup logic ( test-specific )
        when( slingScriptHelper.getService( TeachingResourcesService.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( articleResourcesUse, "activate" );

        // verify logic
        assertNull( articleResourcesUse.getTeachingResources() );

    }

    @Test
    public void testArticleResourcesWhenCustomResourcesIsNull() throws Exception {
        // setup logic ( test-specific )
        when( teachingResourcesService.getTeachingResources( request, articleConfigResource, "custom-resources" ) )
                .thenReturn( null );
        when( teachingResourcesService.createResourcesMap( issueResources, null ) ).thenReturn( issueResources );

        // execute logic
        Whitebox.invokeMethod( articleResourcesUse, "activate" );

        // verify logic
        assertEquals( videoList, articleResourcesUse.getTeachingResources().get( VIDEO_RESOURCE ) );

    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final ArticleResourcesUse articleResourcesUse = new ArticleResourcesUse();

        // verify logic
        assertNotNull( articleResourcesUse );

    }

    /**
     * Creates the resource object.
     * 
     * @param title The resource title
     * @param description The resource description.
     * @param imagePath The image path.
     * @param videoId The video id.
     * @param tags The video tags
     * @param pagePath The page path.
     * @return resourcesObject The resource object.
     */
    private ResourcesObject createResourcesObject( String title, String description, String imagePath, Long videoId,
            List< String > tags, String pagePath ) {
        ResourcesObject resourcesObject = new ResourcesObject();
        resourcesObject.setTitle( title );
        resourcesObject.setDescription( description );
        resourcesObject.setImagePath( imagePath );

        if ( null != videoId ) {
            resourcesObject.setVideoId( videoId );
        }
        resourcesObject.setTags( tags );
        resourcesObject.setPagePath( pagePath );
        return resourcesObject;

    }

}
