package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.models.FooterObject;
import com.scholastic.classmags.migration.models.LinkObject;
import com.scholastic.classmags.migration.models.SocialLinks;
import com.scholastic.classmags.migration.services.FooterService;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * JUnit for FooterUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { GlobalFooterUse.class, RoleUtil.class } )
public class GlobalFooterUseTest extends BaseComponentTest {

    private static final String LINK_PATH_KEY = "linkPath";
    private static final String LINK_TEXT_KEY = "linkText";
    private static final String LINK_USER_TYPE_KEY = "userType";
    private static final String LINK_PATH_VALUE = "/content/scholastic-article.html";
    private static final String LINK_TEXT_VALUE = "scholastic article";
    private static final String LINK_USER_TYPE_VALUE = "student";
    private static final String HEADING_TITLE = "Customer Support";
    private static final String PHONE_NUMBER = "+91 1234567890";
    private static final String SOCIAL_LINK_ICON_KEY = "linkIcon";
    private static final String SOCIAL_LINK_PATH_VALUE = "www.facebook.com";
    private static final String SOCIAL_LINK_ICON_VALUE = "facebook icon";
    private static final String SECTION_HEADING = "Follow Us";
    private static final String USER_TYPE_VALUE = "teacher";
    private static final String CURRENT_PATH = "/content/classroom_magazines/scienceworld/issues/111616/going-batty";
    private static final String MAGAZINE_NAME = "scienceworld";
    
    private GlobalFooterUse footerUse;
    private FooterService footerService;
    private MagazineProps magazineProps;
    private List< FooterObject > footerObjects;
    private FooterObject columnOne;
    private SocialLinks socialLinks;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        footerUse = Whitebox.newInstance( GlobalFooterUse.class );

        stubCommon( footerUse );

        footerService = mock( FooterService.class );
        magazineProps = mock( MagazineProps.class );

        footerObjects = new ArrayList< >();

        columnOne = createFooterObject();
        socialLinks = createSocialLinks();

        footerObjects.add( columnOne );
        PowerMockito.mockStatic( RoleUtil.class );
        when( request.getResource() ).thenReturn( resource );
        when ( resource.getPath() ).thenReturn( CURRENT_PATH );
        when( RoleUtil.getUserRole( request ) ).thenReturn( USER_TYPE_VALUE );
        when( slingScriptHelper.getService( FooterService.class ) ).thenReturn( footerService );
        when( slingScriptHelper.getService( MagazineProps.class) ).thenReturn ( magazineProps );
        when( magazineProps.getMagazineName( CURRENT_PATH )).thenReturn(MAGAZINE_NAME);
    }

    @Test
    public void testFooterUse() throws Exception {
        // setup logic(test-specific)
        when( footerService.getFooterObjects( resource, MAGAZINE_NAME ) ).thenReturn( footerObjects );

        // execute logic
        Whitebox.invokeMethod( footerUse, "activate" );

        // verify logic
        assertEquals( 1, footerUse.getFooterObjects().size() );
        assertEquals( HEADING_TITLE, footerUse.getFooterObjects().get( 0 ).getHeadingTitle() );
        assertEquals( PHONE_NUMBER, footerUse.getFooterObjects().get( 0 ).getPhoneNumber() );
        assertEquals( 1, footerUse.getFooterObjects().get( 0 ).getFooterLinks().size() );
        assertEquals( LINK_TEXT_VALUE,
                footerUse.getFooterObjects().get( 0 ).getFooterLinks().get( 0 ).getLabel() );
        assertEquals( LINK_PATH_VALUE,
                footerUse.getFooterObjects().get( 0 ).getFooterLinks().get( 0 ).getPath() );
        assertEquals( LINK_USER_TYPE_VALUE, footerUse.getFooterObjects().get( 0 ).getFooterLinks().get( 0 )
                .getUserType() );
        assertEquals( USER_TYPE_VALUE, footerUse.getUserType() );
    }

    @Test
    public void testFooterUseWhenResourceIsNull() throws Exception {
        // setup logic(test-specific)
        when( request.getResource() ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( footerUse, "activate" );

        // verify logic
        assertNull( footerUse.getFooterObjects() );
        assertNull( footerUse.getSocialLinks() );
    }

    @Test
    public void testFooterUseSocialLinks() throws Exception {
        // setup logic(test-specific)
        when( footerService.getSocialLinks( resource, MAGAZINE_NAME ) ).thenReturn( socialLinks );

        // execute logic
        Whitebox.invokeMethod( footerUse, "activate" );

        // verify logic
        assertEquals( SECTION_HEADING, footerUse.getSocialLinks().getSectionHeading() );
        assertEquals( 1, footerUse.getSocialLinks().getLinks().size() );
        assertEquals( SOCIAL_LINK_PATH_VALUE,
                footerUse.getSocialLinks().getLinks().get( 0 ).get( LINK_PATH_KEY, String.class ) );
        assertEquals( SOCIAL_LINK_ICON_VALUE,
                footerUse.getSocialLinks().getLinks().get( 0 ).get( SOCIAL_LINK_ICON_KEY, String.class ) );
    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final GlobalFooterUse footerUse = new GlobalFooterUse();

        // verify logic
        assertNotNull( footerUse );
    }

    private FooterObject createFooterObject() {
        FooterObject footerObject = new FooterObject();
        List< LinkObject > footerLinks = new ArrayList< >();
        LinkObject footerLink = new LinkObject();

        footerLink.setLabel( LINK_TEXT_VALUE );
        footerLink.setPath( LINK_PATH_VALUE );
        footerLink.setUserType( LINK_USER_TYPE_VALUE );
 
        footerLinks.add( footerLink );

        footerObject.setHeadingTitle( HEADING_TITLE );
        footerObject.setPhoneNumber( PHONE_NUMBER );
        footerObject.setFooterLinks( footerLinks );
        return footerObject;
    }

    private SocialLinks createSocialLinks() {
        socialLinks = new SocialLinks();
        ValueMap socialLink = mock( ValueMap.class );
        List< ValueMap > links = new ArrayList< >();

        when( socialLink.get( LINK_PATH_KEY, String.class ) ).thenReturn( SOCIAL_LINK_PATH_VALUE );
        when( socialLink.get( SOCIAL_LINK_ICON_KEY, String.class ) ).thenReturn( SOCIAL_LINK_ICON_VALUE );

        links.add( socialLink );

        socialLinks.setSectionHeading( SECTION_HEADING );
        socialLinks.setLinks( links );
        return socialLinks;
    }

}
