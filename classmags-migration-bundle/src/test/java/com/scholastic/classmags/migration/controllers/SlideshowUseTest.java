package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import java.util.List;

import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.services.SlideshowDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for SlideshowUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { SlideshowUse.class, CommonUtils.class } )
public class SlideshowUseTest extends BaseComponentTest {

    private SlideshowUse slideshowUse;
    private SlideshowDataService slideshowDataService;
    private JSONArray slideshowJsonArray;
    private List< ValueMap > valueMap;

    @SuppressWarnings( "unchecked" )
    @Before
    public void setup() throws JSONException {
        slideshowUse = Whitebox.newInstance( SlideshowUse.class );
        stubCommon( slideshowUse );

        PowerMockito.mockStatic( CommonUtils.class );

        slideshowDataService = mock( SlideshowDataService.class );
        slideshowJsonArray = mock( JSONArray.class );
        valueMap = mock( List.class );

        when( resource.getChild( ClassMagsMigrationConstants.SLIDESHOW_META_DATA_NODE ) ).thenReturn( resource );
        when( slingScriptHelper.getService( SlideshowDataService.class ) ).thenReturn( slideshowDataService );
        when( CommonUtils.fetchMultiFieldData( resource ) ).thenReturn( valueMap );
        when( slideshowDataService.convertListToJSONArray( valueMap ) ).thenReturn( slideshowJsonArray );
    }

    @Test
    public void testSlideshowUse() throws Exception {
        // setup logic ( test-specific )

        // execute logic
        Whitebox.invokeMethod( slideshowUse, "activate" );

        // verify logic
        assertNotNull( slideshowUse.getSlideshowJsonArray() );
        assertNotNull( slideshowUse.getUniqueId() );
    }

    @Test
    public void testWhenSlideshowDataServiceIsNull() throws Exception {
        // setup logic ( test-specific )
        when( slingScriptHelper.getService( SlideshowDataService.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( slideshowUse, "activate" );

        // verify logic
        verify( slideshowDataService, times( 0 ) ).convertListToJSONArray( valueMap );
    }

    @Test
    public void testWhenSlingScripthelperIsNull() throws Exception {
        // setup logic ( test-specific )
        stub( PowerMockito.method( slideshowUse.getClass(), "getSlingScriptHelper" ) ).toReturn( null );

        // execute logic
        Whitebox.invokeMethod( slideshowUse, "activate" );

        // verify logic
        verify( slingScriptHelper, times( 0 ) ).getService( SlideshowDataService.class );
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final SlideshowUse slideshowUse = new SlideshowUse();

        // verify logic
        assertNotNull( slideshowUse );
    }
}
