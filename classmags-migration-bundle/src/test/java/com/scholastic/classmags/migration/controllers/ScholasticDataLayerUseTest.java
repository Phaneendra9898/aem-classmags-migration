package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.day.cq.wcm.api.PageManager;
import com.scholastic.classmags.migration.services.ScholasticDataLayerService;

/**
 * JUnit for ScholasticDataLayerUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( ScholasticDataLayerUse.class )
public class ScholasticDataLayerUseTest extends BaseComponentTest {

    /** The Constant EMPTY_JSON. */
    private static final String EMPTY_JSON = "{}";

    /** The scholastic data layer use. */
    private ScholasticDataLayerUse scholasticDataLayerUse;
    private ScholasticDataLayerService scholasticDataLayerService;
    private ResourceResolver resourceResolver;
    private PageManager pageManager;
    private JSONObject sampleJSON;

    /**
     * Setup.
     * @throws JSONException 
     */
    @Before
    public void setup() throws JSONException {
        scholasticDataLayerUse = Whitebox.newInstance( ScholasticDataLayerUse.class );
        
        stubCommon( scholasticDataLayerUse );
        
        sampleJSON = createJSONObject();
        
        scholasticDataLayerService = mock( ScholasticDataLayerService.class );
        resourceResolver = mock( ResourceResolver.class );
        pageManager = mock( PageManager.class );
        
        when( request.getResource() ).thenReturn( resource );
        when( resource.getResourceResolver()).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( PageManager.class )).thenReturn( pageManager );
        when( pageManager.getContainingPage( resource )).thenReturn( currentPage );
        when( slingScriptHelper.getService( ScholasticDataLayerService.class )).thenReturn( scholasticDataLayerService );
        when( scholasticDataLayerService.createDataLayer( request, currentPage )).thenReturn( sampleJSON );
    }

    @Test
    public void testScholasticDataLayerUse() throws Exception  {
        
        // execute logic
        Whitebox.invokeMethod( scholasticDataLayerUse, "activate" );
        
        // verify logic
        assertEquals( sampleJSON.toString(), scholasticDataLayerUse.getDigitalData());
    }
    
    @Test
    public void testScholasticDataLayerUseWhenPageManagerIsNull() throws Exception  {
        // setup logic ( test-specific )
        when( resourceResolver.adaptTo( PageManager.class )).thenReturn( null );
        
        // execute logic
        Whitebox.invokeMethod( scholasticDataLayerUse, "activate" );
        
        // verify logic
        assertEquals( EMPTY_JSON, scholasticDataLayerUse.getDigitalData());
    }
    
    @Test
    public void testScholasticDataLayerUseWhenPageIsNull() throws Exception  {
        // setup logic ( test-specific )
        when( pageManager.getContainingPage( resource )).thenReturn( null );
        
        // execute logic
        Whitebox.invokeMethod( scholasticDataLayerUse, "activate" );
        
        // verify logic
        assertEquals( EMPTY_JSON, scholasticDataLayerUse.getDigitalData());
    }
    
    @Test
    public void testScholasticDataLayerUseWhenServiceIsNull() throws Exception  {
        // setup logic ( test-specific )
        when( slingScriptHelper.getService( ScholasticDataLayerService.class )).thenReturn( null );
        
        // execute logic
        Whitebox.invokeMethod( scholasticDataLayerUse, "activate" );
        
        // verify logic
        assertEquals( EMPTY_JSON, scholasticDataLayerUse.getDigitalData());
    }
    
    @Test
    public void testCreateInstance(){
     // execute logic
        final ScholasticDataLayerUse scholasticDataLayerUse = new ScholasticDataLayerUse();

        // verify logic
        assertNotNull( scholasticDataLayerUse );
    }
    
    /*
     * Create sample json object.
     */
    private JSONObject createJSONObject() throws JSONException {
        JSONObject sampleJSON = new JSONObject();
        JSONObject domain = new JSONObject();
        
        domain.put( "name", "cm:scienceworld" );
        domain.put( "channel", "cm:scienceworld" );
        domain.put( "experienceType", "Content" );
        sampleJSON.put( "domain", domain );
        return sampleJSON;
    }
}
