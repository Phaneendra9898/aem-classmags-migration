package com.scholastic.classmags.migration.social.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.adobe.cq.social.srp.SocialResourceProvider;
import com.adobe.cq.social.srp.config.SocialResourceConfiguration;
import com.adobe.cq.social.ugc.api.SearchResults;
import com.adobe.cq.social.ugc.api.UgcFilter;
import com.adobe.cq.social.ugc.api.UgcSearch;
import com.adobe.cq.social.ugcbase.SocialUtils;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.RoleUtil;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;

/**
 * The Class VotingServiceImplTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { VotingServiceImpl.class, SocialASRPUtils.class, RoleUtil.class } )
public class VotingServiceImplTest {

    /** The voting service. */
    private VotingServiceImpl votingService;

    /** The Constant TEST_USER. */
    private static final String TEST_USER = "testUser";

    /** The Constant TEST_PATH. */
    private static final String TEST_PATH = "testPath";

    /** The Constant TEST_QUESTION_UUID. */
    private static final String TEST_QUESTION_UUID = "1111100000";

    /** The Constant TEST_DATA_TEACHER. */
    private static final String TEST_DATA_TEACHER = "{\"userIdentifier\":\"249261\",\"userRole\":\"teacher\",\"questionUUID\":\"1111100000\",\"disableVotingDate\":\"1493317800000\",\"votePagePath\":\"testPath\",\"answerOptions\":{\"Yes\":\"1\",\"No\":\"1\"}}";

    /** The Constant TEST_DATA_STUDENT. */
    private static final String TEST_DATA_STUDENT = "{\"userIdentifier\":\"249261\",\"userRole\":\"student\",\"questionUUID\":\"1111100000\",\"disableVotingDate\":\"1493317800000\",\"votePagePath\":\"testPath\",\"answerOptions\":{\"Yes\":\"1\",\"No\":\"1\"}}";

    /** The mock sling http servlet request. */
    @Mock
    private SlingHttpServletRequest mockSlingHttpServletRequest;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock resource resolver factory. */
    @Mock
    private ResourceResolverFactory mockResourceResolverFactory;

    /** The mock resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;

    /** The mock social utils. */
    @Mock
    private SocialUtils mockSocialUtils;

    /** The mock srp. */
    @Mock
    private SocialResourceProvider mockSrp;

    /** The mock social resource configuration. */
    @Mock
    private SocialResourceConfiguration mockSocialResourceConfiguration;

    /** The mock property map. */
    @Mock
    private ModifiableValueMap mockPropertyMap;

    /** The mock props map. */
    @Mock
    private Map< String, Object > mockPropsMap;

    /** The mock ugc search. */
    @Mock
    private UgcSearch mockUgcSearch;

    /** The mock results. */
    @Mock
    private SearchResults< Resource > mockResults;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        votingService = new VotingServiceImpl();

        Whitebox.setInternalState( votingService, mockResourceResolverFactory );
        Whitebox.setInternalState( votingService, mockSocialUtils );
        Whitebox.setInternalState( votingService, mockUgcSearch );

        PowerMockito.mockStatic( RoleUtil.class );
        PowerMockito.mockStatic( SocialASRPUtils.class );
    }

    /**
     * Test submit and create vote resource teacher.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSubmitAndCreateVoteResourceTeacher() throws Exception {

        when( RoleUtil.getUserRole( mockSlingHttpServletRequest ) ).thenReturn( RoleUtil.ROLE_TYPE_TEACHER );
        when( SocialASRPUtils.fetchUserId( mockSlingHttpServletRequest, RoleUtil.ROLE_TYPE_TEACHER ) )
                .thenReturn( TEST_USER );
        when( SocialASRPUtils.isValidUser( RoleUtil.ROLE_TYPE_TEACHER, TEST_USER ) ).thenReturn( true );
        UgcFilter mockUGCfilter = PowerMockito.mock( UgcFilter.class );
        PowerMockito.whenNew( UgcFilter.class ).withNoArguments().thenReturn( mockUGCfilter );

        when( mockSlingHttpServletRequest.getResource() ).thenReturn( mockResource );
        when( CommonUtils.getResourceResolver( mockResourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE ) ).thenReturn( mockResourceResolver );
        when( mockSocialUtils.mayAccessUGC( mockResourceResolver ) ).thenReturn( true );
        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.VOTE_DATA ) )
                .thenReturn( TEST_DATA_TEACHER );
        when( mockSocialUtils.getSocialResourceProvider( mockResource ) ).thenReturn( mockSrp );
        when( mockSocialUtils.getDefaultStorageConfig() ).thenReturn( mockSocialResourceConfiguration );
        when( SocialASRPUtils.pathBuilder( TEST_PATH ) ).thenReturn( TEST_PATH );
        when( SocialASRPUtils.pathBuilder( TEST_PATH, TEST_QUESTION_UUID ) ).thenReturn( TEST_PATH );
        when( mockSrp.getResource( mockResourceResolver, TEST_PATH ) ).thenReturn( mockResource );

        when( mockResource.adaptTo( ModifiableValueMap.class ) ).thenReturn( mockPropertyMap );
        when( mockUgcSearch.find( null, mockResourceResolver, mockUGCfilter,
                ClassMagsMigrationASRPConstants.SOCIAL_SEARCH_OFFSET,
                ClassMagsMigrationASRPConstants.SOCIAL_SEARCH_INTIAL_LIMIT, true ) ).thenReturn( mockResults );

        assertEquals( mockResource, votingService.submitAndCreateVoteResource( mockSlingHttpServletRequest ) );
    }

    /**
     * Test submit and create vote resource teacher with no resource.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSubmitAndCreateVoteResourceTeacherWithNoResource() throws Exception {

        when( RoleUtil.getUserRole( mockSlingHttpServletRequest ) ).thenReturn( RoleUtil.ROLE_TYPE_TEACHER );
        when( SocialASRPUtils.fetchUserId( mockSlingHttpServletRequest, RoleUtil.ROLE_TYPE_TEACHER ) )
                .thenReturn( TEST_USER );
        when( SocialASRPUtils.isValidUser( RoleUtil.ROLE_TYPE_TEACHER, TEST_USER ) ).thenReturn( true );
        UgcFilter mockUGCfilter = PowerMockito.mock( UgcFilter.class );
        PowerMockito.whenNew( UgcFilter.class ).withNoArguments().thenReturn( mockUGCfilter );

        when( mockSlingHttpServletRequest.getResource() ).thenReturn( mockResource );
        when( CommonUtils.getResourceResolver( mockResourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE ) ).thenReturn( mockResourceResolver );
        when( mockSocialUtils.mayAccessUGC( mockResourceResolver ) ).thenReturn( true );
        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.VOTE_DATA ) )
                .thenReturn( TEST_DATA_TEACHER );
        when( mockSocialUtils.getSocialResourceProvider( mockResource ) ).thenReturn( mockSrp );
        when( mockSocialUtils.getDefaultStorageConfig() ).thenReturn( mockSocialResourceConfiguration );
        when( SocialASRPUtils.pathBuilder( TEST_PATH ) ).thenReturn( TEST_PATH );
        when( SocialASRPUtils.pathBuilder( TEST_PATH, TEST_QUESTION_UUID ) ).thenReturn( TEST_PATH );
        when( mockSrp.getResource( mockResourceResolver, TEST_PATH ) ).thenReturn( null );

        when( mockSrp.create( mockResourceResolver, TEST_PATH, mockPropsMap ) ).thenReturn( mockResource );
        when( mockUgcSearch.find( null, mockResourceResolver, mockUGCfilter,
                ClassMagsMigrationASRPConstants.SOCIAL_SEARCH_OFFSET,
                ClassMagsMigrationASRPConstants.SOCIAL_SEARCH_INTIAL_LIMIT, true ) ).thenReturn( mockResults );

        assertEquals( null, votingService.submitAndCreateVoteResource( mockSlingHttpServletRequest ) );
    }

    /**
     * Test submit and create vote resource student.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSubmitAndCreateVoteResourceStudent() throws Exception {

        when( RoleUtil.getUserRole( mockSlingHttpServletRequest ) ).thenReturn( RoleUtil.ROLE_TYPE_STUDENT );
        when( SocialASRPUtils.fetchUserId( mockSlingHttpServletRequest, RoleUtil.ROLE_TYPE_STUDENT ) )
                .thenReturn( TEST_USER );
        when( SocialASRPUtils.isValidUser( RoleUtil.ROLE_TYPE_STUDENT, TEST_USER ) ).thenReturn( true );
        UgcFilter mockUGCfilter = PowerMockito.mock( UgcFilter.class );
        PowerMockito.whenNew( UgcFilter.class ).withNoArguments().thenReturn( mockUGCfilter );

        when( mockSlingHttpServletRequest.getResource() ).thenReturn( mockResource );
        when( CommonUtils.getResourceResolver( mockResourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE ) ).thenReturn( mockResourceResolver );
        when( mockSocialUtils.mayAccessUGC( mockResourceResolver ) ).thenReturn( true );
        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.VOTE_DATA ) )
                .thenReturn( TEST_DATA_STUDENT );
        when( mockSocialUtils.getSocialResourceProvider( mockResource ) ).thenReturn( mockSrp );
        when( mockSocialUtils.getDefaultStorageConfig() ).thenReturn( mockSocialResourceConfiguration );
        when( SocialASRPUtils.pathBuilder( TEST_PATH ) ).thenReturn( TEST_PATH );
        when( SocialASRPUtils.pathBuilder( TEST_PATH, TEST_QUESTION_UUID ) ).thenReturn( TEST_PATH );
        when( mockSrp.getResource( mockResourceResolver, TEST_PATH ) ).thenReturn( mockResource );

        when( mockResource.adaptTo( ModifiableValueMap.class ) ).thenReturn( mockPropertyMap );
        when( mockUgcSearch.find( null, mockResourceResolver, mockUGCfilter,
                ClassMagsMigrationASRPConstants.SOCIAL_SEARCH_OFFSET,
                ClassMagsMigrationASRPConstants.SOCIAL_SEARCH_INTIAL_LIMIT, true ) ).thenReturn( mockResults );

        assertEquals( mockResource, votingService.submitAndCreateVoteResource( mockSlingHttpServletRequest ) );
    }

    /**
     * Test get vote data resource.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetVoteDataResource() throws Exception {

        when( RoleUtil.getUserRole( mockSlingHttpServletRequest ) ).thenReturn( RoleUtil.ROLE_TYPE_TEACHER );
        when( SocialASRPUtils.fetchUserId( mockSlingHttpServletRequest, RoleUtil.ROLE_TYPE_TEACHER ) )
                .thenReturn( TEST_USER );
        when( SocialASRPUtils.isValidUser( RoleUtil.ROLE_TYPE_TEACHER, TEST_USER ) ).thenReturn( true );

        when( mockSlingHttpServletRequest.getResource() ).thenReturn( mockResource );
        when( CommonUtils.getResourceResolver( mockResourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE ) ).thenReturn( mockResourceResolver );
        when( mockSocialUtils.mayAccessUGC( mockResourceResolver ) ).thenReturn( true );
        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.VOTE_DATA ) )
                .thenReturn( TEST_DATA_TEACHER );
        when( mockSocialUtils.getSocialResourceProvider( mockResource ) ).thenReturn( mockSrp );
        when( mockSocialUtils.getDefaultStorageConfig() ).thenReturn( mockSocialResourceConfiguration );
        when( SocialASRPUtils.pathBuilder( TEST_PATH ) ).thenReturn( TEST_PATH );
        when( SocialASRPUtils.pathBuilder( TEST_PATH, TEST_QUESTION_UUID ) ).thenReturn( TEST_PATH );
        when( SocialASRPUtils.getUGCResourceFromPath( mockResourceResolver, TEST_PATH, mockSrp ) )
                .thenReturn( mockResource );

        assertEquals( mockResource, votingService.getVoteDataResource( mockSlingHttpServletRequest ) );
    }
}
