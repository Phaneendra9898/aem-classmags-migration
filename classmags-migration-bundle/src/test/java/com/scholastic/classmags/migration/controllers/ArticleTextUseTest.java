package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.RepositoryException;
import javax.script.Bindings;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.scholastic.classmags.migration.services.ArticleTextDataService;

/**
 * JUnits for ArticleTextUse.
 */
@RunWith( MockitoJUnitRunner.class )
public class ArticleTextUseTest {

    /** The Constant PATH. */
    private static final String PATH = "/content";

    /** The Constant ARTICLE_TEST_DATA. */
    private static final String ARTICLE_TEST_DATA = "Test Data";

    /** The article text use. */
    private ArticleTextUse articleTextUse;

    /** The bindings. */
    @Mock
    private Bindings bindings;

    /** The current resource. */
    @Mock
    private Resource currentResource;

    /** The request. */
    @Mock
    private SlingHttpServletRequest request;

    /** The sling script helper. */
    @Mock
    private SlingScriptHelper slingScriptHelper;

    /** The article text data service. */
    @Mock
    private ArticleTextDataService articleTextDataService;

    /** The article data list. */
    @Mock
    private List< String > articleDataList;

    /**
     * Setup.
     *
     * @throws LoginException
     *             the login exception
     * @throws RepositoryException
     *             the repository exception
     */
    @Before
    public void setup() throws LoginException, RepositoryException {
        articleTextUse = new ArticleTextUse();
        articleDataList = new ArrayList< >();
        articleDataList.add( ARTICLE_TEST_DATA );
    }

    /**
     * Test get article data list.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetArticleDataList() throws Exception {

        when( bindings.get( "resource" ) ).thenReturn( currentResource );
        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( request.getResource() ).thenReturn( currentResource );
        when( currentResource.getPath() ).thenReturn( PATH );
        when( slingScriptHelper.getService( ArticleTextDataService.class ) ).thenReturn( articleTextDataService );
        when( articleTextDataService.fetchArticleData( PATH ) ).thenReturn( articleDataList );

        articleTextUse.init( bindings );
        articleTextUse.activate();
        assertEquals( ARTICLE_TEST_DATA, articleTextUse.getArticleDataList().get( 0 ) );
    }

    /**
     * Test get article data list with no sling script helper.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetArticleDataListWithNoSlingScriptHelper() throws Exception {

        when( bindings.get( "resource" ) ).thenReturn( currentResource );
        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "sling" ) ).thenReturn( null );

        articleTextUse.init( bindings );
        articleTextUse.activate();
        assertEquals( true, articleTextUse.getArticleDataList().isEmpty() );
    }

    /**
     * Test get article data list with no service.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetArticleDataListWithNoService() throws Exception {

        when( bindings.get( "resource" ) ).thenReturn( currentResource );
        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( request.getResource() ).thenReturn( currentResource );
        when( currentResource.getPath() ).thenReturn( PATH );
        when( slingScriptHelper.getService( ArticleTextDataService.class ) ).thenReturn( null );

        articleTextUse.init( bindings );
        articleTextUse.activate();
        assertEquals( true, articleTextUse.getArticleDataList().isEmpty() );
    }
}
