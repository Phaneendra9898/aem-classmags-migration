package com.scholastic.classmags.migration.servlets;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;

import javax.servlet.http.Cookie;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.apache.sling.api.request.RequestParameterMap;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.day.cq.commons.Externalizer;
import com.scholastic.classmags.migration.models.UserIdCookieData;
import com.scholastic.classmags.migration.services.LtiLaunchVerificationStrategy;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.CookieUtil;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * The Class CMSDMServletTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { CMSDMServlet.class, CommonUtils.class, InternalURLFormatter.class, CookieUtil.class } )
public class CMSDMServletTest {

    private static final String REQ_ATTR_CUSTOM_DP_LAUNCH = "{ \"identifiers\":{ \"staffId\":\"214064\" }, \"orgContext\":{ \"organization\":{ \"id\":\"498\", \"name\":\"A D OLIVER MIDDLE SCHOOL\", \"parent\":\"499\", \"orgType\":\"school\", \"identifiers\":{ \"ucn\":\"600077240\", \"sourceId\":\"158062\", \"source\":\"SPS\" }, \"lastModifiedDate\":\"2016-06-27T14:58:30.000-0400\", \"address\":{ \"city\":\"BROCKPORT\", \"state\":\"NY\", \"zipCode\":\"14420\", \"address1\":\"40 ALLEN ST\" }, \"countryCode\":\"USA\", \"currentSchoolYear\":\"2016\", \"overrideSchoolYearStart\":false, \"schoolStart\":{ \"monthOfYear\":8, \"dayOfMonth\":1 } }, \"entitlements\":[ { \"id\":\"10\", \"name\":\"Science World\", \"applicationCode\":\"scienceworld\", \"url\":\" https://scienceworld-aem-qa.scholastic.com/bin/classmags/migration/credManager\", \"thumbnail\":\"//digital.scholastic.com/media/applications/thumbnails/scienceworld_logo.jpg\", \"active\":true, \"groupCode\":\"CM\" }, { \"id\":\"19\", \"name\":\"Junior Scholastic\", \"applicationCode\":\"junior\", \"url\":\"https://junior-aem-qa.scholastic.com/bin/classmags/migration/credManager\", \"thumbnail\":\"//digital.scholastic.com/media/applications/thumbnails/junior_logo.jpg\", \"active\":true, \"groupCode\":\"CM\" } ], \"currentSchoolCalendar\":{ \"description\":\"2016-2017 School Year\", \"startDate\":\"2016-08-01\", \"endDate\":\"2017-07-31\" }, \"roles\":[ \"teacher\" ] }, \"launchContext\":{ \"application\":{ \"id\":\"10\", \"name\":\"Science World\", \"applicationCode\":\"scienceworld\", \"url\":\" https://scienceworld-aem-qa.scholastic.com/bin/classmags/migration/credManager\", \"thumbnail\":\"//digital.scholastic.com/media/applications/thumbnails/scienceworld_logo.jpg\", \"active\":true, \"groupCode\":\"CM\" }, \"productEntitlements\":[ { \"id\":\"40\", \"name\":\"Science World\", \"productCode\":\"cmscienceworld\", \"applicationId\":\"10\", \"active\":true, \"includesStudentAccess\":true, \"teacherDirectedSubscriptions\":{ \"enabled\":true } } ], \"role\":\"teacher\" }, \"teacherContext\":{ \"classes\":[ ] }, \"sessionContext\":{ \"sessionType\":\"DAS\", \"sessionToken\":\"676ab216-f0f3-4b07-a456-e2933f00cac3\" } }";
    private static final String REQ_ATTR_CUSTOM_NAV_CTX = "{ \"name\" : \"desai\", \"portalBaseUrl\" : \"https://dp-portal-qa1.scholastic.com\", \"orgId\" : \"498\", \"orgName\" : \"A D OLIVER MIDDLE SCHOOL\", \"orgType\" : \"school\", \"appCodes\" : [ \"scienceworld\", \"junior\" ], \"staff\" : true, \"admin\" : false, \"extSessionId\" : \"676ab216-f0f3-4b07-a456-e2933f00cac3\", \"extSessionEndpoint\" : \"https://jw1nnu8eu0.execute-api.us-east-1.amazonaws.com/qa1/extendedsession\", \"appCode\" : \"scienceworld\", \"appId\" : \"10\", \"parentOrgId\" : \"499\", \"env\" : \"qa1\" }";
    private static final String REQ_ATTR_OAUTH_CONSUMER_KEY = "FJfDkDA3WnoK6VWyhZDokTVfgfsa";
    private static final String REQ_ATTR_OAUTH_TIMESTAMP = "1492506449";
    private static final String CUSTOM_STATE_PARAM = "%2F";
    private static final String DATA_PAGE_PATH = "/content/classroom_magazines/admin/sw-data-page.html";
    private static final String LOGGED_OUT_PAGE_PATH = "/content/classroom_magazines/scienceworld/home-page-logged-out.html";
    private static final String EXTERNALIZED_LOGGED_OUT_PAGE_PATH = "https:scienceworld.scholastic.com/content/classroom_magazines/scienceworld/home-page-logged-out.html";
    private static final String LOGGED_IN_PAGE_PATH = "/content/classroom_magazines/scienceworld/home-page-logged-in.html";
    private static final String EXTERNALIZED_LOGGED_IN_PAGE_PATH = "https:scienceworld.scholastic.com/content/classroom_magazines/scienceworld/home-page-logged-in.html";
    private static final String SDM_LOG_OUT_PAGE = "https://dp-portal-qa1.scholastic.com";
    private static final String ERROR_JSON = "\"identifiers\":{ \"staffId\":\"214064\" }, \"orgContext\":{ \"organization\":{ \"id\":\"498\", \"name\":\"A D OLIVER MIDDLE SCHOOL\", \"parent\":\"499\", \"orgType\":\"school\", \"identifiers\":{ \"ucn\":\"600077240\", \"sourceId\":\"158062\", \"source\":\"SPS\" }, \"lastModifiedDate\":\"2016-06-27T14:58:30.000-0400\", \"address\":{ \"city\":\"BROCKPORT\", \"state\":\"NY\", \"zipCode\":\"14420\", \"address1\":\"40 ALLEN ST\" }, \"countryCode\":\"USA\", \"currentSchoolYear\":\"2016\", \"overrideSchoolYearStart\":false, \"schoolStart\":{ \"monthOfYear\":8, \"dayOfMonth\":1 } }, \"entitlements\":[ { \"id\":\"10\", \"name\":\"Science World\", \"applicationCode\":\"scienceworld\", \"url\":\" https://scienceworld-aem-qa.scholastic.com/bin/classmags/migration/credManager\", \"thumbnail\":\"//digital.scholastic.com/media/applications/thumbnails/scienceworld_logo.jpg\", \"active\":true, \"groupCode\":\"CM\" }, { \"id\":\"19\", \"name\":\"Junior Scholastic\", \"applicationCode\":\"junior\", \"url\":\"https://junior-aem-qa.scholastic.com/bin/classmags/migration/credManager\", \"thumbnail\":\"//digital.scholastic.com/media/applications/thumbnails/junior_logo.jpg\", \"active\":true, \"groupCode\":\"CM\" } ], \"currentSchoolCalendar\":{ \"description\":\"2016-2017 School Year\", \"startDate\":\"2016-08-01\", \"endDate\":\"2017-07-31\" }, \"roles\":[ \"teacher\" ] }, \"launchContext\":{ \"application\":{ \"id\":\"10\", \"name\":\"Science World\", \"applicationCode\":\"scienceworld\", \"url\":\" https://scienceworld-aem-qa.scholastic.com/bin/classmags/migration/credManager\", \"thumbnail\":\"//digital.scholastic.com/media/applications/thumbnails/scienceworld_logo.jpg\", \"active\":true, \"groupCode\":\"CM\" }, \"productEntitlements\":[ { \"id\":\"40\", \"name\":\"Science World\", \"productCode\":\"cmscienceworld\", \"applicationId\":\"10\", \"active\":true, \"includesStudentAccess\":true, \"teacherDirectedSubscriptions\":{ \"enabled\":true } } ], \"role\":\"teacher\" }, \"teacherContext\":{ \"classes\":[ ] }, \"sessionContext\":{ \"sessionType\":\"DAS\", \"sessionToken\":\"676ab216-f0f3-4b07-a456-e2933f00cac3\" } }";
    private static final String ERROR_OAUTH_TIMESTAMP = "149A;6449";

    private CMSDMServlet cMSDMServlet;
    private SlingHttpServletRequest request;
    private SlingHttpServletResponse response;
    private Logger logger;
    private RequestParameterMap requestParameterMap;
    private RequestParameter requestParameter;
    private RequestParameter[] requestParameters;
    private Entry< String, RequestParameter[] > entry;
    private Set entrySet;
    private ResourceResolver resourceResolver;
    private PropertyConfigService propertyConfigService;
    private ResourcePathConfigService resourcePathConfigService;
    private Externalizer externalizer;
    private LtiLaunchVerificationStrategy ltiLaunchStrategy;
    private Cookie cookie;
    private UserIdCookieData userIdCookieData;

    @SuppressWarnings( { "unchecked", "rawtypes" } )
    @Before
    public void setUp() throws Exception {

        cMSDMServlet = Whitebox.newInstance( CMSDMServlet.class );

        request = mock( SlingHttpServletRequest.class );
        response = mock( SlingHttpServletResponse.class );
        requestParameterMap = mock( RequestParameterMap.class );
        requestParameter = mock( RequestParameter.class );
        resourceResolver = mock( ResourceResolver.class );
        logger = mock( Logger.class );
        propertyConfigService = mock( PropertyConfigService.class );
        resourcePathConfigService = mock( ResourcePathConfigService.class );
        externalizer = mock( Externalizer.class );
        ltiLaunchStrategy = mock( LtiLaunchVerificationStrategy.class );
        cookie = mock( Cookie.class );
        entry = mock( Entry.class );
        entrySet = new HashSet();
        entrySet.add( entry );
        entrySet.add( entry );

        userIdCookieData = new UserIdCookieData();
        requestParameters = new RequestParameter[] { requestParameter };

        Whitebox.setInternalState( CMSDMServlet.class, "LOG", logger );
        Whitebox.setInternalState( cMSDMServlet, propertyConfigService );
        Whitebox.setInternalState( cMSDMServlet, resourcePathConfigService );
        Whitebox.setInternalState( cMSDMServlet, ltiLaunchStrategy );

        PowerMockito.mockStatic( CommonUtils.class );
        PowerMockito.mockStatic( InternalURLFormatter.class );
        PowerMockito.mockStatic( CookieUtil.class );

        when( request.getRequestParameterMap() ).thenReturn( requestParameterMap );
        when( request.getParameter( "custom_dp_launch" ) ).thenReturn( REQ_ATTR_CUSTOM_DP_LAUNCH );
        when( request.getParameter( "custom_sdm_nav_ctx" ) ).thenReturn( REQ_ATTR_CUSTOM_NAV_CTX );
        when( request.getParameter( "oauth_timestamp" ) ).thenReturn( REQ_ATTR_OAUTH_TIMESTAMP );
        when( request.getParameter( "oauth_consumer_key" ) ).thenReturn( REQ_ATTR_OAUTH_CONSUMER_KEY );
        when( request.getParameter( "custom_state" ) ).thenReturn( CUSTOM_STATE_PARAM );
        when( entry.getKey() ).thenReturn( "key" );
        when( entry.getValue() ).thenReturn( requestParameters );
        when( requestParameterMap.entrySet() ).thenReturn( entrySet );
        when( requestParameter.getString() ).thenReturn( "Param String" );
        when( request.getResourceResolver() ).thenReturn( resourceResolver );
        when( CommonUtils.getDataPath( resourcePathConfigService, "scienceworld" ) ).thenReturn( DATA_PAGE_PATH );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.HOMEPAGE_LOGGEDOUT,
                DATA_PAGE_PATH ) ).thenReturn( LOGGED_OUT_PAGE_PATH );
        when( InternalURLFormatter.formatURL( resourceResolver, LOGGED_OUT_PAGE_PATH ) )
                .thenReturn( LOGGED_OUT_PAGE_PATH );
        when( resourceResolver.adaptTo( Externalizer.class ) ).thenReturn( externalizer );
        when( externalizer.externalLink( resourceResolver, "scienceworld", LOGGED_OUT_PAGE_PATH ) )
                .thenReturn( EXTERNALIZED_LOGGED_OUT_PAGE_PATH );
        when( resourceResolver.map( EXTERNALIZED_LOGGED_OUT_PAGE_PATH ) )
                .thenReturn( EXTERNALIZED_LOGGED_OUT_PAGE_PATH );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.HOMEPAGE_LOGGEDIN,
                DATA_PAGE_PATH ) ).thenReturn( LOGGED_IN_PAGE_PATH );
        when( InternalURLFormatter.formatURL( resourceResolver, LOGGED_IN_PAGE_PATH ) )
                .thenReturn( LOGGED_IN_PAGE_PATH );
        when( externalizer.externalLink( resourceResolver, "scienceworld", LOGGED_IN_PAGE_PATH ) )
                .thenReturn( EXTERNALIZED_LOGGED_IN_PAGE_PATH );
        when( resourceResolver.map( EXTERNALIZED_LOGGED_IN_PAGE_PATH ) ).thenReturn( EXTERNALIZED_LOGGED_IN_PAGE_PATH );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.SDM_LOGOUT, DATA_PAGE_PATH ) )
                .thenReturn( SDM_LOG_OUT_PAGE );
        when( ltiLaunchStrategy.performOAuthSignatureValidation( any( SortedMap.class ), any( String.class ),
                any( String.class ), any( String.class ) ) ).thenReturn( true );
        when( ltiLaunchStrategy.performOAuthTimeStampValidation( any( Long.class ), any( Long.class ) ) )
                .thenReturn( true );
        when( CookieUtil.createCookie( any( String.class ), any( String.class ), any( String.class ),
                any( String.class ), any( Boolean.class ), any( Boolean.class ) ) ).thenReturn( cookie );
        when( CookieUtil.mapRequestParamCustomDpLaunch( REQ_ATTR_CUSTOM_DP_LAUNCH ) ).thenReturn( userIdCookieData );
    }

    @Test
    public void testCMSDMServlet() throws Exception {

        // execute logic
        Whitebox.invokeMethod( cMSDMServlet, "doPost", request, response );

        // verify logic
        verify( logger, times( 0 ) ).debug( "OAuth Signature is invalid !" );
    }

    @Test
    public void testCMSDMServletWhenCustomStateParam() throws Exception {

        // setup logic ( test-specific )
        when( request.getParameter( "custom_state" ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( cMSDMServlet, "doPost", request, response );

        // verify logic
        verify( logger, times( 0 ) ).debug( "OAuth Signature is invalid !" );
    }

    @Test
    public void testCMSDMServletWhenOAuthValidationIsFalseOne() throws Exception {

        // setup logic ( test-specific )
        when( ltiLaunchStrategy.performOAuthSignatureValidation( any( SortedMap.class ), any( String.class ),
                any( String.class ), any( String.class ) ) ).thenReturn( false );

        // execute logic
        Whitebox.invokeMethod( cMSDMServlet, "doPost", request, response );

        // verify logic
        verify( logger, times( 1 ) ).debug( "OAuth Signature is invalid !" );
    }

    @Test
    public void testCMSDMServletWhenCustomSDMNavCtxIsEmpty() throws Exception {

        // setup logic ( test-specific )
        when( request.getParameter( "custom_sdm_nav_ctx" ) ).thenReturn( StringUtils.EMPTY );

        // execute logic
        Whitebox.invokeMethod( cMSDMServlet, "doPost", request, response );

        // verify logic
        verify( logger, times( 0 ) ).debug( "OAuth Signature is invalid !" );
    }

    @Test
    public void testCMSDMServletWhenOAuthValidationIsFalseTwo() throws Exception {

        // setup logic ( test-specific )
        when( ltiLaunchStrategy.performOAuthTimeStampValidation( any( Long.class ), any( Long.class ) ) )
                .thenReturn( false );

        // execute logic
        Whitebox.invokeMethod( cMSDMServlet, "doPost", request, response );

        // verify logic
        verify( logger, times( 1 ) ).debug( "OAuth Signature is invalid !" );
    }

    @Test
    public void testCMSDMServletWhenException() throws Exception {

        // setup logic ( test-specific )
        when( request.getParameter( "oauth_timestamp" ) ).thenReturn( ERROR_OAUTH_TIMESTAMP );
        when( request.getParameter( "custom_dp_launch" ) ).thenReturn( ERROR_JSON );

        // execute logic
        Whitebox.invokeMethod( cMSDMServlet, "doPost", request, response );
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final CMSDMServlet cMSDMServlet = new CMSDMServlet();

        // verify logic
        assertNotNull( cMSDMServlet );
    }
}