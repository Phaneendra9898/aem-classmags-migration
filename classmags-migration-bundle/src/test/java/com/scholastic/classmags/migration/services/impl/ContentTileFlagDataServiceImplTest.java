package com.scholastic.classmags.migration.services.impl;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.scholastic.classmags.migration.models.ArticleResourcesData;
import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * The Class ContentTileFlagDataServiceImplTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { ContentTileFlagDataServiceImpl.class, CommonUtils.class, InternalURLFormatter.class } )
public class ContentTileFlagDataServiceImplTest {

    /** The content tile flag data service. */
    private ContentTileFlagDataServiceImpl contentTileFlagDataService;

    /** The Constant PATH. */
    private static final String TEST_PATH = "/testPath";

    /** The Constant TEST_RESOURCE_TYPE. */
    private static final String TEST_RESOURCE_TYPE = "articleResourceType";

    /** The Constant TEST_DATA. */
    private static final String TEST_DATA = "testData";

    /** The resource resolver factory. */
    @Mock
    private ResourceResolverFactory mockResourceResolverFactory;

    /** The resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock value map. */
    @Mock
    private ValueMap mockValueMap;

    /** The mock session. */
    @Mock
    private Session mockSession;

    /** The mock query builder. */
    @Mock
    private QueryBuilder mockQueryBuilder;

    /** The mock query map. */
    @Mock
    private Map< String, String > mockQueryMap;

    /** The query. */
    @Mock
    private Query query;

    /** The search result. */
    @Mock
    private SearchResult searchResult;

    /** The mock article configuration nodes list. */
    @Mock
    private List< Hit > mockArticleConfigurationNodesList;

    /** The mock hit. */
    @Mock
    private Hit mockHit;

    /** The mock resources. */
    @Mock
    private Map< String, List< ResourcesObject > > mockResources;

    /** The mock iterator. */
    @Mock
    private Iterator< Hit > mockIterator;

    /** The mock iterator resource. */
    @Mock
    private Iterator< Resource > mockIteratorResource;

    /** The mock set iterator. */
    @Mock
    private Iterator< String > mockSetIterator;

    /** The mock set. */
    @Mock
    private Set< String > mockSet;

    /** The mock resources object list. */
    @Mock
    private List< ResourcesObject > mockResourcesObjectList;

    /** The mock resources object. */
    @Mock
    private ResourcesObject mockResourcesObject;

    /** The mock resources object list iterator. */
    @Mock
    private Iterator< ResourcesObject > mockResourcesObjectListIterator;

    /** The mock article resource data list. */
    @Mock
    private List< ArticleResourcesData > mockArticleResourceDataList;

    /** The mock article resource data list iterator. */
    @Mock
    private Iterator< ArticleResourcesData > mockArticleResourceDataListIterator;

    /** The mock article resources data. */
    @Mock
    private ArticleResourcesData mockArticleResourcesData;

    /**
     * Sets the up.
     */
    @Before
    public void setUp() {
        contentTileFlagDataService = new ContentTileFlagDataServiceImpl();

        Whitebox.setInternalState( contentTileFlagDataService, mockResourceResolverFactory );
        Whitebox.setInternalState( contentTileFlagDataService, mockQueryBuilder );

        PowerMockito.mockStatic( CommonUtils.class );
        PowerMockito.mockStatic( InternalURLFormatter.class );

        when( CommonUtils.getResourceResolver( mockResourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ) )
                .thenReturn( mockResourceResolver );

        mockQueryMap = new HashMap< String, String >();
        mockQueryMap.put( ClassMagsMigrationConstants.QUERY_PATH, TEST_PATH );
        mockQueryMap.put( ClassMagsMigrationConstants.RESULTS_LIMIT, ClassMagsMigrationConstants.QUERY_LOWER_BOUND );
        mockQueryMap.put( ClassMagsMigrationConstants.QUERY_PARAM_1,
                ClassMagsMigrationConstants.QUERY_RESOURCE_TYPE_TAG );
        mockQueryMap.put( ClassMagsMigrationConstants.QUERY_PARAM_1_VALUE,
                ClassMagsMigrationConstants.ARTICLE_CONFIG_RESOURCE_TYPE_PATH );
    }

    /**
     * Test configure article page and flag data.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testConfigureArticlePageAndFlagData() throws Exception {

        when( mockResourceResolver.adaptTo( Session.class ) ).thenReturn( mockSession );
        when( mockQueryBuilder.createQuery( PredicateGroup.create( mockQueryMap ), mockSession ) ).thenReturn( query );
        when( query.getResult() ).thenReturn( searchResult );
        when( searchResult.getHits() ).thenReturn( mockArticleConfigurationNodesList );
        when( mockArticleConfigurationNodesList.isEmpty() ).thenReturn( false );
        when( mockArticleConfigurationNodesList.iterator() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( mockHit );
        when( mockHit.getResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ClassMagsMigrationConstants.RESOURCES ) ).thenReturn( mockResource );
        when( mockResource.listChildren() ).thenReturn( mockIteratorResource );
        when( mockIteratorResource.hasNext() ).thenReturn( true, false );
        when( mockIteratorResource.next() ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.PN_RESOURCE_TYPE, StringUtils.EMPTY ) )
                .thenReturn( TEST_RESOURCE_TYPE );
        when( mockResource.getParent() ).thenReturn( mockResource );
        when( mockResource.getPath() ).thenReturn( TEST_PATH );
        when( mockResources.keySet() ).thenReturn( mockSet );
        when( mockSet.iterator() ).thenReturn( mockSetIterator );
        when( mockSetIterator.hasNext() ).thenReturn( true, false );
        when( mockSetIterator.next() ).thenReturn( TEST_DATA );
        when( mockResources.get( TEST_DATA ) ).thenReturn( mockResourcesObjectList );
        when( mockResourcesObjectList.iterator() ).thenReturn( mockResourcesObjectListIterator );
        when( mockResourcesObjectListIterator.hasNext() ).thenReturn( true, false );
        when( mockResourcesObjectListIterator.next() ).thenReturn( mockResourcesObject );
        when( mockResourcesObject.getPagePath() ).thenReturn( TEST_PATH );
        when( CommonUtils.isAShareableAsset( TEST_PATH ) ).thenReturn( true );
        when( mockArticleResourceDataList.isEmpty() ).thenReturn( false );
        when( mockArticleResourceDataList.iterator() ).thenReturn( mockArticleResourceDataListIterator );
        when( mockArticleResourceDataListIterator.hasNext() ).thenReturn( true, false );
        when( mockArticleResourceDataListIterator.next() ).thenReturn( mockArticleResourcesData );
        when( mockArticleResourcesData.getArticleResourceType() )
                .thenReturn( TEST_DATA.concat( ClassMagsMigrationConstants.HYPHEN ).concat( TEST_PATH ) );
        when( InternalURLFormatter.formatURL( mockResourceResolver, mockArticleResourcesData.getArticlePagePath() ) )
                .thenReturn( TEST_PATH );

        contentTileFlagDataService.configureArticlePageAndFlagData( TEST_PATH, mockResources );
    }
}
