package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import javax.script.Bindings;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import com.day.cq.dam.api.Asset;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.GameModel;
import com.scholastic.classmags.migration.services.GameService;


/**
 * Junit for GameComponentUse
 */
@RunWith(MockitoJUnitRunner.class)
public class GameComponentUseTest {
    /** The mock request. */
    @Mock
    private SlingHttpServletRequest mockRequest;

    /** The mock bindings. */
    @Mock
    private Bindings mockBindings;

    /** The mock sling script helper. */
    @Mock
    private SlingScriptHelper mockSlingScriptHelper;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    private GameComponentUse gameComponentUse;

    @Mock
    private GameService mockGameService;
    
    @Mock
    private GameModel mockData;
    
    @Mock
    private Resource mockAssetResource;
    
    @Mock
    private Asset mockAsset;

    /**
     * Setup.
     * 
     * @throws ClassmagsMigrationBaseException
     */
    @Before
    public void setup() throws ClassmagsMigrationBaseException {
        gameComponentUse = new GameComponentUse();
        when(mockSlingScriptHelper.getService(GameService.class)).thenReturn(mockGameService); 
    }
    
    /**
     * Test getData() when
     * mockResource=null
     */
    @Test
    public void getDataWithNoResource() {
        when(mockBindings.get("resource")).thenReturn(null); 
        gameComponentUse.init(mockBindings);
        assertNull(null,gameComponentUse.getData());
    }
    
    /**
     * Test getData() when
     * mockRequest=null
     */
    @Test
    public void getDataWithNoSlingScriptHelper() {
        when(mockBindings.get("resource")).thenReturn(mockResource); 
        when(mockBindings.get("sling")).thenReturn(null);
        gameComponentUse.init(mockBindings);
        assertNull(null,gameComponentUse.getData());
    }
    
    
    /**
     * Test getData()
     */
    @Test
    public void getDataTest() throws Exception {
        when(mockBindings.get("resource")).thenReturn(mockResource); 
        when(mockBindings.get("sling")).thenReturn(mockSlingScriptHelper);     
        when(mockGameService.fetchGameData(mockResource)).thenReturn(mockData);
        gameComponentUse.init(mockBindings);
        gameComponentUse.activate();
        assertEquals(mockData,gameComponentUse.getData());
 
    }
    /**
     * Test getData() 
     * @throws Exception 
     *
     */
    @Test
    public void getDataWithException() throws Exception{
        LoginException loginexception=new LoginException();
        
        when(mockBindings.get("resource")).thenReturn(mockResource);
        when(mockBindings.get("sling")).thenReturn(mockSlingScriptHelper);       
        when(mockGameService.fetchGameData(mockResource)).thenThrow(loginexception);
        gameComponentUse.init(mockBindings);
        gameComponentUse.activate();
        assertNull(gameComponentUse.getData());
    }
    
    
    
}