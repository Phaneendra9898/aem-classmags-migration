package com.scholastic.classmags.migration.servlets;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

import com.scholastic.classmags.migration.services.CreateBrightcoveVideoService;
import com.scholastic.classmags.migration.services.impl.CreateBrightcoveVideoServiceImpl;

/**
 * The Class CreateBrightcoveVideoServletTest.
 */
@RunWith( MockitoJUnitRunner.class )
public class CreateBrightcoveVideoServletTest extends TestCase {

    /** The request. */
    @Mock
    SlingHttpServletRequest request;

    /** The response. */
    @Mock
    SlingHttpServletResponse response;

    /** The create video from brightcove service. */
    @InjectMocks
    private CreateBrightcoveVideoService createVideoFromBrightcoveService = new CreateBrightcoveVideoServiceImpl();

    /** The response message. */
    @Mock
    private Map< String, String > responseMessage;

    /** The create video from brightcove service 2. */
    @Mock
    private CreateBrightcoveVideoService createVideoFromBrightcoveService2;

    /** The servlet. */
    @InjectMocks
    public CreateBrightcoveVideoServlet servlet = new CreateBrightcoveVideoServlet();

    /** The video servlet. */
    @Mock
    private CreateBrightcoveVideoServlet videoServlet;

    /** The printwriter. */
    @Mock
    private PrintWriter printwriter;

    /*
     * (non-Javadoc)
     * 
     * @see junit.framework.TestCase#setUp()
     */
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks( this );
        Whitebox.setInternalState( new CreateBrightcoveVideoServlet(), "createVideoFromBrightcoveService",
                createVideoFromBrightcoveService );
    }

    /**
     * Test do post success.
     * 
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDoPostSuccess() throws Exception {
        when( request.getParameter( "brightcoveVideoID" ) ).thenReturn( "4803777318001" );
        when( request.getParameter( "damPath" ) ).thenReturn( "/content" );
        when( createVideoFromBrightcoveService2.createVideoinAEM( "4803777318001", "/content" ) ).thenReturn(
                responseMessage );
        when( responseMessage.get( "Success" ) ).thenReturn( "Video successfully created in AEM" );
        when( response.getWriter() ).thenReturn( printwriter );
        servlet.doPost( request, response );
        assertEquals( false, response.getWriter().checkError() );
    }

    /**
     * Test do post error.
     * 
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDoPostError() throws Exception {
        when( request.getParameter( "brightcoveVideoID" ) ).thenReturn( "4803777318001" );
        when( request.getParameter( "damPath" ) ).thenReturn( "/content" );
        Map< String, String > errorResponseMessage = new HashMap< String, String >();
        errorResponseMessage.put( "Error", "Please make sure you typed a valid video ID and Brightcove is accesible" );
        when( createVideoFromBrightcoveService2.createVideoinAEM( "4803777318001", "/content" ) ).thenReturn(
                errorResponseMessage );
        when( response.getWriter() ).thenReturn( printwriter );
        servlet.doPost( request, response );
        assertEquals( false, response.getWriter().checkError() );
    }

    /**
     * Test do post with no brighcove video id dam path.
     * 
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDoPostWithNoBrighcoveVideoIdDamPath() throws Exception {
        when( request.getParameter( "brightcoveVideoID" ) ).thenReturn( "" );
        when( request.getParameter( "damPath" ) ).thenReturn( "" );
        when( response.getWriter() ).thenReturn( printwriter );
        servlet.doPost( request, response );
        verify( response ).setStatus( 400 );
    }

    /**
     * Test do post with no brighcove video id.
     * 
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDoPostWithNoBrighcoveVideoId() throws Exception {
        when( request.getParameter( "brightcoveVideoID" ) ).thenReturn( "" );
        when( request.getParameter( "damPath" ) ).thenReturn( "/content" );
        when( response.getWriter() ).thenReturn( printwriter );
        servlet.doPost( request, response );
        assertEquals( false, response.getWriter().checkError() );
    }

    /**
     * Test do post with no dam path.
     * 
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDoPostWithNoDamPath() throws Exception {
        when( request.getParameter( "brightcoveVideoID" ) ).thenReturn( "1234" );
        when( request.getParameter( "damPath" ) ).thenReturn( "" );
        when( response.getWriter() ).thenReturn( printwriter );
        servlet.doPost( request, response );
        assertEquals( false, response.getWriter().checkError() );
    }

    /**
     * Test do post exception.
     * 
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDoPostException() throws Exception {
        when( request.getParameter( "brightcoveVideoID" ) ).thenReturn( "1234" );
        when( request.getParameter( "damPath" ) ).thenReturn( "" );
        doThrow( new NullPointerException() ).when( videoServlet ).doPost( request, response );
        servlet.doPost( request, response );
    }
}
