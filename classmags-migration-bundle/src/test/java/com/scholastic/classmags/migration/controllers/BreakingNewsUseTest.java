package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import java.util.Calendar;
import java.util.Date;

import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

/**
 * JUnit for BreakingNewsUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { BreakingNewsUse.class } )
public class BreakingNewsUseTest extends BaseComponentTest {

    private static final String LAST_MODIFIED = "jcr:lastModified";

    private BreakingNewsUse breakingNewsUse;
    private ValueMap properties;
    private Date date;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        breakingNewsUse = Whitebox.newInstance( BreakingNewsUse.class );
        properties = mock( ValueMap.class );

        Calendar dt = Calendar.getInstance();
        dt.set( Calendar.YEAR, 2016 );
        dt.set( Calendar.MONTH, 10 );
        dt.set( Calendar.DAY_OF_MONTH, 16 );
        date = dt.getTime();

        stubCommon( breakingNewsUse );

        when( resource.getValueMap() ).thenReturn( properties );
        when( properties.containsKey( LAST_MODIFIED ) ).thenReturn( true );
        when( properties.get( LAST_MODIFIED, Date.class ) ).thenReturn( date );
    }

    @Test
    public void testBreakingNewsUse() throws Exception {

        // execute logic
        Whitebox.invokeMethod( breakingNewsUse, "activate" );

        // verify logic
        assertEquals( ( Long ) date.getTime(), breakingNewsUse.getLastModified() );
    }

    @Test
    public void testBreakingNewsUseWhenResourceIsNull() throws Exception {
        // setup logic(test-specific)
        stub( PowerMockito.method( BreakingNewsUse.class, "getResource" ) ).toReturn( null );

        // execute logic
        Whitebox.invokeMethod( breakingNewsUse, "activate" );

        // verify logic
        assertNull( breakingNewsUse.getLastModified() );
    }

    @Test
    public void testBreakingNewsUseWhenPropertiesIsNull() throws Exception {
        // setup logic(test-specific)
        when( resource.getValueMap() ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( breakingNewsUse, "activate" );

        // verify logic
        assertNull( breakingNewsUse.getLastModified() );
    }

    @Test
    public void testBreakingNewsUseWhenLastModifiedPropertyNotPresent() throws Exception {
        // setup logic(test-specific)
        when( properties.containsKey( LAST_MODIFIED ) ).thenReturn( false );

        // execute logic
        Whitebox.invokeMethod( breakingNewsUse, "activate" );

        // verify logic
        assertNull( breakingNewsUse.getLastModified() );
    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final BreakingNewsUse breakingNewsUse = new BreakingNewsUse();

        // verify logic
        assertNotNull( breakingNewsUse );
    }

}
