package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for IssueHighlightDataService.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { IssueHighlightDataServiceImpl.class, CommonUtils.class} )
public class IssueHighlightDataServiceImplTest {

    private static final Object DESCRIPTION = "test sample description";
    private static final Object IMAGE = "/content/dam/sample-image-path";
    private static final String CURRENT_PATH = "/content/classroom_magazines/scienceworld/homepage/ISSUE-PAGE-2";
    private static final String MAGAZINE_NAME = "scienceworld";
    private static final String DATE_FORMAT = "MMMM d, yyyy";
    private static final String DATA_PATH = "/content/classroom-magazines-admin/scienceworld/global-data-config-page";
    private static final String ISSUE_DATE = "November 30, 2016";
    private static final Object ISSUE_SORTING_DATE = "2016-11-30T00:00:00.000+05:30";
    private static final String ISSUE_DISPLAY_DATE = "November/December";

    /** The issue highlight data service. */
    private IssueHighlightDataServiceImpl issueHighlightDataService;

    /** The resource path config service. */
    private ResourcePathConfigService resourcePathConfigService;
    
    /** The property config service. */
    private PropertyConfigService propertyConfigService;
    
    /** The magazine props service. */
    private MagazineProps magazineProps;
    
    /** The resource resolver factory. */
    private ResourceResolverFactory resourceResolverFactory;
    
    /** The resource resolver. */
    private ResourceResolver resourceResolver;

    /** The resource. */
    private Resource resource;
    
    /** The issue page properties. */
    private List<ValueMap> multifieldData;

    /**
     * Initial Setup.
     * 
     * @throws LoginException
     */
    @Before
    public void setUp() throws LoginException {
        issueHighlightDataService = new IssueHighlightDataServiceImpl();
        resourceResolverFactory = mock( ResourceResolverFactory.class );
        resourcePathConfigService = mock( ResourcePathConfigService.class );
        propertyConfigService = mock( PropertyConfigService.class );
        magazineProps = mock( MagazineProps.class );
        resourceResolver = mock( ResourceResolver.class );
        resource = mock( Resource.class );

        multifieldData = createMultifieldData();

        PowerMockito.mockStatic( CommonUtils.class );

        Whitebox.setInternalState( issueHighlightDataService, resourceResolverFactory );
        Whitebox.setInternalState( issueHighlightDataService, resourcePathConfigService );
        Whitebox.setInternalState( issueHighlightDataService, propertyConfigService );
        Whitebox.setInternalState( issueHighlightDataService, magazineProps );

        when( CommonUtils.getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ) )
                .thenReturn( resourceResolver );
        when( resourceResolver.getResource( CURRENT_PATH.concat( ClassMagsMigrationConstants.JCR_ISSUE_ASSETS_PATH ) ) )
                .thenReturn( resource );
        when( magazineProps.getMagazineName( CURRENT_PATH ) ).thenReturn( MAGAZINE_NAME );
        when( CommonUtils.getDataPath( resourcePathConfigService, MAGAZINE_NAME ) ).thenReturn( DATA_PATH );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                DATA_PATH ) ).thenReturn( DATE_FORMAT );
        when( CommonUtils.fetchMultiFieldData( resource ) ).thenReturn( multifieldData );
    }

    /**
     * Test fetch issue data.
     */
    @Test
    public void testFetchIssueData() {
        
        // execute logic
        List< ValueMap > issueData = issueHighlightDataService.fetchAssetData(
                "/content/classroom_magazines/scienceworld/homepage/ISSUE-PAGE-2/jcr:content/par/issue_highlight" );

        // verify logic
        assertEquals(2, issueData.size());
        assertEquals( DESCRIPTION, issueData.get( 0 ).get( ClassMagsMigrationConstants.PP_DESCRIPTION ) );
        assertEquals( IMAGE, issueData.get( 0 ).get( ClassMagsMigrationConstants.FILE_REFERENCE ) );
    }
    
    /**
     * Test fetch issue sorting date.
     * 
     * @throws ClassmagsMigrationBaseException 
     * @throws LoginException 
     */
    @Test
    public void testFetchIssueSortingDate() throws ClassmagsMigrationBaseException {
        // setup logic ( test-specific )
        ValueMap issuePageProperties = mock( ValueMap.class );

        when( CommonUtils.fetchPagePropertyMap( CURRENT_PATH, resourceResolverFactory ) )
                .thenReturn( issuePageProperties );
        when( issuePageProperties.get( "sortingDate" ) ).thenReturn( ISSUE_SORTING_DATE );
        when( CommonUtils.getSortingDate( issuePageProperties, DATE_FORMAT ) ).thenReturn( ISSUE_DATE );

        // execute logic
        String issueSortingDate = issueHighlightDataService.fetchIssueSortingDate( CURRENT_PATH );

        // verify logic
        assertEquals( ISSUE_DATE, issueSortingDate );

    }
    
    /**
     * Test fetch issue sorting date.
     * 
     * @throws ClassmagsMigrationBaseException The classmags migration base exception. 
     * @throws LoginException The login exception.
     */
    @Test( expected = ClassmagsMigrationBaseException.class )
    public void testFetchIssueSortingDateWithException() throws ClassmagsMigrationBaseException, LoginException {
        // setup logic (test-specific)
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                DATA_PATH ) ).thenThrow( new LoginException() );

        // execute logic
        String issueDate = issueHighlightDataService.fetchIssueSortingDate( CURRENT_PATH );

        // verify logic
        assertEquals( ISSUE_DATE, issueDate );

    }
    
    /**
     * Test fetch issue display date.
     * 
     * @throws ClassmagsMigrationBaseException 
     * @throws LoginException 
     */
    @Test
    public void testFetchIssueDisplayDate() throws ClassmagsMigrationBaseException {
        // setup logic ( test-specific )
        ValueMap issuePageProperties = mock( ValueMap.class );

        when( CommonUtils.fetchPagePropertyMap( CURRENT_PATH, resourceResolverFactory ) )
                .thenReturn( issuePageProperties );
        when( issuePageProperties.get( "displayDate" ) ).thenReturn( ISSUE_DISPLAY_DATE );
        when( CommonUtils.getDisplayDate( issuePageProperties, DATE_FORMAT ) ).thenReturn( ISSUE_DISPLAY_DATE );

        // execute logic
        String issueSortingDate = issueHighlightDataService.fetchIssueDisplayDate( CURRENT_PATH );

        // verify logic
        assertEquals( ISSUE_DISPLAY_DATE, issueSortingDate );

    }
    
    /**
     * Test fetch issue display date with exception.
     * 
     * @throws ClassmagsMigrationBaseException The classmags migration base exception. 
     * @throws LoginException The login exception.
     */
    @Test( expected = ClassmagsMigrationBaseException.class )
    public void testFetchIssueDisplayDateWithException() throws ClassmagsMigrationBaseException, LoginException {
        // setup logic (test-specific)
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                DATA_PATH ) ).thenThrow( new LoginException() );

        // execute logic
        String issueDate = issueHighlightDataService.fetchIssueDisplayDate( CURRENT_PATH );

        // verify logic
        assertEquals( ISSUE_DATE, issueDate );

    }
    
    /*
     * Creates the multifield data.
     */
    private List< ValueMap > createMultifieldData() {

        List< ValueMap > issueDataList = new ArrayList< ValueMap >();
        ValueMap assetOne = mock( ValueMap.class );
        ValueMap assetTwo = mock( ValueMap.class );

        issueDataList.add( assetOne );
        issueDataList.add( assetTwo );

        when( assetOne.get( ClassMagsMigrationConstants.PP_DESCRIPTION ) ).thenReturn( DESCRIPTION );
        when( assetOne.get( ClassMagsMigrationConstants.FILE_REFERENCE ) ).thenReturn( IMAGE );
        when( assetOne.get( "sortingDate" ) ).thenReturn( ISSUE_SORTING_DATE );

        return issueDataList;
    }

}
