package com.scholastic.classmags.migration.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Constructor;

import javax.servlet.http.Cookie;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * JUnit for RoleUtil.
 */
public class RoleUtilTest {

    private String userRole;
    private String visibleTo;
    private SlingHttpServletRequest request;

    /**
     * Initial Setup Method.
     */
    @Before
    public void setUp() {
        userRole = "teacher";
        visibleTo = "student";
        
        request = mock( SlingHttpServletRequest.class );
    }

    @Test
    public void testTeacherRole() {
        Assert.assertFalse( RoleUtil.shouldRender( userRole, visibleTo ) );
        visibleTo = "teacher";
        Assert.assertTrue( RoleUtil.shouldRender( userRole, visibleTo ) );
        visibleTo = "everyone";
        Assert.assertTrue( RoleUtil.shouldRender( userRole, visibleTo ) );
        visibleTo = "teacherOrStudent";
        Assert.assertTrue( RoleUtil.shouldRender( userRole, visibleTo ) );
    }

    @Test
    public void testStudentRole() {
        userRole="student";
        visibleTo = "teacher";
        Assert.assertFalse( RoleUtil.shouldRender( userRole, visibleTo ) );
        visibleTo = "student";
        Assert.assertTrue( RoleUtil.shouldRender( userRole, visibleTo ) );
        visibleTo = "everyone";
        Assert.assertTrue( RoleUtil.shouldRender( userRole, visibleTo ) );
        visibleTo = "teacherOrStudent";
        Assert.assertTrue( RoleUtil.shouldRender( userRole, visibleTo ) );
    }
    
    @Test
    public void testAnonymousRole() {
        userRole="anonymous";
        visibleTo = "teacher";
        Assert.assertFalse( RoleUtil.shouldRender( userRole, visibleTo ) );
        visibleTo = "student";
        Assert.assertFalse( RoleUtil.shouldRender( userRole, visibleTo ) );
        visibleTo = "everyone";
        Assert.assertTrue( RoleUtil.shouldRender( userRole, visibleTo ) );
        visibleTo = "teacherOrStudent";
        Assert.assertFalse( RoleUtil.shouldRender( userRole, visibleTo ) );
    }
    
    @Test
    public void testShouldRenderWhenAccessibleToIsBlank() {

        // execute logic
        boolean render = RoleUtil.shouldRender( userRole, "" );

        // verify logic
        assertFalse( render );

    }
    
    @Test
    public void testGetUserRole() {
        // setup logic
        Cookie swAuthCookie = mock( Cookie.class );
        when( swAuthCookie.getValue()).thenReturn( "v7RJ39IwvOVQ0VKQzhQtQA2" );
        Cookie sdmNavCookie = mock( Cookie.class );
        when( request.getCookie( CmSDMConstants.SW_AUTH_COOKIE_NAME )).thenReturn( swAuthCookie );
        when( request.getCookie( CmSDMConstants.SDM_NAV_COOKIE_NAME )).thenReturn( sdmNavCookie );
        
        // execute logic
        String userRole = RoleUtil.getUserRole( request );
        
        // verify logic
        assertEquals( ClassMagsMigrationConstants.USER_TYPE_TEACHER, userRole );
    }
    
    @Test
    public void testGetUserRoleWhenDecrpytedValueIsBlank() {
        // setup logic
        Cookie swAuthCookie = mock( Cookie.class );
        when( swAuthCookie.getValue()).thenReturn( StringUtils.EMPTY );
        Cookie sdmNavCookie = mock( Cookie.class );
        when( request.getCookie( CmSDMConstants.SW_AUTH_COOKIE_NAME )).thenReturn( swAuthCookie );
        when( request.getCookie( CmSDMConstants.SDM_NAV_COOKIE_NAME )).thenReturn( sdmNavCookie );
        
        // execute logic
        String userRole = RoleUtil.getUserRole( request );
        
        // verify logic
        assertEquals( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS, userRole );
    }
    
    @Test
    public void testGetUserRoleWhenSwAuthCookieIsNull() {
        // setup logic
        
        when( request.getCookie( CmSDMConstants.SW_AUTH_COOKIE_NAME )).thenReturn( null );
        Cookie sdmNavCookie = mock( Cookie.class );
        when( request.getCookie( CmSDMConstants.SDM_NAV_COOKIE_NAME )).thenReturn( sdmNavCookie );
        
        // execute logic
        String userRole = RoleUtil.getUserRole( request );
        
        // verify logic
        assertEquals( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS, userRole );
    }
    
    @Test
    public void testGetUserRoleWhenSdmNavCookieIsNull() {
        // setup logic
        Cookie swAuthCookie = mock( Cookie.class );
        when( request.getCookie( CmSDMConstants.SW_AUTH_COOKIE_NAME )).thenReturn( swAuthCookie );
        when( request.getCookie( CmSDMConstants.SDM_NAV_COOKIE_NAME )).thenReturn( null );
        
        // execute logic
        String userRole = RoleUtil.getUserRole( request );
        
        // verify logic
        assertEquals( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS, userRole );
    }
    
    @Test
    public void testGetUserRoleWhenRequestIsNull() {
        
        // execute logic
        String userRole = RoleUtil.getUserRole( null );
        
        // verify logic
        assertEquals( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS, userRole );
    }
    
    @Test
    public void testPrivateConstructor() throws Exception {
        Constructor< ? >[] constructor = RoleUtil.class.getDeclaredConstructors();
        
        constructor[ 0 ].setAccessible( true );
        constructor[ 0 ].newInstance( ( Object[] ) null );

        assertEquals( 1, constructor.length );
        
    }
    
}
