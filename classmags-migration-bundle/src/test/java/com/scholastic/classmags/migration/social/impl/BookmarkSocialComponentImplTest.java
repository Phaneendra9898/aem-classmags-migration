package com.scholastic.classmags.migration.social.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.xss.XSSAPI;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.adobe.cq.social.scf.ClientUtilities;
import com.adobe.cq.social.srp.SocialResourceProvider;
import com.adobe.cq.social.srp.config.SocialResourceConfiguration;
import com.adobe.cq.social.ugc.api.SearchResults;
import com.adobe.cq.social.ugc.api.UgcFilter;
import com.adobe.cq.social.ugc.api.UgcSearch;
import com.adobe.cq.social.ugcbase.SocialUtils;
import com.scholastic.classmags.migration.models.BookmarkData;
import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.MyBookmarksDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;
import com.scholastic.classmags.migration.utils.RoleUtil;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;

/**
 * The Class BookmarkSocialComponentImplTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { BookmarkSocialComponentImpl.class, SocialASRPUtils.class, RoleUtil.class } )
public class BookmarkSocialComponentImplTest {

    /** The bookmark service. */
    private BookmarkSocialComponentImpl bookmarkService;

    /** The Constant TEST_USER. */
    private static final String TEST_USER = "testUser";

    /** The Constant TEST_APP_NAME. */
    private static final String TEST_APP_NAME = "scienceworld";

    /** The Constant TEST_BOOKMARK_PAGE_PATH. */
    private static final String TEST_BOOKMARK_PAGE_PATH = "testBookmarkPath";

    /** The Constant TEST_BOOKMARK_TEACHING_RESOURCE_PATH. */
    private static final String TEST_BOOKMARK_TEACHING_RESOURCE_PATH = "testTeachingResourcePath";

    /** The Constant TEST_JSON_DATA. */
    private static final String TEST_JSON_DATA = "{\"bookmarkPath\":\"/content/test_folder/scienceworld/issues/2016-17/090516/invisible-train.html\",\"dateSaved\":1492285292076,\"viewArticleLink\":\"/content/test_folder/scienceworld/issues/2016-17/090516/invisible-train.html\"}";

    /** The Constant TEST_PAGE_DATA. */
    private static final String TEST_PAGE_DATA = "PAGEtestData";

    /** The Constant TEST_ENCODED_DATA. */
    private static final String TEST_ENCODED_DATA = "testEncodedData";

    /** The Constant TEST_ASSET_DATA. */
    private static final String TEST_ASSET_DATA = "ASSETtestData";

    /** The mock sling http servlet request. */
    @Mock
    private SlingHttpServletRequest mockSlingHttpServletRequest;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock resource resolver factory. */
    @Mock
    private ResourceResolverFactory mockResourceResolverFactory;

    /** The mock resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;

    /** The mock social utils. */
    @Mock
    private SocialUtils mockSocialUtils;

    /** The mock srp. */
    @Mock
    private SocialResourceProvider mockSrp;

    /** The mock social resource configuration. */
    @Mock
    private SocialResourceConfiguration mockSocialResourceConfiguration;

    /** The mock xssapi. */
    @Mock
    private XSSAPI mockXssapi;

    /** The mock property map. */
    @Mock
    private ModifiableValueMap mockPropertyMap;

    /** The mock props map. */
    @Mock
    private Map< String, Object > mockPropsMap;

    /** The mock set. */
    @Mock
    private Set< String > mockSet;

    /** The mock iterator. */
    @Mock
    private Iterator< Resource > mockIterator;

    /** The mock key set iterator. */
    @Mock
    private Iterator< String > mockKeySetIterator;

    /** The mock client utils. */
    @Mock
    private ClientUtilities mockClientUtils;

    /** The mock ugc search. */
    @Mock
    private UgcSearch mockUgcSearch;

    /** The mock results. */
    @Mock
    private SearchResults< Resource > mockResults;

    /** The mock my bookmarks data service. */
    @Mock
    private MyBookmarksDataService mockMyBookmarksDataService;

    /** The magazine props service. */
    @Mock
    private MagazineProps magazinePropsService;

    /** The mock results list. */
    @Mock
    private List< Resource > mockResultsList;

    /** The mock bookmark page data list. */
    @Mock
    private List< BookmarkData > mockBookmarkPageDataList;

    /** The mock bookmark asset data list. */
    @Mock
    private List< BookmarkData > mockBookmarkAssetDataList;

    /** The mock resource object list. */
    @Mock
    private List< ResourcesObject > mockResourceObjectList;

    /**
     * Test get my bookmarks page data.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetMyBookmarksPageData() throws Exception {

        when( mockClientUtils.getRequest() ).thenReturn( mockSlingHttpServletRequest );
        PowerMockito.mockStatic( RoleUtil.class );
        PowerMockito.mockStatic( SocialASRPUtils.class );
        when( RoleUtil.getUserRole( mockSlingHttpServletRequest ) ).thenReturn( RoleUtil.ROLE_TYPE_TEACHER );
        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.OPERATION ) )
                .thenReturn( ClassMagsMigrationASRPConstants.SOCIAL_GET_MY_BOOKMARKS_OPERATION );
        when( mockXssapi.filterHTML( ClassMagsMigrationASRPConstants.SOCIAL_GET_MY_BOOKMARKS_OPERATION ) )
                .thenReturn( ClassMagsMigrationASRPConstants.SOCIAL_GET_MY_BOOKMARKS_OPERATION );
        when( mockResource.getResourceResolver() ).thenReturn( mockResourceResolver );
        UgcFilter mockUGCfilter = PowerMockito.mock( UgcFilter.class );
        PowerMockito.whenNew( UgcFilter.class ).withNoArguments().thenReturn( mockUGCfilter );
        when( mockUgcSearch.find( null, mockResourceResolver, mockUGCfilter,
                ClassMagsMigrationASRPConstants.SOCIAL_SEARCH_OFFSET,
                ClassMagsMigrationASRPConstants.SOCIAL_SEARCH_INTIAL_LIMIT, true ) ).thenReturn( mockResults );
        when( mockResults.getResults() ).thenReturn( mockResultsList );
        when( mockResultsList.iterator() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( mockResource );
        when( mockResource.getName() ).thenReturn( TEST_APP_NAME );
        when( magazinePropsService.getMagazineActualName( TEST_APP_NAME ) ).thenReturn( TEST_APP_NAME );
        when( mockResource.adaptTo( ModifiableValueMap.class ) ).thenReturn( mockPropertyMap );
        when( mockPropertyMap.keySet() ).thenReturn( mockSet );
        when( mockSet.iterator() ).thenReturn( mockKeySetIterator );
        when( mockKeySetIterator.hasNext() ).thenReturn( true, false );
        when( mockKeySetIterator.next() ).thenReturn( TEST_PAGE_DATA );
        when( mockPropertyMap.get( TEST_PAGE_DATA ) ).thenReturn( TEST_JSON_DATA );

        bookmarkService = new BookmarkSocialComponentImpl( mockResource, mockClientUtils, mockUgcSearch, mockXssapi,
                mockMyBookmarksDataService, magazinePropsService );

        when( mockMyBookmarksDataService.getMyBookmarksPageData( mockBookmarkPageDataList,
                RoleUtil.ROLE_TYPE_TEACHER ) ).thenReturn( mockResourceObjectList );
        when( mockMyBookmarksDataService.getMyBookmarksAssetData( mockBookmarkAssetDataList,
                RoleUtil.ROLE_TYPE_TEACHER ) ).thenReturn( mockResourceObjectList );

        assertEquals( true, bookmarkService.getMyBookmarksPageData().isEmpty() );
        assertEquals( true, bookmarkService.getMyBookmarksAssetData().isEmpty() );
    }

    /**
     * Test get asset paths.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetAssetPaths() throws Exception {

        when( mockClientUtils.getRequest() ).thenReturn( mockSlingHttpServletRequest );
        PowerMockito.mockStatic( RoleUtil.class );
        PowerMockito.mockStatic( SocialASRPUtils.class );
        when( RoleUtil.getUserRole( mockSlingHttpServletRequest ) ).thenReturn( RoleUtil.ROLE_TYPE_TEACHER );
        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.OPERATION ) )
                .thenReturn( ClassMagsMigrationASRPConstants.SOCIAL_GET_ALL_SITE_BOOKMARK_OPERATION );
        when( mockXssapi.filterHTML( ClassMagsMigrationASRPConstants.SOCIAL_GET_ALL_SITE_BOOKMARK_OPERATION ) )
                .thenReturn( ClassMagsMigrationASRPConstants.SOCIAL_GET_ALL_SITE_BOOKMARK_OPERATION );

        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.BOOKMARK_PAGE_PATH ) )
                .thenReturn( TEST_BOOKMARK_PAGE_PATH );
        when( mockXssapi.filterHTML( TEST_BOOKMARK_PAGE_PATH ) ).thenReturn( TEST_BOOKMARK_PAGE_PATH );

        when( mockResource.adaptTo( ModifiableValueMap.class ) ).thenReturn( mockPropertyMap );
        when( SocialASRPUtils.urlEncoder( TEST_BOOKMARK_PAGE_PATH ) ).thenReturn( TEST_ENCODED_DATA );
        when( SocialASRPUtils.urlEncoder( TEST_BOOKMARK_TEACHING_RESOURCE_PATH ) ).thenReturn( TEST_ENCODED_DATA );
        when( mockPropertyMap.containsKey( "ASSET".concat( TEST_ENCODED_DATA ) ) ).thenReturn( true );

        when( mockResource.adaptTo( ModifiableValueMap.class ) ).thenReturn( mockPropertyMap );
        when( mockPropertyMap.keySet() ).thenReturn( mockSet );
        when( mockSet.iterator() ).thenReturn( mockKeySetIterator );
        when( mockKeySetIterator.hasNext() ).thenReturn( true, true, false );
        when( mockKeySetIterator.next() ).thenReturn( TEST_ASSET_DATA );
        when( mockPropertyMap.get( TEST_ASSET_DATA ) ).thenReturn( TEST_JSON_DATA );

        bookmarkService = new BookmarkSocialComponentImpl( mockResource, mockClientUtils, mockUgcSearch, mockXssapi,
                mockMyBookmarksDataService, magazinePropsService );

        assertEquals( false, bookmarkService.getAssetPaths().isEmpty() );
    }
}
