package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for FAQQuestionSetUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { FAQQuestionSetUse.class, CommonUtils.class } )
public class FAQQuestionSetUseTest extends BaseComponentTest {

    private static final String TEST_QUESTION_ID = "SS-1";
    private static final String TEST_QUESTION_VALUE = "How to Subscribe";

    private FAQQuestionSetUse faqQuestionSetUse;
    private List< ValueMap > questionList;
    private ValueMap question;
    private Resource questionListResource;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        faqQuestionSetUse = Whitebox.newInstance( FAQQuestionSetUse.class );

        stubCommon( faqQuestionSetUse );

        PowerMockito.mockStatic( CommonUtils.class );
        
        questionList = new ArrayList< >();
        question = mock( ValueMap.class );
        questionListResource = mock( Resource.class );

        when( question.get( ClassMagsMigrationConstants.QUESTION_ID, StringUtils.EMPTY ) )
                .thenReturn( TEST_QUESTION_ID );
        when( question.get( ClassMagsMigrationConstants.QUESTION, StringUtils.EMPTY ) )
                .thenReturn( TEST_QUESTION_VALUE );
        questionList.add( question );
        when( resource.getChild( ClassMagsMigrationConstants.QUESTION_LIST ) ).thenReturn( questionListResource );
        when( CommonUtils.fetchMultiFieldData( questionListResource ) ).thenReturn( questionList );
    }

    @Test
    public void testFAQQuestionSetUse() throws Exception {
        // execute logic
        Whitebox.invokeMethod( faqQuestionSetUse, "activate" );

        // verify logic
        assertEquals( 1, faqQuestionSetUse.getQuestionList().size() );
        assertEquals( TEST_QUESTION_ID, faqQuestionSetUse.getQuestionList().get( 0 )
                .get( ClassMagsMigrationConstants.QUESTION_ID, StringUtils.EMPTY ) );
        assertEquals( TEST_QUESTION_VALUE, faqQuestionSetUse.getQuestionList().get( 0 )
                .get( ClassMagsMigrationConstants.QUESTION, StringUtils.EMPTY ) );

    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final FAQQuestionSetUse faqQuestionSetUse = new FAQQuestionSetUse();

        // verify logic
        assertNotNull( faqQuestionSetUse );

    }

}
