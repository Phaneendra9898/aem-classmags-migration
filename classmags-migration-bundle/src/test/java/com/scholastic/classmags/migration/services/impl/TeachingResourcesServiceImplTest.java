package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.tagging.TagManager;
import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * JUnit for TeachingResourcesService.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { TeachingResourcesServiceImpl.class, CommonUtils.class, RoleUtil.class } )
public class TeachingResourcesServiceImplTest {

    private static final String VIDEO_RESOURCE = "video";
    private static final String BLOG_RESOURCE = "blog";
    private static final String RESOURCE_TYPE = "resourceType";
    private static final String RESOURCE_PATH = "resourcePath";
    private static final String VIDEO_ID_KEY = "videoId";
    
    // Video resource
    private static final String VIDEO_TITLE = "America's 11 Million";
    private static final String VIDEO_DESCRIPTION = "Scholastic Polls";
    private static final String VIDEO_IMAGE_PATH = "/content/dam/scholastic/videos/UPF-040416-Americas11Million.mp4/jcr:content/renditions/cq5dam.thumbnail.140.100.png";
    private static final long VIDEO_ID = new Long( 123456789 );
    private static final List< String > VIDEO_TAGS = new ArrayList< >( Arrays.asList( "elections", "polls" ) );
    private static final String VIDEO_PAGE_PATH = "/content/dam/scholastic/videos/UPF-040416-Americas11Million.mp4";
    private static final String VIDEO_USER_TYPE = "teacher";
    
    // Blog resource
    private static final String BLOG_TITLE = "Test Blog 1";
    private static final String BLOG_DESCRIPTION = "Test Blog Description";
    private static final String BLOG_IMAGE_PATH = "/content/dam/scholastic/classroom-magazines/teacher/teacher1.jpg";
    private static final Long BLOG_VIDEO_ID = null;
    private static final List< String > BLOG_TAGS = null;
    private static final String BLOG_PAGE_PATH = "/content/classroom-magazines/test-page";
    private static final String BLOG_USER_TYPE = "teacher";

    private Map< String, List< ResourcesObject > > issueResourcesMap = new HashMap< String, List< ResourcesObject > >();
    private Map< String, List< ResourcesObject > > customResourcesMap = new HashMap< String, List< ResourcesObject > >();

    private ResourcesObject video,video1, customVideo;
    private List< ResourcesObject > videoList, customVideoList;
    private List< ResourcesObject > blogList;

    /** The teaching resources service. */
    private TeachingResourcesServiceImpl teachingResourcesService;

    /** The sling http servlet request. */
    SlingHttpServletRequest request;

    /** The resource resolver. */
    ResourceResolver resourceResolver;

    /** The resource. */
    Resource resource;
    
    /** The resource. */
    Resource issueResource1, issueResource2, issueResource3;
    
    /** The child resources */
    Resource videoResource,videoOneResource,blogResource,videoJcrNode, videoOneJcrNode, blogJcrNode,videoMetadataNode, videoOneMetadataNode;
    
    /** The resource. */
    Resource customResource;
    
    /** The article config resource. */
    Resource articleConfigResource;
    
    /** The iterable resource. */
    Iterable< Resource > iterableResource;
    
    /** The resource iterator. */
    Iterator< Resource > resourceIterator;

    /** The properties. */
    ValueMap issueProperties1, issueProperties2, issueProperties3, videoProperties, videoOneProperties, blogProperties, videoMetadataProperties,videoOneMetadataProperties;
    
    /** The resource resolver factory. */
    ResourceResolverFactory resourceResolverFactory;
    
    TagManager tagManager;
    
    Asset videoAsset, videoOneAsset;

    /**
     * Initial Setup.
     */
    @SuppressWarnings( "unchecked" )
    @Before
    public void setUp() {
        teachingResourcesService = new TeachingResourcesServiceImpl();
        resourceResolverFactory = mock(ResourceResolverFactory.class);
        
        Whitebox.setInternalState( teachingResourcesService, resourceResolverFactory);
        
        request = mock( SlingHttpServletRequest.class );
        resourceResolver = mock( ResourceResolver.class );
        
        resource = mock( Resource.class );
        issueResource1 = mock(Resource.class);
        issueResource2 = mock(Resource.class);
        issueResource3 = mock(Resource.class);
        videoResource = mock(Resource.class);
        videoOneResource = mock(Resource.class);
        blogResource = mock(Resource.class);
        videoJcrNode = mock(Resource.class);
        videoOneJcrNode = mock(Resource.class);
        blogJcrNode = mock(Resource.class);
        videoMetadataNode = mock(Resource.class);
        videoOneMetadataNode = mock(Resource.class);
        customResource = mock ( Resource.class);
        articleConfigResource = mock(Resource.class);
        issueProperties1 = mock( ValueMap.class );
        issueProperties2 = mock(ValueMap.class);
        issueProperties3 = mock(ValueMap.class);
        videoProperties = mock(ValueMap.class);
        videoOneProperties = mock(ValueMap.class);
        blogProperties = mock(ValueMap.class);
        videoMetadataProperties = mock(ValueMap.class);
        videoOneMetadataProperties = mock(ValueMap.class);
        iterableResource = mock(Iterable.class);
        resourceIterator = mock(Iterator.class);
        tagManager = mock(TagManager.class);
        videoAsset = mock(Asset.class);
        videoOneAsset = mock(Asset.class);
        
        blogList = new ArrayList< >();
        videoList = new ArrayList< >();
        customVideoList = new ArrayList< >();
        
        video = createResourcesObject( VIDEO_TITLE, VIDEO_DESCRIPTION, VIDEO_IMAGE_PATH, VIDEO_ID, VIDEO_TAGS,
                VIDEO_PAGE_PATH, VIDEO_USER_TYPE );
        video1 = createResourcesObject( VIDEO_TITLE, VIDEO_DESCRIPTION, VIDEO_IMAGE_PATH, VIDEO_ID, VIDEO_TAGS,
                VIDEO_PAGE_PATH, VIDEO_USER_TYPE );
        customVideo = createResourcesObject( VIDEO_TITLE, VIDEO_DESCRIPTION, VIDEO_IMAGE_PATH, VIDEO_ID, null,
                VIDEO_PAGE_PATH, VIDEO_USER_TYPE );
        blogList.add( createResourcesObject( BLOG_TITLE, BLOG_DESCRIPTION, BLOG_IMAGE_PATH, BLOG_VIDEO_ID, BLOG_TAGS,
                BLOG_PAGE_PATH, BLOG_USER_TYPE ) );

        videoList.add( video );
        videoList.add( video1 );
        customVideoList.add( customVideo );
        issueResourcesMap.put( VIDEO_RESOURCE, videoList );
        customResourcesMap.put( BLOG_RESOURCE, blogList );
        customResourcesMap.put( VIDEO_RESOURCE, customVideoList );

        PowerMockito.mockStatic( CommonUtils.class );
        PowerMockito.mockStatic( RoleUtil.class);
        
        when(articleConfigResource.getChild( "resources" )).thenReturn(resource);
        when(resource.hasChildren()).thenReturn( true);
        when(resource.getChildren()).thenReturn( iterableResource );
        when(iterableResource.iterator()).thenReturn( resourceIterator );
        when(resourceIterator.hasNext()).thenReturn( true, true, true, false );
        when(resourceIterator.next()).thenReturn( issueResource1, issueResource2, issueResource3 );
        when(issueResource1.getValueMap()).thenReturn( issueProperties1 );
        when(issueResource2.getValueMap()).thenReturn( issueProperties2 );
        when(issueResource3.getValueMap()).thenReturn( issueProperties3 );
        when(issueProperties1.get( RESOURCE_TYPE, StringUtils.EMPTY )).thenReturn( "video-/content/dam/video" );
        when(issueProperties2.get( RESOURCE_TYPE, StringUtils.EMPTY )).thenReturn( "blog-/content/scholastic/testblog" );
        when(issueProperties3.get( RESOURCE_TYPE, StringUtils.EMPTY )).thenReturn( "video-/content/dam/videoOne" );
        when(issueProperties1.get( RESOURCE_PATH, StringUtils.EMPTY )).thenReturn( "/content/dam/video" );
        when(issueProperties2.get( RESOURCE_PATH, StringUtils.EMPTY )).thenReturn( "/content/scholastic/testblog" );
        when(issueProperties3.get( RESOURCE_PATH, StringUtils.EMPTY )).thenReturn( "/content/dam/videoOne" );
        when(CommonUtils.getResource( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE, "/content/dam/video" )).thenReturn( videoResource );
        when(CommonUtils.getResource( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE, "/content/scholastic/testblog" )).thenReturn(blogResource);
        when(CommonUtils.getResource( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE, "/content/dam/videoOne" )).thenReturn(videoOneResource);
        when(blogResource.getChild( JcrConstants.JCR_CONTENT )).thenReturn( blogJcrNode );
        when(videoJcrNode.getValueMap()).thenReturn( videoProperties );
        when(blogJcrNode.getValueMap()).thenReturn( blogProperties );
        when(videoOneJcrNode.getValueMap()).thenReturn( videoOneProperties );
        when(blogProperties.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY )).thenReturn( "Blog title" );
        when(blogProperties.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY )).thenReturn("Blog description");
        when(blogProperties.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY )).thenReturn( "/content/dam/blog-image" );
        when(CommonUtils.getTagManager( resourceResolverFactory )).thenReturn( tagManager );
        when(CommonUtils.getTags(blogJcrNode, tagManager)).thenReturn( BLOG_TAGS );
        when(videoResource.adaptTo( Asset.class )).thenReturn(videoAsset);
        when(videoResource.getChild( ClassMagsMigrationConstants.ASSET_METADATA_NODE)).thenReturn(videoMetadataNode);
        when(videoAsset.getPath()).thenReturn( "/content/dam/video" );
        when(videoAsset.getMetadataValue(DamConstants.DC_TITLE)).thenReturn( "The path of jaguar" );
        when(videoAsset.getName()).thenReturn( "The path of Jaguar" );
        when(videoAsset.getMetadataValue( DamConstants.DC_DESCRIPTION )).thenReturn( "This is video description" );
        when(videoMetadataNode.getValueMap()).thenReturn( videoMetadataProperties );
        when(videoMetadataProperties.containsKey(VIDEO_ID_KEY)).thenReturn( true );
        when(videoMetadataProperties.get(VIDEO_ID_KEY, Long.class)).thenReturn( new Long(123456789) );
        when(CommonUtils.getTags( videoMetadataNode, tagManager )).thenReturn( VIDEO_TAGS );
        
        when(videoOneResource.adaptTo( Asset.class )).thenReturn(videoOneAsset);
        when(videoOneResource.getChild( ClassMagsMigrationConstants.ASSET_METADATA_NODE)).thenReturn(videoOneMetadataNode);
        when(videoOneAsset.getPath()).thenReturn( "/content/dam/videoOne" );
        when(videoOneAsset.getMetadataValue(DamConstants.DC_TITLE)).thenReturn( "Treasures from the sky" );
        when(videoOneAsset.getName()).thenReturn( "Treasures from the sky" );
        when(videoOneAsset.getMetadataValue( DamConstants.DC_DESCRIPTION )).thenReturn( "This is video description" );
        when(videoOneMetadataNode.getValueMap()).thenReturn( videoOneMetadataProperties );
        when(videoOneMetadataProperties.containsKey(VIDEO_ID_KEY)).thenReturn( true );
        when(videoOneMetadataProperties.get(VIDEO_ID_KEY, Long.class)).thenReturn( new Long(123456789) );
        when(CommonUtils.getTags( videoOneMetadataNode, tagManager )).thenReturn( VIDEO_TAGS );
        when(CommonUtils.getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE)).thenReturn( resourceResolver );
        when( RoleUtil.getUserRole( request ) ).thenReturn( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS );
        when( RoleUtil.shouldRender( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS, VIDEO_USER_TYPE ) ).thenReturn( true );
    }

    /**
     * Test get teaching resources.
     * 
     */
    @Test
    public void testGetTeachingResources(){

        // execute logic
        Map< String, List< ResourcesObject > > teachingResources = teachingResourcesService
                .getTeachingResources( request, articleConfigResource, "resources" );

        // verify logic
        assertNotNull( teachingResources );
        assertEquals(1, teachingResources.get( "blog" ).size());
        assertEquals(2, teachingResources.get("video").size());
    }
    
    /**
     * 
     * Test get teaching resources when Article resource is null.
     * 
     */
    @Test
    public void testGetTeachingResourcesWhenArticleResourceIsNull(){
        // setup logic ( test-specific )
        when(articleConfigResource.getChild( "resources" )).thenReturn( null );
        
        // execute logic
        Map< String, List< ResourcesObject > > teachingResources = teachingResourcesService
                .getTeachingResources( request, articleConfigResource, "resources" );

        // verify logic
        assertNull( teachingResources );
    }
    
    /**
     * 
     * Test get teaching resources when child nodes not present.
     * 
     */
    @Test
    public void testGetTeachingResourcesWhenChildNodesNotPresent(){
        // setup logic ( test-specific )
        when(resource.hasChildren()).thenReturn( false );
        
        // execute logic
        Map< String, List< ResourcesObject > > teachingResources = teachingResourcesService
                .getTeachingResources( request, articleConfigResource, "resources" );

        // verify logic
        assertNull( teachingResources );
    }
    
    /**
     * Test create resources map.
     * 
     */
    @Test
    public void testCreateResourcesMap() {
       
        // execute logic
        Map< String, List< ResourcesObject > > teachingResources = teachingResourcesService
                .createResourcesMap( issueResourcesMap, customResourcesMap );

        // verify logic
        assertNotNull( teachingResources );
        assertEquals( 3, teachingResources.get( VIDEO_RESOURCE ).size() );
        assertEquals( VIDEO_TITLE, teachingResources.get( VIDEO_RESOURCE ).get( 0 ).getTitle() );
        assertEquals( VIDEO_DESCRIPTION, teachingResources.get( VIDEO_RESOURCE ).get( 0 ).getDescription() );
    }
    
    /**
     * Test get teaching resources when no issue resources present.
     * 
     */
    @Test
    public void testCreateResourcesMapWhenIssueResourcesIsEmpty() {
        // setup logic (test-specific)
        issueResourcesMap = new HashMap< String, List< ResourcesObject > >();

        // execute logic
        Map< String, List< ResourcesObject > > teachingResources = teachingResourcesService
                .createResourcesMap( issueResourcesMap, customResourcesMap );

        // verify logic
        assertNotNull( teachingResources );
        assertEquals( customResourcesMap, teachingResources );
        assertEquals( customVideo, teachingResources.get( VIDEO_RESOURCE ).get( 0 ) );

    }

    /**
     * Test get teaching resources when asset resource is null.
     * 
     */
    @Test
    public void testGetTeachingResourcesWhenAssetResourceIsNull() {
        // setup logic (test-specific)
        when(CommonUtils.getResource( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE, "/content/dam/video" )).thenReturn( null );
        when(CommonUtils.getResource( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE, "/content/dam/videoOne" )).thenReturn( null );
        when(videoResource.getChild( ClassMagsMigrationConstants.ASSET_METADATA_NODE)).thenReturn(null);
        
        // execute logic
        Map< String, List< ResourcesObject > > teachingResources = teachingResourcesService
                .getTeachingResources( request, articleConfigResource, "resources" );

        // verify logic
        assertFalse( teachingResources.containsKey( VIDEO_RESOURCE ) );

    }
    
    /**
     * Test get teaching resources when no video id is present.
     * 
     */
    @Test
    public void testGetTeachingResourcesWhenAssetMetadataNodeHasNoVideoId() {
        // setup logic (test-specific)
        when(videoMetadataProperties.containsKey(VIDEO_ID_KEY)).thenReturn( false );
        
        // execute logic
        Map< String, List< ResourcesObject > > teachingResources = teachingResourcesService
                .getTeachingResources( request, articleConfigResource, "resources" );

        // verify logic
        assertEquals( 0 ,teachingResources.get( VIDEO_RESOURCE ).get( 0 ).getVideoId() );

    }

    /**
     * Test get teaching resources when asset metadata node is null.
     * 
     */
    @Test
    public void testTeachingResourcesWhenAssetMetadataNodeIsNull() {
        // setup logic (test-specific)
        when(videoResource.getChild( ClassMagsMigrationConstants.ASSET_METADATA_NODE)).thenReturn(null);
        // execute logic
        Map< String, List< ResourcesObject > > teachingResources = teachingResourcesService
                .getTeachingResources( request, articleConfigResource, "resources" );

        // verify logic
        assertEquals( 0 ,teachingResources.get( VIDEO_RESOURCE ).get( 0 ).getVideoId() );

    }

    /**
     * Test get teaching resources when resource path not present in comboResourceType.
     * 
     */
    @Test
    public void testTeachingResourcesWhenResourcePathNotPresentInComboResourceType() {
        // setup logic (test-specific)
        when(issueProperties1.get( RESOURCE_TYPE, StringUtils.EMPTY )).thenReturn( VIDEO_RESOURCE );
        when(issueProperties2.get( RESOURCE_TYPE, StringUtils.EMPTY )).thenReturn( BLOG_RESOURCE );
        // execute logic
        Map< String, List< ResourcesObject > > teachingResources = teachingResourcesService
                .getTeachingResources( request, articleConfigResource, "resources" );

        // verify logic
        assertEquals( 2 ,teachingResources.get( VIDEO_RESOURCE ).size() );
        assertEquals( 1, teachingResources.get( BLOG_RESOURCE ).size() );

    }

    /**
     * Test get teaching resources when page resource not present.
     * 
     */
    @Test
    public void testTeachingResourcesWhenPageResourceNotPresent() {
        // setup logic (test-specific)
        when(CommonUtils.getResource( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE, "/content/scholastic/testblog" )).thenReturn(null);
        // execute logic
        Map< String, List< ResourcesObject > > teachingResources = teachingResourcesService
                .getTeachingResources( request, articleConfigResource, "resources" );

        // verify logic
        assertFalse(teachingResources.containsKey( BLOG_RESOURCE ));
    }

    /**
     * Test get teaching resources when jcr content not present.
     * 
     */
    @Test
    public void testTeachingResourcesWhenJcrContentNotPresent() {
        // setup logic (test-specific)
        when(blogResource.getChild( JcrConstants.JCR_CONTENT )).thenReturn( null );
        // execute logic
        Map< String, List< ResourcesObject > > teachingResources = teachingResourcesService
                .getTeachingResources( request, articleConfigResource, "resources" );

        // verify logic
        assertFalse(teachingResources.containsKey( BLOG_RESOURCE ));
    }


    /**
     * Test create resources map when no issue resources present.
     * 
     */
    @Test
    public void testCreateResourcesMapWhenIssueResourcesIsNull() {
        
        // execute logic
        Map< String, List< ResourcesObject > > teachingResources = teachingResourcesService
                .createResourcesMap( null, customResourcesMap );

        // verify logic
        assertNotNull( teachingResources );
        assertEquals( customResourcesMap, teachingResources );
        assertEquals( customVideo, teachingResources.get( VIDEO_RESOURCE ).get( 0 ) );

    }
    
    /**
     * Test get teaching resources when type is not asset or page.
     * 
     */
    @Test
    public void testGetTeachingResourcesWhenTypeIsNotAssetOrPage() {
        // setup logic (test-specific)
        when( issueProperties1.get( RESOURCE_TYPE, StringUtils.EMPTY ) ).thenReturn( "newType-/content/dam/video" );
        when( issueProperties2.get( RESOURCE_TYPE, StringUtils.EMPTY ) )
                .thenReturn( "newType-/content/scholastic/testblog" );
        when( issueProperties3.get( RESOURCE_TYPE, StringUtils.EMPTY ) ).thenReturn( "newType-/content/dam/videoOne" );

        // execute logic
        Map< String, List< ResourcesObject > > teachingResources = teachingResourcesService
                .getTeachingResources(request, articleConfigResource, "resources" );

        // verify logic
        assertTrue( teachingResources.isEmpty() );

    }
    
    /**
     * Creates the resource object.
     * 
     * @param title The resource title
     * @param description The resource description.
     * @param imagePath The image path.
     * @param videoId The video id.
     * @param tags The video tags
     * @param pagePath The page path.
     * @return resourcesObject The resource object.
     */
    private ResourcesObject createResourcesObject( String title, String description, String imagePath, Long videoId,
            List< String > tags, String pagePath, String userType ) {
        ResourcesObject resourcesObject = new ResourcesObject();
        resourcesObject.setTitle( title );
        resourcesObject.setDescription( description );
        resourcesObject.setImagePath( imagePath );

        if ( null != videoId ) {
            resourcesObject.setVideoId( videoId );
        }
        resourcesObject.setTags( tags );
        resourcesObject.setPagePath( pagePath );
        resourcesObject.setUserRole( userType );
        return resourcesObject;

    }
    
}
