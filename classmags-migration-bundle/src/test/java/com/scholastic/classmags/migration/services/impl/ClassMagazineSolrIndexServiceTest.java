package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.scholastic.classmags.migration.utils.SolrSearchClassMagsConstants;

public class ClassMagazineSolrIndexServiceTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();
    
    SolrClassMagsConfigService service = new SolrClassMagsConfigService();
    ClassMagsSolrIndexService indexingService = new ClassMagsSolrIndexService();

    Map< String, Object > config = new HashMap< String, Object >();

    @Before
    public void setup()
            throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        config.put( SolrSearchClassMagsConstants.PROTOCOL, SolrSearchClassMagsConstants.DEFAULT_PROTOCOL );
        config.put( SolrSearchClassMagsConstants.SERVER_NAME, SolrSearchClassMagsConstants.DEFAULT_SERVER_NAME );
        config.put( SolrSearchClassMagsConstants.SERVER_PORT, SolrSearchClassMagsConstants.DEFAULT_SERVER_PORT );
        config.put( SolrSearchClassMagsConstants.CONTEXT_PATH, SolrSearchClassMagsConstants.DEFAULT_CONTEXT_PATH );
        config.put( SolrSearchClassMagsConstants.PROXY_URL, SolrSearchClassMagsConstants.DEFAULT_PROXY_URL );
        config.put( SolrSearchClassMagsConstants.PROXY_ENABLED, SolrSearchClassMagsConstants.DEFAULT_PROXY_ENABLED );
        config.put( SolrSearchClassMagsConstants.AEM_SOLR_CORE, SolrSearchClassMagsConstants.DEFAULT_AEM_SOLR_CORE );
        service.activate( config );
        Field field = ClassMagsSolrIndexService.class.getDeclaredField( "solrConfigService" );
        field.setAccessible( true );
        field.set( indexingService, service );
    }

    @Test( expected = NullPointerException.class )
    public void testAddNull() {
        indexingService.addBean( null );
    }
    
    @Test( expected = NullPointerException.class )
    public void testAddBeansAndCommitNull() {
        indexingService.addBeansAndCommit( null );
    }
    
    @Test
    public void testGetCoreName() {
        assertNotNull( indexingService.getCoreName());
    }
    
    @Test
    public void testGetSolrIndexClient() {
        assertNotNull( indexingService.getSolrIndexClient());
    }
    
    @Test
    public void testGetSolrQueryClient() {
        assertNotNull( indexingService.getSolrQueryClient());
    }
}