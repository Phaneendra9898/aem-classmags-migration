package com.scholastic.classmags.migration.controllers;

import static org.mockito.Mockito.when;

import javax.jcr.RepositoryException;
import javax.script.Bindings;

import org.apache.commons.lang.ArrayUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith( MockitoJUnitRunner.class )
public class TabsUseTest {

    /** The Constant Tab Titles. */
    private static final String[] TABS = {"x","y","z"};

    /** The Constant TEST_KEY. */
    private static final String TEST_KEY = "x";

    /** The tabs use. */
    private TabsUse tabsUse;

    /** The bindings. */
    @Mock
    private Bindings bindings;

    /** The current resource. */
    @Mock
    private Resource currentResource;
    
    /** The valueMap when adapted from resource. */
    @Mock
    private ValueMap valueMap;


    /**
     * Setup.
     *
     * @throws RepositoryException
     *             the repository exception
     * @throws LoginException
     *             the login exception
     */
    @Before
    public void setup() throws RepositoryException, LoginException {
        tabsUse = new TabsUse();
    }

    /**
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetTabsMap() throws Exception {
        when( bindings.get( "resource" ) ).thenReturn( currentResource );
        when(currentResource.adaptTo( ValueMap.class )).thenReturn( valueMap );
        when(valueMap.get( "tabs", new String[]{} )).thenReturn( TABS );
        tabsUse.init( bindings );
        tabsUse.activate();
        assert( !tabsUse.getTabs().isEmpty() );
        assert( tabsUse.getTabs().get( TEST_KEY )!=null );
        assert( tabsUse.getTabs().size() == 3 );
    }
    
    /**
    *
    * @throws Exception
    *             the exception
    */
   @Test
   public void testUnconfiguredGetTabsMap() throws Exception {
       when( bindings.get( "resource" ) ).thenReturn( currentResource );
       when(currentResource.adaptTo( ValueMap.class )).thenReturn( valueMap );
       when(valueMap.get( "tabs", new String[]{} )).thenReturn( ArrayUtils.EMPTY_STRING_ARRAY );
       tabsUse.init( bindings );
       tabsUse.activate();
       assert( tabsUse.getTabs().isEmpty() );
       assert( tabsUse.getTabs().get( TEST_KEY )==null );
       assert( tabsUse.getTabs().size() == 0 );
   }
   
   /**
   *
   * @throws Exception
   *             the exception
   */
  @Test
  public void testNullCurrentResourceGetTabsMap() throws Exception {
      when( bindings.get( "resource" ) ).thenReturn( null );
      tabsUse.init( bindings );
      tabsUse.activate();
      assert( tabsUse.getTabs().isEmpty() );
      assert( tabsUse.getTabs().get( TEST_KEY )==null );
      assert( tabsUse.getTabs().size() == 0 );
  }

}
