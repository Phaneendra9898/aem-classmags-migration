package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.never;
import static org.powermock.api.mockito.PowerMockito.whenNew;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.scholastic.classmags.migration.services.LexileLevelsDataService;
import com.scholastic.classmags.migration.utils.DropdownUtils;

/**
 * JUnit for LexileLevelsDropdownUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { LexileLevelsDropdownUse.class, DropdownUtils.class } )
public class LexileLevelsDropdownUseTest extends BaseComponentTest {

    private LexileLevelsDropdownUse lexileLevelsDropdownUse;

    private Map< String, String > lexileLevelsMap;
    private LexileLevelsDataService lexileLevelsDataService;
    private SimpleDataSource dataSource;

    /**
     * Setup.
     * 
     * @throws Exception
     */
    @Before
    public void setup() throws Exception {
        lexileLevelsDropdownUse = Whitebox.newInstance( LexileLevelsDropdownUse.class );

        lexileLevelsDataService = mock( LexileLevelsDataService.class );
        dataSource = mock( SimpleDataSource.class );

        lexileLevelsMap = new HashMap< >();
        lexileLevelsMap.put( "1120L", "Reading Level-1120L" );
        lexileLevelsMap.put( "640M", "Reading Level-640M" );

        stubCommon( lexileLevelsDropdownUse );

        PowerMockito.mockStatic( DropdownUtils.class );

        when( DropdownUtils.getContentPage( request ) ).thenReturn( currentPage );
        when( slingScriptHelper.getService( LexileLevelsDataService.class ) ).thenReturn( lexileLevelsDataService );
        when( lexileLevelsDataService.fetchLexileLevels( request, currentPage ) ).thenReturn( lexileLevelsMap );
        whenNew( SimpleDataSource.class ).withAnyArguments().thenReturn( dataSource );
    }

    @Test
    public void testFAQQuestionSetUse() throws Exception {
        // execute logic
        Whitebox.invokeMethod( lexileLevelsDropdownUse, "activate" );

        // verify logic
        verify( request ).setAttribute( DataSource.class.getName(), dataSource );
    }

    @Test
    public void testFAQQuestionSetUseWhenSlingScriptHelperIsNull() throws Exception {
        // setup logic ( test- specific )
        stub( PowerMockito.method( LexileLevelsDropdownUse.class, "getSlingScriptHelper" ) ).toReturn( null );

        // execute logic
        Whitebox.invokeMethod( lexileLevelsDropdownUse, "activate" );

        // verify logic
        verify( request, never() ).setAttribute( DataSource.class.getName(), dataSource );
    }

    @Test
    public void testFAQQuestionSetUseWhenCurrentPageIsNull() throws Exception {
        // setup logic ( test- specific )
        when( DropdownUtils.getContentPage( request ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( lexileLevelsDropdownUse, "activate" );

        // verify logic
        verify( request, never() ).setAttribute( DataSource.class.getName(), dataSource );
    }

    @Test
    public void testFAQQuestionSetUseWhenLexileLevelsDataServiceIsNull() throws Exception {
        // setup logic ( test- specific )
        when( slingScriptHelper.getService( LexileLevelsDataService.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( lexileLevelsDropdownUse, "activate" );

        // verify logic
        verify( request, never() ).setAttribute( DataSource.class.getName(), dataSource );
    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final LexileLevelsDropdownUse lexileLevelsDropdownUse = new LexileLevelsDropdownUse();

        // verify logic
        assertNotNull( lexileLevelsDropdownUse );

    }

}
