package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.day.cq.commons.Externalizer;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.tagging.TagManager;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for PastIssuesService.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { PastIssuesServiceImpl.class, CommonUtils.class, PredicateGroup.class } )
public class PastIssuesServiceImplTest {

    private static final String ISSUE_ONE_PATH = "/content/classroom_magazines/scienceworld/issues/_2015_16/090715";
    private static final String ISSUE_TWO_PATH = "/content/classroom_magazines/scienceworld/issues/_2015_16/092115";
    private static final String CURRENT_PAGE_PATH = "/content/classroom_magazines/scienceworld/content/issue-archive";
    private static final String MAGAZINE_NAME = "scienceworld";
    private static final String SCIENCE_WORLD_DATA_CONFIG_PAGE = "/content/classroom-magazines-admin/scienceworld-data-config-page";
    private static final String ISSUE_ONE_IMAGE = "/content/dam/scholastic/classroom-magazines/migration/images/robot-rev.jpg";
    private static final String ISSUE_TWO_IMAGE = "/content/dam/scholastic/classroom-magazines/migration/images/going-batty.jpg";
    private static final String FEATURED_RESOURCE_ONE_PATH = "/content/dam/scholastic/classroom-magazines/migration/videos/upf-110215-thepathofthejaguarmp4";
    private static final String FEATURED_RESOURCE_TWO_PATH = "/content/dam/scholastic/classroom-magazines/migration/Skill-Sheets/SW-090516-PupsPatrol-HelpWanted.pdf";
    private static final String FEATURED_RESOURCE_THREE_PATH = "/content/classroom_magazines/scienceworld/issues/LATEST-ISSUE-PAGE/MAIN-ARTILCE-1";
    private static final String VIDEO_TITLE = "The path of jaguars";
    private static final String SKILLS_SHEET_TITLE = "Pups on Patrol";
    private static final List< String > TAGS = new ArrayList< >( Arrays.asList( "elections", "polls" ) );

    /** The past issues service. */
    private PastIssuesServiceImpl pastIssuesService;

    /** The resource resolver factory. */
    private ResourceResolverFactory resourceResolverFactory;

    /** The query builder. */
    private QueryBuilder queryBuilder;

    /** The property config service. */
    private PropertyConfigService propertyConfigService;

    /** The resource path config service. */
    private ResourcePathConfigService resourcePathConfigService;

    /** The Magazine Properties service. */
    private MagazineProps magazineProps;

    /** The externalizer service. */
    private Externalizer externalizer;

    /** The sling http servlet request. */
    SlingHttpServletRequest request;

    /** The resource resolver. */
    ResourceResolver resourceResolver;

    /** The session. */
    Session session;

    /** The predicate group. */
    PredicateGroup predicateGroup;

    /** The query. */
    Query query;

    /** The search result. */
    SearchResult searchResult;

    /** The list of hits. */
    List< Hit > hits;

    /** The search results hit. */
    Hit hitOne, hitTwo;

    /** The resource. */
    Resource issueResource1, issueResource2, issueResource3;

    /** The child resources */
    Resource issueJcrResource1, issueJcrResource2, featuredContentResource1, featuredContentResource2;

    /** The featured content list. */
    List< ValueMap > featuredContentList1, featuredContentList2;

    /** The featured content valuemap. */
    ValueMap featuredContentValueMap1, featuredContentValueMap2, featuredContentValueMap3;

    /** The properties. */
    ValueMap issueProperties1, issueProperties2;

    /** The featured content resources. */
    Resource videoResource, skillsSheetResource, articlePageResource;

    /** The jcr node resource for featured content. */
    Resource videoMetadataResource, skillsSheetMetadataResource, articleJcrNodeResource;

    /** The assets. */
    Asset videoAsset, skillsSheetAsset;

    /** The featured resources valuemap. */
    ValueMap videoAssetValueMap, skillsSheetAssetValueMap, articlePageValueMap;

    /** The tag manager. */
    TagManager tagManager;

    /** The logger. */
    Logger logger;

    /**
     * Initial Setup.
     * 
     * @throws RepositoryException
     * @throws LoginException
     * @throws ClassmagsMigrationBaseException 
     */
    @Before
    public void setUp() throws RepositoryException, LoginException, ClassmagsMigrationBaseException {
        pastIssuesService = new PastIssuesServiceImpl();
        resourceResolverFactory = mock( ResourceResolverFactory.class );
        queryBuilder = mock( QueryBuilder.class );
        propertyConfigService = mock( PropertyConfigService.class );
        resourcePathConfigService = mock( ResourcePathConfigService.class );
        magazineProps = mock( MagazineProps.class );
        externalizer = mock( Externalizer.class );
        resourceResolver = mock( ResourceResolver.class );
        tagManager = mock( TagManager.class );
        logger = mock( Logger.class );

        issueResource1 = mock( Resource.class );
        issueResource2 = mock( Resource.class );
        issueProperties1 = mock( ValueMap.class );
        issueProperties2 = mock( ValueMap.class );
        issueJcrResource1 = mock( Resource.class );
        issueJcrResource2 = mock( Resource.class );

        videoResource = mock( Resource.class );
        skillsSheetResource = mock( Resource.class );
        articlePageResource = mock( Resource.class );
        videoMetadataResource = mock( Resource.class );
        skillsSheetMetadataResource = mock( Resource.class );
        articleJcrNodeResource = mock( Resource.class );

        videoAsset = mock( Asset.class );
        skillsSheetAsset = mock( Asset.class );

        featuredContentResource1 = mock( Resource.class );
        featuredContentResource2 = mock( Resource.class );

        videoAssetValueMap = mock( ValueMap.class );
        skillsSheetAssetValueMap = mock( ValueMap.class );
        articlePageValueMap = mock( ValueMap.class );

        Whitebox.setInternalState( PastIssuesServiceImpl.class, "LOG", logger );
        Whitebox.setInternalState( pastIssuesService, resourceResolverFactory );
        Whitebox.setInternalState( pastIssuesService, queryBuilder );
        Whitebox.setInternalState( pastIssuesService, propertyConfigService );
        Whitebox.setInternalState( pastIssuesService, resourcePathConfigService );
        Whitebox.setInternalState( pastIssuesService, magazineProps );
        Whitebox.setInternalState( pastIssuesService, externalizer );

        PowerMockito.mockStatic( CommonUtils.class );
        PowerMockito.mockStatic( PredicateGroup.class );

        request = mock( SlingHttpServletRequest.class );
        resourceResolver = mock( ResourceResolver.class );
        session = mock( Session.class );
        predicateGroup = mock( PredicateGroup.class );
        query = mock( Query.class );
        searchResult = mock( SearchResult.class );
        hits = new ArrayList< Hit >();
        hitOne = mock( Hit.class );
        hitTwo = mock( Hit.class );

        hits.add( hitOne );
        hits.add( hitTwo );

        featuredContentList1 = new ArrayList< ValueMap >();
        featuredContentList2 = new ArrayList< ValueMap >();
        featuredContentValueMap1 = mock( ValueMap.class );
        featuredContentValueMap2 = mock( ValueMap.class );
        featuredContentValueMap3 = mock( ValueMap.class );

        featuredContentList1.add( featuredContentValueMap1 );
        featuredContentList1.add( featuredContentValueMap2 );
        featuredContentList2.add( featuredContentValueMap3 );

        when( hitOne.getPath() ).thenReturn( ISSUE_ONE_PATH );
        when( hitTwo.getPath() ).thenReturn( ISSUE_TWO_PATH );

        when( CommonUtils.getSession( resourceResolver ) ).thenReturn( session );
        when( PredicateGroup.create( any( Map.class ) ) ).thenReturn( predicateGroup );
        when( queryBuilder.createQuery( predicateGroup, session ) ).thenReturn( query );
        when( query.getResult() ).thenReturn( searchResult );
        when( searchResult.getHits() ).thenReturn( hits );
        when( CommonUtils.getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ) )
                .thenReturn( resourceResolver );
        when( CommonUtils.getTagManager( resourceResolverFactory ) ).thenReturn( tagManager );
        when( resourceResolver.getResource( ISSUE_ONE_PATH ) ).thenReturn( issueResource1 );
        when( resourceResolver.getResource( ISSUE_TWO_PATH ) ).thenReturn( issueResource2 );
        when( issueResource1.getChild( JcrConstants.JCR_CONTENT ) ).thenReturn( issueJcrResource1 );
        when( issueResource2.getChild( JcrConstants.JCR_CONTENT ) ).thenReturn( issueJcrResource2 );
        when( issueJcrResource1.getChild( "issue-configuration" ) ).thenReturn( featuredContentResource1 );
        when( issueJcrResource2.getChild( "issue-configuration" ) ).thenReturn( featuredContentResource2 );

        when( CommonUtils.fetchMultiFieldData( featuredContentResource1, "featuredResources" ) )
                .thenReturn( featuredContentList1 );
        when( CommonUtils.fetchMultiFieldData( featuredContentResource2, "featuredResources" ) )
                .thenReturn( featuredContentList2 );

        when( featuredContentValueMap1.get( "resourcePath", String.class ) ).thenReturn( FEATURED_RESOURCE_ONE_PATH );
        when( featuredContentValueMap1.get( "resourceType", String.class ) ).thenReturn( "video" );
        when( featuredContentValueMap2.get( "resourcePath", String.class ) ).thenReturn( FEATURED_RESOURCE_TWO_PATH );
        when( featuredContentValueMap2.get( "resourceType", String.class ) ).thenReturn( "skillsSheet" );
        when( featuredContentValueMap3.get( "resourcePath", String.class ) ).thenReturn( FEATURED_RESOURCE_THREE_PATH );
        when( featuredContentValueMap3.get( "resourceType", String.class ) ).thenReturn( "article" );

        when( magazineProps.getMagazineName( CURRENT_PAGE_PATH ) ).thenReturn( MAGAZINE_NAME );
        when( CommonUtils.getDataPath( resourcePathConfigService, MAGAZINE_NAME ) )
                .thenReturn( SCIENCE_WORLD_DATA_CONFIG_PAGE );
        when( CommonUtils.getDateFormat( propertyConfigService, SCIENCE_WORLD_DATA_CONFIG_PAGE ) )
                .thenReturn( "MMM yyyy" );

        // The issue page properties
        when( issueJcrResource1.getValueMap() ).thenReturn( issueProperties1 );
        when( issueJcrResource2.getValueMap() ).thenReturn( issueProperties2 );
        when( issueProperties1.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( ISSUE_ONE_IMAGE );
        when( issueProperties2.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( ISSUE_TWO_IMAGE );

        // mock video resource
        when( CommonUtils.getResource( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE,
                FEATURED_RESOURCE_ONE_PATH ) ).thenReturn( videoResource );
        when( videoResource.getChild( ClassMagsMigrationConstants.ASSET_METADATA_NODE ) )
                .thenReturn( videoMetadataResource );
        when( videoResource.adaptTo( Asset.class ) ).thenReturn( videoAsset );
        when( videoAsset.getPath() ).thenReturn( FEATURED_RESOURCE_ONE_PATH );
        when( videoAsset.getMetadataValue( DamConstants.DC_DESCRIPTION ) ).thenReturn( "video description" );
        when( videoMetadataResource.getValueMap() ).thenReturn( videoAssetValueMap );
        when( videoAssetValueMap.containsKey( "videoDuration" ) ).thenReturn( true );
        when( videoAssetValueMap.get( "videoDuration", StringUtils.EMPTY ) ).thenReturn( "2:44" );
        when( videoAssetValueMap.containsKey( "videoId" ) ).thenReturn( true );
        when( videoAssetValueMap.get( "videoId", Long.class ) ).thenReturn( new Long( 5674321 ) );
        when( videoAsset.getName() ).thenReturn( VIDEO_TITLE );
        when( videoAssetValueMap.get( DamConstants.DC_TITLE, StringUtils.EMPTY ) ).thenReturn( VIDEO_TITLE );
        when( CommonUtils.getSubjectTags( videoMetadataResource, tagManager ) ).thenReturn( TAGS );

        // mock skills sheet resource
        when( CommonUtils.getResource( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE,
                FEATURED_RESOURCE_TWO_PATH ) ).thenReturn( skillsSheetResource );
        when( skillsSheetResource.adaptTo( Asset.class ) ).thenReturn( skillsSheetAsset );
        when( skillsSheetAsset.getPath() ).thenReturn( FEATURED_RESOURCE_TWO_PATH );
        when( skillsSheetAsset.getMetadataValue( DamConstants.DC_DESCRIPTION ) )
                .thenReturn( "skills sheet description" );
        when( skillsSheetResource.getChild( ClassMagsMigrationConstants.ASSET_METADATA_NODE ) )
                .thenReturn( skillsSheetMetadataResource );
        when( skillsSheetMetadataResource.getValueMap() ).thenReturn( skillsSheetAssetValueMap );
        when( skillsSheetAssetValueMap.containsKey( "videoDuration" ) ).thenReturn( false );
        when( skillsSheetAssetValueMap.containsKey( "videoId" ) ).thenReturn( false );
        when( skillsSheetAsset.getName() ).thenReturn( SKILLS_SHEET_TITLE );
        when( skillsSheetAssetValueMap.get( DamConstants.DC_TITLE, StringUtils.EMPTY ) )
                .thenReturn( SKILLS_SHEET_TITLE );

        // mock article resource
        when( CommonUtils.getResource( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE,
                FEATURED_RESOURCE_THREE_PATH ) ).thenReturn( articlePageResource );
        when( articlePageValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( "/content/dam/scholastic/goingBatty" );
        when( articlePageResource.getChild( JcrConstants.JCR_CONTENT ) ).thenReturn( articleJcrNodeResource );
        when( articleJcrNodeResource.getValueMap() ).thenReturn( articlePageValueMap );
    }

    /**
     * Test get teaching resources.
     * 
     */
    @Test
    public void testGetTeachingResources() {

        // execute logic
        String pastIssuesJson = pastIssuesService.getPastIssuesJson( CURRENT_PAGE_PATH );

        // verify logic
        assertFalse( pastIssuesJson.isEmpty() );
    }

    /**
     * Test get teaching resources when issue node is null.
     * 
     */
    @Test
    public void testGetTeachingResourcesWhenIssueNodeIsNull() {
        // setup logic ( test-specific )
        when( resourceResolver.getResource( ISSUE_ONE_PATH ) ).thenReturn( null );

        // execute logic
        String pastIssuesJson = pastIssuesService.getPastIssuesJson( CURRENT_PAGE_PATH );

        // verify logic
        assertFalse( pastIssuesJson.isEmpty() );
    }

    /**
     * Test get teaching resources when featured content resource is null.
     * 
     */
    @Test
    public void testGetTeachingResourcesWhenFeaturedContentResourceIsNull() {
        // setup logic ( test-specific )
        when( issueJcrResource1.getChild( "issue-configuration" ) ).thenReturn( null );

        // execute logic
        String pastIssuesJson = pastIssuesService.getPastIssuesJson( CURRENT_PAGE_PATH );

        // verify logic
        assertFalse( pastIssuesJson.isEmpty() );
    }

    /**
     * Test get teaching resources when resource type is not asset or page.
     * 
     */
    @Test
    public void testGetTeachingResourcesWhenResourceTypeIsNotAssetOrPage() {
        // setup logic ( test-specific )
        when( featuredContentValueMap1.get( "resourceType", String.class ) ).thenReturn( "test" );

        // execute logic
        String pastIssuesJson = pastIssuesService.getPastIssuesJson( CURRENT_PAGE_PATH );

        // verify logic
        assertFalse( pastIssuesJson.isEmpty() );
    }

    /**
     * Test get teaching resources when page resource is null.
     * 
     */
    @Test
    public void testGetTeachingResourcesWhenPageResourceIsNull() {
        // setup logic ( test-specific )
        when( CommonUtils.getResource( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE,
                FEATURED_RESOURCE_THREE_PATH ) ).thenReturn( null );

        // execute logic
        String pastIssuesJson = pastIssuesService.getPastIssuesJson( CURRENT_PAGE_PATH );

        // verify logic
        assertFalse( pastIssuesJson.isEmpty() );
    }

    /**
     * Test get teaching resources when page resource has tags.
     * 
     */
    @Test
    public void testGetTeachingResourcesWhenPageResourceHasTags() {
        // setup logic ( test-specific )
        when( CommonUtils.getSubjectTags( articleJcrNodeResource, tagManager ) ).thenReturn( TAGS );

        // execute logic
        String pastIssuesJson = pastIssuesService.getPastIssuesJson( CURRENT_PAGE_PATH );

        // verify logic
        assertFalse( pastIssuesJson.isEmpty() );
    }

    /**
     * Test get teaching resources when video asset is null.
     * 
     */
    @Test
    public void testGetTeachingResourcesWhenVideoAssetIsNull() {
        // setup logic ( test-specific )
        when( CommonUtils.getResource( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE,
                FEATURED_RESOURCE_ONE_PATH ) ).thenReturn( null );

        // execute logic
        String pastIssuesJson = pastIssuesService.getPastIssuesJson( CURRENT_PAGE_PATH );

        // verify logic
        assertFalse( pastIssuesJson.isEmpty() );
    }

    /**
     * Test get teaching resources when video metadata node is null.
     * 
     */
    @Test
    public void testGetTeachingResourcesWhenVideoMetadataNodeIsNull() {
        // setup logic ( test-specific )
        when( videoResource.getChild( ClassMagsMigrationConstants.ASSET_METADATA_NODE ) ).thenReturn( null );

        // execute logic
        String pastIssuesJson = pastIssuesService.getPastIssuesJson( CURRENT_PAGE_PATH );

        // verify logic
        assertFalse( pastIssuesJson.isEmpty() );
    }

    /**
     * Test get teaching resources when date format is not available.
     * 
     * @throws LoginException
     * 
     */
    @Test
    public void testGetTeachingResourcesWhenDateFormatIsNotAvailable() throws LoginException {
        // setup logic ( test-specific )
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                SCIENCE_WORLD_DATA_CONFIG_PAGE ) ).thenReturn( "NO DATA AVAILABLE" );

        // execute logic
        String pastIssuesJson = pastIssuesService.getPastIssuesJson( CURRENT_PAGE_PATH );

        // verify logic
        assertFalse( pastIssuesJson.isEmpty() );
    }

    /**
     * Test get teaching resources when date format throws LoginException.
     * 
     * @throws LoginException
     * @throws ClassmagsMigrationBaseException 
     * 
     */
    @Test
    public void testGetTeachingResourcesWhenDateFormatThrowsLoginException()
            throws LoginException, ClassmagsMigrationBaseException {
        // setup logic ( test-specific )
        ClassmagsMigrationBaseException classmagsMigrationBaseException = new ClassmagsMigrationBaseException();
        when( CommonUtils.getDateFormat( propertyConfigService, SCIENCE_WORLD_DATA_CONFIG_PAGE ) )
                .thenThrow( classmagsMigrationBaseException );

        // execute logic
        pastIssuesService.getPastIssuesJson( CURRENT_PAGE_PATH );

        // verify logic
        verify( logger, times( 2 ) ).error( "::::Exception::::" + classmagsMigrationBaseException );
    }

    /**
     * Test get teaching resources when exception is thrown.
     *
     * @throws RepositoryException
     * 
     */
    @Test
    public void testGetTeachingResourcesWhenExceptionIsThrown() throws RepositoryException {
        // setup logic ( test-specific )
        RepositoryException repositoryException = new RepositoryException();
        when( hitOne.getPath() ).thenThrow( repositoryException );

        // execute logic
        pastIssuesService.getPastIssuesJson( CURRENT_PAGE_PATH );

        // verify logic
        verify( logger ).error( "::::Exception::::" + repositoryException );
    }

}
