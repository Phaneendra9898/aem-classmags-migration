package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.models.FooterObject;
import com.scholastic.classmags.migration.models.SocialLinks;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for FooterService.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { FooterServiceImpl.class, CommonUtils.class } )
public class FooterServiceImplTest {

    public static final String HEADING_TITLE_KEY = "headingTitle";
    public static final String PHONE_NUMBER_KEY = "phoneNumber";
    public static final String LINK_PATH_KEY = "linkPath";
    public static final String LINK_PATH_VALUE_ONE = "/content/scholastic-article-1";
    public static final String LINK_PATH_VALUE_TWO = "/content/scholastic-article-2";
    public static final String FOOTER_NODE_PATH = "/jcr:content/data-page-par/global_footer_contai";
    public static final String FOOTER_NODE_NAME = "footer-links";
    public static final String SOCIAL_LINKS_NODE_NAME = "social-links";
    public static final String FACEBOOK_LINK_PATH = "www.facebook.com";
    public static final String TWITTER_LINK_PATH = "www.twitter.com";
    public static final String HEADING_TITLE_VALUE_ONE = "Customer Service 1";
    public static final String HEADING_TITLE_VALUE_TWO = "Customer Service 2";
    public static final String PHONE_NUMBER_VALUE = "1-800-SCHOLASTIC";
    public static final String FOOTER_PARSYS_NAME = "/global-footer-par";
    public static final String SOCIAL_LINKS_NODE = "/social-links";
    public static final String SECTION_HEADING = "Follow Us";
    public static final String SECTION_HEADING_KEY = "sectionHeading";
    private static final String DISABLED_FOR_STUDENTS = "disabledForStudents";

    /** The footer service. */
    private FooterServiceImpl footerService;

    /** The resource path config service. */
    private ResourcePathConfigService resourcePathConfigService;

    /** The resource resolver. */
    private ResourceResolver resourceResolver;

    /** The resource. */
    private Resource resource;

    /** The global data page resource. */
    private Resource globalDataPageResource;

    /** The footer container resource. */
    private Resource footerContainerResource;

    /** The social links resource. */
    private Resource socialLinksResource;

    /** The child resources. */
    private Resource childResource1, childResource2;

    /** The resource iterator. */
    private Iterator< Resource > resourceIterator;

    /** The child resource properties. */
    private ValueMap childProperties1, childProperties2, socialLinksProperties;

    /** The link list. */
    private List< ValueMap > linkList, socialLinkList;

    /** The link list properties. */
    private ValueMap link1, link2;

    /** The social links. */
    private ValueMap socialLink1, socialLink2;

    /**
     * Initial Setup.
     */
    @SuppressWarnings( "unchecked" )
    @Before
    public void setUp() {
        footerService = new FooterServiceImpl();
        resourceResolver = mock( ResourceResolver.class );
        resource = mock( Resource.class );
        globalDataPageResource = mock( Resource.class );
        footerContainerResource = mock( Resource.class );
        socialLinksResource = mock( Resource.class );
        resourcePathConfigService = mock( ResourcePathConfigService.class );
        resourceIterator = mock( Iterator.class );
        childResource1 = mock( Resource.class );
        childResource2 = mock( Resource.class );
        childProperties1 = mock( ValueMap.class );
        childProperties2 = mock( ValueMap.class );
        socialLinksProperties = mock( ValueMap.class );
        link1 = mock( ValueMap.class );
        link2 = mock( ValueMap.class );
        socialLink1 = mock( ValueMap.class );
        socialLink2 = mock( ValueMap.class );

        linkList = new ArrayList< >();
        linkList.add( link1 );
        linkList.add( link2 );

        socialLinkList = new ArrayList< >();
        socialLinkList.add( socialLink1 );
        socialLinkList.add( socialLink2 );

        PowerMockito.mockStatic( CommonUtils.class );

        Whitebox.setInternalState( footerService, resourcePathConfigService );

        when( resource.getResourceResolver() ).thenReturn( resourceResolver );
        when( CommonUtils.getGlobalFooterPath( resourcePathConfigService, "" ) ).thenReturn( FOOTER_NODE_PATH );
        when( resourceResolver.getResource( FOOTER_NODE_PATH + FOOTER_PARSYS_NAME ) )
                .thenReturn( footerContainerResource );
        when( resourceIterator.hasNext() ).thenReturn( true, true, false );
        when( resourceIterator.next() ).thenReturn( childResource1, childResource2 );
        when( footerContainerResource.listChildren() ).thenReturn( resourceIterator );
        when( childResource1.getValueMap() ).thenReturn( childProperties1 );
        when( childResource2.getValueMap() ).thenReturn( childProperties2 );
        when( childProperties1.get( HEADING_TITLE_KEY, String.class ) ).thenReturn( HEADING_TITLE_VALUE_ONE );
        when( childProperties1.get( PHONE_NUMBER_KEY, String.class ) ).thenReturn( PHONE_NUMBER_VALUE );
        when( childProperties2.get( HEADING_TITLE_KEY, String.class ) ).thenReturn( HEADING_TITLE_VALUE_TWO );
        when( socialLinksProperties.containsKey( SECTION_HEADING_KEY )).thenReturn( true );
        when( socialLinksProperties.get( SECTION_HEADING_KEY, String.class ) ).thenReturn( SECTION_HEADING );
        when( socialLinksProperties.containsKey( DISABLED_FOR_STUDENTS )).thenReturn( true );
        when( socialLinksProperties.get( DISABLED_FOR_STUDENTS, Boolean.class)).thenReturn(true);
        when( link1.get( LINK_PATH_KEY, String.class ) ).thenReturn( LINK_PATH_VALUE_ONE );
        when( link2.get( LINK_PATH_KEY, String.class ) ).thenReturn( LINK_PATH_VALUE_TWO );
        when( CommonUtils.fetchMultiFieldData( childResource1, FOOTER_NODE_NAME ) ).thenReturn( linkList );

    }

    /**
     * Test get footer objects.
     */
    @Test
    public void testGetFooterObjects() {

        // execute logic
        List< FooterObject > footerObjects = footerService.getFooterObjects( resource, "" );

        // verify logic
        assertEquals( 2, footerObjects.size() );
        assertEquals( HEADING_TITLE_VALUE_ONE, footerObjects.get( 0 ).getHeadingTitle() );
        assertEquals( HEADING_TITLE_VALUE_TWO, footerObjects.get( 1 ).getHeadingTitle() );
        assertEquals( PHONE_NUMBER_VALUE, footerObjects.get( 0 ).getPhoneNumber() );
        assertEquals( LINK_PATH_VALUE_ONE,
                footerObjects.get( 0 ).getFooterLinks().get( 0 ).getPath());
        assertEquals( LINK_PATH_VALUE_TWO,
                footerObjects.get( 0 ).getFooterLinks().get( 1 ).getPath() );
    }

    /**
     * Test get footer objects when no container is present.
     */
    @Test
    public void testGetFooterObjectsWhenNoContainerIsPresent() {
        // setup logic ( test-specific )
        when( resourceResolver.getResource( FOOTER_NODE_PATH + FOOTER_PARSYS_NAME ) ).thenReturn( null );

        // execute logic
        List< FooterObject > footerObjects = footerService.getFooterObjects( resource, "" );

        // verify logic
        assertTrue( footerObjects.isEmpty() );
    }

    /**
     * Test get footer objects when resourceResolver is null.
     */
    @Test
    public void testGetFooterObjectsWhenResourceResolverIsNull() {
        // setup logic ( test-specific )
        when( resource.getResourceResolver() ).thenReturn( null );

        // execute logic
        List< FooterObject > footerObjects = footerService.getFooterObjects( resource, "" );

        // verify logic
        assertTrue( footerObjects.isEmpty() );
    }

    /**
     * Test get social links when no container is present.
     */
    @Test
    public void testGetSocialLinksWhenNoContainerIsPresent() {
        // setup logic ( test-specific )
        when( resourceResolver.getResource( FOOTER_NODE_PATH + SOCIAL_LINKS_NODE ) ).thenReturn( null );

        // execute logic
        SocialLinks socialLinks = footerService.getSocialLinks( resource, "" );

        // verify logic
        assertNull( socialLinks );
    }

    /**
     * Test get social links.
     */
    @Test
    public void testGetSocialLinks() {

        // execute logic
        when( resourceResolver.getResource( FOOTER_NODE_PATH ) ).thenReturn( globalDataPageResource );
        when( globalDataPageResource.getChild( SOCIAL_LINKS_NODE_NAME ) ).thenReturn( socialLinksResource );
        when( CommonUtils.fetchMultiFieldData( socialLinksResource, SOCIAL_LINKS_NODE_NAME ) )
                .thenReturn( socialLinkList );
        when( socialLinksResource.getValueMap() ).thenReturn( socialLinksProperties );
        when( socialLink1.get( LINK_PATH_KEY, String.class ) ).thenReturn( FACEBOOK_LINK_PATH );
        when( socialLink2.get( LINK_PATH_KEY, String.class ) ).thenReturn( TWITTER_LINK_PATH );
        SocialLinks socialLinks = footerService.getSocialLinks( resource, "" );

        // verify logic
        assertEquals( 2, socialLinks.getLinks().size() );
        assertEquals( FACEBOOK_LINK_PATH, socialLinks.getLinks().get( 0 ).get( LINK_PATH_KEY, String.class ) );
        assertEquals( TWITTER_LINK_PATH, socialLinks.getLinks().get( 1 ).get( LINK_PATH_KEY, String.class ) );
        assertEquals( SECTION_HEADING, socialLinks.getSectionHeading() );
        assertTrue( socialLinks.isDisabledForStudents() );
    }

    /**
     * Test get social links when properties is null.
     */
    @Test
    public void testGetSocialLinksWhenPropertiesIsNull() {

        // execute logic
        when( resourceResolver.getResource( FOOTER_NODE_PATH ) ).thenReturn( globalDataPageResource );
        when( globalDataPageResource.getChild( SOCIAL_LINKS_NODE_NAME ) ).thenReturn( socialLinksResource );
        when( CommonUtils.fetchMultiFieldData( socialLinksResource, SOCIAL_LINKS_NODE_NAME ) )
                .thenReturn( socialLinkList );
        when( socialLinksResource.getValueMap() ).thenReturn( null );
        when( socialLink1.get( LINK_PATH_KEY, String.class ) ).thenReturn( FACEBOOK_LINK_PATH );
        when( socialLink2.get( LINK_PATH_KEY, String.class ) ).thenReturn( TWITTER_LINK_PATH );
        SocialLinks socialLinks = footerService.getSocialLinks( resource, "" );

        // verify logic
        assertEquals( 2, socialLinks.getLinks().size() );
        assertEquals( FACEBOOK_LINK_PATH, socialLinks.getLinks().get( 0 ).get( LINK_PATH_KEY, String.class ) );
        assertEquals( TWITTER_LINK_PATH, socialLinks.getLinks().get( 1 ).get( LINK_PATH_KEY, String.class ) );
        assertNull( socialLinks.getSectionHeading() );
    }

    /**
     * Test get social links when social links are not configured.
     */
    @Test
    public void testGetSocialLinksWhenSocialLinksNotConfigured() {

        // setup logic ( test-specific )
        when( resourceResolver.getResource( FOOTER_NODE_PATH ) ).thenReturn( globalDataPageResource );
        when( globalDataPageResource.getChild( SOCIAL_LINKS_NODE_NAME ) ).thenReturn( null );

        // execute logic
        SocialLinks socialLinks = footerService.getSocialLinks( resource, "" );

        // verify logic
        assertNull( socialLinks );
    }

    /**
     * Test get social links when resourceResolver is null.
     */
    @Test
    public void testGetSocialLinksWhenResourceResolverIsNull() {
        // setup logic ( test-specific )
        when( resource.getResourceResolver() ).thenReturn( null );

        // execute logic
        SocialLinks socialLinks = footerService.getSocialLinks( resource, "" );

        // verify logic
        assertNull( socialLinks );
    }
    
    /**
     * Test get social links when disable for students key not present.
     */
    @Test
    public void testGetSocialLinksWhenDisableForStudentKeyNotPresent() {
        // setup logic ( test-specific )
        when( resourceResolver.getResource( FOOTER_NODE_PATH ) ).thenReturn( globalDataPageResource );
        when( globalDataPageResource.getChild( SOCIAL_LINKS_NODE_NAME ) ).thenReturn( socialLinksResource );
        when( CommonUtils.fetchMultiFieldData( socialLinksResource, SOCIAL_LINKS_NODE_NAME ) )
                .thenReturn( socialLinkList );
        when( socialLinksResource.getValueMap() ).thenReturn( socialLinksProperties );
        when( socialLinksProperties.containsKey( DISABLED_FOR_STUDENTS )).thenReturn(false);

        // execute logic
        SocialLinks socialLinks = footerService.getSocialLinks( resource, "" );

        // verify logic
        assertTrue( socialLinks.isDisabledForStudents() );
    }
    
    /**
     * Test get social links when section heading key is not present.
     */
    @Test
    public void testGetSocialLinksWhenSectionHeadingKeyNotPresent() {
        // setup logic ( test-specific )
        when( resourceResolver.getResource( FOOTER_NODE_PATH ) ).thenReturn( globalDataPageResource );
        when( globalDataPageResource.getChild( SOCIAL_LINKS_NODE_NAME ) ).thenReturn( socialLinksResource );
        when( CommonUtils.fetchMultiFieldData( socialLinksResource, SOCIAL_LINKS_NODE_NAME ) )
                .thenReturn( socialLinkList );
        when( socialLinksResource.getValueMap() ).thenReturn( socialLinksProperties );
        when( socialLinksProperties.containsKey( SECTION_HEADING_KEY )).thenReturn(false);

        // execute logic
        SocialLinks socialLinks = footerService.getSocialLinks( resource, "" );

        // verify logic
        assertNull( socialLinks.getSectionHeading());
    }
}
