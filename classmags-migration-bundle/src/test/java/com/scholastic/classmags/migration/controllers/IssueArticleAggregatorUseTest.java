package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.models.ArticleAggregatorObject;
import com.scholastic.classmags.migration.services.ArticleAggregatorService;

/**
 * JUnit forSubscriptionBenefitsUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { IssueArticleAggregatorUse.class } )
public class IssueArticleAggregatorUseTest extends BaseComponentTest {

    private IssueArticleAggregatorUse issueArticleAggregatorUse;

    private ArticleAggregatorService articleAggregatorService;
    
    List< ArticleAggregatorObject > articleAggregator;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        issueArticleAggregatorUse = Whitebox.newInstance( IssueArticleAggregatorUse.class );
        stubCommon( issueArticleAggregatorUse );
        
        articleAggregatorService = mock( ArticleAggregatorService.class );
        articleAggregator = mock(List.class);
        when( slingScriptHelper.getService( ArticleAggregatorService.class ) ).thenReturn( articleAggregatorService );
    }

    @Test
    public void testIssueArticleAggregator() throws Exception {

        // execute logic
        Whitebox.invokeMethod( issueArticleAggregatorUse, "activate" );
        
        //verify logic
        assertNotNull(issueArticleAggregatorUse.getArticleAggregator());
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final IssueArticleAggregatorUse issueArticleAggregatorUse = new IssueArticleAggregatorUse();

        // verify logic
        assertNotNull( issueArticleAggregatorUse );
    }

    @Test
    public void testWhenArticleAggregatorServiceIsNull() throws Exception {
        // setup logic ( test-specific )
        when( slingScriptHelper.getService( ArticleAggregatorService.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( issueArticleAggregatorUse, "activate" );

        // verify logic
        assertNull( issueArticleAggregatorUse.getArticleAggregator() );
    }

    @Test
    public void testWhenSlingScriptHelperIsNull() throws Exception {
        // setup logic ( test-specific )
        stub( PowerMockito.method( issueArticleAggregatorUse.getClass(), "getSlingScriptHelper" ) ).toReturn( null );
     
        
        // execute logic
        Whitebox.invokeMethod( issueArticleAggregatorUse, "activate" );

        // verify logic
        assertNull( issueArticleAggregatorUse.getArticleAggregator() );
        
    }
    
    @Test
    public void testWhenCurrentPageIsNull() throws Exception {
        // setup logic ( test-specific )
        stub( PowerMockito.method( issueArticleAggregatorUse.getClass(), "getCurrentPage" ) ).toReturn( null );
     
        
        // execute logic
        Whitebox.invokeMethod( issueArticleAggregatorUse, "activate" );

        // verify logic
        assertNull( issueArticleAggregatorUse.getArticleAggregator() );
        
    }

}
