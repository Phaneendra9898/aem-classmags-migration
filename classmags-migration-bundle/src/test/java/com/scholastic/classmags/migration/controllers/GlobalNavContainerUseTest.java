package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;
import static org.mockito.Mockito.mock;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * JUnit for GlobalNavContainerUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { GlobalNavContainerUse.class, CommonUtils.class, InternalURLFormatter.class } )
public class GlobalNavContainerUseTest extends BaseComponentTest {

    private static final String CURRENT_PAGE_PATH = "/scholastic/classroom_magazines/scienceworld/issues/090117";
    private static final String MAGAZINE_NAME = "scienceworld";
    private static final String SCIENCE_WORLD_DATA_PAGE = "/content/classroom-magazines-admin/scienceworld-data-config-page";
    private static final String GLOBAL_DATA_CONFIG_PAGE = "/content/classroom-magazines-admin/scienceworld-data-config-page";
    private static final String HOME_PAGE_LOGGED_OUT = "/content/classroom_magazines/scienceworld/content/home-page-logged-out";
    private static final String HOME_PAGE_LOGGED_IN = "/content/classroom_magazines/scienceworld/content/home-page-logged-in";
    private static final String SDM_LOGOUT = "/test/sdm/logout/page";
    private static final String SDM_CREATE_AC_PAGE = "/test/create/account/page";
    private static final String SDM_TCH_LOGIN_PAGE = "/test/teacher/login/page";
    private static final String SDM_STU_LOGIN_PAGE = "/test/student/login/page";
    private static final String SEARCH_RESULTS_PAGE = "/search/results/page";
    private static final String HTML_SUFFIX = ".html";
    private static final String HOME_PAGE_LOGGED_OUT_GLOBAL = "/content/classroom_magazines/global/content/home-page-logged-out";
    
    private GlobalNavContainerUse globalNavContainerUse;
    private ResourcePathConfigService resourcePathConfigService;
    private PropertyConfigService propertyConfigService;
    private MagazineProps magazineProps;

    /**
     * Setup.
     * @throws LoginException 
     */
    @Before
    public void setup() throws LoginException {
        globalNavContainerUse = Whitebox.newInstance( GlobalNavContainerUse.class );

        stubCommon( globalNavContainerUse );
        
        resourcePathConfigService = mock( ResourcePathConfigService.class );
        propertyConfigService = mock( PropertyConfigService.class );
        magazineProps = mock( MagazineProps.class );
        
        PowerMockito.mockStatic( CommonUtils.class );
        PowerMockito.mockStatic( InternalURLFormatter.class );
        
        when( slingScriptHelper.getService( ResourcePathConfigService.class )).thenReturn( resourcePathConfigService );
        when( slingScriptHelper.getService( PropertyConfigService.class )).thenReturn( propertyConfigService);
        when( slingScriptHelper.getService( MagazineProps.class )).thenReturn( magazineProps );
        
        when( currentPage.getPath()).thenReturn( CURRENT_PAGE_PATH );
        when( magazineProps.getMagazineName( CURRENT_PAGE_PATH )).thenReturn( MAGAZINE_NAME );
        when( CommonUtils.getDataPath( resourcePathConfigService, MAGAZINE_NAME )).thenReturn( SCIENCE_WORLD_DATA_PAGE );
        
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.HOMEPAGE_LOGGEDOUT, SCIENCE_WORLD_DATA_PAGE )).thenReturn( HOME_PAGE_LOGGED_OUT );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.HOMEPAGE_LOGGEDIN, SCIENCE_WORLD_DATA_PAGE )).thenReturn( HOME_PAGE_LOGGED_IN );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.SDM_LOGOUT, SCIENCE_WORLD_DATA_PAGE )).thenReturn( SDM_LOGOUT );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.SDM_CREATE_AC, SCIENCE_WORLD_DATA_PAGE )).thenReturn( SDM_CREATE_AC_PAGE );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.SDM_TCH_LOGIN, SCIENCE_WORLD_DATA_PAGE )).thenReturn( SDM_TCH_LOGIN_PAGE );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.SDM_STU_LOGIN, SCIENCE_WORLD_DATA_PAGE )).thenReturn( SDM_STU_LOGIN_PAGE );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.SEARCH_RESULTS, SCIENCE_WORLD_DATA_PAGE )).thenReturn( SEARCH_RESULTS_PAGE );
        
        when( InternalURLFormatter.formatURL( resourceResolver, HOME_PAGE_LOGGED_OUT )).thenReturn( HOME_PAGE_LOGGED_OUT + HTML_SUFFIX );
        when( InternalURLFormatter.formatURL( resourceResolver, HOME_PAGE_LOGGED_IN )).thenReturn( HOME_PAGE_LOGGED_IN+ HTML_SUFFIX );
        when( InternalURLFormatter.formatURL( resourceResolver, SEARCH_RESULTS_PAGE )).thenReturn( SEARCH_RESULTS_PAGE + HTML_SUFFIX );
    }

    @Test
    public void testGlobalNavContainerUse() throws Exception {
        // execute logic
        Whitebox.invokeMethod( globalNavContainerUse, "activate" );

        // verify logic
        assertEquals(HOME_PAGE_LOGGED_OUT + HTML_SUFFIX, globalNavContainerUse.getHomePageLoggedOut());
        assertEquals(HOME_PAGE_LOGGED_IN + HTML_SUFFIX, globalNavContainerUse.getHomePageLoggedIn());
        assertEquals(SDM_LOGOUT, globalNavContainerUse.getSdmLogOutUrl());
        assertEquals(SDM_CREATE_AC_PAGE, globalNavContainerUse.getSdmCreateAccountUrl());
        assertEquals(SDM_TCH_LOGIN_PAGE, globalNavContainerUse.getSdmTeacherLoginUrl());
        assertEquals(SDM_STU_LOGIN_PAGE, globalNavContainerUse.getSdmStudentLoginUrl());
        assertEquals(SEARCH_RESULTS_PAGE + HTML_SUFFIX, globalNavContainerUse.getSearchResultsUrl());
    }
    
    @Test
    public void testGlobalNavContainerUseWhenPropertyConfigServiceIsNull() throws Exception {
        // setup logic (test-specific)
        when( slingScriptHelper.getService( PropertyConfigService.class ) ).thenReturn( null );
        
        // execute logic
        Whitebox.invokeMethod( globalNavContainerUse, "activate" );

        // verify logic
        assertNull( globalNavContainerUse.getHomePageLoggedOut() );
        assertNull( globalNavContainerUse.getHomePageLoggedIn() );
        assertNull( globalNavContainerUse.getSdmLogOutUrl() );
        assertNull( globalNavContainerUse.getSdmCreateAccountUrl() );
        assertNull( globalNavContainerUse.getSdmTeacherLoginUrl() );
        assertNull( globalNavContainerUse.getSdmStudentLoginUrl() );
        assertNull( globalNavContainerUse.getSearchResultsUrl() );
    }

    @Test
    public void testGlobalNavContainerUseWhenResourcePathConfigServiceIsNull() throws Exception {
        // setup logic (test-specific)
        when( slingScriptHelper.getService( ResourcePathConfigService.class ) ).thenReturn( null );
        
        // execute logic
        Whitebox.invokeMethod( globalNavContainerUse, "activate" );

        // verify logic
        assertNull( globalNavContainerUse.getHomePageLoggedOut() );
        assertNull( globalNavContainerUse.getHomePageLoggedIn() );
        assertNull( globalNavContainerUse.getSdmLogOutUrl() );
        assertNull( globalNavContainerUse.getSdmCreateAccountUrl() );
        assertNull( globalNavContainerUse.getSdmTeacherLoginUrl() );
        assertNull( globalNavContainerUse.getSdmStudentLoginUrl() );
        assertNull( globalNavContainerUse.getSearchResultsUrl() );
    }
    
    @Test
    public void testGlobalNavContainerUseWhenNoSearchResultsPageIsPresent() throws Exception {
        // setup logic (test-specific)
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.SEARCH_RESULTS, SCIENCE_WORLD_DATA_PAGE )).thenReturn( StringUtils.EMPTY );
        
        // execute logic
        Whitebox.invokeMethod( globalNavContainerUse, "activate" );

        // verify logic
        assertTrue( globalNavContainerUse.getSearchResultsUrl().isEmpty() );
    }
    
    @Test
    public void testGlobalNavContainerUseWhenSlingScriptHelperIsNull() throws Exception {
        // setup logic (test-specific)
        stub( PowerMockito.method( globalNavContainerUse.getClass(), "getSlingScriptHelper" ) ).toReturn( null );
        // execute logic
        Whitebox.invokeMethod( globalNavContainerUse, "activate" );

        // verify logic
        assertNull( globalNavContainerUse.getHomePageLoggedOut() );
        assertNull( globalNavContainerUse.getHomePageLoggedIn() );
        assertNull( globalNavContainerUse.getSdmLogOutUrl() );
        assertNull( globalNavContainerUse.getSdmCreateAccountUrl() );
        assertNull( globalNavContainerUse.getSdmTeacherLoginUrl() );
        assertNull( globalNavContainerUse.getSdmStudentLoginUrl() );
        assertNull( globalNavContainerUse.getSearchResultsUrl() );
    }
    
    @Test
    public void testGlobalNavContainerUseWhenMagazinePropsServiceIsNull() throws Exception {
        // setup logic (test-specific)
        when( slingScriptHelper.getService( MagazineProps.class ) ).thenReturn( null );
        when( CommonUtils.getDataPath( resourcePathConfigService, StringUtils.EMPTY ) )
                .thenReturn( GLOBAL_DATA_CONFIG_PAGE );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.HOMEPAGE_LOGGEDOUT,
                GLOBAL_DATA_CONFIG_PAGE ) ).thenReturn( HOME_PAGE_LOGGED_OUT_GLOBAL );
        when( InternalURLFormatter.formatURL( resourceResolver, HOME_PAGE_LOGGED_OUT_GLOBAL ) )
                .thenReturn( HOME_PAGE_LOGGED_OUT_GLOBAL + HTML_SUFFIX );

        // execute logic
        Whitebox.invokeMethod( globalNavContainerUse, "activate" );

        // verify logic
        assertEquals( HOME_PAGE_LOGGED_OUT_GLOBAL + HTML_SUFFIX, globalNavContainerUse.getHomePageLoggedOut() );
    }
    
    @Test
    public void testGlobalNavContainerUseWhenCurrentPageIsNull() throws Exception {
        // setup logic (test-specific)
        stub( PowerMockito.method( globalNavContainerUse.getClass(), "getCurrentPage" ) ).toReturn( null );
        when( CommonUtils.getDataPath( resourcePathConfigService, StringUtils.EMPTY ) )
                .thenReturn( GLOBAL_DATA_CONFIG_PAGE );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.HOMEPAGE_LOGGEDOUT,
                GLOBAL_DATA_CONFIG_PAGE ) ).thenReturn( HOME_PAGE_LOGGED_OUT_GLOBAL );
        when( InternalURLFormatter.formatURL( resourceResolver, HOME_PAGE_LOGGED_OUT_GLOBAL ) )
                .thenReturn( HOME_PAGE_LOGGED_OUT_GLOBAL + HTML_SUFFIX );

        // execute logic
        Whitebox.invokeMethod( globalNavContainerUse, "activate" );

        // verify logic
        assertEquals( HOME_PAGE_LOGGED_OUT_GLOBAL + HTML_SUFFIX, globalNavContainerUse.getHomePageLoggedOut() );
    }
    
    @Test
    public void testCreateInstance() {
        // execute logic
        final GlobalNavContainerUse globalNavContainerUse = new GlobalNavContainerUse();

        // verify logic
        assertNotNull( globalNavContainerUse );
    }

}
