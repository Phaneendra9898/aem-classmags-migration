package com.scholastic.classmags.migration.services.impl;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for UpdateVotingNodeServiceImpl
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { UpdateVotingNodeServiceImpl.class, CommonUtils.class, URLEncoder.class } )
public class UpdateVotingNodeServiceImplTest {

    private static final String UUID_KEY = "uuid";
    private static final String QUESTION_KEY = "question";
    private static final String SAVED_UUID = "saved-uuid";
    private static final String NEW_UUID = "new-uuid";
    private static final String NEW_QUESTION = "new-question";
    private static final String SAVED_QUESTION = "saved-question";

    private UpdateVotingNodeServiceImpl updateVotingNodeServiceImpl;
    private ModifiableValueMap modifiableValueMap;
    private ValueMap properties;
    private ResourceResolverFactory resourceResolverFactory;
    private ResourceResolver resourceResolver;
    private Resource resource;

    private Logger logger;

    /**
     * Initial setup.
     * 
     * @throws UnsupportedEncodingException
     */
    @Before
    public void setUp() throws UnsupportedEncodingException {
        updateVotingNodeServiceImpl = new UpdateVotingNodeServiceImpl();

        logger = mock( Logger.class );
        modifiableValueMap = mock( ModifiableValueMap.class );
        resource = mock( Resource.class );
        properties = mock( ValueMap.class );
        resourceResolverFactory = mock( ResourceResolverFactory.class );
        resourceResolver = mock( ResourceResolver.class );
        resourceResolver = mock( ResourceResolver.class );

        PowerMockito.mockStatic( CommonUtils.class );
        PowerMockito.mockStatic( URLEncoder.class );

        Whitebox.setInternalState( updateVotingNodeServiceImpl, resourceResolverFactory );

        Whitebox.setInternalState( UpdateVotingNodeServiceImpl.class, "LOG", logger );

        when( resource.adaptTo( ModifiableValueMap.class ) ).thenReturn( modifiableValueMap );
        when( resource.getValueMap() ).thenReturn( properties );
        when( properties.get( QUESTION_KEY, String.class ) ).thenReturn( NEW_QUESTION );
        when( modifiableValueMap.containsKey( UUID_KEY ) ).thenReturn( true );
        when( modifiableValueMap.get( UUID_KEY, String.class ) ).thenReturn( SAVED_UUID );
        when( modifiableValueMap.get( SAVED_UUID, String.class ) ).thenReturn( SAVED_QUESTION );
        when( URLEncoder.encode( NEW_QUESTION, ClassMagsMigrationASRPConstants.ENCODE_FORMAT ) )
                .thenReturn( "New%20Question" );
        when( URLEncoder.encode( SAVED_QUESTION, ClassMagsMigrationASRPConstants.ENCODE_FORMAT ) )
                .thenReturn( "Saved%20Question" );
        when( CommonUtils.generateUniqueIdentifier() ).thenReturn( NEW_UUID );
        when( resource.getResourceResolver() ).thenReturn( resourceResolver );

    }

    /**
     * Test update voting node.
     * 
     * @throws PersistenceException
     */
    @Test
    public void testUpdateVotingNode() throws PersistenceException {
        // execute logic
        updateVotingNodeServiceImpl.updateVotingNode( resource );

        // verify logic
        verify( modifiableValueMap ).replace( UUID_KEY, NEW_UUID );
        verify( modifiableValueMap ).remove( SAVED_UUID );
        verify( modifiableValueMap ).put( NEW_UUID, NEW_QUESTION );
    }

    /**
     * Test update voting node when question is not changed.
     * 
     * @throws PersistenceException
     */
    @Test
    public void testUpdateVotingNodeWhenQuestionIsNotChanged() throws PersistenceException {
        // setup logic ( test-specific )
        when( properties.get( QUESTION_KEY, String.class ) ).thenReturn( SAVED_QUESTION );

        // execute logic
        updateVotingNodeServiceImpl.updateVotingNode( resource );

        // verify logic
        verify( modifiableValueMap, never() ).replace( UUID_KEY, NEW_UUID );
        verify( modifiableValueMap, never() ).remove( SAVED_UUID );
        verify( modifiableValueMap, never() ).put( NEW_UUID, NEW_QUESTION );
    }

    /**
     * Test update voting node when question is not changed.
     * 
     * @throws PersistenceException
     */
    @Test
    public void testUpdateVotingNodeWhenQuestionIsSavedForFirstTime() throws PersistenceException {
        // setup logic ( test-specific )
        when( modifiableValueMap.containsKey( UUID_KEY ) ).thenReturn( false );

        // execute logic
        updateVotingNodeServiceImpl.updateVotingNode( resource );

        // verify logic
        verify( modifiableValueMap ).put( UUID_KEY, NEW_UUID );
        verify( modifiableValueMap ).put( NEW_UUID, NEW_QUESTION );
        verify( resourceResolver ).commit();
    }

    /**
     * Test update voting node when encoding exception occurs.
     * 
     * @throws PersistenceException
     * @throws UnsupportedEncodingException
     */
    @Test
    public void testUpdateVotingNodeWhenUnsupportedEncodingExceptionOccurs()
            throws PersistenceException, UnsupportedEncodingException {
        // setup logic ( test-specific )
        UnsupportedEncodingException e = new UnsupportedEncodingException();
        when( URLEncoder.encode( NEW_QUESTION, ClassMagsMigrationASRPConstants.ENCODE_FORMAT ) ).thenThrow( e );

        // execute logic
        updateVotingNodeServiceImpl.updateVotingNode( resource );

        // verify logic
        verify( logger ).error( "Error while encoding", e );
    }

}
