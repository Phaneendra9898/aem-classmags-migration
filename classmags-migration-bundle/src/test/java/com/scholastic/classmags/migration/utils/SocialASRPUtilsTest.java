package com.scholastic.classmags.migration.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.adobe.cq.social.scf.SocialComponent;
import com.adobe.cq.social.scf.SocialComponentFactory;
import com.adobe.cq.social.scf.SocialComponentFactoryManager;
import com.adobe.cq.social.scf.SocialOperationResult;
import com.adobe.cq.social.srp.SocialResourceProvider;
import com.adobe.cq.social.ugcbase.SocialUtils;
import com.scholastic.classmags.migration.models.UIdHeaderIdentifiers;
import com.scholastic.classmags.migration.models.UserIdCookieData;
import com.scholastic.classmags.migration.social.api.BookmarkSocialComponent;

/**
 * The Class SocialASRPUtilsTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { SocialASRPUtils.class, CookieUtil.class } )
public class SocialASRPUtilsTest {

    /** The Constant TEST_PATH. */
    private static final String TEST_PATH = "/testPath";

    /** The Constant TEST_USER_ID. */
    private static final String TEST_USER_ID = "249216";

    /** The mock sling http servlet request. */
    @Mock
    private SlingHttpServletRequest mockSlingHttpServletRequest;

    /** The mock user id cookie data. */
    @Mock
    private UserIdCookieData mockUserIdCookieData;

    /** The mock U id header identifiers. */
    @Mock
    private UIdHeaderIdentifiers mockUIdHeaderIdentifiers;

    /** The mock resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;

    /** The mock srp. */
    @Mock
    private SocialResourceProvider mockSrp;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock scf manager. */
    @Mock
    private SocialComponentFactoryManager mockScfManager;

    /** The mock social component factory. */
    @Mock
    private SocialComponentFactory mockSocialComponentFactory;

    /** The mock social component. */
    @Mock
    private SocialComponent mockSocialComponent;

    /** The mock social operation result. */
    @Mock
    private SocialOperationResult mockSocialOperationResult;

    /** The mock props map. */
    @Mock
    private Map< String, Object > mockPropsMap;

    /**
     * Setup.
     */
    @Before
    public void setup() {

        PowerMockito.mockStatic( CookieUtil.class );
        when( CookieUtil.fetchUserIdCookieData( mockSlingHttpServletRequest ) ).thenReturn( mockUserIdCookieData );
        when( mockUserIdCookieData.getObjUIdHeaderIdentifiers() ).thenReturn( mockUIdHeaderIdentifiers );
        when( mockUIdHeaderIdentifiers.getStaffId() ).thenReturn( TEST_USER_ID );
        when( mockUIdHeaderIdentifiers.getStudentId() ).thenReturn( TEST_USER_ID );

    }

    /**
     * Testurl encoder.
     *
     * @throws UnsupportedEncodingException
     *             the unsupported encoding exception
     */
    @Test
    public void testurlEncoder() throws UnsupportedEncodingException {

        String encodedPath = SocialASRPUtils.urlEncoder( TEST_PATH );
        assertNotNull( encodedPath );
    }

    /**
     * Testfetch user id teacher.
     */
    @Test
    public void testfetchUserIdTeacher() {

        assertEquals( TEST_USER_ID,
                SocialASRPUtils.fetchUserId( mockSlingHttpServletRequest, RoleUtil.ROLE_TYPE_TEACHER ) );
    }

    /**
     * Testfetch user id student.
     */
    @Test
    public void testfetchUserIdStudent() {

        assertEquals( TEST_USER_ID,
                SocialASRPUtils.fetchUserId( mockSlingHttpServletRequest, RoleUtil.ROLE_TYPE_STUDENT ) );
    }

    /**
     * Test is valid user.
     */
    @Test
    public void testIsValidUser() {

        assertEquals( true, SocialASRPUtils.isValidUser( RoleUtil.ROLE_TYPE_TEACHER, TEST_USER_ID ) );
    }

    /**
     * Test get UGC resource from path.
     */
    @Test
    public void testGetUGCResourceFromPath() {

        when( mockSrp.getResource( mockResourceResolver, TEST_PATH ) ).thenReturn( mockResource );
        assertEquals( mockResource,
                SocialASRPUtils.getUGCResourceFromPath( mockResourceResolver, TEST_PATH, mockSrp ) );
    }

    /**
     * Test path builder.
     */
    @Test
    public void testPathBuilder() {

        assertEquals( SocialUtils.SRP_CLOUD_PATH + TEST_PATH, SocialASRPUtils.pathBuilder( TEST_PATH ) );
    }

    /**
     * Test get social component.
     */
    @Test
    public void testGetSocialComponent() {

        when( mockScfManager.getSocialComponentFactory( BookmarkSocialComponent.BOOKMARK_RESOURCE_TYPE ) )
                .thenReturn( mockSocialComponentFactory );

        when( mockSocialComponentFactory.getSocialComponent( mockResource, mockSlingHttpServletRequest ) )
                .thenReturn( mockSocialComponent );

        assertEquals( mockSocialComponent, SocialASRPUtils.getSocialComponent( mockSlingHttpServletRequest,
                mockResource, mockScfManager, BookmarkSocialComponent.BOOKMARK_RESOURCE_TYPE ) );
    }

    /**
     * Test get social operation result.
     */
    @Test
    public void testGetSocialOperationResult() {

        when( mockSlingHttpServletRequest.getResource() ).thenReturn( mockResource );
        when( mockResource.getPath() ).thenReturn( TEST_PATH );
        when( mockScfManager.getSocialComponentFactory( BookmarkSocialComponent.BOOKMARK_RESOURCE_TYPE ) )
                .thenReturn( mockSocialComponentFactory );
        when( mockSocialComponentFactory.getSocialComponent( mockResource, mockSlingHttpServletRequest ) )
                .thenReturn( mockSocialComponent );

        assertNotNull( SocialASRPUtils.getSocialOperationResult( mockSlingHttpServletRequest, HttpServletResponse.SC_OK,
                ClassMagsMigrationASRPConstants.HTTP_SUCCESS_MESSAGE, mockResource, mockScfManager,
                BookmarkSocialComponent.BOOKMARK_RESOURCE_TYPE ) );
    }

    /**
     * Testcreate social node.
     *
     * @throws PersistenceException
     *             the persistence exception
     */
    @Test
    public void testcreateSocialNode() throws PersistenceException {

        when( mockSrp.getResource( mockResourceResolver, TEST_PATH ) ).thenReturn( null );
        when( mockSrp.create( mockResourceResolver, TEST_PATH, mockPropsMap ) ).thenReturn( mockResource );
        SocialASRPUtils.createSocialNode( mockSrp, mockResourceResolver, TEST_PATH );
    }
}
