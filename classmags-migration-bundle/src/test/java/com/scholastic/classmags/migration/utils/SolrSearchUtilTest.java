package com.scholastic.classmags.migration.utils;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Iterator;
import java.util.List;

import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.solr.client.solrj.response.IntervalFacet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith( PowerMockRunner.class )
@PrepareForTest( { IntervalFacet.Count.class, IntervalFacet.class } )
public class SolrSearchUtilTest {

    private static final String TEST_DATA_1 = "Sample1";
    private static final String TEST_DATA_2 = "Sample2";
    private static final String TEST_DATA_3 = "Sample3";
    private static final String TEST_DATA_4 = "Sample4&+(){}[]^~*?:";
    private static final String SOLR_OR_CONSTRUCTION_TEST = "(Sample1 OR Sample2 OR Sample3)";
    private static final String TEST_DATA_5 = "{\"key\":\"Key1\",\"count\":1}";
    private static final String TEST_DATA_6 = "{\"key\":\"Key2\",\"count\":2}";
    private static final String ESCAPE_SOLR_CHARS_TEST = "Sample4&\\+\\(\\)\\{\\}\\[\\]\\^\\~\\*\\?\\:";
    private static final String ESCAPE_CHARS_TEST = "Sample1 \"Sample2\" &+(){} ";
    private static final String TEST_DATA_7 = "Sample1 \"Sample2\" &+(){} ";
    @Mock
    private List< IntervalFacet.Count > intervalFacetEntries;

    @Mock
    private IntervalFacet.Count mockCount1;

    @Mock
    private IntervalFacet.Count mockCount2;

    private JSONArray intervalFacetField;

    @Mock
    private Iterator< IntervalFacet.Count > countIterator;

    @Before
    public void setup() {

        intervalFacetField = new JSONArray();
    }

    @Test
    public void solrOrConstructionTest() {
        assertEquals( SOLR_OR_CONSTRUCTION_TEST,
                SolrSearchUtil.solrOrConstruction( TEST_DATA_1, TEST_DATA_2, TEST_DATA_3 ) );
        assertEquals( "", SolrSearchUtil.solrOrConstruction() );
    }

    @Test
    public void getIntervalFacetCountByFieldTest() {
        when( intervalFacetEntries.iterator() ).thenReturn( countIterator );
        when( countIterator.hasNext() ).thenReturn( true, true, false );
        when( countIterator.next() ).thenReturn( mockCount1 ).thenReturn( mockCount2 );
        when( mockCount1.getKey() ).thenReturn( "Key1" );
        when( mockCount1.getCount() ).thenReturn( 1 );
        when( mockCount2.getKey() ).thenReturn( "Key2" );
        when( mockCount2.getCount() ).thenReturn( 2 );

        try {
            intervalFacetField = SolrSearchUtil.getIntervalFacetCountByField( intervalFacetEntries );
            Assert.assertEquals( TEST_DATA_5, intervalFacetField.getJSONObject( 0 ).toString() );
            Assert.assertEquals( TEST_DATA_6, intervalFacetField.getJSONObject( 1 ).toString() );
        } catch ( JSONException e ) {
            e.printStackTrace();
        }
    }

    @Test
    public void escapeCharsTest() {
        assertEquals( SolrSearchUtil.escapeChars( TEST_DATA_7 ), ESCAPE_CHARS_TEST );

    }

    @Test
    public void escapeSolrCharactersTest() {
        assertEquals( ESCAPE_SOLR_CHARS_TEST, SolrSearchUtil.escapeSolrCharacters( TEST_DATA_4 ) );
    }

}
