package com.scholastic.classmags.migration.servlets;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import java.util.HashMap;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.scholastic.classmags.migration.models.SearchResponse;
import com.scholastic.classmags.migration.services.ClassMagsQueryService;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * The Class FetchUserRoleServletTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { SearchServlet.class, RoleUtil.class } )
public class SearchServletTest {

    private SearchServlet searchServlet;
    private SlingHttpServletRequest request;
    private SlingHttpServletResponse response;
    private MagazineProps magazineProps;
    private ClassMagsQueryService queryService;
    private SearchResponse searchResponse;
    private PrintWriter printWriter;

    /** The logger. */
    private Logger logger;

    @SuppressWarnings( "unchecked" )
    @Before
    public void setUp() throws Exception {
        searchServlet = Whitebox.newInstance( SearchServlet.class );

        logger = mock( Logger.class );
        request = mock( SlingHttpServletRequest.class );
        response = mock( SlingHttpServletResponse.class );
        magazineProps = mock( MagazineProps.class );
        queryService = mock( ClassMagsQueryService.class );
        printWriter = mock( PrintWriter.class );
        searchResponse = mock( SearchResponse.class );

        PowerMockito.mockStatic( RoleUtil.class );

        Whitebox.setInternalState( SearchServlet.class, "LOG", logger );
        Whitebox.setInternalState( searchServlet, magazineProps );
        Whitebox.setInternalState( searchServlet, queryService );

        when( request.getParameter( "text" ) ).thenReturn( "Sample Text" );
        when( request.getParameter( "path" ) )
                .thenReturn( "/content/scholastic/classroom-magazines/issues/2016-17/pups-patrol.html" );
        when( RoleUtil.getUserRole( request ) ).thenReturn( "teacher" );
        when( magazineProps.getMagazineName( any( String.class ) ) ).thenReturn( "ScienceWorld" );
        when( queryService.getSearchResults( any( HashMap.class ), any( Boolean.class ), any( String.class ) ) )
                .thenReturn( searchResponse );
        when( response.getWriter() ).thenReturn( printWriter );
    }

    @Test
    public void testSearchServlet() throws Exception {
        // execute logic
        Whitebox.invokeMethod( searchServlet, "doGet", request, response );

        // verify logic
        verify( response ).setContentType( "application/json" );
    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testSearchServletWhenSearchResultIsNull() throws Exception {
        // setup logic ( test-specific )
        when( request.getParameter( "text" ) ).thenReturn( null );
        when( queryService.getSearchResults( any( HashMap.class ), any( Boolean.class ), any( String.class ) ) )
                .thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( searchServlet, "doGet", request, response );

        // verify logic
        verify( printWriter ).write( "{}" );
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final SearchServlet searchServlet = new SearchServlet();

        // verify logic
        assertNotNull( searchServlet );
    }
}