package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Dictionary;

import org.apache.sling.api.resource.LoginException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.osgi.service.component.ComponentContext;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.MagazinePropsConstants;

/**
 * JUnit for MagazinePropsImpl.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { MagazinePropsImpl.class } )
public class MagazinePropsImplTest {

    private static final String CURRENT_PATH = "/content/classroom_magazines/scienceworld/homepage/ISSUE-PAGE-2";
    private static final String MAGAZINE_NAME = "scienceworld";

    /** The magazine props service. */
    private MagazineProps magazineProps;
    
    /** Component context. */
    private ComponentContext componentContext;
    
    /** Dictionary. */
    @SuppressWarnings( "rawtypes" )
    private Dictionary dictionary;
    
    /** The logger. */
    private Logger logger;

    
    /**
     * Initial Setup.
     * @throws Exception 
     * 
     * @throws LoginException
     */
    @Before
    public void setUp(){
        
        magazineProps = new MagazinePropsImpl();
        
        componentContext = mock(ComponentContext.class);
        dictionary = mock(Dictionary.class);
        logger = mock( Logger.class );
        
        Whitebox.setInternalState( MagazinePropsImpl.class, "LOG", logger );
        
        when(componentContext.getProperties()).thenReturn( dictionary );
        
    }

    /**
     * Test get magazine name.
     * 
     * @throws Exception 
     */
    @Test
    public void testGetMagazineName() throws Exception {
        // setup logic (test-specific)
        when( dictionary.get( MagazinePropsConstants.SCIENCE_WORLD ) )
                .thenReturn( MagazinePropsConstants.SCIENCE_WORLD );
        Whitebox.invokeMethod( magazineProps, "activate", componentContext );

        // execute logic
        String magazineName = magazineProps.getMagazineName( CURRENT_PATH );

        // verify logic
        assertEquals( MAGAZINE_NAME, magazineName );
    }
    
    /**
     * Test get magazine name when no magazine are added.
     * 
     * @throws Exception
     */
    @Test
    public void testGetMagazineNameWithNoMagazines() throws Exception {
        // setup logic (test-specific)
        Whitebox.invokeMethod( magazineProps, "activate", componentContext );

        // execute logic
        String magazineName = magazineProps.getMagazineName( CURRENT_PATH );

        // verify logic
        assertTrue( magazineName.isEmpty() );
    }
    
    /**
     * Test deactivate method.
     * 
     * @throws Exception 
     */
    @Test
    public void testDeactivate() throws Exception{

        // execute logic
        Whitebox.invokeMethod( magazineProps, "deactivate" );

        // verify logic
       verify(logger).debug( "************************DEACTIVATE METHOD CALLED*******************************" );
    }
}
