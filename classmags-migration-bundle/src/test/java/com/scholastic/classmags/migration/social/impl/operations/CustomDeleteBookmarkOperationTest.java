package com.scholastic.classmags.migration.social.impl.operations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.adobe.cq.social.scf.SocialComponentFactoryManager;
import com.adobe.cq.social.scf.SocialOperationResult;
import com.scholastic.classmags.migration.social.api.BookmarkService;
import com.scholastic.classmags.migration.social.api.BookmarkSocialComponent;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;

/**
 * The Class CustomDeleteBookmarkOperationTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { CustomDeleteBookmarkOperation.class, SocialASRPUtils.class } )
public class CustomDeleteBookmarkOperationTest {

    /** The custom delete bookmark operation. */
    private CustomDeleteBookmarkOperation customDeleteBookmarkOperation;

    /** The mock sling http servlet request. */
    @Mock
    private SlingHttpServletRequest mockSlingHttpServletRequest;

    /** The mock bookmark service. */
    @Mock
    private BookmarkService mockBookmarkService;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock scf manager. */
    @Mock
    private SocialComponentFactoryManager mockScfManager;

    /** The mock social operation result. */
    @Mock
    private SocialOperationResult mockSocialOperationResult;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        customDeleteBookmarkOperation = new CustomDeleteBookmarkOperation();

        Whitebox.setInternalState( customDeleteBookmarkOperation, mockBookmarkService );
        Whitebox.setInternalState( customDeleteBookmarkOperation, mockScfManager );

        PowerMockito.mockStatic( SocialASRPUtils.class );
        when( SocialASRPUtils.getSocialOperationResult( mockSlingHttpServletRequest, HttpServletResponse.SC_OK,
                ClassMagsMigrationASRPConstants.HTTP_SUCCESS_MESSAGE, mockResource, mockScfManager,
                BookmarkSocialComponent.BOOKMARK_RESOURCE_TYPE ) ).thenReturn( mockSocialOperationResult );

    }

    /**
     * Test perform operation.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPerformOperation() throws Exception {

        when( mockBookmarkService.deleteUserBookmarkResource( mockSlingHttpServletRequest ) )
                .thenReturn( mockResource );

        assertEquals( mockSocialOperationResult,
                customDeleteBookmarkOperation.performOperation( mockSlingHttpServletRequest ) );
    }
}
