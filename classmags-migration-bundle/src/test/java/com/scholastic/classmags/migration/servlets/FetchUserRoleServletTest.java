package com.scholastic.classmags.migration.servlets;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.scholastic.classmags.migration.models.UIdHeaderIdentifiers;
import com.scholastic.classmags.migration.models.UIdHeaderOrgContext;
import com.scholastic.classmags.migration.models.UIdOrgContextSubHeaderEntitlements;
import com.scholastic.classmags.migration.models.UIdOrgContextSubHeaderOrganization;
import com.scholastic.classmags.migration.models.UserIdCookieData;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CookieUtil;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * The Class FetchUserRoleServletTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { FetchUserRoleServlet.class, RoleUtil.class, CookieUtil.class } )
public class FetchUserRoleServletTest {

    private static final String STAFF_ID = "214064";
    private static final String STUDENT_ID = "214065";
    private static final String ORG_ID = "770";
    private static final String MAGAZINE_NAME_KEY = "magazineName";
    private static final String SCIENCE_WORLD = "scienceworld";
    private static final String SUPER_SCIENCE = "superscience";
    private static final String CONTENT_TYPE_JSON = "application/json";

    private FetchUserRoleServlet fetchUserRoleServlet;
    private SlingHttpServletRequest request;
    private SlingHttpServletResponse response;
    private PrintWriter printWriter;
    private UserIdCookieData userIdCookie;

    /** The logger. */
    private Logger logger;

    @Before
    public void setUp() throws Exception {
        fetchUserRoleServlet = Whitebox.newInstance( FetchUserRoleServlet.class );

        logger = mock( Logger.class );
        request = mock( SlingHttpServletRequest.class );
        response = mock( SlingHttpServletResponse.class );
        printWriter = mock( PrintWriter.class );

        PowerMockito.mockStatic( RoleUtil.class );
        PowerMockito.mockStatic( CookieUtil.class );

        userIdCookie = mockUserIdCookie();

        Whitebox.setInternalState( FetchUserRoleServlet.class, "LOG", logger );

        when( RoleUtil.getUserRole( request ) ).thenReturn( ClassMagsMigrationConstants.USER_TYPE_TEACHER );
        when( response.getWriter() ).thenReturn( printWriter );
        when( request.getParameter( MAGAZINE_NAME_KEY ) ).thenReturn( SCIENCE_WORLD );
        when( CookieUtil.fetchUserIdCookieData( request ) ).thenReturn( userIdCookie );
    }

    @Test
    public void testFetchUserRoleServlet() throws Exception {

        // execute logic
        Whitebox.invokeMethod( fetchUserRoleServlet, "doGet", request, response );

        // verify logic
        verify( response ).setContentType( CONTENT_TYPE_JSON );

    }

    @Test
    public void testFetchUserRoleServletWhenUserIdCookieIsNotPresent() throws Exception {
        // setup logic ( test-specific )
        when( CookieUtil.fetchUserIdCookieData( request ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( fetchUserRoleServlet, "doGet", request, response );

        // verify logic
        verify( response ).setContentType( CONTENT_TYPE_JSON );
    }

    @Test
    public void testFetchUserRoleServletWhenUserIsAnonymous() throws Exception {
        // setup logic ( test-specific )
        when( RoleUtil.getUserRole( request ) ).thenReturn( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS );

        // execute logic
        Whitebox.invokeMethod( fetchUserRoleServlet, "doGet", request, response );

        // verify logic
        verify( response ).setContentType( CONTENT_TYPE_JSON );
    }

    @Test
    public void testFetchUserRoleServletWhenUserIsStudent() throws Exception {
        // setup logic ( test-specific )
        when( RoleUtil.getUserRole( request ) ).thenReturn( ClassMagsMigrationConstants.USER_TYPE_STUDENT );

        // execute logic
        Whitebox.invokeMethod( fetchUserRoleServlet, "doGet", request, response );

        // verify logic
        verify( response ).setContentType( CONTENT_TYPE_JSON );
    }

    @Test
    public void testFetchUserRoleServletWhenSubscriptionStatusIsFalse() throws Exception {
        // setup logic ( test-specific )
        when( request.getParameter( MAGAZINE_NAME_KEY ) ).thenReturn( SUPER_SCIENCE );

        // execute logic
        Whitebox.invokeMethod( fetchUserRoleServlet, "doGet", request, response );

        // verify logic
        verify( response ).setContentType( CONTENT_TYPE_JSON );
    }

    @Test
    public void testFetchUserRoleServletWhenJSONExceptionOccurs() throws Exception {
        // setup logic ( test-specific )
        JSONException jsonException = new JSONException( "JSON Exception" );
        JSONObject userData = mock( JSONObject.class );

        whenNew( JSONObject.class ).withNoArguments().thenReturn( userData );

        when( userData.put( "loginStatus", true ) ).thenThrow( jsonException );

        // execute logic
        Whitebox.invokeMethod( fetchUserRoleServlet, "doGet", request, response );

        // verify logic
        verify( logger ).error( "Error fetching user information {}", jsonException );
    }

    @Test
    public void testFetchUserRoleServletWhenJSONExceptionOccursForRoleJSON() throws Exception {
        // setup logic ( test-specific )
        JSONException jsonException = new JSONException( "JSON Exception" );
        JSONObject roleJSON = mock( JSONObject.class );

        whenNew( JSONObject.class ).withNoArguments().thenReturn( roleJSON );

        when( roleJSON.put( "role", ClassMagsMigrationConstants.USER_TYPE_TEACHER ) ).thenThrow( jsonException );

        // execute logic
        Whitebox.invokeMethod( fetchUserRoleServlet, "doGet", request, response );

        // verify logic
        verify( logger ).error( "Error fetching user information {}", jsonException );
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final FetchUserRoleServlet fetchUserRoleServlet = new FetchUserRoleServlet();

        // verify logic
        assertNotNull( fetchUserRoleServlet );
    }

    private UserIdCookieData mockUserIdCookie() {
        UserIdCookieData userIdCookie = mock( UserIdCookieData.class );
        UIdHeaderIdentifiers uidHeaderIdentifiers = mock( UIdHeaderIdentifiers.class );
        UIdHeaderOrgContext uidHeaderOrgContext = mock( UIdHeaderOrgContext.class );
        UIdOrgContextSubHeaderOrganization uidOrgContextSubHeaderOrganization = mock(
                UIdOrgContextSubHeaderOrganization.class );

        List< UIdOrgContextSubHeaderEntitlements > entitlements = new ArrayList< >();
        UIdOrgContextSubHeaderEntitlements entitlement1 = mock( UIdOrgContextSubHeaderEntitlements.class );
        UIdOrgContextSubHeaderEntitlements entitlement2 = mock( UIdOrgContextSubHeaderEntitlements.class );

        when( entitlement1.getApplicationCode() ).thenReturn( SCIENCE_WORLD );
        when( entitlement1.getStatus() ).thenReturn( "true" );
        when( entitlement2.getApplicationCode() ).thenReturn( SUPER_SCIENCE );
        when( entitlement2.getStatus() ).thenReturn( "false" );

        entitlements.add( entitlement1 );
        entitlements.add( entitlement2 );

        when( userIdCookie.getObjUIdHeaderIdentifiers() ).thenReturn( uidHeaderIdentifiers );

        when( uidHeaderIdentifiers.getStaffId() ).thenReturn( STAFF_ID );
        when( uidHeaderIdentifiers.getStudentId() ).thenReturn( STUDENT_ID );

        when( userIdCookie.getObjUIdHeaderOrgContext() ).thenReturn( uidHeaderOrgContext );
        when( uidHeaderOrgContext.getObjUIdOrgContextSubHeaderOrganization() )
                .thenReturn( uidOrgContextSubHeaderOrganization );
        when( uidOrgContextSubHeaderOrganization.getOrgId() ).thenReturn( ORG_ID );
        when( uidHeaderOrgContext.getObjListUIdOrgContextSubHeaderEntitlements() ).thenReturn( entitlements );

        return userIdCookie;
    }
}
