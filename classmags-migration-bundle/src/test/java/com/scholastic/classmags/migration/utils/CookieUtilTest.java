package com.scholastic.classmags.migration.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Constructor;
import java.net.URLEncoder;

import javax.servlet.http.Cookie;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.commons.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.scholastic.classmags.migration.models.UserIdCookieData;

/**
 * JUnit for CookieUtil.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { CookieUtil.class, URLEncoder.class, EncryptionUtils.class } )
public class CookieUtilTest {

    private String name = "custom";
    private String payload = "some random payload";
    private String domain = ".scholatic.com";
    private String path = "/";
    private boolean secure = true;
    private boolean encrypt = true;
    private boolean notEncrypted = false;
    private Logger logger;
    private SlingHttpServletRequest request;
    private String SAMPLE_ENCRYPTED_VALUE = "95zIWQi7Dk4PaBB5JMRzy57eK4HsnXLVuShed95bY4vjHbZRypf5Bh7PEZyz0s65qxp1Ldtg9Yy3xgV0HqxeMUxHV3oHTwkZR0Bvway"
            + "S72Uq23wkAeTM8E9hMfzn5lPFOq9aZmO0plw2P9RtLGNBdepUY3fDcPZkYWH1rFSP9SKCdepBOSIy5n4Br_1wqoSGaVhJe8Rk"
            + "8sOLZgN4iEPJCbiMZb2QVWfoLKULmcDZjEmxWzhsvHve_CId9AXEPK6JTJ2_bv1TntQmG-cLyl4VVyHO4GsZEvHaxECpDm2rL7he"
            + "7Lj3aWcWO8Xsaex3m_8GA_Y2dchXC2KDjUT9_r3K_vsPU7tAkMGxqINrJRSLT0pZ9_ObDAzZN6CNyodXdpYkIENd1Sj07nw7sFiRvB6Aj0Ezc"
            + "LFs0SnZxC7SzslR1uNFwZ60t-51x3_FbTmf2bvmgjtBgS8JOt9M9vPInqMqNEavEouxWzL2T2JWziPU1FdLT1hb7oJ2MUeqxLDB9UybZztwjpz"
            + "QxUkUFYILbEKtYfY5WbCTtin5aE5arG_vtEU1";
    private String SAMPLE_DECRYPTED_VALUE = "{\"objUIdHeaderIdentifiers\":{\"staffId\":\"214064\",\"studentId\":\"\"},\"objUIdHeaderLaunchContext\":{\"role\":\"teacher\",\"objUIdLaunchContextSubHeaderApplication\":"
            + "{\"applicationCode\":\"junior\"}},\"objUIdHeaderOrgContext\":{\"objUIdOrgContextSubHeaderOrganization\":"
            + "{\"orgId\":\"498\"},\"objListUIdOrgContextSubHeaderEntitlements\":[{\"applicationCode\":\"scienceworld\","
            + "\"status\":\"true\"},{\"applicationCode\":\"junior\",\"status\":\"true\"}]}}";
    private String CUSTOM_LAUNCH = "{\"identifiers\":{\"staffId\":\"214064\"},"
            + "\"orgContext\":{\"organization\":{\"id\":\"770\",\"name\":\"KLONDIKEMIDDLESCHOOL\","
            + "\"parent\":\"771\",\"orgType\":\"school\",\"identifiers\":{\"ucn\":\"600054102\","
            + "\"sourceId\":\"1615\",\"source\":\"SPS\"},\"lastModifiedDate\":\"2016-11-22T11:37:50.000-0500\","
            + "\"address\":{\"city\":\"WLAFAYETTE\",\"state\":\"IN\",\"zipCode\":\"47906\",\"address1\":\"3307KLONDIKERD\"},"
            + "\"countryCode\":\"USA\",\"currentSchoolYear\":\"2016\",\"overrideSchoolYearStart\":false,\"schoolStart\":{\"monthOfYear\":8,\"dayOfMonth\":1}},"
            + "\"entitlements\":[{\"id\":\"10\",\"name\":\"ScienceWorld\",\"applicationCode\":\"scienceworld\","
            + "\"url\":\"https://scienceworld-aem-qa.scholastic.com/bin/classmags/migration/credManager\","
            + "\"thumbnail\":\"//digital.scholastic.com/media/applications/thumbnails/scienceworld_logo.jpg\","
            + "\"active\":true,\"groupCode\":\"CM\"}],\"currentSchoolCalendar\":{\"description\":\"2016-2017SchoolYear\","
            + "\"startDate\":\"2016-08-01\",\"endDate\":\"2017-07-31\"},\"roles\":[\"teacher\"]},\"launchContext\":{\"application\":{\"id\":\"10\",\"name\":"
            + "\"ScienceWorld\",\"applicationCode\":\"scienceworld\","
            + "\"url\":\"https://scienceworld-aem-qa.scholastic.com/bin/classmags/migration/credManager\","
            + "\"thumbnail\":\"//digital.scholastic.com/media/applications/thumbnails/scienceworld_logo.jpg\","
            + "\"active\":true,\"groupCode\":\"CM\"},\"productEntitlements\":[{\"id\":\"40\",\"name\":\"ScienceWorld\","
            + "\"productCode\":\"cmscienceworld\",\"applicationId\":\"10\",\"active\":true,\"includesStudentAccess\":true,"
            + "\"teacherDirectedSubscriptions\":{\"enabled\":true}}],\"role\":\"teacher\"},\"teacherContext\":{\"classes\":[{\"id\":\"37528\","
            + "\"organizationId\":\"770\",\"staff\":{\"primaryTeacherId\":\"214064\"},\"nickname\":\"MyClass\","
            + "\"lowGrade\":\"4\",\"highGrade\":\"6\",\"active\":true,\"schoolYear\":\"2016\",\"studentAuthenticationMethod\":"
            + "{\"type\":\"easyLogin\",\"enabled\":false,\"requirePassword\":false},\"identifiers\":{},\"displayName\":\"MyClass\"},"
            + "{\"id\":\"38199\",\"organizationId\":\"770\",\"staff\":{\"primaryTeacherId\":\"214064\"},\"nickname\":\"SecondClass\","
            + "\"lowGrade\":\"4\",\"highGrade\":\"5\",\"active\":true,\"schoolYear\":\"2016\","
            + "\"studentAuthenticationMethod\":{\"type\":\"easyLogin\",\"enabled\":false,\"requirePassword\":false},"
            + "\"identifiers\":{},\"displayName\":\"SecondClass\"}]},\"sessionContext\":{\"sessionType\":\"DAS\","
            + "\"sessionToken\":\"d60b6c35-83c6-494b-bdc0-4770451c7265\"}}";

    @Before
    public void setUp() {
        logger = mock( Logger.class );
        request = mock( SlingHttpServletRequest.class );
        Whitebox.setInternalState( CookieUtil.class, "LOGGER", logger );

        PowerMockito.mockStatic( EncryptionUtils.class );
    }

    /**
     */
    @Test
    public void testSecureEncryptedCookie() {

        // execute logic
        Cookie cookie = CookieUtil.createCookie( name, payload, domain, path, secure, encrypt );

        // verify logic
        assertNotNull( cookie );
        assertTrue( cookie.getSecure() );
        assertEquals( EncryptionUtils.encrypt( payload ), cookie.getValue() );
        assertEquals( domain, cookie.getDomain() );
        assertEquals( path, cookie.getPath() );
    }

    @Test
    public void testSecureUnEncryptedCookie() throws UnsupportedEncodingException {

        // execute logic
        Cookie cookie = CookieUtil.createCookie( name, payload, domain, path, secure, notEncrypted );

        // verify logic
        assertNotNull( cookie );
        assertTrue( cookie.getSecure() );

        assertEquals( URLEncoder.encode( payload, "UTF-8" ).replaceAll( "\\+", "%20" ), cookie.getValue() );
        assertEquals( domain, cookie.getDomain() );
        assertEquals( path, cookie.getPath() );
    }

    @Test
    public void testSecureUnEncryptedCookieWhenEncoderExceptionOccurs() throws UnsupportedEncodingException {
        // setup logic ( test-specific )
        PowerMockito.mockStatic( URLEncoder.class );
        UnsupportedEncodingException e = new UnsupportedEncodingException();

        when( URLEncoder.encode( payload, "UTF-8" ) ).thenThrow( e );

        // execute logic
        CookieUtil.createCookie( name, payload, domain, path, secure, notEncrypted );

        // verify logic
        verify( logger ).error( "Error encoding cookie value, {}", e );
    }

    @Test
    public void testFetchUserCookieData() {
        // setup logic
        Cookie swUserIdCookie = mock( Cookie.class );
        when( swUserIdCookie.getValue() ).thenReturn( SAMPLE_ENCRYPTED_VALUE );
        when( request.getCookie( CmSDMConstants.SW_USER_ID_COOKIE_NAME ) ).thenReturn( swUserIdCookie );
        when( EncryptionUtils.decrypt( SAMPLE_ENCRYPTED_VALUE ) ).thenReturn( SAMPLE_DECRYPTED_VALUE );

        // execute logic
        UserIdCookieData userIdCookieData = CookieUtil.fetchUserIdCookieData( request );

        // verify logic
        assertNotNull( userIdCookieData );
        assertEquals( "214064", userIdCookieData.getObjUIdHeaderIdentifiers().getStaffId() );
    }

    @Test
    public void testFetchUserCookieDataWhenRequestIsNull() {
        // execute logic
        UserIdCookieData userIdCookieData = CookieUtil.fetchUserIdCookieData( null );

        // verify logic
        assertNull( userIdCookieData );
    }

    @Test
    public void testFetchUserCookieDataWhenUserIdCookieIsNull() {
        // setup logic ( test-specific )
        when( request.getCookie( CmSDMConstants.SW_USER_ID_COOKIE_NAME ) ).thenReturn( null );

        // execute logic
        UserIdCookieData userIdCookieData = CookieUtil.fetchUserIdCookieData( request );

        // verify logic
        assertNull( userIdCookieData );
    }

    @Test
    public void testFetchUserCookieDataWhenDecryptedDataIsBlank() {
        // setup logic ( test-specific )
        Cookie swUserIdCookie = mock( Cookie.class );
        when( swUserIdCookie.getValue() ).thenReturn( SAMPLE_ENCRYPTED_VALUE );
        when( request.getCookie( CmSDMConstants.SW_USER_ID_COOKIE_NAME ) ).thenReturn( swUserIdCookie );
        when( EncryptionUtils.decrypt( SAMPLE_ENCRYPTED_VALUE ) ).thenReturn( StringUtils.EMPTY );

        // execute logic
        UserIdCookieData userIdCookieData = CookieUtil.fetchUserIdCookieData( request );

        // verify logic
        assertNull( userIdCookieData );
    }

    @Test
    public void testMapRequestParamCustomDpLaunch() throws JSONException {

        // execute logic
        UserIdCookieData userIdCookie = CookieUtil.mapRequestParamCustomDpLaunch( CUSTOM_LAUNCH );

        // verify logic
        assertNotNull( userIdCookie );
        
        assertEquals( "scienceworld", userIdCookie.getObjUIdHeaderLaunchContext().getObjUIdLaunchContextSubHeaderApplication().getApplicationCode());
    }
    
    @Test
    public void testPrivateConstructor() throws Exception {
        Constructor< ? >[] constructor = CookieUtil.class.getDeclaredConstructors();
        
        constructor[ 0 ].setAccessible( true );
        constructor[ 0 ].newInstance( ( Object[] ) null );

        assertEquals( 1, constructor.length );
        
    }
}
