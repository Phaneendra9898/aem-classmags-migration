package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Iterator;
import java.util.List;

import javax.script.Bindings;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.commons.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.ClassmagsMigrationErrorCodes;
import com.scholastic.classmags.migration.models.IssueHighlight;
import com.scholastic.classmags.migration.services.IssueHighlightDataService;

/**
 * JUnit for IssueHighlightsUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { IssueHighlightsUse.class } )
public class IssueHighlightsUseTest {

    /** The Constant PATH. */
    private static final String PATH = "/testPath";

    /** The Constant TEST_SORTING_DATE. */
    private static final String TEST_SORTING_DATE = "October 25, 2016";
    
    /** The Constant TEST_DISPLAY_DATE. */
    private static final String TEST_DISPLAY_DATE = "Oct/Nov 2016";

    /** The Constant TEST_KEY_ASSETS_LABEL. */
    private static final String TEST_KEY_ASSETS_LABEL = "issueKeyAssetsLabel";

    /** The Constant TEST_KEY_ASSETS_PATH. */
    private static final String TEST_KEY_ASSETS_PATH = "issueKeyAssetsLabelPath";

    /** The Constant TEST_DATA. */
    private static final String TEST_DATA = "testData";

    /** The Constant TEST_KEY_USER_TYPE. */
    private static final String TEST_KEY_USER_TYPE = "userType";

    /** The Constant TEST_USER_TYPE_VALUE. */
    private static final String TEST_USER_TYPE_VALUE = "everyone";

    /** The issue highlights use. */
    private IssueHighlightsUse issueHighlightsUse;

    /** The bindings. */
    @Mock
    private Bindings bindings;

    /** The sling script helper. */
    @Mock
    private SlingScriptHelper slingScriptHelper;

    /** The article text data service. */
    @Mock
    private IssueHighlightDataService issueHighlightDataService;

    /** The page. */
    @Mock
    private Page page;

    /** The asset data map list. */
    @Mock
    private List< ValueMap > assetDataMapList;

    /** The asset data map. */
    @Mock
    private ValueMap assetDataMap;

    /** The iterator. */
    @Mock
    private Iterator< ValueMap > iterator;

    /** The request. */
    @Mock
    private SlingHttpServletRequest request;
    
    /**The resource. */
    @Mock
    private Resource resource;
    
    /** The issue highlight. */
    @Mock
    private IssueHighlight issueHighlight;

    /** The logger. */
    @Mock
    private Logger logger;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        issueHighlightsUse = new IssueHighlightsUse();
        Whitebox.setInternalState( IssueHighlightsUse.class, "LOG", logger );
        
        when( request.getResource() ).thenReturn( resource );
        when( resource.getPath() ).thenReturn( PATH );
    }

    /**
     * Test get sorting date.
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     * @throws JSONException 
     */
    @Test
    public void testGetSortingDate() throws ClassmagsMigrationBaseException, JSONException {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( page.getPath() ).thenReturn( PATH );
        when( slingScriptHelper.getService( IssueHighlightDataService.class ) ).thenReturn( issueHighlightDataService );
        when( issueHighlightDataService.fetchIssueSortingDate( PATH ) ).thenReturn( TEST_SORTING_DATE );

        issueHighlightsUse.init( bindings );
        issueHighlightsUse.activate();
        assertEquals( TEST_SORTING_DATE, issueHighlightsUse.getSortingDate() );
    }

    /**
     * Test get sorting date with exception.
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     * @throws JSONException 
     */
    @Test
    public void testGetSortingDateWithException() throws ClassmagsMigrationBaseException, JSONException {

        ClassmagsMigrationBaseException classmagsMigrationBaseException = new ClassmagsMigrationBaseException(
                ClassmagsMigrationErrorCodes.LOGIN_ERROR );

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( page.getPath() ).thenReturn( PATH );
        when( slingScriptHelper.getService( IssueHighlightDataService.class ) ).thenReturn( issueHighlightDataService );

        issueHighlightsUse.init( bindings );

        when( issueHighlightDataService.fetchIssueSortingDate( PATH ) ).thenThrow( classmagsMigrationBaseException );

        issueHighlightsUse.activate();
        assertEquals( null, issueHighlightsUse.getSortingDate() );
        verify( logger ).error( classmagsMigrationBaseException.getMessage(), classmagsMigrationBaseException );
    }

    /**
     * Test get issue highlight.
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     * @throws JSONException 
     */
    @Test
    public void testGetIssueHighlight() throws ClassmagsMigrationBaseException, JSONException {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( page.getPath() ).thenReturn( PATH );
        when( slingScriptHelper.getService( IssueHighlightDataService.class ) ).thenReturn( issueHighlightDataService );
        when( issueHighlightDataService.fetchIssueSortingDate( PATH ) ).thenReturn( TEST_SORTING_DATE );

        issueHighlightsUse.init( bindings );
        issueHighlightsUse.activate();
        issueHighlightsUse.setIssueHighlight( issueHighlight );
        assertEquals( issueHighlight, issueHighlightsUse.getIssueHighlight() );
        assertEquals( "anonymous", issueHighlightsUse.getUserType() );
    }

    /**
     * Test get asset data list.
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     * @throws JSONException 
     */
    @Test
    public void testGetAssetDataList() throws ClassmagsMigrationBaseException, JSONException {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( page.getPath() ).thenReturn( PATH );
        when( request.getAttribute( "role" ) ).thenReturn( TEST_USER_TYPE_VALUE );
        when( slingScriptHelper.getService( IssueHighlightDataService.class ) ).thenReturn( issueHighlightDataService );
        when( issueHighlightDataService.fetchIssueSortingDate( PATH ) ).thenReturn( TEST_SORTING_DATE );
        when( issueHighlightDataService.fetchAssetData( PATH ) ).thenReturn( assetDataMapList );
        when( assetDataMapList.iterator() ).thenReturn( iterator );

        issueHighlightsUse.init( bindings );

        when( iterator.hasNext() ).thenReturn( true, true, false );
        when( iterator.next() ).thenReturn( assetDataMap );
        when( assetDataMap.get( TEST_KEY_ASSETS_LABEL, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( assetDataMap.get( TEST_KEY_ASSETS_PATH, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( assetDataMap.get( TEST_KEY_USER_TYPE, StringUtils.EMPTY ) ).thenReturn( TEST_USER_TYPE_VALUE );

        issueHighlightsUse.activate();
        assertEquals( TEST_DATA, issueHighlightsUse.getAssetDataList().get( 0 ).getIssueKeyAssetsLabel() );
        assertEquals( TEST_USER_TYPE_VALUE, issueHighlightsUse.getAssetDataList().get( 0 ).getUserType() );
    }

    /**
     * Test activate with no sling script helper.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testActivateWithNoSlingScriptHelper() throws Exception {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( null );
        when( page.getPath() ).thenReturn( PATH );

        issueHighlightsUse.init( bindings );
        issueHighlightsUse.activate();
        assertEquals( null, issueHighlightsUse.getSortingDate() );
    }

    /**
     * Test activate with no service.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testActivateWithNoService() throws Exception {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( page.getPath() ).thenReturn( PATH );
        when( slingScriptHelper.getService( IssueHighlightDataService.class ) ).thenReturn( null );

        issueHighlightsUse.init( bindings );
        issueHighlightsUse.activate();
        assertEquals( null, issueHighlightsUse.getSortingDate() );
    }
    
    /**
     * Test get display date.
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     * @throws JSONException 
     */
    @Test
    public void testGetDisplayDate() throws ClassmagsMigrationBaseException, JSONException {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( page.getPath() ).thenReturn( PATH );
        when( slingScriptHelper.getService( IssueHighlightDataService.class ) ).thenReturn( issueHighlightDataService );
        when( issueHighlightDataService.fetchIssueDisplayDate( PATH ) ).thenReturn( TEST_DISPLAY_DATE );

        issueHighlightsUse.init( bindings );
        issueHighlightsUse.activate();
        assertEquals( TEST_DISPLAY_DATE, issueHighlightsUse.getDisplayDate() );
    }
    
    /**
     * Test get issue bookmark path.
     * @throws JSONException 
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    @Test
    public void testGetIssueBookmarkPath() throws JSONException {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( page.getPath() ).thenReturn( PATH );

        issueHighlightsUse.init( bindings );
        issueHighlightsUse.activate();

        assertEquals( PATH, issueHighlightsUse.getIssueBookmarkPath() );
    }
}
