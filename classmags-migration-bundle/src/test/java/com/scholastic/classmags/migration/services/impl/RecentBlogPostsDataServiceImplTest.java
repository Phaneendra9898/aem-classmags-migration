package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class RecentBlogPostsDataServiceImplTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { RecentBlogPostsDataServiceImpl.class, CommonUtils.class } )
public class RecentBlogPostsDataServiceImplTest {

    /** The content tile service. */
    private RecentBlogPostsDataServiceImpl recentBlogPostsDataService;

    /** The Constant PATH. */
    private static final String TEST_PATH = "/testPath";

    /** The Constant TEST_DATA. */
    private static final String TEST_DATA = "testData";

    /** The Constant TEST_DATE_FORMAT. */
    private static final String TEST_DATE_FORMAT = "dd/mm/YYYY";

    /** The resource resolver factory. */
    @Mock
    private ResourceResolverFactory mockResourceResolverFactory;

    /** The resource path config service. */
    @Mock
    private ResourcePathConfigService mockresourcePathConfigService;

    /** The resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock blog posts path list. */
    @Mock
    private List< ValueMap > mockBlogPostsPathList;

    /** The mock property config service. */
    @Mock
    private PropertyConfigService mockPropertyConfigService;

    /** The mock iterator. */
    @Mock
    private Iterator< ValueMap > mockIterator;

    /** The mock blog posts path map. */
    @Mock
    private ValueMap mockBlogPostsPathMap;

    /** The mock magazine props. */
    @Mock
    private MagazineProps mockMagazineProps;

    /**
     * Sets the up.
     */
    @Before
    public void setUp() {
        recentBlogPostsDataService = new RecentBlogPostsDataServiceImpl();
        Whitebox.setInternalState( recentBlogPostsDataService, mockResourceResolverFactory );
        Whitebox.setInternalState( recentBlogPostsDataService, mockresourcePathConfigService );
        Whitebox.setInternalState( recentBlogPostsDataService, mockPropertyConfigService );
        Whitebox.setInternalState( recentBlogPostsDataService, mockMagazineProps );

        PowerMockito.mockStatic( CommonUtils.class );
        when( CommonUtils.getResourceResolver( mockResourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ) )
                .thenReturn( mockResourceResolver );
    }

    /**
     * Test fetch recent blog posts data.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testFetchRecentBlogPostsData() throws Exception {

        when( mockResourceResolver.getResource( TEST_PATH ) ).thenReturn( mockResource );

        when( CommonUtils.fetchMultiFieldData( mockResource, ClassMagsMigrationConstants.BLOG_POSTS_PATH_NODE ) )
                .thenReturn( mockBlogPostsPathList );
        when( mockBlogPostsPathList.iterator() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( mockBlogPostsPathMap );
        when( mockBlogPostsPathMap.get( ClassMagsMigrationConstants.BLOG_PATH_PROPERTY, StringUtils.EMPTY ) )
                .thenReturn( TEST_PATH );
        when( mockMagazineProps.getMagazineName( TEST_PATH ) ).thenReturn( "scienceworld" );
        when( CommonUtils.fetchPagePropertyMap( TEST_PATH, mockResourceResolverFactory ) )
                .thenReturn( mockBlogPostsPathMap );
        when( mockBlogPostsPathMap.get( ClassMagsMigrationConstants.PP_TITLE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( CommonUtils.getDataPath( mockresourcePathConfigService, "scienceworld" ) ).thenReturn( TEST_PATH );
        when( mockPropertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                TEST_PATH ) ).thenReturn( TEST_DATE_FORMAT );
        when( CommonUtils.getDisplayDate( mockBlogPostsPathMap, TEST_DATE_FORMAT ) ).thenReturn( TEST_DATA );

        assertEquals( false, recentBlogPostsDataService.fetchRecentBlogPostsData( TEST_PATH ).isEmpty() );
    }

    /**
     * Test fetch recent blog posts data with exception.
     *
     * @throws Exception
     *             the exception
     */
    @Test( expected = ClassmagsMigrationBaseException.class )
    public void testFetchRecentBlogPostsDataWithException() throws Exception {

        LoginException loginException = new LoginException();

        when( mockResourceResolver.getResource( TEST_PATH ) ).thenReturn( mockResource );

        when( CommonUtils.fetchMultiFieldData( mockResource, ClassMagsMigrationConstants.BLOG_POSTS_PATH_NODE ) )
                .thenReturn( mockBlogPostsPathList );
        when( mockBlogPostsPathList.iterator() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( mockBlogPostsPathMap );
        when( mockBlogPostsPathMap.get( ClassMagsMigrationConstants.BLOG_PATH_PROPERTY, StringUtils.EMPTY ) )
                .thenReturn( TEST_PATH );
        when( mockMagazineProps.getMagazineName( TEST_PATH ) ).thenReturn( "scienceworld" );
        when( CommonUtils.fetchPagePropertyMap( TEST_PATH, mockResourceResolverFactory ) )
                .thenReturn( mockBlogPostsPathMap );
        when( mockBlogPostsPathMap.get( ClassMagsMigrationConstants.PP_TITLE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( CommonUtils.getDataPath( mockresourcePathConfigService, "scienceworld" ) ).thenReturn( TEST_PATH );
        when( mockPropertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                TEST_PATH ) ).thenThrow( loginException );

        assertEquals( null, recentBlogPostsDataService.fetchRecentBlogPostsData( TEST_PATH ) );
    }
}
