package com.scholastic.classmags.migration.utils;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Test;

import com.scholastic.classmags.migration.exception.OAuthException;

/*
 * OAuthUtil is a legacy class, it should be refactored into a component that can be injected
 */
public class OAuthUtilTest {

    private static final String ANY_INVALID_CHARACTER_ENCODING = "ANY_INVALID_CHARACTER_ENCODING";
    private static final String SOME_VALID_CHARACTERS_TO_ENCODE = "some + characters * , ~";
    private static final String EMPTY_STRING = "";
    
    @After
    public void tearDown() {
        OAuthUtil.setDefaultEncoding(null);
    }

    @Test
    public void shouldReturnAnEmptyStringWhenPercentEncodeParameterIsNull() throws Exception {
        String percentEncode = OAuthUtil.percentEncode(null);
        assertNotNull("Percent Encode is Null", percentEncode);
        assertTrue("Percent Encode is not an EmptyString", percentEncode.equals(EMPTY_STRING));
    }

    @Test
    public void shouldEncodeSomeCharactersDiffertlyAsItIsImplemented() throws Exception {
        String percentEncode = OAuthUtil.percentEncode(SOME_VALID_CHARACTERS_TO_ENCODE);
        assertNotNull("Percent Encode is Null", percentEncode);
        assertTrue("Doesn't contain %20", percentEncode.contains("%20"));
        assertTrue("Doesn't contain %2A", percentEncode.contains("%2A"));
        assertTrue("Doesn't contain ~", percentEncode.contains("~"));
    }

    @Test(expected = OAuthException.class)
    public void shouldThrowOAuthExceptionwhenEncodingParamtersIsNotValid() throws Exception {
        OAuthUtil.setDefaultEncoding(ANY_INVALID_CHARACTER_ENCODING);
        OAuthUtil.percentEncode(SOME_VALID_CHARACTERS_TO_ENCODE);
    }

}
