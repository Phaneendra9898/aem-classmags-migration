package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.util.ArrayList;
import java.util.List;

import javax.jcr.Node;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.services.IssueService;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for ScholasticDataLayerServiceImpl
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { ScholasticDataLayerServiceImpl.class, CommonUtils.class } )
public class ScholasticDataLayerServiceImplTest {

    private static final String ARTICLE_PAGE_PATH = "/content/classroom_magazines/scienceworld/issues/2016-17/090516/pup-on-patrol";
    private static final String ISSUE_PAGE_PATH = "/content/classroom_magazines/scienceworld/issues/2016-17/090516";
    private static final String ARCHIVE_PAGE_PATH = "/content/classroom_magazines/scienceworld/pages/archives/all-issues";
    private static final String SEARCH_PAGE_PATH = "/content/classroom_magazines/scienceworld/pages/search-results";
    private static final String MAGAZINE_NAME = "scienceworld";

    private ScholasticDataLayerServiceImpl scholasticDataLayerService;
    private SlingHttpServletRequest request;
    private Page currentPage;
    private ResourceResolverFactory resourceResolverFactory;
    private MagazineProps magazineProps;
    private IssueService issueService;
    private ResourceResolver resourceResolver;
    private TagManager tagManager;
    private Resource resource;
    private Node node;
    private List< String > tags;
    private Logger logger;
    private ValueMap valueMap;

    /**
     * Initial setup.
     */
    @Before
    public void setUp() {
        scholasticDataLayerService = new ScholasticDataLayerServiceImpl();

        logger = mock( Logger.class );
        request = mock( SlingHttpServletRequest.class );
        currentPage = mock( Page.class );
        resourceResolverFactory = mock( ResourceResolverFactory.class );
        magazineProps = mock( MagazineProps.class );
        issueService = mock( IssueService.class );
        resourceResolver = mock( ResourceResolver.class );
        tagManager = mock( TagManager.class );
        resource = mock( Resource.class );
        node = mock( Node.class );
        valueMap = mock (ValueMap.class);

        tags = new ArrayList< >();
        tags.add( "Biology" );
        tags.add( "Earth Science" );

        PowerMockito.mockStatic( CommonUtils.class );

        Whitebox.setInternalState( scholasticDataLayerService, resourceResolverFactory );
        Whitebox.setInternalState( scholasticDataLayerService, magazineProps );
        Whitebox.setInternalState( scholasticDataLayerService, issueService );
        Whitebox.setInternalState( ScholasticDataLayerServiceImpl.class, "LOGGER", logger );

        when( currentPage.getPath() ).thenReturn( ARTICLE_PAGE_PATH );
        when( magazineProps.getMagazineName( ARTICLE_PAGE_PATH ) ).thenReturn( MAGAZINE_NAME );
        when( CommonUtils.getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ) )
                .thenReturn( resourceResolver );
        when( CommonUtils.getTagManager( resourceResolverFactory ) ).thenReturn( tagManager );
        when( resourceResolver.getResource( ARTICLE_PAGE_PATH + "/" + JcrConstants.JCR_CONTENT ) )
                .thenReturn( resource );
        when( resource.adaptTo( Node.class ) ).thenReturn( node );
        when( CommonUtils.propertyCheck( ClassMagsMigrationConstants.ASSET_USER_TYPE, node ) ).thenReturn( "teacher" );
        when( CommonUtils.propertyCheck( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, node ) )
                .thenReturn( ClassMagsMigrationConstants.ARTICLE_PAGE_TEMPLATE_PATH );
        when( CommonUtils.getSubjectTags( resource, tagManager ) ).thenReturn( tags );
        when( issueService.getParentIssuePath( request, currentPage ) ).thenReturn( ISSUE_PAGE_PATH + ".html" );
    }

    /**
     * Test create data layer for article page.
     */
    @Test
    public void testCreateDataLayerForArticlePage() {
        // execute logic
        JSONObject dumbleData = scholasticDataLayerService.createDataLayer( request, currentPage );

        // verify logic
        assertNotNull( dumbleData );
        assertEquals( "cm:scienceworld", dumbleData.optJSONObject( "domain" ).optString( "name" ) );
    }

    /**
     * Test create data layer for issue page.
     */
    @Test
    public void testCreateDataLayerForIssuePage() {
        // setup logic
        when( currentPage.getPath() ).thenReturn( ISSUE_PAGE_PATH );
        when( resourceResolver.getResource( ISSUE_PAGE_PATH + "/" + JcrConstants.JCR_CONTENT ) ).thenReturn( resource );
        when( resource.getValueMap() ).thenReturn( valueMap );
        when( resource.getValueMap().get("cmmarketing", String.class) ).thenReturn( "true" );
        when( magazineProps.getMagazineName( ISSUE_PAGE_PATH ) ).thenReturn( MAGAZINE_NAME );
        when( resourceResolver.getResource( ISSUE_PAGE_PATH + "/" + JcrConstants.JCR_CONTENT ) ).thenReturn( resource );
        when( CommonUtils.propertyCheck( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, node ) )
                .thenReturn( ClassMagsMigrationConstants.ISSUE_PAGE_TEMPLATE_PATH );

        // execute logic
        JSONObject dumbleData = scholasticDataLayerService.createDataLayer( request, currentPage );

        // verify logic
        assertNotNull( dumbleData );
        assertEquals( "cm:marketing", dumbleData.optJSONObject( "domain" ).optString( "name" ) );
        assertEquals( "2016-17/090516", dumbleData.optJSONObject( "page" ).optString( "name" ) );
        assertEquals( "issue", dumbleData.optJSONObject( "page" ).optString( "template" ) );
    }

    /**
     * Test create data layer for archive page.
     */
    @Test
    public void testCreateDataLayerForArchivePage() {
        // setup logic
        when( currentPage.getPath() ).thenReturn( ARCHIVE_PAGE_PATH );
        when( magazineProps.getMagazineName( ARCHIVE_PAGE_PATH ) ).thenReturn( MAGAZINE_NAME );
        when( resourceResolver.getResource( ARCHIVE_PAGE_PATH + "/" + JcrConstants.JCR_CONTENT ) )
                .thenReturn( resource );
        when( CommonUtils.propertyCheck( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, node ) )
                .thenReturn( ClassMagsMigrationConstants.ARCHIVE_PAGE_TEMPLATE_PATH );

        // execute logic
        JSONObject dumbleData = scholasticDataLayerService.createDataLayer( request, currentPage );

        // verify logic
        assertNotNull( dumbleData );
        assertEquals( "cm:scienceworld",
                dumbleData.optJSONObject( "domain" ).optString( "name" ) );
        assertEquals( "archive", dumbleData.optJSONObject( "page" ).optString( "name" ) );
        assertEquals( "archive",
                dumbleData.optJSONObject( "page" ).optString( "template" ) );
    }

    /**
     * Test create data layer for base page.
     */
    @Test
    public void testCreateDataLayerForBasePage() {
        // setup logic
        when( currentPage.getPath() ).thenReturn( SEARCH_PAGE_PATH );
        when( magazineProps.getMagazineName( SEARCH_PAGE_PATH ) ).thenReturn( MAGAZINE_NAME );
        when( resourceResolver.getResource( SEARCH_PAGE_PATH + "/" + JcrConstants.JCR_CONTENT ) )
                .thenReturn( resource );
        when( CommonUtils.propertyCheck( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, node ) )
                .thenReturn( "/apps/scholastic/classroom-magazines-migration/templates/base-page" );

        // execute logic
        JSONObject dumbleData = scholasticDataLayerService.createDataLayer( request, currentPage );

        // verify logic
        assertTrue( dumbleData.optJSONObject( "page" ).optString( "name" ).isEmpty() );
        assertTrue( dumbleData.optJSONObject( "page" ).optString( "template" ).isEmpty() );

        assertNull( dumbleData.optJSONObject( "article" ) );
    }

    /**
     * Test create data layer with page resource as null.
     */
    @Test
    public void testCreateDataLayerWithPageResourceAsNull() {
        // setup logic ( test-specific )
        when( resourceResolver.getResource( ARTICLE_PAGE_PATH + "/" + JcrConstants.JCR_CONTENT ) ).thenReturn( null );
        when( CommonUtils.propertyCheck( ClassMagsMigrationConstants.ASSET_USER_TYPE, null ) )
                .thenReturn( StringUtils.EMPTY );
        when( CommonUtils.propertyCheck( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, null ) )
                .thenReturn( StringUtils.EMPTY );

        // execute logic
        JSONObject dumbleData = scholasticDataLayerService.createDataLayer( request, currentPage );

        // verify logic
        assertNotNull( dumbleData );
        assertEquals( "cm:scienceworld", dumbleData.optJSONObject( "domain" ).optString( "name" ) );
        assertTrue( dumbleData.optJSONObject( "page" ).optString( "template" ).isEmpty() );
    }

    /**
     * Test create data layer for article with no parent issue.
     */
    @Test
    public void testCreateDataLayerForArticleWithNoParentIssue() {
        // setup logic ( test-specific )
        when( issueService.getParentIssuePath( request, currentPage ) ).thenReturn( StringUtils.EMPTY );

        // execute logic
        JSONObject dumbleData = scholasticDataLayerService.createDataLayer( request, currentPage );

        // verify logic
        assertNotNull( dumbleData );
        assertEquals( "cm:scienceworld", dumbleData.optJSONObject( "domain" ).optString( "name" ) );
        assertTrue( dumbleData.optString( "issue" ).isEmpty() );
    }

    /**
     * Test create data layer with JSON exception.
     * 
     * @throws Exception
     */
    @Test
    public void testCreateDataLayerWithJSONException() throws Exception {
        // setup logic ( test-specific )
        JSONException jsonException = new JSONException( "JSON Exception" );
        JSONObject digitalData = mock( JSONObject.class );
        JSONObject domain = mock( JSONObject.class );

        whenNew( JSONObject.class ).withNoArguments().thenReturn( digitalData, domain );
        when( domain.put( "name", "cm:scienceworld" ) ).thenThrow( jsonException );

        // execute logic
        scholasticDataLayerService.createDataLayer( request, currentPage );

        // verify logic
        verify( logger ).error( "JSONException occured during creating data layer :: " + jsonException );
    }

}
