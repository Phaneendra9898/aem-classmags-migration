package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.script.Bindings;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.apache.sling.settings.SlingSettingsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.scholastic.classmags.migration.models.SearchFilter;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for SearchResultsUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { SearchResultsUse.class, CommonUtils.class } )
public class SearchResultsUseTest {

    /** The Constant TEST_DATA. */
    private static final String TEST_DATA = "testData";

    /** The Constant TEST_PATH. */
    private static final String TEST_PATH = "/testpath";

    /** The search results use. */
    private SearchResultsUse searchResultsUse;

    /** The request. */
    @Mock
    private SlingHttpServletRequest mockRequest;

    /** The bindings. */
    @Mock
    private Bindings mockBindings;

    /** The sling script helper. */
    @Mock
    private SlingScriptHelper mockSlingScriptHelper;

    /** The magazine props. */
    @Mock
    private MagazineProps magazineProps;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;

    /** The mock resource resolver factory. */
    @Mock
    private ResourceResolverFactory mockResourceResolverFactory;

    /** The mock page manager. */
    @Mock
    private PageManager mockPageManager;

    /** The mock page. */
    @Mock
    private Page mockPage;

    /** The mock run modes. */
    @Mock
    private Set< String > mockRunModes;

    /** The mock auth info. */
    @Mock
    private Map< String, Object > mockAuthInfo;

    /** The mock subject filters. */
    @Mock
    private List< SearchFilter > mockSubjectFilters;

    /** The mock sling settings service. */
    @Mock
    private SlingSettingsService mockSlingSettingsService;

    /** The logger. */
    @Mock
    private Logger logger;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        searchResultsUse = new SearchResultsUse();
        PowerMockito.mockStatic( CommonUtils.class );
        mockAuthInfo = new HashMap< >();
        mockAuthInfo.put( "sling.service.subservice", ( Object ) "readservice" );
        Whitebox.setInternalState( SearchResultsUse.class, "LOG", logger );
        when( CommonUtils.getSearchFilters( mockResourceResolver,
                ClassMagsMigrationConstants.CLASS_MAGS_TAG_ROOT_PATH + TEST_DATA ) ).thenReturn( mockSubjectFilters );
        when( CommonUtils.getContentTypeFilters( mockResource, mockRequest, true ) ).thenReturn( mockSubjectFilters );
    }

    /**
     * Test get subject filters.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetSubjectFilters() throws Exception {

        mockRunModes.add( "author" );
        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockBindings.get( "currentPage" ) ).thenReturn( mockPage );
        when( mockBindings.get( "resource" ) ).thenReturn( mockResource );
        when( mockSlingScriptHelper.getService( SlingSettingsService.class ) ).thenReturn( mockSlingSettingsService );
        when( mockSlingSettingsService.getRunModes() ).thenReturn( mockRunModes );
        when( mockSlingScriptHelper.getService( MagazineProps.class ) ).thenReturn( magazineProps );
        when( mockPage.getPath() ).thenReturn( TEST_PATH );
        when( magazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockSlingScriptHelper.getService( ResourceResolverFactory.class ) )
                .thenReturn( mockResourceResolverFactory );
        when( mockResourceResolverFactory.getServiceResourceResolver( mockAuthInfo ) )
                .thenReturn( mockResourceResolver );
        when( mockResource.getChild( ClassMagsMigrationConstants.NODE_NAME_CONTENT_TYPES ) ).thenReturn( mockResource );
        when( mockResourceResolver.isLive() ).thenReturn( true );

        searchResultsUse.init( mockBindings );
        searchResultsUse.activate();
        assertEquals( false, searchResultsUse.getSubjectFilters().isEmpty() );
    }

    /**
     * Test get content filters.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetContentFilters() throws Exception {

        mockRunModes.add( "author" );
        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockBindings.get( "currentPage" ) ).thenReturn( mockPage );
        when( mockBindings.get( "resource" ) ).thenReturn( mockResource );
        when( mockSlingScriptHelper.getService( SlingSettingsService.class ) ).thenReturn( mockSlingSettingsService );
        when( mockSlingSettingsService.getRunModes() ).thenReturn( mockRunModes );
        when( mockRunModes.contains( "author" ) ).thenReturn( true );
        when( mockSlingScriptHelper.getService( MagazineProps.class ) ).thenReturn( magazineProps );
        when( mockPage.getPath() ).thenReturn( TEST_PATH );
        when( magazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockSlingScriptHelper.getService( ResourceResolverFactory.class ) )
                .thenReturn( mockResourceResolverFactory );
        when( mockResourceResolverFactory.getServiceResourceResolver( mockAuthInfo ) )
                .thenReturn( mockResourceResolver );
        when( mockResource.getChild( ClassMagsMigrationConstants.NODE_NAME_CONTENT_TYPES ) ).thenReturn( mockResource );
        when( mockResourceResolver.isLive() ).thenReturn( true );

        searchResultsUse.init( mockBindings );
        searchResultsUse.activate();
        assertEquals( false, searchResultsUse.getContentFilters().isEmpty() );
    }

    /**
     * Test get current page path.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetCurrentPagePath() throws Exception {

        mockRunModes.add( "author" );
        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockBindings.get( "currentPage" ) ).thenReturn( mockPage );
        when( mockBindings.get( "resource" ) ).thenReturn( mockResource );
        when( mockSlingScriptHelper.getService( SlingSettingsService.class ) ).thenReturn( mockSlingSettingsService );
        when( mockSlingSettingsService.getRunModes() ).thenReturn( mockRunModes );
        when( mockSlingScriptHelper.getService( MagazineProps.class ) ).thenReturn( magazineProps );
        when( mockPage.getPath() ).thenReturn( TEST_PATH );
        when( magazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockSlingScriptHelper.getService( ResourceResolverFactory.class ) )
                .thenReturn( mockResourceResolverFactory );
        when( mockResourceResolverFactory.getServiceResourceResolver( mockAuthInfo ) )
                .thenReturn( mockResourceResolver );
        when( mockResource.getChild( ClassMagsMigrationConstants.NODE_NAME_CONTENT_TYPES ) ).thenReturn( mockResource );
        when( mockResourceResolver.isLive() ).thenReturn( true );

        searchResultsUse.init( mockBindings );
        searchResultsUse.activate();
        assertEquals( TEST_PATH, searchResultsUse.getCurrentPagePath() );
    }

    /**
     * Test get subject filters with exception.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetSubjectFiltersWithException() throws Exception {

        LoginException loginException = new LoginException();

        mockRunModes.add( "author" );
        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockBindings.get( "currentPage" ) ).thenReturn( mockPage );
        when( mockBindings.get( "resource" ) ).thenReturn( mockResource );
        when( mockSlingScriptHelper.getService( SlingSettingsService.class ) ).thenReturn( mockSlingSettingsService );
        when( mockSlingSettingsService.getRunModes() ).thenReturn( mockRunModes );
        when( mockSlingScriptHelper.getService( MagazineProps.class ) ).thenReturn( magazineProps );
        when( mockPage.getPath() ).thenReturn( TEST_PATH );
        when( magazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockSlingScriptHelper.getService( ResourceResolverFactory.class ) )
                .thenReturn( mockResourceResolverFactory );
        when( mockResourceResolverFactory.getServiceResourceResolver( mockAuthInfo ) ).thenThrow( loginException );

        searchResultsUse.init( mockBindings );
        searchResultsUse.activate();
        assertEquals( true, searchResultsUse.getSubjectFilters().isEmpty() );
        verify( logger, times( 2 ) ).error( "Error getting service resource resolver : {}", loginException );
    }

    /**
     * Test get subject filters with close resource resolver.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetSubjectFiltersWithCloseResourceResolver() throws Exception {

        mockRunModes.add( "author" );
        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockBindings.get( "currentPage" ) ).thenReturn( mockPage );
        when( mockBindings.get( "resource" ) ).thenReturn( mockResource );
        when( mockSlingScriptHelper.getService( SlingSettingsService.class ) ).thenReturn( mockSlingSettingsService );
        when( mockSlingSettingsService.getRunModes() ).thenReturn( mockRunModes );
        when( mockSlingScriptHelper.getService( MagazineProps.class ) ).thenReturn( magazineProps );
        when( mockPage.getPath() ).thenReturn( TEST_PATH );
        when( magazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockSlingScriptHelper.getService( ResourceResolverFactory.class ) )
                .thenReturn( mockResourceResolverFactory );
        when( mockResourceResolverFactory.getServiceResourceResolver( mockAuthInfo ) )
                .thenReturn( mockResourceResolver );
        when( mockResource.getChild( ClassMagsMigrationConstants.NODE_NAME_CONTENT_TYPES ) ).thenReturn( mockResource );
        when( mockResourceResolver.isLive() ).thenReturn( false );

        searchResultsUse.init( mockBindings );
        searchResultsUse.activate();
        assertEquals( false, searchResultsUse.getSubjectFilters().isEmpty() );
    }
}
