package com.scholastic.classmags.migration.social.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import javax.jcr.RepositoryException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.xss.XSSAPI;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.adobe.cq.social.scf.ClientUtilities;
import com.adobe.cq.social.srp.SocialResourceProvider;
import com.adobe.cq.social.srp.config.SocialResourceConfiguration;
import com.adobe.cq.social.ugc.api.SearchResults;
import com.adobe.cq.social.ugc.api.UgcFilter;
import com.adobe.cq.social.ugc.api.UgcSearch;
import com.adobe.cq.social.ugcbase.SocialUtils;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;

/**
 * The Class VotingSocialComponentImplTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { VotingSocialComponentImpl.class, SocialASRPUtils.class } )
public class VotingSocialComponentImplTest {

    /** The voting social component. */
    private VotingSocialComponentImpl votingSocialComponent;

    /** The Constant TEST_APP_NAME. */
    private static final String TEST_APP_NAME = "scienceworld";

    /** The Constant TEST_PATH. */
    private static final String TEST_PATH = "testPath";

    /** The Constant TEST_DATA_TEACHER. */
    private static final String TEST_DATA_TEACHER = "{\"userIdentifier\":\"249261\",\"userRole\":\"teacher\",\"questionUUID\":\"1111100000\",\"disableVotingDate\":\"1493317800000\",\"votePagePath\":\"testPath\",\"answerOptions\":{\"Yes\":\"1\",\"No\":\"1\"}}";

    /** The mock sling http servlet request. */
    @Mock
    private SlingHttpServletRequest mockSlingHttpServletRequest;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;

    /** The mock social utils. */
    @Mock
    private SocialUtils mockSocialUtils;

    /** The mock srp. */
    @Mock
    private SocialResourceProvider mockSrp;

    /** The mock social resource configuration. */
    @Mock
    private SocialResourceConfiguration mockSocialResourceConfiguration;

    /** The mock xssapi. */
    @Mock
    private XSSAPI mockXssapi;

    /** The mock property map. */
    @Mock
    private ValueMap mockPropertyMap;

    /** The mock client utils. */
    @Mock
    private ClientUtilities mockClientUtils;

    /** The mock ugc search. */
    @Mock
    private UgcSearch mockUgcSearch;

    /** The mock results. */
    @Mock
    private SearchResults< Resource > mockResults;

    /** The answer options array. */
    private String[] answerOptionsArray;

    /**
     * Test get answer options map.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetAnswerOptionsMap() throws Exception {

        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.OPERATION ) )
                .thenReturn( ClassMagsMigrationASRPConstants.SOCIAL_SUBMIT_VOTE_OPERATION );
        when( mockXssapi.filterHTML( ClassMagsMigrationASRPConstants.SOCIAL_SUBMIT_VOTE_OPERATION ) )
                .thenReturn( ClassMagsMigrationASRPConstants.SOCIAL_SUBMIT_VOTE_OPERATION );

        setup();

        votingSocialComponent = new VotingSocialComponentImpl( mockResource, mockClientUtils, mockUgcSearch, mockXssapi,
                mockSocialUtils );

        assertEquals( "20", votingSocialComponent.getAnswerOptionsMap().get( "Yes" ) );
        assertEquals( "20", votingSocialComponent.getAnswerOptionsMap().get( "No" ) );
    }

    /**
     * Test is voting enabled.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testIsVotingEnabled() throws Exception {

        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.OPERATION ) )
                .thenReturn( ClassMagsMigrationASRPConstants.SOCIAL_GET_VOTE_DATA_OPERATION );
        when( mockXssapi.filterHTML( ClassMagsMigrationASRPConstants.SOCIAL_GET_VOTE_DATA_OPERATION ) )
                .thenReturn( ClassMagsMigrationASRPConstants.SOCIAL_GET_VOTE_DATA_OPERATION );

        setup();

        votingSocialComponent = new VotingSocialComponentImpl( mockResource, mockClientUtils, mockUgcSearch, mockXssapi,
                mockSocialUtils );

        assertEquals( false, votingSocialComponent.isVotingEnabled() );
    }

    /**
     * Setup.
     *
     * @throws Exception
     *             the exception
     * @throws RepositoryException
     *             the repository exception
     */
    private void setup() throws Exception, RepositoryException {

        answerOptionsArray = new String[] { "Yes", "No" };

        UgcFilter mockUGCfilter = PowerMockito.mock( UgcFilter.class );
        PowerMockito.whenNew( UgcFilter.class ).withNoArguments().thenReturn( mockUGCfilter );
        when( mockClientUtils.getRequest() ).thenReturn( mockSlingHttpServletRequest );
        PowerMockito.mockStatic( SocialASRPUtils.class );
        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.VOTE_DATA ) )
                .thenReturn( TEST_DATA_TEACHER );
        when( mockResource.getValueMap() ).thenReturn( mockPropertyMap );
        when( mockPropertyMap.get( ClassMagsMigrationASRPConstants.JSON_KEY_ANSWER_OPTIONS ) )
                .thenReturn( answerOptionsArray );
        when( mockResource.getResourceType() ).thenReturn( ClassMagsMigrationASRPConstants.RESOURCE_TYPE_CLOUD_SOCIAL );
        when( mockResource.getName() ).thenReturn( TEST_APP_NAME );
        when( mockSocialUtils.getSocialResourceProvider( mockResource ) ).thenReturn( mockSrp );
        when( mockSocialUtils.getDefaultStorageConfig() ).thenReturn( mockSocialResourceConfiguration );
        when( mockResource.getPath() ).thenReturn( TEST_PATH );
        when( mockResource.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( SocialASRPUtils.getUGCResourceFromPath( mockResourceResolver, TEST_PATH + "/" + "yes", mockSrp ) )
                .thenReturn( mockResource );
        when( SocialASRPUtils.getUGCResourceFromPath( mockResourceResolver, TEST_PATH + "/" + "no", mockSrp ) )
                .thenReturn( mockResource );
        when( mockUgcSearch.find( null, mockResourceResolver, mockUGCfilter,
                ClassMagsMigrationASRPConstants.SOCIAL_SEARCH_OFFSET,
                ClassMagsMigrationASRPConstants.SOCIAL_SEARCH_INTIAL_LIMIT, true ) ).thenReturn( mockResults );
        when( mockResults.getTotalNumberOfResults() ).thenReturn( ( long ) 20 );
    }
}
