package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import javax.script.Bindings;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.day.cq.commons.Externalizer;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

@RunWith( PowerMockRunner.class )
@PrepareForTest( { GlobalLinkUse.class, InternalURLFormatter.class } )
public class GlobalLinkUseTest extends BaseComponentTest {

    private static final String MAGAZINE_NAME = "scienceworld";
    private static final String URL = "/content/classroom_magazines/scienceworld/issues/2016-17/090516";
    private static final String FORMATTED_URL = "/content/classroom_magazines/scienceworld/issues/2016-17/090516.html";
    private static final String EXTERNALIZED_URL = "https://scienceworld-aem-dev.scholastic.com/content/classroom_magazines/scienceworld/issues/2016-17/090516";

    private GlobalLinkUse globalLinkUse;
    private Externalizer externalizer;
    private MagazineProps magazineProps;

    private Bindings bindings;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        globalLinkUse = Whitebox.newInstance( GlobalLinkUse.class );

        stubCommon( globalLinkUse );

        bindings = mock( Bindings.class );
        externalizer = mock( Externalizer.class );
        magazineProps = mock( MagazineProps.class );
        PowerMockito.mockStatic( InternalURLFormatter.class );

		when(bindings.get("url")).thenReturn(URL);
		when(slingScriptHelper.getService(MagazineProps.class)).thenReturn(magazineProps);
		when(currentPage.getPath()).thenReturn(URL);
		when(magazineProps.getMagazineName(URL)).thenReturn(MAGAZINE_NAME);
		when(InternalURLFormatter.formatURL(resourceResolver, URL)).thenReturn(FORMATTED_URL);
		when(slingScriptHelper.getService(Externalizer.class)).thenReturn(externalizer);
		when(externalizer.externalLink(resourceResolver, MAGAZINE_NAME, URL)).thenReturn(EXTERNALIZED_URL);
    }

    @Test
    public void testCreateInstance() {
        // execute logic
        globalLinkUse.init( bindings );
        final GlobalLinkUse globalLinkUse = new GlobalLinkUse();

        // verify logic
        assertNotNull( globalLinkUse );

    }

    @Test
    public void testGlobalLinkUse() throws Exception {
        // execute logic
        globalLinkUse.init( bindings );
        Whitebox.invokeMethod( globalLinkUse, "activate" );

        // verify logic
        assertEquals( FORMATTED_URL, globalLinkUse.getFormattedUrl() );
        assertEquals( EXTERNALIZED_URL, globalLinkUse.getExternalizedDomain() );
    }
    
    @Test
    public void testGlobalLinkUseWhenSlingScriptHelperIsNull() throws Exception {
        // setup logic( test-specific )
        stub( PowerMockito.method( GlobalLinkUse.class, "getSlingScriptHelper" ) ).toReturn( null );
        when( externalizer.externalLink( resourceResolver, StringUtils.EMPTY, URL ) )
                .thenReturn( "http://defaultDomain.com" + URL );

        // execute logic
        globalLinkUse.init( bindings );
        Whitebox.invokeMethod( globalLinkUse, "activate" );

        // verify logic
        assertEquals( FORMATTED_URL, globalLinkUse.getFormattedUrl() );
    }

    @Test
    public void testGlobalLinkUseWhenMagazinePropsServiceIsNull() throws Exception {
        // setup logic( test-specific )
        when(slingScriptHelper.getService( MagazineProps.class )).thenReturn( null );

        // execute logic
        globalLinkUse.init( bindings );
        Whitebox.invokeMethod( globalLinkUse, "activate" );

        // verify logic
        assertEquals( FORMATTED_URL, globalLinkUse.getFormattedUrl() );
    }
    
    @Test
    public void testGlobalLinkUseWhenExternalizerIsNull() throws Exception {
        // setup logic( test-specific )
        when( slingScriptHelper.getService( Externalizer.class ) ).thenReturn( null );

        // execute logic
        globalLinkUse.init( bindings );
        Whitebox.invokeMethod( globalLinkUse, "activate" );

        // verify logic
        assertEquals( FORMATTED_URL, globalLinkUse.getFormattedUrl() );
        assertNull( globalLinkUse.getExternalizedDomain() );
    }
    
    @Test
    public void testGlobalLinkUseWhenUrlIsNull() throws Exception {
        // setup logic( test-specific )
        when( bindings.get( "url" ) ).thenReturn( null );
        when( InternalURLFormatter.formatURL( resourceResolver, null )).thenReturn( null );
        
        // execute logic
        globalLinkUse.init( bindings );
        Whitebox.invokeMethod( globalLinkUse, "activate" );

        // verify logic
        assertNull( globalLinkUse.getFormattedUrl() );
        assertNull( globalLinkUse.getExternalizedDomain() );
    }
    
}
