package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * Junit for EReaderUse
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { EReaderUse.class, RoleUtil.class } )
public class EReaderUseTest extends BaseComponentTest {

    private static final String EREADER_PATH = "/content/dam/classroom-magazines/scienceworld/issues/2016-17/090516/epub/40-090516/9780545796293";
    private static final String MAGAZINE_ISBN = "40-090516";

    private EReaderUse eReaderUse;
    private ValueMap valueMap;
    private String userType;

    /**
     * Setup.
     * 
     * @throws LoginException
     */
    @Before
    public void setup() throws LoginException {
        eReaderUse = Whitebox.newInstance( EReaderUse.class );
        stubCommon( eReaderUse );

        PowerMockito.mockStatic( RoleUtil.class );
        valueMap = mock( ValueMap.class );
        userType = "teacher";

        when( currentPage.getContentResource() ).thenReturn( resource );
        when( resource.getChild( "ereader-configuration" ) ).thenReturn( resource );
        when( resource.getValueMap() ).thenReturn( valueMap );
        when( valueMap.get( "linkToEReaderNew", StringUtils.EMPTY ) ).thenReturn( EREADER_PATH );
        when( RoleUtil.getUserRole( request ) ).thenReturn( "teacher" );
        when( resource.getValueMap() ).thenReturn( valueMap );
        when( valueMap.get( "userType", StringUtils.EMPTY ) ).thenReturn( userType );
        when( RoleUtil.shouldRender( userType, userType ) ).thenReturn( true );

    }

    @Test
    public void testEReaderUse() throws Exception {

        // execute logic
        Whitebox.invokeMethod( eReaderUse, "activate" );

        // verify logic
        assertEquals( EREADER_PATH, eReaderUse.getNewEReaderPath() );
        assertEquals( MAGAZINE_ISBN, eReaderUse.getEPubISBN() );

        assertTrue( eReaderUse.getShowMagazineLink() );
    }

    @Test
    public void testEReaderUseWhenPageIsNull() throws Exception {
        // setup logic ( test-specific )
        stub( PowerMockito.method( eReaderUse.getClass(), "getCurrentPage" ) ).toReturn( null );

        // execute logic
        Whitebox.invokeMethod( eReaderUse, "activate" );

        // verify logic
        assertNull( eReaderUse.getNewEReaderPath() );
        assertNull( eReaderUse.getEPubISBN() );
    }

    @Test
    public void testEReaderUseWhenResourceIsNull() throws Exception {

        // setup logic ( test-specific )
        when( resource.getChild( "ereader-configuration" ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( eReaderUse, "activate" );

        // verify logic
        assertNull( eReaderUse.getNewEReaderPath() );
        assertNull( eReaderUse.getEPubISBN() );

    }

    @Test
    public void testEReaderUseWhenNewEReaderPathIsEmpty() throws Exception {
        // setup logic( test-specific )
        when( valueMap.get( "linkToEReaderNew", StringUtils.EMPTY ) ).thenReturn( StringUtils.EMPTY );

        // execute logic
        Whitebox.invokeMethod( eReaderUse, "activate" );

        // verify logic
        assertNull( eReaderUse.getEPubISBN() );
        assertTrue( eReaderUse.getNewEReaderPath().isEmpty() );
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final EReaderUse eReaderUse = new EReaderUse();

        // verify logic
        assertNotNull( eReaderUse );
    }

}
