package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.adobe.granite.ui.components.ds.DataSource;
import com.adobe.granite.ui.components.ds.SimpleDataSource;
import com.scholastic.classmags.migration.services.FAQQuestionsDataService;
import com.scholastic.classmags.migration.utils.DropdownUtils;

/**
 * JUnit for QuestionsDropdownUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { QuestionsDropdownUse.class, DropdownUtils.class } )
public class QuestionsDropdownUseTest extends BaseComponentTest {

    private QuestionsDropdownUse questionsDropdownUse;

    private Map< String, String > questionsMap;
    private FAQQuestionsDataService faqQuestionsDataService;
    private SimpleDataSource dataSource;

    /**
     * Setup.
     * 
     * @throws Exception
     */
    @Before
    public void setup() throws Exception {
        questionsDropdownUse = Whitebox.newInstance( QuestionsDropdownUse.class );

        faqQuestionsDataService = mock( FAQQuestionsDataService.class );
        dataSource = mock( SimpleDataSource.class );

        questionsMap = new HashMap< >();
        questionsMap.put( "SS-1", "SS-1 : How to Subscribe?" );
        questionsMap.put( "SS-2", "SS-2 : Renewing subscription" );

        stubCommon( questionsDropdownUse );

        PowerMockito.mockStatic( DropdownUtils.class );

        when( DropdownUtils.getContentPage( request ) ).thenReturn( currentPage );
        when( slingScriptHelper.getService( FAQQuestionsDataService.class ) ).thenReturn(faqQuestionsDataService );
        when( faqQuestionsDataService.fetchFAQQuestionsData( request, currentPage ) ).thenReturn( questionsMap );
        whenNew( SimpleDataSource.class ).withAnyArguments().thenReturn( dataSource );
    }

    @Test
    public void testFAQQuestionSetUse() throws Exception {
        // execute logic
        Whitebox.invokeMethod( questionsDropdownUse, "activate" );

        // verify logic
        verify( request ).setAttribute( DataSource.class.getName(), dataSource );
    }

    @Test
    public void testFAQQuestionSetUseWhenSlingScriptHelperIsNull() throws Exception {
        // setup logic ( test- specific )
        stub( PowerMockito.method( QuestionsDropdownUse.class, "getSlingScriptHelper" ) ).toReturn( null );

        // execute logic
        Whitebox.invokeMethod( questionsDropdownUse, "activate" );

        // verify logic
        verify( request, never() ).setAttribute( DataSource.class.getName(), dataSource );
    }

    @Test
    public void testFAQQuestionSetUseWhenCurrentPageIsNull() throws Exception {
        // setup logic ( test- specific )
        when( DropdownUtils.getContentPage( request ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( questionsDropdownUse, "activate" );

        // verify logic
        verify( request, never() ).setAttribute( DataSource.class.getName(), dataSource );
    }

    @Test
    public void testFAQQuestionSetUseWhenFAQQuestionsDataServiceIsNull() throws Exception {
        // setup logic ( test- specific )
        when( slingScriptHelper.getService( FAQQuestionsDataService.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( questionsDropdownUse, "activate" );

        // verify logic
        verify( request, never() ).setAttribute( DataSource.class.getName(), dataSource );
    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final QuestionsDropdownUse questionsDropdownUse = new QuestionsDropdownUse();

        // verify logic
        assertNotNull( questionsDropdownUse );
    }

}
