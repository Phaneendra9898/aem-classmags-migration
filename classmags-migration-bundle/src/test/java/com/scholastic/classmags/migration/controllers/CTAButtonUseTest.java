package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.script.Bindings;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.scholastic.classmags.migration.models.CTAButton;

/**
 * JUnit for CTAButtonUse.
 */
@RunWith( MockitoJUnitRunner.class )
public class CTAButtonUseTest {

    /** The c TA button use. */
    private CTAButtonUse cTAButtonUse;

    /** The c TA button. */
    private CTAButton cTAButton;

    /** The bindings. */
    @Mock
    private Bindings bindings;

    /** The request. */
    @Mock
    private SlingHttpServletRequest request;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        cTAButtonUse = new CTAButtonUse();
        cTAButton = mock( CTAButton.class );
    }

    /**
     * Test activate.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testActivate() throws Exception {
        when( bindings.get( "request" ) ).thenReturn( request );
        when( request.adaptTo( CTAButton.class ) ).thenReturn( cTAButton );
        cTAButtonUse.init( bindings );
        cTAButtonUse.activate();
        assertEquals( cTAButton, cTAButtonUse.getCtaButton() );
    }

    /**
     * Test activate with no request.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testActivateWithNoRequest() throws Exception {
        when( bindings.get( "request" ) ).thenReturn( null );
        cTAButtonUse.init( bindings );
        cTAButtonUse.activate();
        cTAButtonUse.setCtaButton( cTAButton );
        assertEquals( cTAButton, cTAButtonUse.getCtaButton() );
    }
}
