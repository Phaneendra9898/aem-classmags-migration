package com.scholastic.classmags.migration.filters;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.day.cq.dam.commons.util.DamUtil;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * JUnit for SubscriptionBenefitsService.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { AuthRequestFilter.class, DamUtil.class, RoleUtil.class } )
public class AuthRequestFilterTest {

    private final String PATH = "/content/class-mags/migration/dam/images/pup-patrol.pdf";
    private AuthRequestFilter authRequestFilter;
    private Map< String, Object > config;
    private FilterConfig filterConfig;
    private SlingHttpServletRequest request;
    private SlingHttpServletResponse response;
    private FilterChain chain;
    private Resource resource;
    private ResourceResolver serviceResolver;
    private ResourceResolverFactory resourceResolverFactory;
    private ValueMap asset;
    private RequestPathInfo requestPathInfo;

    /** The logger. */
    Logger logger;

    /**
     * Setup.
     * 
     * @throws Exception
     */
    @SuppressWarnings( "unchecked" )
    @Before
    public void setup() throws Exception {
        authRequestFilter = new AuthRequestFilter();

        logger = mock( Logger.class );
        config = mock( Map.class );
        filterConfig = mock( FilterConfig.class );
        request = mock( SlingHttpServletRequest.class );
        response = mock( SlingHttpServletResponse.class );
        chain = mock( FilterChain.class );
        resource = mock( Resource.class );
        resourceResolverFactory = mock( ResourceResolverFactory.class );
        serviceResolver = mock( ResourceResolver.class );
        asset = mock( ValueMap.class );
        requestPathInfo = mock(RequestPathInfo.class);

        PowerMockito.mockStatic( DamUtil.class );
        PowerMockito.mockStatic( RoleUtil.class );

        Whitebox.setInternalState( AuthRequestFilter.class, "LOG", logger );
        Whitebox.setInternalState( authRequestFilter, resourceResolverFactory );

        when( config.containsKey( ClassMagsMigrationConstants.SERVICE_ENABLED ) ).thenReturn( true );
        when( config.get( ClassMagsMigrationConstants.SERVICE_ENABLED ) ).thenReturn( true );
        when( request.getResource() ).thenReturn( resource );
        when( DamUtil.isAsset( resource ) ).thenReturn( true );
        when( RoleUtil.getUserRole( request ) ).thenReturn( ClassMagsMigrationConstants.USER_TYPE_TEACHER );
        when( resource.getPath() ).thenReturn( PATH );
        when( resourceResolverFactory.getServiceResourceResolver( any( Map.class ) ) ).thenReturn( serviceResolver );
        when( serviceResolver.getResource( PATH + "/" + ClassMagsMigrationConstants.ASSET_METADATA_NODE ) )
                .thenReturn( resource );
        when( resource.adaptTo( ValueMap.class ) ).thenReturn( asset );
        when( asset.get( "userType", RoleUtil.ROLE_TYPE_EVERYONE ) )
                .thenReturn( ClassMagsMigrationConstants.USER_TYPE_TEACHER );
        when( RoleUtil.shouldRender( ClassMagsMigrationConstants.USER_TYPE_TEACHER,
                ClassMagsMigrationConstants.USER_TYPE_TEACHER ) ).thenReturn( true );
        when( serviceResolver.isLive() ).thenReturn( true );
        
        when(request.getRequestPathInfo()).thenReturn( requestPathInfo );
        when(requestPathInfo.getSuffix()).thenReturn( "xyz" );
    }

    @Test
    public void testCreateInstance() {

        // execute logic
        final AuthRequestFilter authRequestFilter = new AuthRequestFilter();

        // verify logic
        assertNotNull( authRequestFilter );
    }

    @Test
    public void testWhenServiceIsEnabled() throws Exception {
        // setup logic ( test-specific )

        // execute logic
        Whitebox.invokeMethod( authRequestFilter, "init", filterConfig );
        Whitebox.invokeMethod( authRequestFilter, "activate", config );
        Whitebox.invokeMethod( authRequestFilter, "doFilter", request, response, chain );
        Whitebox.invokeMethod( authRequestFilter, "destroy" );

        // verify logic
        verify( chain ).doFilter( request, response );
        verify( response, times( 0 ) ).sendError( HttpServletResponse.SC_NOT_FOUND );
    }

    @Test
    public void testWhenServiceIsDisabled() throws Exception {
        // setup logic ( test-specific )
        when( config.containsKey( ClassMagsMigrationConstants.SERVICE_ENABLED ) ).thenReturn( false );

        // execute logic
        Whitebox.invokeMethod( authRequestFilter, "init", filterConfig );
        Whitebox.invokeMethod( authRequestFilter, "activate", config );
        Whitebox.invokeMethod( authRequestFilter, "doFilter", request, response, chain );
        Whitebox.invokeMethod( authRequestFilter, "destroy" );

        // verify logic
        verify( chain ).doFilter( request, response );
    }

    @Test
    public void testWhenResourceIsNull() throws Exception {
        // setup logic ( test-specific )
        when( request.getResource() ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( authRequestFilter, "init", filterConfig );
        Whitebox.invokeMethod( authRequestFilter, "activate", config );
        Whitebox.invokeMethod( authRequestFilter, "doFilter", request, response, chain );
        Whitebox.invokeMethod( authRequestFilter, "destroy" );

        // verify logic
        verify( chain ).doFilter( request, response );
    }

    @Test
    public void testWhenIsNotAsset() throws Exception {
        // setup logic ( test-specific )
        when( DamUtil.isAsset( resource ) ).thenReturn( false );

        // execute logic
        Whitebox.invokeMethod( authRequestFilter, "init", filterConfig );
        Whitebox.invokeMethod( authRequestFilter, "activate", config );
        Whitebox.invokeMethod( authRequestFilter, "doFilter", request, response, chain );
        Whitebox.invokeMethod( authRequestFilter, "destroy" );

        // verify logic
        verify( chain ).doFilter( request, response );
    }

    @Test
    public void testWhenAssetMetaDataIsNull() throws Exception {
        // setup logic ( test-specific )
        when( serviceResolver.getResource( PATH + "/" + ClassMagsMigrationConstants.ASSET_METADATA_NODE ) )
                .thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( authRequestFilter, "init", filterConfig );
        Whitebox.invokeMethod( authRequestFilter, "activate", config );
        Whitebox.invokeMethod( authRequestFilter, "doFilter", request, response, chain );
        Whitebox.invokeMethod( authRequestFilter, "destroy" );

    }

    @Test
    public void testWhenAssetShouldNotRender() throws Exception {
        // setup logic ( test-specific )
        when( RoleUtil.getUserRole( request ) ).thenReturn( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS );
        when( RoleUtil.shouldRender( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS,
                ClassMagsMigrationConstants.USER_TYPE_TEACHER ) ).thenReturn( false );

        // execute logic
        Whitebox.invokeMethod( authRequestFilter, "init", filterConfig );
        Whitebox.invokeMethod( authRequestFilter, "activate", config );
        Whitebox.invokeMethod( authRequestFilter, "doFilter", request, response, chain );
        Whitebox.invokeMethod( authRequestFilter, "destroy" );

        // verify logic
        verify( response ).sendError( HttpServletResponse.SC_NOT_FOUND );
    }

    @Test
    public void testWhenServiceResolverIsNotLive() throws Exception {
        // setup logic ( test-specific )
        when( serviceResolver.isLive() ).thenReturn( false );

        // execute logic
        Whitebox.invokeMethod( authRequestFilter, "init", filterConfig );
        Whitebox.invokeMethod( authRequestFilter, "activate", config );
        Whitebox.invokeMethod( authRequestFilter, "doFilter", request, response, chain );
        Whitebox.invokeMethod( authRequestFilter, "destroy" );

        // verify logic
        verify( serviceResolver, times( 0 ) ).close();
    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testWhenServiceResolverIsNull() throws Exception {
        // setup logic ( test-specific )
        when( resourceResolverFactory.getServiceResourceResolver( any( Map.class ) ) )
                .thenThrow( new LoginException() );

        // execute logic
        Whitebox.invokeMethod( authRequestFilter, "init", filterConfig );
        Whitebox.invokeMethod( authRequestFilter, "activate", config );
        Whitebox.invokeMethod( authRequestFilter, "doFilter", request, response, chain );
        Whitebox.invokeMethod( authRequestFilter, "destroy" );

        // verify logic
        verify( chain, times( 0 ) ).doFilter( request, response );

    }
}
