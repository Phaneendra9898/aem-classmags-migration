package com.scholastic.classmags.migration.servlets;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;

/**
 * The Class ContactUsSubTopicsServletTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { ContactUsSubTopicsServlet.class, PredicateGroup.class } )
public class ContactUsSubTopicsServletTest {
    private static final String RESOURCE_PATH = "scholastic/classroom-magazines-migration/components/page/base-page";
    private static final String[] PROPERTY_VALUE = {
            "{\"topicTitle\":\"Subscription and shipping\",\"subTopics\":[{\"subTopicTitle\":\"How will my magazines be shipped?\"},{\"subTopicTitle\":\"How will my magazines be shipped?\"}]}" };
    private static final String[] INCORRECT_PROPERTY_VALUE = {
            "\"topicTitle\":\"Subscription and shipping\",\"subTopics\":[\"subTopicTitle\":\"How will my magazines be shipped?\"},{\"subTopicTitle\":\"How will my magazines be shipped?\"}]}" };
    private static final String[] EMPTY_PROPERTY = {};
    private static final String SUB_TOPICS = "[{\"subTopicTitle\":\"How will my magazines be shipped?\"},{\"subTopicTitle\":\"How will my magazines be shipped?\"}]";
    private static final String[] PROPERTY_VALUE1 = { "{\"topicTitle\":\"Subscription and shipping\"}" };
    private static final String[] PROPERTY_VALUE2 = { "{\"topicTitle\":\"Suscription and shipping\"}" };
    private static final String DEFAULT_RESPONSE = "[{\"subTopicTitle\":\"Other\"}]";

    private Resource resource;
    private ContactUsSubTopicsServlet contactUsSubTopicsServlet;
    private SlingHttpServletRequest request;
    private SlingHttpServletResponse response;
    private Logger logger;
    private QueryBuilder queryBuilder;
    private ResourceResolver resourceResolver;
    private Query query;
    private SearchResult searchResult;
    private List< Hit > hits;
    private Hit hit;
    private PredicateGroup predicateGroup;
    private Session session;
    private ValueMap properties;
    private PrintWriter printWriter;

    @SuppressWarnings( "unchecked" )
    @Before
    public void setUp() throws Exception {
        contactUsSubTopicsServlet = Whitebox.newInstance( ContactUsSubTopicsServlet.class );

        request = mock( SlingHttpServletRequest.class );
        response = mock( SlingHttpServletResponse.class );
        resource = mock( Resource.class );
        queryBuilder = mock( QueryBuilder.class );
        resourceResolver = mock( ResourceResolver.class );
        query = mock( Query.class );
        searchResult = mock( SearchResult.class );
        session = mock( Session.class );
        predicateGroup = mock( PredicateGroup.class );
        logger = mock( Logger.class );
        hit = mock( Hit.class );
        properties = mock( ValueMap.class );
        printWriter = mock( PrintWriter.class );

        hits = new ArrayList< Hit >();
        hits.add( hit );
        hits.add( hit );

        PowerMockito.mockStatic( PredicateGroup.class );
        Whitebox.setInternalState( ContactUsSubTopicsServlet.class, "LOG", logger );
        Whitebox.setInternalState( contactUsSubTopicsServlet, queryBuilder );

        when( request.getResource() ).thenReturn( resource );
        when( resource.getPath() ).thenReturn( RESOURCE_PATH );
        when( resource.getResourceType() )
                .thenReturn( "scholastic/classroom-magazines-migration/components/page/base-page" );
        when( request.getResourceResolver() ).thenReturn( resourceResolver );
        when( resource.getChild( JcrConstants.JCR_CONTENT ) ).thenReturn( resource );
        when( PredicateGroup.create( any( Map.class ) ) ).thenReturn( predicateGroup );
        when( resourceResolver.adaptTo( Session.class ) ).thenReturn( session );
        when( queryBuilder.createQuery( predicateGroup, session ) ).thenReturn( query );
        when( query.getResult() ).thenReturn( searchResult );
        when( searchResult.getHits() ).thenReturn( hits );
        when( hit.getProperties() ).thenReturn( properties );
        when( properties.get( "topic", new String[] {} ) ).thenReturn( PROPERTY_VALUE );
        when( request.getParameter( "topic" ) ).thenReturn( "Subscription and shipping" );
        when( response.getWriter() ).thenReturn( printWriter );
    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testContactUsSubTopics() throws Exception {
        // execute logic
        Whitebox.invokeMethod( contactUsSubTopicsServlet, "doGet", request, response );
        Whitebox.invokeMethod( contactUsSubTopicsServlet, "accepts", request );

        // verify logic
        verify( printWriter ).write( SUB_TOPICS );
    }

    @Test
    public void testContactUsSubTopicsWhenSubTopicsIsNull() throws Exception {
        when( properties.get( "topic", new String[] {} ) ).thenReturn( PROPERTY_VALUE1 );

        // execute logic
        Whitebox.invokeMethod( contactUsSubTopicsServlet, "doGet", request, response );

        // verify logic
        verify( printWriter ).write( DEFAULT_RESPONSE );
    }

    @Test
    public void testContactUsSubTopicsWhenKeyIsNotTopicName() throws Exception {
        when( properties.get( "topic", new String[] {} ) ).thenReturn( PROPERTY_VALUE2 );

        // execute logic
        Whitebox.invokeMethod( contactUsSubTopicsServlet, "doGet", request, response );

        // verify logic
        verify( printWriter ).write( DEFAULT_RESPONSE );
    }

    @Test
    public void testContactUsSubTopicsWhenPropertiesIsNull() throws Exception {
        // setup logic ( test-specific )
        when( properties.get( "topic", new String[] {} ) ).thenReturn( EMPTY_PROPERTY );

        // execute logic
        Whitebox.invokeMethod( contactUsSubTopicsServlet, "doGet", request, response );

        // verify logic
        verify( printWriter ).write( DEFAULT_RESPONSE );
    }

    @Test
    public void testContactUsSubTopicsWhenJSONException() throws Exception {
        // setup logic ( test-specific )
        when( properties.get( "topic", new String[] {} ) ).thenReturn( INCORRECT_PROPERTY_VALUE );

        // execute logic
        Whitebox.invokeMethod( contactUsSubTopicsServlet, "doGet", request, response );

    }

    @Test
    public void testContactUsSubTopicsWhenJCRContentResourceIsNull() throws Exception {
        // setup logic ( test-specific )
        when( resource.getChild( JcrConstants.JCR_CONTENT ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( contactUsSubTopicsServlet, "accepts", request );

        // verify logic
        assertFalse( contactUsSubTopicsServlet.accepts( request ) );
    }

    @Test
    public void testContactUsSubTopicsWhenJCRContentResourceIsInvalid() throws Exception {
        // setup logic ( test-specific )
        when( resource.getResourceType() ).thenReturn( "" );

        // execute logic
        Whitebox.invokeMethod( contactUsSubTopicsServlet, "accepts", request );

        // verify logic
        assertFalse( contactUsSubTopicsServlet.accepts( request ) );
    }

    @Test
    public void testContactUsSubTopicsWhenRequestParamIsBlank() throws Exception {
        // setup logic ( test-specific )
        when( request.getParameter( "topic" ) ).thenReturn( "" );

        // execute logic
        Whitebox.invokeMethod( contactUsSubTopicsServlet, "accepts", request );

        // verify logic
        assertFalse( contactUsSubTopicsServlet.accepts( request ) );
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final ContactUsSubTopicsServlet contactUsSubTopicsServlet = new ContactUsSubTopicsServlet();

        // verify logic
        assertNotNull( contactUsSubTopicsServlet );
    }
}