package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import java.util.HashSet;
import java.util.Set;

import javax.script.Bindings;

import org.apache.sling.settings.SlingSettingsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.CommonUtils;

@RunWith( PowerMockRunner.class )
@PrepareForTest( { MagazineNameUse.class, CommonUtils.class } )
public class MagazineNameUseTest extends BaseComponentTest {

    private static final String CURRENT_PATH = "/content/classroom_magazines/scienceworld/pages/topics/biology";
    private static final String MAGAZINE_NAME = "scienceworld";

    private MagazineNameUse magazineNameUse;

    private Bindings bindings;
    
    private MagazineProps magazineProps;
    private SlingSettingsService slingSettingsService;
    private Set<String> runModes;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        magazineNameUse = Whitebox.newInstance( MagazineNameUse.class );
        
        stubCommon( magazineNameUse );
        
        PowerMockito.mockStatic ( CommonUtils.class );

        bindings = mock( Bindings.class );
        magazineProps = mock( MagazineProps.class );
        slingSettingsService = mock( SlingSettingsService.class );
        runModes = new HashSet<>();
        runModes.add( "prod" );

        when( bindings.get( "currentPath" ) ).thenReturn( CURRENT_PATH );
        when( slingScriptHelper.getService( MagazineProps.class ) ).thenReturn( magazineProps );
        when( slingScriptHelper.getService(  SlingSettingsService.class ) ).thenReturn( slingSettingsService );
        when( magazineProps.getMagazineName( CURRENT_PATH )).thenReturn( MAGAZINE_NAME );
        when( slingSettingsService.getRunModes()).thenReturn( runModes );
        when( CommonUtils.isRunMode( runModes, "prod" )).thenReturn( true );
    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final MagazineNameUse magazineNameUse = new MagazineNameUse();

        // verify logic
        assertNotNull( magazineNameUse );

    }

    @Test
    public void testMagazineNameUse() throws Exception {
        // execute logic
        magazineNameUse.init( bindings );
        Whitebox.invokeMethod( magazineNameUse, "activate" );

        // verify logic
        assertEquals(MAGAZINE_NAME, magazineNameUse.getMagazineName() );
        assertTrue( magazineNameUse.isProd());
    }
    
    @Test
    public void testMagazineNameUseWhenMagazinePropsServiceIsNull() throws Exception {
        // setup logic (test-specific)
        when( slingScriptHelper.getService( MagazineProps.class ) ).thenReturn( null );
        
        // execute logic
        magazineNameUse.init( bindings );
        Whitebox.invokeMethod( magazineNameUse, "activate" );

        // verify logic
        assertNull( magazineNameUse.getMagazineName() );
    }

    @Test
    public void testMagazineNameUseWhenSlingScriptHelperIsNull() throws Exception {
        // setup logic (test-specific)
        stub( PowerMockito.method( magazineNameUse.getClass(), "getSlingScriptHelper" ) )
        .toReturn( null );
        
        // execute logic
        magazineNameUse.init( bindings );
        Whitebox.invokeMethod( magazineNameUse, "activate" );

        // verify logic
        assertNull( magazineNameUse.getMagazineName() );
    }
}
