package com.scholastic.classmags.migration.controllers;

import javax.script.Bindings;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.scholastic.classmags.migration.models.ContentTile;
import com.scholastic.classmags.migration.models.ContentTileFlags;
import com.scholastic.classmags.migration.models.ContentTileObject;
import com.scholastic.classmags.migration.services.ContentTileService;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class ContentTileUseTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { ContentTileUse.class, CommonUtils.class } )
public class ContentTileUseTest {

    /** The content tile flags. */
    @Mock
    private ContentTileFlags contentTileFlags;

    /** The content tile use. */
    @Mock
    private ContentTileUse contentTileUse;

    /** The content tile. */
    @Mock
    private ContentTile contentTile;

    /** The content tile object. */
    @Mock
    private ContentTileObject contentTileObject;

    /** The content tile service. */
    @Mock
    private ContentTileService contentTileService;

    /** The mock request. */
    @Mock
    private SlingHttpServletRequest mockRequest;

    /** The mock bindings. */
    @Mock
    private Bindings mockBindings;

    /** The mock sling script helper. */
    @Mock
    private SlingScriptHelper mockSlingScriptHelper;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;

    /** The mock path. */
    private static final String MOCK_PATH = "/path";

    /** The Content tile link Constant. */
    private static final String CONTENT_TILE_LINK = "contentTileLink";

    /** The content tile article page path. */
    private static final String CONTENT_TILE_ARTICLE_PAGE_PATH = "pagePath";

    /** The Constant ARTICLE. */
    public static final String ARTICLE = "article";

    /** The Constant ISSUE. */
    public static final String ISSUE = "issue";

    /** The Constant SKILLS_SHEET. */
    public static final String SKILLS_SHEET = "skillsSheet";

    /** The Constant VIDEO. */
    public static final String VIDEO = "video";

    /** The Constant GAME. */
    public static final String GAME = "game";

    /** The Constant LESSON_PLAN. */
    public static final String LESSON_PLAN = "lessonPlan";

    /** The Constant CONTENT_TYPE_CUSTOM. */
    public static final String CONTENT_TYPE_CUSTOM = "customPage";

    /** The Constant for Default Case. */
    private static final String DEFAULT_CASE = "defaultCase";

    /**
     * Setup.
     */
    @Before
    public void before() {
        contentTileUse = new ContentTileUse();
        PowerMockito.mockStatic( CommonUtils.class );

    }

    /**
     * test CONTENT_TILE_ARTICLE_PAGE_PATH!=null and CONTENT_TILE_LINK!=null.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetContentTileObjectWithArticle() throws Exception {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockSlingScriptHelper.getService( ContentTileService.class ) ).thenReturn( contentTileService );
        when( mockRequest.getResource() ).thenReturn( mockResource );
        when( mockRequest.adaptTo( ContentTile.class ) ).thenReturn( contentTile );
        when( mockResource.getPath() ).thenReturn( MOCK_PATH );
        when( contentTile.getContentTileContentType() ).thenReturn( ARTICLE );
        when( contentTileService.getArticleContent( contentTile, MOCK_PATH ) ).thenReturn( contentTileObject );
        when( contentTile.getContentTileArticlePagePath() ).thenReturn( CONTENT_TILE_ARTICLE_PAGE_PATH );
        when( contentTile.getContentTileLink() ).thenReturn( CONTENT_TILE_LINK );
        when( CommonUtils.isAShareableAsset( CONTENT_TILE_LINK ) ).thenReturn( true );

        contentTileUse.init( mockBindings );
        contentTileUse.activate();

        assertEquals( contentTileObject, contentTileUse.getContentTileObject() );
        assertEquals( contentTile, contentTileUse.getContentTile() );
        assertEquals( true, contentTileUse.getContentTileFlags().isDownloadEnabled() );
        assertEquals( true, contentTileUse.getContentTileFlags().isViewArticleEnabled() );
        assertEquals( CONTENT_TILE_ARTICLE_PAGE_PATH, contentTileUse.getContentTile().getContentTileArticlePagePath() );

    }

    /**
     * test for CONTENT_TILE_ARTICLE_PAGE_PATH=null and CONTENT_TILE_LINK=null.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetContentTileObjectWithArticleWithoutArticlePagePath() throws Exception {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockSlingScriptHelper.getService( ContentTileService.class ) ).thenReturn( contentTileService );
        when( mockRequest.getResource() ).thenReturn( mockResource );
        when( mockRequest.adaptTo( ContentTile.class ) ).thenReturn( contentTile );
        when( mockResource.getPath() ).thenReturn( MOCK_PATH );
        when( contentTile.getContentTileContentType() ).thenReturn( ARTICLE );
        when( contentTileService.getArticleContent( contentTile, MOCK_PATH ) ).thenReturn( contentTileObject );
        when( contentTile.getContentTileArticlePagePath() ).thenReturn( null );
        when( contentTile.getContentTileLink() ).thenReturn( null );
        when( CommonUtils.isAShareableAsset( null ) ).thenReturn( false );
        contentTileUse.init( mockBindings );
        contentTileUse.activate();
        assertEquals( false, contentTileUse.getContentTileFlags().isDownloadEnabled() );
        assertEquals( false, contentTileUse.getContentTileFlags().isViewArticleEnabled() );
        assertEquals( contentTileObject, contentTileUse.getContentTileObject() );
        assertEquals( contentTile, contentTileUse.getContentTile() );
        assertEquals( null, contentTileUse.getContentTile().getContentTileArticlePagePath() );

    }

    /**
     * test get content tile object with issue.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetContentTileObjectWithIssue() throws Exception {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockSlingScriptHelper.getService( ContentTileService.class ) ).thenReturn( contentTileService );
        when( mockRequest.getResource() ).thenReturn( mockResource );
        when( mockRequest.adaptTo( ContentTile.class ) ).thenReturn( contentTile );
        when( mockResource.getPath() ).thenReturn( MOCK_PATH );
        when( contentTile.getContentTileContentType() ).thenReturn( ISSUE );
        when( contentTileService.getIssueContent( contentTile, MOCK_PATH ) ).thenReturn( contentTileObject );
        when( contentTile.getContentTileArticlePagePath() ).thenReturn( CONTENT_TILE_ARTICLE_PAGE_PATH );
        when( contentTile.getContentTileLink() ).thenReturn( CONTENT_TILE_LINK );
        when( CommonUtils.isAShareableAsset( CONTENT_TILE_LINK ) ).thenReturn( true );
        contentTileUse.init( mockBindings );
        contentTileUse.activate();
        assertEquals( contentTileObject, contentTileUse.getContentTileObject() );
        assertEquals( true, contentTileUse.getContentTileFlags().isDownloadEnabled() );
        assertEquals( true, contentTileUse.getContentTileFlags().isViewArticleEnabled() );
    }

    /**
     * test get content tile object with video.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetContentTileObjectWithVideo() throws Exception {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockSlingScriptHelper.getService( ContentTileService.class ) ).thenReturn( contentTileService );
        when( mockRequest.getResource() ).thenReturn( mockResource );
        when( mockRequest.adaptTo( ContentTile.class ) ).thenReturn( contentTile );
        when( mockResource.getPath() ).thenReturn( MOCK_PATH );
        when( contentTile.getContentTileContentType() ).thenReturn( VIDEO );
        when( contentTileService.getVideoContent( contentTile, MOCK_PATH ) ).thenReturn( contentTileObject );
        when( contentTile.getContentTileArticlePagePath() ).thenReturn( CONTENT_TILE_ARTICLE_PAGE_PATH );
        when( contentTile.getContentTileLink() ).thenReturn( CONTENT_TILE_LINK );
        when( CommonUtils.isAShareableAsset( CONTENT_TILE_LINK ) ).thenReturn( true );
        contentTileUse.init( mockBindings );
        contentTileUse.activate();
        assertEquals( contentTileObject, contentTileUse.getContentTileObject() );
        assertEquals( true, contentTileUse.getContentTileFlags().isDownloadEnabled() );
        assertEquals( true, contentTileUse.getContentTileFlags().isViewArticleEnabled() );
    }

    /**
     * test get content tile object with game.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetContentTileObjectWithGame() throws Exception {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockSlingScriptHelper.getService( ContentTileService.class ) ).thenReturn( contentTileService );
        when( mockRequest.getResource() ).thenReturn( mockResource );
        when( mockRequest.adaptTo( ContentTile.class ) ).thenReturn( contentTile );
        when( mockResource.getPath() ).thenReturn( MOCK_PATH );
        when( contentTile.getContentTileContentType() ).thenReturn( GAME );
        when( contentTileService.getGameContent( contentTile, MOCK_PATH ) ).thenReturn( contentTileObject );
        when( contentTile.getContentTileArticlePagePath() ).thenReturn( CONTENT_TILE_ARTICLE_PAGE_PATH );
        when( contentTile.getContentTileLink() ).thenReturn( CONTENT_TILE_LINK );
        when( CommonUtils.isAShareableAsset( CONTENT_TILE_LINK ) ).thenReturn( true );
        contentTileUse.init( mockBindings );
        contentTileUse.activate();
        assertEquals( contentTileObject, contentTileUse.getContentTileObject() );
        assertEquals( true, contentTileUse.getContentTileFlags().isDownloadEnabled() );
        assertEquals( true, contentTileUse.getContentTileFlags().isViewArticleEnabled() );
    }

    /**
     * test get content tile object with lesson plan.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetContentTileObjectWithLessonPlan() throws Exception {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockSlingScriptHelper.getService( ContentTileService.class ) ).thenReturn( contentTileService );
        when( mockRequest.getResource() ).thenReturn( mockResource );
        when( mockRequest.adaptTo( ContentTile.class ) ).thenReturn( contentTile );
        when( mockResource.getPath() ).thenReturn( MOCK_PATH );
        when( contentTile.getContentTileContentType() ).thenReturn( LESSON_PLAN );
        when( contentTileService.getAssetContent( contentTile, MOCK_PATH ) ).thenReturn( contentTileObject );
        when( contentTile.getContentTileArticlePagePath() ).thenReturn( CONTENT_TILE_ARTICLE_PAGE_PATH );
        when( contentTile.getContentTileLink() ).thenReturn( CONTENT_TILE_LINK );
        when( CommonUtils.isAShareableAsset( CONTENT_TILE_LINK ) ).thenReturn( true );
        contentTileUse.init( mockBindings );
        contentTileUse.activate();
        assertEquals( contentTileObject, contentTileUse.getContentTileObject() );
        assertEquals( true, contentTileUse.getContentTileFlags().isDownloadEnabled() );
        assertEquals( true, contentTileUse.getContentTileFlags().isViewArticleEnabled() );
    }

    /**
     * test get content tile object with lesson plan.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetContentTileObjectWithSkillsSheet() throws Exception {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockSlingScriptHelper.getService( ContentTileService.class ) ).thenReturn( contentTileService );
        when( mockRequest.getResource() ).thenReturn( mockResource );
        when( mockRequest.adaptTo( ContentTile.class ) ).thenReturn( contentTile );
        when( mockResource.getPath() ).thenReturn( MOCK_PATH );
        when( contentTile.getContentTileContentType() ).thenReturn( SKILLS_SHEET );
        when( contentTileService.getAssetContent( contentTile, MOCK_PATH ) ).thenReturn( contentTileObject );
        when( contentTile.getContentTileArticlePagePath() ).thenReturn( CONTENT_TILE_ARTICLE_PAGE_PATH );
        when( contentTile.getContentTileLink() ).thenReturn( CONTENT_TILE_LINK );
        when( CommonUtils.isAShareableAsset( CONTENT_TILE_LINK ) ).thenReturn( true );

        contentTileUse.init( mockBindings );
        assertEquals( contentTileObject, contentTileUse.getContentTileObject() );
        assertEquals( true, contentTileUse.getContentTileFlags().isDownloadEnabled() );
        assertEquals( true, contentTileUse.getContentTileFlags().isViewArticleEnabled() );
    }

    /**
     * test get content tile object with CONTENT_TYPE_CUSTOM.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetContentTileObjectWithContentTypeCustom() throws Exception {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockSlingScriptHelper.getService( ContentTileService.class ) ).thenReturn( contentTileService );
        when( mockRequest.getResource() ).thenReturn( mockResource );
        when( mockRequest.adaptTo( ContentTile.class ) ).thenReturn( contentTile );
        when( mockResource.getPath() ).thenReturn( MOCK_PATH );
        when( contentTile.getContentTileContentType() ).thenReturn( CONTENT_TYPE_CUSTOM );
        when( contentTileService.getCustomContent( contentTile, MOCK_PATH ) ).thenReturn( contentTileObject );
        when( contentTile.getContentTileArticlePagePath() ).thenReturn( CONTENT_TILE_ARTICLE_PAGE_PATH );
        when( contentTile.getContentTileLink() ).thenReturn( CONTENT_TILE_LINK );
        when( CommonUtils.isAShareableAsset( CONTENT_TILE_LINK ) ).thenReturn( true );
        contentTileUse.init( mockBindings );
        contentTileUse.activate();
        assertEquals( contentTileObject, contentTileUse.getContentTileObject() );
        assertEquals( false, contentTileUse.getContentTileFlags().isDownloadEnabled() );
        assertEquals( true, contentTileUse.getContentTileFlags().isViewArticleEnabled() );
    }

    /**
     * test get content tile object for Default case.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetContentTileObjectWithDefaultCase() throws Exception {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockSlingScriptHelper.getService( ContentTileService.class ) ).thenReturn( contentTileService );
        when( mockRequest.getResource() ).thenReturn( mockResource );
        when( mockRequest.adaptTo( ContentTile.class ) ).thenReturn( contentTile );
        when( mockResource.getPath() ).thenReturn( MOCK_PATH );
        when( contentTile.getContentTileContentType() ).thenReturn( DEFAULT_CASE );
        when( contentTileService.getIssueContent( contentTile, MOCK_PATH ) ).thenReturn( null );
        contentTileUse.init( mockBindings );
        contentTileUse.activate();
        assertEquals( null, contentTileUse.getContentTileObject() );
        assertEquals( null, contentTileUse.getContentTileFlags() );
        assertEquals( contentTile, contentTileUse.getContentTile() );
    }

    /**
     * test get content tile object with no request.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetContentTileObjectWithNoRequest() throws Exception {
        when( mockBindings.get( "request" ) ).thenReturn( null );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockSlingScriptHelper.getService( ContentTileService.class ) ).thenReturn( contentTileService );
        contentTileUse.init( mockBindings );
        contentTileUse.activate();
        assertEquals( null, contentTileUse.getContentTileObject() );
        assertEquals( null, contentTileUse.getContentTileFlags() );
        assertEquals( null, contentTileUse.getContentTile() );

    }
}
