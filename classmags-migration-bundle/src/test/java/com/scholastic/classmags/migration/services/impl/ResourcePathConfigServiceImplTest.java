package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.reflect.Whitebox;

/**
 * JUnit for ResourcePathConfigServiceImpl.
 */
@RunWith( Parameterized.class )
@PrepareForTest( { ResourcePathConfigServiceImpl.class } )
public class ResourcePathConfigServiceImplTest {

    private static final String DATA_NODE_PATH = "/jcr:content/data-page-par/global_config";
    private static final String FOOTER_NODE_PATH = "/jcr:content/data-page-par/global_footer_contai";
    private static final String GLOBAL_DATA_CONFIG_PAGE = "/content/classroom_magazines/admin/global-cm-data-page";
    private static final Object SCIENCE_WORLD_DATA_CONFIG_PAGE = "/content/classroom-magazines-admin/scienceworld-data-config-page";
    
    /** The resource path config service. */
    private ResourcePathConfigServiceImpl resourcePathConfigService;
    
    /** The properties. */
    private Map< String, Object > properties;
    
    /** The magazine name. */
    private String magazineName;
    
    /** The expected resource path. */
    private String expectedResourcePath;
    
    /** The expected footer resource path. */
    private String expectedFooterResourcePath;
    
    /**
     * Initial Setup.
     * 
     * @throws Exception 
     * 
     */
    @Before
    public void setUp() throws Exception{
        
        resourcePathConfigService = new ResourcePathConfigServiceImpl();
        
        properties = createPropertiesMap();
        
        Whitebox.invokeMethod( resourcePathConfigService, "activate", properties );
        
    }

    /**
     * Test get global resource path.
     * 
     */
    @Test
    public void testGetGlobalResourcePath() {
       
        // execute logic
        String globalResourcePath = resourcePathConfigService.getGlobalResourcePath();
        
        // verify logic
        assertEquals(GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, globalResourcePath);
    }
    
    /**
     * Test get global footer resource path.
     * 
     */
    @Test
    public void testGetGlobalFooterResourcePath() {
       
        // execute logic
        String globalResourcePath = resourcePathConfigService.getGlobalFooterResourcePath();
        
        // verify logic
        assertEquals(GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH, globalResourcePath);
    }
    
    /**
     * Test get global config resource path.
     * 
     */
    @Test
    public void testGlobalConfigResourcePath() {
        // execute logic
        String globalConfigResourcePath = resourcePathConfigService.getGlobalConfigResourcePath( magazineName );

        // verify logic
        assertEquals( expectedResourcePath, globalConfigResourcePath );
    }
    
    /**
     * Test get global footer config resource path.
     * 
     */
    @Test
    public void testGlobalFooterConfigResourcePath() {
        // execute logic
        String globalFooterConfigResourcePath = resourcePathConfigService.getFooterConfigResourcePath( magazineName );

        // verify logic
        assertEquals( expectedFooterResourcePath, globalFooterConfigResourcePath );
    }
    
    @Parameterized.Parameters
    public static Collection< Object[] > magazineNames() {
       return Arrays.asList(new Object[][] {
          { "scienceworld", SCIENCE_WORLD_DATA_CONFIG_PAGE + DATA_NODE_PATH, SCIENCE_WORLD_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "superscience", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH ,GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "action", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "art", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "choices", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "dynamath", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "geographyspin", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "junior", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "letsfindout", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "math", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "mybigworld", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "scholasticnews1", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "scholasticnews2", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "scholasticnews3", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "scholasticnews4", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "scholasticnews56", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "sciencespink1", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "sciencespin2", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "sciencespin36", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "scope", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "storyworks", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "storyworksjr", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "upfront", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH },
          { "", GLOBAL_DATA_CONFIG_PAGE + DATA_NODE_PATH, GLOBAL_DATA_CONFIG_PAGE + FOOTER_NODE_PATH }
       });
    }
    
    /*
     * Constructor for ResourcePathConfigServiceImplTest.
     */
    public ResourcePathConfigServiceImplTest(String magazineName, String expectedResourcePath, String expectedFooterResourcePath) {
        this.magazineName = magazineName;
        this.expectedResourcePath = expectedResourcePath;
        this.expectedFooterResourcePath = expectedFooterResourcePath;
     }
    /*
     * Creates the properties map.
     */
    private Map< String, Object > createPropertiesMap() {
        
        HashMap< String, Object > propertiesMap = new HashMap<String,Object>();
        
        propertiesMap.put( "classmags-global-resource-path", GLOBAL_DATA_CONFIG_PAGE );
        propertiesMap.put( "classmags-scienceworld-resource-path", SCIENCE_WORLD_DATA_CONFIG_PAGE );
        propertiesMap.put( "classmags-superscience-resource-path", "" );
        propertiesMap.put( "classmags-action-resource-path", "" );
        propertiesMap.put( "classmags-art-resource-path", "" );
        propertiesMap.put( "classmags-choices-resource-path", "" );
        propertiesMap.put( "classmags-dynamath-resource-path", "" );
        propertiesMap.put( "classmags-geographyspin-resource-path", "" );
        propertiesMap.put( "classmags-junior-resource-path", "" );
        propertiesMap.put( "classmags-letsfindout-resource-path", "" );
        propertiesMap.put( "classmags-math-resource-path", "" );
        propertiesMap.put( "classmags-mybigworld-resource-path", "" );
        propertiesMap.put( "classmags-scholasticnews1-resource-path", "" );
        propertiesMap.put( "classmags-scholasticnews2-resource-path", "" );
        propertiesMap.put( "classmags-scholasticnews3-resource-path", "" );
        propertiesMap.put( "classmags-scholasticnews4-resource-path", "" );
        propertiesMap.put( "classmags-scholasticnews56-resource-path", "" );
        propertiesMap.put( "classmags-sciencespink1-resource-path", "" );
        propertiesMap.put( "classmags-sciencespin2-resource-path", "" );
        propertiesMap.put( "classmags-sciencespin36-resource-path", "" );
        propertiesMap.put( "classmags-scope-resource-path", "" );
        propertiesMap.put( "classmags-storyworks-resource-path", "" );
        propertiesMap.put( "classmags-storyworksjr-resource-path", "" );
        propertiesMap.put( "classmags-upfront-resource-path", "" );
        
        return propertiesMap;
    }
    
}
