package com.scholastic.classmags.migration.servlets;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.adobe.acs.commons.email.EmailService;
import com.day.cq.commons.jcr.JcrConstants;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.ValidationUtils;

/**
 * The Class ContactUsServletTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { ContactUsServlet.class, ValidationUtils.class } )
public class ContactUsServletTest {
    private static final String JSON_DATA = "{\"topic\":\"Subscription and shipping\",\"contactDetails\":{\"name\":\"Sayantan\",\"email\":\"sasen@deloitte.com\",\"schoolName\":\"FAPS\",\"schoolState\":\"New York\",\"grade\":\"Article\",\"message\":\"ABCD\"},\"issue\":\"How will my magazines be shipped?\",\"recepient\":\"vivdesai@deloitte.com\"}";
    private static final String JSON_DATA1 = "{\"topic\":\"\",\"contactDetails\":{\"name\":\"Sayantan\",\"email\":\"sasen@deloitte.com\",\"schoolName\":\"FAPS\",\"schoolState\":\"New York\",\"grade\":\"Article\",\"message\":\"ABCD\"},\"issue\":\"\",\"recepient\":\"vivdesai@deloitte.com\"}";
    private static final String JSON_DATA2 = "{}";
    private static final String JSON_DATA3 = "";
    private static final String RESOURCE_TYPE = "scholastic/classroom-magazines-migration/components/page/base-page";
    private ContactUsServlet contactUsServlet;
    private SlingHttpServletRequest request;
    private SlingHttpServletResponse response;
    private MagazineProps magazineProps;
    private Resource resource;
    private Resource childResource;
    private ResourceResolver resourceResolver;
    private Logger logger;
    private EmailService emailService;
    private Map map;

    @Before
    public void setUp() throws Exception {
        contactUsServlet = Whitebox.newInstance( ContactUsServlet.class );

        request = mock( SlingHttpServletRequest.class );
        response = mock( SlingHttpServletResponse.class );
        magazineProps = mock( MagazineProps.class );
        resource = mock( Resource.class );
        resourceResolver = mock( ResourceResolver.class );
        emailService = mock( EmailService.class );
        childResource = mock( Resource.class );
        map = mock( Map.class );
        logger = mock( Logger.class );

        Whitebox.setInternalState( contactUsServlet, magazineProps );
        Whitebox.setInternalState( contactUsServlet, emailService );
        Whitebox.setInternalState( ContactUsServlet.class, "log", logger );
        PowerMockito.mockStatic( ValidationUtils.class );

        when( request.getParameter( "data" ) ).thenReturn( JSON_DATA );
        when( request.getResource() ).thenReturn( resource );
        when( resource.getChild( JcrConstants.JCR_CONTENT ) ).thenReturn( childResource );
        when( childResource.getResourceType() ).thenReturn( RESOURCE_TYPE );
        when( resource.getPath() ).thenReturn( "/content/ClassMags/scienceworld" );
        when( magazineProps.getMagazineName( "/content/ClassMags/scienceworld" ) ).thenReturn( "scienceworld" );
        when( request.getResourceResolver() ).thenReturn( resourceResolver );
        when( request.getParameterMap() ).thenReturn( map );
        when( ValidationUtils.getFilteredText( any( ResourceResolver.class ), any( String.class ) ) )
                .thenReturn( "sasen@deloitte.com" );
        when( ValidationUtils.isFieldValid( any( String.class ) ) ).thenReturn( true );
        when( ValidationUtils.isFieldValidFormat( any( String.class ), any( Pattern.class ) ) ).thenReturn( true );
    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testContactUsServlet() throws Exception {
        // execute logic
        Whitebox.invokeMethod( contactUsServlet, "doPost", request, response );
        Whitebox.invokeMethod( contactUsServlet, "accepts", request );

        // verify logic
        verify( emailService ).sendEmail( any( String.class ), any( Map.class ), any( String.class ) );
    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testContactUsServletWhenTopicIsNull() throws Exception {
        // setup logic ( test-specific )
        when( request.getParameter( "data" ) ).thenReturn( JSON_DATA1 );
        when( ValidationUtils.isFieldValid( any( String.class ) ) ).thenReturn( false );
        when( resource.getChild( JcrConstants.JCR_CONTENT ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( contactUsServlet, "doPost", request, response );
        Whitebox.invokeMethod( contactUsServlet, "accepts", request );

        // verify logic
        verify( emailService, times( 0 ) ).sendEmail( any( String.class ), any( Map.class ), any( String.class ) );
        assertFalse( contactUsServlet.accepts( request ) );
    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testContactUsServletWhenEmailFormatInvalid() throws Exception {
        // setup logic ( test-specific )
        when( request.getParameter( "data" ) ).thenReturn( JSON_DATA1 );
        when( ValidationUtils.isFieldValidFormat( any( String.class ), any( Pattern.class ) ) ).thenReturn( false );

        // execute logic
        Whitebox.invokeMethod( contactUsServlet, "doPost", request, response );

        // verify logic
        verify( emailService, times( 0 ) ).sendEmail( any( String.class ), any( Map.class ), any( String.class ) );
    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testContactUsServletWhenContactDetailsIsNull() throws Exception {
        // setup logic ( test-specific )
        when( request.getParameter( "data" ) ).thenReturn( JSON_DATA2 );

        // execute logic
        Whitebox.invokeMethod( contactUsServlet, "doPost", request, response );

        // verify logic
        verify( response ).sendError( HttpServletResponse.SC_BAD_REQUEST, "EMAIL_VALIDATION_FAILED" );
    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testContactUsServletWhenJSONException() throws Exception {
        // setup logic ( test-specific )
        when( request.getParameter( "data" ) ).thenReturn( JSON_DATA3 );

        // execute logic
        Whitebox.invokeMethod( contactUsServlet, "doPost", request, response );

        // verify logic
        verify( response ).sendError( HttpServletResponse.SC_BAD_REQUEST, "EMAIL_VALIDATION_FAILED" );
    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testContactUsServletWhenRequestParameterMapIsNull() throws Exception {
        // setup logic ( test-specific )
        when( map.isEmpty() ).thenReturn( true );

        // execute logic
        Whitebox.invokeMethod( contactUsServlet, "accepts", request );

        // verify logic
        assertFalse( contactUsServlet.accepts( request ) );
    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testContactUsServletChildResourceInvalidResourceType() throws Exception {
        // setup logic ( test-specific )
        when( childResource.getResourceType() ).thenReturn( "" );

        // execute logic
        Whitebox.invokeMethod( contactUsServlet, "accepts", request );

        // verify logic
        assertFalse( contactUsServlet.accepts( request ) );
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final ContactUsServlet contactUsServlet = new ContactUsServlet();

        // verify logic
        assertNotNull( contactUsServlet );
    }
}