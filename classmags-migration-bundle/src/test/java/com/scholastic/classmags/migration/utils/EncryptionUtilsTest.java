package com.scholastic.classmags.migration.utils;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.Constructor;

import org.junit.Test;

/**
 * JUnit for EncryptionUtils.
 */
public class EncryptionUtilsTest {

    @Test
    public void testStringEncryption() {
        String encrypted = EncryptionUtils.encrypt( "test2@test2test2test2test2.test2" );
        assertEquals( "CXFX_L6jcLW8azOl6tkpddrndu5Cf_PMYmOZVoj5LYhSdtu7jGYCf2EQ0hiqIm040", encrypted );
    }

    @Test
    public void testStringDecryption1() {
        String decrypted = EncryptionUtils
                .decrypt( "CXFX_L6jcLW8azOl6tkpddrndu5Cf_PMYmOZVoj5LYhSdtu7jGYCf2EQ0hiqIm040" );
        assertEquals( "test2@test2test2test2test2.test2", decrypted );
    }

    @Test
    public void testStringDecryption2() {
        String decrypted = EncryptionUtils
                .decrypt( "06xd6Epot4jYc3DlwAaxSHEKHvsh2zVfXTV8bHPb43HfiyzQEMHOtQNcr3c0LzLFLHOtZk77b54PnIyGadY4yA2" );
        assertEquals( "dalni8tasdh86G&465487-rf86\"4e%#@^%Efvgb8g/<.~!$%+_)0=-}{;';/..", decrypted );
    }

    @Test
    public void testPrivateConstructor() throws Exception {
        Constructor< ? >[] constructor = EncryptionUtils.class.getDeclaredConstructors();

        constructor[ 0 ].setAccessible( true );
        constructor[ 0 ].newInstance( ( Object[] ) null );

        assertEquals( 1, constructor.length );

    }
}
