package com.scholastic.classmags.migration.servlets;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.scholastic.classmags.migration.services.PastIssuesService;

/**
 * The Class PastIssuesServletTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { PastIssuesServlet.class } )
public class PastIssuesServletTest {

    private PastIssuesServlet pastIssuesServlet;
    private PastIssuesService pastIssuesService;
    private SlingHttpServletRequest request;
    private SlingHttpServletResponse response;
    private PrintWriter printWriter;

    /** The logger. */
    private Logger logger;

    @Before
    public void setUp() throws Exception {
        pastIssuesServlet = Whitebox.newInstance( PastIssuesServlet.class );

        logger = mock( Logger.class );
        request = mock( SlingHttpServletRequest.class );
        response = mock( SlingHttpServletResponse.class );
        printWriter = mock( PrintWriter.class );
        pastIssuesService = mock( PastIssuesService.class );

        Whitebox.setInternalState( PastIssuesServlet.class, "LOG", logger );
        Whitebox.setInternalState( pastIssuesServlet, pastIssuesService );

        when( request.getParameter( "path" ) ).thenReturn( "/content/scienceworld/past-issues" );
        when( pastIssuesService.getPastIssuesJson( "/content/scienceworld/past-issues" ) ).thenReturn( "{field1:{}}" );
        when( response.getWriter() ).thenReturn( printWriter );
    }

    @Test
    public void testPastIssuesServlet() throws Exception {
        // execute logic
        Whitebox.invokeMethod( pastIssuesServlet, "doGet", request, response );

        // verify logic
        verify( response, times( 1 ) ).setContentType( "application/json" );
    }

    @Test
    public void testPastIssuesServletWhenPagePathIsBlank() throws Exception {
        // setup logic ( test-specific )
        when( request.getParameter( "path" ) ).thenReturn( "" );

        // execute logic
        Whitebox.invokeMethod( pastIssuesServlet, "doGet", request, response );

        // verify logic
        verify( response, times( 1 ) ).setContentType( "application/json" );
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final PastIssuesServlet pastIssuesServlet = new PastIssuesServlet();

        // verify logic
        assertNotNull( pastIssuesServlet );
    }
}