package com.scholastic.classmags.migration.utils;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.apache.sling.api.SlingException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.day.cq.wcm.api.NameConstants;

/**
 * JUnit for InternalURLFormatter.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( {InternalURLFormatter.class, ResourceUtil.class} )
public class InternalURLFormatterTest {

    /** The Constant INTERNAL_URL. */
    private static final String INTERNAL_URL = "/content/geometrixx-outdoors/en/men";

    /** The Constant URL_EXTENSION. */
    private static final String URL_EXTENSION = ".html";

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;

    /** The sling exception. */
    @Mock
    private SlingException slingException;
    
    /** The logger. */
    Logger logger;
    
    /**
     * Initial Setup Method.
     */
    @Before
    public void setUp() {
        logger = mock(Logger.class);

        Whitebox.setInternalState(InternalURLFormatter.class, "LOG", logger);
    }

    /**
     * Test format URL for internal url.
     */
    @Test
    public void testFormatURLForInternalUrlPage() {

        when( mockResourceResolver.resolve( INTERNAL_URL ) ).thenReturn( mockResource );
        when( mockResource.isResourceType( NameConstants.NT_PAGE ) ).thenReturn( true );
        when( mockResourceResolver.map( INTERNAL_URL ) ).thenReturn( INTERNAL_URL );

        assertEquals( INTERNAL_URL.concat( URL_EXTENSION ),
                InternalURLFormatter.formatURL( mockResourceResolver, INTERNAL_URL ) );
    }

    /**
     * Test format URL for internal url no page.
     */
    @Test
    public void testFormatURLForInternalUrlNoPage() {

        when( mockResourceResolver.resolve( INTERNAL_URL ) ).thenReturn( mockResource );
        when( mockResource.isResourceType( NameConstants.NT_PAGE ) ).thenReturn( false );
        when( mockResourceResolver.map( INTERNAL_URL ) ).thenReturn( INTERNAL_URL );

        assertEquals( INTERNAL_URL, InternalURLFormatter.formatURL( mockResourceResolver, INTERNAL_URL ) );
    }

    /**
     * Test format URL for external url.
     */
    @Test
    public void testFormatURLForExternalUrl() {

        when( mockResourceResolver.resolve( INTERNAL_URL ) ).thenReturn( null );

        assertEquals( INTERNAL_URL, InternalURLFormatter.formatURL( mockResourceResolver, INTERNAL_URL ) );
    }

    /**
     * Test format URL with exception.
     */
    @Test
    public void testFormatURLWithException() {

        when( mockResourceResolver.resolve( INTERNAL_URL ) ).thenThrow( slingException );

        assertEquals( INTERNAL_URL, InternalURLFormatter.formatURL( mockResourceResolver, INTERNAL_URL ) );
        
        verify(logger).error("Exception resolving path", slingException);
    }

    /**
     * Test format URL for internal url with non existing resource.
     */
    @Test
    public void testFormatURLForInternalUrlWithNonExistingResource() {

        when( mockResourceResolver.resolve( INTERNAL_URL ) ).thenReturn( mockResource );
        PowerMockito.mockStatic( ResourceUtil.class );
        PowerMockito.when( ResourceUtil.isNonExistingResource( mockResource ) ).thenReturn( true );

        assertEquals( INTERNAL_URL, InternalURLFormatter.formatURL( mockResourceResolver, INTERNAL_URL ) );
    }
}
