package com.scholastic.classmags.migration.controllers;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import javax.script.Bindings;

import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.iterators.TransformIterator;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.request.RequestPathInfo;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.adobe.granite.ui.components.Value;
import com.adobe.granite.ui.components.ds.DataSource;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.scholastic.classmags.migration.services.ArticleResourceDataService;
import com.scholastic.classmags.migration.services.TeachingResourcesService;

@RunWith( MockitoJUnitRunner.class )
public class ArticleResourceDropdownUseTest {

    private static final String PATH = "/testPath";

    /** The Constant TEST_SORTING_DATE. */
    private static final String TEST_KEY = "testKey";

    /** The Constant TEST_DESC. */
    private static final String TEST_DATA = "testData";

    /** The current issue hero use. */
    private ArticleResourceDropdownUse articleResourceDropdownUse;

    /** The request. */
    @Mock
    private SlingHttpServletRequest mockRequest;

    /** The bindings. */
    @Mock
    private Bindings mockBindings;

    /** The sling script helper. */
    @Mock
    private SlingScriptHelper mockSlingScriptHelper;

    @Mock
    private ArticleResourceDataService articleResourceDataService;

    /** The teaching resources service. */
    @Mock
    private TeachingResourcesService teachingResourcesService;

    /** The page. */
    @Mock
    private Page mockPage;

    /** The asset data map. */
    @Mock
    private ValueMap mockValueMap;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;

    @Mock
    private PageManager mockPageManager;

    private Map< String, String > resourcesMap;

    @Mock
    private DataSource mockDataSource;

    @Mock
    private RequestPathInfo mockRequestPathInfo;

    private TransformIterator transformIterator;
    
    @Mock
    private Transformer transformer;
    
    @Mock
    private Object mockObject;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        articleResourceDropdownUse = new ArticleResourceDropdownUse();
        resourcesMap = new HashMap< >();
        resourcesMap.put( TEST_KEY, TEST_DATA );
    }

    @Test
    public void testActivate() {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockRequest.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockRequest.getAttribute( Value.CONTENTPATH_ATTRIBUTE ) ).thenReturn( PATH );
        when( mockResourceResolver.getResource( PATH ) ).thenReturn( mockResource );
        when( mockResourceResolver.adaptTo( PageManager.class ) ).thenReturn( mockPageManager );
        when( mockPageManager.getContainingPage( mockResource ) ).thenReturn( mockPage );
        when( mockSlingScriptHelper.getService( ArticleResourceDataService.class ) )
                .thenReturn( articleResourceDataService );
        when( articleResourceDataService.fetchArticleResourceData( mockRequest, mockPage ) ).thenReturn( resourcesMap );
        transformIterator = new TransformIterator( resourcesMap.keySet().iterator(), transformer );
        transformIterator.getTransformer().transform( mockObject );
        articleResourceDropdownUse.init( mockBindings );
        articleResourceDropdownUse.activate();
        articleResourceDropdownUse.getDropdown();
    }

    @Test
    public void testActivateWithNoContentAttribute() {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockRequest.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockRequest.getAttribute( Value.CONTENTPATH_ATTRIBUTE ) ).thenReturn( null );
        when( mockRequest.getRequestPathInfo() ).thenReturn( mockRequestPathInfo );
        when( mockRequestPathInfo.getSuffix() ).thenReturn( PATH );
        when( mockResourceResolver.getResource( PATH ) ).thenReturn( mockResource );
        when( mockResourceResolver.adaptTo( PageManager.class ) ).thenReturn( mockPageManager );
        when( mockPageManager.getContainingPage( mockResource ) ).thenReturn( mockPage );
        when( mockSlingScriptHelper.getService( ArticleResourceDataService.class ) )
                .thenReturn( articleResourceDataService );
        when( articleResourceDataService.fetchArticleResourceData( mockRequest, mockPage ) ).thenReturn( resourcesMap );

        articleResourceDropdownUse.init( mockBindings );
        articleResourceDropdownUse.activate();
    }

    @Test
    public void testActivateWithNoSuffixPath() {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockRequest.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockRequest.getAttribute( Value.CONTENTPATH_ATTRIBUTE ) ).thenReturn( null );
        when( mockRequest.getRequestPathInfo() ).thenReturn( mockRequestPathInfo );
        when( mockRequestPathInfo.getSuffix() ).thenReturn( null );

        articleResourceDropdownUse.init( mockBindings );
        articleResourceDropdownUse.activate();
    }

    @Test
    public void testActivateWithEmptySuffixPath() {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockRequest.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockRequest.getAttribute( Value.CONTENTPATH_ATTRIBUTE ) ).thenReturn( null );
        when( mockRequest.getRequestPathInfo() ).thenReturn( mockRequestPathInfo );
        when( mockRequestPathInfo.getSuffix() ).thenReturn( "" );

        articleResourceDropdownUse.init( mockBindings );
        articleResourceDropdownUse.activate();
    }

    @Test
    public void testActivateNoPageManager() {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockRequest.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockRequest.getAttribute( Value.CONTENTPATH_ATTRIBUTE ) ).thenReturn( PATH );
        when( mockResourceResolver.getResource( PATH ) ).thenReturn( mockResource );
        when( mockResourceResolver.adaptTo( PageManager.class ) ).thenReturn( null );

        articleResourceDropdownUse.init( mockBindings );
        articleResourceDropdownUse.activate();
    }

    @Test
    public void testActivateWithNoScriptHelper() {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( null );
        when( mockRequest.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockRequest.getAttribute( Value.CONTENTPATH_ATTRIBUTE ) ).thenReturn( PATH );
        when( mockResourceResolver.getResource( PATH ) ).thenReturn( mockResource );
        when( mockResourceResolver.adaptTo( PageManager.class ) ).thenReturn( mockPageManager );
        when( mockPageManager.getContainingPage( mockResource ) ).thenReturn( mockPage );

        articleResourceDropdownUse.init( mockBindings );
        articleResourceDropdownUse.activate();
    }

    @Test
    public void testActivateWithNoService() {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockRequest.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockRequest.getAttribute( Value.CONTENTPATH_ATTRIBUTE ) ).thenReturn( PATH );
        when( mockResourceResolver.getResource( PATH ) ).thenReturn( mockResource );
        when( mockResourceResolver.adaptTo( PageManager.class ) ).thenReturn( mockPageManager );
        when( mockPageManager.getContainingPage( mockResource ) ).thenReturn( mockPage );
        when( mockSlingScriptHelper.getService( ArticleResourceDataService.class ) ).thenReturn( null );

        articleResourceDropdownUse.init( mockBindings );
        articleResourceDropdownUse.activate();
    }
}
