package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for ObjectivesAndSkillsUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { ObjectivesAndSkillsUse.class, CommonUtils.class } )
public class ObjectivesAndSkillsUseTest extends BaseComponentTest {

    private ObjectivesAndSkillsUse objectivesAndSkillsUse;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        objectivesAndSkillsUse = Whitebox.newInstance( ObjectivesAndSkillsUse.class );
        stubCommon( objectivesAndSkillsUse );

        PowerMockito.mockStatic( CommonUtils.class );
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final ObjectivesAndSkillsUse objectivesAndSkillsUse = new ObjectivesAndSkillsUse();

        // verify logic
        assertNotNull( objectivesAndSkillsUse );
    }

    @Test
    public void testObjectivesAndSkills() throws Exception {
        // setup logic ( test-specific )
        List< ValueMap > objectivesAndSkillsMapList = new ArrayList<>();
        ValueMap objectivesAndSkillsMap = mock( ValueMap.class );
        objectivesAndSkillsMapList.add( objectivesAndSkillsMap );

        when( CommonUtils.fetchMultiFieldData( resource, "objectivesAndSkills" ) )
                .thenReturn( objectivesAndSkillsMapList );
        when( objectivesAndSkillsMap.get( "title", StringUtils.EMPTY ) ).thenReturn( "Sample Title" );
        when( objectivesAndSkillsMap.get( "description", StringUtils.EMPTY ) ).thenReturn( "Sample Description" );

        // execute logic
        Whitebox.invokeMethod( objectivesAndSkillsUse, "activate" );

        // verify
        assertEquals( objectivesAndSkillsUse.getObjectivesAndSkillsList().get( 0 ).getTitle(), "Sample Title" );
        assertEquals( objectivesAndSkillsUse.getObjectivesAndSkillsList().get( 0 ).getDescription(),
                "Sample Description" );
    }
}
