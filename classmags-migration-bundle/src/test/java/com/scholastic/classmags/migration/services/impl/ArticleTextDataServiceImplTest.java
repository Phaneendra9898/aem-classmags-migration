package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * JUnit for ArticleTextDataServiceImpl
 */
@RunWith( MockitoJUnitRunner.class )
public class ArticleTextDataServiceImplTest {

    /** The vocab data service. */
    private ArticleTextDataServiceImpl articleTextDataService;

    /** The Constant PATH. */
    private static final String PATH = "/content";

    /** The Constant TEST_VOCAB_WORD. */
    private static final String TEST_DATA = "TestData";

    /** The mock property text title. */
    @Mock
    private Property mockPropertyTextTitle;

    /** The resource path config service. */
    @Mock
    private ResourcePathConfigService resourcePathConfigService;

    /** The resource resolver factory. */
    @Mock
    private ResourceResolverFactory resourceResolverFactory;

    /** The resource resolver. */
    @Mock
    private ResourceResolver resourceResolver;

    /** The res. */
    @Mock
    private Resource res;

    /** The session. */
    @Mock
    private Session session;

    /** The query builder. */
    @Mock
    private QueryBuilder queryBuilder;

    /** The mock query map. */
    @Mock
    private Map< String, String > mockQueryMap;

    /** The query. */
    @Mock
    private Query query;

    /** The search result. */
    @Mock
    private SearchResult searchResult;

    /** The mock vocab nodes. */
    @Mock
    private List< Hit > mockArticleLexileLevels;

    /** The hit. */
    @Mock
    private Hit hit;

    /** The mock node. */
    @Mock
    private Node mockNode;

    /** The mock article data. */
    private Value[] mockArticleData = new Value[2];

    /** The mock value. */
    @Mock
    private Value mockValue;

    /**
     * Sets the up.
     *
     * @throws LoginException
     *             the login exception
     * @throws RepositoryException
     *             the repository exception
     */
    @Before
    public void setUp() throws LoginException, RepositoryException {
        articleTextDataService = new ArticleTextDataServiceImpl();
        mockQueryMap = new HashMap< String, String >();
        mockArticleLexileLevels = new ArrayList< >();
        mockQueryMap.put( ClassMagsMigrationConstants.QUERY_PATH, PATH );
        mockQueryMap.put( ClassMagsMigrationConstants.RESULTS_LIMIT, ClassMagsMigrationConstants.QUERY_LOWER_BOUND );
        mockQueryMap.put( ClassMagsMigrationConstants.QUERY_ORDER_BY, ClassMagsMigrationConstants.QUERY_NODE_NAME );
        mockArticleLexileLevels.add( hit );
    }

    /**
     * Test fetch article data.
     *
     * @throws LoginException
     *             the login exception
     * @throws RepositoryException
     *             the repository exception
     */
    @Test
    public void testFetchArticleData() throws LoginException, RepositoryException {
        List< String > articleDataList = new ArrayList< >();
        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );
        mockArticleData[ 0 ] = mockValue;
        mockArticleData[ 1 ] = mockValue;

        articleTextDataService.setResourceResolverFactory( resourceResolverFactory );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( Session.class ) ).thenReturn( session );
        articleTextDataService.setQueryBuilder( queryBuilder );
        when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ), session ) ).thenReturn( query );
        when( query.getResult() ).thenReturn( searchResult );
        when( searchResult.getHits() ).thenReturn( mockArticleLexileLevels );
        when( hit.getNode() ).thenReturn( mockNode );
        when( mockNode.hasProperty( ClassMagsMigrationConstants.ARTICLE_TEXT_TITLE ) ).thenReturn( true );
        when( mockNode.getProperty( ClassMagsMigrationConstants.ARTICLE_TEXT_TITLE ) )
                .thenReturn( mockPropertyTextTitle );
        when( mockPropertyTextTitle.getValues() ).thenReturn( mockArticleData );
        when( mockArticleData[ 1 ].getString() ).thenReturn( TEST_DATA );

        articleDataList = articleTextDataService.fetchArticleData( PATH );
        assertEquals( TEST_DATA, articleDataList.get( 0 ) );
    }

    /**
     * Test fetch article data with no article lexile levels.
     *
     * @throws LoginException
     *             the login exception
     * @throws RepositoryException
     *             the repository exception
     */
    @Test
    public void testFetchArticleDataWithNoArticleLexileLevels() throws LoginException, RepositoryException {
        List< String > articleDataList = new ArrayList< >();
        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );
        mockArticleData[ 0 ] = mockValue;
        mockArticleData[ 1 ] = mockValue;

        articleTextDataService.setResourceResolverFactory( resourceResolverFactory );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( Session.class ) ).thenReturn( session );
        articleTextDataService.setQueryBuilder( queryBuilder );
        when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ), session ) ).thenReturn( query );
        when( query.getResult() ).thenReturn( searchResult );
        when( searchResult.getHits() ).thenReturn( null );

        articleDataList = articleTextDataService.fetchArticleData( PATH );
        assertEquals( true, articleDataList.isEmpty() );
    }

    /**
     * Test fetch article data with empty list.
     *
     * @throws LoginException
     *             the login exception
     * @throws RepositoryException
     *             the repository exception
     */
    @Test
    public void testFetchArticleDataWithEmptyList() throws LoginException, RepositoryException {
        List< String > articleDataList = new ArrayList< >();
        Map< String, Object > param = new HashMap< >();
        List< Hit > emptyArticleLexileLevels = new ArrayList< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );
        mockArticleData[ 0 ] = mockValue;
        mockArticleData[ 1 ] = mockValue;

        articleTextDataService.setResourceResolverFactory( resourceResolverFactory );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( Session.class ) ).thenReturn( session );
        articleTextDataService.setQueryBuilder( queryBuilder );
        when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ), session ) ).thenReturn( query );
        when( query.getResult() ).thenReturn( searchResult );
        when( searchResult.getHits() ).thenReturn( emptyArticleLexileLevels );

        articleDataList = articleTextDataService.fetchArticleData( PATH );
        assertEquals( true, articleDataList.isEmpty() );
    }

    /**
     * Test fetch article data with empty node.
     *
     * @throws LoginException
     *             the login exception
     * @throws RepositoryException
     *             the repository exception
     */
    @Test
    public void testFetchArticleDataWithEmptyNode() throws LoginException, RepositoryException {
        List< String > articleDataList = new ArrayList< >();
        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );
        mockArticleData[ 0 ] = mockValue;
        mockArticleData[ 1 ] = mockValue;

        articleTextDataService.setResourceResolverFactory( resourceResolverFactory );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( Session.class ) ).thenReturn( session );
        articleTextDataService.setQueryBuilder( queryBuilder );
        when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ), session ) ).thenReturn( query );
        when( query.getResult() ).thenReturn( searchResult );
        when( searchResult.getHits() ).thenReturn( mockArticleLexileLevels );
        when( hit.getNode() ).thenReturn( mockNode );
        when( mockNode.hasProperty( ClassMagsMigrationConstants.ARTICLE_TEXT_TITLE ) ).thenReturn( false );

        articleDataList = articleTextDataService.fetchArticleData( PATH );
        assertEquals( true, articleDataList.isEmpty() );
    }

    /**
     * Test fetch article data empty article data.
     *
     * @throws LoginException
     *             the login exception
     * @throws RepositoryException
     *             the repository exception
     */
    @Test
    public void testFetchArticleDataEmptyArticleData() throws LoginException, RepositoryException {
        List< String > articleDataList = new ArrayList< >();
        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        articleTextDataService.setResourceResolverFactory( resourceResolverFactory );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( Session.class ) ).thenReturn( session );
        articleTextDataService.setQueryBuilder( queryBuilder );
        when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ), session ) ).thenReturn( query );
        when( query.getResult() ).thenReturn( searchResult );
        when( searchResult.getHits() ).thenReturn( mockArticleLexileLevels );
        when( hit.getNode() ).thenReturn( mockNode );
        when( mockNode.hasProperty( ClassMagsMigrationConstants.ARTICLE_TEXT_TITLE ) ).thenReturn( true );
        when( mockNode.getProperty( ClassMagsMigrationConstants.ARTICLE_TEXT_TITLE ) )
                .thenReturn( mockPropertyTextTitle );
        when( mockPropertyTextTitle.getValues() ).thenReturn( mockArticleData );

        articleDataList = articleTextDataService.fetchArticleData( PATH );
        assertEquals( true, articleDataList.isEmpty() );
    }

    /**
     * Test fetch article data no article data.
     *
     * @throws LoginException
     *             the login exception
     * @throws RepositoryException
     *             the repository exception
     */
    @Test
    public void testFetchArticleDataNoArticleData() throws LoginException, RepositoryException {
        List< String > articleDataList = new ArrayList< >();
        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );
        mockArticleData = null;

        articleTextDataService.setResourceResolverFactory( resourceResolverFactory );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( Session.class ) ).thenReturn( session );
        articleTextDataService.setQueryBuilder( queryBuilder );
        when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ), session ) ).thenReturn( query );
        when( query.getResult() ).thenReturn( searchResult );
        when( searchResult.getHits() ).thenReturn( mockArticleLexileLevels );
        when( hit.getNode() ).thenReturn( mockNode );
        when( mockNode.hasProperty( ClassMagsMigrationConstants.ARTICLE_TEXT_TITLE ) ).thenReturn( true );
        when( mockNode.getProperty( ClassMagsMigrationConstants.ARTICLE_TEXT_TITLE ) )
                .thenReturn( mockPropertyTextTitle );
        when( mockPropertyTextTitle.getValues() ).thenReturn( mockArticleData );

        articleDataList = articleTextDataService.fetchArticleData( PATH );
        assertEquals( true, articleDataList.isEmpty() );
    }
}
