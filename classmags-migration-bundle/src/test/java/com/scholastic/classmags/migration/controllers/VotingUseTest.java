package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.settings.SlingSettingsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.services.UpdateVotingNodeService;
import com.scholastic.classmags.migration.services.impl.UpdateVotingNodeServiceImpl;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for VotingUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { VotingUse.class, CommonUtils.class } )
public class VotingUseTest extends BaseComponentTest {

    private static final String DATE_FORMAT = "MMMM yyyy";
    private static final String SCIENCEWORLD_CONFIG_RESOURCE_PATH = "/content/classroom_magazines/admin/sw-data-page";
    private static final String GLOBAL_CONFIG_RESOURCE_PATH = "/content/classroom_magazines/admin/global-cm-data-page";
    private static final String CURRENT_PAGE_PATH = "/content/issue-path/article";
    private static final String MAGAZINE_NAME = "scienceworld";

    private VotingUse votingUse;
    private ResourcePathConfigService resourcePathConfigService;
    private PropertyConfigService propertyConfigService;
    private SlingSettingsService slingSettingsService;
    private UpdateVotingNodeService updateVotingNodeService;
    private MagazineProps magazineProps;
    private Resource choicesResource;
    private List< ValueMap > answerChoices;
    private ValueMap properties;
    private Date disableVotingDate;
    private Logger log;

    /**
     * Setup.
     * 
     * @throws ClassmagsMigrationBaseException
     */
    @Before
    public void setup() throws ClassmagsMigrationBaseException {
        votingUse = Whitebox.newInstance( VotingUse.class );

        stubCommon( votingUse );

        PowerMockito.mockStatic( CommonUtils.class );

        magazineProps = mock( MagazineProps.class );
        resourcePathConfigService = mock( ResourcePathConfigService.class );
        propertyConfigService = mock( PropertyConfigService.class );
        slingSettingsService = mock( SlingSettingsService.class );
        updateVotingNodeService = mock( UpdateVotingNodeService.class );
        choicesResource = mock( Resource.class );
        properties = mock( ValueMap.class );
        log = mock( Logger.class );

        Whitebox.setInternalState( VotingUse.class, "LOG", log );

        Calendar dt = Calendar.getInstance();
        dt.setTimeZone( TimeZone.getTimeZone( "UTC" ) );
        dt.set( Calendar.YEAR, 2016 );
        dt.set( Calendar.MONTH, 02 );
        dt.set( Calendar.DAY_OF_MONTH, 29 );
        disableVotingDate = dt.getTime();

        Set< String > runModes = new HashSet< >();
        runModes.add( "author" );

        answerChoices = new ArrayList< >();
        ValueMap answerOne = mock( ValueMap.class );
        ValueMap answerTwo = mock( ValueMap.class );

        answerChoices.add( answerOne );
        answerChoices.add( answerTwo );

        when( answerOne.get( "option" ) ).thenReturn( "Yes" );
        when( answerTwo.get( "option" ) ).thenReturn( "No" );

        when( slingScriptHelper.getService( ResourcePathConfigService.class ) ).thenReturn( resourcePathConfigService );
        when( slingScriptHelper.getService( MagazineProps.class ) ).thenReturn( magazineProps );
        when( slingScriptHelper.getService( PropertyConfigService.class ) ).thenReturn( propertyConfigService );
        when( slingScriptHelper.getService( SlingSettingsService.class ) ).thenReturn( slingSettingsService );
        when( slingScriptHelper.getService( UpdateVotingNodeService.class ) ).thenReturn( updateVotingNodeService );
        when( currentPage.getPath() ).thenReturn( CURRENT_PAGE_PATH );
        when( magazineProps.getMagazineName( CURRENT_PAGE_PATH ) ).thenReturn( MAGAZINE_NAME );
        when( resourcePathConfigService.getGlobalConfigResourcePath( MAGAZINE_NAME ) )
                .thenReturn( SCIENCEWORLD_CONFIG_RESOURCE_PATH );
        when( slingSettingsService.getRunModes() ).thenReturn( runModes );
        when( CommonUtils.getDateFormat( propertyConfigService, SCIENCEWORLD_CONFIG_RESOURCE_PATH ) )
                .thenReturn( DATE_FORMAT );
        when( CommonUtils.isAuthorMode( runModes ) ).thenReturn( true );
        when( resource.getChild( "answerOptions" ) ).thenReturn( choicesResource );
        when( CommonUtils.fetchMultiFieldData( choicesResource ) ).thenReturn( answerChoices );
        when( resource.getValueMap() ).thenReturn( properties );
        when( properties.get( "disableVotingDate", Date.class ) ).thenReturn( disableVotingDate );

    }

    @Test
    public void testVotingUse() throws Exception {
        // setup logic(test-specific)

        // execute logic
        Whitebox.invokeMethod( votingUse, "activate" );

        // verify logic
        assertEquals( 2, votingUse.getAnswerOptions().size() );
        assertEquals( "Yes", votingUse.getAnswerOptions().get( 0 ).get( "option" ) );
        assertEquals( "No", votingUse.getAnswerOptions().get( 1 ).get( "option" ) );
        assertEquals( String.valueOf( disableVotingDate.getTime() ), votingUse.getDisableVotingDate() );
    }

    @Test
    public void testVotingUseWhenSlingScriptHelperIsNull() throws Exception {
        // setup logic(test-specific)
        stub( PowerMockito.method( VotingUse.class, "getSlingScriptHelper" ) ).toReturn( null );

        // execute logic
        Whitebox.invokeMethod( votingUse, "activate" );

        // verify logic
        assertTrue( !votingUse.getDisableVotingDate().isEmpty() );
    }

    @Test
    public void testVotingUseWhenDisableVotingDateIsNull() throws Exception {
        // setup logic(test-specific)
        when( properties.get( "disableVotingDate", Date.class ) ).thenReturn( null );
        // execute logic
        Whitebox.invokeMethod( votingUse, "activate" );

        // verify logic
        assertTrue( votingUse.getDisableVotingDate().isEmpty() );
    }

    @Test
    public void testVotingUseWhenUpdateVotingNodeServiceIsNull() throws Exception {
        // setup logic(test-specific)
        when( slingScriptHelper.getService( UpdateVotingNodeService.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( votingUse, "activate" );

        // verify logic
        verify( updateVotingNodeService, never() ).updateVotingNode( resource );
    }

    @Test
    public void testVotingUseWhenSlingSettingsServiceIsNull() throws Exception {
        // setup logic(test-specific)
        when( slingScriptHelper.getService( SlingSettingsService.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( votingUse, "activate" );

        // verify logic
        verify( updateVotingNodeService, never() ).updateVotingNode( resource );
    }

    @Test( expected = ClassmagsMigrationBaseException.class )
    public void testVotingUseWhenPersistenceExceptionIsThrown() throws Exception {
        // setup logic(test-specific)
        PersistenceException e = new PersistenceException();
        UpdateVotingNodeService spy = Mockito.spy( new UpdateVotingNodeServiceImpl() );
        when( slingScriptHelper.getService( UpdateVotingNodeService.class ) ).thenReturn( spy );
        Mockito.doThrow( e ).when( spy ).updateVotingNode( any( Resource.class ) );

        // execute logic
        Whitebox.invokeMethod( votingUse, "activate" );

        // verify logic
        verify( log ).error( "Error while updating voting node", e );
    }

    @Test
    public void testCreateInstance() {
        // setup logic
        VotingUse votingUse = new VotingUse();

        // verify logic
        assertNotNull( votingUse );
    }
}
