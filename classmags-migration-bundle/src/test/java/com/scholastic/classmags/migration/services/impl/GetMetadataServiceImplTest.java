package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;

/**
 * JUnit for GetMetadataService.
 */
public class GetMetadataServiceImplTest {

    /** The Constant INPUT_KEY. */
    public static final String INPUT_KEY = "dc:title";

    /** The Constant TEST_DATA. */
    public static final String TEST_DATA = "TestData";

    /** The Constant PATH. */
    public static final String PATH = "/content/dam/scholastic/classroom-magazines/migration/videos/UPF-040416-Americas11Million.mp4";

    /** The get metadata service. */
    private GetMetadataServiceImpl getMetadataService;

    /** The sling http servlet request. */
    SlingHttpServletRequest slingHttpServletRequest;

    /** The resource resolver. */
    ResourceResolver resourceResolver;

    /** The res. */
    Resource res;

    /** The asset meta data path. */
    String assetMetaDataPath;

    /** The properties. */
    ValueMap properties;

    /**
     * Initial Setup.
     */
    @Before
    public void setUp() {
        getMetadataService = new GetMetadataServiceImpl();
        slingHttpServletRequest = mock( SlingHttpServletRequest.class );
        resourceResolver = mock( ResourceResolver.class );
        res = mock( Resource.class );
        properties = mock( ValueMap.class );
        assetMetaDataPath = PATH.concat( "/jcr:content/metadata" );
    }

    /**
     * Test get DAM property.
     */
    @Test
    public void testGetDAMProperty() {
        when( slingHttpServletRequest.getResourceResolver() ).thenReturn( resourceResolver );
        when( resourceResolver.getResource( assetMetaDataPath ) ).thenReturn( res );
        when( res.adaptTo( ValueMap.class ) ).thenReturn( properties );
        when( properties.get( INPUT_KEY, String.class ) ).thenReturn( TEST_DATA );
        String testProperty = getMetadataService.getDAMProperty( slingHttpServletRequest, INPUT_KEY, PATH );
        assertEquals( testProperty, TEST_DATA );
    }
}
