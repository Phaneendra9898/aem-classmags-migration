package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.services.GetMetadataService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * JUnit for ArticleResourcesDataServiceImplTest.
 */
@RunWith( MockitoJUnitRunner.class )
public class ArticleResourcesDataServiceImplTest {

    /** The article resources data service impl. */
    private ArticleResourcesDataServiceImpl articleResourcesDataServiceImpl;

    /** The Constant PATH. */
    private static final String PATH = "/testPath";

    /** The Constant TEST_DATA. */
    private static final String TEST_DATA = "testData";

    /** The Constant TEST_MAP_KEY. */
    private static final String TEST_MAP_KEY = "video - /testPath";

    /** The Constant TEST_MAP_DATA. */
    private static final String TEST_MAP_DATA = "Video - testData";

    /** The Constant TEST_PAGE_MAP_KEY. */
    private static final String TEST_PAGE_MAP_KEY = "blog - /testPath";

    /** The Constant TEST_PAGE_MAP_DATA. */
    private static final String TEST_PAGE_MAP_DATA = "Blog - testData";

    /** The Constant NO_TITLE. */
    private static final String TEST_MAP_NO_TITLE = "Blog - NO TITLE AVAILABLE";

    /** The Constant TEST_NO_TYPE_MAP_KEY. */
    private static final String TEST_NO_TYPE_MAP_KEY = "testData - /testPath";

    /** The Constant RESOURCES. */
    private static final String RESOURCES = "resources";

    /** The Constant RESOURCE_TYPE. */
    private static final String RESOURCE_TYPE = "resourceType";

    /** The Constant RESOURCE_PATH. */
    private static final String RESOURCE_PATH = "resourcePath";

    /** The mock sling request. */
    @Mock
    private SlingHttpServletRequest mockSlingRequest;

    /** The mock page. */
    @Mock
    private Page mockPage;

    /** The resource resolver factory. */
    @Mock
    private ResourceResolverFactory mockResourceResolverFactory;

    /** The mock get metadata service. */
    @Mock
    private GetMetadataService mockGetMetadataService;

    /** The resource path config service. */
    @Mock
    private ResourcePathConfigService resourcePathConfigService;

    /** The resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;

    /** The res. */
    @Mock
    private Resource mockResource;

    /** The properties. */
    @Mock
    private ValueMap mockValueMapProperties;

    /** The iterator. */
    @Mock
    private Iterator< Resource > mockIterator;

    /**
     * Sets the up.
     */
    @Before
    public void setUp() {
        articleResourcesDataServiceImpl = new ArticleResourcesDataServiceImpl();
    }

    /**
     * Test fetch article resource data.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testFetchArticleResourceData() throws LoginException {

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        articleResourcesDataServiceImpl.setResourceResolverFactory( mockResourceResolverFactory );
        articleResourcesDataServiceImpl.setGetMetadataService( mockGetMetadataService );

        when( mockPage.getParent() ).thenReturn( mockPage );
        when( mockPage.getPath() ).thenReturn( PATH );
        when( mockResourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( PATH.concat( ClassMagsMigrationConstants.JCR_CONTENT_NODE ) ) )
                .thenReturn( mockResource );
        when( mockResource.adaptTo( ValueMap.class ) ).thenReturn( mockValueMapProperties );
        when( mockValueMapProperties.get( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, StringUtils.EMPTY ) )
                .thenReturn( ClassMagsMigrationConstants.ISSUE_PAGE_TEMPLATE_PATH );
        when( mockResourceResolver.getResource( PATH.concat( ClassMagsMigrationConstants.JCR_ISSUE_CONFIGURATION ) ) )
                .thenReturn( mockResource );
        when( mockResource.getChild( RESOURCES ) ).thenReturn( mockResource );
        when( mockResource.listChildren() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMapProperties );
        when( mockValueMapProperties.get( RESOURCE_TYPE, StringUtils.EMPTY ) )
                .thenReturn( ClassMagsMigrationConstants.VIDEO );
        when( mockValueMapProperties.get( RESOURCE_PATH, StringUtils.EMPTY ) ).thenReturn( PATH );
        when( mockGetMetadataService.getDAMProperty( mockSlingRequest, DamConstants.DC_TITLE, PATH ) )
                .thenReturn( TEST_DATA );

        assertEquals( TEST_MAP_DATA, articleResourcesDataServiceImpl
                .fetchArticleResourceData( mockSlingRequest, mockPage ).get( TEST_MAP_KEY ) );
    }

    /**
     * Test fetch article resource data with custom message.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testFetchArticleResourceDataWithCustomMessage() throws LoginException {

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        articleResourcesDataServiceImpl.setResourceResolverFactory( mockResourceResolverFactory );
        articleResourcesDataServiceImpl.setGetMetadataService( mockGetMetadataService );

        when( mockPage.getParent() ).thenReturn( mockPage );
        when( mockPage.getPath() ).thenReturn( PATH );
        when( mockResourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( PATH.concat( ClassMagsMigrationConstants.JCR_CONTENT_NODE ) ) )
                .thenReturn( mockResource );
        when( mockResource.adaptTo( ValueMap.class ) ).thenReturn( mockValueMapProperties );
        when( mockValueMapProperties.get( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, StringUtils.EMPTY ) )
                .thenReturn( PATH );

        assertEquals( ClassMagsMigrationConstants.ARTICLE_RESOURCE_CUSTOM_MESSAGE,
                articleResourcesDataServiceImpl.fetchArticleResourceData( mockSlingRequest, mockPage )
                        .get( ClassMagsMigrationConstants.DATASOURCE_NO_CONFIGURATION ) );
    }

    /**
     * Test fetch article resource data with no resource.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testFetchArticleResourceDataWithNoResource() throws LoginException {

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        articleResourcesDataServiceImpl.setResourceResolverFactory( mockResourceResolverFactory );
        articleResourcesDataServiceImpl.setGetMetadataService( mockGetMetadataService );

        when( mockPage.getParent() ).thenReturn( mockPage );
        when( mockPage.getPath() ).thenReturn( PATH );
        when( mockResourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( PATH.concat( ClassMagsMigrationConstants.JCR_CONTENT_NODE ) ) )
                .thenReturn( null );

        assertEquals( ClassMagsMigrationConstants.ARTICLE_RESOURCE_CUSTOM_MESSAGE,
                articleResourcesDataServiceImpl.fetchArticleResourceData( mockSlingRequest, mockPage )
                        .get( ClassMagsMigrationConstants.DATASOURCE_NO_CONFIGURATION ) );
    }

    /**
     * Test fetch article resource data with no properties.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testFetchArticleResourceDataWithNoProperties() throws LoginException {

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        articleResourcesDataServiceImpl.setResourceResolverFactory( mockResourceResolverFactory );
        articleResourcesDataServiceImpl.setGetMetadataService( mockGetMetadataService );

        when( mockPage.getParent() ).thenReturn( mockPage );
        when( mockPage.getPath() ).thenReturn( PATH );
        when( mockResourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( PATH.concat( ClassMagsMigrationConstants.JCR_CONTENT_NODE ) ) )
                .thenReturn( mockResource );
        when( mockResource.adaptTo( ValueMap.class ) ).thenReturn( null );

        assertEquals( ClassMagsMigrationConstants.ARTICLE_RESOURCE_CUSTOM_MESSAGE,
                articleResourcesDataServiceImpl.fetchArticleResourceData( mockSlingRequest, mockPage )
                        .get( ClassMagsMigrationConstants.DATASOURCE_NO_CONFIGURATION ) );
    }

    /**
     * Test fetch article resource data with no JCR node.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testFetchArticleResourceDataWithNoJCRNode() throws LoginException {

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        articleResourcesDataServiceImpl.setResourceResolverFactory( mockResourceResolverFactory );
        articleResourcesDataServiceImpl.setGetMetadataService( mockGetMetadataService );

        when( mockPage.getParent() ).thenReturn( mockPage );
        when( mockPage.getPath() ).thenReturn( PATH );
        when( mockResourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( PATH.concat( ClassMagsMigrationConstants.JCR_CONTENT_NODE ) ) )
                .thenReturn( mockResource );
        when( mockResource.adaptTo( ValueMap.class ) ).thenReturn( mockValueMapProperties );
        when( mockValueMapProperties.get( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, StringUtils.EMPTY ) )
                .thenReturn( ClassMagsMigrationConstants.ISSUE_PAGE_TEMPLATE_PATH );
        when( mockResourceResolver.getResource( PATH.concat( ClassMagsMigrationConstants.JCR_ISSUE_CONFIGURATION ) ) )
                .thenReturn( null );

        assertEquals( 0,
                articleResourcesDataServiceImpl.fetchArticleResourceData( mockSlingRequest, mockPage ).size() );
    }

    /**
     * Test fetch article resource data with no child.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testFetchArticleResourceDataWithNoChild() throws LoginException {

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        articleResourcesDataServiceImpl.setResourceResolverFactory( mockResourceResolverFactory );
        articleResourcesDataServiceImpl.setGetMetadataService( mockGetMetadataService );

        when( mockPage.getParent() ).thenReturn( mockPage );
        when( mockPage.getPath() ).thenReturn( PATH );
        when( mockResourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( PATH.concat( ClassMagsMigrationConstants.JCR_CONTENT_NODE ) ) )
                .thenReturn( mockResource );
        when( mockResource.adaptTo( ValueMap.class ) ).thenReturn( mockValueMapProperties );
        when( mockValueMapProperties.get( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, StringUtils.EMPTY ) )
                .thenReturn( ClassMagsMigrationConstants.ISSUE_PAGE_TEMPLATE_PATH );
        when( mockResourceResolver.getResource( PATH.concat( ClassMagsMigrationConstants.JCR_ISSUE_CONFIGURATION ) ) )
                .thenReturn( mockResource );
        when( mockResource.getChild( RESOURCES ) ).thenReturn( null );

        assertEquals( 0,
                articleResourcesDataServiceImpl.fetchArticleResourceData( mockSlingRequest, mockPage ).size() );
    }

    /**
     * Test fetch article resource data with page type.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testFetchArticleResourceDataWithPageType() throws LoginException {

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        articleResourcesDataServiceImpl.setResourceResolverFactory( mockResourceResolverFactory );
        articleResourcesDataServiceImpl.setGetMetadataService( mockGetMetadataService );

        when( mockPage.getParent() ).thenReturn( mockPage );
        when( mockPage.getPath() ).thenReturn( PATH );
        when( mockResourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( PATH.concat( ClassMagsMigrationConstants.JCR_CONTENT_NODE ) ) )
                .thenReturn( mockResource );
        when( mockResource.adaptTo( ValueMap.class ) ).thenReturn( mockValueMapProperties );
        when( mockValueMapProperties.get( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, StringUtils.EMPTY ) )
                .thenReturn( ClassMagsMigrationConstants.ISSUE_PAGE_TEMPLATE_PATH );
        when( mockResourceResolver.getResource( PATH.concat( ClassMagsMigrationConstants.JCR_ISSUE_CONFIGURATION ) ) )
                .thenReturn( mockResource );
        when( mockResource.getChild( RESOURCES ) ).thenReturn( mockResource );
        when( mockResource.listChildren() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMapProperties );
        when( mockValueMapProperties.get( RESOURCE_TYPE, StringUtils.EMPTY ) )
                .thenReturn( ClassMagsMigrationConstants.BLOG );
        when( mockValueMapProperties.get( RESOURCE_PATH, StringUtils.EMPTY ) ).thenReturn( PATH );
        when( mockGetMetadataService.getPageProperty( mockSlingRequest, JcrConstants.JCR_TITLE, PATH ) )
                .thenReturn( TEST_DATA );

        assertEquals( TEST_PAGE_MAP_DATA, articleResourcesDataServiceImpl
                .fetchArticleResourceData( mockSlingRequest, mockPage ).get( TEST_PAGE_MAP_KEY ) );
    }

    /**
     * Test fetch article resource data no title.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testFetchArticleResourceDataNoTitle() throws LoginException {

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        articleResourcesDataServiceImpl.setResourceResolverFactory( mockResourceResolverFactory );
        articleResourcesDataServiceImpl.setGetMetadataService( mockGetMetadataService );

        when( mockPage.getParent() ).thenReturn( mockPage );
        when( mockPage.getPath() ).thenReturn( PATH );
        when( mockResourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( PATH.concat( ClassMagsMigrationConstants.JCR_CONTENT_NODE ) ) )
                .thenReturn( mockResource );
        when( mockResource.adaptTo( ValueMap.class ) ).thenReturn( mockValueMapProperties );
        when( mockValueMapProperties.get( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, StringUtils.EMPTY ) )
                .thenReturn( ClassMagsMigrationConstants.ISSUE_PAGE_TEMPLATE_PATH );
        when( mockResourceResolver.getResource( PATH.concat( ClassMagsMigrationConstants.JCR_ISSUE_CONFIGURATION ) ) )
                .thenReturn( mockResource );
        when( mockResource.getChild( RESOURCES ) ).thenReturn( mockResource );
        when( mockResource.listChildren() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMapProperties );
        when( mockValueMapProperties.get( RESOURCE_TYPE, StringUtils.EMPTY ) )
                .thenReturn( ClassMagsMigrationConstants.BLOG );
        when( mockValueMapProperties.get( RESOURCE_PATH, StringUtils.EMPTY ) ).thenReturn( PATH );
        when( mockGetMetadataService.getPageProperty( mockSlingRequest, JcrConstants.JCR_TITLE, PATH ) )
                .thenReturn( null );

        assertEquals( TEST_MAP_NO_TITLE, articleResourcesDataServiceImpl
                .fetchArticleResourceData( mockSlingRequest, mockPage ).get( TEST_PAGE_MAP_KEY ) );
    }

    /**
     * Test fetch article resource data no type.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testFetchArticleResourceDataNoType() throws LoginException {

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        articleResourcesDataServiceImpl.setResourceResolverFactory( mockResourceResolverFactory );
        articleResourcesDataServiceImpl.setGetMetadataService( mockGetMetadataService );

        when( mockPage.getParent() ).thenReturn( mockPage );
        when( mockPage.getPath() ).thenReturn( PATH );
        when( mockResourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( PATH.concat( ClassMagsMigrationConstants.JCR_CONTENT_NODE ) ) )
                .thenReturn( mockResource );
        when( mockResource.adaptTo( ValueMap.class ) ).thenReturn( mockValueMapProperties );
        when( mockValueMapProperties.get( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, StringUtils.EMPTY ) )
                .thenReturn( ClassMagsMigrationConstants.ISSUE_PAGE_TEMPLATE_PATH );
        when( mockResourceResolver.getResource( PATH.concat( ClassMagsMigrationConstants.JCR_ISSUE_CONFIGURATION ) ) )
                .thenReturn( mockResource );
        when( mockResource.getChild( RESOURCES ) ).thenReturn( mockResource );
        when( mockResource.listChildren() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMapProperties );
        when( mockValueMapProperties.get( RESOURCE_TYPE, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMapProperties.get( RESOURCE_PATH, StringUtils.EMPTY ) ).thenReturn( PATH );

        assertEquals( ClassMagsMigrationConstants.HYPHEN, articleResourcesDataServiceImpl
                .fetchArticleResourceData( mockSlingRequest, mockPage ).get( TEST_NO_TYPE_MAP_KEY ) );
    }
}
