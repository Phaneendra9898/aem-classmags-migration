package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.services.SubscriptionBenefitsService;

/**
 * JUnit for SubscriptionBenefitsUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { SubscriptionBenefitsUse.class } )
public class SubscriptionBenefitsUseTest extends BaseComponentTest {

    private static final String CURRENT_PATH = "/content/classroom_magazines/scienceworld/issues/_2015_16/092115/high-flier/jcr:content";

    /** The subscription benefits use. */
    private SubscriptionBenefitsUse subscriptionBenefitsUse;

    private SubscriptionBenefitsService subscriptionBenefitsService;

    private List< ValueMap > subscriptionBenefitsMapList;
    private ValueMap subscriptionBenefitsMap;

    private ClassmagsMigrationBaseException e;

    /** The logger. */
    Logger logger;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        subscriptionBenefitsUse = Whitebox.newInstance( SubscriptionBenefitsUse.class );
        stubCommon( subscriptionBenefitsUse );

        logger = mock( Logger.class );

        Whitebox.setInternalState( SubscriptionBenefitsUse.class, "LOG", logger );
    }

    @Test
    public void testSubscriptionBenefits() throws Exception {
        // setup logic ( test-specific )
        subscriptionBenefitsService = mock( SubscriptionBenefitsService.class );
        when( currentPage.getPath() ).thenReturn( CURRENT_PATH );
        when( slingScriptHelper.getService( SubscriptionBenefitsService.class ) )
                .thenReturn( subscriptionBenefitsService );

        createSubcriptionBenefitsMapList();

        when( subscriptionBenefitsMap.get( "subscriptionBenefitTitle", StringUtils.EMPTY ) )
                .thenReturn( "Sample Title" );
        when( subscriptionBenefitsMap.get( "subscriptionBenefitDescription", StringUtils.EMPTY ) )
                .thenReturn( "Sample Description" );
        when( subscriptionBenefitsMap.get( "icons", StringUtils.EMPTY ) ).thenReturn( "Sample Icon" );
        when( subscriptionBenefitsService.fetchSubscriptionBenefits( CURRENT_PATH ) )
                .thenReturn( subscriptionBenefitsMapList );

        // execute logic
        Whitebox.invokeMethod( subscriptionBenefitsUse, "activate" );

        // verify
        assertEquals( subscriptionBenefitsUse.getSubscriptionBenefitsDataList().get( 0 ).getSubscriptionBenefitIcon(),
                "Sample Icon" );
        assertEquals( subscriptionBenefitsUse.getSubscriptionBenefitsDataList().get( 0 ).getSubscriptionBenefitTitle(),
                "Sample Title" );
        assertEquals(
                subscriptionBenefitsUse.getSubscriptionBenefitsDataList().get( 0 ).getSubscriptionBenefitDescription(),
                "Sample Description" );
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final SubscriptionBenefitsUse subscriptionBenefitsUse = new SubscriptionBenefitsUse();

        // verify logic
        assertNotNull( subscriptionBenefitsUse );
    }

    @Test
    public void testWhenSlingScripthelperIsNull() throws Exception {
        // setup logic ( test-specific )
        stub( PowerMockito.method( subscriptionBenefitsUse.getClass(), "getSlingScriptHelper" ) ).toReturn( null );

        // execute logic
        Whitebox.invokeMethod( subscriptionBenefitsUse, "activate" );

        // verify logic
        assertNull( subscriptionBenefitsService );
    }

    @Test
    public void testWhenSubscriptionBenefitsServiceIsNull() throws Exception {
        // setup logic ( test-specific )
        when( slingScriptHelper.getService( SubscriptionBenefitsService.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( subscriptionBenefitsUse, "activate" );

        // verify logic
        assertEquals( subscriptionBenefitsUse.getSubscriptionBenefitsDataList().size(), 0 );
    }

    public void createSubcriptionBenefitsMapList() {

        subscriptionBenefitsMapList = new ArrayList<>();

        subscriptionBenefitsMap = mock( ValueMap.class );

        subscriptionBenefitsMapList.add( subscriptionBenefitsMap );
    }

    @Test
    public void testWhenClassmagsMigrationBaseException() throws Exception {
        // setup logic ( test-specific )

        e = new ClassmagsMigrationBaseException();

        subscriptionBenefitsService = mock( SubscriptionBenefitsService.class );

        when( currentPage.getPath() ).thenReturn( CURRENT_PATH );
        when( slingScriptHelper.getService( SubscriptionBenefitsService.class ) )
                .thenReturn( subscriptionBenefitsService );

        when( subscriptionBenefitsService.fetchSubscriptionBenefits( CURRENT_PATH ) )
                .thenThrow( new ClassmagsMigrationBaseException() );

        // execute logic
        Whitebox.invokeMethod( subscriptionBenefitsUse, "activate" );

        // verify logic
    }
}
