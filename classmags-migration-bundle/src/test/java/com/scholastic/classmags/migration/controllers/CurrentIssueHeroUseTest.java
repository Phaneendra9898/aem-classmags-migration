package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.Bindings;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.ClassmagsMigrationErrorCodes;
import com.scholastic.classmags.migration.models.CurrentIssueHero;
import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.services.CurrentIssueHeroDataService;
import com.scholastic.classmags.migration.services.TeachingResourcesService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * Junit for CurrentIssueHeroUse
 */
@RunWith( MockitoJUnitRunner.class )
public class CurrentIssueHeroUseTest {

    /** The Constant PATH. */
    private static final String ISSUE_PATH = "/testPath";
    
    /** The Constant PATH. */
    private static final String CURRENT_PAGE_PATH = "/content/issues/061116";

    /** The Constant TEST_SORTING_DATE. */
    private static final String TEST_SORTING_DATE = "October 25, 2016";

    /** The Constant TEST_DESC. */
    private static final String TEST_DESC = "testDesc";

    /** The Constant FEATURED_RESOURCES. */
    private static final String FEATURED_RESOURCES = "featuredResources";

    /** The current issue hero use. */
    private CurrentIssueHeroUse currentIssueHeroUse;

    /** The request. */
    @Mock
    private SlingHttpServletRequest request;
    
    /** The current issue hero. */
    @Mock
    private CurrentIssueHero currentIssueHero;

    /** The bindings. */
    @Mock
    private Bindings bindings;

    /** The sling script helper. */
    @Mock
    private SlingScriptHelper slingScriptHelper;

    /** The article text data service. */
    @Mock
    private CurrentIssueHeroDataService currentIssueHeroDataService;

    /** The teaching resources service. */
    @Mock
    private TeachingResourcesService teachingResourcesService;

    /** The page. */
    @Mock
    private Page page;

    /** The asset data map. */
    @Mock
    private ValueMap mockValueMap;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;

    /** The featured resources. */
    private Map< String, List< ResourcesObject > > featuredResources = new HashMap< >();

    /**
     * Setup.
     */
    @Before
    public void setup() {
        currentIssueHeroUse = new CurrentIssueHeroUse();
    }

    /**
     * Test get issue date.
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    @Test
    public void testGetIssueDate() throws ClassmagsMigrationBaseException {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( bindings.get( "resource" ) ).thenReturn( mockResource );
        when( request.getResource() ).thenReturn( mockResource );
        when( mockResource.getPath()).thenReturn( CURRENT_PAGE_PATH );
        when( request.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( ISSUE_PATH ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( "currentIssuePath", StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( slingScriptHelper.getService( CurrentIssueHeroDataService.class ) )
                .thenReturn( currentIssueHeroDataService );
        when( currentIssueHeroDataService.fetchIssueDate( ISSUE_PATH, CURRENT_PAGE_PATH ) ).thenReturn( TEST_SORTING_DATE );
        when( currentIssueHeroDataService.fetchIssueData( ISSUE_PATH ) ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.PP_DESCRIPTION, StringUtils.EMPTY ) )
                .thenReturn( TEST_DESC );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( page.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ClassMagsMigrationConstants.ISSUE_CONFIGURATION ) ).thenReturn( mockResource );
        when( slingScriptHelper.getService( TeachingResourcesService.class ) ).thenReturn( teachingResourcesService );
        when( teachingResourcesService.getTeachingResources( request, mockResource, FEATURED_RESOURCES ) )
                .thenReturn( featuredResources );

        currentIssueHeroUse.init( bindings );
        currentIssueHeroUse.activate();
        assertEquals( TEST_SORTING_DATE, currentIssueHeroUse.getIssueDate() );
    }

    /**
     * Test get current issue hero.
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    @Test
    public void testGetCurrentIssueHero() throws ClassmagsMigrationBaseException {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( request.getResource()).thenReturn( mockResource );
        when( mockResource.getPath()).thenReturn( CURRENT_PAGE_PATH );
        when( page.getPath() ).thenReturn( ISSUE_PATH );
        when( slingScriptHelper.getService( CurrentIssueHeroDataService.class ) )
                .thenReturn( currentIssueHeroDataService );
        when( currentIssueHeroDataService.fetchIssueDate( ISSUE_PATH, CURRENT_PAGE_PATH ) ).thenReturn( TEST_SORTING_DATE );
        when( currentIssueHeroDataService.fetchIssueData( ISSUE_PATH ) ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.PP_DESCRIPTION, StringUtils.EMPTY ) )
                .thenReturn( TEST_DESC );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( page.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ClassMagsMigrationConstants.ISSUE_CONFIGURATION ) ).thenReturn( mockResource );
        when( slingScriptHelper.getService( TeachingResourcesService.class ) ).thenReturn( teachingResourcesService );
        when( teachingResourcesService.getTeachingResources( request, mockResource, FEATURED_RESOURCES ) )
                .thenReturn( featuredResources );

        currentIssueHeroUse.init( bindings );
        currentIssueHeroUse.activate();
        currentIssueHeroUse.setCurrentIssueHero( currentIssueHero );
        assertEquals( currentIssueHero, currentIssueHeroUse.getCurrentIssueHero() );
    }

    /**
     * Test get issue date with exception.
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    @Test
    public void testGetIssueDateWithException() throws ClassmagsMigrationBaseException {

        ClassmagsMigrationBaseException classmagsMigrationBaseException = new ClassmagsMigrationBaseException(
                ClassmagsMigrationErrorCodes.LOGIN_ERROR );

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( bindings.get( "resource" ) ).thenReturn( mockResource );
        when( request.getResource() ).thenReturn( mockResource );
        when( request.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( ISSUE_PATH ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( "currentIssuePath", StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( slingScriptHelper.getService( CurrentIssueHeroDataService.class ) )
                .thenReturn( currentIssueHeroDataService );
        when( currentIssueHeroDataService.fetchIssueDate( ISSUE_PATH, CURRENT_PAGE_PATH ) ).thenThrow( classmagsMigrationBaseException );
        when( currentIssueHeroDataService.fetchIssueData( ISSUE_PATH ) ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.PP_DESCRIPTION, StringUtils.EMPTY ) )
                .thenReturn( TEST_DESC );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( page.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ClassMagsMigrationConstants.ISSUE_CONFIGURATION ) ).thenReturn( mockResource );
        when( slingScriptHelper.getService( TeachingResourcesService.class ) ).thenReturn( teachingResourcesService );
        when( teachingResourcesService.getTeachingResources( request, mockResource, FEATURED_RESOURCES ) )
                .thenReturn( featuredResources );

        currentIssueHeroUse.init( bindings );
        currentIssueHeroUse.activate();
        assertEquals( null, currentIssueHeroUse.getIssueDate() );
    }

    /**
     * Test get issue description.
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    @Test
    public void testGetIssueDescription() throws ClassmagsMigrationBaseException {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( bindings.get( "resource" ) ).thenReturn( mockResource );
        when( request.getResource() ).thenReturn( mockResource );
        when( request.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( ISSUE_PATH ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( "currentIssuePath", StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( slingScriptHelper.getService( CurrentIssueHeroDataService.class ) )
                .thenReturn( currentIssueHeroDataService );
        when( currentIssueHeroDataService.fetchIssueDate( ISSUE_PATH, CURRENT_PAGE_PATH ) ).thenReturn( TEST_SORTING_DATE );
        when( currentIssueHeroDataService.fetchIssueData( ISSUE_PATH ) ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.PP_DESCRIPTION, StringUtils.EMPTY ) )
                .thenReturn( TEST_DESC );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( page.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( JcrConstants.JCR_CONTENT ) ).thenReturn( mockResource );
        when( mockResource.getChild( ClassMagsMigrationConstants.ISSUE_CONFIGURATION ) ).thenReturn( mockResource );
        when( slingScriptHelper.getService( TeachingResourcesService.class ) ).thenReturn( teachingResourcesService );
        when( teachingResourcesService.getTeachingResources( request, mockResource, FEATURED_RESOURCES ) )
                .thenReturn( featuredResources );

        currentIssueHeroUse.init( bindings );
        currentIssueHeroUse.activate();
        assertEquals( TEST_DESC, currentIssueHeroUse.getIssueDescription() );
    }

    /**
     * Test get issue image.
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    @Test
    public void testGetIssueImage() throws ClassmagsMigrationBaseException {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( bindings.get( "resource" ) ).thenReturn( mockResource );
        when( request.getResource() ).thenReturn( mockResource );
        when( request.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( ISSUE_PATH ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( "currentIssuePath", StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( slingScriptHelper.getService( CurrentIssueHeroDataService.class ) )
                .thenReturn( currentIssueHeroDataService );
        when( currentIssueHeroDataService.fetchIssueDate( ISSUE_PATH, CURRENT_PAGE_PATH ) ).thenReturn( TEST_SORTING_DATE );
        when( currentIssueHeroDataService.fetchIssueData( ISSUE_PATH ) ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.PP_DESCRIPTION, StringUtils.EMPTY ) )
                .thenReturn( TEST_DESC );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( page.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ClassMagsMigrationConstants.ISSUE_CONFIGURATION ) ).thenReturn( mockResource );
        when( slingScriptHelper.getService( TeachingResourcesService.class ) ).thenReturn( teachingResourcesService );
        when( teachingResourcesService.getTeachingResources( request, mockResource, FEATURED_RESOURCES ) )
                .thenReturn( featuredResources );

        currentIssueHeroUse.init( bindings );
        currentIssueHeroUse.activate();
        assertEquals( ISSUE_PATH, currentIssueHeroUse.getIssueImage() );
    }

    /**
     * Test get featured resources.
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    @Test
    public void testGetFeaturedResources() throws ClassmagsMigrationBaseException {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( bindings.get( "resource" ) ).thenReturn( mockResource );
        when( request.getResource() ).thenReturn( mockResource );
        when( request.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( ISSUE_PATH ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( "currentIssuePath", StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( slingScriptHelper.getService( CurrentIssueHeroDataService.class ) )
                .thenReturn( currentIssueHeroDataService );
        when( currentIssueHeroDataService.fetchIssueDate( ISSUE_PATH, CURRENT_PAGE_PATH ) ).thenReturn( TEST_SORTING_DATE );
        when( currentIssueHeroDataService.fetchIssueData( ISSUE_PATH ) ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.PP_DESCRIPTION, StringUtils.EMPTY ) )
                .thenReturn( TEST_DESC );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( page.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ClassMagsMigrationConstants.ISSUE_CONFIGURATION ) ).thenReturn( mockResource );
        when( slingScriptHelper.getService( TeachingResourcesService.class ) ).thenReturn( teachingResourcesService );
        when( teachingResourcesService.getTeachingResources( request, mockResource, FEATURED_RESOURCES ) )
                .thenReturn( featuredResources );

        currentIssueHeroUse.init( bindings );
        currentIssueHeroUse.activate();
        assertEquals( featuredResources, currentIssueHeroUse.getFeaturedResources() );
    }

    /**
     * Test activate with no sling script helper.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testActivateWithNoSlingScriptHelper() throws Exception {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( null );
        when( bindings.get( "resource" ) ).thenReturn( mockResource );
        when( request.getResource() ).thenReturn( mockResource );
        when( request.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( ISSUE_PATH ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( "currentIssuePath", StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );

        currentIssueHeroUse.init( bindings );
        currentIssueHeroUse.activate();
        assertEquals( null, currentIssueHeroUse.getIssueDate() );
    }

    /**
     * Test activate with no service.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testActivateWithNoService() throws Exception {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( bindings.get( "resource" ) ).thenReturn( mockResource );
        when( request.getResource() ).thenReturn( mockResource );
        when( request.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( ISSUE_PATH ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( "currentIssuePath", StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( slingScriptHelper.getService( CurrentIssueHeroDataService.class ) ).thenReturn( null );

        currentIssueHeroUse.init( bindings );
        currentIssueHeroUse.activate();
        assertEquals( null, currentIssueHeroUse.getIssueDate() );
    }

    /**
     * Test activate with current page null.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testActivateWithCurrentPageNull() throws Exception {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( null );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( bindings.get( "resource" ) ).thenReturn( mockResource );
        when( request.getResource() ).thenReturn( mockResource );
        when( request.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( ISSUE_PATH ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( "currentIssuePath", StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( slingScriptHelper.getService( CurrentIssueHeroDataService.class ) ).thenReturn( null );

        currentIssueHeroUse.init( bindings );
        currentIssueHeroUse.activate();
        assertEquals( null, currentIssueHeroUse.getIssueDate() );
    }

    /**
     * Test get featured resources with no resource.
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    @Test
    public void testGetFeaturedResourcesWithNoResource() throws ClassmagsMigrationBaseException {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( bindings.get( "resource" ) ).thenReturn( mockResource );
        when( request.getResource() ).thenReturn( mockResource );
        when( request.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( ISSUE_PATH ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( "currentIssuePath", StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( slingScriptHelper.getService( CurrentIssueHeroDataService.class ) )
                .thenReturn( currentIssueHeroDataService );
        when( currentIssueHeroDataService.fetchIssueDate( ISSUE_PATH, CURRENT_PAGE_PATH ) ).thenReturn( TEST_SORTING_DATE );
        when( currentIssueHeroDataService.fetchIssueData( ISSUE_PATH ) ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.PP_DESCRIPTION, StringUtils.EMPTY ) )
                .thenReturn( TEST_DESC );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( mockResource.getChild( JcrConstants.JCR_CONTENT ) ).thenReturn( null );
        when( page.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ClassMagsMigrationConstants.ISSUE_CONFIGURATION ) ).thenReturn( null );

        currentIssueHeroUse.init( bindings );
        currentIssueHeroUse.activate();
        assertEquals( 0, currentIssueHeroUse.getFeaturedResources().size() );
    }

    /**
     * Test get featured resources with no issue resource.
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    @Test
    public void testGetFeaturedResourcesWithNoIssueResource() throws ClassmagsMigrationBaseException {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( bindings.get( "resource" ) ).thenReturn( mockResource );
        when( request.getResource() ).thenReturn( mockResource );
        when( request.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( ISSUE_PATH ) ).thenReturn( null );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( "currentIssuePath", StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( slingScriptHelper.getService( CurrentIssueHeroDataService.class ) )
                .thenReturn( currentIssueHeroDataService );
        when( currentIssueHeroDataService.fetchIssueDate( ISSUE_PATH, CURRENT_PAGE_PATH ) ).thenReturn( TEST_SORTING_DATE );
        when( currentIssueHeroDataService.fetchIssueData( ISSUE_PATH ) ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.PP_DESCRIPTION, StringUtils.EMPTY ) )
                .thenReturn( TEST_DESC );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( mockResource.getChild( JcrConstants.JCR_CONTENT ) ).thenReturn( null );
        when( page.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ClassMagsMigrationConstants.ISSUE_CONFIGURATION ) ).thenReturn( null );

        currentIssueHeroUse.init( bindings );
        currentIssueHeroUse.activate();
        assertEquals( 0, currentIssueHeroUse.getFeaturedResources().size() );
    }

    /**
     * Test get featured resources with no JCR resource.
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    @Test
    public void testGetFeaturedResourcesWithNoJCRResource() throws ClassmagsMigrationBaseException {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( bindings.get( "resource" ) ).thenReturn( mockResource );
        when( request.getResource() ).thenReturn( mockResource );
        when( request.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( ISSUE_PATH ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( "currentIssuePath", StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( slingScriptHelper.getService( CurrentIssueHeroDataService.class ) )
                .thenReturn( currentIssueHeroDataService );
        when( currentIssueHeroDataService.fetchIssueDate( ISSUE_PATH, CURRENT_PAGE_PATH ) ).thenReturn( TEST_SORTING_DATE );
        when( currentIssueHeroDataService.fetchIssueData( ISSUE_PATH ) ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.PP_DESCRIPTION, StringUtils.EMPTY ) )
                .thenReturn( TEST_DESC );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( mockResource.getChild( JcrConstants.JCR_CONTENT ) ).thenReturn( mockResource );
        when( page.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ClassMagsMigrationConstants.ISSUE_CONFIGURATION ) ).thenReturn( null );

        currentIssueHeroUse.init( bindings );
        currentIssueHeroUse.activate();
        assertEquals( 0, currentIssueHeroUse.getFeaturedResources().size() );
    }

    /**
     * Test get featured resources with no service.
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    @Test
    public void testGetFeaturedResourcesWithNoService() throws ClassmagsMigrationBaseException {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "currentPage" ) ).thenReturn( page );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( bindings.get( "resource" ) ).thenReturn( mockResource );
        when( request.getResource() ).thenReturn( mockResource );
        when( request.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockResourceResolver.getResource( ISSUE_PATH ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( "currentIssuePath", StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( slingScriptHelper.getService( CurrentIssueHeroDataService.class ) )
                .thenReturn( currentIssueHeroDataService );
        when( currentIssueHeroDataService.fetchIssueDate( ISSUE_PATH, CURRENT_PAGE_PATH ) ).thenReturn( TEST_SORTING_DATE );
        when( currentIssueHeroDataService.fetchIssueData( ISSUE_PATH ) ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.PP_DESCRIPTION, StringUtils.EMPTY ) )
                .thenReturn( TEST_DESC );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) ).thenReturn( ISSUE_PATH );
        when( mockResource.getChild( JcrConstants.JCR_CONTENT ) ).thenReturn( mockResource );
        when( page.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ClassMagsMigrationConstants.ISSUE_CONFIGURATION ) ).thenReturn( mockResource );
        when( slingScriptHelper.getService( TeachingResourcesService.class ) ).thenReturn( null );

        currentIssueHeroUse.init( bindings );
        currentIssueHeroUse.activate();
        assertEquals( null, currentIssueHeroUse.getFeaturedResources() );
    }
}
