package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.models.Issue;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.RecentIssuesService;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for RecentIssuesUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { RecentIssuesUse.class, CommonUtils.class } )
public class RecentIssuesUseTest extends BaseComponentTest {

    private static final String NODE_NAME = "issue-paths";
    private static final String CURRENT_PAGE_PATH = "/content/classroom_magazines/scienceworld/home-page-logged-in";
    private static final String SCIENCE_WORLD = "scienceworld";
    private static final String IMAGE_PATH = "/content/dam/scholastic/classroom-magazines/migration/images/Beach.png";
    private static final String ISSUE_DATE = "test-display-date";

    private RecentIssuesUse recentIssuesUse;
    private MagazineProps magazineProps;
    private RecentIssuesService recentIssuesService;
    private List< ValueMap > issueLinks;
    private Resource issueListResource;
    private ValueMap issueLink;
    private List< Issue > issues;
    private Issue issueOne;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        recentIssuesUse = Whitebox.newInstance( RecentIssuesUse.class );

        stubCommon( recentIssuesUse );

        PowerMockito.mockStatic( CommonUtils.class );
        magazineProps = mock( MagazineProps.class );
        recentIssuesService = mock( RecentIssuesService.class );

        issueLinks = new ArrayList< >();
        issueLink = mock( ValueMap.class );
        issueListResource = mock( Resource.class );

        issueLinks.add( issueLink );

        issues = new ArrayList< >();
        issueOne = new Issue();
        issueOne.setImgsrc( IMAGE_PATH );
        issueOne.setIssuedate( ISSUE_DATE );
        issues.add( issueOne );

        when( resource.getChild( NODE_NAME ) ).thenReturn( issueListResource );
        when( CommonUtils.fetchMultiFieldData( issueListResource ) ).thenReturn( issueLinks );
        when( slingScriptHelper.getService( MagazineProps.class ) ).thenReturn( magazineProps );
        when( slingScriptHelper.getService( RecentIssuesService.class ) ).thenReturn( recentIssuesService );
        when( currentPage.getPath() ).thenReturn( CURRENT_PAGE_PATH );
        when( magazineProps.getMagazineName( CURRENT_PAGE_PATH ) ).thenReturn( SCIENCE_WORLD );
        when( recentIssuesService.getRecentIssues( SCIENCE_WORLD, issueLinks ) ).thenReturn( issues );

    }

    @Test
    public void testIssueLinkUse() throws Exception {

        // execute logic
        Whitebox.invokeMethod( recentIssuesUse, "activate" );

        // verify logic
        assertEquals( 1, recentIssuesUse.getRecentIssues().size() );
        assertEquals( IMAGE_PATH, recentIssuesUse.getRecentIssues().get( 0 ).getImgsrc() );
        assertEquals( ISSUE_DATE, recentIssuesUse.getRecentIssues().get( 0 ).getIssuedate() );
    }

    @Test
    public void testIssueLinkUseWhenSlingScriptHelperIsNull() throws Exception {
        // setup logic ( test - specific)
        stub( PowerMockito.method( recentIssuesUse.getClass(), "getSlingScriptHelper" ) ).toReturn( null );

        // execute logic
        Whitebox.invokeMethod( recentIssuesUse, "activate" );

        // verify logic
        assertNull( recentIssuesUse.getRecentIssues() );
    }

    @Test
    public void testIssueLinkUseWhenCurrentPageIsNull() throws Exception {
        // setup logic ( test - specific)
        stub( PowerMockito.method( recentIssuesUse.getClass(), "getCurrentPage" ) ).toReturn( null );

        // execute logic
        Whitebox.invokeMethod( recentIssuesUse, "activate" );

        // verify logic
        assertNull( recentIssuesUse.getRecentIssues() );
    }

    @Test
    public void testIssueLinkUseWhenMagazinePropsIsNull() throws Exception {
        // setup logic ( test - specific)
        when( slingScriptHelper.getService( MagazineProps.class ) ).thenReturn( null );
        when( recentIssuesService.getRecentIssues( StringUtils.EMPTY, issueLinks ) ).thenReturn( issues );

        // execute logic
        Whitebox.invokeMethod( recentIssuesUse, "activate" );

        // verify logic
        assertEquals( 1, recentIssuesUse.getRecentIssues().size() );
        assertEquals( IMAGE_PATH, recentIssuesUse.getRecentIssues().get( 0 ).getImgsrc() );
        assertEquals( ISSUE_DATE, recentIssuesUse.getRecentIssues().get( 0 ).getIssuedate() );
    }

    @Test
    public void testIssueLinkUseWhenRecentIssuesIsNull() throws Exception {
        // setup logic ( test - specific)
        when( slingScriptHelper.getService( RecentIssuesService.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( recentIssuesUse, "activate" );

        // verify logic
        assertNull( recentIssuesUse.getRecentIssues() );
    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final RecentIssuesUse recentIssuesUse = new RecentIssuesUse();

        // verify logic
        assertNotNull( recentIssuesUse );

    }

}
