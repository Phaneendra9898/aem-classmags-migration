package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.script.Bindings;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.scholastic.classmags.migration.models.VocabData;
import com.scholastic.classmags.migration.services.VocabDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * JUnit for VocabularyUse.
 */
@RunWith( MockitoJUnitRunner.class )
public class VocabularyUseTest {

    /** The Constant PATH. */
    private static final String PATH = "/content";

    /** The Constant TEST_KEY. */
    private static final String TEST_KEY = "TestKey";

    /** The vocabulary use. */
    private VocabularyUse vocabularyUse;

    /** The bindings. */
    @Mock
    private Bindings bindings;

    /** The current resource. */
    @Mock
    private Resource currentResource;

    /** The request. */
    @Mock
    private SlingHttpServletRequest request;

    /** The sling script helper. */
    @Mock
    private SlingScriptHelper slingScriptHelper;

    /** The vocab data service. */
    @Mock
    private VocabDataService vocabDataService;

    /** The vocab data map. */
    @Mock
    private Map< String, VocabData > vocabDataMap;

    /** The vocabdata. */
    @Mock
    private VocabData vocabdata;

    /**
     * Setup.
     *
     * @throws RepositoryException
     *             the repository exception
     * @throws LoginException
     *             the login exception
     */
    @Before
    public void setup() throws RepositoryException, LoginException {
        vocabularyUse = new VocabularyUse();
        vocabDataMap = new HashMap< >();
        vocabDataMap.put( TEST_KEY, vocabdata );
    }

    /**
     * Test get vocab data map.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetVocabDataMap() throws Exception {

        when( bindings.get( "resource" ) ).thenReturn( currentResource );
        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( request.getResource() ).thenReturn( currentResource );
        when( currentResource.getPath() ).thenReturn( PATH );
        when( slingScriptHelper.getService( VocabDataService.class ) ).thenReturn( vocabDataService );
        when( vocabDataService.fetchVocabData( PATH.concat( ClassMagsMigrationConstants.VOCAB_NODE_PATH ) ) )
                .thenReturn( vocabDataMap );

        vocabularyUse.init( bindings );
        vocabularyUse.activate();
        assertEquals( vocabdata, vocabularyUse.getVocabDataMap().get( TEST_KEY ) );
    }

    /**
     * Test get vocab data map with no sling script helper.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetVocabDataMapWithNoSlingScriptHelper() throws Exception {

        when( bindings.get( "resource" ) ).thenReturn( currentResource );
        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "sling" ) ).thenReturn( null );

        vocabularyUse.init( bindings );
        vocabularyUse.activate();
        assertEquals( true, vocabularyUse.getVocabDataMap().isEmpty() );
    }

    /**
     * Test get vocab data map with no service.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetVocabDataMapWithNoService() throws Exception {

        when( bindings.get( "resource" ) ).thenReturn( currentResource );
        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( request.getResource() ).thenReturn( currentResource );
        when( currentResource.getPath() ).thenReturn( PATH );
        when( slingScriptHelper.getService( VocabDataService.class ) ).thenReturn( null );

        vocabularyUse.init( bindings );
        vocabularyUse.activate();
        assertEquals( true, vocabularyUse.getVocabDataMap().isEmpty() );
    }
}
