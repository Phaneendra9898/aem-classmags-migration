package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * JUnit for IssueService.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { IssueServiceImpl.class, CommonUtils.class, InternalURLFormatter.class } )
public class IssueServiceImplTest {

    public static final String PARENT_PATH = "/content/parent-issue";
    public static final String PARENT_NODE = "/content/parent-issue/jcr:content";
    public static final String ISSUE_PAGE_TEMPLATE = "/apps/scholastic/classroom-magazines-migration/templates/issue-page";
    public static final String BASE_PAGE_TEMPLATE = "/apps/scholastic/classroom-magazines-migration/templates/base-page";
    public static final String GLOBAL_DATA_CONFIG_PAGE = "/content/classroom-magazines-admin/global-data-config-page";
    public static final String ISSUE_DATE = "November 16";
    public static final String CQ_TEMPLATE_KEY = "cq:template";
    public static final String SORTING_DATE_KEY = "jcr:sortingDateFormat";
    public static final String SORTING_DATE_FORMAT = "MMMM YYYY";
    public static final String HTML_SUFFIX = ".html";

    /** The issue service. */
    private IssueServiceImpl issueService;

    /** The resource path config service. */
    private ResourcePathConfigService resourcePathConfigService;

    /** The property config service. */
    private PropertyConfigService propertyConfigService;

    /** The current page */
    private Page currentPage;

    /** The parent page */
    private Page parentPage;

    /** The sling http servlet request. */
    SlingHttpServletRequest request;

    /** The resource resolver. */
    ResourceResolver resourceResolver;

    /** The parent resource. */
    Resource parentResource;

    /** The properties. */
    ValueMap properties;

    /**
     * Initial Setup.
     */
    @Before
    public void setUp() {
        issueService = new IssueServiceImpl();
        request = mock( SlingHttpServletRequest.class );
        resourceResolver = mock( ResourceResolver.class );
        parentResource = mock( Resource.class );
        properties = mock( ValueMap.class );
        resourcePathConfigService = mock( ResourcePathConfigService.class );
        propertyConfigService = mock( PropertyConfigService.class );

        PowerMockito.mockStatic( CommonUtils.class );
        PowerMockito.mockStatic( InternalURLFormatter.class);

        Whitebox.setInternalState( issueService, propertyConfigService );
        Whitebox.setInternalState( issueService, resourcePathConfigService );

        currentPage = mock( Page.class );
        parentPage = mock( Page.class );

        when( currentPage.getParent() ).thenReturn( parentPage );
        when( parentPage.getPath() ).thenReturn( PARENT_PATH );
        when( request.getResourceResolver() ).thenReturn( resourceResolver );
        when( resourceResolver.getResource( PARENT_NODE ) ).thenReturn( parentResource );
        when( parentResource.adaptTo( ValueMap.class ) ).thenReturn( properties );
        when( properties.get( CQ_TEMPLATE_KEY, String.class ) ).thenReturn( ISSUE_PAGE_TEMPLATE );
        when( InternalURLFormatter.formatURL( resourceResolver, PARENT_PATH )).thenReturn(PARENT_PATH + HTML_SUFFIX);

    }

    /**
     * Test get Parent issue path.
     */
    @Test
    public void testGetParentIssuePath() {

        // execute logic
        String parentIssuePath = issueService.getParentIssuePath( request, currentPage );

        // verify logic
        assertEquals( parentIssuePath, PARENT_PATH + HTML_SUFFIX );
    }

    /**
     * Test get issue date.
     * 
     * @throws LoginException
     */
    @Test
    public void testGetParentIssueDate() throws LoginException {

        // setup logic(test-specific)
        when( CommonUtils.getDataPath( resourcePathConfigService, "" ) ).thenReturn( GLOBAL_DATA_CONFIG_PAGE );
        when( propertyConfigService.getDataConfigProperty( SORTING_DATE_KEY, GLOBAL_DATA_CONFIG_PAGE ) )
                .thenReturn( SORTING_DATE_FORMAT );
        when( CommonUtils.getDisplayDate( properties, SORTING_DATE_FORMAT ) ).thenReturn( ISSUE_DATE );

        // execute logic
        String parentIssueDate = issueService.getParentIssueDate( request, currentPage, "" );

        // verify logic
        assertEquals( parentIssueDate, ISSUE_DATE );
    }

    /**
     * Test Parent issue path when parent resource is null.
     */
    @Test
    public void testParentIssuePathWhenParentResourceIsNull() {

        // setup logic( test-specific)
        when( resourceResolver.getResource( PARENT_NODE ) ).thenReturn( null );

        // execute logic
        String parentIssuePath = issueService.getParentIssuePath( request, currentPage );

        // verify logic
        assertTrue( parentIssuePath.isEmpty() );
    }

    /**
     * Test Parent issue path when article is not within an issue.
     */
    @Test
    public void testParentIssuePathWhenArticleIsNotWithinIssue() {

        // setup logic( test-specific)
        when( properties.get( CQ_TEMPLATE_KEY, String.class ) ).thenReturn( BASE_PAGE_TEMPLATE );

        // execute logic
        String parentIssuePath = issueService.getParentIssuePath( request, currentPage );

        // verify logic
        assertTrue( parentIssuePath.isEmpty() );
    }

    /**
     * Test Parent issue date when parent resource is null.
     * 
     * @throws LoginException
     */
    @Test
    public void testParentIssueDateWhenParentResourceIsNull() throws LoginException {

        // setup logic( test-specific)
        when( resourceResolver.getResource( PARENT_NODE ) ).thenReturn( null );

        // execute logic
        String parentIssueDate = issueService.getParentIssueDate( request, currentPage, "" );

        // verify logic
        assertTrue( parentIssueDate.isEmpty() );
    }
}
