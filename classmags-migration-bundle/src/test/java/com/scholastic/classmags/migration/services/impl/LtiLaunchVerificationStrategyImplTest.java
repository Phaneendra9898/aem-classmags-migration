package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.SortedMap;
import java.util.TreeMap;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

public class LtiLaunchVerificationStrategyImplTest {

    private static final String ANY_VALID_SHARED_SECRET = "TZKQl9qToKK1dp2G2G4zcKp2meIa";
    private static final String ANY_VALID_CONSUMER_KEY = "l0JkeOfrC2g2ALqB1dtghOjuxP8a";
    private static final String STAFF_LAUNCH_STRING = "com/scholastic/sda/ltiTeacherLaunch.json";
    private static final String STUDENT_LAUNCH_STRING = "com/scholastic/sda/ltiStudentLaunch.json";
    private static final String launchUrl = "http://localhost:8080/srd-portal/launch";
    private static final String oAuthSignature = "vpYFCpCvEbfDGsN2N9PilpmkUxM=";
    private static final String oAuthStudentSignature = "17bSlfa3E/yFb2wq/Mln/Sx7hLU=";
    public static final String CUSTOM_DP_LAUNCH = "custom_dp_launch";

    @InjectMocks
    private LtiLaunchVerificationStrategyImpl defaultBasicLtiLaunchVerificationStrategy;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks( this );
        defaultBasicLtiLaunchVerificationStrategy.setLtiSharedSecret( ANY_VALID_SHARED_SECRET );
    }

    @Test
    public void shouldPerformOAuthValidationForValidStudentOauthSignature() throws Exception {
        SortedMap< String, String > ltiLaunchOAuthDto = creatLtiLaunchOAuthDtoAsAStudent(
                getStudentLtiCustomParameterFromExternalJsonResource() );
        boolean isValidSignature = defaultBasicLtiLaunchVerificationStrategy.performOAuthSignatureValidation(
                ltiLaunchOAuthDto, launchUrl, oAuthStudentSignature, ANY_VALID_CONSUMER_KEY );
        assertTrue( "Student signature is not valid", !isValidSignature );
    }

    @Test
    public void shouldPerformOAuthValidationForValidTeacherOauthSignature() throws Exception {
        SortedMap< String, String > ltiLaunchOAuthmap = creatLtiLaunchOAuthDtoAsATeacher(
                getTeacherLtiCustomParameterFromExternalJsonResource() );
        boolean isValidSignature = defaultBasicLtiLaunchVerificationStrategy.performOAuthSignatureValidation(
                ltiLaunchOAuthmap, launchUrl, oAuthStudentSignature, ANY_VALID_CONSUMER_KEY );
        assertTrue( "Teacher signature is not valid", !isValidSignature );
    }

    @Test
    public void shouldPerformOAuthSignatureValidationAsATeacherWhenParametersAreSetAndCustomLtiParameterIncludesCarriageReturnAndLineFeed()
            throws Exception {
        SortedMap< String, String > ltiLaunchOAuthmap = creatLtiLaunchOAuthDtoAsATeacher(
                getTeacherLtiCustomParameterThatIncludesCarriageReturnAndLineFeed() );
        boolean isValidSignature = defaultBasicLtiLaunchVerificationStrategy.performOAuthSignatureValidation(
                ltiLaunchOAuthmap, launchUrl, "3xNYIx/Z2GuEB3KN+qQApYu70vY=", ANY_VALID_CONSUMER_KEY );
        System.out.println( isValidSignature );
        assertTrue( "Teacher signature is not valid", !isValidSignature );
    }

    @Test
    public void shouldPerformOAuthSignatureValidationAsAStudentWhenParametersAreSetAndCustomLtiParameterIncludeLineFeedOnly()
            throws Exception {
        SortedMap< String, String > ltiLaunchOAuthDto = creatLtiLaunchOAuthDtoAsAStudent(
                getStudentLtiCustomParamterThatIncludesLineFeedOnly() );
        boolean isValidSignature = defaultBasicLtiLaunchVerificationStrategy.performOAuthSignatureValidation(
                ltiLaunchOAuthDto, launchUrl, oAuthStudentSignature, ANY_VALID_CONSUMER_KEY );
        assertTrue( "Student signature is not valid", !isValidSignature );
    }

    @Test
    public void shouldPerformOAuthSignatureValidationAsATeacherWhenParametersAreSetAndCustomLtiParameterIncludeLineFeedOnly()
            throws Exception {
        SortedMap< String, String > ltiLaunchOAuthmap = creatLtiLaunchOAuthDtoAsATeacher(
                getTeacherLtiCustomParamterThatIncludesLineFeedOnly() );
        boolean isValidSignature = defaultBasicLtiLaunchVerificationStrategy.performOAuthSignatureValidation(
                ltiLaunchOAuthmap, launchUrl, oAuthSignature, ANY_VALID_CONSUMER_KEY );
        assertTrue( "Teacher signature is not valid", !isValidSignature );
    }

    private String getStudentLtiCustomParameterFromExternalJsonResource() throws Exception {
        return loadString( STUDENT_LAUNCH_STRING );
    }

    private String getTeacherLtiCustomParameterFromExternalJsonResource() throws Exception {
        return loadString( STAFF_LAUNCH_STRING );
    }

    private String loadString( String string ) throws URISyntaxException, IOException {
        Path path = Paths.get( getClass().getClassLoader().getResource( string ).toURI() );
        return new String( Files.readAllBytes( path ), Charset.defaultCharset() );
    }

    private String getStudentLtiCustomParamterThatIncludesLineFeedOnly() throws Exception {
        return "{\n  \"identifiers\" : {\n    \"studentId\" : \"561\"\n  },\n  \"orgContext\" : {\n    \"organization\" : {\n      \"id\" : \"83\",\n      \"name\" : \"Sweet Home High School\",\n      \"orgType\" : \"school\",\n      \"lastModifiedDate\" : \"2016-02-01T16:58:12.000-0500\"\n    },\n    \"entitlements\" : [ {\n      \"id\" : \"9\",\n      \"name\" : \"Digital Short Reads\",\n      \"applicationCode\" : \"digitalshortreads\",\n      \"url\" : \"http://localhost:8080/srd-portal/launch\",\n      \"thumbnail\" : \"/resources/images/product_icons/dsr.png\",\n      \"active\" : true\n    } ],\n    \"currentSchoolCalendar\" : {\n      \"description\" : \"2015-16\",\n      \"startDate\" : \"2015-07-31T20:00:00.000-0400\",\n      \"endDate\" : \"2016-07-30T20:00:00.000-0400\"\n    },\n    \"roles\" : [ \"student\" ]\n  },\n  \"launchContext\" : {\n    \"application\" : {\n      \"id\" : \"9\",\n      \"name\" : \"Digital Short Reads\",\n      \"applicationCode\" : \"digitalshortreads\",\n      \"url\" : \"http://localhost:8080/srd-portal/launch\",\n      \"thumbnail\" : \"/resources/images/product_icons/dsr.png\",\n      \"active\" : true\n    },\n    \"productEntitlements\" : [ {\n      \"id\" : \"22\",\n      \"name\" : \"Short Read Digital Trail Pack\",\n      \"productCode\" : \"8888833445566\",\n      \"applicationId\" : \"9\",\n      \"active\" : true,\n      \"includesStudentAccess\" : true\n    } ],\n    \"role\" : \"student\"\n  },\n  \"studentContext\" : {\n    \"grade\" : {\n      \"scholasticGradeCode\" : \"7\",\n      \"gradeSystemCode\" : \"USA\",\n      \"localGradeName\" : \"Grade 7\"\n    },\n    \"enrollments\" : [ {\n      \"id\" : \"40\",\n      \"organizationId\" : \"83\",\n      \"staff\" : {\n        \"primaryTeacherId\" : \"560\"\n      },\n      \"students\" : [ ],\n      \"nickname\" : \"DSR Class 1\",\n      \"lowGrade\" : \"7\",\n      \"highGrade\" : \"7\",\n      \"active\" : true,\n      \"studentAuthenticationMethod\" : {\n        \"type\" : \"easyLogin\",\n        \"enabled\" : false,\n        \"requirePassword\" : false\n      },\n      \"identifiers\" : { },\n      \"displayName\" : \"DSR Class 1\"\n    } ]\n  },\n  \"sessionContext\" : {\n    \"sessionType\" : \"DAS\",\n    \"sessionToken\" : \"b3cb8e55-af0e-4b67-ace3-d225d0f2f976\"\n  }\n}";
    }

    private String getTeacherLtiCustomParameterThatIncludesCarriageReturnAndLineFeed() throws Exception {
        return "{\r\n  \"identifiers\" : {\r\n    \"staffId\" : \"560\"\r\n  },\r\n  \"orgContext\" : {\r\n    \"organization\" : {\r\n      \"id\" : \"83\",\r\n      \"name\" : \"Sweet Home High School\",\r\n      \"orgType\" : \"school\",\r\n      \"lastModifiedDate\" : \"2016-02-01T16:58:12.000-0500\"\r\n    },\r\n    \"entitlements\" : [ {\r\n      \"id\" : \"9\",\r\n      \"name\" : \"Digital Short Reads\",\r\n      \"applicationCode\" : \"digitalshortreads\",\r\n      \"url\" : \"http://localhost:8080/srd-portal/launch\",\r\n      \"thumbnail\" : \"/resources/images/product_icons/dsr.png\",\r\n      \"active\" : true\r\n    } ],\r\n    \"currentSchoolCalendar\" : {\r\n      \"description\" : \"2015-16\",\r\n      \"startDate\" : \"2015-07-31T20:00:00.000-0400\",\r\n      \"endDate\" : \"2016-07-30T20:00:00.000-0400\"\r\n    },\r\n    \"roles\" : [ \"teacher\" ]\r\n  },\r\n  \"launchContext\" : {\r\n    \"application\" : {\r\n      \"id\" : \"9\",\r\n      \"name\" : \"Digital Short Reads\",\r\n      \"applicationCode\" : \"digitalshortreads\",\r\n      \"url\" : \"http://localhost:8080/srd-portal/launch\",\r\n      \"thumbnail\" : \"/resources/images/product_icons/dsr.png\",\r\n      \"active\" : true\r\n    },\r\n    \"productEntitlements\" : [ {\r\n      \"id\" : \"22\",\r\n      \"name\" : \"Short Read Digital Trail Pack\",\r\n      \"productCode\" : \"8888833445566\",\r\n      \"applicationId\" : \"9\",\r\n      \"active\" : true,\r\n      \"includesStudentAccess\" : true\r\n    } ],\r\n    \"role\" : \"teacher\"\r\n  },\r\n  \"teacherContext\" : {\r\n    \"classes\" : [ {\r\n      \"id\" : \"40\",\r\n      \"organizationId\" : \"83\",\r\n      \"staff\" : {\r\n        \"primaryTeacherId\" : \"560\"\r\n      },\r\n      \"students\" : [ ],\r\n      \"nickname\" : \"DSR Class 1\",\r\n      \"lowGrade\" : \"7\",\r\n      \"highGrade\" : \"7\",\r\n      \"active\" : true,\r\n      \"studentAuthenticationMethod\" : {\r\n        \"type\" : \"easyLogin\",\r\n        \"enabled\" : false,\r\n        \"requirePassword\" : false\r\n      },\r\n      \"identifiers\" : { },\r\n      \"displayName\" : \"DSR Class 1\"\r\n    }, {\r\n      \"id\" : \"41\",\r\n      \"organizationId\" : \"83\",\r\n      \"staff\" : {\r\n        \"primaryTeacherId\" : \"560\"\r\n      },\r\n      \"students\" : [ ],\r\n      \"nickname\" : \"DSR class 2\",\r\n      \"lowGrade\" : \"2\",\r\n      \"highGrade\" : \"2\",\r\n      \"active\" : true,\r\n      \"studentAuthenticationMethod\" : {\r\n        \"type\" : \"easyLogin\",\r\n        \"enabled\" : false,\r\n        \"requirePassword\" : false\r\n      },\r\n      \"identifiers\" : { },\r\n      \"displayName\" : \"DSR class 2\"\r\n    }, {\r\n      \"id\" : \"11035\",\r\n      \"organizationId\" : \"83\",\r\n      \"staff\" : {\r\n        \"primaryTeacherId\" : \"560\"\r\n      },\r\n      \"students\" : [ ],\r\n      \"nickname\" : \"DP Class 3\",\r\n      \"lowGrade\" : \"8\",\r\n      \"highGrade\" : \"8\",\r\n      \"active\" : true,\r\n      \"studentAuthenticationMethod\" : {\r\n        \"type\" : \"easyLogin\",\r\n        \"enabled\" : false,\r\n        \"requirePassword\" : false\r\n      },\r\n      \"identifiers\" : { },\r\n      \"displayName\" : \"DP Class 3\"\r\n    }, {\r\n      \"id\" : \"11485\",\r\n      \"organizationId\" : \"83\",\r\n      \"staff\" : {\r\n        \"primaryTeacherId\" : \"560\"\r\n      },\r\n      \"students\" : [ ],\r\n      \"nickname\" : \"New Demo Class\",\r\n      \"lowGrade\" : \"10\",\r\n      \"highGrade\" : \"10\",\r\n      \"active\" : true,\r\n      \"studentAuthenticationMethod\" : {\r\n        \"type\" : \"easyLogin\",\r\n        \"enabled\" : false,\r\n        \"requirePassword\" : false\r\n      },\r\n      \"identifiers\" : { },\r\n      \"displayName\" : \"New Demo Class\"\r\n    } ]\r\n  },\r\n  \"sessionContext\" : {\r\n    \"sessionType\" : \"DAS\",\r\n    \"sessionToken\" : \"94f4f21b-9030-4310-8d01-9ac6bc174494\"\r\n  }\r\n}";
    }

    private String getTeacherLtiCustomParamterThatIncludesLineFeedOnly() throws Exception {
        return "{\n  \"identifiers\" : {\n    \"staffId\" : \"560\"\n  },\n  \"orgContext\" : {\n    \"organization\" : {\n      \"id\" : \"83\",\n      \"name\" : \"Sweet Home High School\",\n      \"orgType\" : \"school\",\n      \"lastModifiedDate\" : \"2016-02-01T16:58:12.000-0500\"\n    },\n    \"entitlements\" : [ {\n      \"id\" : \"9\",\n      \"name\" : \"Digital Short Reads\",\n      \"applicationCode\" : \"digitalshortreads\",\n      \"url\" : \"http://localhost:8080/srd-portal/launch\",\n      \"thumbnail\" : \"/resources/images/product_icons/dsr.png\",\n      \"active\" : true\n    } ],\n    \"currentSchoolCalendar\" : {\n      \"description\" : \"2015-16\",\n      \"startDate\" : \"2015-07-31T20:00:00.000-0400\",\n      \"endDate\" : \"2016-07-30T20:00:00.000-0400\"\n    },\n    \"roles\" : [ \"teacher\" ]\n  },\n  \"launchContext\" : {\n    \"application\" : {\n      \"id\" : \"9\",\n      \"name\" : \"Digital Short Reads\",\n      \"applicationCode\" : \"digitalshortreads\",\n      \"url\" : \"http://localhost:8080/srd-portal/launch\",\n      \"thumbnail\" : \"/resources/images/product_icons/dsr.png\",\n      \"active\" : true\n    },\n    \"productEntitlements\" : [ {\n      \"id\" : \"22\",\n      \"name\" : \"Short Read Digital Trail Pack\",\n      \"productCode\" : \"8888833445566\",\n      \"applicationId\" : \"9\",\n      \"active\" : true,\n      \"includesStudentAccess\" : true\n    } ],\n    \"role\" : \"teacher\"\n  },\n  \"teacherContext\" : {\n    \"classes\" : [ {\n      \"id\" : \"40\",\n      \"organizationId\" : \"83\",\n      \"staff\" : {\n        \"primaryTeacherId\" : \"560\"\n      },\n      \"students\" : [ ],\n      \"nickname\" : \"DSR Class 1\",\n      \"lowGrade\" : \"7\",\n      \"highGrade\" : \"7\",\n      \"active\" : true,\n      \"studentAuthenticationMethod\" : {\n        \"type\" : \"easyLogin\",\n        \"enabled\" : false,\n        \"requirePassword\" : false\n      },\n      \"identifiers\" : { },\n      \"displayName\" : \"DSR Class 1\"\n    }, {\n      \"id\" : \"41\",\n      \"organizationId\" : \"83\",\n      \"staff\" : {\n        \"primaryTeacherId\" : \"560\"\n      },\n      \"students\" : [ ],\n      \"nickname\" : \"DSR class 2\",\n      \"lowGrade\" : \"2\",\n      \"highGrade\" : \"2\",\n      \"active\" : true,\n      \"studentAuthenticationMethod\" : {\n        \"type\" : \"easyLogin\",\n        \"enabled\" : false,\n        \"requirePassword\" : false\n      },\n      \"identifiers\" : { },\n      \"displayName\" : \"DSR class 2\"\n    }, {\n      \"id\" : \"11035\",\n      \"organizationId\" : \"83\",\n      \"staff\" : {\n        \"primaryTeacherId\" : \"560\"\n      },\n      \"students\" : [ ],\n      \"nickname\" : \"DP Class 3\",\n      \"lowGrade\" : \"8\",\n      \"highGrade\" : \"8\",\n      \"active\" : true,\n      \"studentAuthenticationMethod\" : {\n        \"type\" : \"easyLogin\",\n        \"enabled\" : false,\n        \"requirePassword\" : false\n      },\n      \"identifiers\" : { },\n      \"displayName\" : \"DP Class 3\"\n    }, {\n      \"id\" : \"11485\",\n      \"organizationId\" : \"83\",\n      \"staff\" : {\n        \"primaryTeacherId\" : \"560\"\n      },\n      \"students\" : [ ],\n      \"nickname\" : \"New Demo Class\",\n      \"lowGrade\" : \"10\",\n      \"highGrade\" : \"10\",\n      \"active\" : true,\n      \"studentAuthenticationMethod\" : {\n        \"type\" : \"easyLogin\",\n        \"enabled\" : false,\n        \"requirePassword\" : false\n      },\n      \"identifiers\" : { },\n      \"displayName\" : \"New Demo Class\"\n    } ]\n  },\n  \"sessionContext\" : {\n    \"sessionType\" : \"DAS\",\n    \"sessionToken\" : \"94f4f21b-9030-4310-8d01-9ac6bc174494\"\n  }\n}";
    }

    private SortedMap< String, String > creatLtiLaunchOAuthDtoAsAStudent( String customDpLaunch ) throws Exception {

        SortedMap< String, String > sortedMap = new TreeMap< >();
        String context_id = "9";
        String custom_country_code = "USA";
        String custom_timezone_id = "US/Eastern";
        String launch_presentation_locale = "en_us";
        String lis_person_name_family = "Cook";
        String lis_person_name_full = "Alister Cook";
        String lis_person_name_given = "Alister";
        String lti_message_type = "basic-lti-launch-request";
        String lti_version = "LTI-1p0";
        String oauth_consumer_key = "l0JkeOfrC2g2ALqB1dtghOjuxP8a";
        String oauth_nonce = "50355181252366";
        String oauth_signature_method = "HMAC-SHA1";
        String oauth_timestamp = "1461171405";
        String oauth_version = "1.0";
        String roles = "Student";
        String tool_consumer_info_product_family_code = "ScholasticDigitalPlatform";
        String tool_consumer_info_version = "2013111804.04";
        String tool_consumer_instance_name = "dev1";
        String user_id = "561";
        sortedMap.put( "context_id", context_id );
        sortedMap.put( "custom_country_code", custom_country_code );
        sortedMap.put( "custom_timezone_id", custom_timezone_id );
        sortedMap.put( "custom_country_code", custom_country_code );
        sortedMap.put( "custom_timezone_id", custom_timezone_id );
        sortedMap.put( "launch_presentation_locale", launch_presentation_locale );
        sortedMap.put( "lis_person_name_family", lis_person_name_family );
        sortedMap.put( "lis_person_name_full", lis_person_name_full );
        sortedMap.put( "lis_person_name_given", lis_person_name_given );
        sortedMap.put( "lti_message_type", lti_message_type );
        sortedMap.put( "lti_version", lti_version );
        sortedMap.put( CUSTOM_DP_LAUNCH, customDpLaunch );
        sortedMap.put( "oauth_consumer_key", oauth_consumer_key );
        sortedMap.put( "oauth_nonce", oauth_nonce );
        sortedMap.put( "oauth_signature_method", oauth_signature_method );
        sortedMap.put( "oauth_timestamp", oauth_timestamp );
        sortedMap.put( "oauth_version", oauth_version );
        sortedMap.put( "roles", roles );
        sortedMap.put( "tool_consumer_info_product_family_code", tool_consumer_info_product_family_code );
        sortedMap.put( "tool_consumer_info_version", tool_consumer_info_version );
        sortedMap.put( "tool_consumer_instance_name", tool_consumer_instance_name );
        sortedMap.put( "user_id", user_id );

        return sortedMap;
    }

    private SortedMap< String, String > creatLtiLaunchOAuthDtoAsATeacher( String customDpLaunch ) throws Exception {

        SortedMap< String, String > sortedMap = new TreeMap< >();
        String context_id = "9";
        String custom_country_code = "USA";
        String custom_timezone_id = "US/Eastern";
        String launch_presentation_locale = "en_us";
        String lis_person_contact_email_primary = "vivdesai@deloitte.com";
        String lis_person_name_family = "desai";
        String lis_person_name_full = "vivek desai";
        String lis_person_name_given = "vivek";
        String lti_message_type = "basic-lti-launch-request";
        String lti_version = "LTI-1p0";
        String oauth_consumer_key = "l0JkeOfrC2g2ALqB1dtghOjuxP8a";
        String oauth_nonce = "222548124566288";
        String oauth_signature_method = "HMAC-SHA1";
        String oauth_timestamp = "1477905394";
        String oauth_version = "1.0";
        String roles = "Instructor";
        String tool_consumer_info_product_family_code = "ScholasticDigitalPlatform";
        String tool_consumer_info_version = "2013111804.04";
        String tool_consumer_instance_name = "dev1";
        String user_id = "249042";

        sortedMap.put( "context_id", context_id );
        sortedMap.put( "custom_country_code", custom_country_code );
        sortedMap.put( "custom_timezone_id", custom_timezone_id );
        sortedMap.put( "custom_country_code", custom_country_code );
        sortedMap.put( "custom_timezone_id", custom_timezone_id );
        sortedMap.put( "launch_presentation_locale", launch_presentation_locale );
        sortedMap.put( "lis_person_contact_email_primary", lis_person_contact_email_primary );
        sortedMap.put( "lis_person_name_family", lis_person_name_family );
        sortedMap.put( "lis_person_name_full", lis_person_name_full );
        sortedMap.put( "lis_person_name_given", lis_person_name_given );
        sortedMap.put( "lti_message_type", lti_message_type );
        sortedMap.put( "lti_version", lti_version );
        sortedMap.put( CUSTOM_DP_LAUNCH, customDpLaunch );
        sortedMap.put( "oauth_consumer_key", oauth_consumer_key );
        sortedMap.put( "oauth_nonce", oauth_nonce );
        sortedMap.put( "oauth_signature_method", oauth_signature_method );
        sortedMap.put( "oauth_timestamp", oauth_timestamp );
        sortedMap.put( "oauth_version", oauth_version );
        sortedMap.put( "roles", roles );
        sortedMap.put( "tool_consumer_info_product_family_code", tool_consumer_info_product_family_code );
        sortedMap.put( "tool_consumer_info_version", tool_consumer_info_version );
        sortedMap.put( "tool_consumer_instance_name", tool_consumer_instance_name );
        sortedMap.put( "user_id", user_id );
        System.out.println( sortedMap.size() );
        System.out.println( "Keys : " + sortedMap.keySet().toString() );

        return sortedMap;
    }

}