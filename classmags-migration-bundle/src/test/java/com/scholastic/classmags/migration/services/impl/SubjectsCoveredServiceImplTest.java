package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.scholastic.classmags.migration.models.TagModel;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for SubjectsCoveredService.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { SubjectsCoveredServiceImpl.class, CommonUtils.class } )
public class SubjectsCoveredServiceImplTest {

    private static final String CURRENT_CONTENT_PATH = "/content/classroom_magazines/scienceworld/issues/MAIN-ARTILCE-1/jcr:content";
    private static final String CURRENT_PATH = "/content/classroom_magazines/scienceworld/issues/MAIN-ARTILCE-1";
    private static final String TAG_ONE_PATH = "/etc/tags/classroom-magazines/scienceworld/biology/adaptations";
    private static final String TAG_TWO_PATH = "/etc/tags/classroom-magazines/scienceworld/chemistry";
    private static final String TAG_THREE_PATH = "/etc/tags/classroom-magazines/scienceworld/biology/cells";
    private static final String TAG_FOUR_PATH = "/etc/tags/classroom-magazines/scienceworld/chemistry/materials";
    private static final String TAG_FIVE_PATH = "/etc/tags/classroom-magazines/scienceworld/biology";
    private static final String TAG_ONE_TITLE = "Adaptations";
    private static final String TAG_TWO_TITLE = "Chemistry";
    private static final String TAG_THREE_TITLE = "Cells";
    private static final String TAG_FOUR_TITLE = "Materials";
    private static final String TAG_FIVE_TITLE = "Biology";
    private static final String TAG_ONE_NAME = "adaptations";
    private static final String TAG_TWO_NAME = "chemistry";
    private static final String TAG_THREE_NAME = "cells";
    private static final String TAG_FOUR_NAME = "materials";
    private static final String TAG_FIVE_NAME = "biology";
    private static final String TAG_CONTENT_TYPE_PATH = "/etc/tags/classroom-magazines/global/contentType/video";
    private static final String TAG_CONTENT_TYPE_TITLE = "Video";
    private static final String TAG_CONTENT_TYPE_NAME = "video";
    private static final String PARENT_PATH_BIOLOGY = "/etc/tags/classroom-magazines/scienceworld/biology";
    private static final String PARENT_PATH_CHEMISTRY = "/etc/tags/classroom-magazines/scienceworld/chemistry";
    private static final String MAGAZINE_NAME = "scienceworld";

    /** The subjects covered service. */
    private SubjectsCoveredServiceImpl subjectsCoveredService;

    /** The resource resolver. */
    private ResourceResolverFactory resourceResolverFactory;

    /** The resource resolver. */
    private ResourceResolver resourceResolver;

    /** The tag manager. */
    private TagManager tagManager;

    /** The resource. */
    private Resource resource;

    /** The JCR node resource. */
    private Resource jcrNodeResource;

    /** The tags. */
    private Tag[] tags;
    
    /** The tag models. */
    private TagModel biologyTagModel, chemistryTagModel;

    /**
     * Initial Setup.
     */
    @Before
    public void setUp() {
        subjectsCoveredService = new SubjectsCoveredServiceImpl();
        resourceResolverFactory = mock( ResourceResolverFactory.class );
        resourceResolver = mock( ResourceResolver.class );
        resource = mock( Resource.class );
        jcrNodeResource = mock( Resource.class );
        tagManager = mock( TagManager.class );

        Tag tag1 = mockTag( TAG_ONE_TITLE, TAG_ONE_PATH, TAG_ONE_NAME );
        Tag tag2 = mockTag( TAG_TWO_TITLE, TAG_TWO_PATH, TAG_TWO_NAME );
        Tag tag3 = mockTag( TAG_THREE_TITLE, TAG_THREE_PATH, TAG_THREE_NAME );
        Tag tag4 = mockTag( TAG_FOUR_TITLE, TAG_FOUR_PATH, TAG_FOUR_NAME );
        Tag tag5 = mockTag( TAG_FIVE_TITLE, TAG_FIVE_PATH, TAG_FIVE_NAME );
        Tag tag6 = mockTag( TAG_CONTENT_TYPE_TITLE, TAG_CONTENT_TYPE_PATH, TAG_CONTENT_TYPE_NAME );
        Tag biologyTag = mockTag( TAG_FIVE_TITLE, PARENT_PATH_BIOLOGY, TAG_FIVE_NAME );
        Tag chemistryTag = mockTag( TAG_TWO_TITLE, PARENT_PATH_CHEMISTRY, TAG_TWO_NAME );

        biologyTagModel = new TagModel( biologyTag );
        chemistryTagModel = new TagModel( chemistryTag );

        tags = new Tag[] { tag1, tag2, tag3, tag4, tag5, tag6 };

        PowerMockito.mockStatic( CommonUtils.class );

        Whitebox.setInternalState( subjectsCoveredService, resourceResolverFactory );

        when( resource.getResourceResolver() ).thenReturn( resourceResolver );
        when( CommonUtils.getTagManager( resourceResolverFactory ) ).thenReturn( tagManager );
        when( resourceResolver.getResource( CURRENT_CONTENT_PATH ) ).thenReturn( jcrNodeResource );
        when( tagManager.getTags( jcrNodeResource ) ).thenReturn( tags );
        when( tagManager.resolve( PARENT_PATH_BIOLOGY ) ).thenReturn( biologyTag );
        when( tagManager.resolve( PARENT_PATH_CHEMISTRY ) ).thenReturn( chemistryTag );
    }

    /**
     * Test get subjects covered.
     */
    @Test
    public void testGetSubjectsCovered() {

        // execute logic
        Map< TagModel, List< TagModel > > subjectsCovered = subjectsCoveredService.getSubjectsCovered( resource,
                CURRENT_PATH, MAGAZINE_NAME );

        // verify logic
        assertTrue( subjectsCovered.containsKey( biologyTagModel ) );
        assertTrue( subjectsCovered.containsKey( chemistryTagModel ) );
        assertEquals( TAG_ONE_TITLE, subjectsCovered.get( biologyTagModel ).get( 0 ).getTag().getTitle() );
        assertEquals( TAG_THREE_TITLE, subjectsCovered.get( biologyTagModel ).get( 1 ).getTag().getTitle() );
        assertEquals( TAG_FOUR_TITLE, subjectsCovered.get( chemistryTagModel ).get( 0 ).getTag().getTitle() );
    }

    /**
     * Test get subjects covered when resource resolver is null.
     */
    @Test
    public void testGetSubjectsCoveredWhenResourceResolverIsNull() {

        // setup logic ( test-specific )
        when( resource.getResourceResolver() ).thenReturn( null );

        // execute logic
        Map< TagModel, List< TagModel > > subjectsCovered = subjectsCoveredService.getSubjectsCovered( resource,
                CURRENT_PATH, MAGAZINE_NAME );

        // verify logic
        assertTrue( subjectsCovered.isEmpty() );

    }

    /**
     * Test get subjects covered when JCR node is null.
     */
    @Test
    public void testGetSubjectsCoveredWhenJcrContentNodeIsNull() {
        // setup logic ( test-specific )
        when( resourceResolver.getResource( CURRENT_CONTENT_PATH ) ).thenReturn( null );

        // execute logic
        Map< TagModel, List< TagModel > > subjectsCovered = subjectsCoveredService.getSubjectsCovered( resource,
                CURRENT_PATH, MAGAZINE_NAME );

        // verify logic
        assertTrue( subjectsCovered.isEmpty() );
    }

    private Tag mockTag( String tagTitle, String tagPath, String tagName ) {
        Tag tag = mock( Tag.class );

        when( tag.getTitle() ).thenReturn( tagTitle );
        when( tag.getPath() ).thenReturn( tagPath );
        when( tag.getName() ).thenReturn( tagName );

        return tag;
    }
}
