package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for FAQQuestionsDataServiceImpl.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { FAQQuestionsDataServiceImpl.class, CommonUtils.class, PredicateGroup.class } )
public class FAQQuestionsDataServiceImplTest {

    /** The FAQ questions data service impl. */
    private FAQQuestionsDataServiceImpl faqQuestionsDataServiceImpl;

    /** The Constant CURRENT_PAGE_PATH. */
    private static final String CURRENT_PAGE_PATH = "/content/classroom_magazines/scienceworld/faq";

    /** The Constant QUESTION_ID. */
    private static final String QUESTION_ID = "SS-1";

    /** The Constant QUESTION. */
    private static final String QUESTION = "How to Subscribe?";

    private SlingHttpServletRequest request;

    private Page currentPage;

    private ResourceResolverFactory resourceResolverFactory;
    
    private QueryBuilder queryBuilder;

    private ResourceResolver resourceResolver;

    private Session session;
    
    private Query query;
    
    private PredicateGroup predicateGroup;
    
    private SearchResult searchResult;
    
    private List<Hit> questionSetNodeList;
    
    private Hit hitOne;
    
    /**
     * Inital setup.
     * 
     * @throws RepositoryException 
     */
    @Before
    public void setUp() throws RepositoryException {
        faqQuestionsDataServiceImpl = new FAQQuestionsDataServiceImpl();
        currentPage = mock(Page.class);
        request = mock ( SlingHttpServletRequest.class );
        resourceResolverFactory = mock( ResourceResolverFactory.class );
        resourceResolver = mock( ResourceResolver.class );
        session = mock ( Session.class );
        queryBuilder = mock( QueryBuilder.class );
        query = mock ( Query.class );
        predicateGroup = mock( PredicateGroup.class );
        searchResult = mock ( SearchResult.class );
        
        PowerMockito.mockStatic(CommonUtils.class);
        PowerMockito.mockStatic( PredicateGroup.class );
        
        Whitebox.setInternalState( faqQuestionsDataServiceImpl, resourceResolverFactory );
        Whitebox.setInternalState( faqQuestionsDataServiceImpl, queryBuilder );
        
        createQuestionSetNodeList();
        
        when( currentPage.getPath()).thenReturn( CURRENT_PAGE_PATH );
        when( CommonUtils.getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE)).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( Session.class )).thenReturn( session );
        when( PredicateGroup.create( any(Map.class) )).thenReturn( predicateGroup );
        when( queryBuilder.createQuery( predicateGroup, session )).thenReturn(query);
        when( query.getResult()).thenReturn( searchResult );
        when( searchResult.getHits()).thenReturn( questionSetNodeList );
    }

    private void createQuestionSetNodeList() throws RepositoryException {
        questionSetNodeList = new ArrayList<>();
        
        hitOne = mock( Hit.class );
        Resource resourceHitOne = mock( Resource.class );
        Resource questionListHitOne = mock ( Resource.class );
        List<ValueMap> questionListOne = new ArrayList<>();
        ValueMap question = mock( ValueMap.class );
        questionListOne.add( question );
        
        when( hitOne.getResource()).thenReturn( resourceHitOne );
        when( resourceHitOne.getChild( ClassMagsMigrationConstants.QUESTION_LIST )).thenReturn( questionListHitOne );
        when( CommonUtils.fetchMultiFieldData( questionListHitOne )).thenReturn( questionListOne );
        when( question.get( ClassMagsMigrationConstants.QUESTION_ID, StringUtils.EMPTY )).thenReturn( QUESTION_ID );
        when( question.get( ClassMagsMigrationConstants.QUESTION, StringUtils.EMPTY )).thenReturn( QUESTION );
        
        questionSetNodeList.add( hitOne );
    }

    /**
     * Test fetch FAQ questions data.
     * @throws ClassmagsMigrationBaseException 
     */
    @Test
    public void testFetchFAQQuestionsData() throws ClassmagsMigrationBaseException {

       // execute logic ( test )
       Map<String, String> questionsData = faqQuestionsDataServiceImpl.fetchFAQQuestionsData( request, currentPage );
       
       // verify logic
       assertEquals(1, questionsData.size());
       assertEquals(QUESTION, questionsData.get( QUESTION_ID + ":" + QUESTION ));
    }

    /**
     * Test fetch FAQ questions data.
     * @throws ClassmagsMigrationBaseException 
     * @throws RepositoryException 
     */
    @Test(expected = ClassmagsMigrationBaseException.class)
    public void testFetchFAQQuestionsDataWithRepositoryException() throws ClassmagsMigrationBaseException, RepositoryException {
       // setup logic ( test- specific )
       when( hitOne.getResource()).thenThrow( new RepositoryException()); 
       
       // execute logic
       Map<String, String> questionsData = faqQuestionsDataServiceImpl.fetchFAQQuestionsData( request, currentPage );
       
       // verify logic
       assertTrue(questionsData.isEmpty());
    }
}