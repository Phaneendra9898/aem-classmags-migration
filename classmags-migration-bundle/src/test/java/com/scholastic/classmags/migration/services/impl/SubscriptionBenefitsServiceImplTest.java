package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for SubscriptionBenefitsService.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { SubscriptionBenefitsServiceImpl.class, CommonUtils.class } )
public class SubscriptionBenefitsServiceImplTest {

    private static final String CURRENT_PATH = "/classroom_magazines/scienceworld/issues/_2015_16/090715/jcr:content/par/subscription_benefit/subscriptionBenefits";

    private SubscriptionBenefitsServiceImpl subscriptionBenefitsService;
    private Resource resource;
    private ResourceResolver resourceResolver;
    private ResourceResolverFactory resourceResolverFactory;

    /**
     * Initial Setup.
     */
    @SuppressWarnings( "unchecked" )
    @Before
    public void setUp() {
        subscriptionBenefitsService = new SubscriptionBenefitsServiceImpl();
        List< ValueMap > subscriptionBenefits = mock( List.class );

        resourceResolverFactory = mock( ResourceResolverFactory.class );
        resourceResolver = mock( ResourceResolver.class );
        resource = mock( Resource.class );

        Whitebox.setInternalState( subscriptionBenefitsService, resourceResolverFactory );
        PowerMockito.mockStatic( CommonUtils.class );

        when( CommonUtils.getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ) )
                .thenReturn( resourceResolver );
        when( resourceResolver.getResource( CURRENT_PATH ) ).thenReturn( resource );
        when( CommonUtils.fetchMultiFieldData( resource ) ).thenReturn( subscriptionBenefits );
    }

    @Test
    public void testFetchSubscriptionBenefits() throws ClassmagsMigrationBaseException {
        // execute logic
        List< ValueMap > subscriptionBenefits = subscriptionBenefitsService.fetchSubscriptionBenefits( CURRENT_PATH );

        // verify logic
        assertNotNull( subscriptionBenefits );
    }
}
