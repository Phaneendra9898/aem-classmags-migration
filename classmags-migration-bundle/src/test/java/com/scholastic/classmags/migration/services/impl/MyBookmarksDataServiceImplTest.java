package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.day.cq.commons.Externalizer;
import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.tagging.TagManager;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.BookmarkData;
import com.scholastic.classmags.migration.models.GameModel;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * The Class MyBookmarksDataServiceImplTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { MyBookmarksDataServiceImpl.class, CommonUtils.class, InternalURLFormatter.class } )
public class MyBookmarksDataServiceImplTest {

    /** The my bookmarks data service. */
    private MyBookmarksDataServiceImpl myBookmarksDataService;

    /** The Constant PATH. */
    private static final String TEST_PATH = "/testPath";

    /** The Constant TEST_MAGAZINE_TYPE. */
    private static final String TEST_MAGAZINE_TYPE = "scienceworld";

    /** The Constant TEST_DATA. */
    private static final String TEST_DATA = "testData";

    /** The Constant TEST_VIDEO_ID. */
    private static final long TEST_VIDEO_ID = 101;

    /** The resource resolver factory. */
    @Mock
    private ResourceResolverFactory mockResourceResolverFactory;

    /** The resource path config service. */
    @Mock
    private ResourcePathConfigService resourcePathConfigService;

    /** The mock property config service. */
    @Mock
    private PropertyConfigService mockPropertyConfigService;

    /** The mock externalizer. */
    @Mock
    private Externalizer mockExternalizer;

    /** The resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock tag manager. */
    @Mock
    private TagManager mockTagManager;

    /** The mock value map. */
    @Mock
    private ValueMap mockValueMap;

    /** The mock tags. */
    @Mock
    private List< String > mockTags;

    /** The mock all page bookmarks. */
    @Mock
    private List< BookmarkData > mockAllPageBookmarks;

    /** The mock all asset bookmarks. */
    @Mock
    private List< BookmarkData > mockAllAssetBookmarks;

    /** The mock bookmark data. */
    @Mock
    private BookmarkData mockBookmarkData;

    /** The mock all page bookmarks iterator. */
    @Mock
    private Iterator< BookmarkData > mockAllPageBookmarksIterator;

    /** The mock all asset bookmarks iterator. */
    @Mock
    private Iterator< BookmarkData > mockAllAssetBookmarksIterator;

    /** The mock asset. */
    @Mock
    private Asset mockAsset;

    /** The mock game model. */
    @Mock
    private GameModel mockGameModel;

    /**
     * Sets the up.
     *
     * @throws ClassmagsMigrationBaseException
     *             the classmags migration base exception
     */
    @Before
    public void setUp() throws ClassmagsMigrationBaseException {
        myBookmarksDataService = new MyBookmarksDataServiceImpl();

        Whitebox.setInternalState( myBookmarksDataService, mockResourceResolverFactory );
        Whitebox.setInternalState( myBookmarksDataService, resourcePathConfigService );
        Whitebox.setInternalState( myBookmarksDataService, mockPropertyConfigService );
        Whitebox.setInternalState( myBookmarksDataService, mockExternalizer );

        PowerMockito.mockStatic( CommonUtils.class );
        PowerMockito.mockStatic( InternalURLFormatter.class );

        when( CommonUtils.getResourceResolver( mockResourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ) )
                .thenReturn( mockResourceResolver );
        when( CommonUtils.getTagManager( mockResourceResolverFactory ) ).thenReturn( mockTagManager );
        when( CommonUtils.getSubjectTags( mockResource, mockTagManager ) ).thenReturn( mockTags );
        when( CommonUtils.getFormattedSubjectTags( mockTags ) ).thenReturn( TEST_DATA );
        when( CommonUtils.getDataPath( resourcePathConfigService, TEST_MAGAZINE_TYPE ) ).thenReturn( TEST_PATH );
        when( CommonUtils.getSortingDate( mockValueMap, TEST_DATA ) ).thenReturn( TEST_DATA );
        when( CommonUtils.getAssetSortingDate( mockValueMap, TEST_DATA ) ).thenReturn( TEST_DATA );
        when( CommonUtils.getContentTypeTag( mockResource, mockTagManager ) ).thenReturn( TEST_DATA );
        when( CommonUtils.getDateFormat( mockPropertyConfigService, TEST_PATH ) ).thenReturn( TEST_DATA );
        when( CommonUtils.getDisplayDate( mockValueMap, TEST_DATA ) ).thenReturn( TEST_DATA );
        when( InternalURLFormatter.formatURL( mockResourceResolver, TEST_PATH ) ).thenReturn( TEST_PATH );
        when( mockExternalizer.externalLink( mockResourceResolver, TEST_MAGAZINE_TYPE, TEST_PATH ) )
                .thenReturn( TEST_PATH );
    }

    /**
     * Test get my bookmarks page data.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetMyBookmarksPageData() throws Exception {

        when( mockAllPageBookmarks.iterator() ).thenReturn( mockAllPageBookmarksIterator );
        when( mockAllPageBookmarksIterator.hasNext() ).thenReturn( true, false );
        when( mockAllPageBookmarksIterator.next() ).thenReturn( mockBookmarkData );
        when( mockBookmarkData.getBookmarkPath() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.resolve( TEST_PATH ) ).thenReturn( mockResource );
        when( mockResource.getChild( JcrConstants.JCR_CONTENT ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_PATH );
        when( mockValueMap.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockBookmarkData.getMagazineType() ).thenReturn( TEST_MAGAZINE_TYPE );
        when( mockValueMap.get( ClassMagsMigrationConstants.ASSET_USER_TYPE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockBookmarkData.getViewArticleLink() ).thenReturn( TEST_PATH );
        when( mockBookmarkData.getDateSaved() ).thenReturn( TEST_DATA );

        assertEquals( TEST_PATH, myBookmarksDataService.getMyBookmarksPageData( mockAllPageBookmarks, TEST_DATA )
                .get( 0 ).getBookmarkPath() );

    }

    /**
     * Test get my bookmarks asset data.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetMyBookmarksAssetData() throws Exception {

        when( mockAllAssetBookmarks.iterator() ).thenReturn( mockAllAssetBookmarksIterator );
        when( mockAllAssetBookmarksIterator.hasNext() ).thenReturn( true, false );
        when( mockAllAssetBookmarksIterator.next() ).thenReturn( mockBookmarkData );
        when( mockBookmarkData.getBookmarkPath() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.resolve( TEST_PATH ) ).thenReturn( mockResource );
        when( mockResource.adaptTo( Asset.class ) ).thenReturn( mockAsset );
        when( mockResource.getChild( ClassMagsMigrationConstants.ASSET_METADATA_NODE ) ).thenReturn( mockResource );
        when( mockAsset.getPath() ).thenReturn( TEST_PATH );
        when( mockAsset.getMetadataValue( DamConstants.DC_TITLE ) ).thenReturn( TEST_DATA );
        when( mockAsset.getName() ).thenReturn( TEST_DATA );
        when( mockAsset.getMetadataValue( ClassMagsMigrationConstants.ASSET_USER_TYPE ) ).thenReturn( TEST_DATA );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.containsKey( ClassMagsMigrationConstants.VIDEO_DURATION ) ).thenReturn( true );
        when( mockValueMap.get( ClassMagsMigrationConstants.VIDEO_ID, Long.class ) ).thenReturn( TEST_VIDEO_ID );
        when( CommonUtils.getContentTypeTag( mockResource, mockTagManager ) )
                .thenReturn( ClassMagsMigrationConstants.GAMES );
        when( mockBookmarkData.getMagazineType() ).thenReturn( TEST_MAGAZINE_TYPE );
        when( mockBookmarkData.getViewArticleLink() ).thenReturn( TEST_PATH );
        when( mockBookmarkData.getDateSaved() ).thenReturn( TEST_DATA );
        when( mockValueMap.get( ClassMagsMigrationConstants.GAME_TYPE, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockPropertyConfigService
                .getDataConfigProperty( ClassMagsMigrationConstants.FLASH_GAME_CONTAINER_PROPERTY, TEST_PATH ) )
                        .thenReturn( TEST_PATH );
        when( mockValueMap.get( ClassMagsMigrationConstants.XML_PATH, StringUtils.EMPTY ) ).thenReturn( TEST_PATH );
        when( mockGameModel.getFlashGameType() ).thenReturn( TEST_DATA );
        when( mockGameModel.getXMLPath() ).thenReturn( TEST_PATH );

        assertEquals( TEST_PATH, myBookmarksDataService.getMyBookmarksAssetData( mockAllAssetBookmarks, TEST_DATA )
                .get( 0 ).getBookmarkPath() );

    }
}
