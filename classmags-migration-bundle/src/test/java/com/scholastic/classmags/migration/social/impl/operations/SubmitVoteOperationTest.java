package com.scholastic.classmags.migration.social.impl.operations;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.adobe.cq.social.scf.SocialComponentFactoryManager;
import com.adobe.cq.social.scf.SocialOperationResult;
import com.scholastic.classmags.migration.social.api.VotingService;
import com.scholastic.classmags.migration.social.api.VotingSocialComponent;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;

/**
 * The Class SubmitVoteOperationTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { SubmitVoteOperation.class, SocialASRPUtils.class } )
public class SubmitVoteOperationTest {

    /** The submit vote operation. */
    private SubmitVoteOperation submitVoteOperation;

    /** The mock sling http servlet request. */
    @Mock
    private SlingHttpServletRequest mockSlingHttpServletRequest;

    /** The mock voting service. */
    @Mock
    private VotingService mockVotingService;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock scf manager. */
    @Mock
    private SocialComponentFactoryManager mockScfManager;

    /** The mock social operation result. */
    @Mock
    private SocialOperationResult mockSocialOperationResult;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        submitVoteOperation = new SubmitVoteOperation();

        Whitebox.setInternalState( submitVoteOperation, mockVotingService );
        Whitebox.setInternalState( submitVoteOperation, mockScfManager );

        PowerMockito.mockStatic( SocialASRPUtils.class );
        when( SocialASRPUtils.getSocialOperationResult( mockSlingHttpServletRequest, HttpServletResponse.SC_OK,
                ClassMagsMigrationASRPConstants.HTTP_SUCCESS_MESSAGE, mockResource, mockScfManager,
                VotingSocialComponent.VOTING_RESOURCE_TYPE ) ).thenReturn( mockSocialOperationResult );

    }

    /**
     * Test perform operation.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testPerformOperation() throws Exception {

        when( mockVotingService.submitAndCreateVoteResource( mockSlingHttpServletRequest ) ).thenReturn( mockResource );

        assertEquals( mockSocialOperationResult, submitVoteOperation.performOperation( mockSlingHttpServletRequest ) );
    }
}
