package com.scholastic.classmags.migration.utils;

import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import com.scholastic.classmags.migration.models.Issue;
import com.scholastic.classmags.migration.utils.SortingDateComparator;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith( MockitoJUnitRunner.class )
public class SortingDateComparatorTest {

    /** The issue object-1 */
    @Mock
    private Issue element1;

    /** The issue object-2 */
    @Mock
    private Issue element2;

    /** The constant Sorting date 1 Test data */
    private static final String SORTING_DATE_1 = "12/12/2016";

    /** The constant Sorting date 2 Test data */
    private static final String SORTING_DATE_2 = "13/12/2016";

    /** The constant unparsable date Test data */
    private static final String PARSE_DATE = "12-13-2016";

    /** The constant date format Test data */
    private static final String DATE_FORMAT = "dd/mm/yyyy";

    /** the integer */
    private int integer;

    /** The Sorting Date Comparator */
    private SortingDateComparator sortingDateComparator;

    @Before
    public void setup() {
        sortingDateComparator = new SortingDateComparator();
    }

    /**
     * Return -1 condition
     */
    @Test
    public void getCompare() {
        when( element1.getIssuedate() ).thenReturn( SORTING_DATE_1 );
        when( element2.getIssuedate() ).thenReturn( SORTING_DATE_2 );
        when( element1.getDateFormat() ).thenReturn( DATE_FORMAT );
        integer = sortingDateComparator.compare( element1, element2 );
        assertEquals( -1, integer );

    }

    /**
     * Return 1 condition
     */
    @Test
    public void getCompareWith1AsReturn() {
        when( element1.getIssuedate() ).thenReturn( SORTING_DATE_2 );
        when( element2.getIssuedate() ).thenReturn( SORTING_DATE_1 );
        when( element1.getDateFormat() ).thenReturn( DATE_FORMAT );
        integer = sortingDateComparator.compare( element1, element2 );
        assertEquals( 1, integer );
    }

    /**
     * Return 0 condition when dates are equal
     */
    @Test
    public void getCompareWithZeroAsReturn() {
        when( element1.getIssuedate() ).thenReturn( SORTING_DATE_2 );
        when( element2.getIssuedate() ).thenReturn( SORTING_DATE_2 );
        when( element1.getDateFormat() ).thenReturn( DATE_FORMAT );
        integer = sortingDateComparator.compare( element1, element2 );
        assertEquals( 0, integer );

    }

    /**
     * when parseException occurs
     */
    @Test
    public void getCompareWithException() {
        when( element1.getIssuedate() ).thenReturn( PARSE_DATE );
        when( element2.getIssuedate() ).thenReturn( SORTING_DATE_2 );
        when( element1.getDateFormat() ).thenReturn( DATE_FORMAT );
        integer = sortingDateComparator.compare( element1, element2 );
        assertEquals( -1, integer );
    }

    /**
     * when first of the strings is empty
     */
    @Test
    public void getCompareWithEmptyStringOne() {
        when( element1.getIssuedate() ).thenReturn( null );
        when( element2.getIssuedate() ).thenReturn( SORTING_DATE_2 );
        integer = sortingDateComparator.compare( element1, element2 );
        assertEquals( -1, integer );
    }

    /**
     * when second of the strings is empty
     */
    @Test
    public void getCompareWithEmptyStringSecond() {
        when( element1.getIssuedate() ).thenReturn( SORTING_DATE_2 );
        when( element2.getIssuedate() ).thenReturn( null );
        integer = sortingDateComparator.compare( element1, element2 );
        assertEquals( -1, integer );

    }
}
