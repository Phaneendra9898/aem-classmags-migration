package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.reflect.Whitebox;

/**
 * JUnits for ArticleSidebarUse.
 */
@RunWith( MockitoJUnitRunner.class )
public class ArticleSidebarUseTest extends BaseComponentTest {

    private ArticleSidebarUse articleSidebarUse;

    /**
     * Initial Setup.
     */
    @Before
    public void setUp() {
        articleSidebarUse = Whitebox.newInstance( ArticleSidebarUse.class );
        stubCommon( articleSidebarUse );

    }

    @Test
    public void testArticleSidebarUse() throws Exception {

        // execute logic
        Whitebox.invokeMethod( articleSidebarUse, "activate" );

        // verify logic
        assertNotNull( articleSidebarUse.getUuid() );
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final ArticleSidebarUse articleSidebarUse = new ArticleSidebarUse();

        // verify logic
        assertNotNull( articleSidebarUse );
    }

}
