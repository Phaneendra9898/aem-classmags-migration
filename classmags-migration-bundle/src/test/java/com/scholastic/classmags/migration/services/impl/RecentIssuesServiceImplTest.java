package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.day.cq.commons.jcr.JcrConstants;
import com.scholastic.classmags.migration.models.Issue;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * JUnit for RecentIssuesServiceImpl
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { RecentIssuesServiceImpl.class, CommonUtils.class, InternalURLFormatter.class } )
public class RecentIssuesServiceImplTest {

    private static final String ISSUE_PATH = "/content/classroom_magazines/scienceworld/issues/2016-17/091916";
    private static final String IMAGE_PATH = "/content/dam/scholastic/classroom-magazines/migration/images/Beach.png";
    private static final String ISSUE_DATE = "test-display-date";
    private static final String SCIENCE_WORLD = "scienceworld";
    private static final String SW_DATA_PAGE = "/content/classroom_magazines/admin/sw-data-page";
    private static final String SORTING_DATE_FORMAT = "MMM yyyy";
    private static final String ISSUE_SORTING_DATE = "Sep 2016";

    private RecentIssuesServiceImpl recentIssuesServiceImpl;

    private ValueMap properties;
    private ResourceResolverFactory resourceResolverFactory;
    private ResourcePathConfigService resourcePathConfigService;
    private PropertyConfigService propertyConfigService;
    private ResourceResolver resourceResolver;
    private Resource issueResource;
    private List< ValueMap > issuePaths;
    private ValueMap issuePath;

    private Logger logger;

    /**
     * Initial setup.
     * 
     */
    @Before
    public void setUp() throws UnsupportedEncodingException {
        recentIssuesServiceImpl = new RecentIssuesServiceImpl();

        logger = mock( Logger.class );
        issueResource = mock( Resource.class );
        issuePath = mock( ValueMap.class );
        properties = mock( ValueMap.class );
        resourceResolverFactory = mock( ResourceResolverFactory.class );
        resourcePathConfigService = mock( ResourcePathConfigService.class );
        propertyConfigService = mock( PropertyConfigService.class );
        resourceResolver = mock( ResourceResolver.class );

        PowerMockito.mockStatic( CommonUtils.class );
        PowerMockito.mockStatic( InternalURLFormatter.class );

        Whitebox.setInternalState( recentIssuesServiceImpl, resourceResolverFactory );
        Whitebox.setInternalState( recentIssuesServiceImpl, resourcePathConfigService );
        Whitebox.setInternalState( recentIssuesServiceImpl, propertyConfigService );

        Whitebox.setInternalState( RecentIssuesServiceImpl.class, "LOG", logger );

        issuePaths = new ArrayList< >();
        issuePaths.add( issuePath );

        when( CommonUtils.getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ) )
                .thenReturn( resourceResolver );
        when( issuePath.get( "issuePath", String.class ) ).thenReturn( ISSUE_PATH );
        when( resourceResolver.getResource( ISSUE_PATH + "/" + JcrConstants.JCR_CONTENT ) ).thenReturn( issueResource );
        when( issueResource.getValueMap() ).thenReturn( properties );
        when( InternalURLFormatter.formatURL( resourceResolver, ISSUE_PATH ) )
                .thenReturn( ISSUE_PATH + ClassMagsMigrationConstants.HTML_SUFFIX );
        when( properties.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( IMAGE_PATH );
        when( properties.get( ClassMagsMigrationConstants.DISPLAY_DATE, StringUtils.EMPTY ) ).thenReturn( ISSUE_DATE );

    }

    @Test
    public void testGetRecentIssues() {
        // execute logic
        List< Issue > issues = recentIssuesServiceImpl.getRecentIssues( SCIENCE_WORLD, issuePaths );

        // verify logic
        assertEquals( 1, issues.size() );
        assertEquals( IMAGE_PATH, issues.get( 0 ).getImgsrc() );
        assertEquals( ISSUE_DATE, issues.get( 0 ).getIssuedate() );
        assertEquals( ISSUE_PATH + ClassMagsMigrationConstants.HTML_SUFFIX, issues.get( 0 ).getLinkto() );
    }

    @Test
    public void testGetRecentIssuesWhenResourceResolverIsNull() {
        // setup logic ( test-specific )
        when( CommonUtils.getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ) )
                .thenReturn( null );

        // execute logic
        List< Issue > issues = recentIssuesServiceImpl.getRecentIssues( SCIENCE_WORLD, issuePaths );

        // verify logic
        assertTrue( issues.isEmpty() );
    }

    @Test
    public void testGetRecentIssuesWhenResourceIsNull() {
        // setup logic ( test-specific )
        when( resourceResolver.getResource( ISSUE_PATH + "/" + JcrConstants.JCR_CONTENT ) ).thenReturn( null );

        // execute logic
        List< Issue > issues = recentIssuesServiceImpl.getRecentIssues( SCIENCE_WORLD, issuePaths );

        // verify logic
        assertEquals( 1, issues.size() );

        assertNull( issues.get( 0 ).getImgsrc() );
        assertNull( issues.get( 0 ).getIssuedate() );
        assertNull( issues.get( 0 ).getLinkto() );
    }

    @Test
    public void testGetRecentIssuesWhenDisplayDateIsNotPresent() throws LoginException {
        // setup logic ( test-specific )
        when( properties.get( ClassMagsMigrationConstants.DISPLAY_DATE, StringUtils.EMPTY ) )
                .thenReturn( StringUtils.EMPTY );
        when( CommonUtils.getDataPath( resourcePathConfigService, SCIENCE_WORLD ) ).thenReturn( SW_DATA_PAGE );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                SW_DATA_PAGE ) ).thenReturn( SORTING_DATE_FORMAT );
        when( CommonUtils.getSortingDate( properties, SORTING_DATE_FORMAT ) ).thenReturn( ISSUE_SORTING_DATE );

        // execute logic
        List< Issue > issues = recentIssuesServiceImpl.getRecentIssues( SCIENCE_WORLD, issuePaths );

        // verify logic
        assertEquals( 1, issues.size() );
        assertEquals( IMAGE_PATH, issues.get( 0 ).getImgsrc() );
        assertEquals( ISSUE_SORTING_DATE, issues.get( 0 ).getIssuedate() );
        assertEquals( ISSUE_PATH + ClassMagsMigrationConstants.HTML_SUFFIX, issues.get( 0 ).getLinkto() );
    }

    @Test
    public void testGetRecentIssuesWhenLoginExceptionIsThrown() throws LoginException {
        // setup logic ( test-specific )
        LoginException e = new LoginException();
        when( properties.get( ClassMagsMigrationConstants.DISPLAY_DATE, StringUtils.EMPTY ) )
                .thenReturn( StringUtils.EMPTY );
        when( CommonUtils.getDataPath( resourcePathConfigService, SCIENCE_WORLD ) ).thenReturn( SW_DATA_PAGE );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                SW_DATA_PAGE ) ).thenThrow( e );

        // execute logic
        List< Issue > issues = recentIssuesServiceImpl.getRecentIssues( SCIENCE_WORLD, issuePaths );

        // verify logic
        verify( logger ).debug( "Error while fetching property in recent issues", e );

        assertEquals( 1, issues.size() );
        assertEquals( IMAGE_PATH, issues.get( 0 ).getImgsrc() );
        assertEquals( ISSUE_PATH + ClassMagsMigrationConstants.HTML_SUFFIX, issues.get( 0 ).getLinkto() );

        assertTrue( issues.get( 0 ).getIssuedate().isEmpty() );
    }

}
