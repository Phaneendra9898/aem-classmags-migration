package com.scholastic.classmags.migration.servlets;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.scholastic.classmags.migration.utils.DamAssetConstants;
import com.scholastic.classmags.migration.utils.DamAssetUtils;

/**
 * The Class FetchDamAssetsServletTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { FetchDamAssetsServlet.class, DamAssetUtils.class} )
public class FetchDamAssetsServletTest {

    private static final String CONTENT_TYPE_JSON = "application/json";

    private FetchDamAssetsServlet fetchDamAssetsServlet;
    
    @Mock
    private SlingHttpServletRequest request;
    
    @Mock
    private SlingHttpServletResponse response;
    
    @Mock
    private PrintWriter printWriter;
    
    @Mock
    private ResourceResolver resourceResolver;

    /** The logger. */
    @Mock
    private Logger logger;

    @Before
    public void setUp() throws Exception {
    	fetchDamAssetsServlet = Whitebox.newInstance( FetchDamAssetsServlet.class );
    	
    	String path = "/content/dam/classroom-magazines/scholasticnews3";
    	
        PowerMockito.mockStatic( DamAssetUtils.class );

    	Whitebox.setInternalState( FetchDamAssetsServlet.class, "LOG", logger );

        when( request.getResourceResolver() ).thenReturn( resourceResolver );
        when( request.getParameter( DamAssetConstants.PATH_PARAM ) ).thenReturn( path );
                
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final FetchDamAssetsServlet fetchDamAssetsServlet = new FetchDamAssetsServlet();

        // verify logic
        assertNotNull( fetchDamAssetsServlet );
    }
    
   
    @Test
    public void testFetchDamAssetsServletValidPath() throws Exception {
		String path = "/content/dam/classroom-magazines/scholasticnews3";
        when( response.getWriter() ).thenReturn( printWriter );
        when(DamAssetUtils.isValidPath(path)).thenReturn(true);
        // execute logic
        Whitebox.invokeMethod( fetchDamAssetsServlet, "doGet", request, response );

        // verify logic
        verify( response ).setContentType( CONTENT_TYPE_JSON );

    }

    @Test
    public void testFetchDamAssetsServletInvalidPath() throws Exception {
    	String path = "content/dam/classroom-magazines/scholasticnews3";
        when( response.getWriter() ).thenReturn( printWriter );
    	when(DamAssetUtils.isValidPath(path)).thenReturn(false);
        // execute logic
        Whitebox.invokeMethod( fetchDamAssetsServlet, "doGet", request, response );

        // verify logic
        verify( response ).setContentType( CONTENT_TYPE_JSON );
    }
    
    @Test
    public void testFetchDamAssetsServletIOException() throws Exception {
    	when( response.getWriter() ).thenReturn( printWriter );
    	String path = "/content/dam/classroom-magazines/scholasticnews3";
    	when(DamAssetUtils.isValidPath(path)).thenReturn(true);
    	IOException ioException = new IOException( "IO Exception" );
    	when( response.getWriter() ).thenThrow( ioException );
        // execute logic
        Whitebox.invokeMethod( fetchDamAssetsServlet, "doGet", request, response );

        // verify logic
        verify( logger ).error( "@FetchDamAssetsServlet.doGet: Exception while fetching the data : ", ioException );
    }
}
