package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith( MockitoJUnitRunner.class )
public class CreateBrightcoveVideoConfigServiceImplTest {
    
    @InjectMocks
    private CreateBrightcoveVideoConfigServiceImpl configServiceImpl;
    
    private final Map<String, Object> properties = new HashMap<String, Object>();
    
    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        
        properties.put("readUrl","http://api.brightcove.com/services/library");
        properties.put("searchVideos","searchVideos");
        properties.put("findById","findById");
        properties.put("fields","fields");
        properties.put("mediaDelivery","mediaDelivery");
        properties.put("sortStartDate","sortStartDate");
        properties.put("sortName","sortName");
        properties.put("getItemCount","getItemCount");
        properties.put("callback","callback");
        properties.put("upfrontAnyFields","upfrontAnyFields");
        properties.put("readToken","readToken");
    }
    
    @Test
    public void testGetReadUrl(){
        configServiceImpl.activate(properties);
        String readUrl = configServiceImpl.getReadUrl();
        assertEquals(readUrl,"http://api.brightcove.com/services/library");
    }
    
    @Test
    public void testGetSearchVideos(){
        configServiceImpl.activate(properties);
        String searchVideos = configServiceImpl.getSearchVideos();
        assertEquals(searchVideos,"searchVideos");
    }
    
    @Test
    public void testGetFindById(){
        configServiceImpl.activate(properties);
        String findById = configServiceImpl.getFindById();
        assertEquals(findById,"findById");
    }
    
    @Test
    public void testGetFields(){
        configServiceImpl.activate(properties);
        String fields = configServiceImpl.getFields();
        assertEquals(fields,"fields");
    }
    
    @Test
    public void testGetMediaDelivery(){
        configServiceImpl.activate(properties);
        String mediaDelivery = configServiceImpl.getMediaDelivery();
        assertEquals(mediaDelivery,"mediaDelivery");
    }
    
    @Test
    public void testGetSortStartDate(){
        configServiceImpl.activate(properties);
        String sortStartDate = configServiceImpl.getSortStartDate();
        assertEquals(sortStartDate,"sortStartDate");
    }
    
    @Test
    public void testGetSortName(){
        configServiceImpl.activate(properties);
        String sortName = configServiceImpl.getSortName();
        assertEquals(sortName,"sortName");
    }
    
    @Test
    public void testGetGetItemCount(){
        configServiceImpl.activate(properties);
        String getItemCount = configServiceImpl.getGetItemCount();
        assertEquals(getItemCount,"getItemCount");
    }
    
    @Test
    public void testGetCallback(){
        configServiceImpl.activate(properties);
        String callback = configServiceImpl.getCallback();
        assertEquals(callback,"callback");
    }
    
    @Test
    public void testGetUpfrontAnyFields(){
        configServiceImpl.activate(properties);
        String upfrontAnyFields = configServiceImpl.getUpfrontAnyFields();
        assertEquals(upfrontAnyFields,"upfrontAnyFields");
    }
    
    @Test
    public void testGetReadToken(){
        configServiceImpl.activate(properties);
        String readToken = configServiceImpl.getReadToken();
        assertEquals(readToken,"readToken");
    }
    
    @After
    public final void tearDown() {
        Mockito.reset();
    }
}
