package com.scholastic.classmags.migration.servlets;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.request.RequestParameter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.scholastic.classmags.migration.services.UploadGameService;

/**
 * The Class UploadGameServletTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { UploadGameServlet.class } )
public class UploadGameServletTest {

    private UploadGameServlet uploadGameServlet;
    private SlingHttpServletRequest request;
    private SlingHttpServletResponse response;
    private RequestParameter gameFile;
    private PrintWriter printWriter;
    private InputStream gameFileStream;
    private UploadGameService uploadGameService;

    /** The logger. */
    private Logger logger;

    @Before
    public void setUp() throws Exception {
        uploadGameServlet = Whitebox.newInstance( UploadGameServlet.class );

        logger = mock( Logger.class );
        request = mock( SlingHttpServletRequest.class );
        response = mock( SlingHttpServletResponse.class );
        gameFile = mock( RequestParameter.class );
        printWriter = mock( PrintWriter.class );
        gameFileStream = mock( InputStream.class );
        uploadGameService = mock( UploadGameService.class );

        Whitebox.setInternalState( UploadGameServlet.class, "LOG", logger );
        Whitebox.setInternalState( uploadGameServlet, uploadGameService );

        when( request.getRequestParameter( "uploadGame" ) ).thenReturn( gameFile );
        when( request.getParameter( "damPath" ) )
                .thenReturn( "/content/dam/classroom-magazines/games/html5/ss-gamemaker" );
        when( gameFile.isFormField() ).thenReturn( false );
        when( gameFile.getInputStream() ).thenReturn( gameFileStream );
        when( response.getWriter() ).thenReturn( printWriter );
        when( gameFile.getFileName() ).thenReturn( "SS-GAMEMaker.zip" );
        when( uploadGameService.uploadGame( gameFileStream, "/content/dam/classroom-magazines/games/html5/ss-gamemaker",
                "SS-GAMEMaker.zip" ) ).thenReturn( "Upload Successful" );
    }

    @Test
    public void testUploadGameServlet() throws Exception {
        // execute logic
        Whitebox.invokeMethod( uploadGameServlet, "doGet", request, response );

        // verify logic
        verify( response ).setContentType( "text/html" );
    }

    @Test
    public void testWhenResponseMessageIsNull() throws Exception {
        // setup logic ( test-specific )
        when( uploadGameService.uploadGame( gameFileStream, "/content/dam/classroom-magazines/games/html5/ss-gamemaker",
                "SS-GAMEMaker.zip" ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( uploadGameServlet, "doGet", request, response );

        // verify logic
        verify( response ).setStatus( HttpServletResponse.SC_INTERNAL_SERVER_ERROR );
    }

    @Test
    public void testWhenGameFileIsNull() throws Exception {
        // setup logic ( test-specific )
        when( request.getRequestParameter( "uploadGame" ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( uploadGameServlet, "doGet", request, response );

        // verify logic
        verify( response ).setStatus( HttpServletResponse.SC_BAD_REQUEST );
    }

    @Test
    public void testWhenGameFileIsFormField() throws Exception {
        // setup logic ( test-specific )
        when( gameFile.isFormField() ).thenReturn( true );

        // execute logic
        Whitebox.invokeMethod( uploadGameServlet, "doGet", request, response );

        // verify logic
        verify( response ).setStatus( HttpServletResponse.SC_BAD_REQUEST );
    }

    @Test
    public void testWhenDamPathIsEmpty() throws Exception {
        // setup logic (test-specific)
        when( request.getParameter( "damPath" ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( uploadGameServlet, "doGet", request, response );

        // verify logic
        verify( response ).setStatus( HttpServletResponse.SC_BAD_REQUEST );
    }

    @Test
    public void testWhenIOException() throws Exception {
        // setup logic (test-specific)
        when( gameFile.getInputStream() ).thenThrow( new IOException() );

        // execute logic
        Whitebox.invokeMethod( uploadGameServlet, "doGet", request, response );

        // verify logic
        verify( logger ).error( "Error creating game in UploadGameServlet: " + new IOException() );
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final UploadGameServlet uploadGameServlet = new UploadGameServlet();

        // verify logic
        assertNotNull( uploadGameServlet );
    }
}