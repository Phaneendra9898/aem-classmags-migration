package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.script.Bindings;
import javax.script.SimpleBindings;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.reflect.Whitebox;

import com.adobe.acs.commons.genericlists.GenericList;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;

/**
 * JUnits for GenericListRenderer.
 */
@RunWith( MockitoJUnitRunner.class )
@PrepareForTest( { GenericListRenderer.class } )
public class GenericListRendererTest extends BaseComponentTest {

    private GenericListRenderer genericListRenderer;
    private Bindings bindings;
    private Page page;
    private PageManager pageManager;
    private GenericList genericList;

    /**
     * Initial Setup.
     */
    @Before
    public void setUp() {
        genericListRenderer = Whitebox.newInstance( GenericListRenderer.class );

        pageManager = mock( PageManager.class );
        bindings = new SimpleBindings();
        page = mock( Page.class );
        genericList = mock( GenericList.class );

        initBinidings( genericListRenderer, bindings );

        bindings.put( "listpath", "SampleList" );
        bindings.put( "pageManager", pageManager );
        when( pageManager.getPage( "SampleList" ) ).thenReturn( page );
        when( page.adaptTo( GenericList.class ) ).thenReturn( genericList );

    }

    @Test
    public void testGenericListRenderer() throws Exception {

        // execute logic
        genericListRenderer.init( bindings );

        // test logic
        assertNotNull( genericListRenderer.getValues() );
    }

    @Test
    public void testGenericListRendererWhenListPageIsNull() throws Exception {

        // setup logic
        bindings.put( "listpath", null );

        // execute logic
        genericListRenderer.init( bindings );

        // test logic
        assertNull( genericListRenderer.getValues() );
    }
    
    @Test
    public void testGenericListRendererWhenListPathIsNull() throws Exception {

        // setup logic
        when( pageManager.getPage( "SampleList" ) ).thenReturn( null );

        // execute logic
        genericListRenderer.init( bindings );

        // test logic
        assertNull( genericListRenderer.getValues() );
    }
    

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final GenericListRenderer genericListRenderer = new GenericListRenderer();

        // verify logic
        assertNotNull( genericListRenderer );
    }

}
