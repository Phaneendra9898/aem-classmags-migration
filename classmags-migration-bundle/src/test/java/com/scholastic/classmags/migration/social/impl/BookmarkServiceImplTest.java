package com.scholastic.classmags.migration.social.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.xss.XSSAPI;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.adobe.cq.social.srp.SocialResourceProvider;
import com.adobe.cq.social.srp.config.SocialResourceConfiguration;
import com.adobe.cq.social.ugcbase.SocialUtils;
import com.scholastic.classmags.migration.social.api.BookmarkSocialComponent;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationASRPConstants;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.RoleUtil;
import com.scholastic.classmags.migration.utils.SocialASRPUtils;

/**
 * The Class BookmarkServiceImplTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { BookmarkServiceImpl.class, SocialASRPUtils.class, RoleUtil.class } )
public class BookmarkServiceImplTest {

    /** The bookmark service. */
    private BookmarkServiceImpl bookmarkService;

    /** The Constant TEST_USER. */
    private static final String TEST_USER = "testUser";

    /** The Constant TEST_APP_NAME. */
    private static final String TEST_APP_NAME = "scienceworld";

    /** The Constant TEST_BOOKMARK_PAGE_PATH. */
    private static final String TEST_BOOKMARK_PAGE_PATH = "testBookmarkPath";

    /** The Constant TEST_BOOKMARK_TEACHING_RESOURCE_PATH. */
    private static final String TEST_BOOKMARK_TEACHING_RESOURCE_PATH = "testTeachingResourcePath";

    /** The Constant TEST_VIEW_ARTICLE_LINK_PATH. */
    private static final String TEST_VIEW_ARTICLE_LINK_PATH = "testViewArticleLinkPath";

    /** The Constant TEST_PATH. */
    private static final String TEST_PATH = "/content/usergenerated/asi/cloud/content/classroom_magazines/home/users/teachertestUser/scienceworld";

    /** The Constant TEST_USER_PATH. */
    private static final String TEST_USER_PATH = "/content/usergenerated/asi/cloud/content/classroom_magazines/home/users/teachertestUser";

    /** The Constant TEST_ENCODED_DATA. */
    private static final String TEST_ENCODED_DATA = "testEncodedData";

    /** The Constant TEST_DATA. */
    private static final String TEST_DATA = "testData";

    /** The mock sling http servlet request. */
    @Mock
    private SlingHttpServletRequest mockSlingHttpServletRequest;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock resource resolver factory. */
    @Mock
    private ResourceResolverFactory mockResourceResolverFactory;

    /** The mock resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;

    /** The mock social utils. */
    @Mock
    private SocialUtils mockSocialUtils;

    /** The mock srp. */
    @Mock
    private SocialResourceProvider mockSrp;

    /** The mock social resource configuration. */
    @Mock
    private SocialResourceConfiguration mockSocialResourceConfiguration;

    /** The mock xssapi. */
    @Mock
    private XSSAPI mockXssapi;

    /** The mock property map. */
    @Mock
    private ModifiableValueMap mockPropertyMap;

    /** The mock props map. */
    @Mock
    private Map< String, Object > mockPropsMap;

    /** The mock set. */
    @Mock
    private Set< String > mockSet;

    /** The mock iterator. */
    @Mock
    private Iterator< String > mockIterator;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        bookmarkService = new BookmarkServiceImpl();

        Whitebox.setInternalState( bookmarkService, mockResourceResolverFactory );
        Whitebox.setInternalState( bookmarkService, mockSocialUtils );
        Whitebox.setInternalState( bookmarkService, mockXssapi );

        PowerMockito.mockStatic( RoleUtil.class );
        PowerMockito.mockStatic( SocialASRPUtils.class );
        when( RoleUtil.getUserRole( mockSlingHttpServletRequest ) ).thenReturn( RoleUtil.ROLE_TYPE_TEACHER );
        when( SocialASRPUtils.fetchUserId( mockSlingHttpServletRequest, RoleUtil.ROLE_TYPE_TEACHER ) )
                .thenReturn( TEST_USER );
        when( SocialASRPUtils.isValidUser( RoleUtil.ROLE_TYPE_TEACHER, TEST_USER ) ).thenReturn( true );
    }

    /**
     * Test get bookmark resource.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetBookmarkResource() throws Exception {

        when( mockSlingHttpServletRequest.getResource() ).thenReturn( mockResource );
        when( CommonUtils.getResourceResolver( mockResourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE ) ).thenReturn( mockResourceResolver );
        when( mockSocialUtils.mayAccessUGC( mockResourceResolver ) ).thenReturn( true );

        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.BOOKMARK_PAGE_PATH ) )
                .thenReturn( TEST_BOOKMARK_PAGE_PATH );
        when( mockXssapi.filterHTML( TEST_BOOKMARK_PAGE_PATH ) ).thenReturn( TEST_BOOKMARK_PAGE_PATH );

        when( mockSlingHttpServletRequest
                .getParameter( ClassMagsMigrationASRPConstants.BOOKMARK_TEACHING_RESOURCE_PATH ) )
                        .thenReturn( TEST_BOOKMARK_TEACHING_RESOURCE_PATH );
        when( mockXssapi.filterHTML( TEST_BOOKMARK_TEACHING_RESOURCE_PATH ) )
                .thenReturn( TEST_BOOKMARK_TEACHING_RESOURCE_PATH );

        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.VIEW_ARTICLE_LINK_PATH ) )
                .thenReturn( TEST_VIEW_ARTICLE_LINK_PATH );
        when( mockXssapi.filterHTML( TEST_VIEW_ARTICLE_LINK_PATH ) ).thenReturn( TEST_VIEW_ARTICLE_LINK_PATH );

        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.APP_NAME ) )
                .thenReturn( TEST_APP_NAME );
        when( mockXssapi.filterHTML( TEST_APP_NAME ) ).thenReturn( TEST_APP_NAME );

        when( mockSocialUtils.getSocialResourceProvider( mockResource ) ).thenReturn( mockSrp );
        when( mockSocialUtils.getDefaultStorageConfig() ).thenReturn( mockSocialResourceConfiguration );
        when( SocialASRPUtils.getUGCResourceFromPath( mockResourceResolver, TEST_PATH, mockSrp ) )
                .thenReturn( mockResource );
        when( mockResource.isResourceType( BookmarkSocialComponent.BOOKMARK_RESOURCE_TYPE ) ).thenReturn( true );
        when( mockResource.adaptTo( ModifiableValueMap.class ) ).thenReturn( mockPropertyMap );
        when( SocialASRPUtils.urlEncoder( TEST_BOOKMARK_PAGE_PATH ) ).thenReturn( TEST_ENCODED_DATA );
        when( SocialASRPUtils.urlEncoder( TEST_BOOKMARK_TEACHING_RESOURCE_PATH ) ).thenReturn( TEST_ENCODED_DATA );

        assertEquals( mockResource, bookmarkService.getBookmarkResource( mockSlingHttpServletRequest ) );
    }

    /**
     * Test get bookmark resource with no resource.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetBookmarkResourceWithNoResource() throws Exception {

        when( mockSlingHttpServletRequest.getResource() ).thenReturn( mockResource );
        when( CommonUtils.getResourceResolver( mockResourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE ) ).thenReturn( mockResourceResolver );
        when( mockSocialUtils.mayAccessUGC( mockResourceResolver ) ).thenReturn( true );

        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.BOOKMARK_PAGE_PATH ) )
                .thenReturn( TEST_BOOKMARK_PAGE_PATH );
        when( mockXssapi.filterHTML( TEST_BOOKMARK_PAGE_PATH ) ).thenReturn( TEST_BOOKMARK_PAGE_PATH );

        when( mockSlingHttpServletRequest
                .getParameter( ClassMagsMigrationASRPConstants.BOOKMARK_TEACHING_RESOURCE_PATH ) )
                        .thenReturn( TEST_BOOKMARK_TEACHING_RESOURCE_PATH );
        when( mockXssapi.filterHTML( TEST_BOOKMARK_TEACHING_RESOURCE_PATH ) )
                .thenReturn( TEST_BOOKMARK_TEACHING_RESOURCE_PATH );

        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.VIEW_ARTICLE_LINK_PATH ) )
                .thenReturn( TEST_VIEW_ARTICLE_LINK_PATH );
        when( mockXssapi.filterHTML( TEST_VIEW_ARTICLE_LINK_PATH ) ).thenReturn( TEST_VIEW_ARTICLE_LINK_PATH );

        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.APP_NAME ) )
                .thenReturn( TEST_APP_NAME );
        when( mockXssapi.filterHTML( TEST_APP_NAME ) ).thenReturn( TEST_APP_NAME );

        when( mockSocialUtils.getSocialResourceProvider( mockResource ) ).thenReturn( mockSrp );
        when( mockSocialUtils.getDefaultStorageConfig() ).thenReturn( mockSocialResourceConfiguration );
        when( SocialASRPUtils.getUGCResourceFromPath( mockResourceResolver, TEST_PATH, mockSrp ) ).thenReturn( null );
        when( SocialASRPUtils.urlEncoder( TEST_BOOKMARK_PAGE_PATH ) ).thenReturn( TEST_ENCODED_DATA );
        when( SocialASRPUtils.urlEncoder( TEST_BOOKMARK_TEACHING_RESOURCE_PATH ) ).thenReturn( TEST_ENCODED_DATA );
        when( mockSrp.create( mockResourceResolver, TEST_PATH, mockPropsMap ) ).thenReturn( mockResource );

        assertEquals( null, bookmarkService.getBookmarkResource( mockSlingHttpServletRequest ) );
    }

    /**
     * Test delete bookmark resource.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDeleteBookmarkResource() throws Exception {

        when( mockSlingHttpServletRequest.getResource() ).thenReturn( mockResource );
        when( CommonUtils.getResourceResolver( mockResourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE ) ).thenReturn( mockResourceResolver );
        when( mockSocialUtils.mayAccessUGC( mockResourceResolver ) ).thenReturn( true );

        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.BOOKMARK_PAGE_PATH ) )
                .thenReturn( TEST_BOOKMARK_PAGE_PATH );
        when( mockXssapi.filterHTML( TEST_BOOKMARK_PAGE_PATH ) ).thenReturn( TEST_BOOKMARK_PAGE_PATH );

        when( mockSlingHttpServletRequest
                .getParameter( ClassMagsMigrationASRPConstants.BOOKMARK_TEACHING_RESOURCE_PATH ) )
                        .thenReturn( TEST_BOOKMARK_TEACHING_RESOURCE_PATH );
        when( mockXssapi.filterHTML( TEST_BOOKMARK_TEACHING_RESOURCE_PATH ) )
                .thenReturn( TEST_BOOKMARK_TEACHING_RESOURCE_PATH );

        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.APP_NAME ) )
                .thenReturn( TEST_APP_NAME );
        when( mockXssapi.filterHTML( TEST_APP_NAME ) ).thenReturn( TEST_APP_NAME );

        when( mockSocialUtils.getSocialResourceProvider( mockResource ) ).thenReturn( mockSrp );
        when( mockSocialUtils.getDefaultStorageConfig() ).thenReturn( mockSocialResourceConfiguration );
        when( SocialASRPUtils.getUGCResourceFromPath( mockResourceResolver, TEST_PATH, mockSrp ) )
                .thenReturn( mockResource );
        when( mockResource.getParent() ).thenReturn( mockResource );
        when( mockResource.adaptTo( ModifiableValueMap.class ) ).thenReturn( mockPropertyMap );
        when( SocialASRPUtils.urlEncoder( TEST_BOOKMARK_PAGE_PATH ) ).thenReturn( TEST_ENCODED_DATA );
        when( SocialASRPUtils.urlEncoder( TEST_BOOKMARK_TEACHING_RESOURCE_PATH ) ).thenReturn( TEST_ENCODED_DATA );
        when( mockPropertyMap
                .containsKey( ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_PAGE.concat( TEST_ENCODED_DATA ) ) )
                        .thenReturn( true );
        when( mockPropertyMap
                .containsKey( ClassMagsMigrationASRPConstants.BOOKMARK_TYPE_ASSET.concat( TEST_ENCODED_DATA ) ) )
                        .thenReturn( true );
        when( mockResource.getValueMap() ).thenReturn( mockPropertyMap );
        when( mockPropertyMap.keySet() ).thenReturn( mockSet );
        when( mockSet.iterator() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( TEST_DATA );

        assertEquals( mockResource, bookmarkService.deleteBookmarkResource( mockSlingHttpServletRequest ) );
    }

    /**
     * Test get all site bookmark resource.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetAllSiteBookmarkResource() throws Exception {

        when( mockSlingHttpServletRequest.getResource() ).thenReturn( mockResource );
        when( CommonUtils.getResourceResolver( mockResourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE ) ).thenReturn( mockResourceResolver );
        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.APP_NAME ) )
                .thenReturn( TEST_APP_NAME );
        when( mockXssapi.filterHTML( TEST_APP_NAME ) ).thenReturn( TEST_APP_NAME );
        when( mockSocialUtils.getSocialResourceProvider( mockResource ) ).thenReturn( mockSrp );
        when( mockSocialUtils.getDefaultStorageConfig() ).thenReturn( mockSocialResourceConfiguration );
        when( SocialASRPUtils.getUGCResourceFromPath( mockResourceResolver, TEST_PATH, mockSrp ) )
                .thenReturn( mockResource );

        assertEquals( mockResource, bookmarkService.getAllSiteBookmarkResource( mockSlingHttpServletRequest ) );
    }

    /**
     * Test get my bookmarks resource.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetMyBookmarksResource() throws Exception {

        when( mockSlingHttpServletRequest.getResource() ).thenReturn( mockResource );
        when( CommonUtils.getResourceResolver( mockResourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE ) ).thenReturn( mockResourceResolver );
        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.APP_NAME ) )
                .thenReturn( TEST_APP_NAME );
        when( mockXssapi.filterHTML( TEST_APP_NAME ) ).thenReturn( TEST_APP_NAME );
        when( mockSocialUtils.getSocialResourceProvider( mockResource ) ).thenReturn( mockSrp );
        when( mockSocialUtils.getDefaultStorageConfig() ).thenReturn( mockSocialResourceConfiguration );
        when( SocialASRPUtils.getUGCResourceFromPath( mockResourceResolver, TEST_USER_PATH, mockSrp ) )
                .thenReturn( mockResource );

        assertEquals( mockResource, bookmarkService.getMyBookmarksResource( mockSlingHttpServletRequest ) );
    }

    /**
     * Test delete user bookmark resource.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testDeleteUserBookmarkResource() throws Exception {

        when( mockSlingHttpServletRequest.getResource() ).thenReturn( mockResource );
        when( CommonUtils.getResourceResolver( mockResourceResolverFactory,
                ClassMagsMigrationConstants.WRITE_SERVICE ) ).thenReturn( mockResourceResolver );

        when( mockSlingHttpServletRequest.getParameter( ClassMagsMigrationASRPConstants.BOOKMARK_PAGE_PATH ) )
                .thenReturn( TEST_BOOKMARK_PAGE_PATH );
        when( mockXssapi.filterHTML( TEST_BOOKMARK_PAGE_PATH ) ).thenReturn( TEST_BOOKMARK_PAGE_PATH );

        when( mockSocialUtils.getSocialResourceProvider( mockResource ) ).thenReturn( mockSrp );
        when( mockSocialUtils.getDefaultStorageConfig() ).thenReturn( mockSocialResourceConfiguration );
        when( SocialASRPUtils.getUGCResourceFromPath( mockResourceResolver, TEST_BOOKMARK_PAGE_PATH, mockSrp ) )
                .thenReturn( mockResource );
        when( mockResource.getParent() ).thenReturn( mockResource );

        assertEquals( mockResource, bookmarkService.deleteUserBookmarkResource( mockSlingHttpServletRequest ) );
    }
}
