package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.jcr.Property;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.tagging.TagManager;
import com.scholastic.classmags.migration.models.ContentTile;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for ContentTileServiceImpl.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { ContentTileServiceImpl.class, CommonUtils.class } )
public class ContentTileServiceImplTest {

    /** The content tile service. */
    private ContentTileServiceImpl contentTileService;

    /** The Constant PATH. */
    private static final String TEST_PATH = "/testPath";

    /** The Constant TEST_DATA. */
    private static final String TEST_DATA = "testData";

    /** The Constant TEST_DATA_LONG. */
    private static final long TEST_DATA_LONG = 101;

    /** The Constant JCR_SORTING_DATE_FORMAT. */
    private static final String JCR_SORTING_DATE_FORMAT = "jcr:sortingDateFormat";

    /** The test date. */
    private Date testDate;

    /** The resource resolver factory. */
    @Mock
    private ResourceResolverFactory mockResourceResolverFactory;

    /** The resource path config service. */
    @Mock
    private ResourcePathConfigService resourcePathConfigService;

    /** The resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock property. */
    @Mock
    private Property mockProperty;

    /** The mock asset. */
    @Mock
    private Asset mockAsset;

    /** The mock content tile. */
    @Mock
    private ContentTile mockContentTile;

    /** The mock tag manager. */
    @Mock
    private TagManager mockTagManager;

    /** The mock value map. */
    @Mock
    private ValueMap mockValueMap;

    /** The mock tags. */
    @Mock
    private List< String > mockTags;

    /** The mock magazine props. */
    @Mock
    private MagazineProps mockMagazineProps;

    /** The mock property config service. */
    @Mock
    private PropertyConfigService mockPropertyConfigService;

    /** The mock calendar. */
    @Mock
    private Calendar mockCalendar;

    /** The logger. */
    @Mock
    private Logger logger;

    /**
     * Sets the up.
     */
    @Before
    public void setUp() {
        contentTileService = new ContentTileServiceImpl();
        testDate = Calendar.getInstance().getTime();

        Whitebox.setInternalState( contentTileService, mockResourceResolverFactory );
        Whitebox.setInternalState( contentTileService, resourcePathConfigService );
        Whitebox.setInternalState( contentTileService, mockPropertyConfigService );
        Whitebox.setInternalState( contentTileService, mockMagazineProps );
        Whitebox.setInternalState( ContentTileServiceImpl.class, "LOG", logger );

        PowerMockito.mockStatic( CommonUtils.class );
        when( CommonUtils.getTagManager( mockResourceResolverFactory ) ).thenReturn( mockTagManager );
        when( CommonUtils.getSubjectTags( mockResource, mockTagManager ) ).thenReturn( mockTags );
        when( CommonUtils.getFormattedSubjectTags( mockTags ) ).thenReturn( TEST_DATA );
        when( CommonUtils.getDataPath( resourcePathConfigService, TEST_DATA ) ).thenReturn( TEST_PATH );
        when( CommonUtils.getSortingDate( mockValueMap, TEST_DATA ) ).thenReturn( TEST_DATA );
        when( CommonUtils.getAssetSortingDate( mockValueMap, TEST_DATA ) ).thenReturn( TEST_DATA );
        when( CommonUtils.getResourceResolver( mockResourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ) )
                .thenReturn( mockResourceResolver );
    }

    /**
     * Test get article content.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetArticleContent() throws LoginException {

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH + "/" + JcrConstants.JCR_CONTENT ) )
                .thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockMagazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockPropertyConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT, TEST_PATH ) )
                .thenReturn( TEST_DATA );

        assertEquals( TEST_DATA, contentTileService.getArticleContent( mockContentTile, TEST_PATH ).getSortingDate() );
    }

    /**
     * Test get article content with exception.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetArticleContentWithException() throws LoginException {

        LoginException loginException = new LoginException();

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH + "/" + JcrConstants.JCR_CONTENT ) )
                .thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockMagazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockPropertyConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT, TEST_PATH ) )
                .thenThrow( loginException );

        assertEquals( null, contentTileService.getArticleContent( mockContentTile, TEST_PATH ).getSortingDate() );
    }

    /**
     * Test get article content no link.
     */
    @Test
    public void testGetArticleContentNoLink() {

        when( mockContentTile.getContentTileLink() ).thenReturn( "" );

        assertEquals( null, contentTileService.getArticleContent( mockContentTile, TEST_PATH ) );
    }

    /**
     * Test get article content with no resource.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetArticleContentWithNoResource() throws LoginException {

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH + "/" + JcrConstants.JCR_CONTENT ) ).thenReturn( null );

        assertEquals( null, contentTileService.getArticleContent( mockContentTile, TEST_PATH ) );
    }

    /**
     * Test get issue content.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetIssueContent() throws LoginException {

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH + "/" + JcrConstants.JCR_CONTENT ) )
                .thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockMagazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockPropertyConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT, TEST_PATH ) )
                .thenReturn( TEST_DATA );

        assertEquals( TEST_DATA, contentTileService.getIssueContent( mockContentTile, TEST_PATH ).getSortingDate() );
    }

    /**
     * Test get issue content with exception.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetIssueContentWithException() throws LoginException {

        LoginException loginException = new LoginException();

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH + "/" + JcrConstants.JCR_CONTENT ) )
                .thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockMagazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockPropertyConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT, TEST_PATH ) )
                .thenThrow( loginException );

        assertEquals( null, contentTileService.getIssueContent( mockContentTile, TEST_PATH ).getSortingDate() );
    }

    /**
     * Test get issue content no link.
     */
    @Test
    public void testGetIssueContentNoLink() {

        when( mockContentTile.getContentTileLink() ).thenReturn( "" );

        assertEquals( null, contentTileService.getIssueContent( mockContentTile, TEST_PATH ) );
    }

    /**
     * Test get issue content with no resource.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetIssueContentWithNoResource() throws LoginException {

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH + "/" + JcrConstants.JCR_CONTENT ) ).thenReturn( null );

        assertEquals( null, contentTileService.getIssueContent( mockContentTile, TEST_PATH ) );
    }

    /**
     * Test get asset content.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetAssetContent() throws LoginException {

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH ) ).thenReturn( mockResource );
        when( mockResource.adaptTo( Asset.class ) ).thenReturn( mockAsset );
        when( mockResource.getChild( "jcr:content/metadata" ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockAsset.getName() ).thenReturn( TEST_DATA );
        when( mockAsset.getMetadataValue( DamConstants.DC_TITLE ) ).thenReturn( TEST_DATA );
        when( mockAsset.getMetadataValue( DamConstants.DC_DESCRIPTION ) ).thenReturn( TEST_DATA );
        when( mockMagazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockPropertyConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT, TEST_PATH ) )
                .thenReturn( TEST_DATA );
        when( mockAsset.getPath() ).thenReturn( TEST_PATH );

        assertEquals( TEST_DATA, contentTileService.getAssetContent( mockContentTile, TEST_PATH ).getSortingDate() );
    }

    /**
     * Test get asset content with exception.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetAssetContentWithException() throws LoginException {

        LoginException loginException = new LoginException();

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH ) ).thenReturn( mockResource );
        when( mockResource.adaptTo( Asset.class ) ).thenReturn( mockAsset );
        when( mockResource.getChild( "jcr:content/metadata" ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockAsset.getName() ).thenReturn( TEST_DATA );
        when( mockAsset.getMetadataValue( DamConstants.DC_TITLE ) ).thenReturn( TEST_DATA );
        when( mockAsset.getMetadataValue( DamConstants.DC_DESCRIPTION ) ).thenReturn( TEST_DATA );
        when( mockMagazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockPropertyConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT, TEST_PATH ) )
                .thenThrow( loginException );

        assertEquals( null, contentTileService.getAssetContent( mockContentTile, TEST_PATH ).getSortingDate() );
    }

    /**
     * Test get asset content with no asset data.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetAssetContentWithNoAssetData() throws LoginException {

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH ) ).thenReturn( mockResource );
        when( mockResource.adaptTo( Asset.class ) ).thenReturn( null );

        assertEquals( null, contentTileService.getAssetContent( mockContentTile, TEST_PATH ) );
    }

    /**
     * Test get asset content with no meta data resource.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetAssetContentWithNoMetaDataResource() throws LoginException {

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH ) ).thenReturn( mockResource );
        when( mockResource.adaptTo( Asset.class ) ).thenReturn( mockAsset );
        when( mockResource.getChild( "jcr:content/metadata" ) ).thenReturn( null );

        assertEquals( null, contentTileService.getAssetContent( mockContentTile, TEST_PATH ) );
    }

    /**
     * Test get asset content with no resource.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetAssetContentWithNoResource() throws LoginException {

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH ) ).thenReturn( null );

        assertEquals( null, contentTileService.getAssetContent( mockContentTile, TEST_PATH ) );
    }

    /**
     * Test get video content.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetVideoContent() throws LoginException {

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH ) ).thenReturn( mockResource );
        when( mockResource.adaptTo( Asset.class ) ).thenReturn( mockAsset );
        when( mockResource.getChild( "jcr:content/metadata" ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( "videoDuration", StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMap.containsKey( "videoId" ) ).thenReturn( true );
        when( mockValueMap.get( "videoId", Long.class ) ).thenReturn( TEST_DATA_LONG );
        when( mockAsset.getName() ).thenReturn( TEST_DATA );
        when( mockAsset.getMetadataValue( DamConstants.DC_TITLE ) ).thenReturn( TEST_DATA );
        when( mockAsset.getMetadataValue( DamConstants.DC_DESCRIPTION ) ).thenReturn( TEST_DATA );
        when( mockMagazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockPropertyConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT, TEST_PATH ) )
                .thenReturn( TEST_DATA );
        when( mockAsset.getPath() ).thenReturn( TEST_PATH );

        assertEquals( TEST_DATA, contentTileService.getVideoContent( mockContentTile, TEST_PATH ).getSortingDate() );
    }

    /**
     * Test get video content with exception.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetVideoContentWithException() throws LoginException {

        LoginException loginException = new LoginException();

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH ) ).thenReturn( mockResource );
        when( mockResource.adaptTo( Asset.class ) ).thenReturn( mockAsset );
        when( mockResource.getChild( "jcr:content/metadata" ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( "videoDuration", StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMap.containsKey( "videoId" ) ).thenReturn( true );
        when( mockValueMap.get( "videoId", Long.class ) ).thenReturn( TEST_DATA_LONG );
        when( mockAsset.getName() ).thenReturn( TEST_DATA );
        when( mockAsset.getMetadataValue( DamConstants.DC_TITLE ) ).thenReturn( TEST_DATA );
        when( mockAsset.getMetadataValue( DamConstants.DC_DESCRIPTION ) ).thenReturn( TEST_DATA );
        when( mockMagazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockPropertyConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT, TEST_PATH ) )
                .thenThrow( loginException );

        assertEquals( null, contentTileService.getVideoContent( mockContentTile, TEST_PATH ).getSortingDate() );
    }

    /**
     * Test get video content with no asset data.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetVideoContentWithNoAssetData() throws LoginException {

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH ) ).thenReturn( mockResource );
        when( mockResource.adaptTo( Asset.class ) ).thenReturn( null );

        assertEquals( null, contentTileService.getVideoContent( mockContentTile, TEST_PATH ) );
    }

    /**
     * Test get video content with no meta data resource.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetVideoContentWithNoMetaDataResource() throws LoginException {

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH ) ).thenReturn( mockResource );
        when( mockResource.adaptTo( Asset.class ) ).thenReturn( mockAsset );
        when( mockResource.getChild( "jcr:content/metadata" ) ).thenReturn( null );

        assertEquals( null, contentTileService.getVideoContent( mockContentTile, TEST_PATH ) );
    }

    /**
     * Test get video content with no resource.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetVideoContentWithNoResource() throws LoginException {

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH ) ).thenReturn( null );

        assertEquals( null, contentTileService.getVideoContent( mockContentTile, TEST_PATH ) );
    }

    /**
     * Test get video content with no video data.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetVideoContentWithNoVideoData() throws LoginException {

        when( mockContentTile.getContentTileLink() ).thenReturn( TEST_PATH );
        when( mockResourceResolver.getResource( TEST_PATH ) ).thenReturn( mockResource );
        when( mockResource.adaptTo( Asset.class ) ).thenReturn( mockAsset );
        when( mockResource.getChild( "jcr:content/metadata" ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( "videoDuration", StringUtils.EMPTY ) ).thenReturn( StringUtils.EMPTY );
        when( mockValueMap.containsKey( "videoId" ) ).thenReturn( false );
        when( mockValueMap.get( "videoId", Long.class ) ).thenReturn( TEST_DATA_LONG );
        when( mockAsset.getName() ).thenReturn( TEST_DATA );
        when( mockAsset.getMetadataValue( DamConstants.DC_TITLE ) ).thenReturn( TEST_DATA );
        when( mockAsset.getMetadataValue( DamConstants.DC_DESCRIPTION ) ).thenReturn( TEST_DATA );
        when( mockMagazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockPropertyConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT, TEST_PATH ) )
                .thenReturn( TEST_DATA );
        when( mockAsset.getPath() ).thenReturn( TEST_PATH );

        assertEquals( TEST_DATA, contentTileService.getVideoContent( mockContentTile, TEST_PATH ).getSortingDate() );
    }

    /**
     * Test get custom content.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetCustomContent() throws LoginException {

        when( mockContentTile.getCustomContentTitle() ).thenReturn( TEST_DATA );
        when( mockContentTile.getCustomContentDate() ).thenReturn( mockCalendar );
        when( mockCalendar.getTime() ).thenReturn( testDate );
        when( mockContentTile.getCustomContentLink() ).thenReturn( TEST_PATH );
        when( mockContentTile.getCustomContentDescText() ).thenReturn( TEST_DATA );
        when( mockContentTile.getFileReference() ).thenReturn( TEST_DATA );
        when( mockMagazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockPropertyConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT, TEST_PATH ) )
                .thenReturn( "MMMM yyyy" );

        assertEquals( TEST_DATA,
                contentTileService.getCustomContent( mockContentTile, TEST_PATH ).getContentTileDesc() );
    }

    /**
     * Test get custom content with no date format.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetCustomContentWithNoDateFormat() throws LoginException {

        when( mockContentTile.getCustomContentTitle() ).thenReturn( TEST_DATA );
        when( mockContentTile.getCustomContentDate() ).thenReturn( mockCalendar );
        when( mockCalendar.getTime() ).thenReturn( testDate );
        when( mockContentTile.getCustomContentLink() ).thenReturn( TEST_PATH );
        when( mockContentTile.getCustomContentDescText() ).thenReturn( TEST_DATA );
        when( mockContentTile.getFileReference() ).thenReturn( TEST_DATA );
        when( mockMagazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockPropertyConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT, TEST_PATH ) )
                .thenReturn( "NO DATA AVAILABLE" );

        assertEquals( TEST_DATA,
                contentTileService.getCustomContent( mockContentTile, TEST_PATH ).getContentTileDesc() );
    }

    /**
     * Test get custom content with exception.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetCustomContentWithException() throws LoginException {

        LoginException loginException = new LoginException();

        when( mockContentTile.getCustomContentTitle() ).thenReturn( TEST_DATA );
        when( mockContentTile.getCustomContentDate() ).thenReturn( mockCalendar );
        when( mockCalendar.getTime() ).thenReturn( testDate );
        when( mockContentTile.getCustomContentLink() ).thenReturn( TEST_PATH );
        when( mockContentTile.getCustomContentDescText() ).thenReturn( TEST_DATA );
        when( mockContentTile.getFileReference() ).thenReturn( TEST_DATA );
        when( mockMagazineProps.getMagazineName( TEST_PATH ) ).thenReturn( TEST_DATA );
        when( mockPropertyConfigService.getDataConfigProperty( JCR_SORTING_DATE_FORMAT, TEST_PATH ) )
                .thenThrow( loginException );

        assertEquals( TEST_DATA,
                contentTileService.getCustomContent( mockContentTile, TEST_PATH ).getContentTileDesc() );

        verify( logger, times( 1 ) ).error( "Exception occured while retriving date format, e : {}", loginException );
    }
}
