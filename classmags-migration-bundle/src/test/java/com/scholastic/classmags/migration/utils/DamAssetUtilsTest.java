package com.scholastic.classmags.migration.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.day.cq.commons.Externalizer;
import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;

@RunWith( PowerMockRunner.class )
@PrepareForTest( { DamAssetUtils.class, DamAssetConstants.class, CommonUtils.class} )
public class DamAssetUtilsTest {
	
	@InjectMocks
    private DamAssetUtils damAssetUtils = new DamAssetUtils();

	 /** The logger. */
	@Mock
    private Logger logger;
    
    @Mock
    private SlingHttpServletRequest request;

    @Mock
    private PropertyConfigService propertyConfigService;

    @Mock
    private ResourcePathConfigService resourcePathConfigService;

    @Mock
    private ResourceResolver resourceResolver;
	
    @Mock
    private QueryBuilder queryBuilder;
	 
    @Mock
    private Session currentSession;
	 
    @Mock
    private Query query;
	 
    @Mock
    private SearchResult searchResult;
	 
    @Mock
    private List<Hit> emptyHitList = new ArrayList<Hit>();
    
    @Mock
    private List<Hit> hitList = new ArrayList<Hit>();
	
    @Mock
    private Hit hitTest1;
	 
    @Mock
    private Externalizer externalizer;	
	 
    @Mock 
    private Node node, assetNode, assetSubNode;
	 
    @Mock
    private Property property, tagProperty, videoProperty;
	 
    @Mock
    private Value value, value2;
	
    @Mock
    private JSONArray tagPropertyJSONArray;
	
    @Mock
    private String path;
	 
    @Mock
    private String mimeType;
	 
    @Mock
    private Map< String, String > mockQueryMap;
    
    @Mock
    private String mockProperty;
    
    @Mock
    private JSONObject metaDataJSONObject,playerIDJsonObject;

    /**
     * Initial Setup Method.
	 * @throws RepositoryException 
     */
    @Before
    public void setUp() throws RepositoryException {
    	path = "/content/dam/classroom-magazines/scholasticnews3";
        
    	Whitebox.setInternalState( DamAssetUtils.class, "LOG", logger );

        hitList.add(hitTest1);
    }

    @Test
    public void testCreateInstance() throws Exception {
        // execute logic
        final DamAssetUtils damAssetUtils = new DamAssetUtils();

        // verify logic
        assertNotNull( damAssetUtils );
    }
	
    @Test
    public void damAssetUtilsTestCurrentSessionNotNull() throws Exception {

		//Mock method calls
		when(request.getResourceResolver()).thenReturn(resourceResolver);
	        
		//Execute Logic
		JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
				propertyConfigService, resourcePathConfigService);

		// verify logic
        assertNotNull(metaDataJSONArray);
	}
    
        @Test
    public void damAssetUtilsTestIsValidPath() throws Exception {

		// verify logic
        assertTrue(DamAssetUtils.isValidPath(path));
	}
	
	@Test
    public void damAssetUtilsTestIsInValidPath() throws Exception {

		// verify logic
        assertFalse(DamAssetUtils.isValidPath("content/dam/classroom-magazines/scholasticnews3"));
        assertFalse(DamAssetUtils.isValidPath("content/dam/sample/scholasticnews3"));
        assertFalse(DamAssetUtils.isValidPath("/content/dam/sample/scholasticnews3"));
        assertFalse(DamAssetUtils.isValidPath(null));
	}

	@Test
    public void damAssetUtilsTestImage() throws Exception {

		mimeType = DamAssetConstants.MIME_TYPE_IMAGE;
		//Mock method calls
		when(request.getResourceResolver()).thenReturn(resourceResolver);
		when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
		when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
		when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
		mockQueryMap = mockQueryBuilder(DamAssetConstants.MIME_TYPE_IMAGE);
		when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
		when( query.getResult()).thenReturn(searchResult);
		when(searchResult.getHits()).thenReturn(emptyHitList);
		
		//Execute Logic
		JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
				propertyConfigService, resourcePathConfigService);

		// verify logic
        assertNotNull(metaDataJSONArray);

	}

	@Test
    public void damAssetUtilsTestAudio() throws Exception {

		mimeType = DamAssetConstants.MIME_TYPE_AUDIO;
		//Mock method calls
		when(request.getResourceResolver()).thenReturn(resourceResolver);
		when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
		when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
		when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
		mockQueryMap = mockQueryBuilder(DamAssetConstants.MIME_TYPE_AUDIO);
		when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
		when( query.getResult()).thenReturn(searchResult);
		when(searchResult.getHits()).thenReturn(emptyHitList);
		
		//Execute Logic
		JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
				propertyConfigService, resourcePathConfigService);

		// verify logic
        assertNotNull(metaDataJSONArray);

	}
	
	@Test
    public void damAssetUtilsTestVideo() throws Exception {

		mimeType = DamAssetConstants.MIME_TYPE_VIDEO;
		//Mock method calls
		when(request.getResourceResolver()).thenReturn(resourceResolver);
		when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
		when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
		when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
		mockQueryMap = mockQueryBuilder(DamAssetConstants.MIME_TYPE_VIDEO);
		when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
		when( query.getResult()).thenReturn(searchResult);
		when(searchResult.getHits()).thenReturn(emptyHitList);
		
		//Execute Logic
		JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
				propertyConfigService, resourcePathConfigService);

		// verify logic
        assertNotNull(metaDataJSONArray);

	}
	
	@Test
    public void damAssetUtilsTestApplication() throws Exception {

		mimeType = DamAssetConstants.MIME_TYPE_APPLICATION;
		//Mock method calls
		when(request.getResourceResolver()).thenReturn(resourceResolver);
		when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
		when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
		when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
		mockQueryMap = mockQueryBuilder(DamAssetConstants.MIME_TYPE_AUDIO);
		when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
		when( query.getResult()).thenReturn(searchResult);
		when(searchResult.getHits()).thenReturn(emptyHitList);
		
		//Execute Logic
		JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
				propertyConfigService, resourcePathConfigService);

		// verify logic
        assertNotNull(metaDataJSONArray);

	}

	@Test
    public void damAssetUtilsTestTitleCheck() throws Exception {

		//Mock method calls
		when(request.getResourceResolver()).thenReturn(resourceResolver);
		when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
		when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
		when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
		mockQueryMap = mockQueryBuilder(DamAssetConstants.MIME_TYPE_IMAGE);
		when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
		when( query.getResult()).thenReturn(searchResult);
		when(searchResult.getHits()).thenReturn(hitList);
		when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
		when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
		when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(true);
		String title = DamAssetConstants.EMPTY_STRING;
		when(assetSubNode.getProperty(DamAssetConstants.DC_TITLE)).thenReturn(property);
		when(property.getValue()).thenReturn(value);
		when(value.getString()).thenReturn(title);

		//Execute Logic
		JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
				propertyConfigService, resourcePathConfigService);

		// verify logic
        assertNotNull(metaDataJSONArray);

	}

	@Test
    public void damAssetUtilsTestDescriptionCheck() throws Exception {

		//Mock method calls
		when(request.getResourceResolver()).thenReturn(resourceResolver);
		when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
		when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
		when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
		mockQueryMap = mockQueryBuilder("image");
		when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
		when( query.getResult()).thenReturn(searchResult);
		when(searchResult.getHits()).thenReturn(hitList);
		when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
		when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
		when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(true);
		String description = "";
		
		when(assetSubNode.getProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(property);
		when(property.getValue()).thenReturn(value);
		when(value.getString()).thenReturn(description);
		
		//Execute Logic
		JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
				propertyConfigService, resourcePathConfigService);

		// verify logic
        assertNotNull(metaDataJSONArray);

	}
	
	@Test
    public void damAssetUtilsTestSubassetsInUrlCheck() throws Exception {

		//Mock method calls
		when(request.getResourceResolver()).thenReturn(resourceResolver);
		when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
		when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
		when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
		mockQueryMap = mockQueryBuilder(DamAssetConstants.MIME_TYPE_IMAGE);
		when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
		when( query.getResult()).thenReturn(searchResult);
		when(searchResult.getHits()).thenReturn(hitList);
		when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL_SUBASSETS);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
		when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(true);
    	
    	String format = "video/mp4";
    	when(assetSubNode.getProperty(DamAssetConstants.DC_FORMAT)).thenReturn(property);
    	when(property.getValue()).thenReturn(value);
    	when(value.getString()).thenReturn(format);

    	when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.BRIGHTCOVE_VIDEO)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.VIDEO_ID)).thenReturn(false);

		//Execute Logic
		JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
				propertyConfigService, resourcePathConfigService);

		// verify logic
        assertNotNull(metaDataJSONArray);

	}

	
	@Test
    public void damAssetUtilsTestFileFormatCheck() throws Exception {

		//Mock method calls
		when(request.getResourceResolver()).thenReturn(resourceResolver);
		when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
		when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
		when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
		mockQueryMap = mockQueryBuilder("image");
		when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
		when( query.getResult()).thenReturn(searchResult);
		when(searchResult.getHits()).thenReturn(hitList);
		when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
		when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
		when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(true);
		String format = "";
		
		when(assetSubNode.getProperty(DamAssetConstants.DC_FORMAT)).thenReturn(property);
		when(property.getValue()).thenReturn(value);
		when(value.getString()).thenReturn(format);
		
		//Execute Logic
		JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
				propertyConfigService, resourcePathConfigService);

		// verify logic
        assertNotNull(metaDataJSONArray);

	}
    
    @Test
    public void damAssetUtilsTestMultipleTagsCheck() throws Exception {

		//Mock method calls
		when(request.getResourceResolver()).thenReturn(resourceResolver);
		when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
		when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
		when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
		mockQueryMap = mockQueryBuilder("image");
		when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
		when( query.getResult()).thenReturn(searchResult);
		when(searchResult.getHits()).thenReturn(hitList);
		when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
		when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
		when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(true);
		
		when(assetSubNode.getProperty(DamAssetConstants.CQ_TAGS)).thenReturn(tagProperty);
		when(tagProperty.isMultiple()).thenReturn(true);
		
		Value[] tagValues = {value,value2};
		when(tagProperty.getValues()).thenReturn(tagValues);
		
		//Execute Logic
		JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
				propertyConfigService, resourcePathConfigService);

		// verify logic
        assertNotNull(metaDataJSONArray);

	}
    @Test
    public void damAssetUtilsTestMultipleTagsWithNoDataCheck() throws Exception {

		//Mock method calls
		when(request.getResourceResolver()).thenReturn(resourceResolver);
		when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
		when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
		when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
		mockQueryMap = mockQueryBuilder("image");
		when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
		when( query.getResult()).thenReturn(searchResult);
		when(searchResult.getHits()).thenReturn(hitList);
		when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
		when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
		when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(true);
		
		when(assetSubNode.getProperty(DamAssetConstants.CQ_TAGS)).thenReturn(tagProperty);
		when(tagProperty.isMultiple()).thenReturn(true);
		
		Value[] tagValues = {};
		when(tagProperty.getValues()).thenReturn(tagValues);
		
		//Execute Logic
		JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
				propertyConfigService, resourcePathConfigService);

		// verify logic
        assertNotNull(metaDataJSONArray);

	}
	
    @Test
    public void damAssetUtilsTestSingleTagCheck() throws Exception {

		//Mock method calls
		when(request.getResourceResolver()).thenReturn(resourceResolver);
		when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
		when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
		when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
		mockQueryMap = mockQueryBuilder("image");
		when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
		when( query.getResult()).thenReturn(searchResult);
		when(searchResult.getHits()).thenReturn(hitList);
		when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
		when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
		when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(true);
		
		when(assetSubNode.getProperty(DamAssetConstants.CQ_TAGS)).thenReturn(tagProperty);
		when(tagProperty.isMultiple()).thenReturn(false);
		
		String sampleText = "Sample Tags";
		when(tagProperty.getString()).thenReturn(sampleText);
		
		//Execute Logic
		JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
				propertyConfigService, resourcePathConfigService);

		// verify logic
        assertNotNull(metaDataJSONArray);

	}

@Test
public void damAssetUtilsTestEmptyTagCheck() throws Exception {

	//Mock method calls
	when(request.getResourceResolver()).thenReturn(resourceResolver);
	when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
	when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
	when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
	mockQueryMap = mockQueryBuilder("image");
	when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
	when( query.getResult()).thenReturn(searchResult);
	when(searchResult.getHits()).thenReturn(hitList);
	when(hitTest1.getNode()).thenReturn(node);
	when(node.getPath()).thenReturn(path);
	when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
	String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
	when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
	when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
	when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
	when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
	when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
	when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(false);
	when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(true);
	
	when(assetSubNode.getProperty(DamAssetConstants.CQ_TAGS)).thenReturn(tagProperty);
	when(tagProperty.isMultiple()).thenReturn(false);
	
	when(tagProperty.getString()).thenReturn("");
	when(tagPropertyJSONArray.length()).thenReturn(1);
	
	//Execute Logic
	JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
			propertyConfigService, resourcePathConfigService);

	// verify logic
    assertNotNull(metaDataJSONArray);

}

    @Test
    public void damAssetUtilsTestVideoValidFormatCheck() throws Exception {

    	//Mock method calls
    	when(request.getResourceResolver()).thenReturn(resourceResolver);
    	when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
    	when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
    	when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
    	mockQueryMap = mockQueryBuilder("image");
    	when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
    	when( query.getResult()).thenReturn(searchResult);
    	when(searchResult.getHits()).thenReturn(hitList);
    	when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
    	when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(true);
		
		String format = "video/mp4";
		when(assetSubNode.getProperty(DamAssetConstants.DC_FORMAT)).thenReturn(property);
		when(property.getValue()).thenReturn(value);
		when(value.getString()).thenReturn(format);

		when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(false);
    	
    	//Execute Logic
    	JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
    			propertyConfigService, resourcePathConfigService);

    	// verify logic
        assertNotNull(metaDataJSONArray);

    }
 
    
    @Test
    public void damAssetUtilsTestVideoInvalidFormatCheck() throws Exception {

    	//Mock method calls
    	when(request.getResourceResolver()).thenReturn(resourceResolver);
    	when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
    	when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
    	when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
    	mockQueryMap = mockQueryBuilder("image");
    	when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
    	when( query.getResult()).thenReturn(searchResult);
    	when(searchResult.getHits()).thenReturn(hitList);
    	when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
    	when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(true);
		
		String format = "vid/mp4";
		when(assetSubNode.getProperty(DamAssetConstants.DC_FORMAT)).thenReturn(property);
		when(property.getValue()).thenReturn(value);
		when(value.getString()).thenReturn(format);

		when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(false);
    	
    	//Execute Logic
    	JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
    			propertyConfigService, resourcePathConfigService);

    	// verify logic
        assertNotNull(metaDataJSONArray);

    }
@Test
public void damAssetUtilsTestBrightcoveVideoCheck() throws Exception {

	//Mock method calls
	when(request.getResourceResolver()).thenReturn(resourceResolver);
	when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
	when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
	when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
	mockQueryMap = mockQueryBuilder(DamAssetConstants.MIME_TYPE_VIDEO);
	when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
	when( query.getResult()).thenReturn(searchResult);
	when(searchResult.getHits()).thenReturn(hitList);
	when(hitTest1.getNode()).thenReturn(node);
	when(node.getPath()).thenReturn(path);
	when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
	String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
	when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
	when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
	when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
	when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
	when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
	when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(true);
	
	String format = "video/mp4";
	when(assetSubNode.getProperty(DamAssetConstants.DC_FORMAT)).thenReturn(property);
	when(property.getValue()).thenReturn(value);
	when(value.getString()).thenReturn(format);

	when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(false);
	when(assetSubNode.hasProperty(DamAssetConstants.BRIGHTCOVE_VIDEO)).thenReturn(true);
	
	String brightcoveId = "testBrightCoveId";
	when(assetSubNode.getProperty(DamAssetConstants.BRIGHTCOVE_VIDEO)).thenReturn(videoProperty);
	when(videoProperty.getValue()).thenReturn(value2);
	when(value2.getString()).thenReturn(brightcoveId);
	
	//Execute Logic
	JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
			propertyConfigService, resourcePathConfigService);

	// verify logic
    assertNotNull(metaDataJSONArray);

}

   	@Test
    public void damAssetUtilsTestGetPlayerIDFromDataPageLoginExceptionCheck() throws RepositoryException, LoginException {

    	//Mock method calls
    	when(request.getResourceResolver()).thenReturn(resourceResolver);
    	when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
    	when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
    	when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
    	mockQueryMap = mockQueryBuilder("image");
    	when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
    	when( query.getResult()).thenReturn(searchResult);
    	when(searchResult.getHits()).thenReturn(hitList);
    	when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
    	when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(true);
    	
    	String format = "video/mp4";
    	when(assetSubNode.getProperty(DamAssetConstants.DC_FORMAT)).thenReturn(property);
    	when(property.getValue()).thenReturn(value);
    	when(value.getString()).thenReturn(format);

    	when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.BRIGHTCOVE_VIDEO)).thenReturn(false);

    	LoginException loginException = new LoginException("Login Exception");
    	when(CommonUtils.getDataPath( resourcePathConfigService, mockMagazineName(path))).thenReturn(path);
    	when(propertyConfigService.getDataConfigProperty(DamAssetConstants.JCR_BRIGHTCOVE_EMBEDDED_PLAYER_ID, path)).thenThrow(loginException);

    	//Execute Logic
    	DamAssetUtils.getAssetsResponse(request, queryBuilder, propertyConfigService, resourcePathConfigService);

        verify( logger).error( "@DamAssetUtils.getPlayerIDFromDataPage: Exception while fetching the data : ", loginException );

	}

   	@Test
    public void damAssetUtilsTestGetPlayerIDJSONExceptionCheck() throws Exception {

    	//Mock method calls
    	when(request.getResourceResolver()).thenReturn(resourceResolver);
    	when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
    	when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
    	when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
    	mockQueryMap = mockQueryBuilder("image");
    	when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
    	when( query.getResult()).thenReturn(searchResult);
    	when(searchResult.getHits()).thenReturn(hitList);
    	when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
    	when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(true);
    	
    	whenNew(JSONObject.class).withNoArguments().thenReturn(metaDataJSONObject,playerIDJsonObject);
    	String format = "video/mp4";
    	when(assetSubNode.getProperty(DamAssetConstants.DC_FORMAT)).thenReturn(property);
    	when(property.getValue()).thenReturn(value);
    	when(value.getString()).thenReturn(format);
    	when(assetNode.getProperty(DamAssetConstants.DC_FORMAT)).thenReturn(property);
    	when(metaDataJSONObject.getString(DamAssetConstants.FILE_FORMAT)).thenReturn(format);
    	when(metaDataJSONObject.get(DamAssetConstants.PATH)).thenReturn(DamAssetConstants.URL);

    	when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.BRIGHTCOVE_VIDEO)).thenReturn(false);

    	String playerId = "testPlayerId";
    	when(CommonUtils.getDataPath( resourcePathConfigService, mockMagazineName(path))).thenReturn(path);
    	when(propertyConfigService.getDataConfigProperty(DamAssetConstants.JCR_BRIGHTCOVE_EMBEDDED_PLAYER_ID, path)).thenReturn(playerId);

    	JSONException jsonException = new JSONException("JSON Exception");
    	
    	when(playerIDJsonObject.put(DamAssetConstants.BRIGHTCOVE_EMBEDDED_PLAYER_ID, playerId)).thenThrow(jsonException);

    	//Execute Logic
    	DamAssetUtils.getAssetsResponse(request, queryBuilder, propertyConfigService, resourcePathConfigService);

        verify( logger).error( "@DamAssetUtils.getPlayerID: Exception while fetching the data : ", jsonException );

	}

   	@Test
    public void damAssetUtilsTestGetPlayerIDEmbeddedCheck() throws RepositoryException, JSONException, LoginException {

    	//Mock method calls
    	when(request.getResourceResolver()).thenReturn(resourceResolver);
    	when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
    	when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
    	when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
    	mockQueryMap = mockQueryBuilder("image");
    	when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
    	when( query.getResult()).thenReturn(searchResult);
    	when(searchResult.getHits()).thenReturn(hitList);
    	when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
    	when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(true);
    	
    	String format = "video/mp4";
    	when(assetSubNode.getProperty(DamAssetConstants.DC_FORMAT)).thenReturn(property);
    	when(property.getValue()).thenReturn(value);
    	when(value.getString()).thenReturn(format);

    	when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.BRIGHTCOVE_VIDEO)).thenReturn(false);

    	String playerId = "testPlayerId";
    	when(CommonUtils.getDataPath( resourcePathConfigService, mockMagazineName(path))).thenReturn(path);
    	when(propertyConfigService.getDataConfigProperty(DamAssetConstants.JCR_BRIGHTCOVE_EMBEDDED_PLAYER_ID, path)).thenReturn(playerId);
    	
    	//Execute Logic
    	JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, propertyConfigService, resourcePathConfigService);
    	// verify logic
        assertNotNull(metaDataJSONArray);

	}
   	@Test
    public void damAssetUtilsTestGetPlayerIDNonEmbeddedCheck() throws RepositoryException, JSONException, LoginException {

    	//Mock method calls
    	when(request.getResourceResolver()).thenReturn(resourceResolver);
    	when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
    	when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
    	when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
    	mockQueryMap = mockQueryBuilder("image");
    	when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
    	when( query.getResult()).thenReturn(searchResult);
    	when(searchResult.getHits()).thenReturn(hitList);
    	when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
    	when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(true);
    	
    	String format = "video/mp4";
    	when(assetSubNode.getProperty(DamAssetConstants.DC_FORMAT)).thenReturn(property);
    	when(property.getValue()).thenReturn(value);
    	when(value.getString()).thenReturn(format);

    	when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.BRIGHTCOVE_VIDEO)).thenReturn(false);

    	String playerId = "testPlayerId";
    	when(CommonUtils.getDataPath( resourcePathConfigService, mockMagazineName(path))).thenReturn(path);
    	when(propertyConfigService.getDataConfigProperty(DamAssetConstants.JCR_BRIGHTCOVE_NON_EMBEDDED_PLAYER_ID, path)).thenReturn(playerId);
    	
    	//Execute Logic
    	JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, propertyConfigService, resourcePathConfigService);
    	// verify logic
        assertNotNull(metaDataJSONArray);

	}
   	@Test
    public void damAssetUtilsTestGetPlayerIDNonEmbeddedEmptyPlayerIdCheck() throws RepositoryException, JSONException, LoginException {

    	//Mock method calls
    	when(request.getResourceResolver()).thenReturn(resourceResolver);
    	when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
    	when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
    	when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
    	mockQueryMap = mockQueryBuilder("image");
    	when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
    	when( query.getResult()).thenReturn(searchResult);
    	when(searchResult.getHits()).thenReturn(hitList);
    	when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
    	when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(true);
    	
    	String format = "video/mp4";
    	when(assetSubNode.getProperty(DamAssetConstants.DC_FORMAT)).thenReturn(property);
    	when(property.getValue()).thenReturn(value);
    	when(value.getString()).thenReturn(format);

    	when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.BRIGHTCOVE_VIDEO)).thenReturn(false);

    	String playerId = "";
    	when(CommonUtils.getDataPath( resourcePathConfigService, mockMagazineName(path))).thenReturn(path);
    	when(propertyConfigService.getDataConfigProperty(DamAssetConstants.JCR_BRIGHTCOVE_NON_EMBEDDED_PLAYER_ID, path)).thenReturn(playerId);
    	
    	//Execute Logic
    	JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, propertyConfigService, resourcePathConfigService);
    	// verify logic
        assertNotNull(metaDataJSONArray);

	}

    @Test
    public void damAssetUtilsTestVideoIdCheck() throws Exception {

    	//Mock method calls
    	when(request.getResourceResolver()).thenReturn(resourceResolver);
    	when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
    	when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
    	when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
    	mockQueryMap = mockQueryBuilder("image");
    	when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
    	when( query.getResult()).thenReturn(searchResult);
    	when(searchResult.getHits()).thenReturn(hitList);
    	when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
    	when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(true);
    	
    	String format = "video/mp4";
    	when(assetSubNode.getProperty(DamAssetConstants.DC_FORMAT)).thenReturn(property);
    	when(property.getValue()).thenReturn(value);
    	when(value.getString()).thenReturn(format);

    	when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.BRIGHTCOVE_VIDEO)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.VIDEO_ID)).thenReturn(true);
		
		String videoId = "";
		when(assetSubNode.getProperty(DamAssetConstants.VIDEO_ID)).thenReturn(videoProperty);
		when(videoProperty.getValue()).thenReturn(value2);
		when(value2.getString()).thenReturn(videoId);
    	
    	//Execute Logic
    	JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
    			propertyConfigService, resourcePathConfigService);

    	// verify logic
        assertNotNull(metaDataJSONArray);

    }
    
    @Test
    public void damAssetUtilsTestNoVideoIdCheck() throws Exception {

    	//Mock method calls
    	when(request.getResourceResolver()).thenReturn(resourceResolver);
    	when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
    	when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
    	when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
    	mockQueryMap = mockQueryBuilder("image");
    	when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
    	when( query.getResult()).thenReturn(searchResult);
    	when(searchResult.getHits()).thenReturn(hitList);
    	when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
    	when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(true);
    	
    	String format = "video/mp4";
    	when(assetSubNode.getProperty(DamAssetConstants.DC_FORMAT)).thenReturn(property);
    	when(property.getValue()).thenReturn(value);
    	when(value.getString()).thenReturn(format);

    	when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.BRIGHTCOVE_VIDEO)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.VIDEO_ID)).thenReturn(true);
		
		String videoId = "";
		when(assetSubNode.getProperty(DamAssetConstants.VIDEO_ID)).thenReturn(videoProperty);
		when(videoProperty.getValue()).thenReturn(value2);
		when(value2.getString()).thenReturn(videoId);
    	
    	//Execute Logic
    	JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
    			propertyConfigService, resourcePathConfigService);

    	// verify logic
        assertNotNull(metaDataJSONArray);

    }
    
    @Test
    public void damAssetUtilsTestNoVideoPropertiesCheck() throws Exception {

    	//Mock method calls
    	when(request.getResourceResolver()).thenReturn(resourceResolver);
    	when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
    	when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
    	when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
    	mockQueryMap = mockQueryBuilder("image");
    	when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
    	when( query.getResult()).thenReturn(searchResult);
    	when(searchResult.getHits()).thenReturn(hitList);
    	when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
    	when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(true);
    	
    	String format = "video/mp4";
    	when(assetSubNode.getProperty(DamAssetConstants.DC_FORMAT)).thenReturn(property);
    	when(property.getValue()).thenReturn(value);
    	when(value.getString()).thenReturn(format);

    	when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.BRIGHTCOVE_VIDEO)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.VIDEO_ID)).thenReturn(false);
		
    	//Execute Logic
    	JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
    			propertyConfigService, resourcePathConfigService);

    	// verify logic
        assertNotNull(metaDataJSONArray);

    }
    
    @Test
    public void damAssetUtilsTestThumbnailImageCheck() throws Exception {

    	//Mock method calls
    	when(request.getResourceResolver()).thenReturn(resourceResolver);
    	when(resourceResolver.adaptTo(Session.class)).thenReturn(currentSession);
    	when(request.getParameter(DamAssetConstants.MIME_TYPE_PARAM)).thenReturn(mimeType);
    	when(request.getParameter(DamAssetConstants.PATH_PARAM)).thenReturn(path);
    	mockQueryMap = mockQueryBuilder("image");
    	when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ),currentSession ) ).thenReturn( query );
    	when( query.getResult()).thenReturn(searchResult);
    	when(searchResult.getHits()).thenReturn(hitList);
    	when(hitTest1.getNode()).thenReturn(node);
		when(node.getPath()).thenReturn(path);
		when(resourceResolver.adaptTo(Externalizer.class)).thenReturn(externalizer);
		String magazineName = mockMagazineName(request.getParameter(DamAssetConstants.PATH_PARAM));
		when(externalizer.externalLink(resourceResolver, magazineName,path)).thenReturn(DamAssetConstants.URL);
		when(node.getNode(DamAssetConstants.JCR_CONTENT)).thenReturn(assetNode);
    	when(assetNode.getNode(DamAssetConstants.METADATA)).thenReturn(assetSubNode);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_TITLE)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_DESCRIPTION)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.DC_FORMAT)).thenReturn(true);
    	
    	String format = "image/jpg";
    	when(assetSubNode.getProperty(DamAssetConstants.DC_FORMAT)).thenReturn(property);
    	when(property.getValue()).thenReturn(value);
    	when(value.getString()).thenReturn(format);

    	when(assetSubNode.hasProperty(DamAssetConstants.CQ_TAGS)).thenReturn(false);
    	when(assetSubNode.hasProperty(DamAssetConstants.BRIGHTCOVE_VIDEO)).thenReturn(false);
		when(assetSubNode.hasProperty(DamAssetConstants.VIDEO_ID)).thenReturn(true);
		
		String videoId = "";
		when(assetSubNode.getProperty(DamAssetConstants.VIDEO_ID)).thenReturn(videoProperty);
		when(videoProperty.getValue()).thenReturn(value2);
		when(value2.getString()).thenReturn(videoId);
    	
    	//Execute Logic
    	JSONArray metaDataJSONArray = DamAssetUtils.getAssetsResponse(request, queryBuilder, 
    			propertyConfigService, resourcePathConfigService);

    	// verify logic
        assertNotNull(metaDataJSONArray);

    }

	private String mockMagazineName(String pathParam) {
		if(pathParam == null) {
			return DamAssetConstants.EMPTY_STRING;
		}
		return pathParam.split("/")[4];
	}

	private Map<String, String> mockQueryBuilder(String mimeType2) {

		Map<String, String> queryMap = new HashMap<String, String>();
		queryMap.put(DamAssetConstants.QUERYMAP_KEY_TYPE, DamAssetConstants.QUERYMAP_VALUE_TYPE);
		queryMap.put(DamAssetConstants.QUERYMAP_KEY_PATH, path);
		queryMap.put(DamAssetConstants.QUERYMAP_KEY_ORDERBYSORT, DamAssetConstants.QUERYMAP_VALUE_ORDERBYSORT);
		queryMap.put(DamAssetConstants.QUERYMAP_KEY_PROPERTYOPERATION,
				DamAssetConstants.QUERYMAP_VALUE_PROPERTYOPERATION);
		queryMap.put(DamAssetConstants.QUERYMAP_KEY_PROPERTY, DamAssetConstants.QUERYMAP_VALUE_PROPERTY);
		queryMap.put(DamAssetConstants.QUERYMAP_KEY_PLimit, DamAssetConstants.QUERYMAP_VALUE_PLimit);

		if (mimeType.equalsIgnoreCase(DamAssetConstants.MIME_TYPE_AUDIO)) {
			queryMap.put(DamAssetConstants.QUERYMAP_PROPERTY_VALUE, DamAssetConstants.QUERYMAP_AUDIO_QUERY_STRING);
		} else if (mimeType.equalsIgnoreCase(DamAssetConstants.MIME_TYPE_VIDEO)) {
			queryMap.put(DamAssetConstants.QUERYMAP_PROPERTY_VALUE, DamAssetConstants.QUERYMAP_VIDEO_QUERY_STRING);
		} else if (mimeType.equalsIgnoreCase(DamAssetConstants.MIME_TYPE_APPLICATION)) {
			queryMap.put(DamAssetConstants.QUERYMAP_PROPERTY_VALUE,
					DamAssetConstants.QUERYMAP_APPLICATION_QUERY_STRING);
		} else if (mimeType.equalsIgnoreCase(DamAssetConstants.MIME_TYPE_IMAGE)) {
			queryMap.put(DamAssetConstants.QUERYMAP_PROPERTY_VALUE, DamAssetConstants.QUERYMAP_IMAGE_QUERY_STRING);
		}
		return queryMap;
	}
}
