package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.models.PromoteMagazine;
import com.scholastic.classmags.migration.services.PromotMagazineDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * JUnit for PromotoMagazinesUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { PromoteMagazinesUse.class } )
public class PromoteMagazinesUseTest extends BaseComponentTest {
    private static final String CURRENT_PATH = "/content/classroom_magazines/scienceworld/issues/_2015_16/092115/high-flier/jcr:content";

    private PromoteMagazinesUse promoteMagazinesUse;
    private PromoteMagazine promoteMagazines;
    private PromotMagazineDataService promoteMagazineDataService;

    private List< ValueMap > promoteMagazinesMapList;
    private ValueMap promoteMagazinesMap;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        promoteMagazinesUse = Whitebox.newInstance( PromoteMagazinesUse.class );
        stubCommon( promoteMagazinesUse );

        promoteMagazines = mock( PromoteMagazine.class );
        promoteMagazineDataService = mock( PromotMagazineDataService.class );

        when( request.getResource() ).thenReturn( resource );
        when( request.adaptTo( PromoteMagazine.class ) ).thenReturn( promoteMagazines );
        when( resource.getPath() ).thenReturn( CURRENT_PATH );
        when( slingScriptHelper.getService( PromotMagazineDataService.class ) )
                .thenReturn( promoteMagazineDataService );
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final PromoteMagazinesUse promoteMagazinesUse = new PromoteMagazinesUse();

        // verify logic
        assertNotNull( promoteMagazinesUse );
    }

    @Test
    public void testPromoteMagazine() throws Exception {
        // setup logic( test-specific )
        createPromoteMagazinesMapList();
        when( promoteMagazineDataService.fetchMagazineData( CURRENT_PATH ) ).thenReturn( promoteMagazinesMapList );
        when( promoteMagazinesMap.get( ClassMagsMigrationConstants.MAGAZINE_NAME, StringUtils.EMPTY ) )
                .thenReturn( "Sample Magazine Name" );
        when( promoteMagazinesMap.get( ClassMagsMigrationConstants.MAGAZINE_DESCRIPTION, StringUtils.EMPTY ) )
                .thenReturn( "Sample Magazine Description" );
        when( promoteMagazinesMap.get( ClassMagsMigrationConstants.MAGAZINE_PATH, StringUtils.EMPTY ) )
                .thenReturn( "/content/scholastic/science-world/2012-15/Going_Batty" );
        when( promoteMagazinesMap.get( ClassMagsMigrationConstants.IMAGE_PATH, StringUtils.EMPTY ) )
        .thenReturn( "/content/dam/scholastic/science-world/scienceworld.png" );

        // execute logic
        Whitebox.invokeMethod( promoteMagazinesUse, "activate" );

        // verify logic
        assertEquals( promoteMagazinesUse.getMagsDataList().get( 0 ).getMagazineName(), "Sample Magazine Name" );
        assertEquals( promoteMagazinesUse.getMagsDataList().get( 0 ).getMagazineDescription(),
                "Sample Magazine Description" );
        assertEquals( promoteMagazinesUse.getMagsDataList().get( 0 ).getMagazinePath(),
                "/content/scholastic/science-world/2012-15/Going_Batty" );
        assertEquals( promoteMagazinesUse.getMagsDataList().get( 0 ).getImagePath(),
                "/content/dam/scholastic/science-world/scienceworld.png" );
    }

    @Test
    public void testWhenPromoteMagazineDataServiceIsNull() throws Exception {
        // setup logic( test-specific )
        when( slingScriptHelper.getService( PromotMagazineDataService.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( promoteMagazinesUse, "activate" );

        // verify logic
        assertNull( promoteMagazinesUse.getMagsDataList() );
    }

    @Test
    public void testWhenSlingScriptHelperIsNull() throws Exception {
        // setup logic ( test-specific )
        stub( PowerMockito.method( promoteMagazinesUse.getClass(), "getSlingScriptHelper" ) ).toReturn( null );

        // execute logic
        Whitebox.invokeMethod( promoteMagazinesUse, "activate" );

        // verify logic
        assertNull( promoteMagazinesUse.getMagsDataList() );
    }

    public void createPromoteMagazinesMapList() {
        promoteMagazinesMapList = new ArrayList<>();

        promoteMagazinesMap = mock( ValueMap.class );

        promoteMagazinesMapList.add( promoteMagazinesMap );
    }

}
