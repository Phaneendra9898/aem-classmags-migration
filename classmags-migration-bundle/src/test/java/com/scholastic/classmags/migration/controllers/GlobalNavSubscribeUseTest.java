package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.script.Bindings;

import org.apache.sling.api.SlingHttpServletRequest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.scholastic.classmags.migration.models.GlobalNavSubscribe;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * JUnit for GlobalNavSubscribeUse.
 */
@RunWith( MockitoJUnitRunner.class )
public class GlobalNavSubscribeUseTest {

    /** The global nav subscribe use. */
    private GlobalNavSubscribeUse globalNavSubscribeUse;

    /** The global nav subscribe. */
    private GlobalNavSubscribe globalNavSubscribe;

    /** The bindings. */
    @Mock
    private Bindings bindings;

    /** The request. */
    @Mock
    private SlingHttpServletRequest request;

    /** The mock object. */
    @Mock
    private Object mockObject;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        globalNavSubscribeUse = new GlobalNavSubscribeUse();
        globalNavSubscribe = mock( GlobalNavSubscribe.class );
    }

    /**
     * Test activate.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testActivate() throws Exception {
        when( bindings.get( "request" ) ).thenReturn( request );
        when( request.adaptTo( GlobalNavSubscribe.class ) ).thenReturn( globalNavSubscribe );
        when( request.getAttribute( ClassMagsMigrationConstants.ROLE ) ).thenReturn( mockObject );
        when( mockObject.toString() ).thenReturn( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS );
        globalNavSubscribeUse.init( bindings );
        globalNavSubscribeUse.activate();
        assertEquals( globalNavSubscribe, globalNavSubscribeUse.getGlobalNavSubscribe() );
    }

    /**
     * Test activate with no request.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testActivateWithNoRequest() throws Exception {
        when( bindings.get( "request" ) ).thenReturn( null );
        globalNavSubscribeUse.init( bindings );
        globalNavSubscribeUse.activate();
        globalNavSubscribeUse.setGlobalNavSubscribe( globalNavSubscribe );
        assertEquals( globalNavSubscribe, globalNavSubscribeUse.getGlobalNavSubscribe() );
    }

    /**
     * Test is user logged out flag.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testIsUserLoggedOutFlag() throws Exception {
        when( bindings.get( "request" ) ).thenReturn( request );
        when( request.adaptTo( GlobalNavSubscribe.class ) ).thenReturn( globalNavSubscribe );
        when( request.getAttribute( ClassMagsMigrationConstants.ROLE ) ).thenReturn( mockObject );
        when( mockObject.toString() ).thenReturn( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS );
        globalNavSubscribeUse.init( bindings );
        globalNavSubscribeUse.activate();
        assertEquals( true, globalNavSubscribeUse.isUserLoggedOutFlag() );
    }
}
