package com.scholastic.classmags.migration.controllers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import javax.script.Bindings;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.powermock.api.mockito.PowerMockito;

import com.adobe.cq.sightly.SightlyWCMMode;
import com.adobe.cq.sightly.WCMUsePojo;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.components.Component;
import com.day.cq.wcm.api.designer.Design;
import com.day.cq.wcm.api.designer.Designer;
import com.day.cq.wcm.api.designer.Style;

public class BaseComponentTest {
    protected Bindings bindings;

    protected Component component;
    protected Design currentDesign;
    protected Style currentStyle;
    protected Page currentPage;
    protected Designer designer;
    protected ValueMap inheritedPageProperties;
    protected PageManager pageManager;
    protected ValueMap pageProperties;
    protected ValueMap properties;
    protected SlingHttpServletRequest request;
    protected Resource resource;
    protected Page resourcePage;
    protected ResourceResolver resourceResolver;
    protected SlingHttpServletResponse response;
    protected SlingScriptHelper slingScriptHelper;
    protected SightlyWCMMode wcmMode;

    public void stubCommon( Object componentUnderTest ) {
        component = mock( Component.class );
        currentDesign = mock( Design.class );
        currentPage = mock( Page.class );
        currentStyle = mock( Style.class );
        designer = mock( Designer.class );
        inheritedPageProperties = mock( ValueMap.class );
        pageManager = mock( PageManager.class );
        pageProperties = mock( ValueMap.class );
        properties = mock( ValueMap.class );
        request = mock( SlingHttpServletRequest.class );
        resource = mock( Resource.class );
        resourcePage = mock( Page.class );
        resourceResolver = mock( ResourceResolver.class );
        response = mock( SlingHttpServletResponse.class );
        slingScriptHelper = mock( SlingScriptHelper.class );
        wcmMode = mock( SightlyWCMMode.class );

        stub( PowerMockito.method( componentUnderTest.getClass(), "getComponent" ) ).toReturn( component );
        stub( PowerMockito.method( componentUnderTest.getClass(), "getCurrentDesign" ) ).toReturn( currentDesign );
        stub( PowerMockito.method( componentUnderTest.getClass(), "getCurrentPage" ) ).toReturn( currentPage );
        stub( PowerMockito.method( componentUnderTest.getClass(), "getCurrentStyle" ) ).toReturn( currentStyle );
        stub( PowerMockito.method( componentUnderTest.getClass(), "getDesigner" ) ).toReturn( designer );
        stub( PowerMockito.method( componentUnderTest.getClass(), "getInheritedProperties" ) )
                .toReturn( inheritedPageProperties );
        stub( PowerMockito.method( componentUnderTest.getClass(), "getPageManager" ) ).toReturn( pageManager );
        stub( PowerMockito.method( componentUnderTest.getClass(), "getPageProperties" ) ).toReturn( pageProperties );
        stub( PowerMockito.method( componentUnderTest.getClass(), "getProperties" ) ).toReturn( properties );
        stub( PowerMockito.method( componentUnderTest.getClass(), "getRequest" ) ).toReturn( request );
        stub( PowerMockito.method( componentUnderTest.getClass(), "getResource" ) ).toReturn( resource );
        stub( PowerMockito.method( componentUnderTest.getClass(), "getResourcePage" ) ).toReturn( resourcePage );
        stub( PowerMockito.method( componentUnderTest.getClass(), "getResourceResolver" ) )
                .toReturn( resourceResolver );
        stub( PowerMockito.method( componentUnderTest.getClass(), "getResponse" ) ).toReturn( response );
        stub( PowerMockito.method( componentUnderTest.getClass(), "getSlingScriptHelper" ) )
                .toReturn( slingScriptHelper );
        stub( PowerMockito.method( componentUnderTest.getClass(), "getWcmMode" ) ).toReturn( wcmMode );
    }

    public void initBinidings( WCMUsePojo wcmUsePojo, Bindings bindings ) {
        component = mock( Component.class );
        currentDesign = mock( Design.class );
        currentPage = mock( Page.class );
        currentStyle = mock( Style.class );
        designer = mock( Designer.class );
        inheritedPageProperties = mock( ValueMap.class );
        pageManager = mock( PageManager.class );
        pageProperties = mock( ValueMap.class );
        properties = mock( ValueMap.class );
        request = mock( SlingHttpServletRequest.class );
        resource = mock( Resource.class );
        resourcePage = mock( Page.class );
        resourceResolver = mock( ResourceResolver.class );
        response = mock( SlingHttpServletResponse.class );
        slingScriptHelper = mock( SlingScriptHelper.class );
        wcmMode = mock( SightlyWCMMode.class );

        bindings.put( "component", component );
        bindings.put( "currentDesign", currentDesign );
        bindings.put( "currentPage", currentPage );
        bindings.put( "currentStyle", currentStyle );
        bindings.put( "designer", designer );
        bindings.put( "inheritedPageProperties", inheritedPageProperties );
        bindings.put( "pageManager", pageManager );
        bindings.put( "pageProperties", pageProperties );
        bindings.put( "properties", properties );
        bindings.put( "request", request );
        bindings.put( "resource", resource );
        bindings.put( "resourcePage", resourcePage );
        bindings.put( "response", response );
        bindings.put( "sling", slingScriptHelper );
        bindings.put( "wcmmode", wcmMode );

        when( request.getResourceResolver() ).thenReturn( resourceResolver );
    }

}
