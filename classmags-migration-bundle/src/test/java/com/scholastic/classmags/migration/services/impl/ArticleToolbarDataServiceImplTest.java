package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Session;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * JUnit for ArticleToolbarDataServiceImpl.
 */
@RunWith( MockitoJUnitRunner.class )
public class ArticleToolbarDataServiceImplTest {

    /** The article toolbar data service. */
    private ArticleToolbarDataServiceImpl articleToolbarDataService;

    /** The Constant PATH. */
    private static final String PATH = "/apps/scholastic/classroom-magazines-migration/templates/issue-page";

    /** The Constant TEST_DATA. */
    private static final String TEST_DATA = "TestData";

    /** The Constant INPUT_KEY. */
    private static final String INPUT_KEY = "linkToEReader";

    /** The resource resolver factory. */
    @Mock
    private ResourceResolverFactory resourceResolverFactory;

    /** The resource resolver. */
    @Mock
    private ResourceResolver resourceResolver;

    /** The res. */
    @Mock
    private Resource res;

    /** The properties. */
    @Mock
    private ValueMap properties;

    /** The session. */
    @Mock
    private Session session;

    /** The query builder. */
    @Mock
    private QueryBuilder queryBuilder;

    /** The mock query map. */
    @Mock
    private Map< String, String > mockQueryMap;

    /** The query. */
    @Mock
    private Query query;

    /** The search result. */
    @Mock
    private SearchResult searchResult;

    /** The mock article lexile levels. */
    @Mock
    private List< Hit > mockArticleLexileLevels;

    /** The hit. */
    @Mock
    private Hit hit;

    /** The page. */
    @Mock
    private Page page;

    /**
     * Sets the up.
     *
     * @throws LoginException
     *             the login exception
     */
    @Before
    public void setUp() throws LoginException {
        articleToolbarDataService = new ArticleToolbarDataServiceImpl();
    }

    /**
     * Test fetch parent issue page path.
     *
     * @throws LoginException
     *             the login exception
     * @throws ClassmagsMigrationBaseException
     */
    @Test
    public void testFetchParentIssuePagePath() throws LoginException, ClassmagsMigrationBaseException {

        String testString;

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, "readservice" );
        mockQueryMap = new HashMap< String, String >();
        mockQueryMap.put( ClassMagsMigrationConstants.QUERY_PATH, PATH );
        mockQueryMap.put( ClassMagsMigrationConstants.RESULTS_LIMIT, ClassMagsMigrationConstants.QUERY_LOWER_BOUND );
        mockQueryMap.put( ClassMagsMigrationConstants.QUERY_PARAM_1, ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG );
        mockQueryMap.put( ClassMagsMigrationConstants.QUERY_PARAM_1_VALUE, PATH );
        mockArticleLexileLevels = new ArrayList< >();
        mockArticleLexileLevels.add( hit );

        articleToolbarDataService.setResourceResolverFactory( resourceResolverFactory );
        when( page.getParent() ).thenReturn( page );
        when( page.getPath() ).thenReturn( PATH );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( Session.class ) ).thenReturn( session );
        articleToolbarDataService.setQueryBuilder( queryBuilder );
        when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ), session ) ).thenReturn( query );
        when( query.getResult() ).thenReturn( searchResult );
        when( searchResult.getHits() ).thenReturn( mockArticleLexileLevels );

        testString = articleToolbarDataService.fetchParentIssuePagePath( page );
        assertEquals( PATH, testString );
    }

    /**
     * Test fetch parent issue page path with no parent.
     *
     * @throws LoginException
     *             the login exception
     * @throws ClassmagsMigrationBaseException
     */
    @Test
    public void testFetchParentIssuePagePathWithNoParent() throws LoginException, ClassmagsMigrationBaseException {

        String testString;

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, "readservice" );
        mockQueryMap = new HashMap< String, String >();
        mockQueryMap.put( ClassMagsMigrationConstants.QUERY_PATH, PATH );
        mockQueryMap.put( ClassMagsMigrationConstants.RESULTS_LIMIT, ClassMagsMigrationConstants.QUERY_LOWER_BOUND );
        mockQueryMap.put( ClassMagsMigrationConstants.QUERY_PARAM_1, ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG );
        mockQueryMap.put( ClassMagsMigrationConstants.QUERY_PARAM_1_VALUE, PATH );
        mockArticleLexileLevels = new ArrayList< >();

        articleToolbarDataService.setResourceResolverFactory( resourceResolverFactory );
        when( page.getParent() ).thenReturn( page );
        when( page.getPath() ).thenReturn( PATH );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( Session.class ) ).thenReturn( session );
        articleToolbarDataService.setQueryBuilder( queryBuilder );
        when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ), session ) ).thenReturn( query );
        when( query.getResult() ).thenReturn( searchResult );
        when( searchResult.getHits() ).thenReturn( mockArticleLexileLevels );

        testString = articleToolbarDataService.fetchParentIssuePagePath( page );
        assertEquals( null, testString );
    }

    /**
     * Test fetchdigital issue link.
     *
     * @throws LoginException
     *             the login exception
     * @throws ClassmagsMigrationBaseException
     */
    @Test
    public void testFetchdigitalIssueLink() throws LoginException, ClassmagsMigrationBaseException {

        String testString;

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, "readservice" );

        articleToolbarDataService.setResourceResolverFactory( resourceResolverFactory );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.getResource( PATH ) ).thenReturn( res );
        when( res.adaptTo( ValueMap.class ) ).thenReturn( properties );
        when( properties.get( INPUT_KEY, String.class ) ).thenReturn( TEST_DATA );

        testString = articleToolbarDataService.fetchdigitalIssueLink( PATH );
        assertEquals( TEST_DATA, testString );
    }

    /**
     * Test fetchdigital issue link with no resource.
     *
     * @throws LoginException
     *             the login exception
     * @throws ClassmagsMigrationBaseException
     */
    @Test
    public void testFetchdigitalIssueLinkWithNoResource() throws LoginException, ClassmagsMigrationBaseException {

        String testString;
        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, "readservice" );

        articleToolbarDataService.setResourceResolverFactory( resourceResolverFactory );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.getResource( PATH ) ).thenReturn( null );

        testString = articleToolbarDataService.fetchdigitalIssueLink( PATH );
        assertEquals( null, testString );
    }

    /**
     * Test fetchdigital issue link with no properties.
     *
     * @throws LoginException
     *             the login exception
     * @throws ClassmagsMigrationBaseException
     */
    @Test
    public void testFetchdigitalIssueLinkWithNoProperties() throws LoginException, ClassmagsMigrationBaseException {

        String testString;
        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, "readservice" );

        articleToolbarDataService.setResourceResolverFactory( resourceResolverFactory );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.getResource( PATH ) ).thenReturn( res );
        when( res.adaptTo( ValueMap.class ) ).thenReturn( null );

        testString = articleToolbarDataService.fetchdigitalIssueLink( PATH );
        assertEquals( null, testString );
    }
}
