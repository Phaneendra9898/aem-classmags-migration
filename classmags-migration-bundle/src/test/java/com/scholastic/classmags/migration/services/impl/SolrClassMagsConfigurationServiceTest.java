package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.scholastic.classmags.migration.utils.SolrSearchClassMagsConstants;

public class SolrClassMagsConfigurationServiceTest {

    SolrClassMagsConfigService service = new SolrClassMagsConfigService();

    Map< String, Object > config = new HashMap< String, Object >();

    @Before
    public void setup() {
        config.put( SolrSearchClassMagsConstants.PROTOCOL, SolrSearchClassMagsConstants.DEFAULT_PROTOCOL );
        config.put( SolrSearchClassMagsConstants.SERVER_NAME, SolrSearchClassMagsConstants.DEFAULT_SERVER_NAME );
        config.put( SolrSearchClassMagsConstants.SERVER_PORT, SolrSearchClassMagsConstants.DEFAULT_SERVER_PORT );
        config.put( SolrSearchClassMagsConstants.CONTEXT_PATH, SolrSearchClassMagsConstants.DEFAULT_CONTEXT_PATH );
        config.put( SolrSearchClassMagsConstants.PROXY_URL, SolrSearchClassMagsConstants.DEFAULT_PROXY_URL );
        config.put( SolrSearchClassMagsConstants.PROXY_ENABLED, SolrSearchClassMagsConstants.DEFAULT_PROXY_ENABLED );
        config.put( SolrSearchClassMagsConstants.AEM_SOLR_CORE, SolrSearchClassMagsConstants.DEFAULT_AEM_SOLR_CORE );
        service.activate( config );
    }

    @Test
    public void testGetCoreName() throws Exception {
        
        assertNotNull( service.getCoreName() );
        assertEquals( service.getCoreName(), SolrSearchClassMagsConstants.DEFAULT_AEM_SOLR_CORE );
    }

    @Test
    public void testGetProxyUrl() throws Exception {
        assertNotNull( service.getProxyUrl() );
        assertEquals( service.getProxyUrl(), SolrSearchClassMagsConstants.DEFAULT_PROXY_URL );
    }

    @Test
    public void testGetSolrClient() throws Exception {
        assertNotNull( service.getSolrClient() );
    }

    @Test
    public void testGetSolrClientForCore() throws Exception {
        assertNotNull( service.getSolrClient( SolrSearchClassMagsConstants.DEFAULT_AEM_SOLR_CORE ) );
    }

    /*@Test
    public void testGetSolrEndPoint() throws Exception {
        assertNotNull( service.getSolrEndPoint() );
    }

    @Test
    public void testGetSolrServerURI() throws Exception {
        assertNotNull( service.getSolrServerURI() );
    }

    @Test
    public void testGetSolrServerURIForCore() throws Exception {
        assertNotNull( service.getSolrServerURI( SolrSearchClassMagsConstants.DEFAULT_AEM_SOLR_CORE ) );
    }*/

}