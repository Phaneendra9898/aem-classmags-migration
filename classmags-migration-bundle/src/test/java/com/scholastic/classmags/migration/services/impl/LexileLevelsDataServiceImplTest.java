package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for LexileLevelsDataServiceImpl.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { LexileLevelsDataServiceImpl.class, CommonUtils.class } )
public class LexileLevelsDataServiceImplTest {

    /** The lexile levels data service impl. */
    private LexileLevelsDataServiceImpl lexileLevelsDataServiceImpl;

    /** The Constant READING_LEVEL_ONE_KEY. */
    private static final String READING_LEVEL_ONE_KEY = "1120L";

    /** The Constant READING_LEVEL_TWO_KEY. */
    private static final String READING_LEVEL_TWO_KEY = "640M";

    /** The Constant READING_LEVEL_ONE_VALUE. */
    private static final String READING_LEVEL_ONE_VALUE = "Reading Level - 1120L";

    /** The Constant READING_LEVEL_TWO_VALUE. */
    private static final String READING_LEVEL_TWO_VALUE = "Reading Level - 640M";

    /** The Constant DEFAULT_KEY. */
    private static final String DEFAULT_KEY = "default";

    /** The Constant DEFAULT_VALUE. */
    private static final String DEFAULT_VALUE = "Default";

    /** The Constant ARTICLE_CONFIGURATION_NODE. */
    private static final String ARTICLE_CONFIGURATION_NODE = "article-configuration";

    /** The Constant LEXILE_SETTINGS_NODE. */
    private static final String LEXILE_SETTINGS_NODE = "lexileSettings";

    private SlingHttpServletRequest request;

    private Page currentPage;

    private Resource jcrResource;
    private Resource articleConfigResource;
    private Resource lexileLevelResource;
    private List< ValueMap > lexileLevels;
    private ValueMap lexileLevelOne, lexileLevelTwo;

    /**
     * Initial Setup.
     */
    @Before
    public void setUp() {
        lexileLevelsDataServiceImpl = new LexileLevelsDataServiceImpl();
        currentPage = mock( Page.class );
        request = mock( SlingHttpServletRequest.class );
        jcrResource = mock( Resource.class );
        articleConfigResource = mock( Resource.class );
        lexileLevelResource = mock( Resource.class );
        lexileLevelOne = mock( ValueMap.class );
        lexileLevelTwo = mock( ValueMap.class );

        lexileLevels = new ArrayList< >();
        lexileLevels.add( lexileLevelOne );
        lexileLevels.add( lexileLevelTwo );

        PowerMockito.mockStatic( CommonUtils.class );

        when( currentPage.getContentResource() ).thenReturn( jcrResource );
        when( jcrResource.getChild( ARTICLE_CONFIGURATION_NODE ) ).thenReturn( articleConfigResource );
        when( articleConfigResource.getChild( LEXILE_SETTINGS_NODE ) ).thenReturn( lexileLevelResource );
        when( CommonUtils.fetchMultiFieldData( lexileLevelResource ) ).thenReturn( lexileLevels );
        when( lexileLevelOne.get( ClassMagsMigrationConstants.ARTICLE_LEXILE_LEVEL, String.class ) )
                .thenReturn( READING_LEVEL_ONE_KEY );
        when( lexileLevelTwo.get( ClassMagsMigrationConstants.ARTICLE_LEXILE_LEVEL, String.class ) )
                .thenReturn( READING_LEVEL_TWO_KEY );
    }

    /**
     * Test fetch lexile levels.
     */
    @Test
    public void testFetchLexileLevels() {

        // execute logic ( test )
        Map< String, String > readingLevelsMap = lexileLevelsDataServiceImpl.fetchLexileLevels( request, currentPage );

        // verify logic
        assertEquals( 2, readingLevelsMap.size() );
        assertEquals( READING_LEVEL_ONE_VALUE, readingLevelsMap.get( READING_LEVEL_ONE_KEY ) );
        assertEquals( READING_LEVEL_TWO_VALUE, readingLevelsMap.get( READING_LEVEL_TWO_KEY ) );
    }

    /**
     * Test fetch lexile levels when JCR resource is null.
     */
    @Test
    public void testFetchLexileLevelsWhenJcrResourceIsNull() {
        // setup logic ( test-specific )
        when( currentPage.getContentResource() ).thenReturn( null );

        // execute logic ( test )
        Map< String, String > readingLevelsMap = lexileLevelsDataServiceImpl.fetchLexileLevels( request, currentPage );

        // verify logic
        assertEquals( DEFAULT_VALUE, readingLevelsMap.get( DEFAULT_KEY ) );
    }

    /**
     * Test fetch lexile levels when article config resource is null.
     */
    @Test
    public void testFetchLexileLevelsWhenArticleConfigResourceIsNull() {
        // setup logic ( test-specific )
        when( jcrResource.getChild( ARTICLE_CONFIGURATION_NODE ) ).thenReturn( null );

        // execute logic ( test )
        Map< String, String > readingLevelsMap = lexileLevelsDataServiceImpl.fetchLexileLevels( request, currentPage );

        // verify logic
        assertEquals( DEFAULT_VALUE, readingLevelsMap.get( DEFAULT_KEY ) );
    }

    /**
     * Test fetch lexile levels when lexile level resource is null.
     */
    @Test
    public void testFetchLexileLevelsWhenLexileLevelResourceIsNull() {
        // setup logic ( test-specific )
        when( articleConfigResource.getChild( LEXILE_SETTINGS_NODE ) ).thenReturn( null );

        // execute logic ( test )
        Map< String, String > readingLevelsMap = lexileLevelsDataServiceImpl.fetchLexileLevels( request, currentPage );

        // verify logic
        assertEquals( DEFAULT_VALUE, readingLevelsMap.get( DEFAULT_KEY ) );
    }

}