package com.scholastic.classmags.migration.services.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.settings.SlingSettingsService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;
import org.osgi.service.event.Event;

import com.day.cq.replication.ReplicationAction;
import com.scholastic.classmags.migration.utils.SolrSearchClassMagsConstants;

public class ReplicationEventListenerTest {

    private ReplicationEventListener listener = new ReplicationEventListener();

    private ClassMagsSolrIndexService indexService = new ClassMagsSolrIndexService();

    Map< String, Object > config = new HashMap< String, Object >();

    @Mock
    private ReplicationAction action;

    @Mock
    private SlingSettingsService slingSettings;

    @Mock
    private Event event;

    @Mock
    private ResourceResolverFactory resourceResolverFactory;

    @Mock
    private ResourceResolver resourceResolver;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks( this );
        config.put( SolrSearchClassMagsConstants.ENABLED, true );
        String[] paths = { "/content/scholastic", "/content/dam/scholastic" };
        config.put( SolrSearchClassMagsConstants.OBSERVED_PATHS, paths );
        Whitebox.setInternalState( listener, "resourceResolverFactory", resourceResolverFactory );
        Whitebox.setInternalState( listener, "slingSettings", slingSettings );
        Whitebox.setInternalState( listener, "indexService", indexService );
    }

    @Test
    public void testHandleNullEvent() {
        listener.handleEvent( null );
    }
}