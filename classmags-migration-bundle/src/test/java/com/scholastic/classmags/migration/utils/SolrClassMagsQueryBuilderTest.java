package com.scholastic.classmags.migration.utils;

import org.apache.commons.lang.ArrayUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.SortClause;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertEquals;
import com.scholastic.classmags.migration.models.SearchRequest;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.FLD_TYPE_VAL_ARTICLE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.FLD_TYPE_VAL_ISSUE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.ROLE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.ROWS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_FACET_FIELD;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_AND;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FACET_SORT_BY_NAME;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_ARTICLES;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_BLOG;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_DOWNLOAD;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_GAME;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_ISSUE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_LESSON_PLANS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_POLL;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_SKILL_SHEET;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_FQ_TYPE_VIDEO;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_OR;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SOLR_QUERY_SORT_TITLE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SORT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_ARTICLE_CONTENT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_ARTICLE_CONTENT_WT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_ARTICLE_TITLE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_ARTICLE_TITLE_WT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_DESCRIPTION;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_DESCRIPTION_WT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_KEYWORDS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_KEYWORDS_WT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_PUBLISHED_DATE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_PUBLISHED_DATE_WT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_SITE_ID;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_SORT_DATE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_TAGS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_TAGS_WT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_TITLE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_TITLE_WT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_USER_TYPE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.START;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SUBJECTS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.TEXT;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.TYPES;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SUBJECTS_OPERATOR;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.CONTENT_TYPES_OPERATOR;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_SUBJECT_TAGS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SQ_FIELD_TYPE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.FLD_TYPE_CONTENTHUB;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SUBJECTS_FILTERS;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SORT_OPTION_TITLE;
import static com.scholastic.classmags.migration.utils.SolrSearchQueryConstants.SORT_OPTION_PUBLISHED;
import static com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS;
import static com.scholastic.classmags.migration.utils.RoleUtil.ROLE_TYPE_TEACHER;
import static com.scholastic.classmags.migration.utils.RoleUtil.ROLE_TYPE_STUDENT;

@RunWith( MockitoJUnitRunner.class )
public class SolrClassMagsQueryBuilderTest {

    /** The Mock SearchRequest */
    @Mock
    private SearchRequest searchRequest;

    /** The SolrQuery */
    private SolrQuery solrQuery;

    /** The String queryString */
    private String queryString;

    /** test Data */
    private static final boolean INCLUDE_FACET = true;

    /** test Data */
    private static final String SITE_ID = "siteId";

    /** test Data */
    private static final String DEFAULT_CASE = "defaultCase";
    /** three cases for filters one of which is null */
    /** the constant filter 1 */
    private static final String FILETRS_1 = "";

    /** the constant filter 2 */
    private static final String FILETRS_2 = "filters";

    /** test Data */
    private static final Integer START_1 = 1;

    /** test Data */
    private static final Integer ROW = 2;

    /** test data */
    private static final String SORT_1 = "sort_desc";

    /** test data */
    private static final String SORT_DUMMY = "sort_dummy";

    /** test data for sanitizeSearchTerm() */
    private static final String TEST_DATA = "sample";

    /** Test data for createFieldQuery() */
    private String queryStringTest = "";

    /** Test data */
    private StringBuilder sb;

    /** listSortClause test data */
    private List< SortClause > listSortClause;

    /** test data for getSeacrhRequest */
    private Map< String, String > params;

    /** Test Data for getQueryField */
    private String getQueryFieldTest;

    /** Test data for Search Query */
    private String[] types = null;

    private String subjectsQuery = null;

    private String additionalSubjectsQuery = null;

    private String contentTypesQuery = null;

    /** Test variable for sanitizeSearchTerm */
    private String sanitizeSearchTerm;

    /**
     * Setup.
     */
    @Before
    public void before() {

        params = new HashMap< >();
        listSortClause = new ArrayList< >();
        sb = new StringBuilder();

    }

    /**
     * Test for generateSolrFilterQuery() for preFilter=null and
     * searchRequest.getRole()=USER_TYPE_ANONYMOUS and
     * searchRequest.getFilters()=FILETRS_1 and searchRequest.getSort()=null;
     */
    @Test
    public void getSolrQueryWithNoPreFilter() {
        solrQuery = new SolrQuery();
        when( searchRequest.getSiteId() ).thenReturn( SITE_ID );
        when( searchRequest.getRole() ).thenReturn( USER_TYPE_ANONYMOUS );
        when( searchRequest.getFilters() ).thenReturn( FILETRS_1 );
        when( searchRequest.getSort() ).thenReturn( null );
        when( searchRequest.getStart() ).thenReturn( START_1 );
        when( searchRequest.getRows() ).thenReturn( ROW );

        solrQuery = SolrClassMagsQueryBuilder.generateSolrFilterQuery( null, searchRequest, solrQuery, INCLUDE_FACET );

        sb.append( "(" ).append( SQ_FIELD_SITE_ID ).append( ":\"" ).append( SITE_ID ).append( "\"" ).append( ")" )
                .append( SOLR_QUERY_AND ).append( "(" ).append( "(" ).append( SOLR_QUERY_FQ_TYPE_ARTICLES )
                .append( ")" ).append( SOLR_QUERY_OR ).append( "(" ).append( SOLR_QUERY_FQ_TYPE_GAME ).append( ")" )
                .append( SOLR_QUERY_OR ).append( "(" ).append( SOLR_QUERY_FQ_TYPE_VIDEO ).append( ")" )
                .append( SOLR_QUERY_OR ).append( "(" ).append( SOLR_QUERY_FQ_TYPE_DOWNLOAD ).append( ")" )
                .append( SOLR_QUERY_OR ).append( "(" ).append( SOLR_QUERY_FQ_TYPE_POLL ).append( ")" )
                .append( SOLR_QUERY_OR ).append( "(" ).append( SOLR_QUERY_FQ_TYPE_BLOG ).append( ")" )
                .append( SOLR_QUERY_OR ).append( "(" ).append( SOLR_QUERY_FQ_TYPE_SKILL_SHEET ).append( ")" )
                .append( SOLR_QUERY_OR ).append( "(" ).append( SOLR_QUERY_FQ_TYPE_ISSUE ).append( ")" )
                .append( SOLR_QUERY_OR ).append( "(" ).append( SOLR_QUERY_FQ_TYPE_LESSON_PLANS ).append( ")" )
                .append( " ) " );
        sb.append( SOLR_QUERY_AND ).append( "(" );
        sb.append( SQ_FIELD_USER_TYPE ).append( ":" ).append( "*" );
        sb.append( ")" );

        assertEquals( START_1, solrQuery.getStart() );
        assertEquals( ROW, solrQuery.getRows() );
        assertEquals( 1, solrQuery.getFacetMinCount() );
        assertEquals( SOLR_QUERY_FACET_SORT_BY_NAME, solrQuery.getFacetSortString() );
        assertEquals( 10000, solrQuery.getFacetLimit() );
        assertEquals( SOLR_FACET_FIELD.toString(), solrQuery.getFacetFields().toString() );
        assertEquals( sb.toString(), String.join( " ", Arrays.asList( solrQuery.getFilterQueries() ) ) );
        assertEquals( listSortClause, solrQuery.getSorts() );
    }

    /**
     * Test for generateSolrFilterQuery() for preFilter=FLD_TYPE_VAL_ARTICLE and
     * searchRequest.getRole()=ROLE_TYPE_TEACHER and
     * searchRequest.getFilters()=FILETRS_2 and
     * searchRequest.getSort()=SORT_OPTION_PUBLISHED;
     */
    @Test
    public void getSolrQueryWithPreFilter1() {
        solrQuery = new SolrQuery();
        when( searchRequest.getSiteId() ).thenReturn( SITE_ID );
        when( searchRequest.getRole() ).thenReturn( ROLE_TYPE_TEACHER );
        when( searchRequest.getFilters() ).thenReturn( FILETRS_2 );
        when( searchRequest.getSort() ).thenReturn( SORT_OPTION_PUBLISHED );
        when( searchRequest.getStart() ).thenReturn( START_1 );
        when( searchRequest.getRows() ).thenReturn( ROW );
        solrQuery = SolrClassMagsQueryBuilder.generateSolrFilterQuery( FLD_TYPE_VAL_ARTICLE, searchRequest, solrQuery,
                INCLUDE_FACET );

        sb.append( "(" ).append( SQ_FIELD_SITE_ID ).append( ":\"" ).append( searchRequest.getSiteId() ).append( "\"" )
                .append( ")" ).append( SOLR_QUERY_AND ).append( "NOT(" ).append( "type_s:Issue*" ).append( ")" );
        sb.append( SOLR_QUERY_AND ).append( "(" );
        sb.append( SQ_FIELD_USER_TYPE ).append( ":\"" ).append( ROLE_TYPE_TEACHER ).append( "\"" )
                .append( SOLR_QUERY_OR );
        sb.append( SQ_FIELD_USER_TYPE ).append( ":\"" ).append( RoleUtil.ROLE_TYPE_TEACHER_OR_STUDENT ).append( "\"" )
                .append( SOLR_QUERY_OR );
        sb.append( SQ_FIELD_USER_TYPE ).append( ":\"" ).append( RoleUtil.ROLE_TYPE_EVERYONE ).append( "\"" );
        sb.append( ")" );

        listSortClause.add( new SortClause( SQ_FIELD_SORT_DATE, SolrQuery.ORDER.desc ) );

        assertEquals( START_1, solrQuery.getStart() );
        assertEquals( ROW, solrQuery.getRows() );
        assertEquals( 1, solrQuery.getFacetMinCount() );
        assertEquals( SOLR_QUERY_FACET_SORT_BY_NAME, solrQuery.getFacetSortString() );
        assertEquals( 10000, solrQuery.getFacetLimit() );
        assertEquals( SOLR_FACET_FIELD.toString(), solrQuery.getFacetFields().toString() );
        String test = "filters " + sb.toString();
        assertEquals( test, String.join( " ", Arrays.asList( solrQuery.getFilterQueries() ) ) );
        assertEquals( listSortClause, solrQuery.getSorts() );

    }

    /**
     * Test for generateSolrFilterQuery() for preFilter=FLD_TYPE_VAL_ISSUE and
     * searchRequest.getRole()=ROLE_TYPE_STUDENT and
     * searchRequest.getFilters()=null and
     * searchRequest.getSort()=SORT_OPTION_TITLE;
     */
    @Test
    public void getSolrQueryWithPreFilter2() {
        solrQuery = new SolrQuery();

        when( searchRequest.getSiteId() ).thenReturn( SITE_ID );
        when( searchRequest.getRole() ).thenReturn( ROLE_TYPE_STUDENT );
        when( searchRequest.getFilters() ).thenReturn( null );
        when( searchRequest.getSort() ).thenReturn( SORT_OPTION_TITLE );

        solrQuery = SolrClassMagsQueryBuilder.generateSolrFilterQuery( FLD_TYPE_VAL_ISSUE, searchRequest, solrQuery,
                INCLUDE_FACET );

        sb.append( "(" ).append( SQ_FIELD_SITE_ID ).append( ":\"" ).append( searchRequest.getSiteId() ).append( "\"" )
                .append( ")" ).append( SOLR_QUERY_AND ).append( "(" ).append( "type_s:Issue*" ).append( ")" );
        sb.append( SOLR_QUERY_AND ).append( "(" );
        sb.append( SQ_FIELD_USER_TYPE ).append( ":\"" ).append( ROLE_TYPE_STUDENT ).append( "\"" )
                .append( SOLR_QUERY_OR );
        sb.append( SQ_FIELD_USER_TYPE ).append( ":\"" ).append( RoleUtil.ROLE_TYPE_TEACHER_OR_STUDENT ).append( "\"" )
                .append( SOLR_QUERY_OR );
        sb.append( SQ_FIELD_USER_TYPE ).append( ":\"" ).append( RoleUtil.ROLE_TYPE_EVERYONE ).append( "\"" );
        sb.append( ")" );

        listSortClause.add( new SortClause( SOLR_QUERY_SORT_TITLE, SolrQuery.ORDER.asc ) );

        assertEquals( sb.toString(), String.join( " ", Arrays.asList( solrQuery.getFilterQueries() ) ) );
        assertEquals( listSortClause, solrQuery.getSorts() );
    }

    /**
     * Test for generateSolrFilterQuery()
     * 
     * for preFilter=FLD_TYPE_CONTENTHUB and
     * searchRequest.getRole()=DEFAULT_CASE;
     */
    @Test
    public void getSolrQueryWithPreFilter3() {
        solrQuery = new SolrQuery();
        when( searchRequest.getSiteId() ).thenReturn( SITE_ID );
        when( searchRequest.getRole() ).thenReturn( DEFAULT_CASE );
        when( searchRequest.getFilters() ).thenReturn( null );
        when( searchRequest.getSort() ).thenReturn( SORT_DUMMY );

        solrQuery = SolrClassMagsQueryBuilder.generateSolrFilterQuery( FLD_TYPE_CONTENTHUB, searchRequest, solrQuery,
                INCLUDE_FACET );

        sb.append( "(" ).append( SQ_FIELD_SITE_ID ).append( ":\"" ).append( searchRequest.getSiteId() ).append( "\"" )
                .append( ")" );
        sb.append( SOLR_QUERY_AND ).append( "(" );
        sb.append( SQ_FIELD_USER_TYPE ).append( ":\"" ).append( DEFAULT_CASE ).append( "\"" );
        sb.append( ")" );

        assertEquals( sb.toString(), String.join( " ", Arrays.asList( solrQuery.getFilterQueries() ) ) );
        assertEquals( listSortClause, solrQuery.getSorts() );

    }

    /**
     * Test for generateSearchRequest() (Case 1)
     * 
     * and sanitizeSearchTerm()
     */
    @Test
    public void getSearchRequestCaseOne() {
        searchRequest = new SearchRequest();
        sanitizeSearchTerm = SolrSearchUtil.escapeSolrCharacters( TEST_DATA );

        params.put( "text", "" );
        params.put( "sort", "" );
        params.put( "subjects", SUBJECTS );
        params.put( "subjectsOperator", SUBJECTS_OPERATOR );
        params.put( "subjectFilters", SUBJECTS_FILTERS );
        params.put( "contentTypes", TYPES );
        params.put( "contentOperator", CONTENT_TYPES_OPERATOR );
        params.put( "siteId", SITE_ID );
        params.put( "start", START );
        params.put( "rows", "5" );
        params.put( "role", ROLE );

        types = params.get( "subjects" ).split( "," );
        if ( ArrayUtils.isNotEmpty( types ) ) {
            subjectsQuery = SolrClassMagsQueryBuilder.createFieldQuery( types, SQ_FIELD_SUBJECT_TAGS,
                    params.get( "subjectsOperator" ) );
        }
        types = params.get( "subjectFilters" ).split( "," );
        if ( ArrayUtils.isNotEmpty( types ) ) {
            additionalSubjectsQuery = SolrClassMagsQueryBuilder.createFieldQuery( types, SQ_FIELD_SUBJECT_TAGS,
                    SOLR_QUERY_OR );
        }

        StringBuilder sb1 = new StringBuilder();
        sb1.append( "(" ).append( subjectsQuery ).append( ")" ).append( SOLR_QUERY_AND ).append( "(" )
                .append( additionalSubjectsQuery ).append( ")" );

        types = params.get( "contentTypes" ).split( "," );
        if ( ArrayUtils.isNotEmpty( types ) ) {
            contentTypesQuery = SolrClassMagsQueryBuilder.createFieldQuery( types, SQ_FIELD_TYPE,
                    params.get( "contentOperator" ) );
        }

        StringBuilder sb = new StringBuilder();
        sb.append( "(" ).append( sb1.toString() ).append( ")" ).append( SOLR_QUERY_AND ).append( "(" )
                .append( contentTypesQuery ).append( ")" );

        searchRequest = SolrClassMagsQueryBuilder.generateSearchRequest( params );
        Assert.assertTrue(
                searchRequest.getText().equalsIgnoreCase( SolrSearchClassMagsConstants.SOLR_QUERY_WILDCARD ) );
        Assert.assertNull( searchRequest.getSort() );
        Assert.assertEquals( searchRequest.getStart(), SolrSearchQueryConstants.DEFAULT_START_VALUE );
        Assert.assertEquals( searchRequest.getRows(), SolrSearchQueryConstants.DEFAULT_ROWS_VALUE );
        assertEquals( searchRequest.getFilters(), sb.toString() );
        assertEquals( SolrClassMagsQueryBuilder.sanitizeSearchTerm( TEST_DATA ), sanitizeSearchTerm );
    }

    /**
     * Test for generateSearchRequest() for Case 2
     */
    @Test
    public void getSearchRequestCaseTwo() {
        searchRequest = new SearchRequest();

        params.put( "text", TEXT );
        params.put( "sort", "" );
        params.put( "subjects", SUBJECTS );
        params.put( "subjectsOperator", "" );
        params.put( "subjectFilters", SUBJECTS_FILTERS );
        params.put( "contenttypes", "" );
        params.put( "contentOperator", CONTENT_TYPES_OPERATOR );
        params.put( "siteId", SITE_ID );
        params.put( "start", START );
        params.put( "rows", "7" );
        params.put( "role", ROLE );

        types = params.get( "subjects" ).split( "," );
        if ( ArrayUtils.isNotEmpty( types ) ) {
            subjectsQuery = SolrClassMagsQueryBuilder.createFieldQuery( types, SQ_FIELD_SUBJECT_TAGS, SOLR_QUERY_OR );
        }
        types = params.get( "subjectFilters" ).split( "," );
        if ( ArrayUtils.isNotEmpty( types ) ) {
            additionalSubjectsQuery = SolrClassMagsQueryBuilder.createFieldQuery( types, SQ_FIELD_SUBJECT_TAGS,
                    SOLR_QUERY_OR );
        }

        StringBuilder sb = new StringBuilder();
        sb.append( "(" ).append( subjectsQuery ).append( ")" ).append( SOLR_QUERY_AND ).append( "(" )
                .append( additionalSubjectsQuery ).append( ")" );

        searchRequest = SolrClassMagsQueryBuilder.generateSearchRequest( params );
        Assert.assertTrue( searchRequest.getText().equalsIgnoreCase( SolrSearchUtil.escapeSolrCharacters( "text" ) ) );
        assertEquals( searchRequest.getFilters(), sb.toString() );
        Assert.assertTrue( searchRequest.getSiteId().equalsIgnoreCase( params.get( SITE_ID ) ) );
        Assert.assertTrue( searchRequest.getRole().equalsIgnoreCase( params.get( ROLE ) ) );
        Assert.assertEquals( searchRequest.getRows(), Integer.parseInt( params.get( "rows" ) ) );

    }

    /**
     * Test for generateSearchRequest() for Case 3
     */

    @Test
    public void getSearchRequestCase3() {
        searchRequest = new SearchRequest();

        /** Generate test data */
        params.put( "text", null );
        params.put( "sort", SORT );
        params.put( "subjects", "" );
        params.put( "subjectsOperator", "" );
        params.put( "subjectFilters", "my, name, is, kaushal" );
        params.put( "contentTypes", TYPES );
        params.put( "contentOperator", "" );
        params.put( "siteId", null );
        params.put( "start", "1234" );
        params.put( "rows", "19" );
        params.put( "role", ROLE );

        types = params.get( "subjectFilters" ).split( "," );
        if ( ArrayUtils.isNotEmpty( types ) ) {
            additionalSubjectsQuery = SolrClassMagsQueryBuilder.createFieldQuery( types, SQ_FIELD_SUBJECT_TAGS,
                    SOLR_QUERY_OR );
        }

        types = params.get( "contentTypes" ).split( "," );
        if ( ArrayUtils.isNotEmpty( types ) ) {
            contentTypesQuery = SolrClassMagsQueryBuilder.createFieldQuery( types, SQ_FIELD_TYPE, SOLR_QUERY_OR );
        }

        StringBuilder sb = new StringBuilder();
        sb.append( "(" ).append( additionalSubjectsQuery ).append( ")" ).append( SOLR_QUERY_AND ).append( "(" )
                .append( contentTypesQuery ).append( ")" );
        searchRequest = SolrClassMagsQueryBuilder.generateSearchRequest( params );

        Assert.assertTrue(
                searchRequest.getText().equalsIgnoreCase( SolrSearchClassMagsConstants.SOLR_QUERY_WILDCARD ) );
        Assert.assertTrue( searchRequest.getSort().equalsIgnoreCase( SORT ) );
        assertEquals( searchRequest.getFilters(), sb.toString() );
        Assert.assertNull( searchRequest.getSiteId() );
        assertEquals( searchRequest.getStart(), Integer.parseInt( params.get( "start" ) ) );
        assertEquals( searchRequest.getRows(), SolrSearchQueryConstants.DEFAULT_ROWS_VALUE );

    }

    /**
     * test for generateSearchRequest() for case 4 and getQueryField()
     */
    @Test
    public void getSearchRequestCase4() {

        searchRequest = new SearchRequest();

        /** Generate test data */
        params.put( "text", "" );
        params.put( "start", START );
        params.put( "rows", ROWS );
        params.put( "sort", null );
        params.put( "subjects", "" );
        params.put( "subjectsOperator", "" );
        params.put( "subjectFilters", "" );
        params.put( "contentTypes", TYPES );
        params.put( "contentOperator", CONTENT_TYPES_OPERATOR );
        params.put( "siteId", SITE_ID );
        params.put( "role", ROLE );

        types = params.get( "contentTypes" ).split( "," );
        if ( ArrayUtils.isNotEmpty( types ) ) {
            contentTypesQuery = SolrClassMagsQueryBuilder.createFieldQuery( types, SQ_FIELD_TYPE,
                    params.get( "contentOperator" ) );
        }

        sb.append(SQ_FIELD_TITLE).append(" ");
	    sb.append(SQ_FIELD_TAGS).append(" ");
	    sb.append(SQ_FIELD_KEYWORDS).append(" ");
	    sb.append(SQ_FIELD_DESCRIPTION).append(" ");
	    sb.append(SQ_FIELD_ARTICLE_TITLE).append(" ");
	    sb.append(SQ_FIELD_ARTICLE_CONTENT).append(" ");
	    sb.append(SQ_FIELD_PUBLISHED_DATE);

        searchRequest = SolrClassMagsQueryBuilder.generateSearchRequest( params );
        getQueryFieldTest = SolrClassMagsQueryBuilder.getQueryFields( "" );
        Assert.assertTrue( searchRequest.getFilters().equalsIgnoreCase( contentTypesQuery ) );
        Assert.assertNull( searchRequest.getSort() );
//        Assert.assertTrue( getQueryFieldTest.equalsIgnoreCase( sb.toString() ) );
    }

    /**
     * test for generateSearchRequest() for case 5 and createFieldQuery(param1,
     * param2)
     */

    @Test
    public void getSearchRequestCase5() {

        searchRequest = new SearchRequest();

        /** Generate test data */
        params.put( "text", "" );
        params.put( "sort", SORT_1 );
        params.put( "subjects", "" );
        params.put( "subjectsOperator", "" );
        params.put( "subjectFilters", "" );
        params.put( "contentTypes", TYPES );
        params.put( "contentOperator", "" );
        params.put( "siteId", SITE_ID );
        params.put( "start", START );
        params.put( "rows", ROWS );
        params.put( "role", ROLE );

        types = params.get( "contentTypes" ).split( "," );

        if ( ArrayUtils.isNotEmpty( types ) ) {
            contentTypesQuery = SolrClassMagsQueryBuilder.createFieldQuery( types, SQ_FIELD_TYPE, SOLR_QUERY_OR );
        }

        sb = new StringBuilder();
        for ( String s : types ) {
            sb.append( "(" ).append( SQ_FIELD_TYPE ).append( ":\"" ).append( s.trim().replaceAll( "-", " " ) )
                    .append( "\")" ).append( SOLR_QUERY_OR );
        }
        queryStringTest = sb.toString();
        if ( queryStringTest.endsWith( SOLR_QUERY_OR ) ) {
            queryStringTest = queryStringTest.subSequence( 0, sb.lastIndexOf( SOLR_QUERY_OR ) ).toString();
        }

        searchRequest = SolrClassMagsQueryBuilder.generateSearchRequest( params );
        queryString = SolrClassMagsQueryBuilder.createFieldQuery( types, SQ_FIELD_TYPE );
        assertEquals( searchRequest.getFilters(), contentTypesQuery );
        Assert.assertEquals( searchRequest.getRows(), SolrSearchQueryConstants.DEFAULT_ROWS_VALUE );
        assertEquals( searchRequest.getSort(), SORT_1.trim().replace( SolrSearchQueryConstants.DESC_SUFIX, "" ) );
        assertEquals( searchRequest.getSortOrder(), SolrQuery.ORDER.desc );
        Assert.assertTrue( queryString.equalsIgnoreCase( queryStringTest ) );

    }

}
