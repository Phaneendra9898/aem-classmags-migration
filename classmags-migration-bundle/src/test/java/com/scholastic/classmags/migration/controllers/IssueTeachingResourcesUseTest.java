package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.services.TeachingResourcesService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * JUnit for IssueTeachingResourcesUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { IssueTeachingResourcesUse.class } )
public class IssueTeachingResourcesUseTest extends BaseComponentTest {

    private IssueTeachingResourcesUse issueTeachingResourcesUse;
    private Map< String, List< ResourcesObject > > teachingResources;
    private TeachingResourcesService teachingResourcesService;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        issueTeachingResourcesUse = Whitebox.newInstance( IssueTeachingResourcesUse.class );
        stubCommon( issueTeachingResourcesUse );

        teachingResourcesService = mock( TeachingResourcesService.class );

        when( currentPage.getContentResource() ).thenReturn( resource );
        when( resource.getChild( ClassMagsMigrationConstants.ISSUE_CONFIGURATION ) ).thenReturn( resource );
        when( slingScriptHelper.getService( TeachingResourcesService.class ) ).thenReturn( teachingResourcesService );
    }

    @Test
    public void testIssueTeachingResources() throws Exception {
        // setup logic ( test-specific )
        createTeachingResources();
        when( teachingResourcesService.getTeachingResources( request, resource, "resources" ) )
                .thenReturn( teachingResources );

        // execute logic
        Whitebox.invokeMethod( issueTeachingResourcesUse, "activate" );

        // verify logic
        assertEquals( issueTeachingResourcesUse.getTeachingResources().get( "List 1" ).get( 0 ).getTitle(),
                "Sample Resource" );
    }

    @Test
    public void testCreateInstance() throws Exception {
        // execute logic
        final IssueTeachingResourcesUse issueTeachingResourcesUse = new IssueTeachingResourcesUse();

        // verify logic
        assertNotNull( issueTeachingResourcesUse );
    }

    @Test
    public void testIssueTeachingResourceWhenCurrentPageIsNull() throws Exception {
        // setup logic ( test-specific )
        stub( PowerMockito.method( issueTeachingResourcesUse.getClass(), "getCurrentPage" ) ).toReturn( null );

        // execute logic
        Whitebox.invokeMethod( issueTeachingResourcesUse, "activate" );

        // verify logic
        assertNull( issueTeachingResourcesUse.getTeachingResources() );
    }

    @Test
    public void testIssueTeachingResourceWhenChildResourceIsNull() throws Exception {
        // setup logic ( test-specific )
        when( resource.getChild( ClassMagsMigrationConstants.ISSUE_CONFIGURATION ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( issueTeachingResourcesUse, "activate" );

        // verify logic
        assertNull( issueTeachingResourcesUse.getTeachingResources() );
    }

    @Test
    public void testIssueTeachingResourceWhenTeachingResourcesServiceIsNull() throws Exception {
        // setup logic ( test-specific )
        when( slingScriptHelper.getService( TeachingResourcesService.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( issueTeachingResourcesUse, "activate" );

        // verify logic
        verify( teachingResourcesService, times( 0 ) ).getTeachingResources( request, resource, "resources" );
    }

    public void createTeachingResources() {

        ResourcesObject resourceObject = new ResourcesObject();
        resourceObject.setTitle( "Sample Resource" );

        List< ResourcesObject > list = new ArrayList<>();
        list.add( resourceObject );

        teachingResources = new HashMap<>();
        teachingResources.put( "List 1", list );
    }
}
