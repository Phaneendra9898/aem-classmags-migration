package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for SocialLinkConfigUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { SocialLinksConfigUse.class, CommonUtils.class } )
public class SocialLinksConfigUseTest extends BaseComponentTest {

    private static final String NODE_NAME = "social-links";
    private static final String LINK_PATH_KEY = "linkPath";
    private static final String LINK_ICON_KEY = "linkIcon";
    private static final String FACEBOOK_LINK_PATH = "www.facebook.com";
    private static final String FACEBOOK_ICON_PATH = "facebook icon";
    private static final String TWITTER_LINK_PATH = "www.twitter.com";
    private static final String TWITTER_ICON_PATH = "twitter icon";

    private SocialLinksConfigUse socialLinksConfigUse;
    private List< ValueMap > socialLinks;
    private ValueMap socialLink1, socialLink2;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        socialLinksConfigUse = Whitebox.newInstance( SocialLinksConfigUse.class );

        stubCommon( socialLinksConfigUse );

        PowerMockito.mockStatic( CommonUtils.class );
        socialLinks = new ArrayList< >();
        socialLink1 = mock( ValueMap.class );
        socialLink2 = mock( ValueMap.class );

        when( socialLink1.get( LINK_PATH_KEY, String.class ) ).thenReturn( FACEBOOK_LINK_PATH );
        when( socialLink1.get( LINK_ICON_KEY, String.class ) ).thenReturn( FACEBOOK_ICON_PATH );
        when( socialLink2.get( LINK_PATH_KEY, String.class ) ).thenReturn( TWITTER_LINK_PATH );
        when( socialLink2.get( LINK_ICON_KEY, String.class ) ).thenReturn( TWITTER_ICON_PATH );

        socialLinks.add( socialLink1 );
        socialLinks.add( socialLink2 );
    }

    @Test
    public void testIssueLinkUse() throws Exception {
        // setup logic(test-specific)
        when( CommonUtils.fetchMultiFieldData( resource, NODE_NAME ) ).thenReturn( socialLinks );
        // execute logic
        Whitebox.invokeMethod( socialLinksConfigUse, "activate" );

        // verify logic
        assertEquals( 2, socialLinksConfigUse.getSocialIcons().size() );
        assertEquals( FACEBOOK_LINK_PATH,
                socialLinksConfigUse.getSocialIcons().get( 0 ).get( LINK_PATH_KEY, String.class ) );
        assertEquals( TWITTER_LINK_PATH,
                socialLinksConfigUse.getSocialIcons().get( 1 ).get( LINK_PATH_KEY, String.class ) );
        assertEquals( FACEBOOK_ICON_PATH,
                socialLinksConfigUse.getSocialIcons().get( 0 ).get( LINK_ICON_KEY, String.class ) );
        assertEquals( TWITTER_ICON_PATH,
                socialLinksConfigUse.getSocialIcons().get( 1 ).get( LINK_ICON_KEY, String.class ) );
    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final SocialLinksConfigUse socialLinksConfigUse = new SocialLinksConfigUse();

        // verify logic
        assertNotNull( socialLinksConfigUse );

    }

}
