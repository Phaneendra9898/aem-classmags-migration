package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.models.NavigationLevel;

/**
 * JUnit for GlobalNavConfigUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { GlobalNavConfigUse.class } )
public class GlobalNavConfigUseTest extends BaseComponentTest {

    private static final String NAVIGATION_TYPE_KEY = "navType";
    private static final String PRIMARY_LINK_KEY = "primaryNavLink";
    private static final String NAVIGATION_TYPE_SECONDARY = "secondary";
    private static final String NAVIGATION_TYPE_TERTIARY = "tertiary";
    private static final String CHILD_TITLE_KEY = "childTitle";
    private static final String CHILD_LINK_KEY = "childLink";
    private static final String ITEM_TITLE_ONE = "Item Title 1";
    private static final String ITEM_TITLE_TWO = "Item Title 2";
    private static final String ITEM_LINK_ONE = "Item Path 1";
    private static final String ITEM_LINK_TWO = "Item Path 2";
    private static final String PRIMARY_NAV_LINK = "/content/classroom_magazines/primarylink";

    private GlobalNavConfigUse globalNavConfigUse;
    private ResourceResolver resourceResolver;
    private Resource childNavResource;
    private Resource resource1, resource2;
    private Iterator< Resource > iteratorResource;
    private ListIterator< NavigationLevel > iteratorNavigationLevel;
    private NavigationLevel nav1, nav2;
    private List< NavigationLevel > allLevels;
    private ValueMap propertiesResource1;
    private ValueMap propertiesResource2;

    /**
     * Setup.
     */
    @SuppressWarnings( "unchecked" )
    @Before
    public void setup() {
        globalNavConfigUse = Whitebox.newInstance( GlobalNavConfigUse.class );

        stubCommon( globalNavConfigUse );
        
        resourceResolver = mock( ResourceResolver.class );
        childNavResource = mock( Resource.class );
        resource1 = mock( Resource.class );
        resource2 = mock( Resource.class );
        allLevels = mock( ArrayList.class );
        iteratorResource = mock( Iterator.class );
        iteratorNavigationLevel = mock( ListIterator.class );
        nav1 = mock( NavigationLevel.class );
        nav2 = mock( NavigationLevel.class );
        propertiesResource1 = mock( ValueMap.class );
        propertiesResource2 = mock( ValueMap.class );

        propertiesResource1.put( CHILD_TITLE_KEY, ITEM_TITLE_ONE );
        propertiesResource2.put( CHILD_TITLE_KEY, ITEM_TITLE_TWO );
        propertiesResource1.put( NAVIGATION_TYPE_KEY, NAVIGATION_TYPE_SECONDARY );
        propertiesResource2.put( NAVIGATION_TYPE_KEY, NAVIGATION_TYPE_TERTIARY );
        propertiesResource1.put( CHILD_LINK_KEY, ITEM_LINK_ONE );
        propertiesResource2.put( CHILD_LINK_KEY, ITEM_LINK_TWO );
        nav1.setLinkTitle( ITEM_TITLE_ONE );
        nav2.setLinkTitle( ITEM_TITLE_TWO );
        nav1.setNavigationType( NAVIGATION_TYPE_SECONDARY );
        nav2.setNavigationType( NAVIGATION_TYPE_TERTIARY );
        nav1.setLinkPath( ITEM_LINK_ONE );
        nav2.setLinkPath( ITEM_LINK_TWO );
        
        when( resource.getResourceResolver()).thenReturn( resourceResolver );
        when( resource.getChild( "childNavSettings" ) ).thenReturn( childNavResource );
        when( childNavResource.listChildren() ).thenReturn( iteratorResource );

        when( iteratorResource.hasNext() ).thenReturn( true, true, false );
        when( iteratorResource.next() ).thenReturn( resource1, resource2 );

        allLevels.add( nav1 );
        allLevels.add( nav2 );

        when( iteratorNavigationLevel.hasNext() ).thenReturn( true, true, false );
        when( allLevels.listIterator() ).thenReturn( iteratorNavigationLevel );
        when( resource1.getValueMap() ).thenReturn( propertiesResource1 );
        when( resource2.getValueMap() ).thenReturn( propertiesResource2 );
        when( propertiesResource1.get( CHILD_TITLE_KEY, String.class ) ).thenReturn( ITEM_TITLE_ONE );
        when( propertiesResource2.get( CHILD_TITLE_KEY, String.class ) ).thenReturn( ITEM_TITLE_TWO );
        when( propertiesResource1.get( CHILD_LINK_KEY, String.class ) ).thenReturn( ITEM_LINK_ONE );
        when( propertiesResource2.get( CHILD_LINK_KEY, String.class ) ).thenReturn( ITEM_LINK_TWO );
        when( propertiesResource1.get( NAVIGATION_TYPE_KEY, String.class ) ).thenReturn( NAVIGATION_TYPE_SECONDARY );
        when( propertiesResource2.get( NAVIGATION_TYPE_KEY, String.class ) ).thenReturn( NAVIGATION_TYPE_TERTIARY );
        when( properties.get( PRIMARY_LINK_KEY, String.class )).thenReturn( PRIMARY_NAV_LINK );

    }

    @Test
    public void testWhenPrimaryLinkIsNull() throws Exception {
        // setup logic(test-specific)
        when(properties.get( PRIMARY_LINK_KEY, String.class )).thenReturn( null );
        
        // execute logic
        Whitebox.invokeMethod( globalNavConfigUse, "activate" );

        // verify logic
        
        assertEquals(null ,globalNavConfigUse.getPrimaryNavLink());
    }
    
    @Test
    public void testWhenPrimaryLinkIsEmpty() throws Exception {
        // setup logic(test-specific)
        when(properties.get( PRIMARY_LINK_KEY, String.class )).thenReturn( StringUtils.EMPTY );
        
        // execute logic
        Whitebox.invokeMethod( globalNavConfigUse, "activate" );

        // verify logic
        assertEquals(null ,globalNavConfigUse.getPrimaryNavLink());
    }
    
    @Test
    public void testWhenChildNodesAreNotPresent() throws Exception {
        // setup logic(test-specific)
        when( resource.getChild( "childNavSettings" ) ).thenReturn( null );
        
        // execute logic
        Whitebox.invokeMethod( globalNavConfigUse, "activate" );

        // verify logic
        assertTrue( globalNavConfigUse.getSecondaryLevels().isEmpty() );
        assertTrue( globalNavConfigUse.getSecondaryLevelsMap().isEmpty() );
    }
    
    @Test
    public void testWhenBothSecondaryAndTertiaryPresent() throws Exception {
        // execute logic
        Whitebox.invokeMethod( globalNavConfigUse, "activate" );

        // verify logic
        assertEquals( globalNavConfigUse.getSecondaryLevels().get( 0 ).getLinkTitle(), ITEM_TITLE_ONE );
        assertEquals( globalNavConfigUse.getSecondaryLevels().get( 0 ).getLinkPath(), ITEM_LINK_ONE );
        assertEquals( globalNavConfigUse.getSecondaryLevelsMap().get( ITEM_TITLE_ONE ).get( 0 ).getLinkTitle(),
                ITEM_TITLE_TWO );
        assertEquals(PRIMARY_NAV_LINK ,globalNavConfigUse.getPrimaryNavLink());
    }

    @Test
    public void testWhenNoSecondaryLevelsPresent() throws Exception {
        // setup logic (test-specific)
        when( iteratorResource.hasNext() ).thenReturn( false );

        // execute logic
        Whitebox.invokeMethod( globalNavConfigUse, "activate" );

        // verify logic
        assertTrue( globalNavConfigUse.getSecondaryLevels().isEmpty() );
        assertTrue( globalNavConfigUse.getSecondaryLevelsMap().isEmpty() );
    }
    
    @Test
    public void testWhenNoChildLinkPathIsNull() throws Exception {
        // setup logic (test-specific)
        when( propertiesResource1.get( CHILD_LINK_KEY, String.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( globalNavConfigUse, "activate" );

        // verify logic
        assertNull( globalNavConfigUse.getSecondaryLevels().get( 0 ).getLinkPath() );
    }
    
    @Test
    public void testWhenNoChildLinkPathIsEmpty() throws Exception {
        // setup logic (test-specific)
        when( propertiesResource1.get( CHILD_LINK_KEY, String.class ) ).thenReturn( StringUtils.EMPTY );

        // execute logic
        Whitebox.invokeMethod( globalNavConfigUse, "activate" );

        // verify logic
        assertNull( globalNavConfigUse.getSecondaryLevels().get( 0 ).getLinkPath() );
    }

    @Test
    public void testWhenConsecutiveSecondaryLevelsPresent() throws Exception {
        // setup logic (test-specific)
        when( propertiesResource2.get( NAVIGATION_TYPE_KEY, String.class ) ).thenReturn( NAVIGATION_TYPE_SECONDARY );

        // execute logic
        Whitebox.invokeMethod( globalNavConfigUse, "activate" );

        // verify logic
        assertEquals( globalNavConfigUse.getSecondaryLevels().size(), 2 );
        assertEquals( globalNavConfigUse.getSecondaryLevels().get( 0 ).getLinkTitle(), ITEM_TITLE_ONE );
        assertEquals( globalNavConfigUse.getSecondaryLevels().get( 1 ).getLinkTitle(), ITEM_TITLE_TWO );
        assertTrue( globalNavConfigUse.getSecondaryLevelsMap().get( ITEM_TITLE_ONE ).isEmpty() );
    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final GlobalNavConfigUse globalNavConfigUse = new GlobalNavConfigUse();

        // verify logic
        assertNotNull( globalNavConfigUse );

    }

}
