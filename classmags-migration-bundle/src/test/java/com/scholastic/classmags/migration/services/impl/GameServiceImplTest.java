package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.util.List;

import javax.jcr.Node;
import javax.jcr.RepositoryException;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.day.cq.dam.api.Asset;
import com.day.cq.dam.api.DamConstants;
import com.day.cq.dam.api.Rendition;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.GameModel;
import com.scholastic.classmags.migration.services.GameService;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for GameService.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ GameServiceImpl.class, CommonUtils.class })
public class GameServiceImplTest {

    private static final String ASSET_PATH = "content/dam/classroom_magazines/games/index";
    private static final String TITLE_NAME = "title";
    private static final String DC_TITLE_META_DATA = "assetMetaData";
    private static final String TEST_DC_DESCRIPTION = "DCDescriptionMetaData";
    private static final String FILE_REFRENCE_PATH = "/content/scholastic/class-mags/fileRefrence";
    private static final String TEST_THUMBNAIL_IMG = "/content/scholastic/class-mags/fileRefrence.transform/content-tile/image.png";
    private static final String NODE_NAME = "game_html5";
    private static final String TEST_GAME_PATH = "content/dam/classroom_magazines/games/index.html";
    private static final String TEST_DATA = "dummyData";
    private static final String TEST_PATH = "testPath";
    private static final String MAGZINE_NAME = "magazineName";
    private static final String CONTAINER_PATH = "containerPath";
    private static final String GAME_TYPE = "gameType";
    private static final String XML_PATH = "xmlPath";
    private static final String GAME_PATH_1 = "gametype=" + GAME_TYPE + "&contentXML=" + XML_PATH + "&assetSWF="+ ASSET_PATH;
    private static final String GAME_PATH_2 = "gametype=" + GAME_TYPE + "&assetSWF=" + ASSET_PATH;
    private static final String RESOURCE_TYPE_GAME = "scholastic/classroom-magazines-migration/components/content/game-html5";
    private static final String RESOURCE_TYPE_FLASH_GAME = "scholastic/classroom-magazines-migration/components/content/game-flash";
    
    private GameService gameService;
    
    @Mock
    private Resource mockResource;
    @Mock
    private Resource mockAssetResource;
    @Mock
    private Resource mockAssetResourceChild;
    @Mock
    private Node mockNode;
    @Mock
    private ResourceResolverFactory mockResourceResolverFactory;
    @Mock
    private ResourcePathConfigService mockResourcePathConfigService;
    @Mock
    private PropertyConfigService mockPropertyConfigService;
    @Mock
    private MagazineProps mockMagazineProps;
    @Mock
    private Asset mockAsset;
    @Mock
    private ValueMap mockValueMap;
    @Mock
    private List<Rendition> renditions;
    @Mock
    private Rendition rendition;
    private GameModel gameData;

    /**
     * Initial Setup.
     * 
     * @throws LoginException
     * @throws RepositoryException
     * @throws ClassmagsMigrationBaseException
     */
    @Before
    public void setUp() throws LoginException {
        gameService = new GameServiceImpl();
        gameData = new GameModel();
        PowerMockito.mockStatic(CommonUtils.class);
        Whitebox.setInternalState(gameService, mockResourceResolverFactory);
        Whitebox.setInternalState(gameService, mockResourcePathConfigService);
        Whitebox.setInternalState(gameService, mockPropertyConfigService);
        Whitebox.setInternalState(gameService, mockMagazineProps);       
        when(mockResource.adaptTo(Node.class)).thenReturn(mockNode);
        when(mockResource.getResourceType()).thenReturn(RESOURCE_TYPE_GAME);
        when(CommonUtils.propertyCheck("gamePath", mockNode)).thenReturn(ASSET_PATH);
        when(mockAssetResource.adaptTo(Asset.class)).thenReturn(mockAsset);        
        when(CommonUtils.getResource(mockResourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE, ASSET_PATH))
                .thenReturn(mockAssetResource); 
        when(mockResource.getPath()).thenReturn(TEST_PATH);
        when(mockMagazineProps.getMagazineName(TEST_PATH)).thenReturn(MAGZINE_NAME);
        when(CommonUtils.getDataPath(mockResourcePathConfigService, MAGZINE_NAME)).thenReturn(TEST_DATA);
        when(mockPropertyConfigService.getDataConfigProperty(ClassMagsMigrationConstants.FLASH_GAME_CONTAINER_PROPERTY,
                TEST_DATA)).thenReturn(CONTAINER_PATH);
        
        when(mockAssetResource.getChild("jcr:content/metadata")).thenReturn(mockAssetResourceChild);
        when(mockAssetResourceChild.getValueMap()).thenReturn(mockValueMap);
    }
/**
 * Test case for fetchGameData
 * @throws RepositoryException
 * @throws ClassmagsMigrationBaseException
 * @throws LoginException
 */
    @Test
    public void fetchGameDataCaseOne() throws RepositoryException, ClassmagsMigrationBaseException, LoginException

    {
        when(CommonUtils.propertyCheck("title", mockNode)).thenReturn("");
        when(mockAsset.getName()).thenReturn(TITLE_NAME);
        when(mockAsset.getMetadataValue("dc:title")).thenReturn(DC_TITLE_META_DATA);
        String testSetTitle = StringUtils.defaultIfBlank(DC_TITLE_META_DATA, TITLE_NAME);
        when(CommonUtils.propertyCheck("description", mockNode)).thenReturn("");
        when(mockAsset.getMetadataValue(DamConstants.DC_DESCRIPTION)).thenReturn(TEST_DC_DESCRIPTION);
        when(CommonUtils.propertyCheck("fileReference", mockNode)).thenReturn("");
        gameData = gameService.fetchGameData(mockResource);

        assertEquals(testSetTitle, gameData.getTitle());
        assertEquals(TEST_DC_DESCRIPTION, gameData.getDescription());
        assertEquals(TEST_GAME_PATH, gameData.getGamePath());
        assertEquals(null, gameData.getGameType());
        assertEquals(null, gameData.getXMLPath());
        assertEquals(null, gameData.getContainerPath());
    }

    /**
     * Test case for fetchGameData when asset=null
     * 
     * @throws RepositoryException
     * @throws ClassmagsMigrationBaseException
     * @throws LoginException
     */
    @Test
    public void fetchGameDataCaseTwo() throws RepositoryException, ClassmagsMigrationBaseException, LoginException {
       
        when(CommonUtils.getResource(mockResourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE, ASSET_PATH))
                .thenReturn(null);

        gameData = gameService.fetchGameData(mockResource);
        assertNull(gameData.getTitle());
        assertNull(gameData.getDescription());
        assertNull(gameData.getThumbnailImg());
        assertNull(gameData.getGamePath());
        assertNull(gameData.getGameType());
        assertNull(gameData.getXMLPath());
        assertNull(gameData.getContainerPath());
    }
/**
 * Test case for fetchGameData
 * @throws RepositoryException
 * @throws LoginException
 * @throws ClassmagsMigrationBaseException
 */
    @Test
    public void fetchGameDataCaseThree() throws RepositoryException, LoginException, ClassmagsMigrationBaseException {

        when(CommonUtils.propertyCheck("title", mockNode)).thenReturn(TEST_DATA);
        when(CommonUtils.propertyCheck("description", mockNode)).thenReturn(TEST_DATA);
        when(CommonUtils.propertyCheck("fileReference", mockNode)).thenReturn("");
        
        when(mockResource.getResourceType()).thenReturn(RESOURCE_TYPE_FLASH_GAME);
        when(mockAssetResource.getChild("jcr:content/metadata")).thenReturn(mockAssetResourceChild);
        when(mockAssetResourceChild.getValueMap()).thenReturn(mockValueMap);
        when(mockValueMap.get("gameType", StringUtils.EMPTY)).thenReturn(GAME_TYPE);
        when(mockValueMap.get("xmlPath", StringUtils.EMPTY)).thenReturn(XML_PATH);
        gameData = gameService.fetchGameData(mockResource);
        
        assertEquals(TEST_DATA, gameData.getTitle());
        assertEquals(TEST_DATA, gameData.getDescription());
        assertEquals(GAME_PATH_1, gameData.getGamePath());
        assertEquals(GAME_TYPE, gameData.getGameType());
        assertEquals(XML_PATH, gameData.getXMLPath());
        assertEquals(CONTAINER_PATH, gameData.getContainerPath());
    }
/**
 * Test case for fetchGameData
 * @throws RepositoryException
 * @throws LoginException
 * @throws ClassmagsMigrationBaseException
 */
    @Test
    public void fetchGameDataCaseFour() throws RepositoryException, LoginException, ClassmagsMigrationBaseException {

        when(CommonUtils.propertyCheck("title", mockNode)).thenReturn(TEST_DATA);
        when(CommonUtils.propertyCheck("description", mockNode)).thenReturn(TEST_DATA);
        when(CommonUtils.propertyCheck("fileReference", mockNode)).thenReturn(TEST_DATA);
        when(mockNode.getName()).thenReturn(TEST_DATA);
        when(mockResource.getResourceType()).thenReturn(RESOURCE_TYPE_FLASH_GAME);
        when(mockValueMap.get("gameType", StringUtils.EMPTY)).thenReturn(GAME_TYPE);
        when(mockValueMap.get("xmlPath", StringUtils.EMPTY)).thenReturn("");

        gameData = gameService.fetchGameData(mockResource);

        assertEquals(TEST_DATA, gameData.getTitle());
        assertEquals(TEST_DATA, gameData.getDescription());
        assertEquals(TEST_DATA, gameData.getThumbnailImg());
        assertEquals(GAME_PATH_2, gameData.getGamePath());
        assertEquals(GAME_TYPE, gameData.getGameType());
        assertEquals(CONTAINER_PATH, gameData.getContainerPath());
    }
    
    /**
     * Test case for fetchGameData
     * @throws RepositoryException
     * @throws LoginException
     * @throws ClassmagsMigrationBaseException
     */
        @Test
        public void fetchGameDataCaseFive() throws RepositoryException, LoginException, ClassmagsMigrationBaseException {
         
            when(CommonUtils.propertyCheck("title", mockNode)).thenReturn(TEST_DATA);
            when(CommonUtils.propertyCheck("description", mockNode)).thenReturn(TEST_DATA);
            when(CommonUtils.propertyCheck("fileReference", mockNode)).thenReturn(TEST_DATA);
            when(mockNode.getName()).thenReturn(TEST_DATA);
            when(mockResource.getResourceType()).thenReturn(RESOURCE_TYPE_FLASH_GAME);
            when(mockValueMap.get("gameType", StringUtils.EMPTY)).thenReturn(GAME_TYPE);
            when(mockValueMap.get("xmlPath", StringUtils.EMPTY)).thenReturn(null);

            gameData = gameService.fetchGameData(mockResource);

            assertEquals(TEST_DATA, gameData.getTitle());
            assertEquals(TEST_DATA, gameData.getDescription());
            assertEquals(TEST_DATA, gameData.getThumbnailImg());
            assertEquals(GAME_PATH_2, gameData.getGamePath());
            assertEquals(GAME_TYPE, gameData.getGameType());
            assertEquals(CONTAINER_PATH, gameData.getContainerPath());
        }
}