package com.scholastic.classmags.migration.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;
import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.SearchFilter;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * JUnit for CommonUtil.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { CommonUtils.class, RoleUtil.class, Jwts.class } )
public class CommonUtilsTest {

    private static final String TEST_JWT_TOKEN = "header.payload.signature";
    private static final String TEST_USER_ID = "12401";
    private static final String TEST_SECRET_KEY = "secret_key";
    private static final String TEST_ISSUER = "classmags";
    private static final String ISSUE_DISPLAY_DATE = "Nov/Dec";
    private static final String DATE_FORMAT = "MMM yyyy";

    private Logger logger;

    private ResourceResolverFactory resourceResolverFactory;
    private ResourceResolver resourceResolver;
    private SlingHttpServletRequest request;
    private Resource resource;
    private ValueMap properties;
    private TagManager tagManager;

    @SuppressWarnings( "unchecked" )
    @Before
    public void setUp() throws LoginException {
        logger = mock( Logger.class );
        Whitebox.setInternalState( CommonUtils.class, "LOG", logger );

        PowerMockito.mockStatic( RoleUtil.class );

        resourceResolverFactory = mock( ResourceResolverFactory.class );
        resourceResolver = mock( ResourceResolver.class );
        resource = mock( Resource.class );
        properties = mock( ValueMap.class );
        tagManager = mock( TagManager.class );

        when( resourceResolverFactory.getServiceResourceResolver( any( HashMap.class ) ) )
                .thenReturn( resourceResolver );

    }

    @Test
    public void testGetResourceResolver() throws LoginException {

        // execute logic
        ResourceResolver resolver = CommonUtils.getResourceResolver( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );

        // verify logic
        assertNotNull( resolver );
    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testGetResourceResolverWithLoginException() throws LoginException {
        // setup logic (test-specific)
        LoginException e = new LoginException();
        ResourceResolverFactory resolverFactory = mock( ResourceResolverFactory.class );

        when( resolverFactory.getServiceResourceResolver( any( HashMap.class ) ) ).thenThrow( e );

        // execute logic (test-specific)
        ResourceResolver resolver = CommonUtils.getResourceResolver( resolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE );

        // verify logic
        verify( logger ).error( "Excepton in getResourceResolver method of CommonUtils::::" + e );
        assertNull( resolver );

    }

    @Test
    public void testGetSortingDate() {
        // setup logic (test-specific)
        Resource resource = mock( Resource.class );
        ValueMap properties = mock( ValueMap.class );
        Calendar dt = Calendar.getInstance();
        dt.set( Calendar.YEAR, 2016 );
        dt.set( Calendar.MONTH, 10 );
        dt.set( Calendar.DAY_OF_MONTH, 16 );
        Date date = dt.getTime();

        when( resource.adaptTo( ValueMap.class ) ).thenReturn( properties );

        when( properties.get( ClassMagsMigrationConstants.SORTING_DATE ) ).thenReturn( date );
        when( properties.get( ClassMagsMigrationConstants.SORTING_DATE, Date.class ) ).thenReturn( date );

        // execute logic (test-specific)
        String sortingDate = CommonUtils.getSortingDate( resource, DATE_FORMAT );

        // verify logic
        assertNotNull( sortingDate );
        assertEquals( "Nov 2016", sortingDate );

    }

    @Test
    public void testGetSortingDateWhenResourceIsNull() {

        // execute logic (test-specific)
        String sortingDate = CommonUtils.getSortingDate( ( Resource ) null, DATE_FORMAT );

        // verify logic
        assertTrue( sortingDate.isEmpty() );
    }

    @Test
    public void testGetSortingDateWhenSortingDateIsNull() {
        // setup logic (test-specific)
        ValueMap properties = mock( ValueMap.class );
        when( properties.get( ClassMagsMigrationConstants.SORTING_DATE ) ).thenReturn( null );
        when( properties.get( ClassMagsMigrationConstants.DISPLAY_DATE ) ).thenReturn( "November 16" );
        when( properties.get( ClassMagsMigrationConstants.DISPLAY_DATE, String.class ) ).thenReturn( "November 16" );

        // execute logic (test-specific)
        String sortingDate = CommonUtils.getSortingDate( properties, DATE_FORMAT );

        // verify logic
        assertEquals( "November 16", sortingDate );
        
    }

    @Test
    public void testGetSortingDateWhenBothDatesAreNull() {
        // setup logic (test-specific)
        ValueMap properties = mock( ValueMap.class );
        when( properties.get( ClassMagsMigrationConstants.SORTING_DATE ) ).thenReturn( null );
        when( properties.get( ClassMagsMigrationConstants.DISPLAY_DATE ) ).thenReturn( null );

        // execute logic (test-specific)
        String sortingDate = CommonUtils.getSortingDate( properties, DATE_FORMAT );

        // verify logic
        assertTrue( sortingDate.isEmpty() );
        
    }

    @Test
    public void testGetAssetSortingDate() {
        // setup logic (test-specific)
        Resource resource = mock( Resource.class );
        ValueMap properties = mock( ValueMap.class );
        Calendar dt = Calendar.getInstance();
        dt.set( Calendar.YEAR, 2016 );
        dt.set( Calendar.MONTH, 11 );
        dt.set( Calendar.DAY_OF_MONTH, 16 );
        Date date = dt.getTime();

        when( resource.adaptTo( ValueMap.class ) ).thenReturn( properties );

        when( properties.get( ClassMagsMigrationConstants.ASSET_SORTING_DATE ) ).thenReturn( date );
        when( properties.get( ClassMagsMigrationConstants.ASSET_SORTING_DATE, Date.class ) ).thenReturn( date );

        // execute logic (test-specific)
        String assetSortingDate = CommonUtils.getAssetSortingDate( resource, DATE_FORMAT );

        // verify logic
        assertNotNull( assetSortingDate );
        assertEquals( "Dec 2016", assetSortingDate );

    }

    @Test
    public void testGetAssetSortingDateWhenResourceIsNull() {

        // execute logic (test-specific)
        String assetSortingDate = CommonUtils.getAssetSortingDate( ( Resource ) null, DATE_FORMAT );

        // verify logic
        assertTrue( assetSortingDate.isEmpty() );
    }

    @Test
    public void testGetAssetSortingDateWhenSortingDateIsNull() {
        // setup logic (test-specific)
        ValueMap properties = mock( ValueMap.class );
        when( properties.get( ClassMagsMigrationConstants.ASSET_SORTING_DATE ) ).thenReturn( null );
        when( properties.get( ClassMagsMigrationConstants.ASSET_DISPLAY_DATE ) ).thenReturn( "December 16" );
        when( properties.get( ClassMagsMigrationConstants.ASSET_DISPLAY_DATE, StringUtils.EMPTY ) )
                .thenReturn( "December 16" );

        // execute logic (test-specific)
        String sortingDate = CommonUtils.getAssetSortingDate( properties, DATE_FORMAT );

        // verify logic
        assertEquals( "December 16", sortingDate );
        ;
    }

    @Test
    public void testGetAssetSortingDateWhenBothDatesAreNull() {
        // setup logic (test-specific)
        ValueMap properties = mock( ValueMap.class );
        when( properties.get( ClassMagsMigrationConstants.ASSET_SORTING_DATE ) ).thenReturn( null );
        when( properties.get( ClassMagsMigrationConstants.ASSET_DISPLAY_DATE ) ).thenReturn( null );

        // execute logic (test-specific)
        String sortingDate = CommonUtils.getAssetSortingDate( properties, DATE_FORMAT );

        // verify logic
        assertTrue( sortingDate.isEmpty() );
        ;
    }

    @Test
    public void testGetSortingDateWhenDateFormatIsNull() {
        // setup logic (test-specific)
        ValueMap properties = mock( ValueMap.class );
        Calendar dt = Calendar.getInstance();
        dt.set( Calendar.YEAR, 2016 );
        dt.set( Calendar.MONTH, 10 );
        dt.set( Calendar.DAY_OF_MONTH, 16 );
        Date date = dt.getTime();

        when( properties.get( ClassMagsMigrationConstants.SORTING_DATE ) ).thenReturn( date );
        when( properties.get( ClassMagsMigrationConstants.SORTING_DATE, Date.class ) ).thenReturn( date );

        // execute logic (test-specific)
        String sortingDate = CommonUtils.getSortingDate( properties, null );

        // verify logic
        assertEquals( "November 16, 2016", sortingDate );
        ;
    }

    @Test
    public void testGetSortingDateWhenDateFormatIsNotAvailable() {
        // setup logic (test-specific)
        ValueMap properties = mock( ValueMap.class );
        Calendar dt = Calendar.getInstance();
        dt.set( Calendar.YEAR, 2016 );
        dt.set( Calendar.MONTH, 10 );
        dt.set( Calendar.DAY_OF_MONTH, 16 );
        Date date = dt.getTime();

        when( properties.get( ClassMagsMigrationConstants.SORTING_DATE ) ).thenReturn( date );
        when( properties.get( ClassMagsMigrationConstants.SORTING_DATE, Date.class ) ).thenReturn( date );

        // execute logic (test-specific)
        String sortingDate = CommonUtils.getSortingDate( properties, ClassMagsMigrationConstants.NO_DATA_AVAILABLE );

        // verify logic
        assertEquals( "November 16, 2016", sortingDate );
        ;
    }

    @Test
    public void testFetchPropertyValue() {
        // setup logic (test-specific)
        when( resourceResolver.getResource(
                "/content/classroom_magazines/scienceworld/issues/2016-17/090516/pup-on-patrol/jcr:content" ) )
                        .thenReturn( resource );
        when( resource.getValueMap() ).thenReturn( properties );
        when( properties.get( "jcr:title", StringUtils.EMPTY ) ).thenReturn( "Pups on Patrol" );

        // execute logic (test-specific)
        String pagePropertyValue = CommonUtils.fetchPagePropertyValue( "jcr:title",
                "/content/classroom_magazines/scienceworld/issues/2016-17/090516/pup-on-patrol",
                resourceResolverFactory );

        // verify logic
        assertEquals( "Pups on Patrol", pagePropertyValue );
    }

    @Test
    public void testFetchPropertyValueWhenResourceIsNull() {
        // setup logic (test-specific)

        when( resourceResolver.getResource(
                "/content/classroom_magazines/scienceworld/issues/2016-17/090516/pup-on-patrol/jcr:content" ) )
                        .thenReturn( null );

        // execute logic (test-specific)
        String pagePropertyValue = CommonUtils.fetchPagePropertyValue( "jcr:title",
                "/content/classroom_magazines/scienceworld/issues/2016-17/090516/pup-on-patrol",
                resourceResolverFactory );

        // verify logic
        assertNull( pagePropertyValue );
    }

    @Test
    public void testFetchPropertyMap() {
        // setup logic (test-specific)
        when( resourceResolver.getResource(
                "/content/classroom_magazines/scienceworld/issues/2016-17/090516/pup-on-patrol/jcr:content" ) )
                        .thenReturn( resource );
        when( resource.getValueMap() ).thenReturn( properties );

        // execute logic (test-specific)
        ValueMap pagePropertyMap = CommonUtils.fetchPagePropertyMap(
                "/content/classroom_magazines/scienceworld/issues/2016-17/090516/pup-on-patrol",
                resourceResolverFactory );

        // verify logic
        assertNotNull( pagePropertyMap );
    }

    @Test
    public void testFetchPropertyMapWhenResourceIsNull() {
        // setup logic (test-specific)
        when( resourceResolver.getResource(
                "/content/classroom_magazines/scienceworld/issues/2016-17/090516/pup-on-patrol/jcr:content" ) )
                        .thenReturn( null );

        // execute logic (test-specific)
        ValueMap pagePropertyMap = CommonUtils.fetchPagePropertyMap(
                "/content/classroom_magazines/scienceworld/issues/2016-17/090516/pup-on-patrol",
                resourceResolverFactory );

        // verify logic
        assertNull( pagePropertyMap );
    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testFetchMultifieldData() {
        // setup logic (test-specific)
        Iterator< Resource > resourceIterator = mock( Iterator.class );
        Resource resource1 = mock( Resource.class );
        Resource resource2 = mock( Resource.class );
        ValueMap properties1 = mock( ValueMap.class );
        ValueMap properties2 = mock( ValueMap.class );

        when( resource.listChildren() ).thenReturn( resourceIterator );
        when( resourceIterator.hasNext() ).thenReturn( true, true, false );
        when( resourceIterator.next() ).thenReturn( resource1, resource2 );
        when( resource1.getValueMap() ).thenReturn( properties1 );
        when( resource2.getValueMap() ).thenReturn( properties2 );

        // execute logic
        List< ValueMap > multifieldData = CommonUtils.fetchMultiFieldData( resource );

        // verify logic
        assertNotNull( multifieldData );
        assertEquals( 2, multifieldData.size() );

    }

    @Test
    public void testFetchMultifieldDataWhenResourceIsNull() {
        // execute logic
        List< ValueMap > multifieldData = CommonUtils.fetchMultiFieldData( null );

        // verify logic
        assertTrue( multifieldData.isEmpty() );

    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testFetchMultifieldDataWithNodeName() {
        // setup logic (test-specific)
        Iterator< Resource > resourceIterator = mock( Iterator.class );
        Resource multifieldResource = mock( Resource.class );
        Resource resource1 = mock( Resource.class );
        Resource resource2 = mock( Resource.class );
        ValueMap properties1 = mock( ValueMap.class );
        ValueMap properties2 = mock( ValueMap.class );

        when( resource.getResourceResolver() ).thenReturn( resourceResolver );
        when( resource.getPath() ).thenReturn(
                "/content/classroom_magazines/admin/sw-data-page/jcr:content/data-page-par/global_footer_contai/global-footer-par/global_footer_config" );
        when( resourceResolver.getResource(
                "/content/classroom_magazines/admin/sw-data-page/jcr:content/data-page-par/global_footer_contai/global-footer-par/global_footer_config/footer-links" ) )
                        .thenReturn( multifieldResource );

        when( multifieldResource.listChildren() ).thenReturn( resourceIterator );
        when( resourceIterator.hasNext() ).thenReturn( true, true, false );
        when( resourceIterator.next() ).thenReturn( resource1, resource2 );
        when( resource1.getValueMap() ).thenReturn( properties1 );
        when( resource2.getValueMap() ).thenReturn( properties2 );

        // execute logic
        List< ValueMap > multifieldData = CommonUtils.fetchMultiFieldData( resource, "footer-links" );

        // verify logic
        assertNotNull( multifieldData );
        assertEquals( 2, multifieldData.size() );

    }

    @Test
    public void testFetchMultifieldDataWithNodeNameWhenMultifieldResourceIsNull() {
        // setup logic (test-specific)
        when( resource.getResourceResolver() ).thenReturn( resourceResolver );
        when( resource.getPath() ).thenReturn(
                "/content/classroom_magazines/admin/sw-data-page/jcr:content/data-page-par/global_footer_contai/global-footer-par/global_footer_config" );
        when( resourceResolver.getResource(
                "/content/classroom_magazines/admin/sw-data-page/jcr:content/data-page-par/global_footer_contai/global-footer-par/global_footer_config/footer-links" ) )
                        .thenReturn( null );

        // execute logic
        List< ValueMap > multifieldData = CommonUtils.fetchMultiFieldData( resource, "footer-links" );

        // verify logic
        assertTrue( multifieldData.isEmpty() );
    }

    @Test
    public void testGetGlobalFooterPath() {
        // setup logic (test-specific)
        ResourcePathConfigService resourcePathConfigService = mock( ResourcePathConfigService.class );
        when( resourcePathConfigService.getFooterConfigResourcePath( "scienceworld" ) ).thenReturn(
                "/content/classroom_magazines/admin/sw-data-page/jcr:content/data-page-par/global_footer_contai" );

        // execute logic
        String globalFooterPath = CommonUtils.getGlobalFooterPath( resourcePathConfigService, "scienceworld" );

        // verify logic
        assertEquals( "/content/classroom_magazines/admin/sw-data-page/jcr:content/data-page-par/global_footer_contai",
                globalFooterPath );
    }

    @Test
    public void testGetResource() {
        // setup logic (test-specific)
        when( resourceResolver.getResource( "/content/classroom_magazines/admin/sw-data-page/jcr:content" ) )
                .thenReturn( resource );

        // execute logic
        Resource testResource = CommonUtils.getResource( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE,
                "/content/classroom_magazines/admin/sw-data-page/jcr:content" );

        // verify logic
        assertNotNull( testResource );
    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testGetResourceWhenResourceResolverIsNull() throws LoginException {
        // setup logic (test-specific)
        when( resourceResolverFactory.getServiceResourceResolver( any( HashMap.class ) ) ).thenReturn( null );

        // execute logic
        Resource testResource = CommonUtils.getResource( resourceResolverFactory,
                ClassMagsMigrationConstants.READ_SERVICE,
                "/content/classroom_magazines/admin/sw-data-page/jcr:content" );

        // verify logic
        assertNull( testResource );
    }

    @Test
    public void testGetTags() {
        // setup logic ( test-specific)
        Tag[] tags = createTagList();
        when( tagManager.getTags( resource ) ).thenReturn( tags );

        // execute logic
        List< String > tagList = CommonUtils.getTags( resource, tagManager );

        // verify logic
        assertEquals( 4, tagList.size() );
        assertEquals( "Science World", tagList.get( 0 ) );
        assertEquals( "Article", tagList.get( 1 ) );
        assertEquals( "Biology", tagList.get( 2 ) );
    }

    @Test
    public void testGetSubjectTags() {
        // setup logic ( test-specific)
        Tag[] tags = createTagList();
        when( tagManager.getTags( resource ) ).thenReturn( tags );

        // execute logic
        List< String > subjectTags = CommonUtils.getSubjectTags( resource, tagManager );

        // verify logic
        assertEquals( 1, subjectTags.size() );
        assertEquals( "Biology", subjectTags.get( 0 ) );
    }

    @Test
    public void testGetContentTypeTags() {
        // setup logic ( test-specific)
        Tag[] tags = createTagList();
        when( tagManager.getTags( resource ) ).thenReturn( tags );

        // execute logic
        String contentTypeTag = CommonUtils.getContentTypeTag( resource, tagManager );

        // verify logic
        assertEquals( "Article", contentTypeTag );
    }

    @Test
    public void testGetContentTypeTagsWhenNoTagsPresent() {
        // setup logic ( test-specific)
        Tag[] tags = new Tag[] {};
        when( tagManager.getTags( resource ) ).thenReturn( tags );

        // execute logic
        String contentTypeTag = CommonUtils.getContentTypeTag( resource, tagManager );

        // verify logic
        assertNull( contentTypeTag );
    }

    @SuppressWarnings( "deprecation" )
    @Test
    public void testGetAdminResourceResolver() throws LoginException {
        // setup logic (test-specific)
        when( resourceResolverFactory.getAdministrativeResourceResolver( null ) ).thenReturn( resourceResolver );

        // execute logic
        ResourceResolver resourceResolver = CommonUtils.getAdminResourceResolver( resourceResolverFactory );

        // verify logic
        assertNotNull( resourceResolver );
    }

    @SuppressWarnings( "deprecation" )
    @Test
    public void testGetAdminResourceResolverWithException() throws LoginException {
        // setup logic (test-specific)
        LoginException e = new LoginException();
        when( resourceResolverFactory.getAdministrativeResourceResolver( null ) ).thenThrow( e );

        // execute logic
        CommonUtils.getAdminResourceResolver( resourceResolverFactory );

        // verify logic
        verify( logger ).error( "Exception in getAdminResourceResolver::::" + e );
    }

    @SuppressWarnings( "deprecation" )
    @Test
    public void testGetTagManager() throws LoginException {
        // setup logic (test-specific)
        when( resourceResolverFactory.getAdministrativeResourceResolver( null ) ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( TagManager.class ) ).thenReturn( tagManager );

        // execute logic
        TagManager tagManager = CommonUtils.getTagManager( resourceResolverFactory );

        // verify logic
        assertNotNull( tagManager );
    }

    @Test
    public void testGetGlobalConfigProperty() throws LoginException {
        // setup logic (test-specific)
        ResourcePathConfigService resourcePathConfigService = mock( ResourcePathConfigService.class );
        PropertyConfigService propertyConfigService = mock( PropertyConfigService.class );

        when( resourcePathConfigService.getGlobalConfigResourcePath( "scienceworld" ) ).thenReturn(
                "/content/classroom_magazines/admin/sw-data-page/jcr:content/data-page-par/global-config" );
        when( propertyConfigService.getDataConfigProperty( "hmPageLoggedOut",
                "/content/classroom_magazines/admin/sw-data-page/jcr:content/data-page-par/global-config" ) )
                        .thenReturn( "/content/classroom_magazines/scienceworld/home-page-logged-out" );

        // execute logic
        String globalConfigProperty = CommonUtils.getGlobalConfigProperty( propertyConfigService,
                resourcePathConfigService, "hmPageLoggedOut", "scienceworld" );

        // verify logic
        assertEquals( "/content/classroom_magazines/scienceworld/home-page-logged-out", globalConfigProperty );
    }

    @Test
    public void testGetSession() {
        // setup logic (test-specific)
        Session mockSession = mock( Session.class );
        when( resourceResolver.adaptTo( Session.class ) ).thenReturn( mockSession );

        // execute logic
        Session session = CommonUtils.getSession( resourceResolver );

        // verify logic
        assertNotNull( session );
    }

    @Test
    public void testGetSessionWithException() {
        // setup logic ( test-specific )
        NullPointerException e = new NullPointerException();
        when( resourceResolver.adaptTo( Session.class ) ).thenThrow( e );

        // execute logic
        Session session = CommonUtils.getSession( resourceResolver );

        // verify logic
        verify( logger ).error( "Error Occured : ", e );
        assertNull( session );
    }

    @Test
    public void testPropertyCheck() throws RepositoryException {
        // setup logic (test-specific)
        Node testNode = mock( Node.class );
        Property property = mock( Property.class );
        when( testNode.hasProperty( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG ) ).thenReturn( true );
        when( testNode.getProperty( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG ) ).thenReturn( property );
        when( property.getString() ).thenReturn( ClassMagsMigrationConstants.ARCHIVE_PAGE_TEMPLATE_PATH );

        // execute logic
        String templatePath = CommonUtils.propertyCheck( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, testNode );

        // verify logic
        assertEquals( ClassMagsMigrationConstants.ARCHIVE_PAGE_TEMPLATE_PATH, templatePath );
    }

    @Test
    public void testPropertyCheckWhenNodeIsNull() {
        // execute logic
        String templatePath = CommonUtils.propertyCheck( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, null );

        // verify logic
        assertTrue( templatePath.isEmpty() );
    }

    @Test
    public void testPropertyCheckWhenPropertyNotPresent() throws RepositoryException {
        // setup logic ( test-specific )
        Node testNode = mock( Node.class );
        when( testNode.hasProperty( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG ) ).thenReturn( false );

        // execute logic
        String templatePath = CommonUtils.propertyCheck( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, testNode );

        // verify logic
        assertTrue( templatePath.isEmpty() );
    }

    @Test
    public void testPropertyCheckWhenValueFormatExceptionOccurs() throws RepositoryException {
        // setup logic ( test-specific )
        ValueFormatException valueFormatException = new ValueFormatException();
        Node testNode = mock( Node.class );
        Property property = mock( Property.class );

        when( testNode.hasProperty( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG ) ).thenReturn( true );
        when( testNode.getProperty( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG ) ).thenReturn( property );
        when( property.getString() ).thenThrow( valueFormatException );

        // execute logic
        String templatePath = CommonUtils.propertyCheck( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, testNode );

        // verify logic
        verify( logger ).error( "ValueFormatException:: ", valueFormatException );
        assertTrue( templatePath.isEmpty() );
    }

    @Test
    public void testPropertyCheckWhenRepositoryExceptionOccurs() throws RepositoryException {
        // setup logic ( test-specific )
        RepositoryException repositoryException = new RepositoryException();
        Node testNode = mock( Node.class );

        when( testNode.hasProperty( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG ) ).thenThrow( repositoryException );

        // execute logic
        String templatePath = CommonUtils.propertyCheck( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, testNode );

        // verify logic
        verify( logger ).error( "RepositoryException:: ", repositoryException );
        assertTrue( templatePath.isEmpty() );
    }

    @Test
    public void testGetFormattedSubjectTags() {
        // setup logic ( test-specific )
        List< String > subjectTags = new ArrayList< >();
        subjectTags.add( "Biology" );
        subjectTags.add( "Earth Science" );
        subjectTags.add( "Maths" );

        // execute logic
        String formattedTags = CommonUtils.getFormattedSubjectTags( subjectTags );

        // verify logic
        assertEquals( "Biology, Earth Science, Maths", formattedTags );

    }

    @Test
    public void testGetFormattedSubjectTagsWhenTagIsEmpty() {
        // setup logic ( test-specific )
        List< String > subjectTags = new ArrayList< >();

        // execute logic
        String formattedTags = CommonUtils.getFormattedSubjectTags( subjectTags );

        // verify logic
        assertTrue( formattedTags.isEmpty() );

    }

    @Test
    public void testGetSearchFilters() {
        // setup logic ( test-specific )
        @SuppressWarnings( "unchecked" )
        Iterator< Tag > tagIterator = mock( Iterator.class );
        Tag scienceWorldTag = mockTag( "Science World", "/etc/tags/classroom-magazines/scienceworld", "scienceworld",
                "classroom-magazines:scienceworld" );
        Tag biologyTag = mockTag( "Biology", "/etc/tags/classroom-magazines/scienceworld/biology", "biology",
                "classroom-magazines:scienceworld/biology" );
        Tag chemistryTag = mockTag( "Chemistry", "/etc/tags/classroom-magazines/scienceworld/chemistry", "chemistry",
                "classroom-magazines:scienceworld/chemistry" );
        when( resourceResolver.adaptTo( TagManager.class ) ).thenReturn( tagManager );
        when( tagManager.resolve( "/etc/tags/classroom-magazines/scienceworld" ) ).thenReturn( scienceWorldTag );
        when( scienceWorldTag.listChildren() ).thenReturn( tagIterator );
        when( tagIterator.hasNext() ).thenReturn( true, true, false );
        when( tagIterator.next() ).thenReturn( biologyTag, chemistryTag );

        // execute logic
        List< SearchFilter > searchFilters = CommonUtils.getSearchFilters( resourceResolver,
                "/etc/tags/classroom-magazines/scienceworld" );

        // verify logic
        assertNotNull( searchFilters );
        assertEquals( 2, searchFilters.size() );
        assertEquals( "Biology", searchFilters.get( 0 ).getContentType() );
        assertEquals( "Chemistry", searchFilters.get( 1 ).getContentType() );
    }

    @Test
    public void testGetSearchFiltersWhenTagIsNull() {
        // setup logic ( test-specific )
        when( resourceResolver.adaptTo( TagManager.class ) ).thenReturn( tagManager );
        when( tagManager.resolve( "/etc/tags/classroom-magazines/scienceworld" ) ).thenReturn( null );

        // execute logic
        List< SearchFilter > searchFilters = CommonUtils.getSearchFilters( resourceResolver,
                "/etc/tags/classroom-magazines/scienceworld" );

        // verify logic
        assertTrue( searchFilters.isEmpty() );
    }

    @Test
    public void testGetSearchFiltersWhenResourceResolverIsNull() {
        // execute logic
        List< SearchFilter > searchFilters = CommonUtils.getSearchFilters( null,
                "/etc/tags/classroom-magazines/scienceworld" );

        // verify logic
        assertTrue( searchFilters.isEmpty() );
    }

    @Test
    public void testGetContentTypeSearchFilter() {
        // setup logic ( test-specific )
        @SuppressWarnings( "unchecked" )
        Iterator< Resource > resourceIterator = mock( Iterator.class );
        Resource videoResource = mock( Resource.class );
        Resource articleResource = mock( Resource.class );

        SearchFilter video = new SearchFilter();
        video.setContentType( "Video" );
        video.setUserType( "student" );

        SearchFilter article = new SearchFilter();
        article.setContentType( "Article" );
        article.setUserType( "teacher" );

        request = mock( SlingHttpServletRequest.class );

        when( resource.hasChildren() ).thenReturn( true );
        when( resource.listChildren() ).thenReturn( resourceIterator );
        when( resourceIterator.hasNext() ).thenReturn( true, true, false );
        when( resourceIterator.next() ).thenReturn( videoResource, articleResource );
        when( videoResource.adaptTo( SearchFilter.class ) ).thenReturn( video );
        when( articleResource.adaptTo( SearchFilter.class ) ).thenReturn( article );
        when( RoleUtil.getUserRole( request ) ).thenReturn( "student" );
        when( RoleUtil.shouldRender( "student", "student" ) ).thenReturn( true );
        when( RoleUtil.shouldRender( "student", "teacher" ) ).thenReturn( false );

        // execute logic
        List< SearchFilter > contentTypeFilters = CommonUtils.getContentTypeFilters( resource, request, false );

        // verify logic
        assertEquals( 2, contentTypeFilters.size() );
        assertEquals( "Video", contentTypeFilters.get( 0 ).getContentType() );
        assertEquals( "Article", contentTypeFilters.get( 1 ).getContentType() );

        assertTrue( contentTypeFilters.get( 0 ).getRender() );
        assertFalse( contentTypeFilters.get( 1 ).getRender() );
    }

    @Test
    public void testGetContentTypeSearchFilterWhenResourceIsNull() {
        // execute logic
        List< SearchFilter > contentTypeFilters = CommonUtils.getContentTypeFilters( null, request, false );

        // verify logic
        assertTrue( contentTypeFilters.isEmpty() );
    }

    @Test
    public void testGetContentTypeSearchFilterWhenChildNodesNotPresent() {
        // setup logic( test-specific )
        when( resource.hasChildren() ).thenReturn( false );

        // execute logic
        List< SearchFilter > contentTypeFilters = CommonUtils.getContentTypeFilters( resource, request, false );

        // verify logic
        assertTrue( contentTypeFilters.isEmpty() );
    }

    @Test
    public void testGetContentTypeSearchFilterWhenFilterIsNull() {
        // setup logic ( test-specific )
        @SuppressWarnings( "unchecked" )
        Iterator< Resource > resourceIterator = mock( Iterator.class );
        Resource videoResource = mock( Resource.class );

        when( resource.hasChildren() ).thenReturn( true );
        when( resource.listChildren() ).thenReturn( resourceIterator );
        when( resourceIterator.hasNext() ).thenReturn( true, false );
        when( resourceIterator.next() ).thenReturn( videoResource );
        when( videoResource.adaptTo( SearchFilter.class ) ).thenReturn( null );

        // execute logic
        List< SearchFilter > contentTypeFilters = CommonUtils.getContentTypeFilters( resource, request, false );

        // verify logic
        assertTrue( contentTypeFilters.isEmpty() );
    }

    @Test
    public void testGetContentTypeSearchFilterInAuthoringMode() {
        // setup logic ( test-specific )
        @SuppressWarnings( "unchecked" )
        Iterator< Resource > resourceIterator = mock( Iterator.class );
        Resource videoResource = mock( Resource.class );
        Resource articleResource = mock( Resource.class );

        SearchFilter video = new SearchFilter();
        video.setContentType( "Video" );
        video.setUserType( "student" );

        SearchFilter article = new SearchFilter();
        article.setContentType( "Article" );
        article.setUserType( "teacher" );

        request = mock( SlingHttpServletRequest.class );

        when( resource.hasChildren() ).thenReturn( true );
        when( resource.listChildren() ).thenReturn( resourceIterator );
        when( resourceIterator.hasNext() ).thenReturn( true, true, false );
        when( resourceIterator.next() ).thenReturn( videoResource, articleResource );
        when( videoResource.adaptTo( SearchFilter.class ) ).thenReturn( video );
        when( articleResource.adaptTo( SearchFilter.class ) ).thenReturn( article );
        when( RoleUtil.getUserRole( request ) ).thenReturn( "student" );
        when( RoleUtil.shouldRender( "student", "student" ) ).thenReturn( true );
        when( RoleUtil.shouldRender( "student", "teacher" ) ).thenReturn( false );

        // execute logic
        List< SearchFilter > contentTypeFilters = CommonUtils.getContentTypeFilters( resource, request, true );

        // verify logic
        assertEquals( 2, contentTypeFilters.size() );
        assertEquals( "Video", contentTypeFilters.get( 0 ).getContentType() );
        assertEquals( "Article", contentTypeFilters.get( 1 ).getContentType() );

        assertTrue( contentTypeFilters.get( 0 ).getRender() );
        assertTrue( contentTypeFilters.get( 1 ).getRender() );
    }

    @Test
    public void testCreateJwtToken() {
        // setup logic
        JwtBuilder jwtBuilder = mock( JwtBuilder.class );
        PowerMockito.mockStatic( Jwts.class );

        byte[] secretKey = TEST_SECRET_KEY.getBytes( java.nio.charset.StandardCharsets.UTF_8 );

        when( Jwts.builder() ).thenReturn( jwtBuilder );
        when( jwtBuilder.claim( "userId", TEST_USER_ID ) ).thenReturn( jwtBuilder );
        when( jwtBuilder.setExpiration( any( Date.class ) ) ).thenReturn( jwtBuilder );
        when( jwtBuilder.setSubject( TEST_USER_ID ) ).thenReturn( jwtBuilder );
        when( jwtBuilder.setIssuer( "classmags" ) ).thenReturn( jwtBuilder );
        when( jwtBuilder.signWith( SignatureAlgorithm.HS512, secretKey ) ).thenReturn( jwtBuilder );
        when( jwtBuilder.compact() ).thenReturn( TEST_JWT_TOKEN );

        // execute logic
        String jwtToken = CommonUtils.createJwtToken( TEST_USER_ID, TEST_ISSUER, TEST_SECRET_KEY );

        // verify logic
        assertEquals( TEST_JWT_TOKEN, jwtToken );
    }

    @Test
    public void testGetDateFormat() throws ClassmagsMigrationBaseException, LoginException {
        // setup logic
        PropertyConfigService propertyConfigService = mock( PropertyConfigService.class );

        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                "/content/classroom_magazines/admin/sw-data-page" ) ).thenReturn( "MMM yyyy" );

        // execute logic
        String dateFormat = CommonUtils.getDateFormat( propertyConfigService,
                "/content/classroom_magazines/admin/sw-data-page" );

        // verify logic
        assertEquals( "MMM yyyy", dateFormat );
    }

    @Test
    public void testGetDateFormatWhenNoDataIsAvailable() throws ClassmagsMigrationBaseException, LoginException {
        // setup logic
        PropertyConfigService propertyConfigService = mock( PropertyConfigService.class );

        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                "/content/classroom_magazines/admin/sw-data-page" ) )
                        .thenReturn( ClassMagsMigrationConstants.NO_DATA_AVAILABLE );

        // execute logic
        String dateFormat = CommonUtils.getDateFormat( propertyConfigService,
                "/content/classroom_magazines/admin/sw-data-page" );

        // verify logic
        assertEquals( ClassMagsMigrationConstants.DEFAULT_DATE_FORMAT, dateFormat );
    }

    @Test( expected = ClassmagsMigrationBaseException.class )
    public void testGetDateFormatWhenLoginExceptionIsThrown() throws ClassmagsMigrationBaseException, LoginException {
        // setup logic
        PropertyConfigService propertyConfigService = mock( PropertyConfigService.class );
        LoginException e = new LoginException();
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                "/content/classroom_magazines/admin/sw-data-page" ) ).thenThrow( e );

        // execute logic
        String dateFormat = CommonUtils.getDateFormat( propertyConfigService,
                "/content/classroom_magazines/admin/sw-data-page" );

        // verify logic
        assertEquals( "MMM yyyy", dateFormat );
    }

    @Test
    public void testGenerateUniqueIdentifer() {
        // setup logic
        PowerMockito.mockStatic( UUID.class );
        UUID uuid = new UUID( new Long( "123456789" ), new Long( "123456789" ) );
        when( UUID.randomUUID() ).thenReturn( uuid );

        // execute logic
        String uniqueIdentifier = CommonUtils.generateUniqueIdentifier();

        // verify logic
        assertEquals( uuid.toString(), uniqueIdentifier );
    }

    @Test
    public void testAddDelimiter() {
        // execute logic
        String text = CommonUtils.addDelimiter( "testInputData" );

        // verify logic
        assertEquals( ClassMagsMigrationConstants.DELIMITER + "testInputData" + ClassMagsMigrationConstants.DELIMITER,
                text );
    }

    @Test
    public void testIsAuthorMode() {
        // setup logic ( test-specific )
        Set< String > runModes = new HashSet< >();
        runModes.add( "author" );

        // execute logic
        boolean isAuthor = CommonUtils.isAuthorMode( runModes );

        // verify logic
        assertTrue( isAuthor );
    }

    @Test
    public void testIsAuthorModeForPublish() {
        // setup logic ( test-specific )
        Set< String > runModes = new HashSet< >();
        runModes.add( "publish" );

        // execute logic
        boolean isAuthor = CommonUtils.isAuthorMode( runModes );

        // verify logic
        assertFalse( isAuthor );
    }
    
    @Test
    public void testGetDisplayDateFromResource() {
        // setup logic ( test-specific )
        when( resource.adaptTo( ValueMap.class ) ).thenReturn( properties );
        when( properties.get( ClassMagsMigrationConstants.DISPLAY_DATE, String.class ) )
                .thenReturn( ISSUE_DISPLAY_DATE );

        // execute logic
        String displayDate = CommonUtils.getDisplayDate( resource, DATE_FORMAT );

        // verify logic
        assertEquals( ISSUE_DISPLAY_DATE, displayDate );
    }
    
    @Test
    public void testGetDisplayDateWhenResourceIsNull() {

        // execute logic
        String displayDate = CommonUtils.getDisplayDate( ( Resource ) null, DATE_FORMAT );

        // verify logic
        assertTrue( displayDate.isEmpty() );
    }
    
    @Test
    public void testGetDisplayDate() {
        // setup logic ( test-specific )
        when( properties.get( ClassMagsMigrationConstants.DISPLAY_DATE, String.class ) )
                .thenReturn( ISSUE_DISPLAY_DATE );

        // execute logic
        String displayDate = CommonUtils.getDisplayDate( properties, DATE_FORMAT );

        // verify logic
        assertEquals( ISSUE_DISPLAY_DATE, displayDate );
    }
    
    @Test
    public void testGetDisplayDateWhenDisplayDateIsNull() {
        // setup logic ( test-specific )
        when( properties.get( ClassMagsMigrationConstants.DISPLAY_DATE, String.class ) ).thenReturn( null );
        Calendar dt = Calendar.getInstance();
        dt.set( Calendar.YEAR, 2016 );
        dt.set( Calendar.MONTH, 10 );
        dt.set( Calendar.DAY_OF_MONTH, 16 );
        Date date = dt.getTime();
        when( properties.get( ClassMagsMigrationConstants.SORTING_DATE ) ).thenReturn( date );
        when( properties.get( ClassMagsMigrationConstants.SORTING_DATE, Date.class ) ).thenReturn( date );

        // execute logic
        String displayDate = CommonUtils.getDisplayDate( properties, DATE_FORMAT );

        // verify logic
        assertEquals( "Nov 2016", displayDate );
    }
    
    @Test
    public void testGetDisplayDateWhenDisplayAndSortingDateAreNull() {
        // setup logic ( test-specific )
        when( properties.get( ClassMagsMigrationConstants.DISPLAY_DATE, String.class ) ).thenReturn( null );
        when( properties.get( ClassMagsMigrationConstants.SORTING_DATE ) ).thenReturn( null );

        // execute logic
        String displayDate = CommonUtils.getDisplayDate( properties, DATE_FORMAT );

        // verify logic
        assertTrue( displayDate.isEmpty() );
    }

    @Test
    public void testGetAssetDisplayDateFromResource() {
        // setup logic ( test-specific )
        when( resource.adaptTo( ValueMap.class ) ).thenReturn( properties );
        when( properties.get( ClassMagsMigrationConstants.ASSET_DISPLAY_DATE, String.class ) )
                .thenReturn( ISSUE_DISPLAY_DATE );

        // execute logic
        String displayDate = CommonUtils.getAssetDisplayDate( resource, DATE_FORMAT );

        // verify logic
        assertEquals( ISSUE_DISPLAY_DATE, displayDate );
    }
    
    @Test
    public void testGetAssetDisplayDateWhenResourceIsNull() {

        // execute logic
        String displayDate = CommonUtils.getAssetDisplayDate( ( Resource ) null, DATE_FORMAT );

        // verify logic
        assertTrue( displayDate.isEmpty() );
    }
    
    @Test
    public void testGetAssetDisplayDate() {
        // setup logic ( test-specific )
        when( properties.get( ClassMagsMigrationConstants.ASSET_DISPLAY_DATE, String.class ) )
                .thenReturn( ISSUE_DISPLAY_DATE );

        // execute logic
        String displayDate = CommonUtils.getAssetDisplayDate( properties, DATE_FORMAT );

        // verify logic
        assertEquals( ISSUE_DISPLAY_DATE, displayDate );
    }
    
    @Test
    public void testGetAssetDisplayDateWhenDisplayDateIsNull(){
        // setup logic ( test-specific )
        when( properties.get( ClassMagsMigrationConstants.DISPLAY_DATE, String.class ) ).thenReturn( null );
        Calendar dt = Calendar.getInstance();
        dt.set( Calendar.YEAR, 2016 );
        dt.set( Calendar.MONTH, 10 );
        dt.set( Calendar.DAY_OF_MONTH, 16 );
        Date date = dt.getTime();
        when( properties.get( ClassMagsMigrationConstants.ASSET_SORTING_DATE ) ).thenReturn( date );
        when( properties.get( ClassMagsMigrationConstants.ASSET_SORTING_DATE, Date.class ) ).thenReturn( date );

        // execute logic
        String displayDate = CommonUtils.getAssetDisplayDate( properties, DATE_FORMAT );

        // verify logic
        assertEquals( "Nov 2016", displayDate );
    }
    
    @Test
    public void testGetAssetDisplayDateWhenDisplayAndSortingDateAreNull() {
        // setup logic ( test-specific )
        when( properties.get( ClassMagsMigrationConstants.ASSET_DISPLAY_DATE, String.class ) ).thenReturn( null );
        when( properties.get( ClassMagsMigrationConstants.ASSET_SORTING_DATE ) ).thenReturn( null );

        // execute logic
        String displayDate = CommonUtils.getAssetDisplayDate( properties, DATE_FORMAT );

        // verify logic
        assertTrue( displayDate.isEmpty() );
    }
    
    private Tag[] createTagList() {
        Tag classroomMagazinesTag = mockTag( "Classroom Magazines", "/etc/tags/classroom-magazines",
                "classroom-magazines", null );
        Tag globalTag = mockTag( "Global", "/etc/tags/classroom-magazines/global", "global", null );
        Tag contentTypeTag = mockTag( "Content Type", "/etc/tags/classroom-magazines/global/content-types",
                "content-types", "classroom-magazines:global/content-types" );
        Tag articleTag = mockTag( "Article", "/etc/tags/classroom-magazines/global/content-types/article", "article",
                "classroom-magazines:global/content-types/article" );
        Tag biologyTag = mockTag( "Biology", "/etc/tags/classroom-magazines/scienceworld/biology", "biology",
                "classroom-magazines:scienceworld/biology" );
        Tag scienceWorldTag = mockTag( "Science World", "/etc/tags/classroom-magazines/scienceworld", "science-world",
                "classroom-magazines:scienceworld" );
        Tag geometrixxMusicTag = mockTag( "Music", "/etc/tags/geometrixx-media/entertainment/music", "music",
                "geometrixx:entertainment/music" );
        Tag geometrixxTag = mockTag( "Geometrixx", "/etc/tags/geometrixx-media", "geometrixx-media", null );
        Tag entertainmentTag = mockTag( "Entertainment", "/etc/tags/geometrixx-media/entertainment", "entertainment",
                "geometrixx:entertainment" );

        when( tagManager.resolve( "classroom-magazines:scienceworld" ) ).thenReturn( scienceWorldTag );
        when( tagManager.resolve( "classroom-magazines:global" ) ).thenReturn( globalTag );
        when( tagManager.resolve( "classroom-magazines:global/content-types" ) ).thenReturn( contentTypeTag );
        when( tagManager.resolve( "classroom-magazines:scienceworld/biology" ) ).thenReturn( biologyTag );
        when( tagManager.resolve( "classroom-magazines:global/content-types/article" ) ).thenReturn( articleTag );
        when( tagManager.resolve( "geometrixx:entertainment" ) ).thenReturn( entertainmentTag );
        when( tagManager.resolve( "geometrixx:entertainment/music" ) ).thenReturn( geometrixxMusicTag );

        when( biologyTag.getNamespace() ).thenReturn( classroomMagazinesTag );
        when( contentTypeTag.getNamespace() ).thenReturn( classroomMagazinesTag );
        when( geometrixxMusicTag.getNamespace() ).thenReturn( geometrixxTag );

        Tag[] tags = new Tag[] { scienceWorldTag, articleTag, biologyTag, geometrixxMusicTag };

        return tags;
    }

    private Tag mockTag( String tagTitle, String tagPath, String tagName, String tagId ) {
        Tag tag = mock( Tag.class );

        when( tag.getTitle() ).thenReturn( tagTitle );
        when( tag.getPath() ).thenReturn( tagPath );
        when( tag.getName() ).thenReturn( tagName );
        when( tag.getTagID() ).thenReturn( tagId );

        return tag;
    }

    @Test
    public void testPrivateConstructor() throws Exception {
        Constructor< ? >[] constructor = CommonUtils.class.getDeclaredConstructors();

        constructor[ 0 ].setAccessible( true );
        constructor[ 0 ].newInstance( ( Object[] ) null );

        assertEquals( 1, constructor.length );

    }
}
