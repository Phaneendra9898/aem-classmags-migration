package com.scholastic.classmags.migration.servlets;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.PrintWriter;
import java.util.HashMap;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.scholastic.classmags.migration.models.SearchResponse;
import com.scholastic.classmags.migration.services.ClassMagsQueryService;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.utils.RoleUtil;

@RunWith( PowerMockRunner.class )
@PrepareForTest( { ContentHubServlet.class, RoleUtil.class, ClassMagsQueryService.class } )
public class ContentHubServletTest {
    private ContentHubServlet contentHubServlet;
    private SlingHttpServletRequest request;
    private SlingHttpServletResponse response;
    private PrintWriter printWriter;
    private MagazineProps magazineProps;
    private ClassMagsQueryService queryService;
    private SearchResponse results;

    /** The logger. */
    private Logger logger;

    @SuppressWarnings( "unchecked" )
    @Before
    public void setUp() throws Exception {
        contentHubServlet = Whitebox.newInstance( ContentHubServlet.class );

        logger = mock( Logger.class );
        request = mock( SlingHttpServletRequest.class );
        response = mock( SlingHttpServletResponse.class );
        printWriter = mock( PrintWriter.class );
        magazineProps = mock( MagazineProps.class );
        queryService = mock( ClassMagsQueryService.class );
        results = mock( SearchResponse.class );

        PowerMockito.mockStatic( RoleUtil.class );
        Whitebox.setInternalState( ContentHubServlet.class, "LOG", logger );
        Whitebox.setInternalState( contentHubServlet, magazineProps );
        Whitebox.setInternalState( contentHubServlet, queryService );

        when( request.getParameter( "path" ) ).thenReturn( "/content/scholastic/contentHub/page1.html" );
        when( RoleUtil.getUserRole( request ) ).thenReturn( "teacher" );
        when( magazineProps.getMagazineName( "/content/scholastic/contentHub/page1.html" ) )
                .thenReturn( "scienceworld" );
        when( queryService.getSearchResults( any( HashMap.class ), any( Boolean.class ), any( String.class ) ) )
                .thenReturn( results );
        when( response.getWriter() ).thenReturn( printWriter );
    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testWhenResultIsNull() throws Exception {

        // setup logic ( test-specific )
        when( queryService.getSearchResults( any( HashMap.class ), any( Boolean.class ), any( String.class ) ) )
                .thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( contentHubServlet, "doGet", request, response );

        // verify
        verify( printWriter ).write( "{}" );
    }

    @Test
    public void testContentHubServlet() throws Exception {

        // execute logic
        Whitebox.invokeMethod( contentHubServlet, "doGet", request, response );

        // verify logic
        verify( printWriter ).write( any( String.class ) );
    }

    @SuppressWarnings( "unchecked" )
    @Test( expected = Exception.class )
    public void testContentHubServletThrowsException() throws Exception {

        // setup logic ( test-specific )
        when( queryService.getSearchResults( any( HashMap.class ), any( Boolean.class ), any( String.class ) ) )
                .thenThrow( new Exception() );

        // execute logic
        Whitebox.invokeMethod( contentHubServlet, "doGet", request, response );

        // verify logic
        verify( logger ).error( "Error Search Services", new Exception() );
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final ContentHubServlet contentHubServlet = new ContentHubServlet();

        // verify logic
        assertNotNull( contentHubServlet );
    }
}
