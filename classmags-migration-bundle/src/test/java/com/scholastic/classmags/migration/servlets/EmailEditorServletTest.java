package com.scholastic.classmags.migration.servlets;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.adobe.acs.commons.email.EmailService;
import com.scholastic.classmags.migration.services.MagazineProps;

/**
 * The Class EmailEditorServletTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { EmailEditorServlet.class } )
public class EmailEditorServletTest {

    private static final String REQUEST_JSON = "{\"name\":\"Sayantan\",\"email\":\"sasen@scholastic.com\",\"subject\":\"Sample Subject\",\"message\":\"Sample Message\",\"to\":\"jrobson@scholastic.com\"}";
    private static final String REQUEST_JSON_1 = "{\"name\":\"Sayantan\",\"email\":\"sasenscholasticcom\",\"subject\":\"Sample Subject\",\"message\":\"Sample Message\",\"to\":\"jrobson@scholastic.com\"}";
    private static final String REQUEST_JSON_2 = "{\"name\":\"Sayantan\",\"email\":\"\",\"subject\":\"Sample Subject\",\"message\":\"Sample Message\",\"to\":\"jrobson@scholastic.com\"}";
    private static final String REQUEST_JSON_3 = "}}";
    private static final String REQUEST_JSON_4 = "{\"name\":\"Sayantan\",\"email\":\"sasen@scholastic.com\",\"subject\":\"\",\"message\":\"Sample Message\",\"to\":\"jrobson@scholastic.com\"}";
    private static final String MAGAZINE_PATH = "/content/superscience/issue-1/article-1/email-editor";

    private EmailEditorServlet emailEditorServlet;
    private SlingHttpServletRequest request;
    private SlingHttpServletResponse response;
    private Resource resource;
    private ResourceResolver resourceResolver;
    private MagazineProps magazineProps;
    private EmailService emailService;
    private Logger logger;

    @Before
    public void setUp() throws Exception {
        emailEditorServlet = Whitebox.newInstance( EmailEditorServlet.class );

        request = mock( SlingHttpServletRequest.class );
        response = mock( SlingHttpServletResponse.class );
        resource = mock( Resource.class );
        resourceResolver = mock( ResourceResolver.class );
        magazineProps = mock( MagazineProps.class );
        emailService = mock( EmailService.class );
        logger = mock( Logger.class );

        Whitebox.setInternalState( emailEditorServlet, magazineProps );
        Whitebox.setInternalState( emailEditorServlet, emailService );
        Whitebox.setInternalState( EmailEditorServlet.class, "LOG", logger );

        when( request.getResource() ).thenReturn( resource );
        when( resource.getResourceResolver() ).thenReturn( resourceResolver );
        when( resource.getPath() ).thenReturn( MAGAZINE_PATH );
        when( magazineProps.getMagazineName( MAGAZINE_PATH ) ).thenReturn( "SuperScience" );
    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testEmailEditorServlet() throws Exception {
        // setup logic (test-specific)
        when( request.getParameter( "data" ) ).thenReturn( REQUEST_JSON );

        // execute logic
        Whitebox.invokeMethod( emailEditorServlet, "doPost", request, response );

        // verify logic
        verify( emailService ).sendEmail( any( String.class ), any( Map.class ), any( String.class ) );
    }

    @Test
    public void testEmailEditorServletWhenEmailIsInvalid() throws Exception {

        // setup logic ( test-specific )
        when( request.getParameter( "data" ) ).thenReturn( REQUEST_JSON_1 );

        // execute logic
        Whitebox.invokeMethod( emailEditorServlet, "doPost", request, response );

        // verify logic
        verify( response ).sendError( SlingHttpServletResponse.SC_BAD_REQUEST );
    }

    @Test
    public void testEmailEditorServletWhenEmailIsEmpty() throws Exception {

        // setup logic ( test-specific )
        when( request.getParameter( "data" ) ).thenReturn( REQUEST_JSON_2 );
        // execute logic
        Whitebox.invokeMethod( emailEditorServlet, "doPost", request, response );

        // verify logic
        verify( response ).sendError( SlingHttpServletResponse.SC_BAD_REQUEST );
    }

    @Test
    public void testEmailEditorServletThrowsException() throws Exception {

        // setup logic ( test-specific )
        when( request.getParameter( "data" ) ).thenReturn( REQUEST_JSON_3 );

        // execute logic
        Whitebox.invokeMethod( emailEditorServlet, "doPost", request, response );

        // verify logic
        verify( response ).sendError( SlingHttpServletResponse.SC_BAD_REQUEST );
    }

    @SuppressWarnings( "unchecked" )
    @Test
    public void testEmailEditorServletWhenSubjectIsEmpty() throws Exception {
        // setup logic (test-specific)
        when( request.getParameter( "data" ) ).thenReturn( REQUEST_JSON_4 );

        // execute logic
        Whitebox.invokeMethod( emailEditorServlet, "doPost", request, response );

        // verify logic
        verify( emailService ).sendEmail( any( String.class ), any( Map.class ), any( String.class ) );
    }

    @Test
    public void testEmailEditorServletWhenResourceIsNull() throws Exception {
        // setup logic (test-specific)
        when( request.getParameter( "data" ) ).thenReturn( REQUEST_JSON );
        when( request.getResource() ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( emailEditorServlet, "doPost", request, response );

        // verify logic
        verify( magazineProps, times( 0 ) ).getMagazineName( any( String.class ) );
    }
    
    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final EmailEditorServlet emailEditorServlet = new EmailEditorServlet();

        // verify logic
        assertNotNull( emailEditorServlet );
    }

}
