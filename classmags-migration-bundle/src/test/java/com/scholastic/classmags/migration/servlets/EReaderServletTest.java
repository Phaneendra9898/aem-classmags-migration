package com.scholastic.classmags.migration.servlets;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.services.GetEpubService;

/**
 * JUnit for EReaderServlet.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { EReaderServlet.class } )
public class EReaderServletTest {
    private static final String EREADER_PATH = "/content/dam/classroom-magazines/scienceworld/epub/9780545796293/9780545796293";
    private static final String JSON_RESPONSE = "{\"pages\":[\"Page_001\",\"Page_002\"]}";
    private EReaderServlet eReaderServlet;
    private SlingHttpServletRequest request;
    private SlingHttpServletResponse response;
    private GetEpubService getEpubService;
    private Logger logger;
    private List< String > pageIds;
    private PrintWriter printWriter;

    @Before
    public void setUp() throws Exception {
        eReaderServlet = Whitebox.newInstance( EReaderServlet.class );

        request = mock( SlingHttpServletRequest.class );
        response = mock( SlingHttpServletResponse.class );
        getEpubService = mock( GetEpubService.class );
        logger = mock( Logger.class );
        printWriter = mock( PrintWriter.class );

        pageIds = new ArrayList< >();
        pageIds.add( "Page_001" );
        pageIds.add( "Page_002" );

        Whitebox.setInternalState( eReaderServlet, getEpubService );
        Whitebox.setInternalState( EReaderServlet.class, "LOG", logger );

        when( request.getParameter( "eReaderPath" ) ).thenReturn( EREADER_PATH );
        when( getEpubService.getPageIds( EREADER_PATH ) ).thenReturn( pageIds );
        when( response.getWriter() ).thenReturn( printWriter );

    }

    @Test
    public void testEReaderServlet() throws Exception {

        // execute logic
        Whitebox.invokeMethod( eReaderServlet, "doGet", request, response );

        // verify logic
        verify( response ).setContentType( "application/json" );
        verify( printWriter ).write( JSON_RESPONSE );

    }

    @Test
    public void testEReaderServletWhenEReaderPathIsBlank() throws Exception {
        // setup logic ( test-specific )
        when( request.getParameter( "eReaderPath" ) ).thenReturn( StringUtils.EMPTY );

        // execute logic
        Whitebox.invokeMethod( eReaderServlet, "doGet", request, response );

        // verify logic
        verify( response ).setContentType( "application/json" );
        verify( printWriter ).write( "{\"pages\":[]}" );
    }

    @Test
    public void testEReaderServletWhenClassmagsMigrationBaseExceptionOccurs() throws Exception {
        // setup logic ( test-specific )
        ClassmagsMigrationBaseException e = new ClassmagsMigrationBaseException();
        when( getEpubService.getPageIds( EREADER_PATH ) ).thenThrow( e );

        // execute logic
        Whitebox.invokeMethod( eReaderServlet, "doGet", request, response );

        // verify logic
        verify( logger ).error( "Error getting eReader page details {}", e );
        verify( response ).sendError( SlingHttpServletResponse.SC_BAD_REQUEST );
        verify( response ).setContentType( "application/json" );
        verify( printWriter ).write( "{}" );
    }

    @Test
    public void testEReaderServletWhenJSONExceptionOccurs() throws Exception {
        // setup logic ( test-specific )
        JSONException e = new JSONException( "JSON Exception" );
        JSONObject pageIds = mock( JSONObject.class );
        whenNew( JSONObject.class ).withNoArguments().thenReturn( pageIds );
        when( pageIds.put( any( String.class ), any( List.class ) ) ).thenThrow( e );

        // execute logic
        Whitebox.invokeMethod( eReaderServlet, "doGet", request, response );

        // verify logic
        verify( logger ).error( "Error getting eReader page details {}", e );
        verify( response ).sendError( SlingHttpServletResponse.SC_BAD_REQUEST );
        verify( response ).setContentType( "application/json" );
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final EReaderServlet eReaderServlet = new EReaderServlet();

        // verify logic
        assertNotNull( eReaderServlet );
    }
}