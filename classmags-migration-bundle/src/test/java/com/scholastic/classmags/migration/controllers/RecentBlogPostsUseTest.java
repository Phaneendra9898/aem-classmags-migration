package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.List;

import javax.script.Bindings;

import org.apache.sling.api.scripting.SlingScriptHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.models.RecentBlogPostsData;
import com.scholastic.classmags.migration.services.RecentBlogPostsDataService;

/**
 * The Class RecentBlogPostsUseTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { RecentBlogPostsUse.class } )
public class RecentBlogPostsUseTest {

    /** The Constant TEST_PATH. */
    private static final String TEST_PATH = "/testpath";

    /** The search results use. */
    private RecentBlogPostsUse recentBlogPostsUse;

    /** The bindings. */
    @Mock
    private Bindings mockBindings;

    /** The mock classmags migration base exception. */
    @Mock
    private ClassmagsMigrationBaseException mockClassmagsMigrationBaseException;

    /** The sling script helper. */
    @Mock
    private SlingScriptHelper mockSlingScriptHelper;

    /** The mock recent blog posts data service. */
    @Mock
    private RecentBlogPostsDataService mockRecentBlogPostsDataService;

    /** The mock recent blog posts data list. */
    @Mock
    private List< RecentBlogPostsData > mockRecentBlogPostsDataList;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        recentBlogPostsUse = new RecentBlogPostsUse();

    }

    /**
     * Test get recent blog posts data list.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetRecentBlogPostsDataList() throws Exception {

        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockBindings.get( "url" ) ).thenReturn( TEST_PATH );
        when( mockSlingScriptHelper.getService( RecentBlogPostsDataService.class ) )
                .thenReturn( mockRecentBlogPostsDataService );
        when( mockRecentBlogPostsDataService.fetchRecentBlogPostsData( TEST_PATH ) )
                .thenReturn( mockRecentBlogPostsDataList );

        recentBlogPostsUse.init( mockBindings );
        recentBlogPostsUse.activate();
        assertEquals( false, recentBlogPostsUse.getRecentBlogPostsDataList().isEmpty() );
    }

    /**
     * Test get recent blog posts data list with no sling script helper.
     */
    @Test
    public void testGetRecentBlogPostsDataListWithNoSlingScriptHelper() {

        when( mockBindings.get( "sling" ) ).thenReturn( null );
        when( mockBindings.get( "url" ) ).thenReturn( null );

        recentBlogPostsUse.init( mockBindings );
        recentBlogPostsUse.activate();
        assertEquals( null, recentBlogPostsUse.getRecentBlogPostsDataList() );
    }

    /**
     * Test get recent blog posts data list with no path.
     */
    @Test
    public void testGetRecentBlogPostsDataListWithNoPath() {

        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockBindings.get( "url" ) ).thenReturn( null );

        recentBlogPostsUse.init( mockBindings );
        recentBlogPostsUse.activate();
        assertEquals( null, recentBlogPostsUse.getRecentBlogPostsDataList() );
    }

    /**
     * Test get recent blog posts data list with exception.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetRecentBlogPostsDataListWithException() throws Exception {

        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockBindings.get( "url" ) ).thenReturn( TEST_PATH );
        when( mockSlingScriptHelper.getService( RecentBlogPostsDataService.class ) )
                .thenReturn( mockRecentBlogPostsDataService );
        when( mockRecentBlogPostsDataService.fetchRecentBlogPostsData( TEST_PATH ) )
                .thenThrow( mockClassmagsMigrationBaseException );
        when( mockClassmagsMigrationBaseException.getMessage() ).thenReturn( "TestData" );

        recentBlogPostsUse.init( mockBindings );
        recentBlogPostsUse.activate();
        assertEquals( null, recentBlogPostsUse.getRecentBlogPostsDataList() );
    }
}
