package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.tagging.TagManager;
import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.models.ArticleAggregatorObject;
import com.scholastic.classmags.migration.models.ResourcesObject;
import com.scholastic.classmags.migration.services.TeachingResourcesService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for ArticleAggregatorServiceImpl.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { ArticleAggregatorServiceImpl.class, CommonUtils.class } )
public class ArticleAggregatorServiceImplTest {

    /** The content tile service. */
    private ArticleAggregatorServiceImpl articleAggregatorService;

    /** The Constant PATH. */
    private static final String TEST_PATH = "/testPath";

    /** The Constant TEST_DATA. */
    private static final String TEST_DATA = "testData";

    /** The Constant ARTICLE_CONFIG_RESOURCES. */
    private static final String ARTICLE_CONFIG_RESOURCES = "article-configuration";

    /** The resource resolver factory. */
    @Mock
    private ResourceResolverFactory mockResourceResolverFactory;

    /** The resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock tag manager. */
    @Mock
    private TagManager mockTagManager;

    /** The mock value map. */
    @Mock
    private ValueMap mockValueMap;

    /** The mock tags. */
    @Mock
    private List< String > mockTags;

    /** The mock article aggregator iterator. */
    @Mock
    private Iterator< ArticleAggregatorObject > mockArticleAggregatorIterator;

    /** The mock iterator. */
    @Mock
    private Iterator< Page > mockIterator;

    /** The mock teaching resources service. */
    @Mock
    private TeachingResourcesService mockTeachingResourcesService;

    /** The mock page. */
    @Mock
    private Page mockPage;

    /** The mock request. */
    @Mock
    private SlingHttpServletRequest mockRequest;

    /** The mock teaching resources. */
    @Mock
    private Map< String, List< ResourcesObject > > mockTeachingResources;

    /** The mock custom resources. */
    @Mock
    private Map< String, List< ResourcesObject > > mockCustomResources;

    /** The article aggregator list. */
    @Mock
    private List< ArticleAggregatorObject > articleAggregatorList;

    /** The mock article aggregator object. */
    @Mock
    private ArticleAggregatorObject mockArticleAggregatorObject;

    /**
     * Sets the up.
     */
    @Before
    public void setUp() {
        articleAggregatorService = new ArticleAggregatorServiceImpl();

        Whitebox.setInternalState( articleAggregatorService, mockResourceResolverFactory );
        Whitebox.setInternalState( articleAggregatorService, mockTeachingResourcesService );

        PowerMockito.mockStatic( CommonUtils.class );
        when( CommonUtils.getTagManager( mockResourceResolverFactory ) ).thenReturn( mockTagManager );
        when( CommonUtils.getSubjectTags( mockResource, mockTagManager ) ).thenReturn( mockTags );
        when( CommonUtils.getResourceResolver( mockResourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ) )
                .thenReturn( mockResourceResolver );
    }

    /**
     * Test get article aggregator data.
     */
    @Test
    public void testGetArticleAggregatorData() {

        when( mockPage.listChildren() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( mockPage );
        when( mockPage.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.FEATURED_ARTICLE_FLAG, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockResource.getChild( ClassMagsMigrationConstants.HERO_IMAGE_NODE ) ).thenReturn( mockResource );
        when( mockValueMap.get( ClassMagsMigrationConstants.HERO_IMAGE_FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockPage.getPath() ).thenReturn( TEST_PATH );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( mockResource );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "resources" ) )
                .thenReturn( mockTeachingResources );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "custom-resources" ) )
                .thenReturn( mockCustomResources );
        when( mockTeachingResourcesService.createResourcesMap( mockTeachingResources, mockCustomResources ) )
                .thenReturn( mockTeachingResources );

        assertEquals( TEST_DATA, articleAggregatorService.getArticleAggregatorData( mockRequest, mockPage ).get( 0 )
                .getResourcesObject().getTitle() );
    }

    /**
     * Test get article aggregator data with no article config.
     */
    @Test
    public void testGetArticleAggregatorDataWithNoArticleConfig() {

        when( mockPage.listChildren() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( mockPage );
        when( mockPage.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( null );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.FEATURED_ARTICLE_FLAG, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockResource.getChild( ClassMagsMigrationConstants.HERO_IMAGE_NODE ) ).thenReturn( mockResource );
        when( mockValueMap.get( ClassMagsMigrationConstants.HERO_IMAGE_FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockPage.getPath() ).thenReturn( TEST_PATH );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( mockResource );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "resources" ) )
                .thenReturn( mockTeachingResources );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "custom-resources" ) )
                .thenReturn( mockCustomResources );
        when( mockTeachingResourcesService.createResourcesMap( mockTeachingResources, mockCustomResources ) )
                .thenReturn( mockTeachingResources );

        assertEquals( TEST_DATA, articleAggregatorService.getArticleAggregatorData( mockRequest, mockPage ).get( 0 )
                .getResourcesObject().getTitle() );
    }

    /**
     * Test get article aggregator data with false featured flag.
     */
    @Test
    public void testGetArticleAggregatorDataWithFalseFeaturedFlag() {

        when( mockPage.listChildren() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( mockPage );
        when( mockPage.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.FEATURED_ARTICLE_FLAG, StringUtils.EMPTY ) )
                .thenReturn( StringUtils.EMPTY );
        when( mockValueMap.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockResource.getChild( ClassMagsMigrationConstants.HERO_IMAGE_NODE ) ).thenReturn( mockResource );
        when( mockValueMap.get( ClassMagsMigrationConstants.HERO_IMAGE_FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockPage.getPath() ).thenReturn( TEST_PATH );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( mockResource );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "resources" ) )
                .thenReturn( mockTeachingResources );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "custom-resources" ) )
                .thenReturn( mockCustomResources );
        when( mockTeachingResourcesService.createResourcesMap( mockTeachingResources, mockCustomResources ) )
                .thenReturn( mockTeachingResources );

        assertEquals( TEST_DATA, articleAggregatorService.getArticleAggregatorData( mockRequest, mockPage ).get( 0 )
                .getResourcesObject().getTitle() );
    }

    /**
     * Test get article aggregator data with no custom res.
     */
    @Test
    public void testGetArticleAggregatorDataWithNoCustomRes() {

        when( mockPage.listChildren() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( mockPage );
        when( mockPage.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.FEATURED_ARTICLE_FLAG, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockResource.getChild( ClassMagsMigrationConstants.HERO_IMAGE_NODE ) ).thenReturn( mockResource );
        when( mockValueMap.get( ClassMagsMigrationConstants.HERO_IMAGE_FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockPage.getPath() ).thenReturn( TEST_PATH );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( mockResource );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "resources" ) )
                .thenReturn( mockTeachingResources );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "custom-resources" ) )
                .thenReturn( null );
        when( mockTeachingResourcesService.createResourcesMap( mockTeachingResources, mockCustomResources ) )
                .thenReturn( mockTeachingResources );

        assertEquals( TEST_DATA, articleAggregatorService.getArticleAggregatorData( mockRequest, mockPage ).get( 0 )
                .getResourcesObject().getTitle() );
    }

    /**
     * Test get article aggregator data with empty custom res.
     */
    @Test
    public void testGetArticleAggregatorDataWithEmptyCustomRes() {

        when( mockPage.listChildren() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( mockPage );
        when( mockPage.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.FEATURED_ARTICLE_FLAG, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockResource.getChild( ClassMagsMigrationConstants.HERO_IMAGE_NODE ) ).thenReturn( mockResource );
        when( mockValueMap.get( ClassMagsMigrationConstants.HERO_IMAGE_FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockPage.getPath() ).thenReturn( TEST_PATH );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( mockResource );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "resources" ) )
                .thenReturn( mockTeachingResources );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "custom-resources" ) )
                .thenReturn( mockCustomResources );
        when( mockCustomResources.isEmpty() ).thenReturn( true );
        when( mockTeachingResourcesService.createResourcesMap( mockTeachingResources, mockCustomResources ) )
                .thenReturn( mockTeachingResources );

        assertEquals( TEST_DATA, articleAggregatorService.getArticleAggregatorData( mockRequest, mockPage ).get( 0 )
                .getResourcesObject().getTitle() );
    }

    /**
     * Test get article aggregator data with no child article config.
     */
    @Test
    public void testGetArticleAggregatorDataWithNoChildArticleConfig() {

        when( mockPage.listChildren() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( mockPage );
        when( mockPage.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.FEATURED_ARTICLE_FLAG, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockResource.getChild( ClassMagsMigrationConstants.HERO_IMAGE_NODE ) ).thenReturn( mockResource );
        when( mockValueMap.get( ClassMagsMigrationConstants.HERO_IMAGE_FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockPage.getPath() ).thenReturn( TEST_PATH );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( null );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "resources" ) )
                .thenReturn( mockTeachingResources );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "custom-resources" ) )
                .thenReturn( mockCustomResources );
        when( mockCustomResources.isEmpty() ).thenReturn( true );
        when( mockTeachingResourcesService.createResourcesMap( mockTeachingResources, mockCustomResources ) )
                .thenReturn( mockTeachingResources );

        assertEquals( TEST_DATA, articleAggregatorService.getArticleAggregatorData( mockRequest, mockPage ).get( 0 )
                .getResourcesObject().getTitle() );
    }

    /**
     * Test get article aggregator data with no hero image res.
     */
    @Test
    public void testGetArticleAggregatorDataWithNoHeroImageRes() {

        when( mockPage.listChildren() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( mockPage );
        when( mockPage.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.FEATURED_ARTICLE_FLAG, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockResource.getChild( ClassMagsMigrationConstants.HERO_IMAGE_NODE ) ).thenReturn( null );
        when( mockValueMap.get( ClassMagsMigrationConstants.HERO_IMAGE_FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockPage.getPath() ).thenReturn( TEST_PATH );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( mockResource );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "resources" ) )
                .thenReturn( mockTeachingResources );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "custom-resources" ) )
                .thenReturn( mockCustomResources );
        when( mockTeachingResourcesService.createResourcesMap( mockTeachingResources, mockCustomResources ) )
                .thenReturn( mockTeachingResources );

        assertEquals( TEST_DATA, articleAggregatorService.getArticleAggregatorData( mockRequest, mockPage ).get( 0 )
                .getResourcesObject().getTitle() );
    }

    /**
     * Test get article aggregator data with empty hero image res.
     */
    @Test
    public void testGetArticleAggregatorDataWithEmptyHeroImageRes() {

        when( mockPage.listChildren() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( true, false );
        when( mockIterator.next() ).thenReturn( mockPage );
        when( mockPage.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.FEATURED_ARTICLE_FLAG, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockResource.getChild( ClassMagsMigrationConstants.HERO_IMAGE_NODE ) ).thenReturn( mockResource );
        when( mockValueMap.get( ClassMagsMigrationConstants.HERO_IMAGE_FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( StringUtils.EMPTY );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockPage.getPath() ).thenReturn( TEST_PATH );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( mockResource );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "resources" ) )
                .thenReturn( mockTeachingResources );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "custom-resources" ) )
                .thenReturn( mockCustomResources );
        when( mockTeachingResourcesService.createResourcesMap( mockTeachingResources, mockCustomResources ) )
                .thenReturn( mockTeachingResources );

        assertEquals( TEST_DATA, articleAggregatorService.getArticleAggregatorData( mockRequest, mockPage ).get( 0 )
                .getResourcesObject().getTitle() );
    }

    /**
     * Test get article aggregator data with empty article aggregator list.
     */
    @Test
    public void testGetArticleAggregatorDataWithEmptyArticleAggregatorList() {

        when( mockPage.listChildren() ).thenReturn( mockIterator );
        when( mockIterator.hasNext() ).thenReturn( false );
        when( mockIterator.next() ).thenReturn( mockPage );
        when( mockPage.getContentResource() ).thenReturn( mockResource );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( mockResource );
        when( mockResource.getValueMap() ).thenReturn( mockValueMap );
        when( mockValueMap.get( ClassMagsMigrationConstants.FEATURED_ARTICLE_FLAG, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_TITLE, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockValueMap.get( JcrConstants.JCR_DESCRIPTION, StringUtils.EMPTY ) ).thenReturn( TEST_DATA );
        when( mockResource.getChild( ClassMagsMigrationConstants.HERO_IMAGE_NODE ) ).thenReturn( mockResource );
        when( mockValueMap.get( ClassMagsMigrationConstants.HERO_IMAGE_FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockValueMap.get( ClassMagsMigrationConstants.FILE_REFERENCE, StringUtils.EMPTY ) )
                .thenReturn( TEST_DATA );
        when( mockPage.getPath() ).thenReturn( TEST_PATH );
        when( mockResource.getChild( ARTICLE_CONFIG_RESOURCES ) ).thenReturn( mockResource );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "resources" ) )
                .thenReturn( mockTeachingResources );
        when( mockTeachingResourcesService.getTeachingResources( mockRequest, mockResource, "custom-resources" ) )
                .thenReturn( mockCustomResources );
        when( mockTeachingResourcesService.createResourcesMap( mockTeachingResources, mockCustomResources ) )
                .thenReturn( mockTeachingResources );

        assertEquals( true, articleAggregatorService.getArticleAggregatorData( mockRequest, mockPage ).isEmpty() );
    }
}
