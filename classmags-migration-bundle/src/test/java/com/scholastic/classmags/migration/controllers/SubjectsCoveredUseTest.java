package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.day.cq.tagging.Tag;
import com.scholastic.classmags.migration.models.TagModel;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.SubjectsCoveredService;

/**
 * JUnit for SubjectsCoveredUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { SubjectsCoveredUse.class } )
public class SubjectsCoveredUseTest extends BaseComponentTest {

    private static final String CURRENT_PAGE_PATH = "/content/classroom_magazines/scienceworld/issues/111616/pup-on-patrol";
    private static final String MAGAZINE_NAME = "scienceworld";
    private static final String BIOLOGY_TAG_TITLE = "Biology";
    private static final String BIOLOGY_TAG_NAME = "biology";
    private static final String CELLS_TAG_TITLE = "Cells and Adaptation";
    private static final String CELLS_TAG_NAME = "cells-and-adaptation";

    private SubjectsCoveredUse subjectsCoveredUse;
    private MagazineProps magazineProps;
    private SubjectsCoveredService subjectsCoveredService;
    private Map< TagModel, List< TagModel > > subjectsCovered;
    private TagModel biologyTagModel, cellsTagModel;

    /**
     * Setup.
     * 
     */
    @Before
    public void setup() {
        subjectsCoveredUse = Whitebox.newInstance( SubjectsCoveredUse.class );

        magazineProps = mock( MagazineProps.class );
        subjectsCoveredService = mock( SubjectsCoveredService.class );

        stubCommon( subjectsCoveredUse );

        subjectsCovered = createSubjectsCoveredMap();
        when( request.getResource() ).thenReturn( resource );
        when( slingScriptHelper.getService( MagazineProps.class ) ).thenReturn( magazineProps );
        when( slingScriptHelper.getService( SubjectsCoveredService.class ) ).thenReturn( subjectsCoveredService );
        when( currentPage.getPath() ).thenReturn( CURRENT_PAGE_PATH );
        when( magazineProps.getMagazineName( CURRENT_PAGE_PATH ) ).thenReturn( MAGAZINE_NAME );
        when( subjectsCoveredService.getSubjectsCovered( resource, CURRENT_PAGE_PATH, MAGAZINE_NAME ) )
                .thenReturn( subjectsCovered );

    }

    @Test
    public void testSubjectsCoveredUse() throws Exception {

        // execute logic
        Whitebox.invokeMethod( subjectsCoveredUse, "activate" );

        // verify logic

        assertEquals( MAGAZINE_NAME, subjectsCoveredUse.getMagazineName() );
        assertEquals( CELLS_TAG_TITLE,
                subjectsCoveredUse.getSubjects().get( biologyTagModel ).get( 0 ).getTag().getTitle() );
        assertEquals( CELLS_TAG_NAME,
                subjectsCoveredUse.getSubjects().get( biologyTagModel ).get( 0 ).getTag().getName() );

    }

    @Test
    public void testSubjectsCoveredUseWhenSlingScriptHelperIsNull() throws Exception {
        // setup logic (test-specific)
        stub( PowerMockito.method( subjectsCoveredUse.getClass(), "getSlingScriptHelper" ) ).toReturn( null );

        // execute logic
        Whitebox.invokeMethod( subjectsCoveredUse, "activate" );

        // verify logic
        assertTrue( subjectsCoveredUse.getMagazineName().isEmpty() );
        assertNull( subjectsCoveredUse.getSubjects() );

    }

    @Test
    public void testSubjectsCoveredUseWhenMagazinePropsIsNull() throws Exception {
        // setup logic (test-specific)
        when( slingScriptHelper.getService( MagazineProps.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( subjectsCoveredUse, "activate" );

        // verify logic
        assertTrue( subjectsCoveredUse.getMagazineName().isEmpty() );
    }

    @Test
    public void testSubjectsCoveredUseWhenSubjectsCoveredServiceIsNull() throws Exception {
        // setup logic (test-specific)
        when( slingScriptHelper.getService( SubjectsCoveredService.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( subjectsCoveredUse, "activate" );

        // verify logic
        assertEquals( MAGAZINE_NAME, subjectsCoveredUse.getMagazineName() );
        assertNull( subjectsCoveredUse.getSubjects() );
    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final SubjectsCoveredUse subjectsCoveredUse = new SubjectsCoveredUse();

        // verify logic
        assertNotNull( subjectsCoveredUse );

    }

    private Map< TagModel, List< TagModel > > createSubjectsCoveredMap() {
        subjectsCovered = new HashMap< >();
        Tag biologyTag = mockTag( BIOLOGY_TAG_TITLE, BIOLOGY_TAG_NAME );
        Tag cellsTag = mockTag( CELLS_TAG_TITLE, CELLS_TAG_NAME );
        biologyTagModel = new TagModel( biologyTag );
        cellsTagModel = new TagModel( cellsTag );

        List< TagModel > subTopics = new ArrayList< >();
        subTopics.add( cellsTagModel );

        subjectsCovered.put( biologyTagModel, subTopics );

        return subjectsCovered;
    }

    private Tag mockTag( String tagTitle, String tagName ) {
        Tag tag = mock( Tag.class );

        when( tag.getTitle() ).thenReturn( tagTitle );
        when( tag.getName() ).thenReturn( tagName );

        return tag;
    }
}
