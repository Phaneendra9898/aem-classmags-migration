package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for CurrentIssueHeroDataService.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { CurrentIssueHeroDataServiceImpl.class, CommonUtils.class} )
public class CurrentIssueHeroDataServiceImplTest {

    private static final String CURRENT_PATH = "/content/classroom-magazines/scienceworld/homepage";
    private static final Object DESCRIPTION = "test sample description";
    private static final Object IMAGE = "/content/dam/sample-image-path";
    private static final String ISSUE_PATH = "/content/classroom_magazines/scienceworld/homepage/ISSUE-PAGE-2";
    private static final String MAGAZINE_NAME = "scienceworld";
    private static final String DATE_FORMAT = "MMMM d, yyyy";
    private static final String DATA_PATH = "/content/classroom-magazines-admin/scienceworld/global-data-config-page";
    private static final String ISSUE_DATE = "November 30, 2016";
    private static final Object ISSUE_SORTING_DATE = "2016-11-30T00:00:00.000+05:30";

    /** The current issue hero data service. */
    private CurrentIssueHeroDataServiceImpl currentIssueHeroDataService;

    /** The resource path config service. */
    private ResourcePathConfigService resourcePathConfigService;
    
    /** The property config service. */
    private PropertyConfigService propertyConfigService;
    
    /** The magazine props service. */
    private MagazineProps magazineProps;
    
    /** The resource resolver factory. */
    private ResourceResolverFactory resourceResolverFactory;

    /** The issue page properties. */
    private ValueMap issuePageProperties;

    /**
     * Initial Setup.
     * @throws LoginException 
     */
    @Before
    public void setUp() throws LoginException {
        currentIssueHeroDataService = new CurrentIssueHeroDataServiceImpl();
        resourceResolverFactory = mock( ResourceResolverFactory.class );
        resourcePathConfigService = mock( ResourcePathConfigService.class );
        propertyConfigService = mock( PropertyConfigService.class );
        magazineProps = mock( MagazineProps.class );

        issuePageProperties = createIssuePageProperties();

        PowerMockito.mockStatic( CommonUtils.class );

        Whitebox.setInternalState( currentIssueHeroDataService, resourceResolverFactory );
        Whitebox.setInternalState( currentIssueHeroDataService, resourcePathConfigService );
        Whitebox.setInternalState( currentIssueHeroDataService, propertyConfigService );
        Whitebox.setInternalState( currentIssueHeroDataService, magazineProps );

        when( CommonUtils.fetchPagePropertyMap( ISSUE_PATH, resourceResolverFactory ) )
                .thenReturn( issuePageProperties );
        when( magazineProps.getMagazineName( CURRENT_PATH ) ).thenReturn( MAGAZINE_NAME );
        when( CommonUtils.getDataPath( resourcePathConfigService, MAGAZINE_NAME ) ).thenReturn( DATA_PATH );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                DATA_PATH ) ).thenReturn( DATE_FORMAT );

    }

    /**
     * Test fetch issue data.
     */
    @Test
    public void testFetchIssueData() {
        // execute logic
        ValueMap issueData = currentIssueHeroDataService.fetchIssueData( ISSUE_PATH );

        // verify logic
        assertEquals( DESCRIPTION, issueData.get( ClassMagsMigrationConstants.PP_DESCRIPTION ) );
        assertEquals( IMAGE, issueData.get( ClassMagsMigrationConstants.FILE_REFERENCE ) );

    }
    
    /**
     * Test fetch issue date.
     * @throws ClassmagsMigrationBaseException 
     */
    @Test
    public void testFetchIssueDate() throws ClassmagsMigrationBaseException {
        // setup logic (test-specific)
        when( CommonUtils.getDisplayDate( issuePageProperties, DATE_FORMAT ) ).thenReturn( ISSUE_DATE );

        // execute logic
        String issueDate = currentIssueHeroDataService.fetchIssueDate( ISSUE_PATH, CURRENT_PATH );

        // verify logic
        assertEquals( ISSUE_DATE, issueDate );

    }
    
    /**
     * Test fetch issue date with exception.
     * @throws ClassmagsMigrationBaseException The classmags migration base exception. 
     * @throws LoginException The login exception.
     */
    @Test( expected = ClassmagsMigrationBaseException.class )
    public void testFetchIssueDateWithException() throws ClassmagsMigrationBaseException, LoginException {
        // setup logic (test-specific)
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                DATA_PATH ) ).thenThrow( new LoginException() );

        // execute logic
        String issueDate = currentIssueHeroDataService.fetchIssueDate( ISSUE_PATH, CURRENT_PATH );

        // verify logic
        assertEquals( ISSUE_DATE, issueDate );

    }
    
    /*
     * Creates the issue page properties.
     */
    private ValueMap createIssuePageProperties() {

        ValueMap issuePageProperties = mock( ValueMap.class );

        when( issuePageProperties.get( ClassMagsMigrationConstants.PP_DESCRIPTION ) ).thenReturn( DESCRIPTION );
        when( issuePageProperties.get( ClassMagsMigrationConstants.FILE_REFERENCE ) ).thenReturn( IMAGE );
        when( issuePageProperties.get( "sortingDate" ) ).thenReturn( ISSUE_SORTING_DATE );

        return issuePageProperties;
    }

}
