package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.scholastic.classmags.migration.models.VocabData;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * JUnit for VocabDataServiceImpl.
 */
@RunWith( MockitoJUnitRunner.class )
public class VocabDataServiceImplTest {

    /** The vocab data service. */
    private VocabDataServiceImpl vocabDataService;

    /** The Constant PATH. */
    private static final String PATH = "/content";

    /** The Constant TEST_VOCAB_WORD. */
    private static final String TEST_VOCAB_WORD = "testVocabWord";

    /** The Constant TEST_VOCAB_WORD_DEFINITION. */
    private static final String TEST_VOCAB_WORD_DEFINITION = "test definition";

    /** The Constant TEST_VOCAB_WORD_IMAGE_PATH. */
    private static final String TEST_VOCAB_WORD_IMAGE_PATH = "/imagepath";

    /** The Constant TEST_VOCAB_WORD_AUDIO_PATH. */
    private static final String TEST_VOCAB_WORD_AUDIO_PATH = "/audiotpath";

    /** The Constant TEST_VOCAB_WORD_P_TEXT. */
    private static final String TEST_VOCAB_WORD_P_TEXT = "test pronunciation-text";

    /** The mock property word. */
    @Mock
    private Property mockPropertyWord;

    /** The mock property definition. */
    @Mock
    private Property mockPropertyDefinition;

    /** The mock property image path. */
    @Mock
    private Property mockPropertyImagePath;

    /** The mock property audio path. */
    @Mock
    private Property mockPropertyAudioPath;

    /** The mock property P text. */
    @Mock
    private Property mockPropertyPText;

    /** The resource path config service. */
    @Mock
    private ResourcePathConfigService resourcePathConfigService;

    /** The resource resolver factory. */
    @Mock
    private ResourceResolverFactory resourceResolverFactory;

    /** The resource resolver. */
    @Mock
    private ResourceResolver resourceResolver;

    /** The res. */
    @Mock
    private Resource res;

    /** The session. */
    @Mock
    private Session session;

    /** The query builder. */
    @Mock
    private QueryBuilder queryBuilder;

    /** The mock query map. */
    @Mock
    private Map< String, String > mockQueryMap;

    /** The query. */
    @Mock
    private Query query;

    /** The search result. */
    @Mock
    private SearchResult searchResult;

    /** The mock vocab nodes. */
    @Mock
    private List< Hit > mockVocabNodes;

    /** The hit. */
    @Mock
    private Hit hit;

    /** The mock node. */
    @Mock
    private Node mockNode;

    /**
     * Sets the up.
     *
     * @throws LoginException
     *             the login exception
     * @throws RepositoryException
     *             the repository exception
     */
    @Before
    public void setUp() throws LoginException, RepositoryException {
        vocabDataService = new VocabDataServiceImpl();
        mockQueryMap = new HashMap< String, String >();
        mockVocabNodes = new ArrayList< >();
        mockQueryMap.put( ClassMagsMigrationConstants.QUERY_PATH, PATH );
        mockQueryMap.put( ClassMagsMigrationConstants.RESULTS_LIMIT, ClassMagsMigrationConstants.QUERY_LOWER_BOUND );
        mockVocabNodes.add( hit );
    }

    /**
     * Test fetch vocab data.
     *
     * @throws LoginException
     *             the login exception
     * @throws RepositoryException
     *             the repository exception
     */
    @Test
    public void testFetchVocabData() throws LoginException, RepositoryException {
        Map< String, VocabData > vocabDataMap = new HashMap< >();
        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        vocabDataService.setResourceResolverFactory( resourceResolverFactory );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( Session.class ) ).thenReturn( session );
        vocabDataService.setQueryBuilder( queryBuilder );
        when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ), session ) ).thenReturn( query );
        when( query.getResult() ).thenReturn( searchResult );
        when( searchResult.getHits() ).thenReturn( mockVocabNodes );
        when( hit.getNode() ).thenReturn( mockNode );
        when( mockNode.hasProperty( ClassMagsMigrationConstants.VOCAB_WORD_DEFINITION ) ).thenReturn( true );
        when( mockNode.hasProperty( ClassMagsMigrationConstants.VOCAB_WORD_IMAGE_PATH ) ).thenReturn( true );
        when( mockNode.hasProperty( ClassMagsMigrationConstants.VOCAB_WORD_AUDIO_PATH ) ).thenReturn( true );
        when( mockNode.hasProperty( ClassMagsMigrationConstants.VOCAB_WORD_P_TEXT ) ).thenReturn( true );
        when( mockNode.hasProperty( ClassMagsMigrationConstants.VOCAB_WORD ) ).thenReturn( true );
        when( mockNode.getProperty( ClassMagsMigrationConstants.VOCAB_WORD_DEFINITION ) )
                .thenReturn( mockPropertyDefinition );
        when( mockNode.getProperty( ClassMagsMigrationConstants.VOCAB_WORD_IMAGE_PATH ) )
                .thenReturn( mockPropertyImagePath );
        when( mockNode.getProperty( ClassMagsMigrationConstants.VOCAB_WORD_AUDIO_PATH ) )
                .thenReturn( mockPropertyAudioPath );
        when( mockNode.getProperty( ClassMagsMigrationConstants.VOCAB_WORD_P_TEXT ) ).thenReturn( mockPropertyPText );
        when( mockNode.getProperty( ClassMagsMigrationConstants.VOCAB_WORD ) ).thenReturn( mockPropertyWord );
        when( mockPropertyWord.getString() ).thenReturn( TEST_VOCAB_WORD );
        when( mockPropertyDefinition.getString() ).thenReturn( TEST_VOCAB_WORD_DEFINITION );
        when( mockPropertyImagePath.getString() ).thenReturn( TEST_VOCAB_WORD_IMAGE_PATH );
        when( mockPropertyAudioPath.getString() ).thenReturn( TEST_VOCAB_WORD_AUDIO_PATH );
        when( mockPropertyPText.getString() ).thenReturn( TEST_VOCAB_WORD_P_TEXT );

        vocabDataMap = vocabDataService.fetchVocabData( PATH );
        assertEquals( TEST_VOCAB_WORD_DEFINITION, vocabDataMap.get( TEST_VOCAB_WORD ).getWordDefinition() );
    }

    /**
     * Test fetch vocab data empty node.
     *
     * @throws LoginException
     *             the login exception
     * @throws RepositoryException
     *             the repository exception
     */
    @Test
    public void testFetchVocabDataEmptyNode() throws LoginException, RepositoryException {
        Map< String, VocabData > vocabDataMap = new HashMap< >();
        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        vocabDataService.setResourceResolverFactory( resourceResolverFactory );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( Session.class ) ).thenReturn( session );
        vocabDataService.setQueryBuilder( queryBuilder );
        when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ), session ) ).thenReturn( query );
        when( query.getResult() ).thenReturn( searchResult );
        when( searchResult.getHits() ).thenReturn( mockVocabNodes );
        when( hit.getNode() ).thenReturn( mockNode );
        when( mockNode.hasProperty( ClassMagsMigrationConstants.VOCAB_WORD_DEFINITION ) ).thenReturn( false );
        when( mockNode.hasProperty( ClassMagsMigrationConstants.VOCAB_WORD_IMAGE_PATH ) ).thenReturn( false );
        when( mockNode.hasProperty( ClassMagsMigrationConstants.VOCAB_WORD_AUDIO_PATH ) ).thenReturn( false );
        when( mockNode.hasProperty( ClassMagsMigrationConstants.VOCAB_WORD_P_TEXT ) ).thenReturn( false );
        when( mockNode.hasProperty( ClassMagsMigrationConstants.VOCAB_WORD ) ).thenReturn( false );

        vocabDataMap = vocabDataService.fetchVocabData( PATH );
        assertEquals( null, vocabDataMap.get( TEST_VOCAB_WORD ) );
    }

    /**
     * Test fetch vocab data with no vocab data.
     *
     * @throws LoginException
     *             the login exception
     * @throws RepositoryException
     *             the repository exception
     */
    @Test
    public void testFetchVocabDataWithNoVocabData() throws LoginException, RepositoryException {
        Map< String, VocabData > vocabDataMap = new HashMap< >();
        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        vocabDataService.setResourceResolverFactory( resourceResolverFactory );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( Session.class ) ).thenReturn( session );
        vocabDataService.setQueryBuilder( queryBuilder );
        when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ), session ) ).thenReturn( query );
        when( query.getResult() ).thenReturn( searchResult );
        when( searchResult.getHits() ).thenReturn( null );

        vocabDataMap = vocabDataService.fetchVocabData( PATH );
        assertEquals( true, vocabDataMap.isEmpty() );
    }

    /**
     * Test fetch vocab data with empty vocab nodes.
     *
     * @throws LoginException
     *             the login exception
     * @throws RepositoryException
     *             the repository exception
     */
    @Test
    public void testFetchVocabDataWithEmptyVocabNodes() throws LoginException, RepositoryException {
        Map< String, VocabData > vocabDataMap = new HashMap< >();
        List< Hit > emptyVocabNodes = new ArrayList< >();
        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, ClassMagsMigrationConstants.READ_SERVICE );

        vocabDataService.setResourceResolverFactory( resourceResolverFactory );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( Session.class ) ).thenReturn( session );
        vocabDataService.setQueryBuilder( queryBuilder );
        when( queryBuilder.createQuery( PredicateGroup.create( mockQueryMap ), session ) ).thenReturn( query );
        when( query.getResult() ).thenReturn( searchResult );
        when( searchResult.getHits() ).thenReturn( emptyVocabNodes );

        vocabDataMap = vocabDataService.fetchVocabData( PATH );
        assertEquals( true, vocabDataMap.isEmpty() );
    }
}
