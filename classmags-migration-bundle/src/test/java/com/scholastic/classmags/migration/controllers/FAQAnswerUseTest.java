package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * JUnit for FAQAnswerUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { FAQAnswerUse.class } )
public class FAQAnswerUseTest extends BaseComponentTest {

    private static final String TEST_QUESTION_ID = "SS-1";
    private static final String TEST_QUESTION_VALUE = "How to Subscribe";
    private static final String QUESTION_DETAILS = "SS-1:How to Subscribe";

    private FAQAnswerUse faqAnswerUse;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        faqAnswerUse = Whitebox.newInstance( FAQAnswerUse.class );

        stubCommon( faqAnswerUse );

        when( properties.get( ClassMagsMigrationConstants.QUESTION, String.class ) ).thenReturn( QUESTION_DETAILS );
    }

    @Test
    public void testFAQAnswerUse() throws Exception {
        // execute logic
        Whitebox.invokeMethod( faqAnswerUse, "activate" );

        // verify logic
        assertEquals( TEST_QUESTION_ID, faqAnswerUse.getQuestionId() );
        assertEquals( TEST_QUESTION_VALUE, faqAnswerUse.getQuestion() );

    }

    @Test
    public void testFAQAnswerUseWhenQuestionDetailsIsNull() throws Exception {
        // setup logic ( test-specific )
        when( properties.get( ClassMagsMigrationConstants.QUESTION, String.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( faqAnswerUse, "activate" );

        // verify logic
        assertNull( faqAnswerUse.getQuestionId() );
        assertNull( faqAnswerUse.getQuestion() );

    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final FAQAnswerUse faqAnswerUse = new FAQAnswerUse();

        // verify logic
        assertNotNull( faqAnswerUse );

    }

}
