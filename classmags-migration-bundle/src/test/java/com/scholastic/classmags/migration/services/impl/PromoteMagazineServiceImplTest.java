package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for SubscriptionBenefitsService.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { PromoteMagazineDataServiceImpl.class, CommonUtils.class } )
public class PromoteMagazineServiceImplTest {

    private static final String CURRENT_PATH = "/classroom_magazines/scienceworld/issues/_2015_16/090715/jcr:content/par/promote_magazines/promoteMagazines";

    private PromoteMagazineDataServiceImpl promoteMagazineService;
    private Resource resource;
    private ResourceResolver resourceResolver;
    private ResourceResolverFactory resourceResolverFactory;

    List< ValueMap > promoteMagazines;

    /**
     * Initial Setup.
     */
    @SuppressWarnings( "unchecked" )
    @Before
    public void setUp() {
        promoteMagazineService = new PromoteMagazineDataServiceImpl();
        promoteMagazines = mock( List.class );

        resourceResolverFactory = mock( ResourceResolverFactory.class );
        resourceResolver = mock( ResourceResolver.class );
        resource = mock( Resource.class );

        Whitebox.setInternalState( promoteMagazineService, resourceResolverFactory );
        PowerMockito.mockStatic( CommonUtils.class );

        when( CommonUtils.getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ) )
                .thenReturn( resourceResolver );
        when( resourceResolver.getResource( CURRENT_PATH ) ).thenReturn( resource );
        when( resource.getChild( ClassMagsMigrationConstants.MAGAZINES_DATA ) ).thenReturn( resource );
        when( CommonUtils.fetchMultiFieldData( resource ) ).thenReturn( promoteMagazines );
    }

    @Test
    public void testFetchSubscriptionBenefits() throws ClassmagsMigrationBaseException {
        // execute logic
        List< ValueMap > promoteMagazines = promoteMagazineService.fetchMagazineData( CURRENT_PATH );

        // verify logic
        assertNotNull( promoteMagazines );
    }

    @Test
    public void testFetchSubscriptionBenefitsWhenResourceIsNull() throws ClassmagsMigrationBaseException {
        // setup logic ( test-specific )
        when( resourceResolver.getResource( CURRENT_PATH ) ).thenReturn( null );

        // execute logic
        List< ValueMap > promoteMagazines = promoteMagazineService.fetchMagazineData( CURRENT_PATH );

        // verify logic
        assertEquals( promoteMagazines.size(), 0);
    }
}
