package com.scholastic.classmags.migration.servlets;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.whenNew;

import java.io.PrintWriter;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.scholastic.classmags.migration.models.UIdHeaderIdentifiers;
import com.scholastic.classmags.migration.models.UserIdCookieData;
import com.scholastic.classmags.migration.services.JwtConfigService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.CookieUtil;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * The Class GetJwtTokenServletTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { GetJwtTokenServlet.class, CommonUtils.class, RoleUtil.class, CookieUtil.class } )
public class GetJwtTokenServletTest {

    private static final String STAFF_ID = "214064";
    private static final String STUDENT_ID = "214065";
    private static final String CONTENT_TYPE_JSON = "application/json";
    private static final String SECRET_KEY = "ClassMagazinesPr0d";
    private static final String ISSUER = "classmags";

    private GetJwtTokenServlet getJwtTokenServlet;
    private SlingHttpServletRequest request;
    private SlingHttpServletResponse response;
    private PrintWriter printWriter;
    private UserIdCookieData userIdCookie;
    private JwtConfigService jwtConfigService;

    /** The logger. */
    private Logger logger;

    @Before
    public void setUp() throws Exception {
        getJwtTokenServlet = Whitebox.newInstance( GetJwtTokenServlet.class );

        logger = mock( Logger.class );
        request = mock( SlingHttpServletRequest.class );
        response = mock( SlingHttpServletResponse.class );
        printWriter = mock( PrintWriter.class );
        jwtConfigService = mock( JwtConfigService.class );

        PowerMockito.mockStatic( CommonUtils.class );
        PowerMockito.mockStatic( RoleUtil.class );
        PowerMockito.mockStatic( CookieUtil.class );

        Whitebox.setInternalState( GetJwtTokenServlet.class, "LOG", logger );
        Whitebox.setInternalState( getJwtTokenServlet, jwtConfigService );

        userIdCookie = mockUserIdCookie();

        when( RoleUtil.getUserRole( request ) ).thenReturn( ClassMagsMigrationConstants.USER_TYPE_TEACHER );
        when( CookieUtil.fetchUserIdCookieData( request ) ).thenReturn( userIdCookie );
        when( jwtConfigService.getSecretKey() ).thenReturn( SECRET_KEY );
        when( jwtConfigService.getIssuer() ).thenReturn( ISSUER );
        when( CommonUtils.createJwtToken( STAFF_ID, ISSUER, SECRET_KEY ) )
                .thenReturn( "header.payload_teacher.signature" );
        when( CommonUtils.generateUniqueIdentifier() ).thenReturn( "testRandomUuid" );
        when( response.getWriter() ).thenReturn( printWriter );

    }

    @Test
    public void testGetJwtTokenServlet() throws Exception {

        // execute logic
        Whitebox.invokeMethod( getJwtTokenServlet, "doGet", request, response );

        // verify logic
        verify( response ).setContentType( CONTENT_TYPE_JSON );
        verify( response ).setCharacterEncoding( "UTF-8" );
        verify( printWriter ).write( "{\"accessToken\":\"header.payload_teacher.signature\"}" );

    }

    @Test
    public void testFetchUserRoleServletWhenUserIdCookieIsNotPresent() throws Exception {
        // setup logic ( test-specific )
        when( CookieUtil.fetchUserIdCookieData( request ) ).thenReturn( null );
        when( CommonUtils.createJwtToken( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS, ISSUER, "testRandomUuid" ) )
                .thenReturn( "header.payload_anonymous.randomSignature" );

        // execute logic
        Whitebox.invokeMethod( getJwtTokenServlet, "doGet", request, response );

        // verify logic
        verify( response ).setContentType( CONTENT_TYPE_JSON );
        verify( response ).setCharacterEncoding( "UTF-8" );
        verify( printWriter ).write( "{\"accessToken\":\"header.payload_anonymous.randomSignature\"}" );
    }

    @Test
    public void testFetchUserRoleServletWhenUserIsAnonymous() throws Exception {
        // setup logic ( test-specific )
        when( RoleUtil.getUserRole( request ) ).thenReturn( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS );
        when( CommonUtils.createJwtToken( ClassMagsMigrationConstants.USER_TYPE_ANONYMOUS, ISSUER, "testRandomUuid" ) )
                .thenReturn( "header.payload_anonymous.randomSignature" );
        // execute logic
        Whitebox.invokeMethod( getJwtTokenServlet, "doGet", request, response );

        // verify logic
        verify( response ).setContentType( CONTENT_TYPE_JSON );
        verify( response ).setCharacterEncoding( "UTF-8" );
        verify( printWriter ).write( "{\"accessToken\":\"header.payload_anonymous.randomSignature\"}" );
    }

    @Test
    public void testFetchUserRoleServletWhenUserIsStudent() throws Exception {
        // setup logic ( test-specific )
        when( RoleUtil.getUserRole( request ) ).thenReturn( ClassMagsMigrationConstants.USER_TYPE_STUDENT );
        when( CommonUtils.createJwtToken( STUDENT_ID, ISSUER, SECRET_KEY ) )
                .thenReturn( "header.payload_student.signature" );
        // execute logic
        Whitebox.invokeMethod( getJwtTokenServlet, "doGet", request, response );

        // verify logic
        verify( response ).setContentType( CONTENT_TYPE_JSON );
        verify( response ).setCharacterEncoding( "UTF-8" );
        verify( printWriter ).write( "{\"accessToken\":\"header.payload_student.signature\"}" );
    }

    @Test
    public void testFetchUserRoleServletWhenJSONExceptionOccurs() throws Exception {
        // setup logic ( test-specific )
        JSONException jsonException = new JSONException( "JSON Exception" );
        JSONObject jwtToken = mock( JSONObject.class );

        whenNew( JSONObject.class ).withNoArguments().thenReturn( jwtToken );

        when( jwtToken.put( "accessToken", "header.payload_teacher.signature" ) ).thenThrow( jsonException );

        // execute logic
        Whitebox.invokeMethod( getJwtTokenServlet, "doGet", request, response );

        // verify logic
        verify( logger ).error( "Error creating token {}", jsonException );
    }

    @Test
    public void testCreateInstance() throws Exception {

        // execute logic
        final GetJwtTokenServlet getJwtTokenServlet = new GetJwtTokenServlet();

        // verify logic
        assertNotNull( getJwtTokenServlet );
    }

    private UserIdCookieData mockUserIdCookie() {
        UserIdCookieData userIdCookie = mock( UserIdCookieData.class );
        UIdHeaderIdentifiers uidHeaderIdentifiers = mock( UIdHeaderIdentifiers.class );

        when( userIdCookie.getObjUIdHeaderIdentifiers() ).thenReturn( uidHeaderIdentifiers );
        when( uidHeaderIdentifiers.getStaffId() ).thenReturn( STAFF_ID );
        when( uidHeaderIdentifiers.getStudentId() ).thenReturn( STUDENT_ID );

        return userIdCookie;
    }

}
