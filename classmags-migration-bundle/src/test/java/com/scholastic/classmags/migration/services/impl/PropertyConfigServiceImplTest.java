package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.scholastic.classmags.migration.services.ResourcePathConfigService;

/**
 * JUnit for PropertyConfigServiceImpl.
 */
@RunWith( MockitoJUnitRunner.class )
public class PropertyConfigServiceImplTest {

    /** The property config service. */
    private PropertyConfigServiceImpl propertyConfigService;

    /** The Constant SITE_DATA_PATH. */
    public static final String SITE_DATA_PATH = "/content/classroom-magazines-admin/science-world-data-config-page";

    /** The Constant GLOBAL_DATA_PATH. */
    public static final String GLOBAL_DATA_PATH = "/content/classroom-magazines-admin/global-data-config-page";

    /** The Constant TEST_DATA. */
    public static final String TEST_DATA = "TestData";

    /** The Constant INPUT_KEY. */
    public static final String INPUT_KEY = "jcr:livefyreNetworkName";

    /** The Constant NO_DATA_AVAILABLE. */
    private static final String NO_DATA_AVAILABLE = "NO DATA AVAILABLE";

    /** The resource path config service. */
    ResourcePathConfigService resourcePathConfigService;

    /** The resource resolver factory. */
    ResourceResolverFactory resourceResolverFactory;

    /** The resource resolver. */
    ResourceResolver resourceResolver;

    /** The res. */
    Resource res;

    /** The properties. */
    ValueMap properties;

    /**
     * Sets the up.
     *
     * @throws LoginException
     *             the login exception
     */
    @Before
    public void setUp() throws LoginException {
        propertyConfigService = new PropertyConfigServiceImpl();
        resourceResolver = mock( ResourceResolver.class );
        resourcePathConfigService = mock( ResourcePathConfigService.class );
        resourceResolverFactory = mock( ResourceResolverFactory.class );
        res = mock( Resource.class );
        properties = mock( ValueMap.class );
    }

    /**
     * Test get data config property site path.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetDataConfigPropertySitePath() throws LoginException {

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, "readservice" );

        propertyConfigService.setResourceResolverFactory( resourceResolverFactory );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.getResource( SITE_DATA_PATH ) ).thenReturn( res );
        when( res.adaptTo( ValueMap.class ) ).thenReturn( properties );
        when( properties.get( INPUT_KEY, String.class ) ).thenReturn( TEST_DATA );
        String testProperty = propertyConfigService.getDataConfigProperty( INPUT_KEY, SITE_DATA_PATH );
        assertEquals( testProperty, TEST_DATA );
    }

    /**
     * Test get data config property global path.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetDataConfigPropertyGlobalPath() throws LoginException {

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, "readservice" );

        propertyConfigService.setResourceResolverFactory( resourceResolverFactory );
        propertyConfigService.setResourcePathConfigService( resourcePathConfigService );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.getResource( GLOBAL_DATA_PATH ) ).thenReturn( res );
        when( res.adaptTo( ValueMap.class ) ).thenReturn( properties );
        when( properties.get( INPUT_KEY, String.class ) ).thenReturn( TEST_DATA );
        when( resourcePathConfigService.getGlobalResourcePath() ).thenReturn( GLOBAL_DATA_PATH );
        String testProperty = propertyConfigService.getDataConfigProperty( INPUT_KEY, SITE_DATA_PATH );
        assertEquals( testProperty, TEST_DATA );
    }

    /**
     * Test get data config property no data.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetDataConfigPropertyNoData() throws LoginException {

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, "readservice" );

        propertyConfigService.setResourceResolverFactory( resourceResolverFactory );
        propertyConfigService.setResourcePathConfigService( resourcePathConfigService );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.getResource( GLOBAL_DATA_PATH ) ).thenReturn( null );
        when( res.adaptTo( ValueMap.class ) ).thenReturn( properties );
        when( properties.get( INPUT_KEY, String.class ) ).thenReturn( NO_DATA_AVAILABLE );
        when( resourcePathConfigService.getGlobalResourcePath() ).thenReturn( GLOBAL_DATA_PATH );
        String testProperty = propertyConfigService.getDataConfigProperty( INPUT_KEY, GLOBAL_DATA_PATH );
        assertEquals( testProperty, NO_DATA_AVAILABLE );
    }

    /**
     * Test get data config property no properties.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetDataConfigPropertyNoProperties() throws LoginException {

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, "readservice" );

        propertyConfigService.setResourceResolverFactory( resourceResolverFactory );
        propertyConfigService.setResourcePathConfigService( resourcePathConfigService );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.getResource( SITE_DATA_PATH ) ).thenReturn( res );
        when( res.adaptTo( ValueMap.class ) ).thenReturn( null );
        when( resourcePathConfigService.getGlobalResourcePath() ).thenReturn( GLOBAL_DATA_PATH );
        String testProperty = propertyConfigService.getDataConfigProperty( INPUT_KEY, SITE_DATA_PATH );
        assertEquals( testProperty, NO_DATA_AVAILABLE );
    }

    /**
     * Test get data config property no property.
     *
     * @throws LoginException
     *             the login exception
     */
    @Test
    public void testGetDataConfigPropertyNoProperty() throws LoginException {

        Map< String, Object > param = new HashMap< >();
        param.put( ResourceResolverFactory.SUBSERVICE, "readservice" );

        propertyConfigService.setResourceResolverFactory( resourceResolverFactory );
        propertyConfigService.setResourcePathConfigService( resourcePathConfigService );
        when( resourceResolverFactory.getServiceResourceResolver( param ) ).thenReturn( resourceResolver );
        when( resourceResolver.getResource( GLOBAL_DATA_PATH ) ).thenReturn( res );
        when( res.adaptTo( ValueMap.class ) ).thenReturn( properties );
        when( properties.get( INPUT_KEY, String.class ) ).thenReturn( null );
        when( resourcePathConfigService.getGlobalResourcePath() ).thenReturn( GLOBAL_DATA_PATH );
        String testProperty = propertyConfigService.getDataConfigProperty( INPUT_KEY, GLOBAL_DATA_PATH );
        assertEquals( testProperty, null );
    }

}
