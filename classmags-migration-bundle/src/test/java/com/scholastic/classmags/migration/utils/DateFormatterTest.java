package com.scholastic.classmags.migration.utils;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;

/**
 * JUnits for DateFormatter.
 */
public class DateFormatterTest {

    /** The cal. */
    Calendar cal = Calendar.getInstance();

    /** The Constant ABBREVIATED_MONTH. */
    private static final String ABBREVIATED_MONTH = "MMM YYYY";

    /** The Constant ABBREVIATED_MONTH_DAY. */
    private static final String ABBREVIATED_MONTH_DAY = "MMM d, YYYY";

    /** The Constant NOT_ABBREVIATED. */
    private static final String NOT_ABBREVIATED = "MMMM d, YYYY";

    /** The Constant TEST_DATE_ABBREVIATED_MONTH. */
    public static final String TEST_DATE_ABBREVIATED_MONTH = "Sept 2016";

    /** The Constant TEST_DATE_ABBREVIATED_MONTH_DAY. */
    public static final String TEST_DATE_ABBREVIATED_MONTH_DAY = "Sept 20, 2016";

    /** The Constant TEST_DATE_NOT_ABBREVIATED. */
    public static final String TEST_DATE_NOT_ABBREVIATED = "September 20, 2016";

    /**
     * Sets the up.
     */
    @Before
    public void setUp() {
        cal.set( 2016, Calendar.SEPTEMBER, 20 );
    }

    /**
     * Test format sorting date abb month.
     */
    @Test
    public void testFormatSortingDateAbbMonth() {
        SimpleDateFormat format = new SimpleDateFormat( ABBREVIATED_MONTH );
        assertEquals( TEST_DATE_ABBREVIATED_MONTH,
                DateFormatter.formatSortingDate( ABBREVIATED_MONTH, format.format( cal.getTime() ) ) );

    }

    /**
     * Test format sorting date abb month day.
     */
    @Test
    public void testFormatSortingDateAbbMonthDay() {
        SimpleDateFormat format = new SimpleDateFormat( ABBREVIATED_MONTH_DAY );
        assertEquals( TEST_DATE_ABBREVIATED_MONTH_DAY,
                DateFormatter.formatSortingDate( ABBREVIATED_MONTH_DAY, format.format( cal.getTime() ) ) );
    }

    /**
     * Test format sorting not abb format.
     */
    @Test
    public void testFormatSortingNotAbbFormat() {
        SimpleDateFormat format = new SimpleDateFormat( NOT_ABBREVIATED );
        assertEquals( TEST_DATE_NOT_ABBREVIATED,
                DateFormatter.formatSortingDate( NOT_ABBREVIATED, format.format( cal.getTime() ) ) );
    }

}
