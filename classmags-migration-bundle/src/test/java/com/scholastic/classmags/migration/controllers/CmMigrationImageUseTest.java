package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.script.Bindings;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.scholastic.classmags.migration.models.CmMigrationImage;

/**
 * JUnit for CmMigrationImageUse.
 */
@RunWith( MockitoJUnitRunner.class )
public class CmMigrationImageUseTest {

    /** The cm migration image use. */
    private CmMigrationImageUse cmMigrationImageUse;

    /** The mock cm migration image. */
    private CmMigrationImage MOCK_CM_MIGRATION_IMAGE;

    /** The bindings. */
    @Mock
    private Bindings bindings;

    /** The current resource. */
    @Mock
    private Resource currentResource;

    /** The component resource. */
    @Mock
    private Resource componentResource;

    /** The resource resolver. */
    @Mock
    private ResourceResolver resourceResolver;

    /** The request. */
    @Mock
    private SlingHttpServletRequest request;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        cmMigrationImageUse = new CmMigrationImageUse();
        MOCK_CM_MIGRATION_IMAGE = mock( CmMigrationImage.class );
    }

    /**
     * Test activate.
     */
    @Test
    public void testActivate() {
        when( bindings.get( "request" ) ).thenReturn( request );
        when( request.adaptTo( CmMigrationImage.class ) ).thenReturn( MOCK_CM_MIGRATION_IMAGE );
        cmMigrationImageUse.init( bindings );
        cmMigrationImageUse.activate();
        assertEquals( MOCK_CM_MIGRATION_IMAGE, cmMigrationImageUse.getCmMigrationImage() );
    }

    /**
     * Test activate with no request.
     */
    @Test
    public void testActivateWithNoRequest() {
        when( bindings.get( "request" ) ).thenReturn( null );
        cmMigrationImageUse.init( bindings );
        cmMigrationImageUse.activate();
        cmMigrationImageUse.setCmMigrationImage( MOCK_CM_MIGRATION_IMAGE );
        assertEquals( MOCK_CM_MIGRATION_IMAGE, cmMigrationImageUse.getCmMigrationImage() );
    }
}
