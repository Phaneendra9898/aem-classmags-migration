package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.models.LinkObject;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * JUnit for GlobalNavExploreUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { GlobalNavExploreUse.class, CommonUtils.class, InternalURLFormatter.class } )
public class GlobalNavExploreUseTest extends BaseComponentTest {

    /** The Constant TEST_PATH. */
    private static final String LINK_LABEL = "Biology";

    private static final String LINK_PATH = "/content/classroom_magazines/scienceworld/search-results/biology";

    private static final String SEE_ALL_LABEL = "See all subjects";

    private static final String SEE_ALL_PATH = "/content/classroom_magazines/scienceworld/see-all-subjects";

    /** The global nav explore use. */
    private GlobalNavExploreUse globalNavExploreUse;
    
    private List<LinkObject> exploreLinksObject;
    private List<ValueMap> exploreLinks;
    private ValueMap exploreLink;
    private LinkObject exploreLinkObject;
    private Iterator< ValueMap > iterator;
    
    /**
     * Setup.
     */
    @SuppressWarnings( "unchecked" )
    @Before
    public void setup() {
        globalNavExploreUse = Whitebox.newInstance( GlobalNavExploreUse.class );
        
        stubCommon( globalNavExploreUse );
        
        PowerMockito.mockStatic( CommonUtils.class );
        PowerMockito.mockStatic( InternalURLFormatter.class );
        
        iterator = mock(Iterator.class);
        exploreLinks = new ArrayList<>();
        exploreLinksObject = new ArrayList<>();
        
        exploreLink = mockExploreLink();
        exploreLinks.add( exploreLink );
        
        exploreLinkObject = createLinkObject(LINK_LABEL, LINK_PATH);
        exploreLinksObject.add( exploreLinkObject );
        
        when(CommonUtils.fetchMultiFieldData( resource, "explore-links" )).thenReturn( exploreLinks );
        when(resource.getResourceResolver()).thenReturn( resourceResolver );
        when(iterator.hasNext()).thenReturn( true, false );
        when(iterator.next()).thenReturn(exploreLink);
        when(properties.get( "seeAllLabel", String.class )).thenReturn( SEE_ALL_LABEL );
        when(properties.get( "seeAllLink", String.class )).thenReturn( SEE_ALL_PATH );
        when(InternalURLFormatter.formatURL( resourceResolver, LINK_PATH )).thenReturn( LINK_PATH + ".html" );
        when(InternalURLFormatter.formatURL( resourceResolver, SEE_ALL_PATH )).thenReturn( SEE_ALL_PATH + ".html");
        
    }

    /**
     * Test get explore links.
     * @throws Exception 
     */
    @Test
    public void testGetExploreLinks() throws Exception {
        // execute logic
        Whitebox.invokeMethod( globalNavExploreUse, "activate" );

        // verify logic
        assertEquals( 1, globalNavExploreUse.getExploreLinks().size() );
        assertEquals( LINK_PATH + ".html", globalNavExploreUse.getExploreLinks().get( 0 ).getPath());
        assertEquals( LINK_LABEL, globalNavExploreUse.getExploreLinks().get( 0 ).getLabel());
        assertEquals( SEE_ALL_LABEL, globalNavExploreUse.getSeeAllLink().getLabel());
        assertEquals( SEE_ALL_PATH + ".html", globalNavExploreUse.getSeeAllLink().getPath());
    }
    
    /**
     * Test get explore links.
     * @throws Exception 
     */
    @Test
    public void testGetExploreLinksWhenResourceIsNull() throws Exception {
        // setup logic (test-specific)
        stub( PowerMockito.method( globalNavExploreUse.getClass(), "getResource" ) ).toReturn( null );
        when(InternalURLFormatter.formatURL( null, SEE_ALL_PATH )).thenReturn( SEE_ALL_PATH );
        // execute logic
        Whitebox.invokeMethod( globalNavExploreUse, "activate" );

        // verify logic
        assertTrue( globalNavExploreUse.getExploreLinks().isEmpty() );
        assertEquals( SEE_ALL_LABEL, globalNavExploreUse.getSeeAllLink().getLabel());
        assertEquals( SEE_ALL_PATH, globalNavExploreUse.getSeeAllLink().getPath());
    }
    
    @Test
    public void testCreateInstance() {
        // execute logic
        final GlobalNavExploreUse globalNavExploreUse = new GlobalNavExploreUse();

        // verify logic
        assertNotNull( globalNavExploreUse );
    }
    
    private LinkObject createLinkObject(String linkLabel, String linkPath)
    {
        LinkObject exploreLink = new LinkObject();
        exploreLink.setLabel( linkLabel );
        exploreLink.setPath( linkPath );
        
        return exploreLink;
    }
    
    private ValueMap mockExploreLink()
    {
        ValueMap exploreLink = mock(ValueMap.class);
        
        when(exploreLink.get( "linkLabel", String.class )).thenReturn( LINK_LABEL );
        when(exploreLink.get( "linkPath", String.class )).thenReturn( LINK_PATH );
        
        return exploreLink;
    }
}
