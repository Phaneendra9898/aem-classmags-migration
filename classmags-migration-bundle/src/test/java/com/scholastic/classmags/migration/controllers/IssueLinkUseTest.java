package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.day.cq.wcm.api.PageManager;
import com.scholastic.classmags.migration.services.IssueService;
import com.scholastic.classmags.migration.services.MagazineProps;

/**
 * JUnit for IssueLinkUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { IssueLinkUse.class } )
public class IssueLinkUseTest extends BaseComponentTest {

    private static final String PARENT_ISSUE_PATH = "/content/issue-path.html";
    private static final String PARENT_ISSUE_DATE = "October 2016";
    private static final String CURRENT_PAGE_PATH = "/content/issue-path/article";
    private static final String MAGAZINE_NAME = "scienceworld";
    
    private IssueLinkUse issueLinkUse;
    private IssueService issueService;
    private MagazineProps magazineProps;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        issueLinkUse = Whitebox.newInstance( IssueLinkUse.class );

        stubCommon( issueLinkUse );

        issueService = mock( IssueService.class );
        magazineProps = mock ( MagazineProps.class );
        when( request.getResource() ).thenReturn( resource );
        when( resource.getPath() ).thenReturn( CURRENT_PAGE_PATH );
        when( resource.getResourceResolver() ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( PageManager.class ) ).thenReturn( pageManager );
        when( pageManager.getContainingPage( resource ) ).thenReturn( currentPage );
        when( slingScriptHelper.getService( IssueService.class ) ).thenReturn( issueService );
        when( slingScriptHelper.getService( MagazineProps.class ) ).thenReturn( magazineProps );
        when( magazineProps.getMagazineName( CURRENT_PAGE_PATH )).thenReturn( MAGAZINE_NAME );

    }

    @Test
    public void testIssueLinkUse() throws Exception {
        // setup logic(test-specific)
        when( issueService.getParentIssuePath( request, currentPage ) ).thenReturn( PARENT_ISSUE_PATH );
        when( issueService.getParentIssueDate( request, currentPage, MAGAZINE_NAME ) ).thenReturn( PARENT_ISSUE_DATE );
        // execute logic
        Whitebox.invokeMethod( issueLinkUse, "activate" );

        // verify logic
        assertEquals( PARENT_ISSUE_PATH, issueLinkUse.getParentIssuePath() );
        assertEquals( PARENT_ISSUE_DATE, issueLinkUse.getIssueDate() );
    }

    @Test
    public void testIssueLinkUseWhenCurrentPageIsNull() throws Exception {
        // setup logic(test-specific)
        when( pageManager.getContainingPage( resource ) ).thenReturn( null );
        // execute logic
        Whitebox.invokeMethod( issueLinkUse, "activate" );

        // verify logic
        assertTrue( issueLinkUse.getParentIssuePath().isEmpty() );
        assertNull( issueLinkUse.getIssueDate() );
    }

    @Test
    public void testIssueLinkWhenPageManagerIsNull() throws Exception {
        // setup logic(test-specific)
        when( resourceResolver.adaptTo( PageManager.class ) ).thenReturn( null );
        // execute logic
        Whitebox.invokeMethod( issueLinkUse, "activate" );

        // verify logic
        assertTrue( issueLinkUse.getParentIssuePath().isEmpty() );
    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final IssueLinkUse issueLinkUse = new IssueLinkUse();

        // verify logic
        assertNotNull( issueLinkUse );

    }

}
