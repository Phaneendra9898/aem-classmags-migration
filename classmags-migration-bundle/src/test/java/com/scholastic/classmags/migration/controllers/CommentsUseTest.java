package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import org.apache.sling.api.resource.LoginException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.day.cq.commons.Externalizer;
import com.day.cq.wcm.api.Page;
import com.livefyre.Livefyre;
import com.livefyre.core.Collection;
import com.livefyre.core.Network;
import com.livefyre.core.Site;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * The Class CommentsUseTest.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { CommentsUse.class, CommonUtils.class, Livefyre.class } )
public class CommentsUseTest extends BaseComponentTest {

    private static final String PAGE_URL = "/content/classroom_magazines/scienceworld/issues/2016-17/090516/pup-on-patrol";
    private static final String EXTERNALIZED_PATH = "https://scienceworld.com/issues/2016-17/090516/pup-on-patrol.html";
    private static final String DATA_PAGE_PATH = "/content/classroom_magazines/admin/sw-data-page";
    private static final String RESOURCE_PATH = "/content/classroom_magazines/scienceworld/issues/2016-17/090516/pup-on-patrol/jcr:content/par/comments";

    private CommentsUse commentsUse;
    private MagazineProps magazineProps;
    private PropertyConfigService propertyConfigService;
    private ResourcePathConfigService resourcePathConfigService;
    private Externalizer externalizer;
    private Network network;
    private Site site;
    private Collection collection;

    /**
     * Setup.
     * 
     * @throws LoginException
     */
    @Before
    public void setup() throws LoginException {

        PowerMockito.mockStatic( CommonUtils.class );
        PowerMockito.mockStatic( Livefyre.class );

        magazineProps = mock( MagazineProps.class );
        propertyConfigService = mock( PropertyConfigService.class );
        resourcePathConfigService = mock( ResourcePathConfigService.class );
        externalizer = mock( Externalizer.class );
        network = mock( Network.class );
        site = mock( Site.class );
        collection = mock( Collection.class );

        commentsUse = Whitebox.newInstance( CommentsUse.class );
        stubCommon( commentsUse );

        when( currentPage.getPath() ).thenReturn( PAGE_URL );
        when( resource.getPath() ).thenReturn( RESOURCE_PATH );
        when( magazineProps.getMagazineName( PAGE_URL ) ).thenReturn( "scienceworld" );
        when( slingScriptHelper.getService( MagazineProps.class ) ).thenReturn( magazineProps );
        when( slingScriptHelper.getService( PropertyConfigService.class ) ).thenReturn( propertyConfigService );
        when( slingScriptHelper.getService( ResourcePathConfigService.class ) ).thenReturn( resourcePathConfigService );
        when( slingScriptHelper.getService( Externalizer.class ) ).thenReturn( externalizer );

        when( CommonUtils.getDataPath( resourcePathConfigService, "scienceworld" ) )
                .thenReturn( "/content/classroom_magazines/admin/sw-data-page" );
        when( propertyConfigService.getDataConfigProperty( "livefyreNetworkName", DATA_PAGE_PATH ) )
                .thenReturn( "scholastic-int-0.fyre.co" );
        when( propertyConfigService.getDataConfigProperty( "livefyreNetworkKey", DATA_PAGE_PATH ) )
                .thenReturn( "PZvsG7vWUnHniOWcOfuBye+vQUA=" );
        when( propertyConfigService.getDataConfigProperty( "livefyreSiteId", DATA_PAGE_PATH ) ).thenReturn( "306372" );
        when( propertyConfigService.getDataConfigProperty( "livefyreSiteKey", DATA_PAGE_PATH ) )
                .thenReturn( "2QcrsKhZmz6BUmypB3PTtpaPIF4=" );
        when( Livefyre.getNetwork( "scholastic-int-0.fyre.co", "PZvsG7vWUnHniOWcOfuBye+vQUA=" ) ).thenReturn( network );
        when( network.getSite( "306372", "2QcrsKhZmz6BUmypB3PTtpaPIF4=" ) ).thenReturn( site );
        when( externalizer.externalLink( resourceResolver, "scienceworld", PAGE_URL ) ).thenReturn( EXTERNALIZED_PATH );
        when( site.buildCommentsCollection( PAGE_URL, RESOURCE_PATH, EXTERNALIZED_PATH ) ).thenReturn( collection );
        when( collection.buildChecksum() ).thenReturn( "fgkd4rsf34lkl54" );
        when( collection.buildCollectionMetaToken() ).thenReturn( "da12erfvg5432dfv43" );
    }

    @Test
    public void testCommentsUse() throws Exception {

        // execute logic
        Whitebox.invokeMethod( commentsUse, "activate" );

        // verify logic
        assertNotNull( commentsUse.getArticleId() );
        assertNotNull( commentsUse.getCheckSum() );
        assertNotNull( commentsUse.getMetaToken() );
        assertNotNull( commentsUse.getNetworkName() );
        assertNotNull( commentsUse.getSiteId() );
    }

    @Test
    public void testCommentsUseWhenSlingScripthelperIsNull() throws Exception {

        // setup logic ( test-specific )
        stub( PowerMockito.method( CommentsUse.class, "getSlingScriptHelper" ) ).toReturn( null );

        // execute logic
        Whitebox.invokeMethod( commentsUse, "activate" );

        // verify logic
        verify( network, times( 0 ) ).getSite( "306372", "2QcrsKhZmz6BUmypB3PTtpaPIF4=" );
    }

    @Test
    public void testCommentsUseWhenResourcePathConfigServiceIsNull() throws Exception {

        // setup logic ( test-specific )
        when( slingScriptHelper.getService( ResourcePathConfigService.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( commentsUse, "activate" );

        // verify logic
        verify( network, times( 0 ) ).getSite( "306372", "2QcrsKhZmz6BUmypB3PTtpaPIF4=" );
    }

    @Test
    public void testCommentsUseWhenExternalizerIsNull() throws Exception {

        // setup logic ( test-specific )
        when( slingScriptHelper.getService( Externalizer.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( commentsUse, "activate" );

        // verify logic
        verify( network, times( 0 ) ).getSite( "306372", "2QcrsKhZmz6BUmypB3PTtpaPIF4=" );
    }

    @Test
    public void testCommentsUseWhenPropertyConfigServiceIsNull() throws Exception {

        // setup logic ( test-specific )
        when( slingScriptHelper.getService( PropertyConfigService.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( commentsUse, "activate" );

        // verify logic
        verify( network, times( 0 ) ).getSite( "306372", "2QcrsKhZmz6BUmypB3PTtpaPIF4=" );
    }

    @Test
    public void testCommentsUseWhenMagazinePropsIsNull() throws Exception {

        // setup logic ( test-specific )
        when( slingScriptHelper.getService( MagazineProps.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( commentsUse, "activate" );

        // verify logic
        verify( network, times( 0 ) ).getSite( "306372", "2QcrsKhZmz6BUmypB3PTtpaPIF4=" );
    }

    @Test
    public void testCreateInstance() {

        // execute logic
        final CommentsUse commentsUse = new CommentsUse();

        // verify logic
        assertNotNull( commentsUse );
    }
}
