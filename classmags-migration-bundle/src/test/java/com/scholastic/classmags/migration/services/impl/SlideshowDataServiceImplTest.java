package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.services.SlideshowDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.InternalURLFormatter;

/**
 * JUnit for SlideshowDataService.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { SlideshowDataServiceImpl.class, CommonUtils.class } )
public class SlideshowDataServiceImplTest {

    private SlideshowDataService slideshowDataService;
    private List< ValueMap > properties = new ArrayList< ValueMap >();
    private ResourceResolverFactory resourceResolverFactory;
    private ResourceResolver resourceResolver;
    private ValueMap property;

    /**
     * Initial Setup.
     */
    @SuppressWarnings( "unchecked" )
    @Before
    public void setUp() {
        slideshowDataService = new SlideshowDataServiceImpl();

        resourceResolver = mock( ResourceResolver.class );
        resourceResolverFactory = mock( ResourceResolverFactory.class );

        Whitebox.setInternalState( slideshowDataService, resourceResolverFactory );

        PowerMockito.mockStatic( CommonUtils.class );
        when( CommonUtils.getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ) )
                .thenReturn( resourceResolver );

    }

    @Test
    public void testFetchSlideshowDataService() throws JSONException {
        // setup logic ( test-specific )
        property = mock( ValueMap.class );
        createProperties();

        // execute logic
        JSONArray slideshowJSONArray = slideshowDataService.convertListToJSONArray( properties );

        // verify logic
        assertNotNull( slideshowJSONArray );
    }

    @Test
    public void testFetchSlideshowDataServiceWhenPropertiesAreNull() throws JSONException {
        // setup logic ( test-specific )
        properties.add( null );

        // execute logic
        JSONArray slideshowJSONArray = slideshowDataService.convertListToJSONArray( properties );

        // verify logic
        assertNotNull( slideshowJSONArray );
    }

    void createProperties() {

        ValueMap valueMap = mock( ValueMap.class );

        properties.add( valueMap );
        properties.add( valueMap );
        properties.add( valueMap );
    }
}
