package com.scholastic.classmags.migration.controllers;

import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.script.Bindings;

import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.iterators.TransformIterator;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.adobe.granite.ui.components.Value;
import com.adobe.granite.ui.components.ds.DataSource;
import com.day.cq.tagging.Tag;
import com.day.cq.tagging.TagManager;

@RunWith( MockitoJUnitRunner.class )
public class ContentTagsDropdownUseTest {

    private static final String PATH = "/testPath";

    /** The Constant TEST_SORTING_DATE. */
    private static final String TEST_KEY = "testKey";

    /** The Constant TEST_DESC. */
    private static final String TEST_DATA = "testData";

    /** The current issue hero use. */
    private ContentTagsDropdownUse contentTagsDropdownUse;

    /** The request. */
    @Mock
    private SlingHttpServletRequest mockRequest;

    /** The bindings. */
    @Mock
    private Bindings mockBindings;

    /** The sling script helper. */
    @Mock
    private SlingScriptHelper mockSlingScriptHelper;

    @Mock
    private TagManager mockTagManager;

    @Mock
    private Tag mockTag;

    @Mock
    private Iterator<Tag> mockTagIterator;

    /** The asset data map. */
    @Mock
    private ValueMap mockValueMap;

    /** The mock resource. */
    @Mock
    private Resource mockResource;

    /** The mock resource resolver. */
    @Mock
    private ResourceResolver mockResourceResolver;
    
    @Mock
    private ResourceResolverFactory mockResourceResolverFactory;

    private Map< String, String > resourcesMap;

    @Mock
    private DataSource mockDataSource;

    private TransformIterator transformIterator;
    
    @Mock
    private Transformer transformer;
    
    @Mock
    private Object mockObject;
    
    private Map<String, Object> mockAuthInfo;
    
    /**
     * Setup.
     */
    @Before
    public void setup() {
        contentTagsDropdownUse = new ContentTagsDropdownUse();
        mockAuthInfo = new HashMap<>();
        mockAuthInfo.put( "sling.service.subservice", (Object) "readservice" );
        resourcesMap = new HashMap< >();
        resourcesMap.put( TEST_KEY, TEST_DATA );
        
    }

    @Test
    public void testActivate() throws LoginException {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockRequest.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockSlingScriptHelper.getService( ResourceResolverFactory.class ) )
                .thenReturn( mockResourceResolverFactory );
        when( mockResourceResolverFactory.getServiceResourceResolver( mockAuthInfo )).thenReturn( mockResourceResolver );

        when( mockResourceResolver.adaptTo( TagManager.class )).thenReturn( mockTagManager );
        when( mockTagManager.resolve( Mockito.anyString() )).thenReturn( mockTag );
        when( mockTag.listChildren()).thenReturn( mockTagIterator );
        when( mockTag.getTitle()).thenReturn( TEST_KEY );
        when( mockTagIterator.hasNext()).thenReturn( true ).thenReturn( false );
        when( mockTagIterator.next()).thenReturn( mockTag );
        when(mockResourceResolver.isLive()).thenReturn( true );
        
        transformIterator = new TransformIterator( resourcesMap.keySet().iterator(), transformer );
        transformIterator.getTransformer().transform( mockObject );
        
        contentTagsDropdownUse.init( mockBindings );
        contentTagsDropdownUse.activate();
        contentTagsDropdownUse.getDropdown();
    }

    @Test
    public void testActivateWithNoContentTags() throws LoginException {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockRequest.getResourceResolver() ).thenReturn( mockResourceResolver );
        
        when( mockSlingScriptHelper.getService( ResourceResolverFactory.class ) )
        .thenReturn( mockResourceResolverFactory );
        when( mockResourceResolverFactory.getServiceResourceResolver( mockAuthInfo )).thenReturn( mockResourceResolver );
        
        when( mockResourceResolver.adaptTo( TagManager.class )).thenReturn( mockTagManager );
        when( mockTagManager.resolve( Mockito.anyString() )).thenReturn( null );
        contentTagsDropdownUse.init( mockBindings );
        contentTagsDropdownUse.activate();
    }

    @Test
    public void testActivateWithoutResourceResolver() throws LoginException {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( mockSlingScriptHelper );
        when( mockSlingScriptHelper.getService( ResourceResolverFactory.class ) )
        .thenReturn( mockResourceResolverFactory );
        when( mockResourceResolverFactory.getServiceResourceResolver( mockAuthInfo )).thenReturn( null ).thenThrow( LoginException.class );
        contentTagsDropdownUse.init( mockBindings );
        contentTagsDropdownUse.activate();
    }

    @Test
    public void testActivateWithNoScriptHelper() {

        when( mockBindings.get( "request" ) ).thenReturn( mockRequest );
        when( mockBindings.get( "sling" ) ).thenReturn( null );
        when( mockRequest.getResourceResolver() ).thenReturn( mockResourceResolver );
        when( mockRequest.getAttribute( Value.CONTENTPATH_ATTRIBUTE ) ).thenReturn( PATH );
        when( mockResourceResolver.getResource( PATH ) ).thenReturn( mockResource );

        contentTagsDropdownUse.init( mockBindings );
        contentTagsDropdownUse.activate();
    }
}
