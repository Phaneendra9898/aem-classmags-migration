package com.scholastic.classmags.migration.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.jcr.Binary;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.RepositoryException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.scholastic.classmags.migration.exception.ClassmagsMigrationBaseException;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for GetEpubServiceImpl
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { GetEpubServiceImpl.class, CommonUtils.class, DocumentBuilderFactory.class } )
public class GetEpubServiceImplTest {

    private static final String EREADER_PATH = "/content/dam/classroom-magazines/scienceworld/epub/9780545796293/9780545796293";
    private static final String CONTENT_OPF_PATH = "/OPS/content.opf/jcr:content/renditions/original/jcr:content";

    private GetEpubServiceImpl getEpubServiceImpl;
    private ResourceResolverFactory resourceResolverFactory;
    private ResourceResolver resourceResolver;
    private Resource eReaderResource;
    private Node eReaderNode;
    private DocumentBuilderFactory dbf;
    private DocumentBuilder db;
    private Property property;
    private Binary binary;
    private InputStream inputStream;
    private Document document;
    private NodeList nodeList;
    private org.w3c.dom.Node nodeOne;
    private NamedNodeMap nodeOneMap;

    private Logger logger;

    /**
     * Initial setup.
     * 
     * @throws ParserConfigurationException
     * @throws RepositoryException
     * @throws PathNotFoundException
     * @throws IOException
     * @throws SAXException
     */
    @Before
    public void setUp()
            throws ParserConfigurationException, PathNotFoundException, RepositoryException, SAXException, IOException {
        getEpubServiceImpl = new GetEpubServiceImpl();

        logger = mock( Logger.class );
        eReaderResource = mock( Resource.class );
        resourceResolverFactory = mock( ResourceResolverFactory.class );
        resourceResolver = mock( ResourceResolver.class );
        eReaderNode = mock( Node.class );
        dbf = mock( DocumentBuilderFactory.class );
        db = mock( DocumentBuilder.class );
        property = mock( Property.class );
        binary = mock( Binary.class );
        inputStream = mock( InputStream.class );
        document = mock( Document.class );
        nodeList = mock( NodeList.class );
        nodeOne = mock( org.w3c.dom.Node.class );
        nodeOneMap = mock( NamedNodeMap.class );

        PowerMockito.mockStatic( CommonUtils.class );
        PowerMockito.mockStatic( DocumentBuilderFactory.class );

        Whitebox.setInternalState( getEpubServiceImpl, resourceResolverFactory );

        Whitebox.setInternalState( GetEpubServiceImpl.class, "LOG", logger );

        when( CommonUtils.getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ) )
                .thenReturn( resourceResolver );
        when( resourceResolver.getResource( EREADER_PATH + CONTENT_OPF_PATH ) ).thenReturn( eReaderResource );
        when( eReaderResource.adaptTo( Node.class ) ).thenReturn( eReaderNode );
        when( DocumentBuilderFactory.newInstance() ).thenReturn( dbf );
        when( dbf.newDocumentBuilder() ).thenReturn( db );
        when( eReaderNode.getProperty( "jcr:data" ) ).thenReturn( property );
        when( property.getBinary() ).thenReturn( binary );
        when( binary.getStream() ).thenReturn( inputStream );
        when( db.parse( inputStream ) ).thenReturn( document );
        when( document.getElementsByTagName( "itemref" ) ).thenReturn( nodeList );
        when( nodeList.getLength() ).thenReturn( 1 );
        when( nodeList.item( 0 ) ).thenReturn( nodeOne );
        when( nodeOne.getAttributes() ).thenReturn( nodeOneMap );
        when( nodeOneMap.getNamedItem( "idref" ) ).thenReturn( nodeOne );
        when( nodeOne.getNodeValue() ).thenReturn( "Page_001" );

    }

    @Test
    public void testGetPageIds() throws ClassmagsMigrationBaseException {
        // execute logic
        List< String > pageIds = getEpubServiceImpl.getPageIds( EREADER_PATH );

        // verify logic
        assertEquals( 1, pageIds.size() );
        assertEquals( "Page_001", pageIds.get( 0 ) );
    }

    @Test
    public void testGetPageIdsWhenResourceResolverIsNull() throws ClassmagsMigrationBaseException {
        // setup logic ( test-specific )
        when( CommonUtils.getResourceResolver( resourceResolverFactory, ClassMagsMigrationConstants.READ_SERVICE ) )
                .thenReturn( null );

        // execute logic
        List< String > pageIds = getEpubServiceImpl.getPageIds( EREADER_PATH );

        // verify logic
        assertTrue( pageIds.isEmpty() );
    }

    @Test
    public void testGetPageIdsWhenEReaderResourceIsNull() throws ClassmagsMigrationBaseException {
        // setup logic ( test-specific )
        when( resourceResolver.getResource( EREADER_PATH + CONTENT_OPF_PATH ) ).thenReturn( null );

        // execute logic
        List< String > pageIds = getEpubServiceImpl.getPageIds( EREADER_PATH );

        // verify logic
        assertTrue( pageIds.isEmpty() );
    }

    @Test
    public void testGetPageIdsWhenEReaderNodeIsNull() throws ClassmagsMigrationBaseException {
        // setup logic ( test-specific )
        when( eReaderResource.adaptTo( Node.class ) ).thenReturn( null );

        // execute logic
        List< String > pageIds = getEpubServiceImpl.getPageIds( EREADER_PATH );

        // verify logic
        assertTrue( pageIds.isEmpty() );
    }

    @Test( expected = ClassmagsMigrationBaseException.class )
    public void testGetPageIdsWhenParserConfigurationExceptionOccurs()
            throws ClassmagsMigrationBaseException, ParserConfigurationException {
        // setup logic ( test-specific )
        ParserConfigurationException e = new ParserConfigurationException();
        when( dbf.newDocumentBuilder() ).thenThrow( e );

        // execute logic
        List< String > pageIds = getEpubServiceImpl.getPageIds( EREADER_PATH );

        // verify logic
        assertTrue( pageIds.isEmpty() );
        verify( logger ).error( "Error while parsing EPub asset", e );
    }

    @Test( expected = ClassmagsMigrationBaseException.class )
    public void testGetPageIdsWhenSAXExceptionOccurs()
            throws ClassmagsMigrationBaseException, SAXException, IOException {
        // setup logic ( test-specific )
        SAXException e = new SAXException();
        when( db.parse( inputStream ) ).thenThrow( e );

        // execute logic
        List< String > pageIds = getEpubServiceImpl.getPageIds( EREADER_PATH );

        // verify logic
        assertTrue( pageIds.isEmpty() );
        verify( logger ).error( "Error while parsing EPub asset", e );
    }

    @Test( expected = ClassmagsMigrationBaseException.class )
    public void testGetPageIdsWhenIOExceptionOccurs()
            throws ClassmagsMigrationBaseException, SAXException, IOException {
        // setup logic ( test-specific )
        IOException e = new IOException();
        when( db.parse( inputStream ) ).thenThrow( e );

        // execute logic
        List< String > pageIds = getEpubServiceImpl.getPageIds( EREADER_PATH );

        // verify logic
        assertTrue( pageIds.isEmpty() );
        verify( logger ).error( "Error while parsing EPub asset", e );
    }

    @Test( expected = ClassmagsMigrationBaseException.class )
    public void testGetPageIdsWhenRepositoryExceptionOccurs()
            throws ClassmagsMigrationBaseException, RepositoryException {
        // setup logic ( test-specific )
        RepositoryException e = new RepositoryException();
        when( eReaderNode.getProperty( "jcr:data" ) ).thenThrow( e );

        // execute logic
        List< String > pageIds = getEpubServiceImpl.getPageIds( EREADER_PATH );

        // verify logic
        assertTrue( pageIds.isEmpty() );
        verify( logger ).error( "Error while parsing EPub asset", e );
    }
}
