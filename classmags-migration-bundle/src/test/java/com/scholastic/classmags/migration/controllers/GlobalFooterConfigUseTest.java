package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import com.scholastic.classmags.migration.utils.CommonUtils;

/**
 * JUnit for GlobalFooterConfigUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { GlobalFooterConfigUse.class, CommonUtils.class } )
public class GlobalFooterConfigUseTest extends BaseComponentTest {

    private static final String NODE_NAME = "footer-links";
    private static final String LINK_PATH_KEY = "linkPath";
    private static final String LINK_TEXT_KEY = "linkText";
    private static final String LINK_USER_TYPE_KEY = "userType";
    private static final String LINK_PATH_VALUE = "/content/scholastic-article.html";
    private static final String LINK_TEXT_VALUE = "scholastic article";
    private static final String LINK_USER_TYPE_VALUE = "student";

    private GlobalFooterConfigUse globalFooterConfigUse;
    private List< ValueMap > footerLinks;
    private ValueMap footerLink;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        globalFooterConfigUse = Whitebox.newInstance( GlobalFooterConfigUse.class );

        stubCommon( globalFooterConfigUse );

        PowerMockito.mockStatic( CommonUtils.class );
        footerLinks = new ArrayList< >();
        footerLink = mock( ValueMap.class );

        when( footerLink.get( LINK_PATH_KEY, String.class ) ).thenReturn( LINK_PATH_VALUE );
        when( footerLink.get( LINK_TEXT_KEY, String.class ) ).thenReturn( LINK_TEXT_VALUE );
        when( footerLink.get( LINK_USER_TYPE_KEY, String.class ) ).thenReturn( LINK_USER_TYPE_VALUE );

        footerLinks.add( footerLink );
    }

    @Test
    public void testIssueLinkUse() throws Exception {
        // setup logic(test-specific)
        when( CommonUtils.fetchMultiFieldData( resource, NODE_NAME ) ).thenReturn( footerLinks );
        // execute logic
        Whitebox.invokeMethod( globalFooterConfigUse, "activate" );

        // verify logic
        assertEquals( 1, globalFooterConfigUse.getFooterLinks().size() );
        assertEquals( LINK_PATH_VALUE,
                globalFooterConfigUse.getFooterLinks().get( 0 ).get( LINK_PATH_KEY, String.class ) );
        assertEquals( LINK_TEXT_VALUE,
                globalFooterConfigUse.getFooterLinks().get( 0 ).get( LINK_TEXT_KEY, String.class ) );
        assertEquals( LINK_USER_TYPE_VALUE,
                globalFooterConfigUse.getFooterLinks().get( 0 ).get( LINK_USER_TYPE_KEY, String.class ) );

    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final GlobalFooterConfigUse globalFooterConfigUse = new GlobalFooterConfigUse();

        // verify logic
        assertNotNull( globalFooterConfigUse );

    }

}
