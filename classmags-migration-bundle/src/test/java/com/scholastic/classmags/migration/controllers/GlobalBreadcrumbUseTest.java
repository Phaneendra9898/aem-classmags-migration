package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.support.membermodification.MemberModifier.stub;

import org.apache.commons.lang.StringUtils;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ValueMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.slf4j.Logger;

import com.day.cq.wcm.api.Page;
import com.scholastic.classmags.migration.services.MagazineProps;
import com.scholastic.classmags.migration.services.PropertyConfigService;
import com.scholastic.classmags.migration.services.ResourcePathConfigService;
import com.scholastic.classmags.migration.services.impl.PropertyConfigServiceImpl;
import com.scholastic.classmags.migration.services.impl.ResourcePathConfigServiceImpl;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;
import com.scholastic.classmags.migration.utils.CommonUtils;
import com.scholastic.classmags.migration.utils.RoleUtil;

/**
 * JUnit for GlobalBreadcrumbUse.
 */
@RunWith( PowerMockRunner.class )
@PrepareForTest( { GlobalBreadcrumbUse.class, RoleUtil.class, CommonUtils.class } )
public class GlobalBreadcrumbUseTest extends BaseComponentTest {

    private static final String CURRENT_PAGE_PATH = "/content/classroom_magazines/scienceworld/issues/LATEST-ISSUE-PAGE/MAIN-ARTILCE-1";
    private static final String MAGAZINE_NAME = "scienceworld";
    private static final String SCIENCE_WORLD_DATA_PAGE_PATH = "/content/classroom-magazines-admin/scienceworld/scienceworld-data-config-page";
    private static final String GLOBAL_DATA_PAGE_PATH = "/content/classroom-magazines-admin/global-data-config-page";
    private static final String SCIENCE_WORLD_DATE_FORMAT = "MMM d, yyyy";
    private static final String GLOBAL_DATE_FORMAT = "MMMM d, YYYY";
    private static final String HOME_PAGE_LOGGED_IN = "/content/classroom_magazines/scienceworld/issues/HOME-PAGE-LOGGED-IN";
    private static final String HOME_PAGE_LOGGED_OUT = "/content/classroom_magazines/scienceworld/issues/HOME-PAGE-LOGGED-OUT";
    private static final String ISSUE_DATE_SCIENCEWORLD_FORMAT = "Jan 1, 2017";
    private static final String ISSUE_DATE_GLOBAL_FORMAT = "January 1, 2017";
    private static final String ARTICLE_TEMPLATE_PATH = "apps/scholastic/classroom-magazines-migration/templates/article-page";
    private static final String ISSUE_PAGE_PATH = "/content/classmags-migration/scienceworld/issue/01012017";
    private static final String ARTICLE_PAGE_TITLE = "Gone Batty";

    private GlobalBreadcrumbUse globalBreadcrumbUse;
    private PropertyConfigServiceImpl propertyConfigService;
    private ResourcePathConfigService resourcePathConfigService;
    private MagazineProps magazineProps;
    private Page page1, page2;
    private ValueMap pageProperties1, pageProperties2;
    private Logger logger;
    private Resource jcrContentResource;
    private ValueMap pageProperties;

    /**
     * Setup.
     * 
     * @throws LoginException
     */
    @Before
    public void setup() throws LoginException {
        globalBreadcrumbUse = Whitebox.newInstance( GlobalBreadcrumbUse.class );

        propertyConfigService = mock( PropertyConfigServiceImpl.class );
        resourcePathConfigService = mock( ResourcePathConfigServiceImpl.class );
        magazineProps = mock( MagazineProps.class );
        logger = mock( Logger.class );
        jcrContentResource = mock( Resource.class );
        pageProperties = mock( ValueMap.class );

        page1 = mock( Page.class );
        page2 = mock( Page.class );

        pageProperties1 = mock( ValueMap.class );
        pageProperties2 = mock( ValueMap.class );

        stubCommon( globalBreadcrumbUse );

        PowerMockito.mockStatic( RoleUtil.class );
        PowerMockito.mockStatic( CommonUtils.class );

        Whitebox.setInternalState( GlobalBreadcrumbUse.class, "LOG", logger );

        when( RoleUtil.getUserRole( request ) ).thenReturn( "student" );
        when( slingScriptHelper.getService( PropertyConfigService.class ) ).thenReturn( propertyConfigService );
        when( slingScriptHelper.getService( ResourcePathConfigService.class ) ).thenReturn( resourcePathConfigService );
        when( slingScriptHelper.getService( MagazineProps.class ) ).thenReturn( magazineProps );
        when( request.getResource() ).thenReturn( resource );
        when( resource.getPath() ).thenReturn( CURRENT_PAGE_PATH );
        when( currentPage.getPath() ).thenReturn( CURRENT_PAGE_PATH );
        when( magazineProps.getMagazineName( CURRENT_PAGE_PATH ) ).thenReturn( MAGAZINE_NAME );
        when( CommonUtils.getDataPath( resourcePathConfigService, MAGAZINE_NAME ) )
                .thenReturn( SCIENCE_WORLD_DATA_PAGE_PATH );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                SCIENCE_WORLD_DATA_PAGE_PATH ) ).thenReturn( SCIENCE_WORLD_DATE_FORMAT );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.HOMEPAGE_LOGGEDIN,
                SCIENCE_WORLD_DATA_PAGE_PATH ) ).thenReturn( HOME_PAGE_LOGGED_IN );

        when( currentPage.getContentResource() ).thenReturn( jcrContentResource );
        when( jcrContentResource.getValueMap() ).thenReturn( pageProperties );
        when( pageProperties.get( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, StringUtils.EMPTY ) )
                .thenReturn( ClassMagsMigrationConstants.ARTICLE_PAGE_TEMPLATE_PATH );

        when( currentPage.getDepth() ).thenReturn( 4 );
        when( currentPage.getAbsoluteParent( 2 ) ).thenReturn( page1 );
        when( currentPage.getAbsoluteParent( 3 ) ).thenReturn( page2 );

        when( page1.getProperties() ).thenReturn( pageProperties1 );
        when( page2.getProperties() ).thenReturn( pageProperties2 );

        when( pageProperties1.get( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG ) )
                .thenReturn( ClassMagsMigrationConstants.ISSUE_PAGE_TEMPLATE_PATH );
        when( pageProperties2.get( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG ) )
                .thenReturn( ARTICLE_TEMPLATE_PATH );

        when( CommonUtils.getDisplayDate( pageProperties1, SCIENCE_WORLD_DATE_FORMAT ) )
                .thenReturn( ISSUE_DATE_SCIENCEWORLD_FORMAT );
        when( page2.getTitle() ).thenReturn( ARTICLE_PAGE_TITLE );
        when( page1.getPath() ).thenReturn( ISSUE_PAGE_PATH );
        when( page2.getPath() ).thenReturn( ISSUE_PAGE_PATH + "/gone-batty" );
    }

    @Test
    public void testGlobalBreadcrumbUse() throws Exception {
        // execute logic
        Whitebox.invokeMethod( globalBreadcrumbUse, "activate" );

        // verify logic

        assertEquals( ISSUE_DATE_SCIENCEWORLD_FORMAT, globalBreadcrumbUse.getBreadCrumbLinks().get( 1 ).getTitle() );
        assertEquals( ISSUE_PAGE_PATH, globalBreadcrumbUse.getBreadCrumbLinks().get( 1 ).getPath() );

    }

    @Test
    public void testGlobalBreadcrumbUseForAnonymousUser() throws Exception {
        // setup logic (test-specific)
        when( RoleUtil.getUserRole( request ) ).thenReturn( "anonymous" );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.HOMEPAGE_LOGGEDOUT,
                SCIENCE_WORLD_DATA_PAGE_PATH ) ).thenReturn( HOME_PAGE_LOGGED_OUT );
        // execute logic
        Whitebox.invokeMethod( globalBreadcrumbUse, "activate" );

        // verify logic
        assertEquals( 3, globalBreadcrumbUse.getBreadCrumbLinks().size() );
        assertEquals( HOME_PAGE_LOGGED_OUT, globalBreadcrumbUse.getBreadCrumbLinks().get( 0 ).getPath() );
        assertEquals( ISSUE_DATE_SCIENCEWORLD_FORMAT, globalBreadcrumbUse.getBreadCrumbLinks().get( 1 ).getTitle() );
        assertEquals( ISSUE_PAGE_PATH, globalBreadcrumbUse.getBreadCrumbLinks().get( 1 ).getPath() );

    }

    @Test
    public void testGlobalBreadcrumbUseWhenSlingScriptHelperIsNull() throws Exception {
        // setup logic (test-specific)
        stub( PowerMockito.method( globalBreadcrumbUse.getClass(), "getSlingScriptHelper" ) ).toReturn( null );

        // execute logic
        Whitebox.invokeMethod( globalBreadcrumbUse, "activate" );

        // verify logic
        assertTrue( globalBreadcrumbUse.getBreadCrumbLinks().isEmpty() );
    }

    @Test
    public void testGlobalBreadcrumbUseWhenPropertyConfigServiceIsNull() throws Exception {
        // setup logic (test-specific)
        when( slingScriptHelper.getService( PropertyConfigService.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( globalBreadcrumbUse, "activate" );

        // verify logic
        assertEquals( 2, globalBreadcrumbUse.getBreadCrumbLinks().size() );
    }

    @Test
    public void testGlobalBreadcrumbUseWhenResourcePathConfigServiceIsNull() throws Exception {
        // setup logic (test-specific)
        when( slingScriptHelper.getService( ResourcePathConfigService.class ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( globalBreadcrumbUse, "activate" );

        // verify logic
        assertEquals( 2, globalBreadcrumbUse.getBreadCrumbLinks().size() );
    }

    @Test
    public void testGlobalBreadcrumbUseWhenMagazinePropsIsNull() throws Exception {
        // setup logic (test-specific)
        when( slingScriptHelper.getService( MagazineProps.class ) ).thenReturn( null );

        when( CommonUtils.getDataPath( resourcePathConfigService, StringUtils.EMPTY ) )
                .thenReturn( GLOBAL_DATA_PAGE_PATH );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.JCR_SORTING_DATE_FORMAT,
                GLOBAL_DATA_PAGE_PATH ) ).thenReturn( GLOBAL_DATE_FORMAT );
        when( CommonUtils.getDisplayDate( pageProperties1, GLOBAL_DATE_FORMAT ) )
                .thenReturn( ISSUE_DATE_GLOBAL_FORMAT );
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.HOMEPAGE_LOGGEDIN,
                GLOBAL_DATA_PAGE_PATH ) ).thenReturn( HOME_PAGE_LOGGED_IN );
        // execute logic
        Whitebox.invokeMethod( globalBreadcrumbUse, "activate" );

        // verify logic
        assertEquals( HOME_PAGE_LOGGED_IN, globalBreadcrumbUse.getBreadCrumbLinks().get( 0 ).getPath() );
        assertEquals( ISSUE_DATE_GLOBAL_FORMAT, globalBreadcrumbUse.getBreadCrumbLinks().get( 1 ).getTitle() );
    }

    @Test
    public void testGlobalBreadcrumbUseWhenHomePageIsAccessed() throws Exception {
        // setup logic (test-specific)
        when( currentPage.getDepth() ).thenReturn( 3 );

        // execute logic
        Whitebox.invokeMethod( globalBreadcrumbUse, "activate" );

        // verify logic
        assertEquals( 1, globalBreadcrumbUse.getBreadCrumbLinks().size() );
        assertEquals( ClassMagsMigrationConstants.HOMEPAGE,
                globalBreadcrumbUse.getBreadCrumbLinks().get( 0 ).getTitle() );
    }

    @Test
    public void testGlobalBreadcrumbUseWhenAbsoluteParentIsNull() throws Exception {
        // setup logic (test-specific)
        when( currentPage.getAbsoluteParent( 2 ) ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( globalBreadcrumbUse, "activate" );

        // verify logic
        assertEquals( 2, globalBreadcrumbUse.getBreadCrumbLinks().size() );
        assertEquals( ClassMagsMigrationConstants.HOMEPAGE,
                globalBreadcrumbUse.getBreadCrumbLinks().get( 0 ).getTitle() );
        assertEquals( ARTICLE_PAGE_TITLE, globalBreadcrumbUse.getBreadCrumbLinks().get( 1 ).getTitle() );
    }

    @Test
    public void testGlobalBreadcrumbUseWhenHideInBreadcrumbIsTrue() throws Exception {
        // setup logic (test-specific)
        when( pageProperties1.containsKey( "hideInBreadcrumb" ) ).thenReturn( true );
        when( pageProperties1.get( "hideInBreadcrumb", Boolean.class ) ).thenReturn( true );

        // execute logic
        Whitebox.invokeMethod( globalBreadcrumbUse, "activate" );

        // verify logic
        assertEquals( 2, globalBreadcrumbUse.getBreadCrumbLinks().size() );
        assertEquals( ClassMagsMigrationConstants.HOMEPAGE,
                globalBreadcrumbUse.getBreadCrumbLinks().get( 0 ).getTitle() );
        assertEquals( ARTICLE_PAGE_TITLE, globalBreadcrumbUse.getBreadCrumbLinks().get( 1 ).getTitle() );
    }

    @Test
    public void testGlobalBreadcrumbUseWhenValueMapIsNull() throws Exception {
        // setup logic (test-specific)
        when( page1.getProperties() ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( globalBreadcrumbUse, "activate" );

        // verify logic
        assertEquals( 3, globalBreadcrumbUse.getBreadCrumbLinks().size() );
        assertEquals( ClassMagsMigrationConstants.HOMEPAGE,
                globalBreadcrumbUse.getBreadCrumbLinks().get( 0 ).getTitle() );
    }

    @Test
    public void testGlobalBreadcrumbUseWhenJcrNodeResourceIsNull() throws Exception {
        // setup logic (test-specific)
        when( currentPage.getContentResource() ).thenReturn( null );

        // execute logic
        Whitebox.invokeMethod( globalBreadcrumbUse, "activate" );

        // verify logic
        assertFalse( globalBreadcrumbUse.getHideBreadcrumb() );
    }

    @Test
    public void testGlobalBreadcrumbUseWhenTemplateTypeIsHomePage() throws Exception {
        // setup logic (test-specific)
        when( pageProperties.get( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, StringUtils.EMPTY ) )
                .thenReturn( ClassMagsMigrationConstants.HOME_PAGE_TEMPLATE_PATH );
        // execute logic
        Whitebox.invokeMethod( globalBreadcrumbUse, "activate" );

        // verify logic
        assertTrue( globalBreadcrumbUse.getHideBreadcrumb() );
    }

    @Test
    public void testGlobalBreadcrumbUseWhenTemplateTypeIsErrorPage() throws Exception {
        // setup logic (test-specific)
        when( pageProperties.get( ClassMagsMigrationConstants.QUERY_TEMPLATE_TAG, StringUtils.EMPTY ) )
                .thenReturn( ClassMagsMigrationConstants.ERROR_PAGE_TEMPLATE_PATH );
        // execute logic
        Whitebox.invokeMethod( globalBreadcrumbUse, "activate" );

        // verify logic
        assertTrue( globalBreadcrumbUse.getHideBreadcrumb() );
    }

    @Test
    public void testGlobalBreadcrumbUseWhenLoginExceptionOccurs() throws Exception {
        // setup logic (test-specific)
        LoginException e = new LoginException();
        when( propertyConfigService.getDataConfigProperty( ClassMagsMigrationConstants.HOMEPAGE_LOGGEDIN,
                SCIENCE_WORLD_DATA_PAGE_PATH ) ).thenThrow( e );
        // execute logic
        Whitebox.invokeMethod( globalBreadcrumbUse, "activate" );

        // verify logic
        verify( logger ).error( "Login error occured in breadcrumb" + e );
    }

    @Test
    public void testCreateInstance() {
        // execute logic
        final GlobalBreadcrumbUse globalBreadcrumbUse = new GlobalBreadcrumbUse();

        // verify logic
        assertNotNull( globalBreadcrumbUse );

    }

}
