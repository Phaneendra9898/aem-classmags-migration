package com.scholastic.classmags.migration.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.script.Bindings;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.scripting.SlingScriptHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.scholastic.classmags.migration.services.ArticleToolbarDataService;
import com.scholastic.classmags.migration.utils.ClassMagsMigrationConstants;

/**
 * JUnit for ArticleToolbarUse .
 */
@RunWith( MockitoJUnitRunner.class )
public class ArticleToolbarUseTest {

    /** The Constant TEST_NODE_PATH. */
    private static final String TEST_NODE_PATH = "/content/classroom-magazines/science-world/english/issuepage/testarticletoolbar";

    /** The Constant TEST_VALUE. */
    private static final int TEST_VALUE = 2;

    /** The Constant TEST_VALUE. */
    private static final List<String> TEST_LEXILE_VALUE = new ArrayList<String> ();

    /** The Constant TEST_PATH. */
    private static final String TEST_PATH = "/content/classroom-magazines/science-world/english/issuepage.html";

    /** The lexile levels count. */
    private String lexileLevelsCount;

    /** The article toolbar use. */
    private ArticleToolbarUse articleToolbarUse;

    /** The bindings. */
    @Mock
    private Bindings bindings;

    /** The current resource. */
    @Mock
    private Resource currentResource;

    /** The request. */
    @Mock
    private SlingHttpServletRequest request;

    /** The sling script helper. */
    @Mock
    private SlingScriptHelper slingScriptHelper;

    /** The resource resolver. */
    @Mock
    private ResourceResolver resourceResolver;

    /** The page manager. */
    @Mock
    private PageManager pageManager;

    /** The page. */
    @Mock
    private Page page;

    /** The article toolbar data service. */
    @Mock
    private ArticleToolbarDataService articleToolbarDataService;

    /**
     * Setup.
     *
     * @throws LoginException
     *             the login exception
     */
    @Before
    public void setup() throws LoginException {
        articleToolbarUse = new ArticleToolbarUse();
        TEST_LEXILE_VALUE.add("101");
        TEST_LEXILE_VALUE.add("102");
    }

    /**
     * Test get lexile levels count.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetLexileLevelsCount() throws Exception {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( request.getResource() ).thenReturn( currentResource );
        when( currentResource.getResourceResolver() ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( PageManager.class ) ).thenReturn( pageManager );
        when( pageManager.getContainingPage( currentResource ) ).thenReturn( page );
        when( page.getPath() ).thenReturn( TEST_NODE_PATH );
        when( slingScriptHelper.getService( ArticleToolbarDataService.class ) ).thenReturn( articleToolbarDataService );
        when( articleToolbarDataService
                .fetchLexileLevels( TEST_NODE_PATH.concat( ClassMagsMigrationConstants.LEXILE_LEVEL_NODE ) ) )
                        .thenReturn( TEST_LEXILE_VALUE );

        articleToolbarUse.init( bindings );
        articleToolbarUse.activate();
        assert( !articleToolbarUse.getLexileLevels().isEmpty() );
    }

    /**
     * Test get parent issue page path.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetParentIssuePagePath() throws Exception {

        lexileLevelsCount = String.valueOf( TEST_VALUE );

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( request.getResource() ).thenReturn( currentResource );
        when( currentResource.getResourceResolver() ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( PageManager.class ) ).thenReturn( pageManager );
        when( pageManager.getContainingPage( currentResource ) ).thenReturn( page );
        when( page.getPath() ).thenReturn( TEST_NODE_PATH );
        when( slingScriptHelper.getService( ArticleToolbarDataService.class ) ).thenReturn( articleToolbarDataService );
        when( articleToolbarDataService.fetchParentIssuePagePath( page ) ).thenReturn( TEST_PATH );

        articleToolbarUse.init( bindings );
        articleToolbarUse.activate();
        assertEquals( TEST_PATH, articleToolbarUse.getParentIssuePagePath() );
    }

    /**
     * Test get digital issue link.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testGetDigitalIssueLink() throws Exception {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( request.getResource() ).thenReturn( currentResource );
        when( currentResource.getResourceResolver() ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( PageManager.class ) ).thenReturn( pageManager );
        when( pageManager.getContainingPage( currentResource ) ).thenReturn( page );
        when( page.getPath() ).thenReturn( TEST_NODE_PATH );
        when( slingScriptHelper.getService( ArticleToolbarDataService.class ) ).thenReturn( articleToolbarDataService );
        when( articleToolbarDataService
                .fetchdigitalIssueLink( TEST_NODE_PATH.concat( ClassMagsMigrationConstants.ARTICLE_CONFIGURATION_PATH ) ) )
                        .thenReturn( TEST_PATH );

        articleToolbarUse.init( bindings );
        articleToolbarUse.activate();
        assertEquals( TEST_PATH, articleToolbarUse.getDigitalIssueLink() );
    }

    /**
     * Test activate with no sling script helper.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testActivateWithNoSlingScriptHelper() throws Exception {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "sling" ) ).thenReturn( null );
        when( request.getResource() ).thenReturn( currentResource );
        when( currentResource.getResourceResolver() ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( PageManager.class ) ).thenReturn( pageManager );
        when( pageManager.getContainingPage( currentResource ) ).thenReturn( page );
        when( page.getPath() ).thenReturn( TEST_NODE_PATH );

        articleToolbarUse.init( bindings );
        articleToolbarUse.activate();
        assertEquals( null, articleToolbarUse.getLexileLevels() );
    }

    /**
     * Test activate with no page manager.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testActivateWithNoPageManager() throws Exception {

        when( bindings.get( "request" ) ).thenReturn( request );
        when( request.getResource() ).thenReturn( currentResource );
        when( currentResource.getResourceResolver() ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( PageManager.class ) ).thenReturn( null );

        articleToolbarUse.init( bindings );
        articleToolbarUse.activate();
        assertEquals( null, articleToolbarUse.getLexileLevels() );
    }

    /**
     * Test activate with no service.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testActivateWithNoService() throws Exception {

        lexileLevelsCount = String.valueOf( TEST_VALUE );

        when( bindings.get( "request" ) ).thenReturn( request );
        when( bindings.get( "sling" ) ).thenReturn( slingScriptHelper );
        when( request.getResource() ).thenReturn( currentResource );
        when( currentResource.getResourceResolver() ).thenReturn( resourceResolver );
        when( resourceResolver.adaptTo( PageManager.class ) ).thenReturn( pageManager );
        when( pageManager.getContainingPage( currentResource ) ).thenReturn( page );
        when( page.getPath() ).thenReturn( TEST_NODE_PATH );
        when( slingScriptHelper.getService( ArticleToolbarDataService.class ) ).thenReturn( null );

        articleToolbarUse.init( bindings );
        articleToolbarUse.activate();
        assertEquals( null, articleToolbarUse.getLexileLevels() );
    }
}
